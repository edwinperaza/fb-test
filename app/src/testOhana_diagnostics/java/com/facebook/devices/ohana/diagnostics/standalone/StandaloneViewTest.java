package com.facebook.devices.ohana.diagnostics.standalone;

import android.content.Intent;
import android.support.v7.widget.CardView;
import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.ohana.diagnostics.activities.StandaloneActivity;
import com.facebook.devices.ohana.diagnostics.mvp.view.StandaloneView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.ohana.diagnostics.utils.recyclers.GroupsRecyclerAdapter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class StandaloneViewTest {

    private StandaloneView view;
    private StandaloneActivity activity;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private GroupsRecyclerAdapter adapter;

    private CardView cardRunAll;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, StandaloneActivity.class);
        activity = Robolectric.buildActivity(StandaloneActivity.class, intent).create().start().resume().get();
        view = new StandaloneView(activity, bus);

        cardRunAll = activity.findViewById(R.id.card_run_all);
    }

    @Test
    public void init() {
        /*Arrange*/
        String title = activity.getString(com.facebook.devices.R.string.quality_toolbar_title);
        /*Assert*/
        assertEquals(activity.getToolbarTitle(), title);
        assertFalse(activity.getToolbar().isBackButtonVisible());
    }

    @Test
    public void onRunAllClickedTest() {
        cardRunAll.performClick();
        verify(bus).post(any(StandaloneView.RunAllClickEvent.class));
    }

    @Test
    @Ignore
    public void updateRecyclerItemsTest() {
        //Arrange
        TestInfoEntity testInfo1 = new TestInfoEntity("0", "TestOne", "TestOne", "Some Desc", "Type1", TestInfoEntity.PASS,  0, 0);
        testInfo1.groupId = "Type1";
        Map<String, TestInfoEntity> map = new HashMap<>();
        map.put("Type1", testInfo1);

        GroupTestsEntity group = new GroupTestsEntity("icon", "Type1");
        List<GroupTestsEntity> groupTestsEntityList = new ArrayList<>();
        groupTestsEntityList.add(group);

        //Act
        view.updateRecyclerItems(groupTestsEntityList, map);
        //Assert
        verify(adapter).updateItems(groupTestsEntityList, map);
    }
}