package com.facebook.devices.ohana.diagnostics.testsresults;

import android.os.Handler;

import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.ohana.diagnostics.mvp.model.TestsResultsModel;
import com.facebook.devices.ohana.diagnostics.mvp.presenter.TestsResultsPresenter;
import com.facebook.devices.ohana.diagnostics.mvp.view.TestsResultsView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestsResultsPresenterTest {

    private TestsResultsPresenter presenter;

    @Mock
    private TestsResultsView view;
    @Mock
    private TestsResultsModel model;
    @Mock
    private Handler mockHandler;
    @Captor
    private ArgumentCaptor<Runnable> runnableArgumentCaptor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getUIHandler()).thenReturn(mockHandler);
        presenter = new TestsResultsPresenter(model, view);
    }

    @Test
    public void onResumeTest() {
        //Act
        presenter.onResume();
        //Assert
        verify(model).getTestResults();
    }

    @Test
    public void onTestResultsEventSomeTestFailedTest() {
        //Arrange
        List<TestInfoEntity> testInfoEntityList = new ArrayList<>();
        String serialNumber = "serialNumber";
        String data = "datamatrix";
        long totalFailedTest = 10L;
        long totalPassedTest = 5L;
        long totalTime = 2000L;
        TestsResultsModel.TestResultsEvent event = new TestsResultsModel.TestResultsEvent(testInfoEntityList,
                true,
                data,
                totalFailedTest,
                totalPassedTest,
                totalTime,
                serialNumber);
        //Act
        presenter.onTestResultsEvent(event);
        //Assert
        verify(view.getUIHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();

        verify(view).setResumeTestValues(serialNumber, totalFailedTest, totalPassedTest, totalTime);
        verify(view).updateRecyclerItems(testInfoEntityList);

        verify(view).setToolbarFail();
        verify(view, never()).setToolbarSuccess();
        verify(view).drawDataMatrix(data);
    }

    @Test
    public void onTestResultsEventTest() {
        //Arrange
        List<TestInfoEntity> testInfoEntityList = new ArrayList<>();
        String serialNumber = "serialNumber";
        String data = "datamatrix";
        long totalFailedTest = 10L;
        long totalPassedTest = 5L;
        long totalTime = 2000L;
        TestsResultsModel.TestResultsEvent event = new TestsResultsModel.TestResultsEvent(testInfoEntityList,
                false,
                data,
                totalFailedTest,
                totalPassedTest,
                totalTime,
                serialNumber);
        //Act
        presenter.onTestResultsEvent(event);
        //Assert
        verify(view.getUIHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();

        verify(view).setResumeTestValues(serialNumber, totalFailedTest, totalPassedTest, totalTime);
        verify(view).updateRecyclerItems(testInfoEntityList);

        verify(view).setToolbarSuccess();
        verify(view, never()).setToolbarFail();
        verify(view).drawDataMatrix(data);
    }
}
