package com.facebook.devices.ohana.diagnostics.testsresults;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.db.dao.TestInfoDao;
import com.facebook.devices.ohana.diagnostics.mvp.model.TestsResultsModel;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.db.entity.GroupTestsEntity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestsResultsModelTest {

    private TestsResultsModel model;
    private List<TestInfoEntity> testInfoList = new ArrayList<>();

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private Executor executor;

    @Mock
    private TestDatabase testDatabase;

    @Mock
    private TestInfoDao testInfoDao;

    @Captor
    private ArgumentCaptor<Runnable> runnableArgumentCaptor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        List<GroupTestsEntity> groupTests = new ArrayList<>();
        List<String> testIdList = new ArrayList<>();
        Map<String, TestInfoEntity> testsInfo = new HashMap<>();

        groupTests.add(new GroupTestsEntity("icon", "Type1"));

        /*Populate Tests*/
        TestInfoEntity testInfo1 = new TestInfoEntity("0", "TestOne", "TestOne", "Some Desc", "Type1", TestInfoEntity.PASS,  0, 0);
        TestInfoEntity testInfo2 = new TestInfoEntity("1", "TestTwo", "TestTwo", "Some Desc", "Type1", TestInfoEntity.PASS, 1, 0);
        TestInfoEntity testInfo3 = new TestInfoEntity("2", "TestThree", "TestThree", "Some Desc", "Type1", TestInfoEntity.PASS, 2, 0);

        testInfoList.add(testInfo1);
        testInfoList.add(testInfo2);
        testInfoList.add(testInfo3);

        testIdList.add("0");
        testIdList.add("1");
        testIdList.add("2");


        testsInfo.put("TestOne", testInfo1);
        testsInfo.put("TestTwo", testInfo2);
        testsInfo.put("TestThree", testInfo3);

        model = new TestsResultsModel(testDatabase, true, bus, executor);
    }

    @Test
    public void getTestResultsTest() {
        when(testDatabase.testInfoDao()).thenReturn(testInfoDao);
        //Act
        model.getTestResults();
        //Assert
        verify(executor).execute(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(bus).post(any(TestsResultsModel.TestResultsEvent.class));
    }
}