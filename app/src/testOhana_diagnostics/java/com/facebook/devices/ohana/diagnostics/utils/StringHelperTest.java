package com.facebook.devices.ohana.diagnostics.utils;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class StringHelperTest {

    @Test
    public void getTimeFromMillisecondsTest() {
        // Less than 1000
        assertEquals("0:0:0.100", StringHelper.getTimeFromMilliseconds(100));
        // Equals 1000
        assertEquals("00:00:01", StringHelper.getTimeFromMilliseconds(1000));
        // More than 1000
        assertEquals("00:01:10", StringHelper.getTimeFromMilliseconds(70000));
    }
}
