package com.facebook.devices.ohana.diagnostics.mvp.view;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.ohana.diagnostics.activities.TestsResultsActivity;
import com.facebook.devices.ohana.diagnostics.utils.StringHelper;
import com.facebook.devices.ohana.diagnostics.utils.recyclers.ResultTestsRecyclerAdapter;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.hwtp.mvp.view.ViewBase;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.DataMatrixWriter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestsResultsView extends ViewBase<TestsResultsActivity> {

    @BindView(R.id.recycler_tests) RecyclerView recyclerTests;
    @BindView(R.id.iv_result_data_matrix) ImageView ivResultDataMatrix;
    @BindView(R.id.iv_overall_result) ImageView ivResultOverall;
    @BindView(R.id.tv_overall_result) TextView tvResultOverall;
    @BindView(R.id.ll_result_header_container) LinearLayout llResultOverallContainer;
    @BindView(R.id.tv_total_failed_test_value) TextView tvTotalFailedTest;
    @BindView(R.id.tv_total_passed_test_value) TextView tvTotalPassedTest;
    @BindView(R.id.tv_result_total_time_spent) TextView tvTotalTimeSpent;
    @BindView(R.id.tv_result_serial_number) TextView tvSerialNumber;

    private ResultTestsRecyclerAdapter adapter;
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public TestsResultsView(TestsResultsActivity activity) {
        super(activity);
        ButterKnife.bind(this, activity);

        setupToolbar();
        setupRecycler();
    }

    public Handler getUIHandler() {
        return uiHandler;
    }

    private void setupToolbar() {
        getActivity().setToolbarTitle(getContext().getString(R.string.multiple_test_result_toolbar_title));
    }

    private void setupRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        llm.setAutoMeasureEnabled(true);
        recyclerTests.setLayoutManager(llm);
        recyclerTests.setHasFixedSize(true);
        /* Adapter creation */
        adapter = new ResultTestsRecyclerAdapter(getContext(), new ArrayList<>());
        recyclerTests.setAdapter(adapter);
    }

    public void setToolbarSuccess() {
        llResultOverallContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.light_sage));
        ivResultOverall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.passed_bar));
        tvResultOverall.setTextColor(ContextCompat.getColor(getContext(), R.color.turtle_green));
        tvResultOverall.setText(R.string.overall_test_passed);
    }

    public void setToolbarFail() {
        llResultOverallContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.beige));
        ivResultOverall.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.failed_bar));
        tvResultOverall.setTextColor(ContextCompat.getColor(getContext(), R.color.warm_pink));
        tvResultOverall.setText(R.string.overall_test_failed);
    }

    public void updateRecyclerItems(List<TestInfoEntity> testInfoEntityList) {
        adapter.updateItems(testInfoEntityList);
    }

    public void drawDataMatrix(String data) {
        BitMatrix bitMatrix = new DataMatrixWriter().encode(data, BarcodeFormat.DATA_MATRIX, 300, 300);

        int BLACK = 0xFF000000;
        int WHITE = 0xFFFFFFFF;

        // change the values to your needs
        int requestedWidth = 300;
        int requestedHeight = 300;

        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();

        // calculating the scaling factor
        int pixelSize = requestedWidth / width;
        if (pixelSize > requestedHeight / height) {
            pixelSize = requestedHeight / height;
        }

        int[] pixels = new int[requestedWidth * requestedHeight];

        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * requestedWidth * pixelSize;

            // scaling pixel height
            for (int pixelSizeHeight = 0; pixelSizeHeight < pixelSize; pixelSizeHeight++, offset += requestedWidth) {
                for (int x = 0; x < width; x++) {
                    int color = bitMatrix.get(x, y) ? BLACK : WHITE;

                    // scaling pixel width
                    for (int pixelSizeWidth = 0; pixelSizeWidth < pixelSize; pixelSizeWidth++) {
                        pixels[offset + x * pixelSize + pixelSizeWidth] = color;
                    }
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(requestedWidth, requestedHeight, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, requestedWidth, 0, 0, requestedWidth, requestedHeight);

        ivResultDataMatrix.setImageBitmap(bitmap);
    }

    public void setResumeTestValues(String serialNumber, long totalFailedTest, long totalPassedTest, long totalTime) {
        tvSerialNumber.setText(serialNumber);
        tvTotalFailedTest.setText(String.valueOf(totalFailedTest));
        tvTotalPassedTest.setText(String.valueOf(totalPassedTest));
        tvTotalTimeSpent.setText(StringHelper.getTimeFromMilliseconds(totalTime));
    }
}