package com.facebook.devices.ohana.diagnostics.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.hwtp.service.ConnectionService;
import com.facebook.hwtp.wire.TestResponseStatus;
import com.facebook.hwtp.wire.WireController;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.hwtp.wire.WireController.TestResult;

public class HwtpServer extends Service {

    public static final String HWTP_SERVER_SERVICE_FILTER = "com.facebook.devices.aloha.diagnostics.service.HwtpServer";
    public static final String PASS = "pass";

    public static final String ACTION_PARAM = "action_param";
    public static final String TESTS_PARAM = "tests_param";

    public static final String RUN_TESTS = "run_tests";
    public static final String STOP_TEST = "stop_action";

    private static final String START_PARAM = "START_SERVICE";
    private static final String START_ACTION = "START_ACTION";
    private static final String PARAM_PORT = "PORT";
    private static final String PARAM_SESSION_ID = "SESSION_ID";

    private TestWorker worker;

    private TestDatabase testDatabase = TestDatabase.getDatabase(this);

    @Override
    public void onDestroy() {
        if (worker != null) {
            worker.stop();
        }
        worker = null;
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null && intent.getStringExtra(ACTION_PARAM) != null) {
            switch (intent.getStringExtra(ACTION_PARAM)) {

                case RUN_TESTS:
                    worker = new TestWorker();
                    execute(worker);
                    return START_STICKY;

                case STOP_TEST:
                    if (worker != null) {
                        worker.stop();
                    }
                    stopSelf();
                    return START_NOT_STICKY;
            }
        }

        return START_STICKY;
    }

    private void execute(Runnable worker) {
        Thread t = new Thread(worker, "Hwtp Server");
        t.start();
    }

    private class TestWorker implements Runnable {

        private volatile boolean running = true;

        public TestWorker() {}

        public void stop() {
            running = false;
        }

        @Override
        public void run() {
            try (ServerSocket serverSocket = new ServerSocket(0)) {

                Intent i = new Intent(HwtpServer.this, ConnectionService.class);
                i.putExtra(START_ACTION, START_PARAM);
                i.putExtra(PARAM_PORT, String.valueOf(serverSocket.getLocalPort()));
                i.putExtra(PARAM_SESSION_ID, "Stand alone Connection");
                startService(i);

                try (WireController wc = new WireController(serverSocket.accept())) {

                    List<TestInfoEntity> testsList = testDatabase.testInfoDao().findTestsByStatus(TestInfoEntity.RUNNING);

                    for (TestInfoEntity test : testsList) {

                        List<ArgumentEntity> argsEntity = testDatabase.argumentDao().getArgumentsByTest(test.id);
                        Map<String, String> arguments = new HashMap<>();
                        for (ArgumentEntity arg: argsEntity) {
                            arguments.put(arg.name, arg.value);
                        }

                        long initTimer = System.currentTimeMillis();
                        TestResult testResult = wc.test(test.getCanonicalName(), arguments);
                        test.time = System.currentTimeMillis() - initTimer;

                        boolean didPass = didPass(testResult);
                        test.status = didPass ? TestInfoEntity.PASS : TestInfoEntity.FAIL;

                        if (!didPass && testResult.output != null) {
                            for (String s : testResult.output) {
                                Log.w(HwtpServer.class.getName(), s);
                            }
                        }

                        testDatabase.testInfoDao().updateTestInfo(test);

                        if (!running) {
                            break;
                        }
                    }

                    notifyExecutionIsFinished();
                } catch (Exception e) {
                    Log.w(HwtpServer.class.getName(), e.getMessage());
                    running = false;
                }

            } catch (IOException e) {
                Log.w(HwtpServer.class.getName(), e.getMessage());
                running = false;
            }

            worker = null;
        }
    }

    private boolean didPass(TestResult testResult) {
        return testResult != null && (
                TestResponseStatus.SUCCEEDED.equals(testResult.status) ||
                        TestResponseStatus.FORCE_SUCCEEDED.equals(testResult.status));
    }

    private void notifyExecutionIsFinished() {
        Intent intent = new Intent(HwtpServer.HWTP_SERVER_SERVICE_FILTER);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        /* Not used - service not bind */
        return null;
    }
}
