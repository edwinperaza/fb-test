package com.facebook.devices.ohana.diagnostics.utils.recyclers;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.ohana.diagnostics.utils.StringHelper;

import java.util.List;

public class ResultTestsRecyclerAdapter extends RecyclerView.Adapter<ResultTestsRecyclerAdapter.TestViewHolder> {

    private Context context;
    private List<TestInfoEntity> testsList;

    public ResultTestsRecyclerAdapter(Context context, List<TestInfoEntity> testsList) {
        this.context = context;
        this.testsList = testsList;
    }

    public void updateItems(List<TestInfoEntity> tests) {
        testsList = tests;
        notifyDataSetChanged();
    }

    @Override
    public TestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TestViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.test_result_recycler_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(TestViewHolder holder, int position) {
        TestInfoEntity test = testsList.get(position);
        String time = StringHelper.getTimeFromMilliseconds(test.getTime());
        /* Populate */
        holder.tvTitle.setText(test.getName());
        holder.tvDeltaTime.setText(time);
        testResult(holder, test);
    }

    private void testResult(TestViewHolder holder, TestInfoEntity test) {
        if (test.getStatus() == TestInfoEntity.PASS) {
            holder.ivTestResult.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.passed_small));
        } else {
            holder.ivTestResult.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.failed_small));
        }
    }

    @Override
    public int getItemCount() {
        return testsList.size();
    }

    public class TestViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivTestResult;
        public TextView tvTitle;
        public TextView tvDeltaTime;

        public TestViewHolder(View itemView) {
            super(itemView);

            ivTestResult = itemView.findViewById(R.id.iv_test_result);
            tvTitle = itemView.findViewById(R.id.tv_test_title);
            tvDeltaTime = itemView.findViewById(R.id.tv_test_delta_time);
        }
    }
}