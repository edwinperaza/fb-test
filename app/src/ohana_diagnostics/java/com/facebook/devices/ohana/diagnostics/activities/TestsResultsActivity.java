package com.facebook.devices.ohana.diagnostics.activities;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.HwtpApplication;
import com.facebook.devices.R;
import com.facebook.devices.activities.ToolBarActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.ohana.diagnostics.mvp.model.TestsResultsModel;
import com.facebook.devices.ohana.diagnostics.mvp.presenter.TestsResultsPresenter;
import com.facebook.devices.ohana.diagnostics.mvp.view.TestsResultsView;
import com.facebook.devices.db.TestDatabase;

public class TestsResultsActivity extends ToolBarActivity {

    private TestsResultsPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.results_activity_layout);

        boolean hasPermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;

        presenter = new TestsResultsPresenter(
                new TestsResultsModel(
                        TestDatabase.getDatabase(this),
                        hasPermission,
                        BusProvider.getInstance(),
                        HwtpApplication.getDatabaseIO()),
                new TestsResultsView(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        super.onStop();
    }
}
