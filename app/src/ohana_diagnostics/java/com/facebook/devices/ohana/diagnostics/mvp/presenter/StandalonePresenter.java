package com.facebook.devices.ohana.diagnostics.mvp.presenter;

import com.facebook.devices.ohana.diagnostics.mvp.model.StandaloneModel;
import com.facebook.devices.ohana.diagnostics.mvp.view.StandaloneView;

import org.greenrobot.eventbus.Subscribe;

public class StandalonePresenter {

    private StandaloneModel model;
    private StandaloneView view;

    public StandalonePresenter(StandaloneModel model, StandaloneView view) {
        this.model = model;
        this.view = view;

        /*Phone permission request*/
        this.model.requestPermissions();

        if (this.model.shouldReadPlan()) {
            model.readPlan();
        }

        model.getAllTestAndGroupInfo();
    }

    @Subscribe
    public void onTestAndGroupEvent(StandaloneModel.TestAndGroupEvent event) {
        view.getUIHandler().post(() -> this.view.updateRecyclerItems(event.groupTestList, event.testInfoEntityMap));
    }

    @Subscribe
    public void onRunAllClicked(StandaloneView.RunAllClickEvent event) {
        /*Test Tracker*/
        model.resetTutorialTracker();
        /*Execute Tests*/
        model.runAllTests();
    }

    @Subscribe
    public void onItemClickListener(StandaloneView.ItemClickListenerEvent event) {
        /*Test Tracker*/
        model.resetTutorialTracker();
        /*Execute test*/
        if (event.test != null) {
            model.runTest(event.test);
        }
    }
}