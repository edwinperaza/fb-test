package com.facebook.aloha.bluetoothremote;

interface IBluetoothRemotePairingService {
    boolean pairStoredRemote();
    boolean isRemoteConnected();
    boolean isRemotePaired();
    String getConnectedRemote();
    String getPairedRemote();
    boolean pairNewRemote(String address);
    boolean removeRemote(String address);
}