package com.facebook.devices;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.devices.db.TestDatabase;
import com.squareup.leakcanary.LeakCanary;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class HwtpApplication extends Application {

    private final static Executor databaseIO = Executors.newSingleThreadExecutor();

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        LeakCanary.install(this);

        //TODO tidy up
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putBoolean("should_read_plan", true).apply();

        getDatabaseIO().execute(() -> TestDatabase.getDatabase(this).clearAllTables());

        // Catch unhandled exception
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> Log.e(HwtpApplication.class.getName(), e.getMessage()));
    }

    public static Executor getDatabaseIO() {
        return databaseIO;
    }
}
