package com.facebook.devices.permission;

public interface PermissionHandler {

    boolean shouldRequestPermission();

    void requestPermission();

    boolean isPermissionGranted();

    void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults);
}
