package com.facebook.devices.permission;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import com.facebook.devices.bus.BusProvider;

public class RuntimePermissionHandler implements PermissionHandler {

    private static final int REQUEST_PERMISSION = 1;
    private Activity activity;
    private BusProvider.Bus bus;
    private String[] permissions;

    public RuntimePermissionHandler(Activity activity, BusProvider.Bus bus, String... permissions) {
        this.activity = activity;
        this.bus = bus;
        this.permissions = permissions;
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    public boolean shouldRequestPermission() {
        for (String permission : permissions) {
            if (activity.shouldShowRequestPermissionRationale(permission)) {
                return true;
            }
        }
        return false;
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermission() {
        activity.requestPermissions(permissions, REQUEST_PERMISSION);
    }

    @Override
    public boolean isPermissionGranted() {

        // is below Marshmallow version  (means permission must be placed in Manifest file)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        for (String permission : permissions) {
            if (activity.checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION:
                boolean granted = true;
                for (int i : grantResults) {
                    granted &= i == PackageManager.PERMISSION_GRANTED;
                }
                bus.post(granted ? new PermissionGrantedEvent() : new PermissionDeniedEvent());
                break;
        }
    }

    public static class PermissionGrantedEvent {
        // nothing to do
    }

    public static class PermissionDeniedEvent {
        // nothing to do
    }
}
