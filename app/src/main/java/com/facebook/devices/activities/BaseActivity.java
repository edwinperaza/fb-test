package com.facebook.devices.activities;


import android.animation.Animator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;

import butterknife.BindView;

public class BaseActivity extends Activity {

    private static final String DEVICE_NUMBER = "HWTP_DEVICE_NUMBER";

    @BindView(android.R.id.content) FrameLayout rootLayout;

    /* package */ class DeviceBroadcastReceiver extends BroadcastReceiver {

        @Extra int color = Color.WHITE;
        @Extra int background = Color.BLACK;
        @Extra String device = "";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || !intent.hasExtra("device")) {
                return;
            }

            ExtraKnife.setExtras(this, intent);

            View v = View.inflate(context, R.layout.overlay_layout, null);
            rootLayout.addView(v);

            TextView tv = v.findViewById(R.id.stage_number);
            tv.setText(device);
            tv.setTextColor(color);
            tv.setBackgroundColor(background);

            tv.animate().alpha(0).setStartDelay(300).setDuration(2000).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    rootLayout.removeView(v);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
        }
    }

    public BroadcastReceiver receiver = new DeviceBroadcastReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(DEVICE_NUMBER);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }
}
