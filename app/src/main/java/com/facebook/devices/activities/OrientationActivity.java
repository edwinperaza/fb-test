package com.facebook.devices.activities;



import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.mvp.model.OrientationModel;
import com.facebook.devices.mvp.presenter.OrientationPresenter;
import com.facebook.devices.mvp.view.OrientationView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class OrientationActivity extends TestBaseActivity {

    private OrientationPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.facebook.devices.R.layout.orientation_activity_layout);

        int orientationSate = 0;

        if (savedInstanceState != null) {
            orientationSate = savedInstanceState.getInt(OrientationModel.ORIENTATION_STATE_SAVED);
        } else {
            switch (getResources().getConfiguration().orientation) {
                case Configuration.ORIENTATION_PORTRAIT:
                    orientationSate = OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_ONE;
                    break;

                case Configuration.ORIENTATION_LANDSCAPE:
                    orientationSate = OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT;
                    break;
            }
        }

        presenter = new OrientationPresenter(
                new OrientationModel(orientationSate),
                new OrientationView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        presenter.saveInstanceState(state);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        presenter.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
