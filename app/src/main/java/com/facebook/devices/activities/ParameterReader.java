package com.facebook.devices.activities;

import android.content.Intent;

import java.util.Iterator;
import java.util.Set;

class ParameterReader {

    private static final String ARGS = "args";
    private String[] args;
    private int index;

    public ParameterReader(Intent intent) {
        this.index = 0;
        if (intent != null && intent.hasExtra(ARGS)) {
            args = intent.getStringArrayExtra(ARGS);
            return;
        }

        if (intent != null && intent.getExtras() != null) {
            Set<String> keys = intent.getExtras().keySet();
            int size = keys.size();
            Iterator<String> it = keys.iterator();
            args = new String[size];
            int i = 0;
            while (it.hasNext()) {
                args[i] = intent.getStringExtra(it.next());
                i++;
            }
            return;
        }
    }

    public boolean readBoolean() {
        return readBoolean(false);
    }

    public boolean readBoolean(boolean defaultBoolean) {
        String tmp = readArg();
        return tmp == null ? defaultBoolean : Boolean.parseBoolean(tmp);
    }

    public int readInteger() {
        return readInteger(0);
    }

    public int readInteger(int defaultInt) {
        String tmp = readArg();
        return tmp == null ? defaultInt : Integer.parseInt(tmp);
    }

    public long readLong() {
        return readLong(0);
    }

    public long readLong(long defaultDouble) {
        String tmp = readArg();
        return tmp == null ? defaultDouble : Long.parseLong(tmp);
    }

    public double readDouble() {
        return readDouble(0);
    }

    public double readDouble(double defaultDouble) {
        String tmp = readArg();
        return tmp == null ? defaultDouble : Double.parseDouble(tmp);
    }

    public float readFloat() {
        return readFloat(0F);
    }

    public float readFloat(float defaultFloat) {
        String tmp = readArg();
        return tmp == null ? defaultFloat : Float.parseFloat(tmp);
    }

    public String readString() {
        return readString(null);
    }

    public String readString(String defaultString) {
        String tmp = readArg();
        return tmp == null ? defaultString : tmp;
    }

    public byte readByte() {
        return readByte((byte) 0);
    }

    public byte readByte(byte defaultByte) {
        String tmp = readArg();
        return tmp == null ? defaultByte : Integer.decode(tmp).byteValue();
    }

    public String[] readStringArray(String... defaultStrings) {
        if (args == null) {
            return defaultStrings;
        }

        int size = args.length - index;
        String[] result = new String[size];
        System.arraycopy(args, index, result, 0, size);
        return result;
    }

    private String readArg() {
        if (args == null || index >= args.length) {
            return null;
        }

        return args[index++];
    }
}
