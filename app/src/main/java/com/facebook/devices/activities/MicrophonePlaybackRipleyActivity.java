package com.facebook.devices.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.MicrophonePlaybackRipleyModel;
import com.facebook.devices.mvp.presenter.MicrophonePlaybackRipleyPresenter;
import com.facebook.devices.mvp.view.MicrophonePlaybackRipleyView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioPlayer;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.service.ConnectionService;

public class MicrophonePlaybackRipleyActivity extends TestBaseActivity{

    private MicrophonePlaybackRipleyPresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.facebook.devices.R.layout.microphone_playback_ripley_activity_layout);

        short[] recordedBuffer = new short[0];
        if (savedInstanceState != null && savedInstanceState.getShortArray(MicrophonePlaybackRipleyModel.RECORDED_BUFFER) != null) {
            recordedBuffer = savedInstanceState.getShortArray(MicrophonePlaybackRipleyModel.RECORDED_BUFFER);
        }

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.RECORD_AUDIO);

        presenter = new MicrophonePlaybackRipleyPresenter(
                new MicrophonePlaybackRipleyModel(
                        permissionHandler,
                        recordedBuffer,
                        new AudioRecorder(AudioRecorder.MODE_RECORD),
                        new AudioPlayer(),
                        new VolumeManager(this)),
                new MicrophonePlaybackRipleyView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPaused();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
    }
}
