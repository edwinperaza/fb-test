package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.presenter.TouchPatternPresenter;
import com.facebook.devices.mvp.view.TouchPatternView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.customviews.TouchDetectorView;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class TouchPatternActivity extends TestBaseActivity {

    private TouchPatternPresenter presenter;

    @Extra int defaultOrientation = TouchDetectorView.ORIENTATION_HORIZONTAL;
    @Extra int defaultSize = TouchPatternModel.SIX_SIX;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.touch_pattern_activity_layout);

        ExtraKnife.setExtras(this);

        defaultSize = (defaultSize > TouchPatternModel.TEN_TEN || defaultSize < TouchPatternModel.TWO_TWO) ?
                TouchPatternModel.SIX_SIX : defaultSize;

        presenter = new TouchPatternPresenter(
                new TouchPatternModel(
                        defaultOrientation,
                        defaultSize
                ),
                new TouchPatternView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

}
