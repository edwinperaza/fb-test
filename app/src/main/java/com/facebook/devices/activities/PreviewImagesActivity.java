package com.facebook.devices.activities;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.R;
import com.facebook.devices.mvp.model.PreviewImagesModel;
import com.facebook.devices.mvp.presenter.PreviewImagesPresenter;
import com.facebook.devices.mvp.view.PreviewImagesView;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.RawResource;
import com.facebook.hwtp.service.ConnectionService;

public class PreviewImagesActivity extends TestBaseActivity {

    private PreviewImagesPresenter presenter;
    private PermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preview_images_activity_layout);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.READ_EXTERNAL_STORAGE
        );

        presenter = new PreviewImagesPresenter(
                new PreviewImagesModel(
                        this,
                        permissionHandler,
                        new RawResource(
                                this,
                                new ParameterReader(getIntent()).readStringArray()
                        ).listResources()
                ),
                new PreviewImagesView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        /* Bind to local service */
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        presenter.onStop();
        BusProvider.unregister(presenter);
        /* Unbind service */
        unbindService(presenter);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
