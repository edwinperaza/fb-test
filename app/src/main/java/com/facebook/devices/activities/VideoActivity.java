package com.facebook.devices.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.VideoModel;
import com.facebook.devices.mvp.presenter.VideoPresenter;
import com.facebook.devices.mvp.view.VideoView;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;


public class VideoActivity extends TestBaseActivity {

    private VideoPresenter presenter;
    private PermissionHandler permissionHandler;

    @Extra String file = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.video_activity_layout);

        ExtraKnife.setExtras(this);

        /*Intro value*/
        boolean firstQuestionIsBeingShown = savedInstanceState != null && savedInstanceState.getBoolean(VideoModel.FIRST_QUESTION_IS_BEING_SHOWN);
        /*Intro value*/
        boolean secondQuestionBeingShown = savedInstanceState != null && savedInstanceState.getBoolean(VideoModel.SECOND_QUESTION_IS_BEING_SHOWN);

        permissionHandler = new RuntimePermissionHandler(this, BusProvider.getInstance(), Manifest.permission.READ_EXTERNAL_STORAGE);

        presenter = new VideoPresenter(
                new VideoModel(
                        this,
                        permissionHandler,
                        file,
                        firstQuestionIsBeingShown,
                        secondQuestionBeingShown,
                        new VolumeManager(this)
                ),
                new VideoView(
                        this,
                        BusProvider.getInstance(),
                        new MediaPlayer()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstantState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
