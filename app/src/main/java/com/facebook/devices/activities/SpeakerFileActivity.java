package com.facebook.devices.activities;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.SpeakerFileModel;
import com.facebook.devices.mvp.presenter.SpeakerFilePresenter;
import com.facebook.devices.mvp.view.SpeakerFileView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.RawResource;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.service.ConnectionService;

import java.util.Random;

public class SpeakerFileActivity extends TestBaseActivity {

    private SpeakerFilePresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker_check_file_activity_layout);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE
        );

        presenter = new SpeakerFilePresenter(
                new SpeakerFileModel(
                        this,
                        permissionHandler,
                        new RawResource(
                                this,
                                new ParameterReader(getIntent()).readStringArray()).getRandomResource(new Random()),
                        new VolumeManager(this)
                ),
                new SpeakerFileView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPaused();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
