package com.facebook.devices.activities;

import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.mvp.model.LightSensorModel;
import com.facebook.devices.mvp.presenter.LightSensorPresenter;
import com.facebook.devices.mvp.view.LightSensorView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class LightSensorActivity extends TestBaseActivity {

    @Extra float minLux = 20f;

    @Extra float maxLux = 200f;

    private LightSensorPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.facebook.devices.R.layout.light_sensor_layout);

        ExtraKnife.setExtras(this);

        presenter = new LightSensorPresenter(
                new LightSensorModel(
                        (SensorManager) getSystemService(SENSOR_SERVICE),
                        BusProvider.getInstance(),
                        minLux,
                        maxLux
                ),
                new LightSensorView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
        presenter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
