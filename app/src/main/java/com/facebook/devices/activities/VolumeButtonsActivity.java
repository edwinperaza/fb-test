package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.facebook.devices.mvp.model.VolumeButtonsModel;
import com.facebook.devices.mvp.presenter.VolumeButtonsPresenter;
import com.facebook.devices.mvp.view.VolumeButtonsView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class VolumeButtonsActivity extends TestBaseActivity {

    private VolumeButtonsPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.facebook.devices.R.layout.volume_buttons_activity_layout);

        presenter = new VolumeButtonsPresenter(
                new VolumeButtonsModel(),
                new VolumeButtonsView(this, BusProvider.getInstance())
        );
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return (presenter.onKeyDown(keyCode)) || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Bind local service
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Unbind from service
        unbindService(presenter);
    }


}
