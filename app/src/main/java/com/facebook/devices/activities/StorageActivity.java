package com.facebook.devices.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.NonNull;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.StorageModel;
import com.facebook.devices.mvp.presenter.StoragePresenter;
import com.facebook.devices.mvp.view.StorageView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class StorageActivity extends TestBaseActivity {

    private StoragePresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Extra byte pattern = (byte) 0xff;
    @Extra int storagePercentage = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storage_activity);

        ExtraKnife.setExtras(this);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        presenter = new StoragePresenter(
                new StorageModel(pattern,
                        storagePercentage,
                        permissionHandler,
                        new StatFs(Environment.getExternalStorageDirectory().getPath())
                ),
                new StorageView(this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        presenter.onStop();
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}