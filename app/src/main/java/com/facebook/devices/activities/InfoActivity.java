package com.facebook.devices.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.WindowManager;

import com.facebook.devices.mvp.model.InitialScreenModel;
import com.facebook.devices.mvp.presenter.InitialScreenPresenter;
import com.facebook.devices.mvp.view.InitialScreenView;
import com.facebook.hwtp.service.ConnectionService;

public class InfoActivity extends Activity {

    private static final String DOWNLOAD_RESOURCE_FILTER = "HWTP_DOWNLOAD_RESOURCE";
    private static final String EXTRA_SHOW_DOWNLOADING = "downloading";
    private static final String EXTRA_SHOW_TRANSFERRING = "transferring";

    private InitialScreenPresenter presenter;
    private ConnectionBroadcasterReceiver broadcasterReceiver;
    private DownloadBroadcasterReceiver downloadReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.facebook.devices.R.layout.activity_info);

        /*No sleep*/
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        broadcasterReceiver = new ConnectionBroadcasterReceiver();
        downloadReceiver = new DownloadBroadcasterReceiver();

        String deviceId = "UNKNOWN";
        String sessionId = "UNKNOWN";
        if (getIntent().getExtras() != null) {
            deviceId = getIntent().getExtras().getString("DEVICE_ID");
            sessionId = getIntent().getExtras().getString("SESSION_ID");
        }

        presenter = new InitialScreenPresenter(
                new InitialScreenModel(
                        deviceId,
                        sessionId
                ),
                new InitialScreenView(
                        this
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();

        IntentFilter intentFilter = new IntentFilter(ConnectionService.CONNECTION_SERVICE_FILTER);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcasterReceiver, intentFilter);

        intentFilter = new IntentFilter(DOWNLOAD_RESOURCE_FILTER);
        registerReceiver(downloadReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        unregisterReceiver(downloadReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcasterReceiver);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    private class ConnectionBroadcasterReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                boolean connected = b.getBoolean(ConnectionService.EXTRA_CONNECTION, false);
                presenter.onConnected(connected);
            }
        }
    }

    private class DownloadBroadcasterReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            if (b != null) {
                if (b.containsKey(EXTRA_SHOW_DOWNLOADING)) {
                    boolean show = Boolean.parseBoolean(b.getString(EXTRA_SHOW_DOWNLOADING));
                    presenter.showDownloading(show);
                }

                if (b.containsKey(EXTRA_SHOW_TRANSFERRING)) {
                    boolean show = Boolean.parseBoolean(b.getString(EXTRA_SHOW_TRANSFERRING));
                    presenter.showTransferring(show);
                }
            }
        }
    }
}
