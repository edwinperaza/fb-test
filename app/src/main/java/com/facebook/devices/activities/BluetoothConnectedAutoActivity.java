package com.facebook.devices.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.BluetoothConnectedAutoModel;
import com.facebook.devices.mvp.presenter.BluetoothConnectedAutoPresenter;
import com.facebook.devices.mvp.view.BluetoothConnectedAutoView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.DevConstants;
import com.facebook.devices.utils.RawResource;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

import java.util.Random;

public class BluetoothConnectedAutoActivity extends TestBaseActivity {

    private BluetoothConnectedAutoPresenter presenter;
    private RuntimePermissionHandler permissionHandler;
    @Extra boolean shouldPlayAudio = false;
    @Extra String deviceName = DevConstants.NAME;
    @Extra String deviceAddress = DevConstants.ADDRESS;
    @Extra("file") String[] file = new String[]{"classic"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetoothcheck_activity_layout);

        ExtraKnife.setExtras(this);

        /*Intro value*/
        boolean wasPermissionBluetoothAccepted = savedInstanceState != null && savedInstanceState.getBoolean(BluetoothConnectedAutoModel.PERMISSION_BLUETOOTH_ACCEPTED);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO);

        presenter = new BluetoothConnectedAutoPresenter(
                new BluetoothConnectedAutoModel(this,
                        BusProvider.getInstance(),
                        BluetoothAdapter.getDefaultAdapter(),
                        shouldPlayAudio,
                        deviceName,
                        deviceAddress,
                        wasPermissionBluetoothAccepted,
                        new RawResource(this, file).getRandomResource(new Random()),
                        permissionHandler
                ),
                new BluetoothConnectedAutoView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstantState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }
}