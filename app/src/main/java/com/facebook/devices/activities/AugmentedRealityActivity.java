package com.facebook.devices.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.AugmentedRealityModel;
import com.facebook.devices.mvp.model.AugmentedRealityNoOpenglModel;
import com.facebook.devices.mvp.presenter.AugmentedRealityNoOpenglPresenter;
import com.facebook.devices.mvp.presenter.AugmentedRealityPresenter;
import com.facebook.devices.mvp.presenter.utils.AugmentedRealityPresenterInterface;
import com.facebook.devices.mvp.view.AugmentedRealityNoOpenglView;
import com.facebook.devices.mvp.view.AugmentedRealityView;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.SystemHelper;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class AugmentedRealityActivity extends TestBaseActivity {

    private final float MIN_OPENGL_VERSION = 3.1f;

    private AugmentedRealityPresenterInterface presenter;

    private PermissionHandler permissionHandler;

    @Extra
    public String model;
    @Extra
    public String objective;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ExtraKnife.setExtras(this);

        float openGLVersion = SystemHelper.getOpenGLSystemVersion(this);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.CAMERA
        );

        if (openGLVersion >= MIN_OPENGL_VERSION) {
            setContentView(R.layout.augmented_reality_activity_layout);

            presenter = new AugmentedRealityPresenter(
                    new AugmentedRealityModel(this, permissionHandler, model, objective, new CameraUtils(this)),
                    new AugmentedRealityView(this, BusProvider.getInstance()));
        } else {
            setContentView(R.layout.augmented_reality_activity_no_opengl_layout);

            presenter = new AugmentedRealityNoOpenglPresenter(
                    new AugmentedRealityNoOpenglModel(String.valueOf(openGLVersion)),
                    new AugmentedRealityNoOpenglView(this, BusProvider.getInstance()));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        /*Bind to LocalService*/
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter.getServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter.getServiceConnection());
        super.onStop();
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
