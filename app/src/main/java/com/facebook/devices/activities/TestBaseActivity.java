package com.facebook.devices.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.facebook.devices.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestBaseActivity extends BaseActivity {

    @BindView(R.id.layout_container) ViewGroup mainContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.setContentView(R.layout.test_base_activity_layout);
        ButterKnife.bind(this);

        /*No sleep*/
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    @Override
    public void onBackPressed() {
        /*Back behavior removed*/
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        if (layoutResID != 0) {
            ViewGroup content = (ViewGroup) getLayoutInflater().inflate(layoutResID, mainContainer, false);
            mainContainer.addView(content);
        }
    }
}