package com.facebook.devices.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.PowerButtonModel;
import com.facebook.devices.mvp.presenter.PowerButtonPresenter;
import com.facebook.devices.mvp.view.PowerButtonView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class PowerButtonActivity extends TestBaseActivity {

    private PowerButtonPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.power_button_activity_layout);

        if (Build.VERSION.SDK_INT >= 27) {
            setShowWhenLocked(true);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        presenter = new PowerButtonPresenter(
                new PowerButtonModel(this),
                new PowerButtonView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(presenter);
        presenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


}
