package com.facebook.devices.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.ColorModel;
import com.facebook.devices.mvp.presenter.ColorPresenter;
import com.facebook.devices.mvp.view.ColorView;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class ColorActivity extends TestBaseActivity {

    private ColorPresenter presenter;

    @Extra boolean showMuraQuestion;
    @Extra("color") String[] color = new String[]{"#FFFFFF00"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.colors_check_activity);

        ExtraKnife.setExtras(this);

        boolean showTutorial = TutorialTracker.of(this).shouldShowTutorial(getClass().getCanonicalName()).thenUpdate();

        presenter = new ColorPresenter(
                new ColorModel(
                        showTutorial,
                        showMuraQuestion,
                        color),
                new ColorView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        presenter.onPause();
        BusProvider.unregister(presenter);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
