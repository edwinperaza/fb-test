package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.VolumeButtonsAndSoundModel;
import com.facebook.devices.mvp.presenter.VolumeButtonsAndSoundPresenter;
import com.facebook.devices.mvp.view.VolumeButtonsAndSoundView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class VolumeButtonsAndSoundActivity extends TestBaseActivity {

    private VolumeButtonsAndSoundPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.volume_buttons_and_sound_check_activity_layout);

        presenter = new VolumeButtonsAndSoundPresenter(
                new VolumeButtonsAndSoundModel(this),
                new VolumeButtonsAndSoundView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return (presenter.onKeyDown(keyCode)) || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
