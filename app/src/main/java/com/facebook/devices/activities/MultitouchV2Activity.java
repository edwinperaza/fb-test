package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.MultitouchV2Model;
import com.facebook.devices.mvp.presenter.MultitouchV2Presenter;
import com.facebook.devices.mvp.view.MultitouchV2View;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class MultitouchV2Activity extends TestBaseActivity {

    private MultitouchV2Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.multitouch_v2_check_activity_layout);

        presenter = new MultitouchV2Presenter(
                new MultitouchV2Model(),
                new MultitouchV2View(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(presenter);
    }
}
