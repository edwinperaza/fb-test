package com.facebook.devices.activities;


import android.os.Bundle;

import com.facebook.devices.R;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.mvp.model.PreferencesModel;
import com.facebook.devices.mvp.presenter.PreferencesPresenter;
import com.facebook.devices.mvp.view.PreferencesView;
import com.facebook.devices.bus.BusProvider;

public class PreferencesActivity extends ToolBarActivity {

    private PreferencesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        presenter = new PreferencesPresenter(
                new PreferencesModel(TestDatabase.getDatabase(this)),
                new PreferencesView(this, BusProvider.getInstance()));
        setActionIconRightVisible(false);
        setActionIconVisible(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

}
