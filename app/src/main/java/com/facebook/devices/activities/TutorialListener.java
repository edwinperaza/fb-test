package com.facebook.devices.activities;

public interface TutorialListener {
    void tutorialButtonPressed();
}
