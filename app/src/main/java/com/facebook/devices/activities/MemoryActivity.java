package com.facebook.devices.activities;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.MemoryModel;
import com.facebook.devices.mvp.presenter.MemoryPresenter;
import com.facebook.devices.mvp.view.MemoryView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class MemoryActivity extends TestBaseActivity implements ComponentCallbacks2 {

    private MemoryPresenter presenter;

    @Extra byte pattern = (byte) 0xFF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memory_layout);

        ExtraKnife.setExtras(this);

        presenter = new MemoryPresenter(
                new MemoryModel(pattern,
                        this),
                new MemoryView(this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        presenter.onStop();
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }
}
