package com.facebook.devices.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.CameraCaptureModel;
import com.facebook.devices.mvp.presenter.CameraCapturePresenter;
import com.facebook.devices.mvp.view.CameraCaptureView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.devices.utils.customcomponents.CancellableHandler;
import com.facebook.hwtp.service.ConnectionService;

public class CameraCaptureActivity extends TestBaseActivity {

    private CameraCapturePresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_capture_activity_layout);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        String filePath = "";
        if (savedInstanceState != null) {
            filePath = savedInstanceState.getString(CameraCaptureModel.IMAGE_FILE_PATH, "");
        }

        /*Screen size*/
        Point screenSize = new Point();
        getWindowManager().getDefaultDisplay().getSize(screenSize);

        presenter = new CameraCapturePresenter(
                new CameraCaptureModel(
                        (CameraManager) getSystemService(CAMERA_SERVICE),
                        screenSize,
                        BusProvider.getInstance(),
                        permissionHandler,
                        new CameraUtils(this),
                        new CancellableHandler(Looper.getMainLooper()),
                        filePath,
                        true
                ),
                new CameraCaptureView(this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        presenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onViewResumed();
    }

    @Override
    protected void onPause() {
        presenter.onViewPaused();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        Intent intent = new Intent(this, ConnectionService.class);
        /*Bind to LocalService*/
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        presenter.onStop(isChangingConfigurations());
        /*Unbind from the service*/
        unbindService(presenter);
        BusProvider.unregister(presenter);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
