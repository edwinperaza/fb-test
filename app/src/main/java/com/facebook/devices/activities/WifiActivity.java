package com.facebook.devices.activities;


import android.Manifest;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.WifiModel;
import com.facebook.devices.mvp.presenter.WifiPresenter;
import com.facebook.devices.mvp.view.WifiView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.models.WifiDataContainer;
import com.facebook.devices.utils.models.WifiDataContainerBuilder;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class WifiActivity extends TestBaseActivity {

    private WifiPresenter presenter;

    @Extra boolean keepWifiConnected = false;
    @Extra("ssid") String[] ssids;
    @Extra("password") String[] passwords;
    @Extra String baseUrl = "";
    @Extra int fileSize = 0;
    @Extra boolean measureTransferRate = false;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi_check_activity_layout);

        ExtraKnife.setExtras(this);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION);

        WifiDataContainer[] wifiData = new WifiDataContainer[]{};
        try {
            wifiData = WifiDataContainerBuilder.buildWifiData(ssids, passwords);
        } catch (WifiDataContainerBuilder.DataMissingException e) {
            Log.e("DataMissing", e.getMessage());
        }

        presenter = new WifiPresenter(
                new WifiModel(
                        this,
                        keepWifiConnected,
                        measureTransferRate,
                        (NetworkStatsManager) getSystemService(Context.NETWORK_STATS_SERVICE),
                        baseUrl,
                        fileSize,
                        permissionHandler,
                        wifiData),
                new WifiView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }
}
