package com.facebook.devices.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.aloha.bluetoothremote.IBluetoothRemotePairingService;
import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.RemoteControlModel;
import com.facebook.devices.mvp.presenter.RemoteControlPresenter;
import com.facebook.devices.mvp.view.RemoteControlView;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class RemoteControlActivity extends TestBaseActivity {

    @Extra long timeToFail = 30000;
    @Extra long timeToRetryPairing = 500;

    private RemoteControlPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remote_control_activity_layout);

        ExtraKnife.setExtras(this);

        presenter = new RemoteControlPresenter(
                new RemoteControlModel(timeToFail, timeToRetryPairing),
                new RemoteControlView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);

        Intent intentRemote = new Intent();
        intentRemote.setClassName("com.facebook.aloha.bluetoothremote", "com.facebook.aloha.bluetoothremote.BluetoothRemotePairingService");
        intentRemote.setAction(IBluetoothRemotePairingService.class.getName());
        bindService(intentRemote, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        presenter.onPause();
        BusProvider.unregister(presenter);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
