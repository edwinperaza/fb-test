package com.facebook.devices.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.CameraPrivacyModel;
import com.facebook.devices.mvp.presenter.CameraPrivacyPresenter;
import com.facebook.devices.mvp.view.CameraPrivacyView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.devices.utils.customcomponents.CancellableHandler;
import com.facebook.hwtp.service.ConnectionService;

public class CameraPrivacyActivity extends TestBaseActivity {

    private CameraPrivacyPresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_privacy_activity_layout);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        presenter = new CameraPrivacyPresenter(
                new CameraPrivacyModel((CameraManager) getSystemService(CAMERA_SERVICE),
                        BusProvider.getInstance(),
                        permissionHandler,
                        new CancellableHandler(Looper.getMainLooper()),
                        new CameraUtils(this), true),
                new CameraPrivacyView(this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        presenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onViewResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onViewPaused();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        /*Bind to LocalService*/
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        presenter.onStop(isChangingConfigurations());
        BusProvider.unregister(presenter);
        /*Unbind from the service*/
        unbindService(presenter);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
