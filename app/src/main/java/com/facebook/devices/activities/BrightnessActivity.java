package com.facebook.devices.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.BrightnessModel;
import com.facebook.devices.mvp.presenter.BrightnessPresenter;
import com.facebook.devices.mvp.view.BrightnessView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

public class BrightnessActivity extends TestBaseActivity {

    private BrightnessPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.brightness_activity_layout);

        presenter = new BrightnessPresenter(
                new BrightnessModel(),
                new BrightnessView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
