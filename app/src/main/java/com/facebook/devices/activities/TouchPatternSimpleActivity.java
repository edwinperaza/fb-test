package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.devices.mvp.presenter.GridTouchPatternPresenter;
import com.facebook.devices.mvp.view.GridTouchPatternView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class TouchPatternSimpleActivity extends TestBaseActivity {

    private GridTouchPatternPresenter presenter;

    @Extra int defaultSize = TouchPatternModel.SIX_SIX;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.touch_pattern_simple_activity_layout);

        ExtraKnife.setExtras(this);

        defaultSize = (defaultSize > TouchPatternModel.TEN_TEN || defaultSize < TouchPatternModel.TWO_TWO) ?
                TouchPatternModel.SIX_SIX : defaultSize;


        presenter = new GridTouchPatternPresenter(
                new GridTouchPatternModel(defaultSize, new Handler(Looper.getMainLooper())),
                new GridTouchPatternView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intentService = new Intent(this, ConnectionService.class);
        bindService(intentService, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
        unbindService(presenter);
    }
}
