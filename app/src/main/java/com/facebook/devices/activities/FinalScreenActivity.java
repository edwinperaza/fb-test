package com.facebook.devices.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.FinalScreenModel;
import com.facebook.devices.mvp.presenter.FinalScreenPresenter;
import com.facebook.devices.mvp.view.FinalScreenView;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class FinalScreenActivity extends Activity {

    @Extra int status = FinalScreenModel.MODE_FAILURE;

    @Extra long duration = FinalScreenModel.DEFAULT_MILLIS_TO_CLOSE;

    private FinalScreenPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.final_screen_layout);

        ExtraKnife.setExtras(this);

        presenter = new FinalScreenPresenter(
                new FinalScreenModel(status, duration),
                new FinalScreenView(this, BusProvider.getInstance()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        BusProvider.unregister(presenter);
        super.onPause();
    }
}
