package com.facebook.devices.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.MultitouchModel;
import com.facebook.devices.mvp.presenter.MultitouchPresenter;
import com.facebook.devices.mvp.view.MultitouchView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class MultitouchActivity extends TestBaseActivity {

    private MultitouchPresenter presenter;

    @Extra int squareSizePercentage = 20;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.multitouch_check_activity_layout);

        ExtraKnife.setExtras(this);

        presenter = new MultitouchPresenter(
                new MultitouchModel(),
                new MultitouchView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
