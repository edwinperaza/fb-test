package com.facebook.devices.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.R;
import com.facebook.devices.utils.customviews.CustomToolbar;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ToolBarActivity extends Activity {

    private static final int PHONE_STATE_PERMISSION_REQUEST_CODE = 0;

    @BindView(R.id.toolbar) CustomToolbar toolbar;
    @BindView(R.id.toolbar_big_container) LinearLayout toolbarBigContainer;
    @BindView(R.id.toolbar_layout_external_container) LinearLayout layoutContainer;

    private boolean deviceInfoPressed = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.toolbar_normal_activity_layout);
        ButterKnife.bind(this);

        initToolbar();
    }

    private void initToolbar() {
        /*Menu setup*/
        toolbar.setOnBackIconPressed(v -> finish());
        toolbar.setActionIcon(R.drawable.ic_info);
        toolbar.setActionIconVisible(true);
        setActionIconListener(v -> executeDeviceInfoDialog());
        if (BuildConfig.BUILD_TYPE.equals("engineering")) {
            setActionIconRightVisible(true);
            setActionIconRightListener(v -> showPreferencesActivity());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (deviceInfoPressed &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            showDeviceInfoDialog();
            deviceInfoPressed = false;
        }
    }

    @Override
    public void onBackPressed() {
        /*Back behavior removed*/
    }

    @Override
    public void setContentView(int layoutResID) {
        ViewGroup mainContainer = findViewById(R.id.layout_container);
        if (layoutResID != 0) {
            ViewGroup content = (ViewGroup) getLayoutInflater().inflate(layoutResID, mainContainer, false);
            mainContainer.addView(content);
        }
    }

    public void setExternalToolbarLayout(int layoutResourceId) {
        View toolbarExtraView = LayoutInflater.from(this).inflate(layoutResourceId, null);
        toolbarExtraView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        layoutContainer.addView(toolbarExtraView);
    }

    private void executeDeviceInfoDialog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                /* Show device dialog */
                showDeviceInfoDialog();
            } else {
                deviceInfoPressed = true;
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PHONE_STATE_PERMISSION_REQUEST_CODE);
            }
        } else {
            showDeviceInfoDialog();
        }
    }

    @SuppressLint({"MissingPermission", "NewApi"})
    private void showDeviceInfoDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.device_info_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.MaterialBaseDialogStyle)
                .setCancelable(false)
                .setView(dialogView)
                .setPositiveButton(R.string.ok_base, ((dialog, which) -> dialog.dismiss()));

        setText(dialogView, R.id.tv_build_number, BuildConfig.VERSION_NAME);
        setText(dialogView, R.id.tv_os_build_number, Build.FINGERPRINT);
        setText(dialogView, R.id.tv_wifi_mac, getWiFiMacAddress());
        setText(dialogView, R.id.tv_serial_number, (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ? Build.getSerial() : Build.SERIAL);
        setText(dialogView, R.id.tv_bluetooth_name, (BluetoothAdapter.getDefaultAdapter() != null) ? BluetoothAdapter.getDefaultAdapter().getName() : "");

        alertDialogBuilder.create().show();
    }

    private void setText(View view, @IdRes int id, String str) {
        ((TextView) view.findViewById(id)).setText(str);
    }

    public String getWiFiMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress();
    }

    private void showPreferencesActivity() {
        Intent singleIntent = new Intent(this, PreferencesActivity.class);
        startActivity(singleIntent);
    }


    public CustomToolbar getToolbar() {
        return toolbar;
    }

    public void enableTitle() {
        toolbar.setTitleVisible(true);
    }

    public void disableTitle() {
        toolbar.setTitleVisible(false);
    }

    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    public void setToolbarTitleSize(float size) {
        toolbar.setTitleSize(size);
    }

    public String getToolbarTitle() {
        return toolbar.getTitle();
    }

    public float getToolbarTitleSize() {
        return toolbar.getTitleSize();
    }

    public void setToolbarBackIconVisible(boolean isVisible) {
        toolbar.setBackIconVisible(isVisible);
    }

    public void setToolbarBackIconGone() {
        toolbar.setBackButtonGone();
    }

    public void setToolbarBackIconResource(int iconResource) {
        toolbar.setBackIconResource(iconResource);
    }

    public void setToolbarOnBackIconPressed(View.OnClickListener listener) {
        toolbar.setOnBackIconPressed(listener);
    }

    public void setToolbarElevation(float elevation) {
        toolbar.setElevation(elevation);
    }

    public void setToolbarIconTintColor(int color) {
        toolbar.setGenericTintIconColor(color);
    }

    public void setToolbarDotsIconVisible(boolean isVisible) {
        toolbar.setActionIconVisible(isVisible);
    }

    public void setToolbarBackground(int color) {
        toolbarBigContainer.setBackgroundColor(color);
    }

    public void setToolbarBackground(Drawable drawable) {
        toolbarBigContainer.setBackground(drawable);
    }

    public void setTitleTextColor(int color) {
        toolbar.setTitleColor(color);
    }

    public void setTitleMarginStart(int dimenResource) {
        toolbar.setTitleMarginStart((int) getResources().getDimension(dimenResource));
    }

    public void setActionIconListener(View.OnClickListener listener) {
        toolbar.setActionIconListener(listener);
    }

    public void setActionIconVisible(boolean isVisible) {
        toolbar.setActionIconVisible(isVisible);
    }

    public void setActionIconRightListener(View.OnClickListener listener) {
        toolbar.setActionIconRightListener(listener);
    }

    public void setActionIconRightVisible(boolean isVisible) {
        toolbar.setActionIconRightVisible(isVisible);
    }
}