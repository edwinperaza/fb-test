package com.facebook.devices.activities;


import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.OrientationRipleyModel;
import com.facebook.devices.mvp.presenter.OrientationRipleyPresenter;
import com.facebook.devices.mvp.view.OrientationRipleyView;
import com.facebook.hwtp.service.ConnectionService;

public class OrientationRipleyActivity extends TestBaseActivity {

    private OrientationRipleyPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orientation_ripley_activity_layout);

        presenter = new OrientationRipleyPresenter(
                new OrientationRipleyModel(
                        (SensorManager) getSystemService(SENSOR_SERVICE),
                        BusProvider.getInstance()),
                new OrientationRipleyView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
        presenter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
