package com.facebook.devices.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.DecibelModel;
import com.facebook.devices.mvp.presenter.DecibelPresenter;
import com.facebook.devices.mvp.view.DecibelView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;
import com.facebook.hwtp.extra.Extra;

public class DecibelActivity extends TestBaseActivity {

    private DecibelPresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Extra double goalDb = 20;
    @Extra double referenceAmp = 12;
    @Extra int waitingMeasurementTime = 5000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.decibel_layout);

        ExtraKnife.setExtras(this);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.RECORD_AUDIO);

        presenter = new DecibelPresenter(
                new DecibelModel(BusProvider.getInstance(),
                        permissionHandler,
                        referenceAmp,
                        goalDb,
                        waitingMeasurementTime,
                        new AudioRecorder(AudioRecorder.MODE_DETECTOR)),
                new DecibelView(
                        this,
                        BusProvider.getInstance())
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

}