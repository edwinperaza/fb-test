package com.facebook.devices.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.PatternDisplayModel;
import com.facebook.devices.mvp.presenter.PatternDisplayPresenter;
import com.facebook.devices.mvp.view.PatternDisplayView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class PatternDisplayActivity extends TestBaseActivity {

    private PatternDisplayPresenter presenter;

    @Extra int patternSize = BuildConfig.DEFAULT_PATTERN_SIZE;
    @Extra boolean showMuraQuestion = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.facebook.devices.R.layout.pattern_display_activity_layout);

        ExtraKnife.setExtras(this);

        /*Setting color activity as canonical name for now to avoid multiple tutorial showed up*/
        boolean showTutorial = TutorialTracker.of(this).shouldShowTutorial(ColorActivity.class.getCanonicalName()).thenUpdate();

        presenter = new PatternDisplayPresenter(
                new PatternDisplayModel(showTutorial, patternSize, showMuraQuestion),
                new PatternDisplayView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
        presenter.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}