package com.facebook.devices.activities;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel;
import com.facebook.devices.mvp.model.BluetoothSpeakerRipleyModel;
import com.facebook.devices.mvp.presenter.BluetoothSpeakerPresenter;
import com.facebook.devices.mvp.presenter.BluetoothSpeakerRipleyPresenter;
import com.facebook.devices.mvp.view.BluetoothSpeakerRipleyView;
import com.facebook.devices.mvp.view.BluetoothSpeakerView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.service.ConnectionService;

public class BluetoothSpeakerRipleyActivity extends TestBaseActivity {

    private BluetoothSpeakerRipleyPresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bluetooth_activity_layout);

        /*Intro value*/
        boolean wasPermissionBluetoothAccepted = savedInstanceState != null && savedInstanceState.getBoolean(BluetoothSpeakerModel.PERMISSION_BLUETOOTH_ACCEPTED);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO);

        presenter = new BluetoothSpeakerRipleyPresenter(
                new BluetoothSpeakerRipleyModel(this,
                        BusProvider.getInstance(),
                        BluetoothAdapter.getDefaultAdapter(),
                        wasPermissionBluetoothAccepted,
                        permissionHandler
                ),
                new BluetoothSpeakerRipleyView(this, BusProvider.getInstance())
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstantState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }
}
