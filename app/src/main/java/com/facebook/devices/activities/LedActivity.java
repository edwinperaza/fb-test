package com.facebook.devices.activities;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.mvp.model.LedModel;
import com.facebook.devices.mvp.presenter.LedPresenter;
import com.facebook.devices.mvp.view.LedView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

import static com.facebook.devices.mvp.model.LedModel.LED_CAMERA;

public class LedActivity extends TestBaseActivity {

    private LedPresenter presenter;

    @Extra String action = LED_CAMERA;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.facebook.devices.R.layout.led_check_activity_layout);

        ExtraKnife.setExtras(this);

        presenter = new LedPresenter(
                new LedModel(
                        this,
                        action
                ),
                new LedView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
