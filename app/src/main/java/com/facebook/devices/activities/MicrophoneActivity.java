package com.facebook.devices.activities;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.MicrophoneModel;
import com.facebook.devices.mvp.presenter.MicrophonePresenter;
import com.facebook.devices.mvp.view.MicrophoneView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.hwtp.service.ConnectionService;

public class MicrophoneActivity extends TestBaseActivity {

    private MicrophonePresenter presenter;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.microphone_activity_layout);

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.RECORD_AUDIO);

        presenter = new MicrophonePresenter(
                new MicrophoneModel(permissionHandler, new AudioRecorder(AudioRecorder.MODE_DETECTOR)),
                new MicrophoneView(this, BusProvider.getInstance()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPaused();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        // Unbind from the service
        unbindService(presenter);
        super.onStop();
    }
}
