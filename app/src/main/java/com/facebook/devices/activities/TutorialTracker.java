package com.facebook.devices.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class TutorialTracker {
    private static String KEY = "previous";
    private static String PREFERENCE_FILE = "tracker_preferences";

    @SuppressLint("ApplySharedPref")
    public static TutorialState of(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        return new TutorialState() {
            @Override
            public TutorialUpdate shouldShowTutorial(@NonNull String testCanonicalName) {
                return buildTutorialState(sp, testCanonicalName);
            }

            @Override
            public void reset() {
                sp.edit().remove(KEY).commit();
            }
        };
    }

    public interface TutorialState {
        TutorialUpdate shouldShowTutorial(@NonNull String testName);

        void reset();
    }

    public interface TutorialUpdate {
        boolean thenUpdate();

        boolean value();
    }

    @SuppressLint("ApplySharedPref")
    private static TutorialUpdate buildTutorialState(SharedPreferences sp, String testCanonicalName) {
        boolean showTutorial = !(sp.contains(KEY) && testCanonicalName.equalsIgnoreCase(sp.getString(KEY, null)));
        return new TutorialUpdate() {
            @Override
            public boolean thenUpdate() {
                sp.edit().putString(KEY, testCanonicalName).commit();
                return showTutorial;
            }

            @Override
            public boolean value() {
                return showTutorial;
            }
        };
    }
}
