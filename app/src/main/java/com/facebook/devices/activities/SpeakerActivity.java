package com.facebook.devices.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.SpeakerModel;
import com.facebook.devices.mvp.model.SpeakerModel.ChannelOutput;
import com.facebook.devices.mvp.presenter.SpeakerPresenter;
import com.facebook.devices.mvp.view.SpeakerView;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.extra.Extra;
import com.facebook.hwtp.extraknife.ExtraKnife;
import com.facebook.hwtp.service.ConnectionService;

public class SpeakerActivity extends TestBaseActivity {

    @Extra int frequency = 440; // Hertz
    @ChannelOutput @Extra String output = SpeakerModel.STEREO;

    private SpeakerPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker_check_activity_layout);

        ExtraKnife.setExtras(this);

        presenter = new SpeakerPresenter(
                new SpeakerModel(frequency, output, new VolumeManager(this)),
                new SpeakerView(
                        this,
                        BusProvider.getInstance()
                )
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        presenter.onPaused();
        BusProvider.unregister(presenter);
        super.onPause();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, presenter, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        unbindService(presenter);
    }
}
