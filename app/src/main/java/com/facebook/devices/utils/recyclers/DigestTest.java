package com.facebook.devices.utils.recyclers;

import com.facebook.devices.db.entity.TestInfoEntity;

public class DigestTest {
    private String iconResource;
    private TestInfoEntity test;

    public DigestTest(String iconResource, TestInfoEntity test){
        this.iconResource = iconResource;
        this.test = test;
    }

    public String getIconResource() {
        return iconResource;
    }

    public TestInfoEntity getTest() {
        return test;
    }
}
