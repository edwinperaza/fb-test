package com.facebook.devices.utils.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

public class RemoteControlCustomView extends View {

    private ControllerListener listener;

    public RemoteControlCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public interface ControllerListener {
        void getMessage(String message);
    }

    public void setListener(ControllerListener listener) {
        this.listener = listener;
    }

    //TODO Buttons: Home, Volume Up, Volume Down, Mute, Block work on Linux
    //TODO Crossbar is not working yet
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        Log.d("RemoteControl", "event.getSource(): " + event.getSource());
//        if ((event.getSource() & InputDevice.SOURCE_GAMEPAD) == InputDevice.SOURCE_GAMEPAD) {
        if (event.getRepeatCount() == 0) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP: //19
                    listener.getMessage("KEYCODE_DPAD_UP");
                    Log.d("RemoteControl", "KEYCODE_DPAD_UP: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_DPAD_DOWN: //20
                    listener.getMessage("KEYCODE_DPAD_DOWN");
                    Log.d("RemoteControl", "KEYCODE_DPAD_DOWN: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT: //21
                    listener.getMessage("KEYCODE_DPAD_LEFT");
                    Log.d("RemoteControl", "KEYCODE_DPAD_LEFT: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT: //22
                    listener.getMessage("KEYCODE_DPAD_RIGHT");
                    Log.d("RemoteControl", "KEYCODE_DPAD_RIGHT: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_DPAD_CENTER: //23
                    listener.getMessage("KEYCODE_DPAD_CENTER");
                    Log.d("RemoteControl", "KEYCODE_DPAD_CENTER: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_MENU: //82 Profile
                    listener.getMessage("KEYCODE_MENU");
                    Log.d("RemoteControl", "KEYCODE_MENU: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE: //85 Play and Pause
                    listener.getMessage("KEYCODE_MEDIA_PLAY_PAUSE");
                    Log.d("RemoteControl", "KEYCODE_MEDIA_PLAY_PAUSE: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_SEARCH: //84 Mic
                    listener.getMessage("KEYCODE_SEARCH");
                    Log.d("RemoteControl", "KEYCODE_SEARCH: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                case KeyEvent.KEYCODE_ESCAPE: //111 Back Button
                    listener.getMessage("KEYCODE_ESCAPE");
                    Log.d("RemoteControl", "KEYCODE_ESCAPE: " + String.valueOf(keyCode));
                    handled = true;
                    break;
                default:
                    listener.getMessage(String.valueOf(keyCode));
                    Log.d("RemoteControl", String.valueOf(keyCode));
                    handled = true;
                    break;
            }
        }
        if (handled) {
            return true;
        }
//        }
        return super.onKeyDown(keyCode, event);
    }
}
