package com.facebook.devices.utils;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import java.util.Arrays;

public class AudioRecorder {

    public static final int MODE_DETECTOR = 0;
    public static final int MODE_RECORD = 1;

    @IntDef({MODE_DETECTOR, MODE_RECORD})
    @interface Mode {
    }

    private @Mode int mode;

    @Mode
    public int getModeSet() {
        return mode;
    }

    private InnerRecorderThread innerRecorderThread;
    private int sampleRate;
    private int bufferSize;

    public AudioRecorder(@Mode int mode) {
        this.mode = mode;
    }

    public AudioRecorder(@Mode int mode, int sampleRate) {
        this.mode = mode;
        this.sampleRate = sampleRate;
        this.bufferSize = loadBufferSize(sampleRate);
    }

    public void setMode(@Mode int mode) {
        if (innerRecorderThread == null || !innerRecorderThread.isRunning()) {
            this.mode = mode;
        }
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
        this.bufferSize = loadBufferSize(sampleRate);
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    private int loadBufferSize(int sampleRate) {
        int bufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = sampleRate * 2;
        }
        return bufferSize;
    }

    public void stopRecording() {
        if (innerRecorderThread != null && innerRecorderThread.isRunning()) {
            innerRecorderThread.stopRecording();
        }
    }

    public void startRecording(@NonNull AudioRecorderListener listener) {
        /*Check sample rate and running state*/
        if (sampleRate == 0 ||
                (innerRecorderThread != null && innerRecorderThread.isRunning())) {
            return;
        }

        innerRecorderThread = new InnerRecorderThread(sampleRate, bufferSize, listener);
        innerRecorderThread.start();
    }

    public boolean isRunning() {
        return (innerRecorderThread != null && innerRecorderThread.isRunning());
    }

    private class InnerRecorderThread extends Thread {

        private AudioRecorderListener listener;
        private int sampleRate;
        private int bufferSize;
        private short[] recordBuffer = new short[0];
        private boolean isRunning = false;

        public InnerRecorderThread(int sampleRate, int bufferSize, AudioRecorderListener listener) {
            this.sampleRate = sampleRate;
            this.bufferSize = bufferSize;
            this.listener = listener;
        }

        public boolean isRunning() {
            return isRunning;
        }

        public void stopRecording() {
            isRunning = false;
        }

        @Override
        public void run() {
            /*Avoid execution if not configured*/
            if (sampleRate == 0 || listener == null) {
                return;
            }

            isRunning = true;

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

            AudioRecord audioRecord = new AudioRecord(
                    MediaRecorder.AudioSource.DEFAULT,
                    sampleRate,
                    AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSize);

            short[] audioBuffer = new short[bufferSize / 2];

            if (audioRecord.getState() == AudioRecord.STATE_INITIALIZED) {

                audioRecord.startRecording();
                while (isRunning) {

                    int readSize = audioRecord.read(audioBuffer, 0, audioBuffer.length);

                    //Calculate the amplitude value from sample data
                    if (readSize > 0) {
                        float maxValue = 0;
                        float minValue = 0;

                        for (int i = 0; i < readSize; i++) {
                            if (audioBuffer[i] > maxValue) {
                                maxValue = audioBuffer[i];
                                continue;
                            }

                            if (audioBuffer[i] < minValue) {
                                minValue = audioBuffer[i];
                            }
                        }
                        float amp = maxValue + Math.abs(minValue);

                        listener.onAmplitudeUpdated(amp);
                    }

                    if (mode == MODE_RECORD) {
                        int size = recordBuffer.length;

                        recordBuffer = Arrays.copyOf(recordBuffer, recordBuffer.length + audioBuffer.length);

                        System.arraycopy(audioBuffer, 0, recordBuffer, size, audioBuffer.length);
                    }

                    listener.onAvailableData(audioBuffer);
                }

                audioRecord.stop();
            }

            /*Release resource*/
            if (audioRecord != null) {
                audioRecord.release();
            }
            /*Record completed listener*/
            if (mode == MODE_RECORD) {
                listener.onRecordCompleted(recordBuffer);
                recordBuffer = new short[0];
            }

            isRunning = false;
        }
    }

    public abstract static class AudioRecorderListener {

        /**
         * Real time data
         */
        public void onAvailableData(short[] data){}

        /**
         * Real time amplitude
         */
        public void onAmplitudeUpdated(float amplitude){}

        /**
         * Only will return value for {@link #MODE_RECORD} mode
         */
        public void onRecordCompleted(short[] data){}
    }
}
