package com.facebook.devices.utils.models;


public class WifiDataContainer {
    private String ssID;
    private String password;
    private int level = -255; /*Min dbm signal based on Manufacturers*/

    public WifiDataContainer(String ssID, String password) {
        this.ssID = ssID;
        this.password = password;
    }

    public String getSsID() {
        return ssID;
    }

    public String getPassword() {
        return password;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
