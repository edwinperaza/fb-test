package com.facebook.devices.utils.models;


import android.support.annotation.Size;

import java.util.ArrayList;
import java.util.List;

public class WifiDataContainerBuilder {

    public static WifiDataContainer[] buildWifiData(@Size(min = 1) String[] wifiSsIds,
                                                    @Size(min = 1) String[] wifiPasswords) throws DataMissingException {

        if (wifiSsIds == null || wifiPasswords == null) {
            throw new DataMissingException();
        }

        if (wifiSsIds.length != wifiPasswords.length) {
            throw new DataMissingException(wifiSsIds.length, wifiPasswords.length);
        }

        List<WifiDataContainer> containers = new ArrayList<>();
        for (int i = 0; i < wifiSsIds.length; i++) {
            containers.add(new WifiDataContainer(wifiSsIds[i], wifiPasswords[i]));
        }
        return containers.toArray(new WifiDataContainer[containers.size()]);
    }

    public static class DataMissingException extends Exception {

        public DataMissingException() {
            super("Wifi data is missing, null arguments.");
        }

        public DataMissingException(int ssidSize, int passSize) {
            super("Wifi data is missing, SSID count was [" + ssidSize + "] and passwords count was [" + passSize + "]. Size of both should be equal.");
        }
    }
}
