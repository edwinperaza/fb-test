package com.facebook.devices.utils.augmentedreality.rendering;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.facebook.devices.R;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.concurrent.CompletableFuture;

public class AugmentedImageNode extends AnchorNode {

    private AugmentedImage image;

    private CompletableFuture<ModelRenderable> modelFuture;

    public AugmentedImageNode(Context context, String fileName) {
        if (modelFuture == null) {
            modelFuture = ModelRenderable.builder().setRegistryId("modelOne").setSource(context, Uri.parse(fileName)).build();
        }
    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Scene form node tree is
     * created based on an Anchor created from the image.
     */
    public void setImage(AugmentedImage image) {
        this.image = image;

        if (!modelFuture.isDone()) {
            CompletableFuture.allOf(modelFuture).thenAccept(aVoid -> setImage(image)).exceptionally(throwable -> {
                Log.e("AugmentedImageNode", "Error loading " + throwable.getMessage());
                return null;
            });
        }

        /*Setting anchor based on center of the image*/
        setAnchor(image.createAnchor(image.getCenterPose()));

        /*Base position*/
        Node base = new Node();
        base.setParent(this);
        base.setLocalPosition(new Vector3(0.0f, 0.08f, 0.02f));

        /*Rotation node*/
        RotatingNode rotatingNode = new RotatingNode();
        rotatingNode.setParent(base);

        /*Adding model node*/
        Node modelNode = new Node();
        modelNode.setParent(rotatingNode);
        modelNode.setLocalPosition(new Vector3(0.0f, 0.0f, 0.0f));
        modelNode.setLocalScale(new Vector3(0.35f, 0.35f, 0.35f));
        modelNode.setLocalRotation(Quaternion.axisAngle(new Vector3(1.0f, 0.0f, 0.0f), -90));
        modelNode.setRenderable(modelFuture.getNow(null));
    }

    @Override
    public void onActivate() {
        super.onActivate();
    }

    public AugmentedImage getImage() {
        return image;
    }
}
