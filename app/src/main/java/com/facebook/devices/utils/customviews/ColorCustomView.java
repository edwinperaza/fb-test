package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.devices.R;

import java.util.ArrayList;
import java.util.List;

public class ColorCustomView extends View {

    /*Data*/
    private int colorsNumber;
    private int baseWidth;
    private int baseHeight;
    private int[] colors = new int[]{Color.WHITE, Color.YELLOW, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.RED, Color.BLUE, Color.BLACK};
    private List<Square> squareList = new ArrayList<>();

    private class Square {

        /*Coordinates*/
        private float left, top, right, bottom;
        private int color;

        public Square(float left, float top, float right, float bottom, int color) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.color = color;
        }

        public float getLeft() {
            return left;
        }

        public float getTop() {
            return top;
        }

        public float getRight() {
            return right;
        }

        public float getBottom() {
            return bottom;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public int getColor() {
            return color;
        }
    }

    /*Paint*/
    private Paint squareGridPaint;

    public ColorCustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ColorCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    /*Use it if needed*/
    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PatternView, defStyle, 0);

        colorsNumber = typedArray.getInt(R.styleable.ColorCustomView_rectangleNumber, 8);
        typedArray.recycle();

        /*ItemRender paint*/
        squareGridPaint = new Paint();
        squareGridPaint.setStyle(Paint.Style.FILL);
        squareGridPaint.setAntiAlias(true);
    }

    public void drawColors(int... colors) {
        if (colors != null && colors.length > 0) {
            this.colors = colors;
            this.colorsNumber = colors.length;
            this.invalidate();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        /*Prepare grid squares*/
        setupGrid(colorsNumber);
    }

    private void setupGrid(int colorsNumber) {

        int viewWidth = baseWidth;
        int viewHeight = baseHeight;

        squareList.clear();

        double squareHeight = (double) viewHeight / colorsNumber;

        float lastTop = 0;
        float lastBottom = (float) Math.ceil(squareHeight);
        for (int row = 0; row < colorsNumber; row++) {

            /* Save square data */
            squareList.add(new Square(0, lastTop, viewWidth, lastBottom, colors[row]));

            /* Update last top/bottom */
            lastTop += squareHeight;
            lastBottom += squareHeight;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*Grid*/
        drawGrid(canvas, squareGridPaint);
    }

    private void drawGrid(Canvas canvas, Paint squareGridPaint) {
        for (int i = 0; i < squareList.size(); i++) {
            Square square = squareList.get(i);

            /* Drawing square based on positions */
            squareGridPaint.setColor(square.getColor());
            canvas.drawRect(square.getLeft(), square.getTop(), square.getRight(), square.getBottom(), squareGridPaint);
        }
    }
}
