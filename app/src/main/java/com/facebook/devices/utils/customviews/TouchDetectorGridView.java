package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class TouchDetectorGridView extends View {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATE_TOUCHING, STATE_RELEASED})
    public @interface TouchDetectorState {
    }

    public static final int STATE_RELEASED = 0;
    public static final int STATE_TOUCHING = 1;

    @TouchDetectorState private int viewState = STATE_RELEASED;

    /*Logic data*/
    private List<Square> squareList = new ArrayList<>();
    private int sizeMode;
    private int[] touchedPath;

    private int squareDefaultColor = 0;
    private int squareTouchedColor = 0;
    private int squareGridColor = 0;

    private TouchDetectorViewCallback callback;


    /* UI Data */
    private int baseWidth;
    private int baseHeight;
    private Paint paintBackground;
    private Paint drawingPaint;
    private Paint squareBgPaint;
    private Paint squareBorderPaint;

    private Path linePath = new Path();

    public interface TouchDetectorViewCallback {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({FINISHED_PATTERN_SUCCESS})
        @interface TouchDetectorCallbackResult {
        }

        int FINISHED_PATTERN_SUCCESS = 1;

        void onPatternDetectionFinishes(@TouchDetectorCallbackResult int resultMode);

        void onViewStateChanged(@TouchDetectorState int state);
    }

    public void setTouchDetectorViewCallback(TouchDetectorViewCallback callback) {
        this.callback = callback;
    }

    public TouchDetectorViewCallback getCallback(){
        return callback;
    }

    protected static class Square {

        /*Coordinates*/
        private float left, top, right, bottom;
        /*Sates*/
        private boolean wasTouched = false;

        public Square(float left, float top, float right, float bottom) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        public boolean wasTouched() {
            return wasTouched;
        }

        public void resetState() {
            wasTouched = false;
        }

        public boolean seekCoordinates(float x, float y) {
            if (!wasTouched) {
                wasTouched = (x > left && x < right) && (y > top && y < bottom);
            }
            return wasTouched;
        }

        public float getLeft() {
            return left;
        }

        public float getTop() {
            return top;
        }

        public float getRight() {
            return right;
        }

        public float getBottom() {
            return bottom;
        }
    }

    public TouchDetectorGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public TouchDetectorGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    /*Use it if needed*/
    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TouchDetectorGridView, defStyle, 0);

        sizeMode = typedArray.getInt(R.styleable.TouchDetectorGridView_size, 6);

        squareDefaultColor = typedArray.getColor(R.styleable.TouchDetectorGridView_colorDefaultState, Color.GRAY);
        squareTouchedColor = typedArray.getColor(R.styleable.TouchDetectorGridView_colorTouchedState, Color.GREEN);
        squareGridColor = typedArray.getColor(R.styleable.TouchDetectorGridView_colorGrid, Color.BLACK);

        typedArray.recycle();

        /*Background base paint*/
        paintBackground = new Paint();

        /*Drawing paint*/
        drawingPaint = new Paint();
        drawingPaint.setColor(Color.BLUE);
        drawingPaint.setStyle(Paint.Style.STROKE);
        drawingPaint.setStrokeWidth(3);
        drawingPaint.setAntiAlias(true);

        /*ItemRender paint*/
        squareBgPaint = new Paint();
        squareBgPaint.setStyle(Paint.Style.FILL);
        squareBgPaint.setAntiAlias(true);

        /*ItemRender Borders paint*/
        squareBorderPaint = new Paint();
        squareBorderPaint.setStyle(Paint.Style.STROKE);
        squareBorderPaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getContext().getResources().getDisplayMetrics())); /*1dp*/
        squareBorderPaint.setColor(squareGridColor);
        squareBorderPaint.setAntiAlias(true);
    }

    public void initView(int squaresSize) {
        this.sizeMode = squaresSize;

        /*Prepare grid squares*/
        setupGrid(sizeMode);
        /*Init touched path*/
        resetBehavior();
    }


    public void setSizeMode(int sizeMode) {
        this.sizeMode = sizeMode;
        resetBehavior();
    }

    public int getSizeModeSet() {
        return sizeMode;
    }

    private void clearTouchedPath() {
        touchedPath = new int[squareList.size()];
        for (int i = 0; i < touchedPath.length; i++) {
            touchedPath[i] = -1;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        /*Prepare grid squares*/
        setupGrid(sizeMode);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*Draw background base*/
        drawBackground(canvas, paintBackground);

        /*Draw Grid*/
        drawGrid(canvas, squareBgPaint/*, squareGridStrokePaint*/);

        canvas.drawPath(linePath, drawingPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isEnabled()) {
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    updateViewState(STATE_TOUCHING);
                    callbackOnStateChanged(viewState);
                    break;

                case MotionEvent.ACTION_MOVE:
                    for (int position = 0; position < squareList.size(); position++) {
                        Square square = squareList.get(position);
                        if (!square.wasTouched() && square.seekCoordinates(event.getX(), event.getY())) {
                            addHistoryPath(position);
                        }
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    updateViewState(STATE_RELEASED);
                    if (touchedPath.length == getHistoryTouchedSize()) {
                        callBackOnPatternDetectionFinishes(TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS);
                    } else {
                        callbackOnStateChanged(viewState);
                    }
                    break;
            }
            invalidate();
        }
        return true;
    }

    private void updateViewState(@TouchDetectorState int state) {
        viewState = state;
    }

    private void addHistoryPath(int position) {
        for (int i = 0; i < touchedPath.length; i++) {
            if (touchedPath[i] == -1) {
                touchedPath[i] = position;
                break;
            }
        }
    }

    private int getHistoryTouchedSize() {
        int count = 0;
        for (int i = 0; i < touchedPath.length; i++) {
            if (touchedPath[i] != -1) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }

    private void callBackOnPatternDetectionFinishes(@TouchDetectorViewCallback.TouchDetectorCallbackResult int result) {
        if (callback != null) {
            callback.onPatternDetectionFinishes(result);
        }
    }

    private void callbackOnStateChanged(@TouchDetectorState int state) {
        if (callback != null) {
            callback.onViewStateChanged(state);
        }
    }

    private void setupGrid(int sizeMode) {
        /*Background*/
        paintBackground.setShader(new LinearGradient(0,0, baseWidth, baseHeight,
                ContextCompat.getColor(getContext(), R.color.dark_sky_blue),
                ContextCompat.getColor(getContext(), R.color.light_navy_blue), Shader.TileMode.CLAMP));

        /*Colums - Rows*/
        int viewWidth = baseWidth;
        int viewHeight = baseHeight;

        squareList.clear();

        int squareNumber = sizeMode;

        float squareWidth = viewWidth / squareNumber;
        float squareHeight = viewHeight / squareNumber;

        float lastTop = 0;
        float lastBottom = squareHeight;
        for (int row = 0; row < squareNumber; row++) {

            float lastLeft = 0;
            float lastRight = squareWidth;
            for (int column = 0; column < squareNumber; column++) {

                /** Save square data */
                squareList.add(new Square(lastLeft, lastTop, lastRight, lastBottom));

                /* Update last left/right */
                lastLeft += squareWidth;
                lastRight += squareWidth;
            }

            /* Update last top/bottom */
            lastTop += squareHeight;
            lastBottom += squareHeight;
        }
    }

    private void drawBackground(Canvas canvas, Paint backgroundPaint) {
        canvas.drawPaint(backgroundPaint);
    }

    private void drawGrid(Canvas canvas, Paint squareFillPaint) {
        for (int i = 0; i < squareList.size(); i++) {
            Square square = squareList.get(i);
            /** Prepare paint */
            Paint squarePaint = squareFillPaint;
            squarePaint.setColor((square.wasTouched()) ? squareTouchedColor : squareDefaultColor);

            /** Drawing square based on positions */
            canvas.drawRect(square.getLeft(), square.getTop(), square.getRight(), square.getBottom(), squarePaint);

            /** Drawing specific tile */
            canvas.drawRect(square.getLeft(), square.getTop(), square.getRight(), square.getBottom(), squareBorderPaint);
        }
    }

    public void resetBehavior() {
        for (Square square : squareList) {
            square.resetState();
        }
        clearTouchedPath();
        invalidate();
    }
}
