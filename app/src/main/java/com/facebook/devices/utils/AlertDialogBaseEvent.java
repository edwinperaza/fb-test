package com.facebook.devices.utils;


import android.support.annotation.IntDef;

public abstract class AlertDialogBaseEvent {

    @IntDef({OPTION_POSITIVE, OPTION_NEGATIVE, OPTION_NEUTRAL})
    @interface Option {
    }

    public static final int OPTION_POSITIVE = 1;
    public static final int OPTION_NEGATIVE = -1;
    public static final int OPTION_NEUTRAL = 0;

    @Option public int optionSelected = OPTION_NEUTRAL;

    public AlertDialogBaseEvent(@Option int optionSelected) {
        this.optionSelected = optionSelected;
    }
}
