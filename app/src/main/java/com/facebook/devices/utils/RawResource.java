package com.facebook.devices.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RawResource {

    private Activity activity;
    private String[] resources;

    public RawResource(Activity activity, String... resources) {
        this.activity = activity;
        this.resources = resources;
    }

    public Uri getRandomResource(Random random) {
        if (resources == null || resources.length == 0) {
            return null;
        }

        String resourceName = resources[random.nextInt(resources.length)];

        int resourceId = activity.getResources().getIdentifier(resourceName, "raw", activity.getPackageName());

        if (activity != null && resourceId != 0) {
            return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + activity.getPackageName() + "/" + resourceId);
        }

        File file = new File(resourceName);
        return file.exists() ? Uri.fromFile(file) : null;
    }

    public Uri[] listResources() {
        if (resources == null || resources.length == 0) {
            return new Uri[0];
        }

        List<Uri> uris = new ArrayList<>();
        for (String arg : resources) {
            int resourceId = activity.getResources().getIdentifier(arg, "raw", activity.getPackageName());

            if (activity != null && resourceId != 0) {
                uris.add(Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + activity.getPackageName() + "/" + resourceId));
                continue;
            }

            File file = new File(arg);
            if (file.exists()) {
                uris.add(Uri.fromFile(file));
            }
        }

        return uris.toArray(new Uri[uris.size()]);
    }
}

