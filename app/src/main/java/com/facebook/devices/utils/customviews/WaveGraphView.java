package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.LinkedList;

public class WaveGraphView extends View {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MODE_SAMPLES, MODE_BYTES /*add More if needed*/})
    public @interface WaveMode {
    }

    /* Modes */
    public static final int MODE_SAMPLES = 1;
    public static final int MODE_BYTES = 2;
    /* Add more modes if needed */


    private static final int HISTORY_SIZE = 10;

    private Paint wavePaint;

    private int width, height;
    private float centerY;
    private int waveMode;
    private int colorAlpha = 255 / (HISTORY_SIZE + 1);

    /* Internal data */
    private LinkedList<float[]> historicalData;

    /*Recording*/
    private short[] audioSamples;

    /*Playing*/
    private byte[] playableBytes;


    public WaveGraphView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public WaveGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public WaveGraphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public class WaveViewException extends Exception {
        public WaveViewException(String exceptionMessage) {
            super(exceptionMessage);
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.WaveGraphView, defStyle, 0);

        waveMode = a.getInt(R.styleable.WaveGraphView_mode, MODE_SAMPLES);

        float strokeThickness = a.getFloat(R.styleable.WaveGraphView_waveGraphStroke, 1f);
        int mStrokeColor = a.getColor(R.styleable.WaveGraphView_waveGraphColor, ContextCompat.getColor(context, R.color.colorAccent));

        a.recycle();

        wavePaint = new Paint();
        wavePaint.setColor(mStrokeColor);
        wavePaint.setStyle(Paint.Style.STROKE);
        wavePaint.setStrokeWidth(strokeThickness);
        wavePaint.setAntiAlias(true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = getMeasuredWidth();
        height = getMeasuredHeight();
        centerY = height / 2f;

        if (historicalData != null) {
            historicalData.clear();
        }
    }

    private boolean isCorrectMode() {
        return waveMode == MODE_SAMPLES || waveMode == MODE_BYTES;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        LinkedList<float[]> bufferHistory = historicalData;
        int waveAlpha;
        if (isCorrectMode() && bufferHistory != null) {
            waveAlpha = colorAlpha;
            for (float[] samplesBuffer : bufferHistory) {
                wavePaint.setAlpha(waveAlpha);
                canvas.drawLines(samplesBuffer, wavePaint);
                waveAlpha += colorAlpha;
            }
        }
    }

    @WaveMode
    public int getMode() {
        return waveMode;
    }

    public void setMode(@WaveMode int mMode) {
        this.waveMode = mMode;
    }

    public short[] getSamples() {
        return audioSamples;
    }

    public byte[] getBytes() {
        return playableBytes;
    }

    public void setSamples(short[] samples) throws WaveViewException {
        if (waveMode == MODE_SAMPLES && samples != null) {
            audioSamples = samples;
            onSamplesChanged();
        } else {
            throw new WaveViewException("WaveView is not in the correct mode for that");
        }
    }

    public void setBytes(byte[] playingBytes) throws WaveViewException {
        if (waveMode == MODE_BYTES && playingBytes != null) {
            playableBytes = playingBytes;
            onPlayingBytesChanged();
        } else {
            throw new WaveViewException("WaveView is not in the correct mode for that");
        }
    }

    private void onSamplesChanged() {
        if (historicalData == null)
            historicalData = new LinkedList<>();
        LinkedList<float[]> temporalHistory = new LinkedList<>(historicalData);

        float[] waveformPoints;
        if (temporalHistory.size() == HISTORY_SIZE) {
            waveformPoints = temporalHistory.removeFirst();
        } else {
            waveformPoints = new float[width * 4];
        }

        drawRecordingWaveform(audioSamples, waveformPoints);
        temporalHistory.addLast(waveformPoints);
        historicalData = temporalHistory;
        postInvalidate();
    }

    /**
     * @param buffer         - latest samples
     * @param waveformPoints - history sample removed
     */
    private void drawRecordingWaveform(short[] buffer, float[] waveformPoints) {
        float lastX = -1;
        float lastY = -1;
        int pointIndex = 0;
        float max = Short.MAX_VALUE;

        /* Don't draw all of samples in the buffer, only the ones that align with the pixel boundaries*/
        for (int x = 0; x < width; x++) {
            int index = (int) (((x * 1.0f) / width) * buffer.length);
            short sample = buffer[index];
            float y = centerY - ((sample / max) * centerY);

            if (lastX != -1) {
                waveformPoints[pointIndex++] = lastX;
                waveformPoints[pointIndex++] = lastY;
                waveformPoints[pointIndex++] = x;
                waveformPoints[pointIndex++] = y;
            }

            lastX = x;
            lastY = y;
        }
    }

    /**
     * @param buffer         - latest samples
     * @param waveformPoints - float bytes history
     */
    private void drawPlayingWaveform(byte[] buffer, float[] waveformPoints) {
        if (waveformPoints != null && waveformPoints.length > 0) {
            Rect mRect = new Rect();
            mRect.set(0, 0, width, height);

            for (int i = 0; i < buffer.length - 1; i++) {
                /* Point 1*/
                waveformPoints[i * 4] = mRect.width() * i / (buffer.length - 1);/*x*/
                waveformPoints[i * 4 + 1] = mRect.height() / 2 + ((byte) (buffer[i] + 128)) * (mRect.height() / 2) / 128;/*y*/

                /* Point 2 */
                waveformPoints[i * 4 + 2] = mRect.width() * (i + 1) / (buffer.length - 1); /*x*/
                waveformPoints[i * 4 + 3] = mRect.height() / 2 + ((byte) (buffer[i + 1] + 128)) * (mRect.height() / 2) / 128;/*y*/
            }
        }
    }

    private void onPlayingBytesChanged() {
        if (historicalData == null)
            historicalData = new LinkedList<>();
        LinkedList<float[]> temporalPlayingHistory = new LinkedList<>(historicalData);

        float[] waveformPoints;
        if (temporalPlayingHistory.size() == HISTORY_SIZE) {
            waveformPoints = temporalPlayingHistory.removeFirst();
        } else {
            waveformPoints = new float[width * 4];
        }

        drawPlayingWaveform(playableBytes, waveformPoints);
        temporalPlayingHistory.addLast(waveformPoints);
        historicalData = temporalPlayingHistory;
        postInvalidate();
    }
}
