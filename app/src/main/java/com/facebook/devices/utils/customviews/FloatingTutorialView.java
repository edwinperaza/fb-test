package com.facebook.devices.utils.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.facebook.devices.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FloatingTutorialView {

    /*Animations*/
    private final int showFloatingButtonAnimRes = R.anim.translate_bottom_center;
    private final int hideFloatingButtonAnimRes = R.anim.translate_center_bottom;

    private Context context;
    private ImageButton floatingButton;

    public FloatingTutorialView(Activity activity) {
        this.context = activity;
        this.floatingButton = createFloatingButton(activity, activity.getResources().getConfiguration().orientation);
    }

    private ImageButton createFloatingButton(Activity activity, int orientation) {

        /*Main container creation*/
        FrameLayout viewGroup = activity.findViewById(android.R.id.content);
        /*Layout creation*/
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout containerLayout = new RelativeLayout(activity);
        containerLayout.setLayoutParams(lp);

        viewGroup.addView(containerLayout);

        /*Button creation*/
        RelativeLayout.LayoutParams lpBtn = new RelativeLayout.LayoutParams(
                context.getResources().getDimensionPixelSize(R.dimen.floating_tutorial_size),
                context.getResources().getDimensionPixelSize(R.dimen.floating_tutorial_size));
        lpBtn.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lpBtn.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        lpBtn.setMargins(0, 0, 0, context.getResources().getDimensionPixelSize(
                (orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        R.dimen.fab_tutorial_button_bottom_margin : R.dimen.fab_tutorial_landscape_button_bottom_margin));
        lpBtn.setMarginEnd(context.getResources().getDimensionPixelSize(
                (orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        R.dimen.fab_tutorial_button_right_margin : R.dimen.fab_tutorial_landscape_button_right_margin));

        ImageButton imageButton = new ImageButton(context);
        imageButton.setLayoutParams(lpBtn);
        imageButton.setVisibility(View.GONE);

        /*Button values*/
        imageButton.setImageResource(R.drawable.help_tutorial);
        imageButton.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_floating_button_tutorial_background));
        imageButton.setElevation(context.getResources().getDimension(R.dimen.floating_tutorial_button_elevation));

        containerLayout.addView(imageButton);

        /*Setup floating tutorial Y position*/
        Animation animation = AnimationUtils.loadAnimation(context, hideFloatingButtonAnimRes);
        animation.setDuration(0);
        imageButton.startAnimation(animation);

        return imageButton;
    }

    public ImageButton getButton() {
        return floatingButton;
    }

    public void setButtonListener(View.OnClickListener listener) {
        floatingButton.setOnClickListener(listener);
    }

    public void showButton() {
        showButton(true);
    }

    public void showButton(boolean isAnimated) {
        if (!isButtonVisible() && isAnimated) {
            Animation anim = AnimationUtils.loadAnimation(context, showFloatingButtonAnimRes);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    floatingButton.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            floatingButton.startAnimation(anim);
        } else {
            floatingButton.setVisibility(VISIBLE);
        }
    }

    public void hideButton() {
        hideButton(true);
    }

    public void hideButton(boolean isAnimated) {
        if (isButtonVisible() && isAnimated) {
            Animation anim = AnimationUtils.loadAnimation(context, hideFloatingButtonAnimRes);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    floatingButton.setVisibility(GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            floatingButton.startAnimation(anim);
        } else {
            floatingButton.setVisibility(GONE);
        }
    }

    public boolean isButtonVisible() {
        return floatingButton.getVisibility() == VISIBLE;
    }
}
