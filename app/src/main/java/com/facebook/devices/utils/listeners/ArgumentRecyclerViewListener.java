package com.facebook.devices.utils.listeners;


import android.view.View;

public interface ArgumentRecyclerViewListener<T> {

    void recyclerViewOnItemClickListener(View view, int position, T object);

    void recyclerViewOnItemTextChange(int position, T object);

    void recyclerViewOnItemLongClickListener(View view, int position, T object);
}
