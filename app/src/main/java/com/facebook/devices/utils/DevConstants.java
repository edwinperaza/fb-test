package com.facebook.devices.utils;

/**
 * Normal usage constants for dev build variant
 */
public class DevConstants {

    //Temporal Devices Address
//    public static final String ADDRESS = "28:16:AD:1D:A8:C0"; //LAPTOP
//    public static final String NAME = "WINDOWS EDWIN"; //LAPTOP_NAME
    public static final String ADDRESS = "FC:58:FA:23:D2:26"; //Philips BT50 Speaker Address
    public static final String NAME = "Philips BT50"; //Philips BT50 Speaker Name
//    public static final String ADDRESS = "2C:54:CF:7B:2B:8C";
//    public static final String NAME = "Nexus 5";
//    public static final String ADDRESS = "40:78:6A:57:DE:E8"; //Motorola
//    public static final String NAME = "Motorola"; //Motorola Phone
}