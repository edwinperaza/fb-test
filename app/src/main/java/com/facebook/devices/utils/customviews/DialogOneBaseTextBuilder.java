package com.facebook.devices.utils.customviews;

import android.content.Context;
import android.support.annotation.StringRes;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.facebook.devices.R;

public class DialogOneBaseTextBuilder {

    /*Line spacing applied*/
    public static View buildView(Context context, String text){
        TextView internalView = (TextView) LayoutInflater.from(context).inflate(R.layout.dialog_base_text_view_layout, null);
        internalView.setText(text);
        internalView.setLineSpacing(context.getResources().getDimension(R.dimen.line_spacing_base), 1.0f);
        return internalView;
    }

    /*No line spacing applied*/
    public static View buildView(Context context, SpannableString text){
        TextView internalView = (TextView) LayoutInflater.from(context).inflate(R.layout.dialog_base_text_view_layout, null);
        internalView.setText(text);
        return internalView;
    }

    public static View buildView(Context context, @StringRes int textRes){
        return buildView(context, context.getString(textRes));
    }
}
