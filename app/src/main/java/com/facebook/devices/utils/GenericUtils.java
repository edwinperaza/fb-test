package com.facebook.devices.utils;

import java.text.DecimalFormat;

public class GenericUtils {

    public static String getDynamicSpace(long bytes) {
        if (bytes <= 0) {
            return "0";
        }

        final String[] units = new String[]{"B", "Kb", "Mb", "Gb", "Tb"};
        int digitGroups = (int) (Math.log10(bytes) / Math.log10(1024));
        return new DecimalFormat("#,##0.##").format(bytes / Math.pow(1024, digitGroups))
                + " " + units[digitGroups];
    }

    public static Size[] arrayToSizeArray(android.util.Size... array) {
        Size[] sizes = new Size[array.length];

        for (int i = 0; i < array.length; i++) {
            sizes[i] = new Size(array[i].getWidth(), array[i].getHeight());
        }

        return sizes;
    }

}
