package com.facebook.devices.utils.recyclers;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.utils.listeners.ArgumentRecyclerViewListener;

import java.util.List;
import java.util.Map;

public class ArgumentsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArgumentRecyclerViewListener<ArgumentEntity> listener;
    private Context context;
    private List<ArgumentEntity> argumentEntityList;
    private List<ColorEntity> colorEntityList;
    private Map<String, TestInfoEntity> testDefinitions;
    private final int COLOR = 0;
    private final int GENERAL = 1;

    public ArgumentsRecyclerAdapter(Context context, List<ArgumentEntity> argumentEntityList) {
        this.context = context;
        this.argumentEntityList = argumentEntityList;
    }

    public void setListener(ArgumentRecyclerViewListener<ArgumentEntity> listener) {
        this.listener = listener;
    }

    public ArgumentRecyclerViewListener<ArgumentEntity> getListener(){
        return listener;
    }

    public List<ArgumentEntity> getArgumentEntityList() {
        return argumentEntityList;
    }

    public void updateItems(List<ArgumentEntity> argumentEntities, Map<String, TestInfoEntity> testDefinitions, List<ColorEntity> colorEntityList) {
        argumentEntityList = argumentEntities;
        this.testDefinitions = testDefinitions;
        this.colorEntityList = colorEntityList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return argumentEntityList.get(position).type.equals("color") ? COLOR : GENERAL;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == COLOR) {
            View v1 = inflater.inflate(R.layout.argument_recycler_item_color_layout, parent, false);
            viewHolder = new TestColorViewHolder(v1, listener);
        } else {
            View v2 = inflater.inflate(R.layout.argument_recycler_item_general_layout, parent, false);
            viewHolder = new TestGeneralViewHolder(v2, listener);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case COLOR:
                TestColorViewHolder vh1 = (TestColorViewHolder) holder;
                setUpTestColorViewHolder(vh1, position);
                break;
            case GENERAL:
                TestGeneralViewHolder vhg = (TestGeneralViewHolder) holder;
                setUpTestGeneralViewHolder(vhg, position);
                break;
        }

    }

    private void setUpTestColorViewHolder(TestColorViewHolder holder, int position) {
        ArgumentEntity argument = argumentEntityList.get(position);
        holder.tvArgumentTitle.setText(argument.name);
        holder.tvArgumentTestTitle.setText(testDefinitions.get(argument.testId).name);
        holder.etArgumentOptions.setEnabled(!argument.readOnly);
        int positionSelected = 0;

        for (int i = 0; i < colorEntityList.size(); i++) {
            if (colorEntityList.get(i).hexCode.equals(argument.value)) {
                positionSelected = i;
                break;
            }
        }
        holder.etArgumentOptions.setSelection(positionSelected);
        holder.etArgumentOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (!colorEntityList.get(pos).hexCode.equals(argumentEntityList.get(position).value)) {
                    argumentEntityList.get(position).value = colorEntityList.get(pos).hexCode;
                    listener.recyclerViewOnItemClickListener(view, pos, argumentEntityList.get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpTestGeneralViewHolder(TestGeneralViewHolder holder, int position) {
        ArgumentEntity argument = argumentEntityList.get(position);
        holder.tvArgumentTitle.setText(argument.name);
        holder.tvArgumentTestTitle.setText(testDefinitions.get(argument.testId).name);
        if (argument.type.equals("int")) {
            holder.etArgumentValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            holder.etArgumentValue.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        holder.etArgumentValue.setText(argument.value);
        holder.etArgumentValue.setEnabled(!argument.readOnly);
        holder.etArgumentValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                argumentEntityList.get(position).value = String.valueOf(s);
                listener.recyclerViewOnItemTextChange(count, argumentEntityList.get(position));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return argumentEntityList.size();
    }

    public class TestColorViewHolder extends RecyclerView.ViewHolder {

        public TextView tvArgumentTestTitle;
        public TextView tvArgumentTitle;
        public Spinner etArgumentOptions;

        public TestColorViewHolder(View itemView, ArgumentRecyclerViewListener<ArgumentEntity> listener) {
            super(itemView);

            tvArgumentTestTitle = itemView.findViewById(R.id.tv_arg_test_title);
            tvArgumentTitle = itemView.findViewById(R.id.tv_arg_arg_title);
            etArgumentOptions = itemView.findViewById(R.id.sp_arg_color);

            ColorsArrayAdapter adapter = new ColorsArrayAdapter(context,
                    R.layout.color_spinner_item_layout, colorEntityList);


            etArgumentOptions.setAdapter(adapter);
        }
    }

    public class TestGeneralViewHolder extends RecyclerView.ViewHolder {

        public TextView tvArgumentTestTitle;
        public TextView tvArgumentTitle;
        public EditText etArgumentValue;

        public TestGeneralViewHolder(View itemView, ArgumentRecyclerViewListener<ArgumentEntity> listener) {
            super(itemView);

            tvArgumentTestTitle = itemView.findViewById(R.id.tv_arg_test_title);
            tvArgumentTitle = itemView.findViewById(R.id.tv_arg_arg_title);
            etArgumentValue = itemView.findViewById(R.id.et_arg_value);
        }
    }
}
