package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashSet;
import java.util.Set;

public class MultitouchCirclesView extends View {

    private static int SMALL_CIRCLES_ROW_NUMBER = 5;
    private static int MIN_MULTITOUCH_FINGERS = 2;

    private int mainCircleColor = 0;
    private int mainCircleColorTouched = 0;
    private int smallCircleColor = 0;
    private int smallCircleColorTouched = 0;
    private int backgroundColor = 0;
    private int mainCircleInternalColor = 0;
    private int shadowColor = 0;
    private MultitouchCirclesView.MultiTouchDetectorViewCallback listener;

    /*UI data*/
    private int baseWidth;
    private int baseHeight;
    private int mainCircleRadiusInPixels = 0;
    private int smallCircleRadiusInPixels = 0;
    private MainCircle mainCircle;
    private HashSet<Circle> circles = new HashSet<>();
    private boolean dualFingersWereDetected = false;
    private boolean runningMode = true;

    /*Center screen references*/
    private int screenCenterX = 0;
    private int screenCenterY = 0;


    /*Paints*/
    private Paint backgroundPaint = new Paint();

    public interface MultiTouchDetectorViewCallback {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({FINISHED_SUCCESS, FINISHED_DROPPED})
        @interface MultiTouchDetectorCallbackResult {
        }

        int FINISHED_SUCCESS = 1;
        int FINISHED_DROPPED = 0;

        void onMultitouchReplies(@MultitouchCirclesView.MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult int resultMode);
    }

    private abstract static class BaseCircle implements Comparable<BaseCircle> {

        @IntDef({RESULT_OUTSIDE, RESULT_INSIDE, RESULT_OVERLAP})
        @interface CompareResult {
        }

        public static final int RESULT_OUTSIDE = 0;
        public static final int RESULT_INSIDE = 1;
        public static final int RESULT_OVERLAP = 2;

        /* Values */
        protected float canvasMaxHeight = 0;
        protected float canvasMaxWidth = 0;
        protected float radius;
        protected int colorNormal = 0;
        protected int colorActioned = 0;
        protected int shadowColor = 0;

        /* Rendering */
        protected Paint paint = new Paint();

        /* Coordinates */
        protected int originalCenterX;
        protected int originalCenterY;
        protected int currentCenterX;
        protected int currentCenterY;
        /*Pivot*/
        protected int pivotXDiff;
        protected int pivotYDiff;

        public BaseCircle(float canvasMaxHeight, float canvasMaxWidth, int centerX, int centerY, float radius,
                          int colorNormal, int colorActioned, int shadowColor) {
            /*Canvas data*/
            this.canvasMaxHeight = canvasMaxHeight;
            this.canvasMaxWidth = canvasMaxWidth;
            /*Values*/
            this.colorNormal = colorNormal;
            this.colorActioned = colorActioned;
            this.shadowColor = shadowColor;
            /*Circle data*/
            this.originalCenterX = centerX;
            this.originalCenterY = centerY;
            this.currentCenterX = centerX;
            this.currentCenterY = centerY;
            this.radius = radius;
            /*Paint setup*/
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            paint.setShadowLayer(10, 0, 5, shadowColor);
        }

        public float getRadius() {
            return radius;
        }

        public float getCurrentCenterX() {
            return currentCenterX;
        }

        public float getCurrentCenterY() {
            return currentCenterY;
        }

        public void moveTo(int newX, int newY) {

            int auxCurrentCenterX = pivotXDiff + newX;
            int auxCurrentCenterY = pivotYDiff + newY;

            /*Check X*/
            if ((auxCurrentCenterX + radius) <= canvasMaxWidth && (auxCurrentCenterX - radius) >= 0) {
                currentCenterX = auxCurrentCenterX;
            }
            /*Check Y*/
            if ((auxCurrentCenterY + radius) < canvasMaxHeight && (auxCurrentCenterY - radius) >= 0) {
                currentCenterY = auxCurrentCenterY;
            }
        }

        @Override
        public int compareTo(@NonNull BaseCircle secondCircle) {
            /*Calculate distance*/
            double distance = Math.hypot(currentCenterX - secondCircle.getCurrentCenterX(), currentCenterY - secondCircle.getCurrentCenterY());
            /*Check*/
            if (distance > (radius + secondCircle.getRadius())) {
                return RESULT_OUTSIDE;
            } else {
                if (distance <= Math.abs(radius - secondCircle.getRadius())) {
                    return RESULT_INSIDE;
                } else {
                    return RESULT_OVERLAP;
                }
            }
        }

        /* Draw circle */
        public void render(Canvas canvas) {
            paint.setColor(isActioned() ? colorActioned : colorNormal);
            canvas.drawCircle(currentCenterX, currentCenterY, radius, paint);
        }

        protected abstract boolean isActioned();
    }

    private class MainCircle extends BaseCircle {

        /* States */
        private boolean isTouched = false;
        /* Rendering */
        private Paint internalCirclePaint = new Paint();
        private final float INTERNAL_PERCENTAGE = 0.76f; /* 76% */
        /* Values */
        private int internalColor = 0;
        private int internalColorTouched = 0;

        public MainCircle(float canvasMaxHeight, float canvasMaxWidth, int centerX, int centerY, float radius,
                          int colorNormal, int internalColor, int internalColorTouched, int shadowColor) {
            super(canvasMaxHeight, canvasMaxWidth, centerX, centerY, radius, colorNormal, colorNormal, shadowColor);
            this.internalColor = internalColor;
            this.internalColorTouched = internalColorTouched;
            /*Paint setup*/
            internalCirclePaint.setStyle(Paint.Style.FILL);
            internalCirclePaint.setAntiAlias(true);
            internalCirclePaint.setShadowLayer(10, 0, 5, shadowColor);
        }

        public void setTouched(int pivotX, int pivotY) {
            isTouched = true;
            this.pivotXDiff = currentCenterX - pivotX;
            this.pivotYDiff = currentCenterY - pivotY;
        }

        public void setReleased(){
            isTouched = false;
        }

        public boolean isTouched() {
            return isTouched;
        }

        @Override
        public void render(Canvas canvas) {
            super.render(canvas);
            /*Draw internal circle*/
            internalCirclePaint.setColor(isActioned() ? internalColorTouched : internalColor);
            canvas.drawCircle(currentCenterX, currentCenterY, radius * INTERNAL_PERCENTAGE, internalCirclePaint);
        }

        /**
         * Reset to original coordinates
         */
        public void reset() {
            pivotXDiff = 0;
            pivotYDiff = 0;
            isTouched = false;
            moveTo(originalCenterX, originalCenterY);
        }

        @Override
        protected boolean isActioned() {
            return isTouched;
        }
    }

    private class Circle extends BaseCircle {

        /* States */
        private boolean wasOverlapped = false;

        public Circle(float canvasMaxHeight, float canvasMaxWidth, int centerX, int centerY, float radius,
                      int colorNormal, int colorOverlapped, int shadowColor) {
            super(canvasMaxHeight, canvasMaxWidth, centerX, centerY, radius, colorNormal, colorOverlapped, shadowColor);
        }

        @Override
        protected boolean isActioned() {
            return wasOverlapped;
        }

        public void setOverlapped(boolean wasOverlap) {
            this.wasOverlapped = wasOverlap;
        }

        public boolean isAlreadyOverlapped() {
            return wasOverlapped;
        }
    }

    public MultitouchCirclesView(Context context) {
        super(context);
    }

    public MultitouchCirclesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MultitouchCirclesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MultitouchCirclesView, defStyle, 0);

        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        mainCircleColor = typedArray.getColor(R.styleable.MultitouchCirclesView_mainCircleColor, Color.GREEN);
        mainCircleInternalColor = typedArray.getColor(R.styleable.MultitouchCirclesView_mainCircleInternalColor, Color.GREEN);
        mainCircleColorTouched = typedArray.getColor(R.styleable.MultitouchCirclesView_mainCircleColorTouched, Color.BLUE);
        smallCircleColor = typedArray.getColor(R.styleable.MultitouchCirclesView_smallCircleColor, Color.RED);
        smallCircleColorTouched = typedArray.getColor(R.styleable.MultitouchCirclesView_smallCircleColorTouched, Color.CYAN);
        shadowColor = typedArray.getColor(R.styleable.MultitouchCirclesView_shadowColor, Color.GRAY);
        backgroundColor = typedArray.getColor(R.styleable.MultitouchCirclesView_backgroundColor, Color.TRANSPARENT);

        /*Sizes*/
        mainCircleRadiusInPixels = typedArray.getDimensionPixelSize(R.styleable.MultitouchCirclesView_mainCircleDiameterDimen, 0) / 2;
        smallCircleRadiusInPixels = typedArray.getDimensionPixelSize(R.styleable.MultitouchCirclesView_smallCircleDiameterDimen, 0) / 2;

        typedArray.recycle();

        /* Background Paint */
        backgroundPaint.setColor(backgroundColor);
    }

    public void setListener(MultitouchCirclesView.MultiTouchDetectorViewCallback listener) {
        this.listener = listener;
    }

    public MultiTouchDetectorViewCallback getListener() {
        return listener;
    }

    private void onMultitouchReplies(@MultitouchCirclesView.MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult int resultMode) {
        if (listener != null) {
            listener.onMultitouchReplies(resultMode);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        screenCenterX = (int) (baseWidth / 2);
        screenCenterY = (int) (baseHeight / 2);

        /*Setup square position*/
        setupElements();
    }

    private float convertDpToPixels(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float pixels = dp * (metrics.densityDpi / 160f);
        return Math.round(pixels);
    }

    /**
     * Circle radius: if the circle radius in dp is not set, a value of %20 of the pixels of the smallest side of the
     * screen will be used.
     */
    private void setupElements() {

        /** Main circle setup */
        if (mainCircleRadiusInPixels == 0) {
            float smallestSide = (baseHeight < baseWidth) ? baseHeight : baseWidth;
            mainCircleRadiusInPixels = (int) (smallestSide * 0.25); /* 25% by default */
        }

        mainCircle = new MainCircle(baseHeight, baseWidth, screenCenterX, screenCenterY, mainCircleRadiusInPixels,
                mainCircleColor, mainCircleInternalColor, mainCircleColorTouched, shadowColor);

        /** Small circles setup */
        int baseMargin = (int) convertDpToPixels(80);

        if (smallCircleRadiusInPixels == 0) {
            float smallestSide = (baseHeight < baseWidth) ? baseHeight : baseWidth;
            smallCircleRadiusInPixels = (int) (smallestSide * 0.2); /* 20% default*/
        }

        int smallRadiusPx = smallCircleRadiusInPixels;

        int smallY = baseMargin + smallRadiusPx;
        int incrementY = (int) ((baseHeight - (smallY * 2)) / (SMALL_CIRCLES_ROW_NUMBER - 1));
        for (int i = 0; i < SMALL_CIRCLES_ROW_NUMBER; i++, smallY += incrementY) {

            if (i % 2 == 0) {
                /*First*/
                circles.add(new Circle(baseHeight, baseWidth, baseMargin + smallRadiusPx, smallY, smallRadiusPx,
                        smallCircleColor, smallCircleColorTouched, shadowColor));
                /*Second*/
                if (i != (SMALL_CIRCLES_ROW_NUMBER - 1) / 2) { /* Avoid center circle creation */
                    circles.add(new Circle(baseHeight, baseWidth, screenCenterX, smallY, smallRadiusPx,
                            smallCircleColor, smallCircleColorTouched, shadowColor));
                }
                /*Third*/
                circles.add(new Circle(baseHeight, baseWidth, baseWidth - baseMargin - smallRadiusPx, smallY, smallRadiusPx,
                        smallCircleColor, smallCircleColorTouched, shadowColor));

            } else {
                /*First*/
                circles.add(new Circle(baseHeight, baseWidth,
                        screenCenterX - ((screenCenterX - (baseMargin + smallRadiusPx)) / 2),
                        smallY, smallRadiusPx, smallCircleColor, smallCircleColorTouched, shadowColor));
                /*Second*/
                circles.add(new Circle(baseHeight, baseWidth,
                        screenCenterX + ((screenCenterX - (baseMargin + smallRadiusPx)) / 2),
                        smallY, smallRadiusPx, smallCircleColor, smallCircleColorTouched, shadowColor));
            }

        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*Draw background*/
        if (backgroundPaint.getColor() != Color.TRANSPARENT) {
            canvas.drawPaint(backgroundPaint);
        }

        /*Draw circles*/
        for (Circle circle : circles) {
            circle.render(canvas);
        }

        /*Draw square*/
        mainCircle.render(canvas);
    }

    private boolean fingersInsideCircle(MainCircle mainCircle, MotionEvent event) {
        /*Data load*/
        boolean areInside = false;
        if (event.getPointerCount() >= MIN_MULTITOUCH_FINGERS) {
            for (int i = 0; i < event.getPointerCount(); i++) {
                int pointerId = event.getPointerId(i);

                int fingerX = (int) event.getX(event.findPointerIndex(pointerId));
                int fingerY = (int) event.getY(event.findPointerIndex(pointerId));

                Circle fingerCircle = new Circle(baseHeight, baseWidth, fingerX, fingerY, convertDpToPixels(20), 0, 0, 0);
                areInside = (mainCircle.compareTo(fingerCircle) != BaseCircle.RESULT_OUTSIDE) || areInside;
            }
        }
        return areInside;
    }

    private void moveCircle(MainCircle mainCircle, HashSet<Circle> circles, int newX, int newY) {
        /* Update data*/
        mainCircle.moveTo(newX, newY);
        /* Check new location */
        checkCircleTouch(mainCircle, circles);
    }

    private void checkCircleTouch(MainCircle mainCircle, Set<Circle> circles) {
        for (Circle circle : circles) {
            if (!circle.isAlreadyOverlapped() && mainCircle.compareTo(circle) != BaseCircle.RESULT_OUTSIDE) {
                circle.setOverlapped(true);
            }
        }
    }

    private boolean checkCirclesOverlapping(Set<Circle> circles) {
        for (Circle circle : circles) {
            if (!circle.isAlreadyOverlapped()) {
                return false;
            }
        }
        return true;
    }


    private void onFailure() {
        runningMode = false;
        mainCircle.setReleased();
        onMultitouchReplies(MultitouchCirclesView.MultiTouchDetectorViewCallback.FINISHED_DROPPED);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!runningMode || !isEnabled()) {
            return true;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_MOVE:
                if (fingersInsideCircle(mainCircle, event)) {

                    if (!dualFingersWereDetected) {
                        dualFingersWereDetected = true;
                        int pointerIdOne = event.getPointerId(0);
                        int xc = (int) event.getX(event.findPointerIndex(pointerIdOne));
                        int yc = (int) event.getY(event.findPointerIndex(pointerIdOne));
                        mainCircle.setTouched(xc, yc);
                    }

                    int pointerIdOne = event.getPointerId(0);
                    int fingerOneX = (int) event.getX(event.findPointerIndex(pointerIdOne));
                    int fingerOneY = (int) event.getY(event.findPointerIndex(pointerIdOne));

                    moveCircle(mainCircle, circles, fingerOneX, fingerOneY);

                    if (checkCirclesOverlapping(circles)) {
                        runningMode = false;
                        onMultitouchReplies(MultiTouchDetectorViewCallback.FINISHED_SUCCESS);
                    }
                }
                break;

            case MotionEvent.ACTION_POINTER_UP:
                if (dualFingersWereDetected) { /*One of fingers were lost*/
                    onFailure();
                }
                break;

            case MotionEvent.ACTION_UP:
                break;
        }

        invalidate();
        return true;
    }

    private void resetAllCircles() {
        /*Main Circle*/
        if (mainCircle != null) {
            mainCircle.reset();
        }
        /*Small Circles*/
        for (Circle circle : circles) {
            circle.setOverlapped(false);
        }
    }

    public void resetViewState() {
        resetAllCircles();
        runningMode = true;
        dualFingersWereDetected = false;
        invalidate();
    }

    public boolean isRunningMode() {
        return runningMode;
    }

    public boolean isDualFingersWereDetected() {
        return dualFingersWereDetected;
    }
}
