package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IntDef;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;

import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A {@link TextureView} that can be adjusted to a specified aspect ratio.
 */
public class AutoFitTextureView extends TextureView {

    @IntDef({FITMODE_CENTER_INSIDE, FITMODE_CENTER_CROP})
    @Retention(RetentionPolicy.SOURCE)
    public static @interface FitMode {
    }

    public static final int FITMODE_CENTER_INSIDE = 0;
    public static final int FITMODE_CENTER_CROP = 1;


    @FitMode int fitMode = FITMODE_CENTER_INSIDE;
    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    public AutoFitTextureView(Context context) {
        this(context, null);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(attrs, 0);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.AutoFitTextureView, defStyle, 0);
        fitMode = typedArray.getInt(R.styleable.AutoFitTextureView_fitMode, FITMODE_CENTER_INSIDE);

        typedArray.recycle();
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }

    public Size getAspectRatio(){
        return new Size(mRatioWidth,mRatioHeight);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height) {
                // width is smaller than height, mostly when you are in portrait
                switch (fitMode) {
                    case FITMODE_CENTER_INSIDE:
                        setMeasuredDimension(width, width * mRatioWidth / mRatioHeight); // Preview fit in the with of the screen in portrait
                        break;

                    case FITMODE_CENTER_CROP:
                        setMeasuredDimension(height * mRatioWidth / mRatioHeight, height); // Preview cover the whole screen but cuts on the sides in portrait
                        break;
                }
            } else {
                switch (fitMode) {
                    case FITMODE_CENTER_INSIDE:
                        setMeasuredDimension(height * mRatioWidth / mRatioHeight, height); // Preview fit in the height of the screen in landscape
                        break;

                    case FITMODE_CENTER_CROP:
                        setMeasuredDimension(width, width * mRatioHeight / mRatioWidth); // Preview cover the whole screen but cuts on the top and bottom sides in landscape
                        break;
                }
            }
        }
    }

    public void setFitMode(@FitMode int fitMode) {
        this.fitMode = fitMode;
    }
}
