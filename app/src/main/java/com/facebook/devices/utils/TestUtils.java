package com.facebook.devices.utils;

import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.utils.recyclers.DigestTest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestUtils {

    public static List<DigestTest> processTests(List<GroupTestsEntity> groupsEntity, Map<String, TestInfoEntity> testDefinitions) {

        /*Sorting test definitions*/
        Map<String, TestInfoEntity> sortedMap = sortTestMap(testDefinitions);

        /*Processing data*/
        List<DigestTest> tests = new ArrayList<>();
        for (GroupTestsEntity entity : groupsEntity) {
            for (Map.Entry<String, TestInfoEntity> map : sortedMap.entrySet()) {
                if (map.getValue().groupId.equals(entity.getGroupType())) {
                    tests.add(new DigestTest(entity.iconResource, map.getValue()));
                }
            }
        }
        return tests;
    }

    private static Map<String, TestInfoEntity> sortTestMap(Map<String, TestInfoEntity> testDefinitions) {
        return testDefinitions.entrySet()
                .stream()
                .sorted(Comparator.comparingInt(value -> value.getValue().orderId))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }
}
