package com.facebook.devices.utils.customcomponents;


import android.os.Handler;
import android.os.Looper;

public class CancellableHandler extends Handler implements Cancellable {

    private Runnable runnable;

    public CancellableHandler(Looper looper) {
        super(looper);
    }

    public boolean postDelayedCustom(Runnable r, long delayMillis) {
        runnable = r;
        return super.postDelayed(r, delayMillis);
    }

    @Override
    public void removeCallbacks() {
        if (runnable != null) {
            removeCallbacks(runnable);
            runnable = null;
        }
    }
}