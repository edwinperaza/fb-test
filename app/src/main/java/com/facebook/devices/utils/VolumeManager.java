package com.facebook.devices.utils;

import android.content.Context;
import android.media.AudioManager;

public class VolumeManager {

    private Context context;

    public VolumeManager(Context context) {
        this.context = context;
    }

    public void setDefaultMediaVolume() {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.5f),
                0);
    }
}
