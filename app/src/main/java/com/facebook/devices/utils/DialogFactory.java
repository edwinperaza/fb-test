package com.facebook.devices.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.facebook.devices.R;

public class DialogFactory {

    public static AlertDialog.Builder createMaterialDialog(Context context, CharSequence title, CharSequence message) {

        View dialogView = LayoutInflater.from(context).inflate(R.layout.base_material_dialog_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MaterialBaseDialogStyle)
                .setCancelable(false)
                .setView(dialogView);

        setText(dialogView, R.id.tv_material_dialog_title, title);
        setText(dialogView, R.id.tv_material_dialog_message, message);

        return alertDialogBuilder;
    }

    public static AlertDialog.Builder createMaterialDialog(Context context, @StringRes int title, @StringRes int message) {
        return createMaterialDialog(context, getText(context, title), getText(context, message));
    }

    private static String getText(Context context, @StringRes int res) {
        if (res != 0) {
            return context.getString(res);
        }
        return "";
    }

    private static void setText(View view, @IdRes int id, CharSequence str) {
        TextView textView = view.findViewById(id);
        if (TextUtils.isEmpty(str)) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setText(str);
        }
    }

}
