package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.devices.R;

public class CustomToolbar extends LinearLayout {

    private ImageView ivBack;
    private ImageView ivActionIcon;
    private ImageView ivActionIconRight;
    private TextView tvTitle;

    public CustomToolbar(Context context) {
        super(context);
        init(context);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.custom_toolbar_view_layout, this);

        ivBack = findViewById(R.id.iv_toolbar_back);
        tvTitle = findViewById(R.id.tv_toolbar_title);

        ivActionIcon = findViewById(R.id.iv_toolbar_action_icon);
        ivActionIconRight = findViewById(R.id.iv_toolbar_action_icon_right);
    }

    public void setTitleVisible(boolean isVisible) {
        tvTitle.setVisibility((isVisible) ? VISIBLE : INVISIBLE);
    }

    public boolean isTitleVisible() {
        return tvTitle.getVisibility() == VISIBLE;
    }

    public void setTitle(String string) {
        tvTitle.setText(string);
    }

    public void setTitle(int stringResource) {
        tvTitle.setText(stringResource);
    }

    public String getTitle() {
        return tvTitle.getText().toString();
    }

    public void setTitleColor(int color) {
        tvTitle.setTextColor(color);
    }

    public void setTitleSize(float size) {
        tvTitle.setTextSize(size);
    }

    public float getTitleSize() {
        return tvTitle.getTextSize();
    }

    public void setTitleMarginStart(int marginStart) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        params.setMargins(marginStart, 0, 0, 0);
        tvTitle.setLayoutParams(params);
    }

    public void setBackIconVisible(boolean isVisible) {
        ivBack.setVisibility((isVisible) ? VISIBLE : INVISIBLE);
    }

    public void setBackButtonGone(){
        ivBack.setVisibility(GONE);
    }

    public boolean isBackButtonVisible() {
        return ivBack.getVisibility() == VISIBLE;
    }

    public void setBackIconResource(int iconResouce) {
        ivBack.setImageResource(iconResouce);
    }

    public void setOnBackIconPressed(OnClickListener listener) {
        ivBack.setOnClickListener(listener);
    }

    public void setElevation(float elevation) {
        super.setElevation(elevation);
    }

    public void setGenericTintIconColor(int color) {
        ivBack.setColorFilter(color);
        ivActionIcon.setColorFilter(color);
        ivActionIconRight.setColorFilter(color);
    }

    public void setActionIconVisible(boolean isVisible) {
        ivActionIcon.setVisibility((isVisible) ? VISIBLE : GONE);
    }

    public void setActionIconRightVisible(boolean isVisible) {
        ivActionIconRight.setVisibility((isVisible) ? VISIBLE : GONE);
    }

    public void setActionIconListener(OnClickListener listener) {
        ivActionIcon.setOnClickListener(listener);
    }

    public void setActionIconRightListener(OnClickListener listener) {
        ivActionIconRight.setOnClickListener(listener);
    }

    public void setActionIcon(int drawableResource) {
        ivActionIcon.setImageResource(drawableResource);
    }

    public void setActionIconRight(int drawableResource) {
        ivActionIconRight.setImageResource(drawableResource);
    }
}
