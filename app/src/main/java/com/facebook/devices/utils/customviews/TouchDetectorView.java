package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.devices.R;
import com.facebook.devices.mvp.model.OrientationModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.facebook.devices.utils.customviews.TouchDetectorView.TileRender.TYPE_HEAD;

public class TouchDetectorView extends View {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ORIENTATION_HORIZONTAL, ORIENTATION_VERTICAL})
    public @interface TouchDetectorOrientation {
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TILE_DOT, TILE_LINE, TILE_ARC})
    public @interface TileTypes {
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TILE_0_DEGREES, TILE_180_DEGREES, TILE_90_DEGREES, TILE_270_DEGREES})
    public @interface TileOrientation {
    }

    /*Tiles designs*/
    public static final int TILE_DOT = 0;
    public static final int TILE_LINE = 1;
    public static final int TILE_ARC = 2;
    /*Tiles configuration*/
    public static final int TILE_0_DEGREES = 0;
    public static final int TILE_90_DEGREES = 1;
    public static final int TILE_180_DEGREES = 2;
    public static final int TILE_270_DEGREES = 3;

    /*Orientation modes*/
    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;

    /*Logic data*/
    private List<TileRender> tileRenderList = new ArrayList<>();
    private int sizeMode;
    private int orientationMode;
    private int[] correctPath = new int[0];
    private int[] touchedPath;
    private int nextCorrectPathIndex = 0;
    private int tileLineDefaultColor = 0;
    private int tileLineTouchedColor = 0;
    private Bitmap progressPathDrawable;
    private Bitmap endPathDrawable;
    private TouchDetectorViewCallback callback;


    /* UI Data */
    private int baseWidth;
    private int baseHeight;
    private Paint paintBackground;
    private Paint squareGridPaint;
    private Paint tilePaintDefault;
    private Paint tilePaintTouched;

    public interface TouchDetectorViewCallback {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({FINISHED_PATTERN_SUCCESS, FINISHED_PATTERN_FAILED, FINISHED_PATTERN_FAILED_INCOMPLETE})
        @interface TouchDetectorCallbackResult {
        }

        int FINISHED_PATTERN_SUCCESS = 1;
        int FINISHED_PATTERN_FAILED = 0;
        int FINISHED_PATTERN_FAILED_INCOMPLETE = -1;


        void onPatternDetectionFinishes(@TouchDetectorCallbackResult int resultMode);
    }

    public void setTouchDetectorViewCallback(TouchDetectorViewCallback callback) {
        this.callback = callback;
    }

    protected static class TileGeneratorHelper {

        /**
         * Base method to generate dynamically all TileRender child instances needed
         * This method should be used to generate it's child
         * This method needs sizeMode as param in order to morph in relation with it's brothers in list
         *
         * @param tile            tile base used to be morph
         * @param correctPath     path solution to the puzzle
         * @param sizeMode        the size of the puzzle just for one side (i.e: 4x4 should be 4)
         * @param squarePosition  the position of the square in the loop list creation
         * @param orientationMode the orientation of the puzzle
         * @param squareListCount the number of items in the puzzle (side at the power of two)
         */
        public static TileRender generateTile(TileRender tile, int[] correctPath, int sizeMode, int squarePosition,
                                              @TouchDetectorOrientation int orientationMode, int squareListCount, Bitmap progressPathDrawRes, Bitmap endPathDrawRes) {
            @TileTypes int tileType = tile.getTileTypeNeeded(squarePosition, sizeMode, correctPath);
            switch (tileType) {
                case TILE_LINE:
                    TileLineNormal tileLineNormal = new TileLineNormal(tile.getLeft(), tile.getTop(), tile.getRight(), tile.getBottom(),
                            tile.getRectangleType(), progressPathDrawRes, tile.getTilePaintDefault(), tile.getTilePaintProgress());
                    tileLineNormal.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);
                    return tileLineNormal;
                case TILE_DOT:
                    TileLineDot tileLineDot = new TileLineDot(tile.getLeft(), tile.getTop(), tile.getRight(), tile.getBottom(),
                            tile.getRectangleType(), progressPathDrawRes, endPathDrawRes, tile.getTilePaintDefault(), tile.getTilePaintProgress());
                    tileLineDot.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);
                    return tileLineDot;

                case TILE_ARC:
                    TileArc tileArc = new TileArc(tile.getLeft(), tile.getTop(), tile.getRight(), tile.getBottom(),
                            tile.getRectangleType(), progressPathDrawRes, tile.getTilePaintDefault(), tile.getTilePaintProgress());
                    tileArc.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);
                    return tileArc;
            }

            return null;
        }
    }

    protected static class Coordinate {
        private int x;
        private int y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }

    protected static class TileRender {

        @IntDef({TYPE_HEAD, TYPE_BODY, TYPE_TAIL})
        @interface Type {
        }

        public static final int TYPE_HEAD = 0;
        public static final int TYPE_BODY = 1;
        public static final int TYPE_TAIL = 2;

        /*ItemRender type*/
        @Type protected int rectangleType;

        /*Configuration*/
        @TouchDetectorOrientation int touchDetectorOrientationMode;
        @TileTypes int tileType = TILE_LINE; /*Default value*/
        @TileOrientation int tileOrientation = TILE_0_DEGREES; /*Default value*/

        /*Coordinates*/
        protected int left, top, right, bottom;
        protected int lastTouchedX, lastTouchedY;
        protected int squareApothemHoriz;
        protected int squareApothemVert;

        /*Rendering*/
        protected Paint tilePaintDefault;
        protected Paint tilePaintProgress;

        /*Sates*/
        protected boolean wasCompleted = false;
        protected int squarePosition;

        /*Constructor without configuration*/
        public TileRender(int left, int top, int right, int bottom, @Type int rectangleType, Paint tilePaintDefault, Paint tilePaintProgress) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.rectangleType = rectangleType;
            this.tilePaintDefault = tilePaintDefault;
            this.tilePaintProgress = tilePaintProgress;
            this.squareApothemHoriz = (right - left) / 2;
            this.squareApothemVert = (bottom - top) / 2;
        }

        public Paint getTilePaintDefault() {
            return tilePaintDefault;
        }

        public Paint getTilePaintProgress() {
            return tilePaintProgress;
        }

        public boolean wasCompleted() {
            return wasCompleted;
        }

        public void resetState() {
            wasCompleted = false;
        }

        /**
         * Update the last touched coordinates
         *
         * @return false if the tile was not completed
         */
        public boolean setLastTouchedCoordinates(int x, int y) {

            if (!wasCompleted &&
                    (x > left && x < right) && (y > top && y < bottom)) { /*Check completed state and inside square boundaries*/

                this.lastTouchedX = x;
                this.lastTouchedY = y;

                return wasCompleted = checkCompletedState(lastTouchedX, lastTouchedY, tileType, tileOrientation);
            }

            return wasCompleted;
        }

        protected boolean checkCompletedState(int touchX, int touchY, @TileTypes int tileType, @TileOrientation int tileOrientation) {
            /*Nothing to do here*/
            return false;
        }

        public int getLeft() {
            return left;
        }

        public int getTop() {
            return top;
        }

        public int getRight() {
            return right;
        }

        public int getBottom() {
            return bottom;
        }

        @TileOrientation
        public int getTileOrientation() {
            return tileOrientation;
        }

        @TileTypes
        public int getTileType() {
            return tileType;
        }

        @Type
        public int getRectangleType() {
            return rectangleType;
        }

        @TouchDetectorOrientation
        public int getTouchDetectorOrientationMode() {
            return touchDetectorOrientationMode;
        }

        /* ----- Configuration section ----- */

        /**
         * @param squarePosition index of the square in the list contained
         * @param sizeMode       the size of the puzzle just for one side (i.e: 4x4 should be 4)
         * @param correctPath    path solution to the puzzle
         */
        @TileTypes
        private int getTileTypeNeeded(int squarePosition, int sizeMode, int[] correctPath) {

            /*Dot check*/
            if (correctPath[0] == squarePosition || /*Check first position*/
                    correctPath[correctPath.length - 1] == squarePosition /*Check for last position*/) {
                return TILE_DOT;
            }

            /*Arc check*/
            int squareIndexInPath = getSquareIndexInPath(correctPath, squarePosition);
            if ((squareIndexInPath + 1) % sizeMode == 0 ||
                    (squareIndexInPath % sizeMode == 0) && (correctPath[squareIndexInPath - 1] != correctPath[0])) {
                return TILE_ARC;
            }

            /*Line*/
            return TILE_LINE;
        }

        /**
         * @param correctPath    correct path array
         * @param squarePosition index of the square in the squareList
         * @return the index of the specific square in the correct path
         */
        private int getSquareIndexInPath(int[] correctPath, int squarePosition) {
            for (int i = 0; i < correctPath.length; i++) {
                if (correctPath[i] == squarePosition) {
                    return i;
                }
            }
            return -1;
        }

        @TileOrientation
        private int getTileOrientation(int sizeMode, @TileTypes int tileType, @TouchDetectorOrientation int orientationMode, int squarePosition, int squareCount, int[] correctPath) {

            int squareCountLine = sizeMode;

            /*Start/End check*/
            switch (tileType) {

                case TILE_DOT:
                    switch (orientationMode) {
                        case ORIENTATION_HORIZONTAL:
                            return (getSquareIndexInPath(correctPath, squarePosition) == 0 || squareCount % 2 == 0) ? TILE_0_DEGREES : TILE_180_DEGREES;

                        case ORIENTATION_VERTICAL:
                            return (getSquareIndexInPath(correctPath, squarePosition) == 0 || squareCount % 2 == 0) ? TILE_270_DEGREES : TILE_90_DEGREES;
                    }
                    break;


                case TILE_ARC:

                    switch (orientationMode) {
                        case ORIENTATION_HORIZONTAL:

                            /*First column*/
                            if (squarePosition != 0 && (squarePosition % squareCountLine == 0)) {
                                return (getSquareIndexInPath(correctPath, squarePosition) % squareCountLine == 0) ? TILE_90_DEGREES : TILE_0_DEGREES;
                            }

                            /*Last column*/
                            if ((squarePosition + 1) % squareCountLine == 0) {
                                return ((getSquareIndexInPath(correctPath, squarePosition) + 1) % squareCountLine == 0) ? TILE_270_DEGREES : TILE_180_DEGREES;
                            }

                            break;


                        case ORIENTATION_VERTICAL:

                            /*First Row*/
                            if (squarePosition >= 0 && squarePosition < squareCountLine) {
                                return ((getSquareIndexInPath(correctPath, squarePosition) + 1) % squareCountLine == 0) ? TILE_0_DEGREES : TILE_270_DEGREES;
                            }

                            /*Last Row*/
                            if (squarePosition >= (squareCount - squareCountLine) && squarePosition < squareCount) {
                                return ((getSquareIndexInPath(correctPath, squarePosition) + 1) % squareCountLine == 0) ? TILE_90_DEGREES : TILE_180_DEGREES;
                            }

                            break;
                    }
                    break;


                case TILE_LINE:
                    switch (orientationMode) {
                        case ORIENTATION_HORIZONTAL:
                            return (((int) squarePosition / squareCountLine) % 2 == 0) ? TILE_0_DEGREES : TILE_180_DEGREES;

                        case ORIENTATION_VERTICAL:
                            return (squarePosition % 2 == 0) ? TILE_270_DEGREES : TILE_90_DEGREES;
                    }
                    break;
            }

            return TILE_0_DEGREES; /*Default*/
        }

        /**
         * This method should be used only one time for configuration setup
         * If one of the parameters changes in the view lifetime this method should be re executed
         */
        protected void configureTile(int[] correctPath, int sizeMode, int squarePosition, @TouchDetectorOrientation int orientationMode, int squareListCount) {
            this.squarePosition = squarePosition;
            tileType = getTileTypeNeeded(squarePosition, sizeMode, correctPath);
            tileOrientation = getTileOrientation(sizeMode, tileType, orientationMode, squarePosition, squareListCount, correctPath);
            touchDetectorOrientationMode = orientationMode;
        }

        /* ---- Rendering section ---- */
        public void draw(Canvas canvas, int currentProgressPosition) {
            /*Nothing to do here for now*/
        }
    }

    protected abstract static class TileLine extends TileRender {

        private static final float TOLERANCE_PERCENTAGE = 0.10f; /* 10% */
        protected Bitmap progressPathBitmap;

        int startX = 0, startY = 0, endX = 0, endY = 0;

        public TileLine(int left, int top, int right, int bottom, int squareType, Paint tilePaintDefault, Paint tilePaintProgress) {
            super(left, top, right, bottom, squareType, tilePaintDefault, tilePaintProgress);
        }

        /* ---- Check state section ---- */
        @Override
        public boolean checkCompletedState(int touchX, int touchY, @TileTypes int tileType, @TileOrientation int tileOrientation) {

            int touchMatters = 0;
            int endMatters = 0;
            int tolerance = 0;

            switch (tileOrientation) {

                case TILE_0_DEGREES:
                case TILE_180_DEGREES:
                    touchMatters = touchX;
                    endMatters = endX;
                    tolerance = (int) (endX * TOLERANCE_PERCENTAGE);
                    break;

                case TILE_90_DEGREES:
                case TILE_270_DEGREES:
                    touchMatters = touchY;
                    endMatters = endY;
                    tolerance = (int) (endY * TOLERANCE_PERCENTAGE);
                    break;
            }

            return (touchMatters >= endMatters - tolerance) && (touchMatters <= endMatters + tolerance);
        }

        @Override
        public void resetState() {
            super.resetState();
            /*Default last touched coordinates*/
            lastTouchedX = startX;
            lastTouchedY = startY;
        }
    }

    protected static class TileLineNormal extends TileLine {

        public TileLineNormal(int left, int top, int right, int bottom, int squareType, Bitmap progressPathDrawRes,
                              Paint tilePaintDefault, Paint tilePaintProgress) {
            super(left, top, right, bottom, squareType, tilePaintDefault, tilePaintProgress);
            this.progressPathBitmap = progressPathDrawRes;
        }

        /*Adding extra configuration*/
        @Override
        public void configureTile(int[] correctPath, int sizeMode, int squarePosition, int orientationMode, int squareListCount) {
            super.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);

            /*Pre-specific configurations*/
            switch (tileOrientation) {

                /*Left to right*/
                case TILE_0_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = left;
                    startY = top + squareApothemVert;
                    endX = right;
                    endY = top + squareApothemVert;
                    break;

                /*Right to left*/
                case TILE_180_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = right;
                    startY = top + squareApothemVert;
                    endX = left;
                    endY = top + squareApothemVert;
                    break;

                /*Bottom to top*/
                case TILE_90_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = left + squareApothemHoriz;
                    startY = bottom;
                    endX = left + squareApothemHoriz;
                    endY = top;
                    break;

                /*Top to bottom*/
                case TILE_270_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = left + squareApothemHoriz;
                    startY = top;
                    endX = left + squareApothemHoriz;
                    endY = bottom;
                    break;
            }

            resetState();
        }

        /* ---- Rendering section ---- */
        @Override
        public void draw(Canvas canvas, int currentProgressPosition) {
            super.draw(canvas, currentProgressPosition);
            drawTile(canvas, tileOrientation, lastTouchedX, lastTouchedY, currentProgressPosition, tilePaintDefault, tilePaintProgress);
        }

        /**
         * Basic component drawing methods
         */
        private void drawTile(Canvas canvas, @TileOrientation int tileOrientation, int lastTouchedX, int lastTouchedY, int currentProgressPosition, Paint tilePaintDefault, Paint tilePaintProgress) {
            /*Base line*/
            canvas.drawLine(startX, startY, endX, endY, tilePaintDefault);
            /*Touched line*/
            if (wasCompleted) {
                canvas.drawLine(startX, startY, endX, endY, tilePaintProgress);
            } else {
                int auxEndX = 0;
                int auxEndY = 0;

                switch (tileOrientation) {
                    case TILE_0_DEGREES:
                    case TILE_180_DEGREES:
                        /*Y axis is fixed*/
                        auxEndX = lastTouchedX;
                        auxEndY = endY;
                        break;

                    case TILE_90_DEGREES:
                    case TILE_270_DEGREES:
                        /*X axis is fixed*/
                        auxEndX = endX;
                        auxEndY = lastTouchedY;
                        break;
                }
                canvas.drawLine(startX, startY, auxEndX, auxEndY, tilePaintProgress);

                /*Progress drawable*/
                if ((lastTouchedX != startX || lastTouchedY != startY) || squarePosition == currentProgressPosition) {
                    Matrix matrix = new Matrix();
                    matrix.postTranslate(auxEndX - (progressPathBitmap.getWidth() / 2), auxEndY - (progressPathBitmap.getHeight() / 2));
                    canvas.drawBitmap(progressPathBitmap, matrix, null);
                }
            }
        }
    }

    protected static class TileLineDot extends TileLine {

        private static final int DOT_RADIUS = 15;
        private Bitmap endPathBitmap;

        public TileLineDot(int left, int top, int right, int bottom, int squareType, Bitmap progressPathDrawRes, Bitmap endPathBitmap,
                           Paint tilePaintDefault, Paint tilePaintProgress) {
            super(left, top, right, bottom, squareType, tilePaintDefault, tilePaintProgress);
            this.progressPathBitmap = progressPathDrawRes;
            this.endPathBitmap = endPathBitmap;
        }

        /*Adding extra configuration*/
        @Override
        public void configureTile(int[] correctPath, int sizeMode, int squarePosition, int orientationMode, int squareListCount) {
            super.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);

            /*Pre-specific configurations*/
            switch (tileOrientation) {

                /*Left to right*/
                case TILE_0_DEGREES:
                    startX = left + squareApothemHoriz;
                    startY = top + squareApothemVert;
                    endX = right;
                    endY = top + squareApothemVert;

                    /*If is tail type, swap values*/
                    if (rectangleType == TYPE_TAIL) {
                        swapLineValues();
                    }
                    break;

                /*Right to left*/
                case TILE_180_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = right - squareApothemHoriz;
                    startY = top + squareApothemVert;
                    endX = left;
                    endY = top + squareApothemVert;

                    /*If is tail type, swap values*/
                    if (rectangleType == TYPE_TAIL) {
                        swapLineValues();
                    }
                    break;

                /*Bottom to top*/
                case TILE_90_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = left + squareApothemHoriz;
                    startY = bottom - squareApothemVert;
                    endX = left + squareApothemHoriz;
                    endY = top - squareApothemVert;

                    /*If is tail type, swap values*/
                    if (rectangleType == TYPE_TAIL) {
                        swapLineValues();
                    }
                    break;

                /*Top to bottom*/
                case TILE_270_DEGREES:
                    /*Extra data left just for possible future logic addition*/
                    startX = left + squareApothemHoriz;
                    startY = top + squareApothemVert;
                    endX = left + squareApothemHoriz;
                    endY = bottom;

                    /*If is tail type, swap values*/
                    if (rectangleType == TYPE_TAIL) {
                        swapLineValues();
                    }
                    break;
            }

            resetState();
        }

        /**
         * Used for swapping values for start and ending positions in 2D
         */
        private void swapLineValues() {
            int auxStartX = startX;
            int auxStartY = startY;

            startX = endX;
            startY = endY;
            endX = auxStartX;
            endY = auxStartY;
        }

        /* ---- Rendering section ---- */
        @Override
        public void draw(Canvas canvas, int currentProgressPosition) {
            super.draw(canvas, currentProgressPosition);
            drawTile(canvas, rectangleType, tileOrientation, lastTouchedX, lastTouchedY, currentProgressPosition, tilePaintDefault, tilePaintProgress);
        }

        /**
         * Basic component drawing methods
         */
        private void drawTile(Canvas canvas, @Type int squareType, @TileOrientation int tileOrientation, int lastTouchedX, int lastTouchedY,
                              int currentProgressPosition, Paint tilePaintDefault, Paint tilePaintProgress) {

            int progressEndX = startX;
            int progressEndY = startY;

            /*Line*/
            canvas.drawLine(startX, startY, endX, endY, (wasCompleted) ? tilePaintProgress : tilePaintDefault);

            /*Dot or Drawable*/
            Paint paintCircle = new Paint();
            paintCircle.setStyle(Paint.Style.FILL);
            paintCircle.setAntiAlias(true);

            switch (squareType){
                case TYPE_HEAD:
                    paintCircle.setColor(tilePaintProgress.getColor());
                    canvas.drawCircle(startX, startY, DOT_RADIUS, paintCircle);
                    break;
                case TYPE_BODY:
                    paintCircle.setColor(tilePaintDefault.getColor());
                    canvas.drawCircle(endX, endY, DOT_RADIUS, paintCircle);
                    break;

                case TYPE_TAIL:
                    Matrix matrix = new Matrix();
                    matrix.postTranslate(endX - (endPathBitmap.getWidth() / 2), endY - (endPathBitmap.getHeight() / 2));
                    canvas.drawBitmap(endPathBitmap, matrix, null);
                    break;
            }

            /*Progress*/
            if (!wasCompleted) {
                switch (tileOrientation) {
                    case TILE_0_DEGREES:
                    case TILE_180_DEGREES:
                        /*Y axis is fixed*/
                        progressEndX = lastTouchedX;
                        progressEndY = endY;
                        break;

                    case TILE_90_DEGREES:
                    case TILE_270_DEGREES:
                        /*X axis is fixed*/
                        progressEndX = endX;
                        progressEndY = lastTouchedY;
                        break;
                }

                canvas.drawLine(startX, startY, progressEndX, progressEndY, tilePaintProgress);

                /*Progress drawable*/
                if ((lastTouchedX != startX || lastTouchedY != startY) || squarePosition == currentProgressPosition) {
                    Matrix matrix = new Matrix();
                    matrix.postTranslate(progressEndX - (progressPathBitmap.getWidth() / 2), progressEndY - (progressPathBitmap.getHeight() / 2));
                    canvas.drawBitmap(progressPathBitmap, matrix, null);
                }
            }
        }
    }

    protected static class TileArc extends TileRender {

        private static final int TOLERANCE_PERCENTAGE = 20; /* 20% */
        private Bitmap progressPathBitmap;

        @IntDef({SIDE_TOP, SIDE_RIGHT, SIDE_BOTTOM, SIDE_LEFT})
        @interface Sides {

        }

        protected static final int SIDE_TOP = 0;
        protected static final int SIDE_RIGHT = 1;
        protected static final int SIDE_BOTTOM = 2;
        protected static final int SIDE_LEFT = 3;

        /*Data*/
        @Sides protected int originSide;
        @Sides protected int endSide;

        /*Coordinates*/
        int startX = 0, startY = 0, endX = 0, endY = 0;


        public TileArc(int left, int top, int right, int bottom, int squareType, Bitmap progressPathBitmap,
                       Paint tilePaintDefault, Paint tilePaintProgress) {
            super(left, top, right, bottom, squareType, tilePaintDefault, tilePaintProgress);
            this.progressPathBitmap = progressPathBitmap;
        }

        @Override
        public void configureTile(int[] correctPath, int sizeMode, int squarePosition, int orientationMode, int squareListCount) {
            super.configureTile(correctPath, sizeMode, squarePosition, orientationMode, squareListCount);

            /*Sides*/
            switch (getTileOrientation()) {
                case TILE_0_DEGREES:
                    /*HorizontalMode default*/
                    originSide = SIDE_RIGHT;
                    endSide = SIDE_BOTTOM;

                    /*Swap if needed*/
                    if (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) {
                        swapOriginEndSides();
                    }
                    break;

                /** 90 degrees doesn't need swap execution
                 * Same origin and end for both touch detector orientations*/
                case TILE_90_DEGREES:
                    originSide = SIDE_TOP;
                    endSide = SIDE_RIGHT;
                    break;

                case TILE_180_DEGREES:
                    /*HorizontalMode default*/
                    originSide = SIDE_TOP;
                    endSide = SIDE_LEFT;

                    /*Swap if needed*/
                    if (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) {
                        swapOriginEndSides();
                    }
                    break;

                /** 270 degrees doesn't need swap execution
                 * Same origin and end for both touch detector orientations*/
                case TILE_270_DEGREES:
                    originSide = SIDE_LEFT;
                    endSide = SIDE_BOTTOM;
                    break;
            }

            /*Start & End coordinates configuration*/
            configureStartEndPoints();

            resetState();
        }

        private void configureStartEndPoints() {
            /*Start*/
            switch (originSide) {
                case SIDE_TOP:
                    startX = left + squareApothemHoriz;
                    startY = top;
                    break;

                case SIDE_RIGHT:
                    startX = right;
                    startY = top + squareApothemVert;
                    break;

                case SIDE_BOTTOM:
                    startX = left + squareApothemHoriz;
                    startY = bottom;
                    break;

                case SIDE_LEFT:
                    startX = left;
                    startY = top + squareApothemVert;
                    break;
            }


            /*End*/
            switch (endSide) {
                case SIDE_TOP:
                    endX = left + squareApothemHoriz;
                    endY = top;
                    break;

                case SIDE_RIGHT:
                    endX = right;
                    endY = top + squareApothemVert;
                    break;

                case SIDE_BOTTOM:
                    endX = left + squareApothemHoriz;
                    endY = bottom;
                    break;

                case SIDE_LEFT:
                    endX = left;
                    endY = top + squareApothemVert;
                    break;
            }
        }

        private void swapOriginEndSides() {
            int auxSide = originSide;

            originSide = endSide;
            endSide = auxSide;
        }

        @Override
        public void resetState() {
            super.resetState();

            lastTouchedX = startX;
            lastTouchedY = startY;
        }

        /* ---- Check state section ---- */

        /*Specific tile complete logic here*/
        @Override
        protected boolean checkCompletedState(int touchX, int touchY, @TileTypes int tileType, @TileOrientation int tileOrientation) {
            return getAdvancePercentage(touchX, touchY) >= (100 - TOLERANCE_PERCENTAGE);
        }

        /* ---- Rendering section ---- */
        public void draw(Canvas canvas, int currentProgressPosition) {
            super.draw(canvas, currentProgressPosition);
            drawTile(canvas, tileType, tileOrientation, lastTouchedX, lastTouchedY, currentProgressPosition, tilePaintDefault, tilePaintProgress); /* TODO check paint stuffs */
        }

        /**
         * Basic component drawing methods
         */
        private void drawTile(Canvas canvas, @TileTypes int tileType, @TileOrientation int tileOrientation, int lastTouchedX, int lastTouchedY,
                              int currentProgressPosition, Paint tilePaintDefault, Paint tilePaintProgress) {
            /*Init data*/
            float newLeft = 0;
            float newRight = 0;
            float newTop = 0;
            float newBottom = 0;
            float startAngle = 0;
            float sweepAngle = 0;

            int tileWidth = getRight() - getLeft();
            int tileHeight = getBottom() - getTop();

            switch (tileOrientation) {
                case TILE_0_DEGREES:
                    newLeft = left + (tileWidth / 2);
                    newRight = right + (tileWidth / 2);
                    newTop = top + (tileHeight / 2);
                    newBottom = bottom + (tileHeight / 2);

                    /*Change the angle sweep based on the view orientation */
                    startAngle = (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) ? 180 : 270;
                    sweepAngle = (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) ? 90 : -90;
                    break;

                case TILE_90_DEGREES:
                    newLeft = left + (tileWidth / 2);
                    newRight = right + (tileWidth / 2);
                    newTop = top - (tileHeight / 2);
                    newBottom = bottom - (tileHeight / 2);

                    /*Same angle sweep for horizontal and vertical touch detector mode*/
                    startAngle = 180;
                    sweepAngle = -90;
                    break;

                case TILE_180_DEGREES:
                    newLeft = left - (tileWidth / 2);
                    newRight = right - (tileWidth / 2);
                    newTop = top - (tileHeight / 2);
                    newBottom = bottom - (tileHeight / 2);

                    /*Change the angle sweep based on the view orientation */
                    startAngle = (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) ? 90 : 0;
                    sweepAngle = (getTouchDetectorOrientationMode() == ORIENTATION_VERTICAL) ? -90 : 90;
                    break;

                case TILE_270_DEGREES:
                    newLeft = left - (tileWidth / 2);
                    newRight = right - (tileWidth / 2);
                    newTop = top + (tileHeight / 2);
                    newBottom = bottom + (tileHeight / 2);

                    /*Same angle sweep for horizontal and vertical touch detector mode*/
                    startAngle = 270;
                    sweepAngle = 90;
                    break;
            }

            RectF rectF = new RectF();
            rectF.set(newLeft, newTop, newRight, newBottom);

            /*Draw base arc*/
            canvas.drawArc(rectF, startAngle, sweepAngle, false, (wasCompleted) ? tilePaintProgress : tilePaintDefault);

            /*Draw Progress*/
            if (!wasCompleted) {
                canvas.drawArc(rectF, startAngle, sweepAngle * (getAdvancePercentage(lastTouchedX, lastTouchedY) / 100), false, tilePaintProgress);

                /*Progress drawable*/
                if ((lastTouchedX != startX || lastTouchedY != startY) || squarePosition == currentProgressPosition) {

                    Coordinate progressCord = getProgressCoordinate(tileWidth, tileHeight, lastTouchedX, lastTouchedY);

                    if (progressCord != null) {
                        Matrix matrix = new Matrix();
                        matrix.postTranslate(progressCord.getX() - (progressPathBitmap.getWidth() / 2), progressCord.getY() - (progressPathBitmap.getHeight() / 2));
                        canvas.drawBitmap(progressPathBitmap, matrix, null);
                    }
                }
            }
        }

        private float getAdvancePercentage(int lastTouchedX, int lastTouchedY) {

            int difference;
            int relevantMiddleSection;
            switch (endSide) {

                case SIDE_TOP: /*Y axis matters*/
                    relevantMiddleSection = top + squareApothemVert;
                    if (lastTouchedY > relevantMiddleSection) {
                        return 0.0f;
                    }
                    difference = relevantMiddleSection - lastTouchedY;
                    return (difference * 100) / squareApothemVert;

                case SIDE_RIGHT: /*X axis matters*/
                    relevantMiddleSection = left + squareApothemHoriz;
                    if (lastTouchedX < relevantMiddleSection) {
                        return 0.0f;
                    }
                    difference = lastTouchedX - relevantMiddleSection;
                    return (difference * 100) / squareApothemHoriz;

                case SIDE_BOTTOM: /*Y axis matters*/
                    relevantMiddleSection = top + squareApothemVert;
                    if (lastTouchedY < relevantMiddleSection) {
                        return 0.0f;
                    }
                    difference = lastTouchedY - relevantMiddleSection;
                    return (difference * 100) / squareApothemVert;

                case SIDE_LEFT: /*X axis matters*/
                    relevantMiddleSection = left + squareApothemHoriz;
                    if (lastTouchedX > relevantMiddleSection) {
                        return 0.0f;
                    }
                    difference = relevantMiddleSection - lastTouchedX;
                    return (difference * 100) / squareApothemHoriz;
            }
            return 0.0f;
        }

        private Coordinate getProgressCoordinate(int tileWidth, int tileHeight, int lastTouchedX, int lastTouchedY) {

            int relevantMiddleSection;
            boolean foundX = false;
            switch (endSide) {
                case SIDE_TOP: /*Y axis matters*/
                    relevantMiddleSection = top + squareApothemVert;
                    if (lastTouchedY > relevantMiddleSection) {
                        return new Coordinate(startX, startY);
                    }
                    foundX = true;
                    break;
                case SIDE_BOTTOM: /*Y axis matters*/
                    relevantMiddleSection = top + squareApothemVert;
                    if (lastTouchedY < relevantMiddleSection) {
                        return new Coordinate(startX, startY);
                    }
                    foundX = true;
                    break;
                case SIDE_LEFT: /*X axis matters*/
                    relevantMiddleSection = left + squareApothemHoriz;
                    if (lastTouchedX > relevantMiddleSection) {
                        return new Coordinate(startX, startY);
                    }
                    foundX = false;
                    break;
                case SIDE_RIGHT: /*X axis matters*/
                    relevantMiddleSection = left + squareApothemHoriz;
                    if (lastTouchedX < relevantMiddleSection) {
                        return new Coordinate(startX, startY);
                    }
                    foundX = false;
                    break;
            }

            int a = (tileHeight) / 2;
            int b = (tileWidth) / 2;

            int x;
            int y;

            int anchor = getTileOrientation();
            switch (anchor) {

                case TILE_0_DEGREES:
                    if (foundX) {
                        /*Found X - Uses lastTouchedY*/
                        y = bottom - lastTouchedY;
                        x = (int) Math.sqrt(((1 - (Math.pow(y, 2) / Math.pow(a, 2))) * Math.pow(b, 2)));
                        return new Coordinate(right - x, lastTouchedY);
                    } else {
                        /*Found Y - Uses lastTouchedX*/
                        x = right - lastTouchedX;
                        y = (int) Math.sqrt(((1 - (Math.pow(x, 2) / Math.pow(b, 2))) * Math.pow(a, 2)));
                        return new Coordinate(lastTouchedX, bottom - y);
                    }

                case TILE_90_DEGREES:
                    if (foundX) {
                        /*Found X - Uses lastTouchedY*/
                        y = lastTouchedY - top;
                        x = (int) Math.sqrt(((1 - (Math.pow(y, 2) / Math.pow(a, 2))) * Math.pow(b, 2)));
                        return new Coordinate(left + x, lastTouchedY);
                    } else {
                        /*Found Y - Uses lastTouchedX*/
                        x = right - lastTouchedX;
                        y = (int) Math.sqrt(((1 - (Math.pow(x, 2) / Math.pow(b, 2))) * Math.pow(a, 2)));
                        return new Coordinate(lastTouchedX, top + y);
                    }

                case TILE_180_DEGREES:
                    if (foundX) {
                        /*Found X - Uses lastTouchedY*/
                        y = lastTouchedY - top;
                        x = (int) Math.sqrt(((1 - (Math.pow(y, 2) / Math.pow(a, 2))) * Math.pow(b, 2)));
                        return new Coordinate(left + x, lastTouchedY);
                    } else {
                        /*Found Y - Uses lastTouchedX*/
                        x = lastTouchedX - left;
                        y = (int) Math.sqrt(((1 - (Math.pow(x, 2) / Math.pow(b, 2))) * Math.pow(a, 2)));
                        return new Coordinate(lastTouchedX, top + y);
                    }

                case TILE_270_DEGREES:
                    if (foundX) {
                        /*Found X - Uses lastTouchedY*/
                        y = bottom - lastTouchedY;
                        x = (int) Math.sqrt(((1 - (Math.pow(y, 2) / Math.pow(a, 2))) * Math.pow(b, 2)));
                        return new Coordinate(left + x, lastTouchedY);
                    } else {
                        /*Found Y - Uses lastTouchedX*/
                        x = lastTouchedX - left;
                        y = (int) Math.sqrt(((1 - (Math.pow(x, 2) / Math.pow(b, 2))) * Math.pow(a, 2)));
                        return new Coordinate(lastTouchedX, bottom - y);
                    }
            }

            return new Coordinate(0, 0);
        }
    }

    public TouchDetectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public TouchDetectorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    /*Use it if needed*/
    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, com.facebook.devices.R.styleable.TouchDetectorView, defStyle, 0);

        sizeMode = typedArray.getInt(com.facebook.devices.R.styleable.TouchDetectorView_size, 6);
        orientationMode = typedArray.getInt(com.facebook.devices.R.styleable.TouchDetectorView_orientation, ORIENTATION_HORIZONTAL);

        tileLineDefaultColor = typedArray.getColor(com.facebook.devices.R.styleable.TouchDetectorView_colorTileLine, Color.WHITE);
        tileLineTouchedColor = typedArray.getColor(com.facebook.devices.R.styleable.TouchDetectorView_colorTileLineTouched, Color.WHITE);
        progressPathDrawable = BitmapFactory.decodeResource(getResources(), typedArray.getResourceId(R.styleable.TouchDetectorView_progressPathDrawable, R.drawable.touch_path));
        endPathDrawable = BitmapFactory.decodeResource(getResources(), typedArray.getResourceId(R.styleable.TouchDetectorView_endPathDrawable, R.drawable.path_end));

        typedArray.recycle();

        /*Background base paint*/
        paintBackground = new Paint();

        /*ItemRender paint*/
        squareGridPaint = new Paint();
        squareGridPaint.setStyle(Paint.Style.STROKE);
        squareGridPaint.setStrokeWidth(1);
        squareGridPaint.setAntiAlias(true);
        squareGridPaint.setColor(ContextCompat.getColor(getContext(), R.color.gray4_M));

        /*Tile Paint Default*/
        tilePaintDefault = new Paint();
        tilePaintDefault.setStyle(Paint.Style.STROKE);
        tilePaintDefault.setStrokeWidth(10);
        tilePaintDefault.setColor(tileLineDefaultColor);
        /*Tile Paint Touched*/
        tilePaintTouched = new Paint();
        tilePaintTouched.setStyle(tilePaintDefault.getStyle());
        tilePaintTouched.setStrokeWidth(tilePaintDefault.getStrokeWidth());
        tilePaintTouched.setColor(tileLineTouchedColor);
    }

    public void initView(@TouchDetectorOrientation int orientationMode, int squaresSize, int[] correctPath) {
        this.orientationMode = orientationMode;
        this.sizeMode = squaresSize;
        this.correctPath = correctPath;

        /*Init touched path*/
        resetBehavior();
    }

    public void setOrientationMode(@TouchDetectorOrientation int orientationMode, int[] correctPath) {
        this.orientationMode = orientationMode;
        this.correctPath = correctPath;
        resetBehavior();
    }

    @TouchDetectorOrientation
    public int getOrientationModeSet() {
        return orientationMode;
    }

    public void setSizeMode(int sizeMode, int[] correctPath) {
        this.sizeMode = sizeMode;
        this.correctPath = correctPath;
        resetBehavior();
    }

    public int getSizeModeSet() {
        return sizeMode;
    }

    public int[] getCorrectPathSet() {
        return correctPath;
    }

    private void clearTouchedPath() {
        touchedPath = new int[correctPath.length];
        for (int i = 0; i < touchedPath.length; i++) {
            touchedPath[i] = -1;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        /*Background paint reset*/
        paintBackground.setShader(new LinearGradient(0, 0, baseWidth, baseHeight,
                ContextCompat.getColor(getContext(), R.color.dark_sky_blue),
                ContextCompat.getColor(getContext(), R.color.light_navy_blue), Shader.TileMode.CLAMP));

        /*Prepare grid squares*/
        setupGrid(sizeMode, correctPath, orientationMode);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*Draw background base*/
        drawBackground(canvas, paintBackground);

        /*Draw Grid*/
        drawGrid(canvas, squareGridPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        /*Avoid functionality if not enabled*/
        if (!isEnabled()){
            return true;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (nextCorrectPathIndex < correctPath.length) {
                    int squareNumberObjective = correctPath[nextCorrectPathIndex];
                    if (tileRenderList.get(squareNumberObjective).setLastTouchedCoordinates((int) event.getX(), (int) event.getY())) {
                        addHistoryPath(squareNumberObjective);

                        if (nextCorrectPathIndex < correctPath.length - 1) {
                            nextCorrectPathIndex++;
                        }
                    }
                }
                break;

            case MotionEvent.ACTION_UP:
                if (touchedPath.length == getHistoryTouchedSize()) {
                    comparePaths();
                } else {
                    callBackOnPatternDetectionFinishes(TouchDetectorViewCallback.FINISHED_PATTERN_FAILED_INCOMPLETE);
                }
                resetBehavior();
                break;
        }
        invalidate();
        return true;
    }

    private void addHistoryPath(int position) {
        for (int i = 0; i < touchedPath.length; i++) {
            if (touchedPath[i] == -1) {
                touchedPath[i] = position;
                break;
            }
        }
    }

    private int getHistoryTouchedSize() {
        int count = 0;
        for (int i = 0; i < touchedPath.length; i++) {
            if (touchedPath[i] != -1) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }

    private void comparePaths() {
        /*Compare data*/
        callBackOnPatternDetectionFinishes((Arrays.equals(correctPath, touchedPath)) ?
                TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS :
                TouchDetectorViewCallback.FINISHED_PATTERN_FAILED);
    }

    private void callBackOnPatternDetectionFinishes(@TouchDetectorViewCallback.TouchDetectorCallbackResult int result) {
        if (callback != null) {
            callback.onPatternDetectionFinishes(result);
        }
    }

    private void setupGrid(int sizeMode, int[] correctPath, @OrientationModel.OrientationStateMode int orientationMode) {
        int viewWidth = baseWidth;
        int viewHeight = baseHeight;

        tileRenderList.clear();

        int squareNumber = sizeMode;

        int squareWidth = viewWidth / squareNumber;
        int squareHeight = viewHeight / squareNumber;

        int lastTop = 0;
        int lastBottom = squareHeight;

        for (int row = 0; row < squareNumber; row++) {

            int lastLeft = 0;
            int lastRight = squareWidth;
            for (int column = 0; column < squareNumber; column++) {

                /** Save square data */
                int squarePosition = (row * squareNumber) + column;
                tileRenderList.add(TileGeneratorHelper.generateTile(
                        new TileRender(lastLeft, lastTop, lastRight, lastBottom, getSquareType(row, column, squareNumber), tilePaintDefault, tilePaintTouched),
                        correctPath, sizeMode, squarePosition, orientationMode, (int) Math.pow(squareNumber, 2), progressPathDrawable, endPathDrawable));

                /* Update last left/right */
                lastLeft += squareWidth;
                lastRight += squareWidth;
            }

            /* Update last top/bottom */
            lastTop += squareHeight;
            lastBottom += squareHeight;
        }
    }

    @TileRender.Type
    private int getSquareType(int row, int column, int squareCountSide) {
        int squarePosition = (row * squareCountSide) + column;
        if (correctPath[0] == squarePosition) {
            /*HEAD*/
            return TYPE_HEAD;
        } else {
            if (correctPath[correctPath.length - 1] == squarePosition) {
                /*TAIL*/
                return TileRender.TYPE_TAIL;
            } else {
                /*BODY*/
                return TileRender.TYPE_BODY;
            }
        }
    }

    private void drawBackground(Canvas canvas, Paint backgroundPaint) {
        canvas.drawPaint(backgroundPaint);
    }

    private void drawGrid(Canvas canvas, Paint squareFillPaint) {
        for (int i = 0; i < correctPath.length; i++) {
            TileRender tileRender = tileRenderList.get(correctPath[i]);

            /** Drawing grid */
            canvas.drawRect(tileRender.getLeft(), tileRender.getTop(), tileRender.getRight(), tileRender.getBottom(), squareFillPaint);

            /** Drawing specific tile */
            tileRender.draw(canvas, correctPath[nextCorrectPathIndex]);
        }
    }


    public void resetBehavior() {
        for (TileRender tileRender : tileRenderList) {
            tileRender.resetState();
        }
        nextCorrectPathIndex = 0;
        clearTouchedPath();
        invalidate();
    }
}
