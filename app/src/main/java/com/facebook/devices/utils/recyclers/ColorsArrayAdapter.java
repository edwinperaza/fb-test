package com.facebook.devices.utils.recyclers;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.db.entity.ColorEntity;

import java.util.List;

public class ColorsArrayAdapter extends ArrayAdapter<ColorEntity> {

    private final LayoutInflater inflater;
    private final Context context;
    private final List<ColorEntity> items;
    private final int resource;

    public ColorsArrayAdapter(@NonNull Context context, int resource, @NonNull List<ColorEntity> items) {
        super(context, resource, items);
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.resource = resource;
        this.items = items;

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
            @NonNull ViewGroup parent) {
        return createItemView(position, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, parent);
    }

    private View createItemView(int position, ViewGroup parent){
        final View view = inflater.inflate(resource, parent, false);

        ImageView colorCode = view.findViewById(R.id.iv_color);
        TextView colorName = view.findViewById(R.id.tv_color_name);

        ColorEntity colorEntity = items.get(position);

        colorCode.setBackgroundColor(Color.parseColor(colorEntity.getHexCode()));
        colorName.setText(colorEntity.getName());

        return view;
    }
}
