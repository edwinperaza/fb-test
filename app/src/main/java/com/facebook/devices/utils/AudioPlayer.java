package com.facebook.devices.utils;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;

import java.util.Arrays;

public class AudioPlayer {

    public interface AudioPlayerListener {

        /**
         * Real time data
         */
        void onPlayingDataAvailable(short[] data);

        /**
         * Data played successfully
         */
        void onPlaybackCompleted();
    }

    private int sampleRate;
    private int bufferSize;
    private InnerAudioPlayerThread innerAudioPlayerThread;

    public AudioPlayer() {
    }

    public AudioPlayer(int sampleRate) {
        this.sampleRate = sampleRate;
        this.bufferSize = loadBufferSize(sampleRate);
    }

    public void setSampleRate(int sampleRate) {
        this.sampleRate = sampleRate;
        this.bufferSize = loadBufferSize(sampleRate);
    }

    private int loadBufferSize(int sampleRate) {
        int bufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = sampleRate * 2;
        }
        return bufferSize;
    }

    public void stopPlaying() {
        if (innerAudioPlayerThread != null && innerAudioPlayerThread.isRunning()){
            innerAudioPlayerThread.stopPlaying();
        }
    }

    public void startPlaying(AudioPlayerListener listener, short[] bufferData) {
        /*Check sample rate and running state*/
        if (sampleRate == 0 ||
                (innerAudioPlayerThread != null && innerAudioPlayerThread.isRunning())) {
            return;
        }

        innerAudioPlayerThread = new InnerAudioPlayerThread(sampleRate, bufferSize, bufferData, listener);
        innerAudioPlayerThread.start();
    }

    public boolean isRunning() {
        return (innerAudioPlayerThread != null && innerAudioPlayerThread.isRunning());
    }

    public class InnerAudioPlayerThread extends Thread {

        private AudioPlayerListener listener;
        private int sampleRate;
        private int bufferSize;
        private short[] bufferData;
        private boolean isRunning = false;

        public InnerAudioPlayerThread(int sampleRate, int bufferSize, short[] bufferData, AudioPlayerListener listener) {
            this.sampleRate = sampleRate;
            this.bufferSize = bufferSize;
            this.bufferData = bufferData;
            this.listener = listener;
        }

        public short[] getBufferData(){
            return bufferData;
        }

        public boolean isRunning() {
            return isRunning;
        }

        public void stopPlaying() {
            isRunning = false;
        }

        @Override
        public void run() {

            if (sampleRate == 0 || listener == null) {
                return;
            }

            isRunning = true;

            AudioTrack audioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    sampleRate,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSize,
                    AudioTrack.MODE_STREAM);

            audioTrack.play();

            int size = bufferSize / 2;
            int start = 0;

            while (start <= bufferData.length && isRunning) {
                short[] audioBuffer = Arrays.copyOfRange(bufferData, start, start + size);
                start += size;

                audioTrack.write(audioBuffer, 0, audioBuffer.length);

                listener.onPlayingDataAvailable(audioBuffer);
            }

            audioTrack.stop();

            /*Release resource*/
            if (audioTrack != null) {
                audioTrack.release();
            }

            /*Check if startPlayback is completed*/
            if (start >= bufferData.length) {
                listener.onPlaybackCompleted();
            }

            isRunning = false;
        }
    }
}
