package com.facebook.devices.utils.augmentedreality.rendering;

import android.animation.ObjectAnimator;
import android.support.annotation.Nullable;
import android.view.animation.LinearInterpolator;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.QuaternionEvaluator;
import com.google.ar.sceneform.math.Vector3;

public class RotatingNode extends Node {

    @Nullable private ObjectAnimator modelAnimation = null;
    /*Data*/
    private float degreesPerSecond = 90.0f;
    private float rotationSpeedMultiplier = 0.3f;

    @Override
    public void onActivate() {
        startAnimation();
    }

    @Override
    public void onDeactivate() {
        stopAnimation();
    }

    private long getAnimationDuration() {
        return (long) (1000 * 360 / (degreesPerSecond * rotationSpeedMultiplier));
    }

    private void startAnimation() {
        if (modelAnimation != null) {
            return;
        }
        modelAnimation = createAnimator();
        modelAnimation.setTarget(this);
        modelAnimation.setDuration(getAnimationDuration());
        modelAnimation.start();
    }

    private void stopAnimation() {
        if (modelAnimation == null) {
            return;
        }
        modelAnimation.cancel();
        modelAnimation = null;
    }

    /**
     * Returns an ObjectAnimator that makes this node rotate.
     */
    private ObjectAnimator createAnimator() {

        /*Setting Orientations to animate circle*/
        ObjectAnimator orbitAnimation = new ObjectAnimator();

        orbitAnimation.setObjectValues(
                Quaternion.axisAngle(new Vector3(0.0f, 0.0f, 1.0f), 0),
                Quaternion.axisAngle(new Vector3(0.0f, 0.0f, 1.0f), 120),
                Quaternion.axisAngle(new Vector3(0.0f, 0.0f, 1.0f), 240),
                Quaternion.axisAngle(new Vector3(0.0f, 0.0f, 1.0f), 360));

        /*Setting localRotation property needed*/
        orbitAnimation.setPropertyName("localRotation");

        /*QuaternionEvaluator needed to process orientations*/
        orbitAnimation.setEvaluator(new QuaternionEvaluator());

        /*Setting infinite loop*/
        orbitAnimation.setRepeatCount(ObjectAnimator.INFINITE);
        orbitAnimation.setRepeatMode(ObjectAnimator.RESTART);
        orbitAnimation.setInterpolator(new LinearInterpolator());
        orbitAnimation.setAutoCancel(true);

        return orbitAnimation;
    }
}
