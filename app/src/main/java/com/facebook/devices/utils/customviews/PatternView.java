package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.devices.R;

import java.util.ArrayList;
import java.util.List;

public class PatternView extends View {

    /*Data*/
    private int squareNumberSide;
    private int baseWidth;
    private int baseHeight;
    private List<Square> squareList = new ArrayList<>();

    private class Square {

        /*Coordinates*/
        private float left, top, right, bottom;
        private int color;

        public Square(float left, float top, float right, float bottom, int color) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.color = Color.rgb(color, color, color);
        }

        public float getLeft() {
            return left;
        }

        public float getTop() {
            return top;
        }

        public float getRight() {
            return right;
        }

        public float getBottom() {
            return bottom;
        }

        public void setColor(int color) {
            this.color = color;
        }

        public int getColor() {
            return color;
        }
    }

    /*Paint*/
    private Paint paintBackground;
    private Paint squareGridPaint;

    public PatternView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public PatternView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    /*Use it if needed*/
    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PatternView, defStyle, 0);

        squareNumberSide = typedArray.getInt(R.styleable.PatternView_squareNumberSide, 3);
        typedArray.recycle();

        /*Background base paint*/
        paintBackground = new Paint();

        /*ItemRender paint*/
        squareGridPaint = new Paint();
        squareGridPaint.setStyle(Paint.Style.FILL);
        squareGridPaint.setAntiAlias(true);
    }

    public void initView(int squareNumberSide) {
        this.squareNumberSide = squareNumberSide;
        this.invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        /*Prepare grid squares*/
        setupGrid(squareNumberSide);
    }

    private void setupGrid(int squareNumberSide) {

        int viewWidth = baseWidth;
        int viewHeight = baseHeight;

        squareList.clear();

        int squareNumber = squareNumberSide;

        float squareWidth = viewWidth / squareNumber;
        float squareHeight = viewHeight / squareNumber;

        float decrement = 255 / (float) Math.pow(squareNumber, 2);
        float lastColor = 255;

        float lastTop = 0;
        float lastBottom = squareHeight;
        for (int row = 0; row < squareNumber; row++) {

            float lastLeft = 0;
            float lastRight = squareWidth;
            for (int column = 0; column < squareNumber; column++) {

                /** Save square data */
                squareList.add(new Square(lastLeft, lastTop, lastRight, lastBottom, (int) lastColor));

                /* Update last left/right + color*/
                lastLeft += squareWidth;
                lastRight += squareWidth;
                lastColor -= decrement;
            }

            /* Update last top/bottom */
            lastTop += squareHeight;
            lastBottom += squareHeight;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*Background*/
        drawBackground(canvas, paintBackground);
        /*Grid*/
        drawGrid(canvas, squareGridPaint);
    }

    private void drawBackground(Canvas canvas, Paint backgroundPaint) {
        canvas.drawPaint(backgroundPaint);
    }

    private void drawGrid(Canvas canvas, Paint squareGridPaint) {
        for (int i = 0; i < squareList.size(); i++) {
            Square square = squareList.get(i);

            /** Drawing square based on positions */
            squareGridPaint.setColor(square.getColor());
            canvas.drawRect(square.getLeft(), square.getTop(), square.getRight(), square.getBottom(), squareGridPaint);
        }
    }
}
