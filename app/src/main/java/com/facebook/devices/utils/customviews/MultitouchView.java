package com.facebook.devices.utils.customviews;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class MultitouchView extends View {

    private static int MIN_MULTITOUCH_FINGERS = 2;

    private int squareColor = 0;
    private int squareColorTouched = 0;
    private int squareColorCompleted = 0;
    private int lineColor = 0;
    private int backgroundColor = 0;
    private Drawable backgroundDrawable;
    private MultiTouchDetectorViewCallback listener;

    /*Preset components data*/
    private int defaultPaddingTop = 0;
    private int paddingTopSecondary = 0;
    private int lineMarginBottom = 0;
    private int roundedCorners = 0;
    private int squareWidthHeight = 0;
    private int rectangleVerticalWidth = 0;
    private int rectangleVerticalHeight = 0;
    private int rectangleHorizontalWidth = 0;
    private int rectangleHorizontalHeight = 0;
    private int objectsMargin = 0;
    private int lineWidth = 0;
    private int lineHeight = 0;
    private int alphaCompleted = 102; /*40% alpha*/

    /*UI data*/
    private int baseWidth;
    private int baseHeight;
    private ItemRender actualItemDraggingState = null;
    private List<ItemRender> itemsList = new ArrayList<>();
    private Line line;
    private boolean dualFingersWereDetected = false;
    private boolean runningMode = true;

    /*Paints*/
    private Paint squarePaint = new Paint();
    private Paint linePaint = new Paint();

    public interface MultiTouchDetectorViewCallback {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({FINISHED_SUCCESS, FINISHED_DROPPED})
        @interface MultiTouchDetectorCallbackResult {
        }

        int FINISHED_SUCCESS = 1;
        int FINISHED_DROPPED = 0;

        void onMultitouchReplies(@MultiTouchDetectorCallbackResult int resultMode);
    }

    private static class ItemRender {

        private Context context;

        private static class Coordinates {
            protected float left, originalLeft, top, originalTop, right, originalRight, bottom, originalBottom;

            protected void reset() {
                left = originalLeft;
                top = originalTop;
                right = originalRight;
                bottom = originalBottom;
            }

            protected float getCenterX() {
                return right - left;
            }

            protected float getCenterY() {
                return bottom - top;
            }

            protected void copyFrom(Coordinates coordinates) {
                left = coordinates.left;
                right = coordinates.right;
                top = coordinates.top;
                bottom = coordinates.bottom;
                originalLeft = coordinates.originalLeft;
                originalRight = coordinates.originalRight;
                originalTop = coordinates.originalTop;
                originalBottom = coordinates.originalBottom;
            }
        }

        /*States*/
        private boolean isTouchable = true;
        private boolean isTouched = false;
        private boolean isCompleted = false;

        /*Properties*/
        private String name = "";
        private Paint itemPaint = new Paint();
        private int squareColor = 0;
        private int squareColorTouched = 0;
        private int squareColorCompleted = 0;
        private int roundedCorners = 0;
        private int alphaCompleted = 255;

        /*References*/
        private float canvasMaxHeight = 0;
        private float canvasMaxWidth = 0;
        private float pivotPointX = 0;
        private float pivotPointY = 0;

        /*Coordinates*/
        private Coordinates coordinates = new Coordinates();

        public ItemRender(Context context, String name, float canvasMaxHeight, float canvasMaxWidth, float left, float top, float right, float bottom, int squareColor,
                          int squareColorTouched, int squareColorCompleted, int roundedCorners, int alphaCompleted) {
            this.context = context;
            /*Canvas data*/
            this.canvasMaxHeight = canvasMaxHeight;
            this.canvasMaxWidth = canvasMaxWidth;
            /*ItemRender data*/
            this.coordinates.originalLeft = this.coordinates.left = left;
            this.coordinates.originalTop = this.coordinates.top = top;
            this.coordinates.originalRight = this.coordinates.right = right;
            this.coordinates.originalBottom = this.coordinates.bottom = bottom;
            /*Properties*/
            this.name = name;
            this.squareColor = squareColor;
            this.squareColorTouched = squareColorTouched;
            this.squareColorCompleted = squareColorCompleted;
            this.roundedCorners = roundedCorners;
            this.alphaCompleted = alphaCompleted;
        }

        public float getLeft() {
            return coordinates.left;
        }

        public float getTop() {
            return coordinates.top;
        }

        public float getRight() {
            return coordinates.right;
        }

        public float getBottom() {
            return coordinates.bottom;
        }


        public void moveTo(float newPivotX, float newPivotY, List<ItemRender> collisionItems) {

            List<ItemRender> itemsCollisionFiltered = new ArrayList<>();
            for (ItemRender item : collisionItems) {
                if (item != this) {
                    itemsCollisionFiltered.add(item);
                }
            }

            /*Process pivoting*/
            if (pivotPointX == 0 && pivotPointY == 0) {
                /*Set pivot point first time*/
                pivotPointX = newPivotX;
                pivotPointY = newPivotY;
            } else {
                /*If already set (no 0), move based on pivot point and update it*/
                float distanceLeft = pivotPointX - coordinates.left;
                float distanceRight = coordinates.right - pivotPointX;
                float distanceTop = pivotPointY - coordinates.top;
                float distanceBottom = coordinates.bottom - pivotPointY;

                /*Left - Right*/
                if ((newPivotX - distanceLeft >= 0) && (newPivotX + distanceRight <= canvasMaxWidth)) { /*Inside canvas threshold*/

                    /*Create future coordinates*/
                    Coordinates auxCoords = new Coordinates();
                    auxCoords.copyFrom(coordinates);
                    auxCoords.left = newPivotX - distanceLeft;
                    auxCoords.right = newPivotX + distanceRight;

                    boolean collisionDetected = false;
                    for (ItemRender item : itemsCollisionFiltered) {
                        if (collideWith(auxCoords, item)) {
                            collisionDetected = true;
                            break;
                        }
                    }

                    if (!collisionDetected) {
                        coordinates.left = auxCoords.left;
                        coordinates.right = auxCoords.right;
                        pivotPointX = newPivotX;
                    }
                }

                /*Top - Bottom*/
                if ((newPivotY - distanceTop >= 0) && (newPivotY + distanceBottom <= canvasMaxHeight)) { /*Inside canvas threshold*/

                    /*Create future coordinates*/
                    Coordinates auxCoords = new Coordinates();
                    auxCoords.copyFrom(coordinates);
                    auxCoords.top = newPivotY - distanceTop;
                    auxCoords.bottom = newPivotY + distanceBottom;

                    boolean collisionDetected = false;
                    for (ItemRender item : itemsCollisionFiltered) {
                        if (collideWith(auxCoords, item)) {
                            collisionDetected = true;
                            break;
                        }
                    }

                    if (!collisionDetected) {
                        coordinates.top = auxCoords.top;
                        coordinates.bottom = auxCoords.bottom;
                        pivotPointY = newPivotY;
                    }
                }
            }
        }

        private boolean collideInXWith(Coordinates coordinates, ItemRender itemRender) {
            return (isBetween(coordinates.left, itemRender.coordinates.left, itemRender.coordinates.right) || isBetween(coordinates.right, itemRender.coordinates.left, itemRender.coordinates.right))
                    || (isBetween(itemRender.coordinates.left, coordinates.left, coordinates.right) || isBetween(itemRender.coordinates.right, coordinates.left, coordinates.right));
        }

        private boolean collideInYWith(Coordinates coordinates, ItemRender itemRender) {
            return (isBetween(coordinates.top, itemRender.coordinates.top, itemRender.coordinates.bottom) || isBetween(coordinates.bottom, itemRender.coordinates.top, itemRender.coordinates.bottom))
                    || (isBetween(itemRender.coordinates.top, coordinates.top, coordinates.bottom) || isBetween(itemRender.coordinates.bottom, coordinates.top, coordinates.bottom));
        }

        public boolean collideWith(Coordinates coordinates, ItemRender itemRender) {
            return !itemRender.isCompleted() && collideInXWith(coordinates, itemRender) && collideInYWith(coordinates, itemRender);
        }

        public boolean collideWith(Line line) {
            for (int x = (int) line.getStartX(); x <= (int) line.getStopX(); x++) {
                if (coordinatesInside(x, line.startY)) {
                    return true;
                }
            }
            return false;
        }

        public boolean coordinatesInside(float x, float y) {
            return (x >= coordinates.left && x <= coordinates.right) && (y >= coordinates.top && y <= coordinates.bottom);
        }


        /**
         * Calculate point between 2 points
         *
         * @param a      value to process
         * @param bStart start value
         * @param bEnd   end value
         */
        private boolean isBetween(float a, float bStart, float bEnd) {
            return a >= bStart && a <= bEnd;
        }

        public void setTouchable(boolean isTouchable) {
            this.isTouchable = isTouchable;
        }

        public boolean isTouchable() {
            return isTouchable;
        }

        public void setTouched(boolean isBeingTouched) {
            isTouched = isBeingTouched;
        }

        public void render(Canvas canvas) {
            /*Configure paint*/
            itemPaint.setColor((isCompleted) ? squareColorCompleted : (isTouched) ? squareColorTouched : squareColor);
            itemPaint.setShadowLayer(5f, 0f, 4f, context.getColor(R.color.black));
            itemPaint.setAlpha((isCompleted)? alphaCompleted : 255);
            /*Draw*/
            canvas.drawRoundRect(getLeft(), getTop(), getRight(), getBottom(), roundedCorners, roundedCorners, itemPaint);
        }

        public boolean isCompleted() {
            return isCompleted;
        }

        public void setCompleted(boolean isCompleted) {
            this.isCompleted = isCompleted;
            this.isTouched = false;
        }

        /**
         * Reset to original coordinates
         */
        public void reset() {
            coordinates.reset();
            pivotPointX = 0;
            pivotPointY = 0;
            isTouched = false;
        }
    }

    private static class Line {
        private float startX, startY, stopX, stopY;

        public Line(float startX, float startY, float stopX, float stopY) {
            this.startX = startX;
            this.startY = startY;
            this.stopX = stopX;
            this.stopY = stopY;
        }

        public float getStartX() {
            return startX;
        }

        public float getStartY() {
            return startY;
        }

        public float getStopX() {
            return stopX;
        }

        public float getStopY() {
            return stopY;
        }
    }

    public MultitouchView(Context context) {
        super(context);
    }

    public MultitouchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public MultitouchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    private void init(AttributeSet attrs, int defStyle) {
        final TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MultitouchView, defStyle, 0);

        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        /*Colors*/
        squareColor = typedArray.getColor(R.styleable.MultitouchView_squareColor, Color.GREEN);
        squareColorTouched = typedArray.getColor(R.styleable.MultitouchView_squareColorTouched, Color.BLUE);
        squareColorCompleted = typedArray.getColor(R.styleable.MultitouchView_squareColorCompleted, Color.GRAY);
        lineColor = typedArray.getColor(R.styleable.MultitouchView_lineColor, Color.MAGENTA);
        backgroundColor = typedArray.getColor(R.styleable.MultitouchView_backgroundColor, 0);
        backgroundDrawable = ContextCompat.getDrawable(getContext(), typedArray.getResourceId(R.styleable.MultitouchView_backgroundDrawable, -1));
        alphaCompleted = typedArray.getInt(R.styleable.MultitouchView_alphaCompleted, alphaCompleted);

        /*Dimensions*/
        defaultPaddingTop = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_defaultPaddingTop, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_padding_top));
        paddingTopSecondary = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_paddingTopSecondary, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_padding_top_secondary));
        lineMarginBottom = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_lineMarginBottom, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_line_margin_bottom));
        roundedCorners = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_roundedCorners, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_rounded_corners));

        squareWidthHeight = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_squareWidthHeight, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_square_side_size));
        rectangleVerticalHeight = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_rectangleVerticalHeight, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_rectangle_vertical_height));
        rectangleVerticalWidth = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_rectangleVerticalWidth, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_rectangle_vertical_width));
        rectangleHorizontalHeight = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_rectangleHorizontalHeight, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_rectangle_horizontal_height));
        rectangleHorizontalWidth = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_rectangleHorizontalWidth, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_rectangle_horizontal_width));
        lineWidth = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_lineWidth, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_line_width));
        lineHeight = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_lineHeight, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_line_height));

        objectsMargin = typedArray.getDimensionPixelSize(R.styleable.MultitouchView_objectsMargin, (int) getContext().getResources().getDimension(R.dimen.multitouch_v1_items_margin));

        typedArray.recycle();

        /* ItemRender Paint */
        squarePaint.setColor(squareColor);
        squarePaint.setStyle(Paint.Style.FILL);
        squarePaint.setStrokeWidth(3);
        squarePaint.setAntiAlias(true);
        /* Line Paint */
        linePaint.setColor(lineColor);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(5);
        linePaint.setAntiAlias(true);
    }

    public void setListener(MultiTouchDetectorViewCallback listener) {
        this.listener = listener;
    }

    public MultiTouchDetectorViewCallback getListener(){
        return listener;
    }

    /**
     * Used for custom data in running time
     * - The view is reset when it's applied
     */
    public void setUpView() {
        setUpView(0);
    }

    public void setUpView(int minFingersSupported) {

        if (minFingersSupported != 0) {
            this.MIN_MULTITOUCH_FINGERS = minFingersSupported;
        }

        resetItemsNotCompleted();
    }

    private void onMultitouchReplies(@MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult int resultMode) {
        if (listener != null) {
            listener.onMultitouchReplies(resultMode);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        baseWidth = getMeasuredWidth();
        baseHeight = getMeasuredHeight();

        /*Setup itemRender position*/
        setupElements();
    }

    /*Items setup*/
    private void setupElements() {

        /*Base*/
        float left;
        float top;
        float right;
        float bottom;

        /*Rectangle Horizontal*/
        left = (baseWidth / 2) - (rectangleHorizontalWidth / 2);
        top = paddingTopSecondary;
        right = left + rectangleHorizontalWidth;
        bottom = top + rectangleHorizontalHeight;
        ItemRender rectangleHorizontal = new ItemRender(getContext(), "RectangleHorizontal", baseHeight, baseWidth, left, top, right, bottom, squareColor, squareColorTouched, squareColorCompleted, roundedCorners, alphaCompleted);
        itemsList.add(rectangleHorizontal);

        /*TileRender*/
        left = rectangleHorizontal.getLeft() - objectsMargin - squareWidthHeight;
        top = defaultPaddingTop;
        right = left + squareWidthHeight;
        bottom = top + squareWidthHeight;
        itemsList.add(new ItemRender(getContext(), "TileRender", baseHeight, baseWidth, left, top, right, bottom, squareColor, squareColorTouched, squareColorCompleted, roundedCorners, alphaCompleted));

        /*Rectangle Vertical*/
        left = rectangleHorizontal.getRight() + objectsMargin;
        top = defaultPaddingTop;
        right = left + rectangleVerticalWidth;
        bottom = top + rectangleVerticalHeight;
        itemsList.add(new ItemRender(getContext(), "RectangleVertical", baseHeight, baseWidth, left, top, right, bottom, squareColor, squareColorTouched, squareColorCompleted, roundedCorners, alphaCompleted));

        /*Line*/
        float startX = (baseWidth / 2) - (lineWidth / 2);
        float startY = baseHeight - lineMarginBottom - lineHeight;
        float stopX = (baseWidth / 2) + (lineWidth / 2);
        float stopY = baseHeight - lineMarginBottom;
        line = new Line(startX, startY, stopX, stopY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*Draw background*/
        drawBackground(canvas);

        /*Draw line*/
        drawLine(canvas, line, linePaint);

        /*Draw itemRender*/
        drawParallelograms(canvas, itemsList, squarePaint);
    }

    private void drawBackground(Canvas canvas) {
        /*Draw*/
        if (backgroundColor != 0) {
            Paint backgroundPaint = new Paint();
            backgroundPaint.setColor(backgroundColor);
            canvas.drawPaint(backgroundPaint);
        } else {
            if (backgroundDrawable != null) {
                backgroundDrawable.setBounds(0, 0, baseWidth, baseHeight);
                backgroundDrawable.draw(canvas);
            }
        }
    }

    private void drawParallelograms(Canvas canvas, List<ItemRender> itemsList, Paint squarePaint) {
        /*Render items*/
        for (ItemRender item : itemsList) {
            item.render(canvas);
        }
    }

    private void drawLine(Canvas canvas, Line line, Paint linePaint) {
        /*Draw line*/
        canvas.drawLine(line.getStartX(), line.getStartY(), line.getStopX(), line.getStopY(), linePaint);
    }

    private boolean areInsideSquare(ItemRender itemRender, MotionEvent event) {
        /*Data load*/
        boolean areInside = false;
        if (event.getPointerCount() >= MIN_MULTITOUCH_FINGERS) {
            for (int i = 0; i < event.getPointerCount(); i++) {
                int pointerId = event.getPointerId(i);

                float fingerX = event.getX(event.findPointerIndex(pointerId));
                float fingerY = event.getY(event.findPointerIndex(pointerId));

                areInside = (itemRender.coordinatesInside(fingerX, fingerY)) ? true : areInside;
            }
        }
        return areInside;
    }

    private void moveItem(ItemRender itemRender, float newPivotX, float newPivotY) {
        /* Update data*/
        itemRender.moveTo(newPivotX, newPivotY, itemsList);
        /* Check new location */
        checkSquarePosition(itemRender, line);
    }

    private void checkSquarePosition(ItemRender itemRender, Line line) {
        if (itemRender.collideWith(line) && runningMode) {

            /*Change state*/
            itemRender.setTouchable(false); /*Disable to not be dragged again*/
            itemRender.setCompleted(true);

            /*Check all items*/
            if (areAllItemsCompleted(itemsList)) {
                runningMode = false;
                onMultitouchReplies(MultiTouchDetectorViewCallback.FINISHED_SUCCESS);
                return;
            }
        }
    }

    private boolean areAllItemsCompleted(List<ItemRender> itemsList) {
        for (ItemRender item : itemsList) {
            if (!item.isCompleted()) {
                return false;
            }
        }
        return true;
    }


    private void onFailure() {
        runningMode = false;
        setSquareTouchedState(itemsList, false);
        onMultitouchReplies(MultiTouchDetectorViewCallback.FINISHED_DROPPED);
    }

    private void setSquareTouchedState(ItemRender itemRender, boolean isTouched) {
        itemRender.setTouched(isTouched);
    }

    private void setSquareTouchedState(List<ItemRender> itemsRender, boolean isTouched) {
        for (ItemRender item : itemsRender) {
            item.setTouched(isTouched);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!isEnabled()) {
            return true;
        }

        if (runningMode) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_MOVE:

                    if (actualItemDraggingState == null) {
                        for (ItemRender itemRender : itemsList) {
                            if (areInsideSquare(itemRender, event) && itemRender.isTouchable()) {
                                actualItemDraggingState = itemRender;
                                /*Touch state*/
                                if (!dualFingersWereDetected) {
                                    dualFingersWereDetected = true;
                                    setSquareTouchedState(actualItemDraggingState, true);
                                }
                                break;
                            }
                        }
                    }

                    if (actualItemDraggingState != null && !actualItemDraggingState.isCompleted()) {
                        int pointerIdOne = event.getPointerId(0);
                        float fingerOneX = event.getX(event.findPointerIndex(pointerIdOne));
                        float fingerOneY = event.getY(event.findPointerIndex(pointerIdOne));

                        moveItem(actualItemDraggingState, fingerOneX, fingerOneY);
                    }

                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    if (dualFingersWereDetected && runningMode
                            && actualItemDraggingState != null && !actualItemDraggingState.isCompleted()) { /*One of fingers were lost & not released by code*/
                        onFailure();
                    }
                    actualItemDraggingState = null;
                    break;

                case MotionEvent.ACTION_UP:
                    break;
            }
        }
        invalidate();
        return true;
    }

    public void resetItemsNotCompleted() {
        for (ItemRender item : itemsList) {
            if (!item.isCompleted()) item.reset();
        }
        runningMode = true;
        dualFingersWereDetected = false;
        invalidate();
    }

    public boolean areItemsNotCompletedReset(){
        for (ItemRender item : itemsList) {
            if (!item.isCompleted() &&
                    (item.coordinates.top != item.coordinates.originalTop ||
                    item.coordinates.bottom != item.coordinates.originalBottom ||
                    item.coordinates.left != item.coordinates.originalLeft ||
                    item.coordinates.right != item.coordinates.originalRight ||
                    item.pivotPointX != 0 || item.pivotPointY != 0 || item.isTouched)){
                return false;
            }
        }

        return (!runningMode || dualFingersWereDetected)? false : true;
    }
}
