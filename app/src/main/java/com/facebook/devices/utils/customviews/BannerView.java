package com.facebook.devices.utils.customviews;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.devices.R;

public class BannerView {

    /*Animations*/
    private final int showDialogAnimationRes = R.anim.translate_top_center;
    private final int hideDialogAnimationRes = R.anim.translate_center_top;

    private RelativeLayout mainContainer;
    private Context context;
    private BannerCV currentBanner;

    public BannerView(Activity activity) {
        this.context = activity;
        this.mainContainer = createMainContainer(activity);
    }

    private RelativeLayout createMainContainer(Activity activity) {
        FrameLayout viewGroup = activity.findViewById(android.R.id.content);
        /*Layout creation*/
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout mainContainer = new RelativeLayout(activity);
        mainContainer.setLayoutParams(lp);

        viewGroup.addView(mainContainer);
        return mainContainer;
    }

    /* User available methods */
    public void renderNewBanner(BannerCV newBanner) {
        /*Layout params setup*/
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        newBanner.setLayoutParams(lp);

        /*Animations & listener*/
        Animation animationIn = AnimationUtils.loadAnimation(context, showDialogAnimationRes);
        Animation animationOut = AnimationUtils.loadAnimation(context, hideDialogAnimationRes);

        if (isBannerHidden()) {
            if (currentBanner != null) {
                mainContainer.removeView(currentBanner);
                currentBanner = null;
            }
            mainContainer.addView(newBanner);
            currentBanner = newBanner;
            currentBanner.setVisibility(View.VISIBLE);
            currentBanner.startAnimation(animationIn);
            return;
        }

        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (currentBanner != null) {
                    mainContainer.removeView(currentBanner);
                    currentBanner = null;
                }
                mainContainer.addView(newBanner);
                currentBanner = newBanner;
                currentBanner.setVisibility(View.VISIBLE);
                currentBanner.startAnimation(animationIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        currentBanner.startAnimation(animationOut);
    }

    public void showBanner() {
        if (currentBanner != null && currentBanner.getVisibility() != View.VISIBLE) {
            Animation animationIn = AnimationUtils.loadAnimation(context, showDialogAnimationRes);
            currentBanner.setVisibility(View.VISIBLE);
            currentBanner.startAnimation(animationIn);
        }
    }

    public void showBannerNoAnimated() {
        if (currentBanner != null && currentBanner.getVisibility() != View.VISIBLE) {
            currentBanner.setVisibility(View.VISIBLE);
        }
    }

    public void hideBanner() {
        if (currentBanner != null && currentBanner.getVisibility() != View.GONE) {
            Animation animationOut = AnimationUtils.loadAnimation(context, hideDialogAnimationRes);
            animationOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    currentBanner.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            currentBanner.startAnimation(animationOut);
        }
    }

    public void hideBannerNoAnimated() {
        if (currentBanner != null && currentBanner.getVisibility() != View.GONE) {
            currentBanner.setVisibility(View.GONE);
        }
    }

    public boolean isBannerHidden() {
        return currentBanner == null || currentBanner.getVisibility() == View.GONE;
    }

    /*Extra developer methods*/
    public void resizeComponentsForLandscape() {
        if (currentBanner != null) {
            currentBanner.setMinimumWidth((int) context.getResources().getDimension(R.dimen.dialog_top_width_landscape));

            /*Create layout params*/
            RelativeLayout.LayoutParams bannerParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dialog_top_width_landscape),
                    (int) context.getResources().getDimension(R.dimen.dialog_top_height_landscape));
            bannerParams.setMargins(0, (int) context.getResources().getDimension(R.dimen.dialog_card_top_margin), 0, 0);

            currentBanner.setLayoutParams(bannerParams);
            currentBanner.findViewById(R.id.message_container).setLayoutParams(
                    new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dialog_top_width_landscape),
                            ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    public void resizeComponentsForPortrait() {
        if (currentBanner != null) {
            currentBanner.setMinimumWidth((int) context.getResources().getDimension(R.dimen.dialog_top_width_portrait));

            /*Create layout params*/
            RelativeLayout.LayoutParams bannerParams = new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dialog_top_width_portrait),
                    (int) context.getResources().getDimension(R.dimen.dialog_top_height_portrait));
            bannerParams.setMargins(0, (int) context.getResources().getDimension(R.dimen.dialog_card_top_margin), 0, 0);

            currentBanner.setLayoutParams(bannerParams);
            currentBanner.findViewById(R.id.message_container).setLayoutParams(
                    new RelativeLayout.LayoutParams((int) context.getResources().getDimension(R.dimen.dialog_top_width_portrait),
                            ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    /* Banner reference methods */
    public View getBannerContainer(){
        return currentBanner.getContainer();
    }

    public String getBannerTextOptionOne(){
        return currentBanner.getTextOptionOne();
    }

    public String getBannerTextOptionTwo(){
        return currentBanner.getTextOptionTwo();
    }

    public String getBannerTextOptionThree(){
        return currentBanner.getTextOptionThree();
    }

    public TextView getBannerOptionOneButton() {
        return currentBanner.getOptionOneButton();
    }

    public TextView getBannerOptionTwoButton() {
        return  currentBanner.getOptionTwoButton();
    }

    public TextView getBannerOptionThreeButton() {
        return  currentBanner.getOptionThreeButton();
    }

    /* Builder */
    public static class BannerBuilder {

        private BannerCV bannerCV;
        private Context context;

        public BannerBuilder(Context context, @LayoutRes int viewRes) {
            this.context = context;
            bannerCV = new BannerCV(context);
            bannerCV.refreshInternalContainer(LayoutInflater.from(context).inflate(viewRes, null));
            preSetData();
        }

        public BannerBuilder(Context context, View view) {
            this.context = context;
            bannerCV = new BannerCV(context);
            bannerCV.refreshInternalContainer(view);
            preSetData();
        }

        private void preSetData() {
            bannerCV.setCollapsingEnabled(false);
            bannerCV.setVisibility(View.GONE);
        }

        public BannerBuilder setCollapsingEnabled(boolean isEnabled) {
            bannerCV.setCollapsingEnabled(isEnabled);
            return this;
        }

        public BannerBuilder setOptionTextPositive(String text) {
            bannerCV.setOptionTextOne(text);
            return this;
        }

        public BannerBuilder setOptionTextNegative(String text) {
            bannerCV.setOptionTextTwo(text);
            return this;
        }

        public BannerBuilder setOptionTextNeutral(String text) {
            bannerCV.setOptionTextThree(text);
            return this;
        }

        public BannerBuilder setOptionTextPositive(@StringRes int textRes) {
            bannerCV.setOptionTextOne(context.getString(textRes));
            return this;
        }

        public BannerBuilder setOptionTextNegative(@StringRes int textRes) {
            bannerCV.setOptionTextTwo(context.getString(textRes));
            return this;
        }

        public BannerBuilder setOptionTextNeutral(@StringRes int textRes) {
            bannerCV.setOptionTextThree(context.getString(textRes));
            return this;
        }

        public BannerBuilder setOptionPositiveListener(@NonNull BannerCV.OnOptionOneListener listener) {
            bannerCV.setOptionOneListener(listener);
            return this;
        }

        public BannerBuilder setOptionNegativeListener(@NonNull BannerCV.OnOptionTwoListener listener) {
            bannerCV.setOptionTwoListener(listener);
            return this;
        }

        public BannerBuilder setOptionNeutralListener(@NonNull BannerCV.OnOptionThreeListener listener) {
            bannerCV.setOptionThreeListener(listener);
            return this;
        }

        public BannerCV build() {
            return bannerCV;
        }
    }

    /* Banner Custom view */
    private static class BannerCV extends LinearLayout {

        private boolean collapsingEnabled = false;

        private CardView cardViewBanner;
        private FrameLayout container;
        private TextView tvOptOne;
        private TextView tvOptTwo;
        private TextView tvOptThree;
        private ImageView ivHide;
        private LinearLayout smallBannerLayout;
        private LinearLayout messageContainer;

        public interface OnOptionOneListener {
            void onAction();
        }

        public interface OnOptionTwoListener {
            void onAction();
        }

        public interface OnOptionThreeListener {
            void onAction();
        }

        public BannerCV(Context context) {
            super(context);
            init(context, null, 0);
        }

        public BannerCV(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
            init(context, attrs, 0);
        }

        public BannerCV(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            init(context, attrs, defStyleAttr);
        }

        private void init(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            inflate(context, R.layout.banner_cv_layout, this);

            /*Design Properties*/
            setPadding(0, (int) context.getResources().getDimension(R.dimen.dialog_card_top_margin), 0, 0);

            cardViewBanner = findViewById(R.id.cv_big_banner);

            container = findViewById(R.id.container);

            messageContainer = findViewById(R.id.message_container);

            smallBannerLayout = findViewById(R.id.ll_small_banner);
            smallBannerLayout.setOnClickListener(v -> expandedMode());

            ivHide = findViewById(R.id.iv_hide);
            ivHide.setOnClickListener(v -> hidedMode());

            tvOptOne = findViewById(R.id.tv_opt_1);
            tvOptTwo = findViewById(R.id.tv_opt_2);
            tvOptThree = findViewById(R.id.tv_opt_3);

            loadAttrs(context, attrs, defStyleAttr);
        }

        private void loadAttrs(Context context, AttributeSet attrs, int defStyleAttr) {
            final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BannerCV, defStyleAttr, 0);

            /*Options*/
            if (!TextUtils.isEmpty(typedArray.getString(R.styleable.BannerCV_optionOne))) {
                tvOptOne.setText(typedArray.getString(R.styleable.BannerCV_optionOne));
            }
            if (!TextUtils.isEmpty(typedArray.getString(R.styleable.BannerCV_optionTwo))) {
                tvOptTwo.setText(typedArray.getString(R.styleable.BannerCV_optionTwo));
            }
            if (!TextUtils.isEmpty(typedArray.getString(R.styleable.BannerCV_optionThree))) {
                tvOptThree.setText(typedArray.getString(R.styleable.BannerCV_optionThree));
            }

            /*Collapsing*/
            collapsingEnabled = typedArray.getBoolean(R.styleable.BannerCV_collapsingEnabled, false);
            setCollapsingEnabled(collapsingEnabled);

            /*Container*/
            int resId = typedArray.getResourceId(R.styleable.BannerCV_internalLayout, 0);
            if (resId != 0) {
                container.addView(LayoutInflater.from(context).inflate(resId, null));
            }

            typedArray.recycle();
        }

        private void expandedMode() {
            cardViewBanner.setRadius(getContext().getResources().getDimension(R.dimen.dialog_corner_radius));
            messageContainer.setVisibility(VISIBLE);
            smallBannerLayout.setVisibility(GONE);
        }

        private void hidedMode() {
            cardViewBanner.setRadius(getContext().getResources().getDimension(R.dimen.dialog_top_small_mode_corner_radius));
            messageContainer.setVisibility(GONE);
            smallBannerLayout.setVisibility(VISIBLE);
        }

        /*Developers section*/
        public void inflateInternalContainer(View view) {
            container.addView(view);
        }

        public void refreshInternalContainer(View view) {
            /*Reset views*/
            container.removeAllViews();
            container.addView(view);
        }

        public void resetCollapsingState() {
            expandedMode();
        }

        public void inflateInternalContainer(Context context, int resource) {
            container.addView(LayoutInflater.from(context).inflate(resource, null));
        }

        public void refreshInternalContainer(Context context, int resource) {
            container.removeAllViews();
            container.addView(LayoutInflater.from(context).inflate(resource, null));
        }

        public void setCollapsingEnabled(boolean enabled) {
            collapsingEnabled = enabled;
            ivHide.setVisibility(enabled ? VISIBLE : GONE);
        }

        public void setOptionTextOne(String optionOne) {
            if (optionOne != null && !TextUtils.isEmpty(optionOne)) {
                tvOptOne.setVisibility(VISIBLE);
                tvOptOne.setText(optionOne);
            } else {
                tvOptOne.setVisibility(GONE);
            }
        }

        public void setOptionTextTwo(String optionTwo) {
            if (optionTwo != null && !TextUtils.isEmpty(optionTwo)) {
                tvOptTwo.setVisibility(VISIBLE);
                tvOptTwo.setText(optionTwo);
            } else {
                tvOptTwo.setVisibility(GONE);
            }
        }

        public void setOptionTextThree(String optionThree) {
            if (optionThree != null && !TextUtils.isEmpty(optionThree)) {
                tvOptThree.setVisibility(VISIBLE);
                tvOptThree.setText(optionThree);
            } else {
                tvOptThree.setVisibility(GONE);
            }
        }

        public void setOptionOneListener(@Nullable BannerCV.OnOptionOneListener listener) {
            tvOptOne.setOnClickListener(v -> listener.onAction());
        }

        public void setOptionTwoListener(@Nullable BannerCV.OnOptionTwoListener listener) {
            tvOptTwo.setOnClickListener(v -> listener.onAction());
        }

        public void setOptionThreeListener(@Nullable BannerCV.OnOptionThreeListener listener) {
            tvOptThree.setOnClickListener(v -> listener.onAction());
        }

        public View getContainer() {
            return container;
        }

        public void setContainerVisible(boolean isVisible) {
            container.setVisibility((isVisible) ? View.VISIBLE : View.GONE);
        }

        public TextView getOptionOneButton() {
            return tvOptOne;
        }

        public TextView getOptionTwoButton() {
            return tvOptTwo;
        }

        public TextView getOptionThreeButton() {
            return tvOptThree;
        }

        public String getTextOptionOne() {
            return tvOptOne.getText().toString();
        }

        public String getTextOptionTwo() {
            return tvOptTwo.getText().toString();
        }

        public String getTextOptionThree() {
            return tvOptThree.getText().toString();
        }

    }
}
