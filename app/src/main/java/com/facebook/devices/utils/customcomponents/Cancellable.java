package com.facebook.devices.utils.customcomponents;


public interface Cancellable {
    void removeCallbacks();
}
