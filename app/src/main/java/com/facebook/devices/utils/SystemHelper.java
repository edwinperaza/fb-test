package com.facebook.devices.utils;

import android.app.ActivityManager;
import android.content.Context;

public class SystemHelper {

    public static float getOpenGLSystemVersion(Context context){
        return Float.parseFloat(((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getDeviceConfigurationInfo().getGlEsVersion());
    }
}
