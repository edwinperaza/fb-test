package com.facebook.devices.utils.camera;

import android.app.Activity;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;

public class CameraUtils {

    /**
     * The resource is taken by the system, so any other app could use it until {@link #STOP_CAMERA} is sent as broadcast
     * */
    private static final String START_CAMERA = "com.facebook.aloha.camera.START_ALOHA_CAMERA";
    /**
     *  Release resource to be used with our app, after use it remember to call {@link #START_CAMERA} so the resource is locked again
     * */
    private static final String STOP_CAMERA = "com.facebook.aloha.camera.STOP_ALOHA_CAMERA";

    private Activity activity;

    public CameraUtils(Activity activity){
        this.activity = activity;
    }

    public void sendCameraUnlockRequest() {
        activity.sendBroadcast(new Intent(STOP_CAMERA));
    }

    public void sendCameraLockRequest() {
        activity.sendBroadcast(new Intent(START_CAMERA));
    }

    /* Camera Management */
    public CameraCharacteristics getCameraCharacteristics(CameraManager cameraManager, String cameraId) throws CameraAccessException {
        return cameraManager.getCameraCharacteristics(cameraId);
    }

    public StreamConfigurationMap getCameraStreamConfigurationMap(CameraManager cameraManager, String cameraId) throws CameraAccessException {
        return getCameraCharacteristics(cameraManager, cameraId).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
    }

    public int getSensorOrientation(CameraManager cameraManager, String cameraId) throws CameraAccessException {
        return getCameraCharacteristics(cameraManager, cameraId).get(CameraCharacteristics.SENSOR_ORIENTATION);
    }

    public Integer getCameraLensFacing(CameraManager cameraManager, String cameraId) throws CameraAccessException {
        return getCameraCharacteristics(cameraManager, cameraId).get(CameraCharacteristics.LENS_FACING);
    }
}
