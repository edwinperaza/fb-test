package com.facebook.devices.utils.customviews;

import android.support.annotation.IntDef;

public class BannerEventBase {

    @IntDef({OPTION_ONE, OPTION_TWO, OPTION_THREE})
    @interface Option{
    }

    public static final int OPTION_ONE = 0;
    public static final int OPTION_TWO = 1;
    public static final int OPTION_THREE = 2;

    @Option
    public int optionSelected;

    public BannerEventBase(@Option int option){
        optionSelected = option;
    }
}
