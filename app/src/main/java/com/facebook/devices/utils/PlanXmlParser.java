package com.facebook.devices.utils;

import android.util.Xml;

import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlanXmlParser {

    private static final String TYPE_APK_TEST = "APKTest";

    private static final String TAG_ACTION = "action";
    private static final String TAG_ARG = "arg";
    private static final String TAG_ARG_TYPE = "type";
    private static final String TAG_ARG_NAME = "name";
    private static final String TAG_ARG_READ_ONLY = "readOnly";
    private static final String TAG_DESCRIPTION = "description";

    private static final String TAG_STANDALONE = "standalone";
    private static final String TAG_STANDALONE_ACTION_REF = "action-ref";
    private static final String TAG_STANDALONE_DESCRIPTION = "description";
    private static final String TAG_STANDALONE_ATTR_CODE = "code";
    private static final String TAG_STANDALONE_ATTR_NAME = "name";
    private static final String TAG_STANDALONE_ATTR_ICON = "icon";

    private static final String TAG_ATTR_ID = "id";
    private static final String TAG_ATTR_TYPE = "type";
    private static final String TAG_ATTR_NAME = "name";
    private static final String TAG_ATTR_ACTIVITY = "activity";
    private static final String TAG_ATTR_TEST_TYPE_ICON = "icon";

    private static final String TAG_KEY_START = "${";
    private static final String TAG_KEY_END = "}";
    private static final String EMPTY_STRING = "";
    private static final String DEFAULT_VALUE_SEPARATOR = ":";
    private static final long EMPTY_TIME = 0;

    public static boolean getPlanTests(TestDatabase testDatabase, Reader reader, Map<String, String> mapValues) {
        return parser(testDatabase, reader, mapValues);
    }

    private static boolean parser(TestDatabase testDatabase, Reader reader, Map<String, String> mapValues) {
        boolean result = false;
        try {

            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(reader);
            parser.nextTag();
            result = parseTestDefinitionXML(testDatabase, parser, mapValues);

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static boolean parseTestDefinitionXML(TestDatabase testDatabase, XmlPullParser parser, Map<String, String> mapValues) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        int order = 0;
        /*Temp Data*/
        TestInfoEntity tempTest = null;
        GroupTestsEntity groupTest = null;
        List<ArgumentEntity> tempArgs = new ArrayList<>();
        String tempDescription = EMPTY_STRING;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {

                case XmlPullParser.START_DOCUMENT:
                    /*Nothing to do here*/
                    break;

                case XmlPullParser.START_TAG:
                    String name = parser.getName();
                    switch (name) {
                        case TAG_ACTION:
                            if (parser.getAttributeValue(null, TAG_ATTR_TYPE).equals(TYPE_APK_TEST)) {
                                /*Create test info*/
                                tempTest = new TestInfoEntity(
                                        parser.getAttributeValue(null, TAG_ATTR_ID),
                                        parser.getAttributeValue(null, TAG_ATTR_ACTIVITY),
                                        parser.getAttributeValue(null, TAG_ATTR_NAME),
                                        EMPTY_STRING,
                                        parser.getAttributeValue(null, TAG_ATTR_TEST_TYPE_ICON).toUpperCase(),
                                        TestInfoEntity.IDLE,
                                        0,
                                        EMPTY_TIME);
                            }
                            break;

                        case TAG_DESCRIPTION:
                            /*Add description*/
                            if (tempTest != null && parser.next() == XmlPullParser.TEXT) {
                                tempDescription = parser.getText();
                            }
                            break;

                        case TAG_ARG:
                            /*Add argument*/
                            String typeArg = parser.getAttributeValue(null, TAG_ARG_TYPE);
                            String nameArg = parser.getAttributeValue(null, TAG_ARG_NAME);
                            Boolean readOnlyArg = Boolean.valueOf(parser.getAttributeValue(null, TAG_ARG_READ_ONLY));

                            if (tempTest != null && parser.next() == XmlPullParser.TEXT) {
                                tempArgs.add(
                                        getArgument(
                                                typeArg,
                                                nameArg,
                                                readOnlyArg,
                                                parser.getText(),
                                                tempTest.id,
                                                mapValues));
                            }
                            break;
                        case TAG_STANDALONE:
                            groupTest = new GroupTestsEntity(
                                    parser.getAttributeValue(null, TAG_STANDALONE_ATTR_ICON),
                                    parser.getAttributeValue(null, TAG_STANDALONE_ATTR_CODE));
                            break;

                        case TAG_STANDALONE_ACTION_REF:
                            /*Add ACTION REF*/
                            String id = parser.getAttributeValue(null, TAG_ATTR_ID);
                            TestInfoEntity testInfoEntity = testDatabase.testInfoDao().getTestInfo(id);
                            if (testInfoEntity != null && groupTest != null) {
                                testInfoEntity.groupId = groupTest.getGroupType();
                                testInfoEntity.orderId = order++;
                                testDatabase.testInfoDao().updateTestInfo(testInfoEntity);
                            }
                            break;
                    }
                    break;

                case XmlPullParser.END_TAG:
                    if (parser.getName().equals(TAG_ACTION) && tempTest != null) {
                        tempTest.setDescription(tempDescription);
                        testDatabase.testInfoDao().insert(tempTest);

                        testDatabase.argumentDao().insert(tempArgs);

                        /*Clear temps*/
                        tempTest = null;
                        tempArgs = new ArrayList<>();
                        tempDescription = EMPTY_STRING;
                    } else if (parser.getName().equals(TAG_STANDALONE) && groupTest != null) {

                        testDatabase.groupTestDao().insert(groupTest);

                        /*Clear temps*/
                        groupTest = null;
                    }
                    break;
            }
            eventType = parser.next();
        }
        return true;
    }

    public static ArgumentEntity getArgument(String type, String name, Boolean readOnlyArg, String initialArgument, String testId, Map<String, String> mapValues) {
        int finalLength = initialArgument.length();
        StringBuilder value = new StringBuilder();
        for (int i = 0; i < finalLength; i++) {
            if (i < (finalLength - 3)) {
                if (TAG_KEY_START.equals(initialArgument.substring(i, i + 2))) {
                    //Evaluate Scape key
                    if (TAG_KEY_START.equals(initialArgument.substring(i + 2, i + 4))) {

                        value.append(initialArgument.charAt(i + 2));
                        value.append(initialArgument.charAt(i + 3));
                        i = i + 3;

                    } else {

                        int index = i + 2;
                        boolean nextValue = true;
                        StringBuilder keyValue = new StringBuilder();

                        while (nextValue) {
                            if (TAG_KEY_END.charAt(0) != initialArgument.charAt(index)) {
                                keyValue.append(initialArgument.charAt(index));
                                index++;
                            } else {
                                nextValue = false;
                                i = index;
                            }
                        }
                        String[] keyValueArray = keyValue.toString().split(DEFAULT_VALUE_SEPARATOR);
                        String valueFromMap = mapValues.get(String.valueOf(keyValueArray[0]));

                        if (valueFromMap == null) {
                            if (keyValueArray.length >= 2) {
                                StringBuilder valueToAssign = new StringBuilder();
                                for (int j = 1; j < keyValueArray.length; j++) {
                                    valueToAssign.append(keyValueArray[j]);
                                    if (j < keyValueArray.length - 1) {
                                        valueToAssign.append(DEFAULT_VALUE_SEPARATOR);
                                    }
                                }
                                value.append(valueToAssign);
                            } else {
                                value.append(EMPTY_STRING);
                            }
                        } else {
                            value.append(valueFromMap);
                        }
                    }
                } else {
                    value.append(initialArgument.charAt(i));
                }
            } else {
                value.append(initialArgument.charAt(i));
            }
        }
        return new ArgumentEntity(name, type, String.valueOf(value), testId, readOnlyArg);
    }
}
