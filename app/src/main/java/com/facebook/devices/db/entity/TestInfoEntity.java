package com.facebook.devices.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

@Entity
public class TestInfoEntity {

    @IntDef({RUNNING, IDLE, FAIL, PASS})
    @interface State {
    }

    public static final int RUNNING = 1;
    public static final int IDLE = 0;
    public static final int FAIL = -1;
    public static final int PASS = 2;

    @PrimaryKey
    @NonNull
    public String id;

    @ColumnInfo(name = "canonical_name")
    public String canonicalName;

    public String name;

    public String type;

    public int status = IDLE;

    public String description;

    public String groupId;

    public int orderId;

    public long time;

    public TestInfoEntity() {
    }

    @Ignore
    public TestInfoEntity(@NonNull String id,
            @NonNull String canonicalName,
            @NonNull String name,
            @NonNull String description,
            @NonNull String type,
            int status,
            int orderId,
            long time) {
        this.id = id;
        this.canonicalName = canonicalName;
        this.name = name;
        this.description = description;
        this.type = type;
        this.status = status;
        this.orderId = orderId;
        this.time = time;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setCanonicalName(@NonNull String canonicalName) {
        this.canonicalName = canonicalName;
    }

    @NonNull
    public String getCanonicalName() {
        return canonicalName;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setStatus(@State int status) {
        this.status = status;
    }

    @State
    public int getStatus() {
        return status;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}