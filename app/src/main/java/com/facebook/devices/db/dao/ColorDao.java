package com.facebook.devices.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.facebook.devices.db.entity.ColorEntity;

import java.util.List;

@Dao
public interface ColorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ColorEntity colorEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<ColorEntity> colorEntityList);

    @Query("DELETE FROM colorentity")
    void deleteAll();

    @Query("SELECT * from colorentity")
    List<ColorEntity> getAllColors();

}
