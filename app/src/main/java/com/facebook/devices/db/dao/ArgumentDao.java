package com.facebook.devices.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.facebook.devices.db.entity.ArgumentEntity;

import java.util.List;

@Dao
public interface ArgumentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ArgumentEntity argumentEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<ArgumentEntity> argumentEntities);

    @Update
    void updateArgument(ArgumentEntity argumentEntity);

    @Query("DELETE FROM argumentEntity")
    void deleteAll();

    @Query("SELECT * from argumentEntity")
    List<ArgumentEntity> getAllArguments();

    @Query("SELECT * from argumentEntity WHERE testId IN (:testIds) ")
    List<ArgumentEntity> getCurrentArguments(List<String> testIds);

    @Query("SELECT * from argumentEntity WHERE testId = :id")
    List<ArgumentEntity> getArgumentsByTest(String id);
}
