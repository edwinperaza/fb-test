package com.facebook.devices.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;

@Entity(primaryKeys = {"name", "testId"}, foreignKeys = {
        @ForeignKey(entity = TestInfoEntity.class,
                    parentColumns = "id",
                    childColumns = "testId",
                    onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = "testId")})
public class ArgumentEntity {

    @NonNull
    public String name;

    public String type;

    public String value;

    @ColumnInfo(name = "read_only")
    public Boolean readOnly;

    @NonNull
    public String testId;

    public ArgumentEntity(@NonNull String name, String type, String value, @NonNull String testId, Boolean readOnly) {
        this.name = name;
        this.type = type;
        this.value = value;
        this.testId = testId;
        this.readOnly = readOnly;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    @NonNull
    public String getValue() {
        return value;
    }

    public void setValue(@NonNull String value) {
        this.value = value;
    }
}
