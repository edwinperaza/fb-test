package com.facebook.devices.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.facebook.devices.db.entity.GroupTestsEntity;

import java.util.List;

@Dao
public interface GroupTestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(GroupTestsEntity groupTestsEntity);

    @Query("DELETE FROM grouptestsentity")
    void deleteAll();

    @Query("SELECT * from grouptestsentity")
    List<GroupTestsEntity> getAllGroupTests();

    @Query("SELECT * from grouptestsentity WHERE groupType = :groupType")
    GroupTestsEntity getGroupTestById(String groupType);
}
