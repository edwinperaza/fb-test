package com.facebook.devices.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class ColorEntity {

    @PrimaryKey
    public long id;

    public String hexCode;

    public String name;

    public ColorEntity() {
    }

    @Ignore
    public ColorEntity(long id, @NonNull String hexCode, @NonNull String name) {
        this.id = id;
        this.hexCode = hexCode;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHexCode() {
        return hexCode;
    }

    public String getName() {
        return name;
    }
}
