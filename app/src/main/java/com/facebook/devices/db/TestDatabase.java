package com.facebook.devices.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.facebook.devices.HwtpApplication;
import com.facebook.devices.db.dao.ArgumentDao;
import com.facebook.devices.db.dao.ColorDao;
import com.facebook.devices.db.dao.GroupTestDao;
import com.facebook.devices.db.dao.TestInfoDao;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

@Database(entities = {ArgumentEntity.class, TestInfoEntity.class, GroupTestsEntity.class, ColorEntity.class}, version = 1)
public abstract class TestDatabase extends RoomDatabase {

    public abstract ArgumentDao argumentDao();

    public abstract TestInfoDao testInfoDao();

    public abstract GroupTestDao groupTestDao();

    public abstract ColorDao colorDao();

    private volatile static TestDatabase INSTANCE;

    public static TestDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TestDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TestDatabase.class,
                            "hwtp_database")
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Executor executor = HwtpApplication.getDatabaseIO();
                                    executor.execute(() -> {
                                        getInstance().runInTransaction(() -> getInstance().colorDao().insert(loadColors()));
                                    });
                                }
                            })
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static TestDatabase getInstance() {
        return INSTANCE;
    }

    public static List<ColorEntity> loadColors() {
        List<ColorEntity> colorEntityList = new ArrayList<>();
        colorEntityList.add(new ColorEntity(1, "#FF0000", "red"));
        colorEntityList.add(new ColorEntity(2, "#00FF00", "green"));
        colorEntityList.add(new ColorEntity(3, "#0000FF", "blue"));
        colorEntityList.add(new ColorEntity(4, "#767A79", "gray"));
        colorEntityList.add(new ColorEntity(5, "#FFFFFF", "white"));
        colorEntityList.add(new ColorEntity(6, "#000000", "black"));
        colorEntityList.add(new ColorEntity(7, "#FFFF00", "Yellow"));
        colorEntityList.add(new ColorEntity(8, "#00FFFF", "Light Blue"));
        colorEntityList.add(new ColorEntity(9, "#FF00FF", "Violet"));

        return colorEntityList;
    }
}
