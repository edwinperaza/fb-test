package com.facebook.devices.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.facebook.devices.db.entity.TestInfoEntity;

import java.util.List;

@Dao
public interface TestInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TestInfoEntity testInfoEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<TestInfoEntity> argumentEntities);

    @Update
    void updateTestInfo(TestInfoEntity... testInfoEntities);

    @Query("DELETE FROM testinfoentity")
    void deleteAll();

    @Query("SELECT * from testinfoentity WHERE groupId IS NOT NULL ORDER BY orderId")
    List<TestInfoEntity> getAllTestInfo();

    @Query("SELECT * from testinfoentity WHERE groupId IS NOT NULL AND status != 0 ORDER BY orderId")
    List<TestInfoEntity> getAllTestInfoExecutedList();

    @Query("SELECT * from testinfoentity WHERE id = :id")
    TestInfoEntity getTestInfo(String id);

    @Query("UPDATE testinfoentity SET status = :value")
    void updateAllTestStatus(int value);

    @Query("UPDATE testinfoentity SET time = 0")
    void resetAllTestTimeSpent();

    @Query("UPDATE testinfoentity SET status = :value WHERE id = :id")
    void updateStatusByTestId(String id, int value);

    @Query("SELECT * from testinfoentity WHERE groupId IS NOT NULL AND status == :status ORDER BY orderId")
    List<TestInfoEntity> findTestsByStatus(int status);
}
