package com.facebook.devices.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class GroupTestsEntity {

    @PrimaryKey
    @NonNull
    public String groupType;

    @ColumnInfo(name = "icon_resource")
    public String iconResource;

    public GroupTestsEntity(@NonNull String iconResource,@NonNull String groupType) {
        this.iconResource = iconResource;
        this.groupType = groupType;
    }

    public void setGroupType(@NonNull String groupType) {
        this.groupType = groupType;
    }

    @NonNull
    public String getGroupType() {
        return groupType;
    }

    public void setIconResource(@NonNull String iconResource) {
        this.iconResource = iconResource;
    }

    @NonNull
    public String getIconResource() {
        return iconResource;
    }

}