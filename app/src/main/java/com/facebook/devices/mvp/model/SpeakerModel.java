package com.facebook.devices.mvp.model;


import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringDef;

import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class SpeakerModel extends ServiceModel {

    /*States*/
    @IntDef({QUESTION_STATE_1, QUESTION_STATE_2})
    public @interface QuestionState {


    }

    /*Add more if needed*/
    public static final int QUESTION_STATE_1 = 0;
    public static final int QUESTION_STATE_2 = 1;

    /*Channel out*/
    @StringDef({MONO, STEREO, LEFT, RIGHT})
    public @interface ChannelOutput {


    }

    public static final String MONO = "mono";
    public static final String STEREO = "stereo";
    public static final String LEFT = "left";
    public static final String RIGHT = "right";

    private static final int SAMPLE_RATE = 44100; /*hz*/
    private int PCM_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private int channelConfig;

    private final String output;

    private int bufferSize = 0;

    /* Dynamic Use */
    private int questionState = QUESTION_STATE_1;
    private float frequency = 0;
    private float angle = 0;
    private SoundThread soundThread;
    private AudioCallback callback;
    private VolumeManager volumeManager;

    /**
     * @param frequency expressed in Hz.
     * @param output
     */
    public SpeakerModel(int frequency, @ChannelOutput String output, VolumeManager volumeManager) {
        this.frequency = frequency;
        this.output = output.toLowerCase();
        this.volumeManager = volumeManager;

        switch (output) {
            case MONO:
                channelConfig = AudioFormat.CHANNEL_OUT_MONO;
                break;
            case STEREO:
                channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
            case LEFT:
                channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
            case RIGHT:
                channelConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
            default:
                channelConfig = AudioFormat.CHANNEL_OUT_MONO;
        }

        this.bufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, channelConfig, PCM_ENCODING) / 4;
    }

    public void setDefaultVolume(){
        volumeManager.setDefaultMediaVolume();
    }

    public interface AudioCallback {
        void onAvailableData(short[] data);
    }

    public void setCallback(AudioCallback callback) {
        this.callback = callback;
    }

    public void play() {
        soundThread = new SoundThread();
        soundThread.start();
    }

    public void stop() {
        if (soundThread != null) {
            soundThread.kill();
            soundThread = null;
        }
    }

    public void setQuestionState(@QuestionState int questionState) {
        this.questionState = questionState;
    }

    public int getQuestionState() {
        return questionState;
    }

    public int getSampleRate() {
        return SAMPLE_RATE;
    }

    public int getPCMEncoding() {
        return PCM_ENCODING;
    }

    public int getChannelConfig() {
        return channelConfig;
    }

    private class SoundThread extends Thread {

        private AudioTrack audioTrack;
        private volatile boolean isRunning = false;

        public void kill() {
            isRunning = false;
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void run() {

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

            try {
                /* Construct audio track */
                audioTrack = new AudioTrack.Builder()
                        .setTransferMode(AudioTrack.MODE_STREAM)
                        .setAudioFormat(new AudioFormat.Builder()
                                .setEncoding(getPCMEncoding())
                                .setSampleRate(getSampleRate())
                                .setChannelMask(getChannelConfig())
                                .build())
                        .setBufferSizeInBytes(bufferSize)
                        .build();


                audioTrack.play();

                isRunning = true;
                if (audioTrack.getState() == AudioTrack.STATE_INITIALIZED) {
                    while (isRunning) {

                        short[] samples = generateSample();

                        /* Play */
                        audioTrack.write(samples, 0, samples.length);

                        if (callback != null) {
                            callback.onAvailableData(samples);
                        }
                    }

                } else {
                    // Fail ?
                }
            } catch (Exception e) {
                // fail ?
            } finally {
                isRunning = false;

                if (audioTrack != null) {
                    audioTrack.stop();
                    audioTrack.release();
                    audioTrack = null;
                }
            }
        }

        private short[] generateSample() {
            float angularFrequency = (float) (2 * Math.PI) * frequency;
            float increment = angularFrequency / SAMPLE_RATE;
            float samples[] = new float[bufferSize];

            /* Generate samples from -1 to 1 */

            boolean mono = output.equalsIgnoreCase(MONO);
            int step = mono ? 1 : 2;
            boolean left = output.equalsIgnoreCase(STEREO) || output.equalsIgnoreCase(LEFT) || output.equalsIgnoreCase(MONO);
            boolean right = output.equalsIgnoreCase(STEREO) || output.equalsIgnoreCase(RIGHT);

            for (int i = 0; i < samples.length; i = i + step) {
                samples[i] = left ? (float) Math.sin(angle) : 0; // Left channel
                if (!mono) {
                    samples[i + 1] = right ? (float) Math.sin(angle) : 0; // Right channel
                }
                angle += increment;
            }

            short[] buffer = new short[samples.length];
            for (int i = 0; i < samples.length; i++) {
                buffer[i] = (short) (samples[i] * (Short.MAX_VALUE));
            }

            return buffer;
        }
    }
}
