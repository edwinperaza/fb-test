package com.facebook.devices.mvp.view;

import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.view.TextureView;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.devices.R;
import com.facebook.devices.activities.CameraPrivacyActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.AutoFitTextureView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CameraPrivacyView extends TestViewBase {

    @BindView(R.id.texture_camera) AutoFitTextureView textureView;
    @BindView(R.id.camera_access_spinner) ProgressBar cameraAccessSpinner;

    public CameraPrivacyView(CameraPrivacyActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        /*TextureView listener config*/
        setupTextureView();
    }

    private void setupTextureView() {
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                post(new OnSurfaceTextureAvailable(surface, width, height));
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                /* This is invoked every time a new camera preview frame is showed */
            }
        });
    }

    /**
     * Apply mirror effect to the camera preview
     */
    public void setMirrorEffect(boolean isMirrorEffect) {
        Matrix matrix = new Matrix();
        if (textureView.getWidth() > textureView.getHeight()) { /*Horizontal*/
            textureView.setScaleX((isMirrorEffect) ? -1 : 1);
        } else { /*Vertical*/
            matrix.postScale((isMirrorEffect) ? -1 : 1, 1, textureView.getWidth() * 0.85f, textureView.getHeight());
            textureView.setTransform(matrix);
        }
    }

    public TextureView getTextureView() {
        return textureView;
    }

    public boolean isPortrait() {
        return getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public void setAspectRatio(int width, int height) {
        textureView.setAspectRatio(width, height);
    }

    public boolean isAvailable() {
        return textureView.isAvailable();
    }

    /**
     * Available states
     */
    @Override
    public void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.camera_privacy_dialog_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showFloatingTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void showDialogOne() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.privacy_dialog_message_one))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new DialogOneEvent()))
                        .build()
        );
    }

    public void showDialogTwo() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.privacy_dialog_message_two))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new DialogTwoPassEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new DialogTwoFailEvent()))
                        .setCollapsingEnabled(true)
                        .build()
        );
    }

    public void showDialogTurnOffPrivacy() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.privacy_dialog_turn_off_privacy))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new DialogTurnOffPrivacyEvent()))
                        .build()
        );
    }

    public void showCameraAccessSpinner() {
        cameraAccessSpinner.setVisibility(View.VISIBLE);
    }

    public void hideCameraAccessSpinner() {
        cameraAccessSpinner.setVisibility(View.GONE);
    }

    /**
     * Static classes for BUS
     */
    public static class DialogOneEvent {/*Nothing to do here*/
    }

    public static class DialogTwoPassEvent {
    }

    public static class DialogTwoFailEvent {
    }

    public static class DialogTurnOffPrivacyEvent {
    }

    /* SurfaceTextureView available callbacks */
    public static class OnSurfaceTextureAvailable {
        public SurfaceTexture surface;
        public int width;
        public int height;

        public OnSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            this.surface = surfaceTexture;
            this.width = width;
            this.height = height;
        }
    }
}