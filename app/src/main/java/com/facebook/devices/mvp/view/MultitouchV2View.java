package com.facebook.devices.mvp.view;

import android.support.annotation.IntDef;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.MultitouchCirclesView;
import com.facebook.devices.utils.customviews.MultitouchCirclesView.MultiTouchDetectorViewCallback;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MultitouchV2View extends TestViewBase {

    @BindView(R.id.multitouch_v2_view) MultitouchCirclesView multitouchCirclesView;

    public MultitouchV2View(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        setup();
    }

    private void setup() {
        /* Multitouch v2 init */
        multitouchCirclesView.setListener(resultMode -> post(new OnMultitouchReplies(resultMode)));
    }


    public void showFloatingTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideFloatingTutorialButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showInitBanner() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.multitouch_v2_init_message_dialog))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnIntroAccepted()))
                        .build()
        );
    }

    public void hideBannerTop() {
        getBannerView().hideBanner();
    }

    public void disableMultitouchView() {
        multitouchCirclesView.setEnabled(false);
    }

    public void enableMultitouchView() {
        multitouchCirclesView.setEnabled(true);
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        /* Tutorial */
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.multitouch_v2_tutorial_text_dialog)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showDroppedDialog() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.multitouch_v2_failure_title_dialog,
                R.string.multitouch_v2_failure_message_dialog)
                .setPositiveButton(R.string.try_again_base, ((dialog, which) -> post(new OnFailureDialogOptionSelectedEvent(OnFailureDialogOptionSelectedEvent.OPTION_RETRY))))
                .setNegativeButton(R.string.no_base, ((dialog, which) -> post(new OnFailureDialogOptionSelectedEvent(OnFailureDialogOptionSelectedEvent.OPTION_FAIL))))
                .show();
    }

    public void resetMultitouchView() {
        multitouchCirclesView.resetViewState();
    }

    public MultitouchCirclesView getMultitouchCirclesView() {
        return multitouchCirclesView;
    }

    /**
     * Static classes for BUS
     */
    public static class OnIntroAccepted {
        /*Nothing to do here*/
    }

    public static class OnMultitouchReplies {

        @MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult
        private int resultMode;

        public OnMultitouchReplies(@MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult int resultMode) {
            this.resultMode = resultMode;
        }

        @MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult
        public int getResultMode() {
            return resultMode;
        }
    }

    public static class OnFailureDialogOptionSelectedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({OPTION_FAIL, OPTION_RETRY})
        @interface FailureDialogOptions {
        }

        public static final int OPTION_FAIL = 0;
        public static final int OPTION_RETRY = 1;

        @FailureDialogOptions
        private int optionSelected;

        public OnFailureDialogOptionSelectedEvent(@FailureDialogOptions int optionSelected) {
            this.optionSelected = optionSelected;
        }

        @FailureDialogOptions
        public int getOptionSelected() {
            return optionSelected;
        }
    }
}
