package com.facebook.devices.mvp.model;

import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.TestInfoEntity;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PreferencesModel {

    private TestDatabase testDatabase;

    public PreferencesModel(TestDatabase testDatabase) {
        this.testDatabase = testDatabase;
    }

    public Map<String, TestInfoEntity> getAllTestInfo() {
        return testDatabase.testInfoDao()
                .getAllTestInfo()
                .stream()
                .collect(Collectors.toMap(TestInfoEntity::getId, p -> p));
    }

    public List<ArgumentEntity> getAllArguments() {
        return testDatabase.argumentDao().getCurrentArguments(testDatabase.testInfoDao()
                .getAllTestInfo().stream().map(TestInfoEntity::getId).collect(Collectors.toList()));
    }

    public List<ColorEntity> getColors() {
        return testDatabase.colorDao().getAllColors();
    }

    public void updateArgument(ArgumentEntity argumentEntity){
        testDatabase.argumentDao().updateArgument(argumentEntity);
    }
}
