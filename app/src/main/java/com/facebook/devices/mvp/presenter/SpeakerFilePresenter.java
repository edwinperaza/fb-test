package com.facebook.devices.mvp.presenter;

import android.media.audiofx.Visualizer;

import com.facebook.devices.mvp.model.SpeakerFileModel;
import com.facebook.devices.mvp.view.SpeakerFileView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

public class SpeakerFilePresenter extends PresenterTestBase<SpeakerFileModel, SpeakerFileView> {

    private Visualizer.OnDataCaptureListener visualizerCapture = new Visualizer.OnDataCaptureListener() {
        @Override
        public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
            if (waveform.length > 0) {
                view.updateWaveView(waveform);
            }
        }

        @Override
        public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
            /*Nothing to do here*/
        }
    };

    public SpeakerFilePresenter(SpeakerFileModel model, SpeakerFileView view) {
        super(model, view);

        model.setDefaultVolume();
        view.showTutorialFloatingButton();
    }

    public void onResumed() {
        if (model.isPermissionGranted()) {
            /*Views behavior*/
            switch (model.getQuestionState()) {
                case SpeakerFileModel.QUESTION_STATE_1:
                    view.showDialogStateOne();
                    break;
                case SpeakerFileModel.QUESTION_STATE_2:
                    view.showDialogStateTwo();
                    break;
            }
            /*Media player*/
            model.startPlayer(visualizerCapture);
            return;
        }
        model.requestPermissions();
    }

    public void onPaused() {
        model.stopPlayer();
    }

    /* Process result from permission requests */
    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onDialogOnePassPressed(SpeakerFileView.DialogOnePassEvent event) {
        model.nextQuestion();
        view.showDialogStateTwo();
    }

    @Subscribe
    public void onDialogTwoPassPressed(SpeakerFileView.DialogTwoPassEvent event) {
        model.stopPlayer();
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onDialogFailedPressed(SpeakerFileView.DialogFailedPressedEvent event) {
        model.stopPlayer();
        model.fail();
        view.finishActivity();
    }
}