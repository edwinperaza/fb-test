package com.facebook.devices.mvp.view;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IdRes;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WifiView extends TestViewBase {

    @BindView(R.id.cl_animation_container) ConstraintLayout loadingLayout;
    @BindView(R.id.wifi_transfer_container) ConstraintLayout wifiTransferLayout;
    @BindView(R.id.tv_waiting_message) TextView tvLoadingMsg;
    @BindView(R.id.tv_wifi_transfer_message) TextView tvSsidConnected;
    @BindView(R.id.lottie_animation) LottieAnimationView lottieAnimationView;
    @BindView(R.id.pb_wifi_transfer_upstream) ProgressBar upStream;
    @BindView(R.id.tv_wifi_transfer_upstream_value) TextView tvUpStream;
    @BindView(R.id.tv_wifi_transfer_downstream_value) TextView tvDownStream;
    @BindView(R.id.pb_wifi_transfer_downstream) ProgressBar downStream;

    private AlertDialog tutorialDialog;
    private AlertDialog finalDialog;
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public WifiView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    private void setLoadingStateBase() {
        loadingLayout.setVisibility(View.VISIBLE);
        tvLoadingMsg.setVisibility(View.VISIBLE);
        lottieAnimationView.setVisibility(View.VISIBLE);
    }

    public void showInitialDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.wifi_dialog_message))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnFailureBtnClicked()))
                        .build()
        );
    }

    public void setLoadingEnablingWifiMessage() {
        setLoadingStateBase();
        tvLoadingMsg.setText(getContext().getString(R.string.wifi_loading_message_enabling_wifi));
    }

    public void setLoadingConnectingWifiMessage() {
        setLoadingStateBase();
        tvLoadingMsg.setText(getContext().getString(R.string.wifi_loading_message_connecting_network));
    }

    public void setLoadingConnectingWifiMessage(String ssid) {
        setLoadingStateBase();
        tvLoadingMsg.setText(getContext().getString(R.string.wifi_loading_message_connecting_network_ssid, ssid));
    }

    public void updateConnectionInfo(String ssId, int linkSpeed, int frequency) {
        loadingLayout.setVisibility(View.GONE);
        tvLoadingMsg.setVisibility(View.GONE);
        if (finalDialog == null) {
            finalDialog = new AlertDialog.Builder(getContext(), R.style.MaterialBaseDialogStyle)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok_base, (dialog, which) -> {
                        dialog.dismiss();
                        post(new OnOkResultClicked());
                    }).create();

            View dialogView = getActivity().getLayoutInflater().inflate(R.layout.wifi_info_layout, null);
            setText(dialogView, R.id.tv_wifi_ssid_value, ssId);
            setText(dialogView, R.id.tv_wifi_speed_value, getContext().getString(R.string.mbps_data, linkSpeed));
            setText(dialogView, R.id.tv_wifi_frequency_value, getContext().getString(R.string.mhz_data, frequency));

            finalDialog.setView(dialogView);
        }
        finalDialog.show();
    }

    private void setText(View view, @IdRes int id, String msg) {
        ((TextView) view.findViewById(id)).setText(msg);
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideTutorialButton() {
        getFloatingTutorialView().hideButton();
    }

    public void onDestroy() {
        if (tutorialDialog != null) {
            tutorialDialog.dismiss();
            tutorialDialog = null;
        }

        if (finalDialog != null) {
            finalDialog.dismiss();
            finalDialog = null;
        }
    }

    public void showProgressUpStream(double rate, double progress) {
        upStream.setProgress((int) progress);
        tvUpStream.setText(getContext().getString(R.string.wifi_transfer_rate, formatDouble(rate)));
    }

    public void showProgressDownStream(double rate, double progress) {
        downStream.setProgress((int) progress);
        tvDownStream.setText(getContext().getString(R.string.wifi_transfer_rate, formatDouble(rate)));
    }

    private String formatDouble(double db) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(db);
    }

    @Override
    public void onFloatingTutorialClicked() {
        if (tutorialDialog == null) {
            tutorialDialog = DialogFactory.createMaterialDialog(
                    getContext(),
                    R.string.more_help_base,
                    R.string.wifi_tutorial_message)
                    .setPositiveButton(R.string.ok_base, null)
                    .create();
        }
        tutorialDialog.show();
    }

    public void showTransferRateDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.wifi_transfer_rate_dialog_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnOkTransferRateBtnClicked()))
                        .build()
        );
    }

    public void hideBannerTop() {
        getBannerView().hideBanner();
    }

    public void showTransferContainer() {
        wifiTransferLayout.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.GONE);
    }

    public void setSsidValue(String value) {
        tvSsidConnected.setText(getContext().getString(R.string.wifi_transfer_connected_message, value));
    }

    public AlertDialog getTutorialDialog() {
        return tutorialDialog;
    }

    public AlertDialog getFinalDialog() {
        return finalDialog;
    }

    /**
     * Static classes for BUS
     */
    public static class OnFailureBtnClicked {/*Nothing to do here*/
    }

    public static class OnOkTransferRateBtnClicked {/*Nothing to do here*/
    }

    public static class OnOkResultClicked {/*Nothing to do here*/
    }
}