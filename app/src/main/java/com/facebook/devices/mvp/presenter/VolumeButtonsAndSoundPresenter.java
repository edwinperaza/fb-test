package com.facebook.devices.mvp.presenter;


import android.view.KeyEvent;

import com.facebook.devices.mvp.model.VolumeButtonsAndSoundModel;
import com.facebook.devices.mvp.view.VolumeButtonsAndSoundView;

import org.greenrobot.eventbus.Subscribe;

public class VolumeButtonsAndSoundPresenter extends PresenterTestBase<VolumeButtonsAndSoundModel, VolumeButtonsAndSoundView> {

    public VolumeButtonsAndSoundPresenter(VolumeButtonsAndSoundModel model, VolumeButtonsAndSoundView view) {
        super(model, view);

        /*Init*/
        view.showQuestionUp();
        view.showTutorialButton(true);
    }

    public void onResumed() {
        model.init();
        view.showTouchCount(model.getTouchedTimes());
    }

    public boolean onKeyDown(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                view.showTouchCount(model.incrementTouchCount());
                if (model.isModeUp() && model.checkForNextStep()) {
                    view.showQuestionDown();
                }
                model.volumeUp();
                model.playSound();
                return true;

            case KeyEvent.KEYCODE_VOLUME_DOWN:
                view.showTouchCount(model.decrementTouchCount());
                if (model.isModeDown() && model.checkForNextStep()) {
                    passTest();
                }
                model.volumeDown();
                model.playSound();
                return true;
        }
        return false;
    }

    public void onDestroy() {
        model.resetDefaultValues();
    }

    private void passTest() {
        model.pass();
        view.finishActivity();
    }

    private void failTest() {
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void onFailureButtonEvent(VolumeButtonsAndSoundView.FailureButtonEvent event) {
        failTest();
    }
}