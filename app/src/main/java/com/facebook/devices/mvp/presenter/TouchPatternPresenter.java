package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.view.TouchPatternView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.utils.customviews.TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_FAILED;
import static com.facebook.devices.utils.customviews.TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_FAILED_INCOMPLETE;
import static com.facebook.devices.utils.customviews.TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS;

public class TouchPatternPresenter extends PresenterTestBase<TouchPatternModel, TouchPatternView> {

    public TouchPatternPresenter(TouchPatternModel model, TouchPatternView view) {
        super(model, view);
        init();
    }

    private void init() {
        /* Banner & tutorial */
        view.showBanner();
        view.showFloatingTutorialButton();

        /* Touch detector setup */
        view.setTouchDetectorEnabled(false);
        view.initTouchDetectorView(model.getTouchOrientation(), model.getTouchSize(), model.generateRespectivePath());
    }

    @Subscribe
    public void onBannerIntroClosed(TouchPatternView.OnBannerOkPressedEvent event) {
        view.hideBanner();
        view.hideFloatingButton();
        view.setTouchDetectorEnabled(true);
    }

    @Subscribe
    public void onTouchDetectorCallback(TouchPatternView.OnTouchDetectorViewCallback event) {
        switch (event.getResult()) {

            case FINISHED_PATTERN_SUCCESS:
                model.pass();
                view.finishActivity();
                break;

            case FINISHED_PATTERN_FAILED:
                view.showIncorrectPatternDialog();
                break;

            case FINISHED_PATTERN_FAILED_INCOMPLETE:
                view.showIncompletePatternDialog();
                break;
        }
    }

    @Subscribe
    public void onIncorrectPatterMessageInteracted(TouchPatternView.IncorrectDialogOptionPressedEvent event) {
        switch (event.getOptionSelected()) {
            case TouchPatternView.IncorrectDialogOptionPressedEvent.NO:
                model.fail();
                view.finishActivity();
                break;

            case TouchPatternView.IncorrectDialogOptionPressedEvent.TRY_AGAIN:
                view.resetTouchDetectorView();
                break;
        }
    }
}
