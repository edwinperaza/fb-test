package com.facebook.devices.mvp.presenter;

import android.util.Log;

import com.facebook.devices.mvp.model.StorageModel;
import com.facebook.devices.mvp.model.WifiModel;
import com.facebook.devices.mvp.view.StorageView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

public class StoragePresenter extends PresenterTestBase<StorageModel, StorageView> {

    private static final String TAG = StoragePresenter.class.getSimpleName();

    public StoragePresenter(StorageModel model, StorageView view) {
        super(model, view);
        model.setStorageProgress(storageProgressCallback);
        view.showTutorialButton();
    }

    public void onResume() {
        if (!model.isPermissionGranted()) {
            model.requestPermissions();
        } else {
            Log.d(TAG, "onResume init()");
            model.runStorageTest();
        }
    }

    private StorageModel.StorageProgress storageProgressCallback = new StorageModel.StorageProgress() {
        @Override
        public void onSetup() {
            Log.d(TAG, "onSetup");
            view.getUiHandler().post(view::setupView);
        }

        @Override
        public void onStart(long limit) {
            Log.d(TAG, "onStart");
            view.getUiHandler().post(() -> {
                view.setMin(0);
                view.setMax(limit);
                view.showStart();
                view.showWritingDialog();
            });
        }

        @Override
        public void onValidation() {
            Log.d(TAG, "onValidation");
            view.getUiHandler().post(() -> {
                view.showValidation();
                view.showValidationDialog();
            });
        }

        @Override
        public void onSucceed() {
            Log.d(TAG, "onSucceed");
            model.pass();
            view.finishActivity();
        }

        @Override
        public void onFail(String msg) {
            Log.d(TAG, "onFail: " + msg);
            model.fail(msg);
            view.finishActivity();
        }

        @Override
        public void onProgress(float progress, int position) {
            view.getUiHandler().post(() -> {
                view.updatePercentage(progress);
                view.updateProgressBar(position);
            });
        }

        @Override
        public void onPatternFail() {
            Log.d(TAG, "onPatternFail");
            view.getUiHandler().post(view::patternFail);
            model.fail();
            view.finishActivity();
        }

        @Override
        public void onRemainingTime(int hourOne, int hourTwo, int minOne, int minTwo, int secOne, int secTwo) {
            view.getUiHandler().post(() -> view.showRemainingTime(hourOne, hourTwo, minOne, minTwo, secOne, secTwo));
        }
    };

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onButtonFailurePressed(StorageView.OnButtonFailurePressed event) {
        model.cancel();
    }

    public void onStop() {
        model.stop();
    }
}
