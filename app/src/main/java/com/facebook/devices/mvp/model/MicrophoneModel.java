package com.facebook.devices.mvp.model;

import android.support.annotation.NonNull;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class MicrophoneModel extends ServiceModel {

    public final int SAMPLE_RATE = 44100; /*khz*/

    private AudioRecorder audioRecorder;
    private RuntimePermissionHandler permissionHandler;

    public MicrophoneModel(RuntimePermissionHandler permissionHandler, @NonNull AudioRecorder audioRecorder) {
        this.permissionHandler = permissionHandler;
        this.audioRecorder = audioRecorder;
    }

    public int getBufferSize() {
        return audioRecorder.getBufferSize();
    }

    public void startRecording(AudioRecorder.AudioRecorderListener listener) {
        if (!audioRecorder.isRunning()){
            audioRecorder.setSampleRate(SAMPLE_RATE);
            audioRecorder.startRecording(listener);
        }
    }

    public void stopRecording() {
        if (audioRecorder != null && audioRecorder.isRunning()){
            audioRecorder.stopRecording();
        }
    }

    public boolean isRecordAudioThreadRunning(){
        return (audioRecorder != null && audioRecorder.isRunning());
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
       permissionHandler.requestPermission();
    }

    public boolean isPermissionGranted() {
       return permissionHandler.isPermissionGranted();
    }

}
