package com.facebook.devices.mvp.view;


import android.support.annotation.IntDef;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MultitouchView extends TestViewBase {

    @BindView(R.id.multitouch_view)
    com.facebook.devices.utils.customviews.MultitouchView multitouchView;

    public MultitouchView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        setup();
    }

    private void setup() {
        /* Multitouch setup*/
        multitouchView.setListener(resultMode -> post(new OnMultitouchReplies(resultMode)));
    }

    public void showFloatingTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void showDialogOne() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.multitouch_dialog_one_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new InitDialogEvent()))
                        .build()
        );
    }

    public void hideDialogOne() {
        getBannerView().hideBanner();
    }

    public void hideFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        /* Tutorial setup*/
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.multitouch_tutorial_dialog_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showDroppedDialog() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.multitouch_dropped_dialog_title,
                R.string.multitouch_dropped_dialog_message)
                .setPositiveButton(R.string.multitouch_dropped_option_one_message, (dialog, which) -> post(new OnFailureDialogOptionSelected(OnFailureDialogOptionSelected.OPTION_RETRY)))
                .setNegativeButton(R.string.fail_base, (dialog, which) -> post(new OnFailureDialogOptionSelected(OnFailureDialogOptionSelected.OPTION_FAIL)))
                .show();
    }

    public void resetMultitouch() {
        multitouchView.resetItemsNotCompleted();
    }

    public void enableMultitouchComponent() {
        multitouchView.setEnabled(true);
    }

    public void disableMultitouchComponent() {
        multitouchView.setEnabled(false);
    }

    /**
     * Static classes for BUS
     */
    public static class InitDialogEvent {
        /*Nothing to do here*/
    }

    public static class OnMultitouchReplies {

        @MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult
        private int resultMode;

        public OnMultitouchReplies(@MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult int resultMode) {
            this.resultMode = resultMode;
        }

        @MultiTouchDetectorViewCallback.MultiTouchDetectorCallbackResult
        public int getResultMode() {
            return resultMode;
        }
    }

    public static class OnFailureDialogOptionSelected {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({OPTION_FAIL, OPTION_RETRY})
        @interface FailureDialogOptions {
        }

        public static final int OPTION_FAIL = 0;
        public static final int OPTION_RETRY = 1;

        @FailureDialogOptions
        private int optionSelected;

        public OnFailureDialogOptionSelected(@FailureDialogOptions int optionSelected) {
            this.optionSelected = optionSelected;
        }

        @FailureDialogOptions
        public int getOptionSelected() {
            return optionSelected;
        }
    }
}
