package com.facebook.devices.mvp.view;

import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.ColorCustomView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ColorView extends TestViewBase {

    @BindView(R.id.multicolor_view) ColorCustomView multicolorView;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public ColorView(TestBaseActivity activity, BusProvider.Bus eventBus) {
        super(activity, eventBus);
        ButterKnife.bind(this, activity);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    public void setColor(String... colorCodes) {
        int[] colors = new int[colorCodes.length];
        for (int i = 0; i < colorCodes.length; i++) {
            colors[i] = Color.parseColor(colorCodes[i]);
        }
        multicolorView.drawColors(colors);
    }

    public void hideTutorialFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showTutorialFloatingButton() {
        getFloatingTutorialView().showButton();
    }

    public void showFirstQuestion() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.color_check_question_one_dialog_text))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE)))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEGATIVE)))
                        .setOptionTextNeutral(R.string.try_again_base)
                        .setOptionNeutralListener(() -> post(new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEUTRAL)))
                        .build());
    }

    public void showSecondQuestion() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.color_check_question_two_dialog_text))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_POSITIVE)))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEGATIVE)))
                        .setOptionTextNeutral(R.string.try_again_base)
                        .setOptionNeutralListener(() -> post(new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEUTRAL)))
                        .build());
    }

    public void showTutorialOnBannerTop() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.color_check_tutorial_dialog_text))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnTutorialTopBannerEvent()))
                        .build());
    }

    public void hideBannerTop() {
        getBannerView().hideBanner();
    }

    @Override
    protected void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.color_check_tutorial_dialog_text)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    /**
     * Static classes for BUS
     */
    public static class QuestionOneEvent extends AlertDialogBaseEvent {

        public QuestionOneEvent(int optionSelected) {
            super(optionSelected);
        }
    }

    public static class QuestionTwoEvent extends AlertDialogBaseEvent {

        public QuestionTwoEvent(int optionSelected) {
            super(optionSelected);
        }
    }

    public static class OnTutorialTopBannerEvent {
    }

}
