package com.facebook.devices.mvp.view;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.annotation.IntDef;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.BluetoothConnectedAutoModel;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.WaveGraphView;
import com.facebook.devices.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BluetoothConnectedAutoView extends TestViewBase {

    @BindView(R.id.layout_bluetooth_loading) ConstraintLayout loadingLayout;
    @BindView(R.id.pb_bluetooth_loading) ProgressBar bluetoothLoading;
    @BindView(R.id.wave_view) WaveGraphView waveGraphView;
    @BindView(R.id.tv_loading_message) TextView loadingMessage;

    public BluetoothConnectedAutoView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_init_dialog_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnInitDialogPressedEvent()))
                        .build()
        );
    }

    public void hideDialog() {
        getBannerView().hideBanner();
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton(true);
    }

    public void showNoAvailableBluetooth() {
        Toast.makeText(getContext(), R.string.bluetooth_not_available, Toast.LENGTH_LONG).show();
    }

    public void showFailedBluetoothPairedLayout() {
        loadingLayout.setVisibility(View.GONE);
        Toast.makeText(getContext(), R.string.bluetooth_pairing_error_message, Toast.LENGTH_LONG).show();
    }

    public void showPairingBluetoothLayout() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_waiting_error))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnUserFailedEvent()))
                        .build()
        );
        loadingLayout.setVisibility(View.VISIBLE);
        loadingMessage.setText(R.string.bluetooth_pairing_message);
    }

    public void showConnectToDeviceDialog(BluetoothDevice device) {
        loadingAndFailureLayoutVisibilityGone();
        String title = getContext().getString(R.string.bluetooth_connection_title) + " " + device.getName() +
                ", " + getContext().getString(R.string.bluetooth_connection_address) + ": " + device.getAddress() + ", " +
                getContext().getString(R.string.bluetooth_connection_message);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), title))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new ConnectOptionPressedEvent(ConnectOptionPressedEvent.CONNECT)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new ConnectOptionPressedEvent(ConnectOptionPressedEvent.NO)))
                        .build()
        );
    }

    public void showConnectToDeviceWithoutPlayAudioDialog(BluetoothDevice device) {
        loadingAndFailureLayoutVisibilityGone();
        String title = getContext().getString(R.string.bluetooth_connection_title) + " " + device.getName() +
                ", " + getContext().getString(R.string.bluetooth_connection_address) + ": " + device.getAddress() + ", " +
                getContext().getString(R.string.bluetooth_connection_without_audio_message);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), title))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnUserPassedEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new OnUserFailedEvent()))
                        .build()
        );
    }

    public void showAudioPlayingDialogView() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_check_test_result_title))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new ResultOptionPressedEvent(ResultOptionPressedEvent.YES)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new ResultOptionPressedEvent(ResultOptionPressedEvent.NO)))
                        .build()
        );
    }

    public void showWaitingDialogView() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_waiting_for_audio_question))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionNegativeListener(() -> post(new OnUserFailedEvent()))
                        .build()
        );
    }

    public void showConnectedMessage() {
        loadingAndFailureLayoutVisibilityGone();
        Toast.makeText(getContext(), getActivity().getString(R.string.bluetooth_device_connected) , Toast.LENGTH_LONG).show();
    }

    public void showWaitingForPlayer() {
        loadingLayout.setVisibility(View.VISIBLE);
        loadingMessage.setText(R.string.bluetooth_waiting_for_audio);
    }

    public void requestEnableBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        getActivity().startActivityForResult(enableBtIntent, BluetoothConnectedAutoModel.REQUEST_ENABLE_BT);
    }

    public void updateWaveView(byte[] bytes) {
        try {
            waveGraphView.setBytes(bytes);
        } catch (WaveGraphView.WaveViewException e) {
            Log.e("WaveView", e.getMessage());
        }
    }

    public void loadingAndFailureLayoutVisibilityGone() {
        loadingLayout.setVisibility(View.GONE);
    }

    /**
     * Static classes for BUS
     */
    public static class OnInitDialogPressedEvent {/*Nothing to do here*/
    }

    public static class OnUserFailedEvent {/*Nothing to do here*/
    }

    public static class OnUserPassedEvent {/*Nothing to do here*/
    }

    public static class IncorrectPairedOptionPressedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({NO, TRY_AGAIN})
        @interface IncorrectPairedCallbackOptions {}

        public static final int NO = 0;
        public static final int TRY_AGAIN = 1;

        private int option;

        public IncorrectPairedOptionPressedEvent(@IncorrectPairedCallbackOptions int option) {
            this.option = option;
        }

        @IncorrectPairedCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }

    public static class ConnectOptionPressedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({NO, CONNECT})
        @interface ConnectCallbackOptions {}

        public static final int NO = 0;
        public static final int CONNECT = 1;

        private int option;

        public ConnectOptionPressedEvent(@ConnectCallbackOptions int option) {
            this.option = option;
        }

        @ConnectCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }

    public static class ResultOptionPressedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({NO,  YES})
        @interface ResultCallbackOptions {}

        public static final int NO = 0;
        public static final int YES = 1;

        private int option;

        public ResultOptionPressedEvent(@ResultCallbackOptions int option) {
            this.option = option;
        }

        @ResultCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }
}