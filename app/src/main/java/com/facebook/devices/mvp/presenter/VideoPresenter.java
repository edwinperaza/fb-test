package com.facebook.devices.mvp.presenter;

import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;

import com.facebook.devices.mvp.model.VideoModel;
import com.facebook.devices.mvp.view.VideoView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

public class VideoPresenter extends PresenterTestBase<VideoModel, VideoView> {

    public VideoPresenter(VideoModel model, VideoView view) {
        super(model, view);

        model.setDefaultMediaVolume();
        view.setup();
        view.showInitialDialog();
    }

    public void onResume() {
        if (!model.isPermissionGranted()) {
            model.requestPermissions();
            return;
        }

        view.showVideoLayout();
        view.showTutorialButton();

        if (model.isFirstQuestionBeingShown()) {
            view.showFirstQuestion();
            view.showReplayButton();
            return;
        }

        if (model.isSecondQuestionBeingShown()) {
            view.showSecondQuestion();
            view.showReplayButton();
        }
    }

    private void loadData() {
        try {
            Uri uri = model.getVideoPathUri();
            if (uri != null) {
                view.setVideoDataSource(uri);
            } else {
                view.setVideoDataSource(model.getVideoPath());
            }
        } catch (IOException e) {
            failTest(e.getMessage());
        }
    }

    public void onDestroy() {
        /*Release MediaPlayer in case of not managed error
         * that doesn't include fail or pass scenarios*/
        view.releaseResources();
    }

    public void onSaveInstantState(Bundle savedInstantState) {
        savedInstantState.putBoolean(VideoModel.FIRST_QUESTION_IS_BEING_SHOWN, model.isFirstQuestionBeingShown());
        savedInstantState.putBoolean(VideoModel.SECOND_QUESTION_IS_BEING_SHOWN, model.isSecondQuestionBeingShown());
    }

    @Subscribe
    public void onPlayButtonPressed(VideoView.OnPlayButtonPressed event) {
        view.playVideo();
        view.hidePlayButton();
        view.hideTutorialButton();
        view.hideDialogTop();
    }

    @Subscribe
    public void onButtonFailurePressed(VideoView.OnButtonFailurePressed event) {
        failTest();
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            failTest("User denied permissions");
        }
    }

    @Subscribe
    public void onPermissionGranted(RuntimePermissionHandler.PermissionGrantedEvent event) {
        if (model.isRenderReady()) {
            loadData();
        }
    }

    @Subscribe
    public void onFirstQuestionReply(VideoView.OnFirstQuestionReply event) {
        switch (event.getOptionSelected()) {
            case VideoView.OnFirstQuestionReply.SUCCESS:
                model.firstQuestionIsNotBeingShown();
                model.secondQuestionIsBeingShown();
                view.showSecondQuestion();
                break;

            case VideoView.OnFirstQuestionReply.FAILED:
                failTest();
                break;
        }
    }

    @Subscribe
    public void onSecondQuestionReply(VideoView.OnSecondQuestionReply event) {
        switch (event.getOptionSelected()) {
            case VideoView.OnSecondQuestionReply.SUCCESS:
                passTest();
                break;

            case VideoView.OnSecondQuestionReply.FAILED:
                failTest();
                break;
        }
    }
    
    @Subscribe
    public void onReplayButtonPressed(VideoView.OnReplayButtonPressed event) {
        model.firstQuestionIsNotBeingShown();
        model.secondQuestionIsNotBeingShown();
        view.hideDialogTop();
        view.hideTutorialButton();
        view.hideReplayButton();
        view.playVideo();
    }

    @Subscribe
    public void onCompletionPlayer(VideoView.CompletionPlayerEvent event) {
        model.firstQuestionIsBeingShown();
        view.showFirstQuestion();
        view.showReplayButton();
        view.showTutorialButton();
    }

    @Subscribe
    public void onVideoSizeChanged(VideoView.VideoSizeChangedEvent event) {
        Point screenSize = view.getScreenSize();
        Point bestScreenSize = model.calculateBestSize(event.width, event.height, screenSize);
        view.setSize(bestScreenSize.x, bestScreenSize.y);
        view.showSurfaceView();
    }

    @Subscribe
    public void onVideoLoaded(VideoView.OnVideoLoaded event) {
        /*Skip black screen intro*/
        view.seekVideoTo(100);
        if (model.isFirstQuestionBeingShown() || model.isSecondQuestionBeingShown()) {
            view.showReplayButton();
        } else {
            view.showPlayButton();
        }
    }

    @Subscribe
    public void onRenderingCompleted(VideoView.OnRenderingCompleted event) {
        model.setRenderReady();
        if (!model.isPermissionGranted()) { /*Check permissions*/
            return;
        }
        loadData();
    }

    private void failTest(String... message) {
        view.releaseResources();
        model.fail(message);
        view.finishActivity();
    }

    private void passTest(String... message) {
        view.releaseResources();
        model.pass(message);
        view.finishActivity();
    }
}
