package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DecibelView extends TestViewBase {

    @BindView(R.id.cl_decibel_layout) RelativeLayout decibelLayout;
    @BindView(R.id.tv_decibel_current_value) TextView decibelCurrentValue;
    @BindView(R.id.tv_decibel_goal_value) TextView decibelGoalValue;
    @BindView(R.id.tv_decibel_environment_value) TextView decibelEnvironmentValue;
    @BindView(R.id.iv_decibel_01) ImageView ivDecibelOne;
    @BindView(R.id.iv_decibel_02) ImageView ivDecibelTwo;
    @BindView(R.id.iv_decibel_03) ImageView ivDecibelThree;
    @BindView(R.id.iv_decibel_04) ImageView ivDecibelFour;
    @BindView(R.id.iv_decibel_05) ImageView ivDecibelFive;
    @BindView(R.id.iv_decibel_06) ImageView ivDecibelSix;
    @BindView(R.id.iv_decibel_07) ImageView ivDecibelSeven;
    @BindView(R.id.iv_decibel_08) ImageView ivDecibelEight;
    @BindView(R.id.iv_decibel_09) ImageView ivDecibelNine;
    @BindView(R.id.iv_decibel_10) ImageView ivDecibelTen;
    @BindView(R.id.iv_decibel_11) ImageView ivDecibelEleven;
    @BindView(R.id.iv_decibel_12) ImageView ivDecibelTwelve;
    @BindView(R.id.iv_decibel_13) ImageView ivDecibelThirteen;
    @BindView(R.id.iv_decibel_14) ImageView ivDecibelFourTeen;

    private List<ImageView> decibelViews = new LinkedList<>();
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public DecibelView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);

        decibelViews.add(ivDecibelOne);
        decibelViews.add(ivDecibelTwo);
        decibelViews.add(ivDecibelThree);
        decibelViews.add(ivDecibelFour);
        decibelViews.add(ivDecibelFive);
        decibelViews.add(ivDecibelSix);
        decibelViews.add(ivDecibelSeven);
        decibelViews.add(ivDecibelEight);
        decibelViews.add(ivDecibelNine);
        decibelViews.add(ivDecibelTen);
        decibelViews.add(ivDecibelEleven);
        decibelViews.add(ivDecibelTwelve);
        decibelViews.add(ivDecibelThirteen);
        decibelViews.add(ivDecibelFourTeen);
    }

    public Handler getUIHandler() {
        return uiHandler;
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.decibel_tutorial_dialog)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showEnvironmentMeasurementInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.decibel_init_dialog_reference_value))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showEnvironmentMeasurementFailDialog() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.decibel_environment_value_fail))
                        .setOptionTextPositive(R.string.try_again_base)
                        .setOptionPositiveListener(() -> post(new OnButtonRetryPressed()))
                        .setOptionTextNegative(R.string.fail_base)
                        .setOptionNegativeListener(() -> post(new OnButtonFailurePressed()))
                        .build());
    }

    public void showMeasurementDialog() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.decibel_check_measurement_dialog_title))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showEnvironmentMediaResult(double value, double goal) {
        decibelEnvironmentValue.setText(getContext().getString(R.string.decibel_value, formatDouble(value)));
        decibelGoalValue.setText(getContext().getString(R.string.decibel_value, formatDouble(goal)));
    }

    public void showDecibel(int image, double decibels) {
        IntStream.range(0, decibelViews.size())
                .forEach(i -> decibelViews.get(i).setVisibility(i < image ? View.VISIBLE : View.INVISIBLE));
        decibelCurrentValue.setText(getContext().getString(R.string.decibel_value, formatDouble(decibels)));
    }

    public void permissionAccepted() {
        decibelLayout.setVisibility(View.VISIBLE);
    }

    private String formatDouble(double db) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(db);
    }

    /**
     * Static classes for BUS
     */
    public static class OnButtonFailurePressed {/*Nothing to do here*/
    }

    public static class OnButtonRetryPressed {/*Nothing to do here*/
    }
}