package com.facebook.devices.mvp.model;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.reflect.Method;

public class BluetoothSpeakerModel extends ServiceModel {

    public static final int REQUEST_ENABLE_BT = 1;
    public static final int REQUEST_DISCOVERABLE_BT_SECONDS = 60;
    public static final int REQUEST_DISCOVERABLE_BT = 2;
    public static final String PERMISSION_BLUETOOTH_ACCEPTED = "permissionAccepted";

    private Activity activity;
    private boolean permissionBluetoothAccepted;
    private boolean discoverableBluetoothDialogShown = false;
    private BroadcastReceiver mReceiver;
    private BluetoothDevice device;
    private BluetoothAdapter bluetoothAdapter;
    private final BusProvider.Bus bus;
    private RuntimePermissionHandler permissionHandler;

    public BluetoothSpeakerModel(Activity activity, BusProvider.Bus bus, BluetoothAdapter bluetoothAdapter,
                                 boolean permissionBluetoothAccepted, RuntimePermissionHandler permissionHandler) {
        this.activity = activity;
        this.permissionBluetoothAccepted = permissionBluetoothAccepted;
        this.bus = bus;
        this.bluetoothAdapter = bluetoothAdapter;
        this.permissionHandler = permissionHandler;
    }

    /**
     * The BroadcastReceiver that listens for discovered devices and when discovery is finished
     */
    public void registerReceiver() {
        Log.d("BT_FACE", "Model registerReceiver()");
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action == null) {
                    return;
                }
                Log.d("BT_FACE", "Model registerReceiver action: " + action);
                switch (action) {
                    case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                        Log.d("BT_FACE", "Model registerReceiver ACTION_BOND_STATE_CHANGED");
                        device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        if (device != null) {
                            Log.d("BT_FACE", "Model ACTION_BOND_STATE_CHANGED, Bond State: " + device.getBondState());
                            if (device.getBondState() == BluetoothDevice.BOND_BONDED) { //remote device is bonded (paired), not necessarily connected.

                                Log.d("BT_FACE", "Model ACTION_BOND_STATE_CHANGED === BOND_BONDED");
                                Log.d("BT_FACE", "Model ACTION_FOUND Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                                bus.post(new ConnectedBluetoothSuccess(device.getName(), device.getAddress()));

                            } else if (device.getBondState() == BluetoothDevice.BOND_NONE) { //remote device is not bonded (paired).

                                Log.d("BT_FACE", "Model ACTION_BOND_STATE_CHANGED === BOND_NONE");
                                Log.d("BT_FACE", "Model ACTION_FOUND Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                                bus.post(new FailedBluetoothBonded());
                            }
                        }
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        activity.registerReceiver(mReceiver, filter);
    }

    public void onDestroy() {
        Log.d("BT_FACE", "Model onDestroy");
        try {
            Log.d("BT_FACE", "Model unregisterReceiver()");
            activity.unregisterReceiver(mReceiver);
        } catch (IllegalArgumentException ex) {
            Log.d("BT_FACE", "Model Tried to unregister Receiver that was not registered: " + ex.getMessage());
        }

        if (device != null) {
            Log.d("BT_FACE", "Model disconnectDevice");
            disconnectDevice(device.getAddress());
        }
    }

    private void disconnectDevice(String macAddress) {
        Log.d("BT_FACE", "Model disconnectDevice()");
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(macAddress);
        try {
            removeBond(device);
        } catch (Exception e) {
            Log.d("BT_FACE", "Model disconnectDevice Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean removeBond(BluetoothDevice btDevice) throws Exception {
        Log.d("BT_FACE", "Model removeBond()");
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        return (Boolean) removeBondMethod.invoke(btDevice);
    }

    public boolean isAvailableBluetoothAdapter() {
        return bluetoothAdapter != null;
    }

    public boolean isEnabledBluetoothAdapter() {
        Log.d("BT_FACE", "Model isEnabledBluetoothAdapter");
        return bluetoothAdapter.isEnabled();
    }

    public String getBluetoothName() {
        return (isAvailableBluetoothAdapter() && isEnabledBluetoothAdapter()) ? bluetoothAdapter.getName() : "";
    }

    public boolean isPermissionGranted() {
        Log.d("BT_FACE", "Model isPermissionGranted");
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        Log.d("BT_FACE", "Model shouldRequestPermissions");
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        Log.d("BT_FACE", "Model requestPermissions");
        permissionHandler.requestPermission();
    }

    public boolean wasPermissionBluetoothAccepted() {
        return permissionBluetoothAccepted;
    }

    public void permissionBluetoothAccepted() {
        this.permissionBluetoothAccepted = true;
    }

    public boolean wasDiscoverableBluetoothShown() {
        return discoverableBluetoothDialogShown;
    }

    public void discoverableBluetoothDialogShown() {
        this.discoverableBluetoothDialogShown = true;
    }

    public boolean showDiscoverableView() {
        return bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
    }

    public boolean isPermissionGranted(int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Static classes for BUS
     */
    public static class FailedBluetoothBonded {/*Nothing to do here*/
    }

    public static class ConnectedBluetoothSuccess {
        String deviceName;
        String deviceAddress;

        public ConnectedBluetoothSuccess(String deviceName, String deviceAddress) {
            this.deviceName = deviceName;
            this.deviceAddress = deviceAddress;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public String getDeviceAddress() {
            return deviceAddress;
        }
    }

    public static class DisconnectedBluetoothSuccess {/*Nothing to do here*/
    }

}