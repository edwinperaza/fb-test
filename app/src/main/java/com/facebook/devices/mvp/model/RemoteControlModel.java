package com.facebook.devices.mvp.model;

import android.os.RemoteException;
import android.support.annotation.IntDef;
import android.util.Log;

import com.facebook.aloha.bluetoothremote.IBluetoothRemotePairingService;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class RemoteControlModel extends ServiceModel {

    private IBluetoothRemotePairingService iBluetoothRemotePairingService;
    private RemotePairingCallback remotePairingCallback;
    private RemotePairingThread remotePairingThread;
    private long timeToFail;
    private long timeToRetryPairing;
    private Map<String, Boolean> buttons = new HashMap<>();


    public static final int PRE_PAIR_AND_CONNECTED = -1;
    public static final int KEYCODE_DPAD_CENTER = 0;
    public static final int KEYCODE_ESCAPE = 1;
    public static final int KEYCODE_SEARCH = 2;
    public static final int KEYCODE_MEDIA_PLAY_PAUSE = 3;
    public static final int KEYCODE_MENU = 4;
    public static final int FINISH_TEST = 5;

    public static final String KEYCODE_DPAD_CENTER_STRING = "KEYCODE_DPAD_CENTER";
    public static final String KEYCODE_ESCAPE_STRING = "KEYCODE_ESCAPE";
    public static final String KEYCODE_SEARCH_STRING = "KEYCODE_SEARCH";
    public static final String KEYCODE_MEDIA_PLAY_PAUSE_STRING = "KEYCODE_MEDIA_PLAY_PAUSE";
    public static final String KEYCODE_MENU_STRING = "KEYCODE_MENU";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PRE_PAIR_AND_CONNECTED, KEYCODE_DPAD_CENTER, KEYCODE_ESCAPE, KEYCODE_SEARCH,
             KEYCODE_MEDIA_PLAY_PAUSE, KEYCODE_MENU, FINISH_TEST})
    public @interface RemoteControlModelState {
    }

    @RemoteControlModelState
    private int actualState;

    @RemoteControlModelState
    public int getActualState() {
        return actualState;
    }

    public void setActualState(@RemoteControlModelState int actualState) {
        this.actualState = actualState;
    }

    public RemoteControlModel(long timeToFail, long timeToRetryPairing) {
        this.timeToFail = timeToFail;
        this.timeToRetryPairing = timeToRetryPairing;
        this.actualState = PRE_PAIR_AND_CONNECTED;
        initMapButtons();
    }

    private void initMapButtons() {
//        This buttons are not still available to test
//        buttons.put("KEYCODE_DPAD_UP", false);
//        buttons.put("KEYCODE_DPAD_DOWN", false);
//        buttons.put("KEYCODE_DPAD_LEFT", false);
//        buttons.put("KEYCODE_DPAD_RIGHT", false);
        buttons.put(KEYCODE_DPAD_CENTER_STRING, false);
        buttons.put(KEYCODE_ESCAPE_STRING, false);
        buttons.put(KEYCODE_SEARCH_STRING, false);
        buttons.put(KEYCODE_MEDIA_PLAY_PAUSE_STRING, false);
        buttons.put(KEYCODE_MENU_STRING, false);
    }

    public long getTimeToFail() {
        return timeToFail;
    }

    public long getTimeToRetryPairing() {
        return timeToRetryPairing;
    }

    public void validateButtonPressed(String buttonText) {
        if (buttons.containsKey(buttonText)) {
            buttons.put(buttonText, true);
        }
    }

    public boolean shouldGoToNextKeyButton(String buttonText) {
        Log.d("RemoteControl", "shouldGoToNextKeyButton buttonText : " + buttonText + ", actualState: " + actualState);
        boolean isValid = false;
        switch (actualState) {
            case KEYCODE_DPAD_CENTER:
                if (buttonText.equals(KEYCODE_DPAD_CENTER_STRING)) {
                    actualState = KEYCODE_ESCAPE;
                    isValid = true;
                }
                break;
            case KEYCODE_ESCAPE:
                if (buttonText.equals(KEYCODE_ESCAPE_STRING)) {
                    actualState = KEYCODE_SEARCH;
                    isValid = true;
                }
                break;
            case KEYCODE_SEARCH:
                if (buttonText.equals(KEYCODE_SEARCH_STRING)) {
                    actualState = KEYCODE_MEDIA_PLAY_PAUSE;
                    isValid = true;
                }
                break;
            case KEYCODE_MEDIA_PLAY_PAUSE:
                if (buttonText.equals(KEYCODE_MEDIA_PLAY_PAUSE_STRING)) {
                    actualState = KEYCODE_MENU;
                    isValid = true;
                }
                break;
            case KEYCODE_MENU:
                if (buttonText.equals(KEYCODE_MENU_STRING)) {
                    actualState = FINISH_TEST;
                    isValid = true;
                }
                break;
        }
        if (isValid) {
            validateButtonPressed(buttonText);
        }
        Log.d("RemoteControl", "shouldGoToNextKeyButton isValid : " + isValid);
        return isValid;
    }

    public boolean validateButtons() {
        for (Map.Entry<String, Boolean> entry : buttons.entrySet()) {
            if (!entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    public void onServiceBluetoothRemotePairingConnected(IBluetoothRemotePairingService iBluetoothRemotePairingService,
            RemotePairingCallback remotePairingCallback) {
        this.remotePairingCallback = remotePairingCallback;
        this.iBluetoothRemotePairingService = iBluetoothRemotePairingService;
        remotePairingThread = new RemotePairingThread(iBluetoothRemotePairingService, remotePairingCallback);
        remotePairingThread.start();
    }

    public void runPairingThread() {
        if (!remotePairingThread.isRunning()) {
            remotePairingThread = new RemotePairingThread(iBluetoothRemotePairingService, remotePairingCallback);
            remotePairingThread.start();
        }
    }

    public void onServiceBluetoothRemotePairingDisconnected() {
        this.iBluetoothRemotePairingService = null;
    }

    private class RemotePairingThread extends Thread {

        private WeakReference<RemotePairingCallback> remotePairingCallback;
        private IBluetoothRemotePairingService iBluetoothRemotePairingService;
        private boolean isRunning = false;

        public RemotePairingThread(IBluetoothRemotePairingService iBluetoothRemotePairingService, RemotePairingCallback remotePairingCallback) {
            this.remotePairingCallback = new WeakReference<>(remotePairingCallback);
            this.iBluetoothRemotePairingService = iBluetoothRemotePairingService;
        }

        @Override
        public void run() {
            try {
                isRunning = true;
                boolean isPaired = iBluetoothRemotePairingService.isRemotePaired();
                if (!isPaired) {
                    //Factory SO will create a file to be able to get the mac address of the remote control
                    //if you are testing with a User SO you need to call pairNewRemote
                    //wait 3 or more seconds to get response
                    isPaired = iBluetoothRemotePairingService.pairStoredRemote();
                }

                Log.d("RemoteControl", (!isPaired)?
                        "iBluetoothRemotePairingService: Remote is NOT Paired, pairStoredRemote() response: " + isPaired
                        : "iBluetoothRemotePairingService: Remote is Paired");

                if (!isPaired) {
                    isRunning = false;
                    remotePairingCallback.get().isRemotePaired(isPaired);
                    return; /*Finish thread work*/
                } else {
                    remotePairingCallback.get().isRemotePaired(isPaired);
                }

                if (!iBluetoothRemotePairingService.isRemoteConnected()) {
                    Log.d("RemoteControl", "iBluetoothRemotePairingService: Remote is NOT Connected");
                    remotePairingCallback.get().isRemoteConnected(false);
                } else {
                    Log.d("RemoteControl", "iBluetoothRemotePairingService: Remote is Connected");
                    remotePairingCallback.get().isRemoteConnected(true);
                }
            } catch (RemoteException e) {
                Log.d("RemoteControl", "RemoteException: " + e.getMessage());
            }

            isRunning = false;
        }

        public boolean isRunning() {
            return isRunning;
        }
    }

    public interface RemotePairingCallback {

        void isRemoteConnected(boolean isRemoteConnected);

        void isRemotePaired(boolean isRemotePaired);

    }
}