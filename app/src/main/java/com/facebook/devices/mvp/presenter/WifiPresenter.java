package com.facebook.devices.mvp.presenter;

import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.facebook.devices.mvp.model.WifiModel;
import com.facebook.devices.mvp.view.WifiView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

public class WifiPresenter extends PresenterTestBase<WifiModel, WifiView> {

    private WifiModel.WifiModelCallback modelCallback = new WifiModel.WifiModelCallback() {
        @Override
        public void onReceiverRegistered(boolean wifiEnabled) {
            /* Wifi connection behavior */
            if (wifiEnabled) {
                model.disconnect();
            } else {
                view.setLoadingEnablingWifiMessage();
                model.enableWifi();
            }
        }

        @Override
        public void onWifiSateEnabled(Intent intent) {
            startConnectionQueue();
        }

        @Override
        public void onWifiConnecting(String ssid) {
            view.setLoadingConnectingWifiMessage(ssid);
        }

        @Override
        public void onWifiConnected(String ssid, int speed, int frequency) {
            view.updateConnectionInfo(ssid, speed, frequency);
        }

        @Override
        public void onQueueFailed() {
            Log.d(WifiPresenter.class.toString(), "FAILED CONNECTIONS");
        }
    };

    private WifiModel.WifiTransferProgress wifiTransferCallback = new WifiModel.WifiTransferProgress() {
        @Override
        public void onUpStream(double rate, double progress) {
            view.getUiHandler().post(() -> view.showProgressUpStream(rate, progress));
        }

        @Override
        public void onDownStream(double rate, double progress) {
            view.getUiHandler().post(() -> view.showProgressDownStream(rate, progress));
        }

        @Override
        public void onErrorStream(String errorMessage) {
            model.fail(errorMessage);
            view.getUiHandler().post(view::finishActivity);
        }

        @Override
        public void onUploadSuccess() {
            model.downloadFile(wifiTransferCallback);
        }

        @Override
        public void onDownLoadSuccess() {
            model.pass();
            view.getUiHandler().post(view::finishActivity);
        }
    };

    public WifiPresenter(WifiModel model, WifiView view) {
        super(model, view);
        view.showTutorialButton();
        view.showInitialDialog();
    }

    public void onPause() {
        model.unRegisterReceiver();
        model.clearAllData();
    }

    public void onDestroy() {
        view.onDestroy();
    }

    public void onStop() {
        model.interruptBackgroundTasks();
    }

    /**
     * Configured for WPA
     */
    private void startConnectionQueue() {
        /*Checking coarse location permissions*/
        if (model.checkCoarseLocationPermissionsAvailable()) {
            model.startConnectionQueueInRangeOrder();
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        /* Check wifi data status */
        if (model.isWifiListEmpty()) {
            model.fail();
            view.finishActivity();
        } else {
            model.setModelCallback(modelCallback);
            // Register receiver
            model.registerReceiver();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        } else {
            model.requestPermissions();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent  event) {
        if (!model.isBroadcastRegistered()) {
            model.registerReceiver();
        }
        model.setModelState(WifiModel.STATE_CONNECTION_IDLE);
        model.startConnectionQueueInRangeOrder();
    }

    @Subscribe
    public void onFailureBtnClicked(WifiView.OnFailureBtnClicked event) {
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void OnOkTransferRateBtnClicked(WifiView.OnOkTransferRateBtnClicked event) {
        view.hideBannerTop();
        view.hideTutorialButton();
    }

    @Subscribe
    public void onOkResultClicked(WifiView.OnOkResultClicked event) {
        if (model.shouldMeasureTransferRate()) {
            view.hideTutorialButton();
            view.setSsidValue(model.getSsidConnected());
            view.showTransferContainer();
            view.showTransferRateDialog();
            model.uploadFile(wifiTransferCallback);
        } else {
            model.pass();
            view.finishActivity();
        }
    }
}