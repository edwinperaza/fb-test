package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.SpeakerModel;
import com.facebook.devices.mvp.view.SpeakerView;

import org.greenrobot.eventbus.Subscribe;

public class SpeakerPresenter extends PresenterTestBase<SpeakerModel, SpeakerView> {

    public SpeakerPresenter(SpeakerModel model, SpeakerView view) {
        super(model, view);

        view.showDialogOne();
        view.showTutorialButton();
        model.setDefaultVolume();
        model.setCallback(view::drawAudioSample);
    }

    public void onResume() {
        model.play();
    }

    public void onPaused() {
        model.stop();
    }


    @Subscribe
    public void onDialogOnePassEvent(SpeakerView.DialogOnePassEvent event) {
        model.setQuestionState(SpeakerModel.QUESTION_STATE_2);
        view.showDialogTwo();
    }

    @Subscribe
    public void onDialogOneFailEvent(SpeakerView.DialogOneFailEvent event) {
        fail();
    }

    @Subscribe
    public void onDialogTwoPassEvent(SpeakerView.DialogTwoPassEvent event) {
        model.stop();
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onDialogTwoFailEvent(SpeakerView.DialogTwoFailEvent event) {
        fail();
    }

    private void fail() {
        model.fail();
        view.finishActivity();
    }
}