package com.facebook.devices.mvp.model;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioPlayer;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class MicrophonePlaybackRipleyModel extends ServiceModel {

    public static final String RECORDED_BUFFER = "RecordedBuffer";
    private final int SAMPLE_RATE = 44100; /*khz*/

    private short[] bufferData = new short[0];

    private AudioRecorder audioRecorder;
    private AudioPlayer audioPlayer;
    private RuntimePermissionHandler permissionHandler;

    @IntDef({STATE_RECORDING, STATE_PLAYBACK})
    @interface State {
    }

    public static final int STATE_RECORDING = 0;
    public static final int STATE_PLAYBACK = 1;

    @State private int actualState = STATE_RECORDING;
    private VolumeManager volumeManager;

    public short[] getRecordedBuffer() {
        return bufferData;
    }

    public void setRecordedBuffer(short[] bufferData) {
        this.bufferData = bufferData;
    }

    @State
    public int getActualState() {
        return actualState;
    }

    /**
     * Normal: no bufferData (Record mode)
     * Playback: available bufferData (Playback mode)
     */
    public MicrophonePlaybackRipleyModel(
            RuntimePermissionHandler permissionHandler,
            short[] bufferData,
            @NonNull AudioRecorder audioRecorder,
            @NonNull AudioPlayer audioPlayer,
            VolumeManager volumeManager) {
        this.permissionHandler = permissionHandler;

        /*Audio Recorder*/
        this.audioRecorder = audioRecorder;
        this.audioRecorder.setSampleRate(SAMPLE_RATE);
        this.audioRecorder.setMode(AudioRecorder.MODE_RECORD);

        /*Audio player*/
        this.audioPlayer = audioPlayer;
        this.audioPlayer.setSampleRate(SAMPLE_RATE);

        /*Buffer data recovering */
        if (bufferData != null && bufferData.length > 0) {
            this.bufferData = bufferData;
            this.actualState = STATE_PLAYBACK;
        }

        this.volumeManager = volumeManager;
    }

    public void setDefaultVolume() {
        volumeManager.setDefaultMediaVolume();
    }

    public void stopRecording() {
        if (audioRecorder.isRunning()) {
            audioRecorder.stopRecording();
        }
    }

    public void startRecording(AudioRecorder.AudioRecorderListener recorderListener) {
        if (!audioRecorder.isRunning() && !audioPlayer.isRunning()) {
            actualState = STATE_RECORDING;
            audioRecorder.startRecording(recorderListener);
        }
    }

    public void stopPlayback() {
        if (audioPlayer.isRunning()) {
            audioPlayer.stopPlaying();
        }
    }

    public void startPlayback(AudioPlayer.AudioPlayerListener audioPlayerListener, short[] bufferData) {
        if (!audioRecorder.isRunning() && !audioPlayer.isRunning()) {
            actualState = STATE_PLAYBACK;
            audioPlayer.startPlaying(audioPlayerListener, bufferData);
        }
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }
}
