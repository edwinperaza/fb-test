package com.facebook.devices.mvp.model;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.support.annotation.IntRange;
import android.support.annotation.StringDef;
import android.util.Log;

import com.facebook.devices.R;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LedModel extends ServiceModel {

    private Context context;

    /**
     * LED location
     */
    @StringDef({LED_CAMERA, LED_SIDE_1})
    @Retention(RetentionPolicy.SOURCE)
    @interface LedLocations {
    }

    public static final String LED_CAMERA = "cam";
    public static final String LED_SIDE_1 = "side_1";

    @LedLocations private String ledLocation;

    /**
     * LED Mode
     */
    @StringDef({STATE_RED, STATE_GREEN, STATE_BLUE})
    @Retention(RetentionPolicy.SOURCE)
    @interface LedState {
    }

    public static final String STATE_RED = "_red";
    public static final String STATE_GREEN = "_green";
    public static final String STATE_BLUE = "_blue";

    @LedState private String actualColorTesting = STATE_RED;

    /**
     * LED Mode available values
     */
    @IntDef({LED_ON, LED_OFF})
    @Retention(RetentionPolicy.SOURCE)
    @interface LedValues {
    }

    public static final int LED_ON = 255;
    public static final int LED_OFF = 0;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    private class FlashingLedThread extends Thread {

        public final long INTERVAL_MILLIS = 2000; /*2 seconds*/

        private boolean isRunning = true;
        private LedCallback ledCallback;
        private Handler uiHandler;
        @LedState private String ledState;

        public FlashingLedThread(@LedState String ledState, Handler uiHandler, LedCallback ledCallback) {
            this.ledState = ledState;
            this.uiHandler = uiHandler;
            this.ledCallback = ledCallback;
        }

        @Override
        public void run() {
            super.run();
            try {
                while (isRunning) {

                    /*Turn LED ON*/
                    changeLEDState(ledState, LED_ON);
                    Thread.sleep(INTERVAL_MILLIS);

                    /*Turn LED OFF*/
                    changeLEDState(ledState, LED_OFF);

                    /*Listener call*/
                    if (ledCallback != null) {
                        uiHandler.post(() -> ledCallback.onLedLifecycleCompleted(ledState));
                    }

                    /*Kill thread*/
                    kill();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                kill();
            }
        }

        public void kill() {
            this.isRunning = false;
        }

    }

    public interface LedCallback {
        void onLedLifecycleCompleted(@LedState String ledState);
    }

    public String getPrettyLEDLocation() {
        switch (ledLocation) {
            case LED_CAMERA:
                return context.getString(R.string.led_camera_pretty);

            case LED_SIDE_1:
                return context.getString(R.string.led_side_1_pretty);

            /*Add more if necessary*/
        }
        return "";
    }

    public LedModel(Context context, @LedLocations String ledLocation) {
        this.context = context;
        this.ledLocation = ledLocation;
    }

    public void setLedLocation(@LedLocations String location) {
        this.ledLocation = location;
    }

    @LedLocations
    public String getCurrentLed() {
        return ledLocation;
    }

    @LedState
    public String getCurrentState() {
        return actualColorTesting;
    }

    public void resetLedState() {
        actualColorTesting = STATE_RED;
    }

    /**
     * @return if the color has changed or not
     */
    public boolean jumpNextColor() {
        switch (actualColorTesting) {
            case STATE_RED:
                actualColorTesting = STATE_GREEN;
                return true;

            case STATE_GREEN:
                actualColorTesting = STATE_BLUE;
                return true;
        }
        return false;
    }

    public void startLEDBehavior(@LedState String ledState, LedCallback callback) {
        FlashingLedThread ledThread = new FlashingLedThread(ledState, uiHandler, callback);
        ledThread.start();
    }

    private void changeLEDState(@LedState String ledState, @LedValues int value) {
        executeLEDCommand(ledLocation, ledState, value);
    }

    private void executeLEDCommand(@LedLocations String ledLocation, @LedState String ledState, @IntRange(from = 0, to = 255) int colorValue) {

        String ledName = ledLocation + ledState;
        String command = context.getString(R.string.led_base_dir, colorValue, ledName);

        try {
            Log.i("CommandToRun", command);
            Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("CommandExecute", e.getMessage());
        }
    }

}
