package com.facebook.devices.mvp.view;


import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BrightnessView extends TestViewBase {

    @BindView(R.id.brightness_seek_bar) SeekBar brightnessSeekBar;

    public BrightnessView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        setup();
    }

    private void setup() {
        /* Seek bar */
        brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                post(new OnProgressChanged(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                /*Nothing to do here*/
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                /*Nothing to do here*/
            }
        });
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.brightness_tutorial_dialog_text)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.brightness_init_dialog_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new OnSucceededBtnPressed()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new OnFailedBtnPressed()))
                        .build()
        );
    }


    /**
     * @param progress - this value should be between 0 and 255
     *                 according to  {@link android.provider.Settings.System#SCREEN_BRIGHTNESS}
     */
    public void setSeekBarProgress(int progress) {
        if (progress >= 0 && progress <= brightnessSeekBar.getMax()) {
            brightnessSeekBar.setProgress(progress);
        }
    }

    public Window getWindow() {
        return getActivity().getWindow();
    }

    public void applyWindowBrightness(int brightness) {
        /*Set value to the window*/
        WindowManager.LayoutParams layoutParams = getActivity().getWindow().getAttributes();
        layoutParams.screenBrightness = (float) brightness / 255; /*This attribute is managed in the value range of (0-1)*/
        getActivity().getWindow().setAttributes(layoutParams);
    }

    /**
     * Static classes for BUS
     */
    public static class OnFailedBtnPressed {/*Nothing to do here*/
    }

    public static class OnSucceededBtnPressed {/*Nothing to do here*/
    }

    public static class OnProgressChanged {
        private int progress;

        public OnProgressChanged(int progress) {
            this.progress = progress;
        }

        public int getProgress() {
            return progress;
        }
    }
}
