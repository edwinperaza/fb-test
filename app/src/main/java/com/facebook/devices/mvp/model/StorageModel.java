package com.facebook.devices.mvp.model;

import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.devices.R;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class StorageModel extends ServiceModel {

    private static final String TAG = StorageModel.class.getSimpleName();

    private byte pattern;
    private int storagePercentage;
    private RuntimePermissionHandler permissionHandler;
    private StorageProgress storageProgressCallback;
    private StatFs statFs;
    private volatile boolean forceCancel = false;
    private volatile boolean activityStopped = false;
    private int partsNumber = 2;
    private int initialProgressChecking = 50;

    private Integer[] imagesNumbers = {R.drawable.num_cero, R.drawable.num_one, R.drawable.num_two,
                                       R.drawable.num_three, R.drawable.num_four, R.drawable.num_five,
                                       R.drawable.num_six, R.drawable.num_seven, R.drawable.num_eight,
                                       R.drawable.num_nine};

    public StorageModel(byte pattern, int storagePercentage, RuntimePermissionHandler permissionHandler, StatFs statFs) {
        this.pattern = pattern;
        this.storagePercentage = storagePercentage;
        this.permissionHandler = permissionHandler;
        this.statFs = statFs;
    }

    public void setStorageProgress(@NonNull StorageProgress storageProgressCallback) {
        this.storageProgressCallback = storageProgressCallback;
    }

    public void runStorageTest() {
        new Thread(() -> {
            activityStopped = false;
            logValues();

            long availableBlocks = statFs.getAvailableBlocksLong();
            if (storagePercentage >= 1 && storagePercentage <= 100) {
                availableBlocks = (availableBlocks * storagePercentage / 100);
            }
            long blockSizeLong = statFs.getBlockSizeLong();
            float divider = (float) availableBlocks / 100 * partsNumber; //Split progress Bar in two parts

            long initialTime = System.currentTimeMillis();
            storageProgressCallback.onSetup();

            //Buffer Size
            byte[] bytesPatternArray = new byte[(int) blockSizeLong];
            ByteBuffer buffer = ByteBuffer.allocateDirect(bytesPatternArray.length);

            //Set value to append
            for (int i = 0; i < bytesPatternArray.length && !forceCancel && !activityStopped; i++) {
                bytesPatternArray[i] = pattern;
            }

            if (forceCancel) {
                storageProgressCallback.onFail("Canceled by user.");
                return;
            }

            if (activityStopped) {
                return;
            }

            /* Crate file to write pattern */
            File file = new File(Environment.getExternalStorageDirectory(), ".patternfile");

            // Init writing process
            try (RandomAccessFile writer = new RandomAccessFile(file, "rw")) {

                FileChannel channel = writer.getChannel();
                int writeRate = Math.round(availableBlocks / 1000);
                storageProgressCallback.onStart(availableBlocks);

                buffer.put(bytesPatternArray);

                long lastTimeUpdatedTime = System.currentTimeMillis();
                for (long writtenStorage = 1; writtenStorage <= availableBlocks && !forceCancel && !activityStopped; writtenStorage++) {
                    buffer.flip();
                    channel.write(buffer);
                    channel.force(true);

                    if (writtenStorage % writeRate == 0) {

                        long currentTime = System.currentTimeMillis();
                        long remainingTime = (availableBlocks * (currentTime - initialTime) / writtenStorage) - (currentTime - initialTime);

                        if (currentTime - lastTimeUpdatedTime >= TimeUnit.SECONDS.toMillis(1)) { /*Each second*/
                            lastTimeUpdatedTime = currentTime;
                            setImagesToShowFromRemainingTime(remainingTime);
                        }

                        storageProgressCallback.onProgress(writtenStorage / divider, (int) writtenStorage / partsNumber);
                    }
                }

                buffer.clear();
                writer.close();
                channel.close();

            } catch (IOException e) {
                storageProgressCallback.onFail(e.getMessage());
            }

            //If user forceCancel process while is writing, app avoids to continue with following steps
            if (forceCancel) {
                boolean fileDeleted = false;
                if (file.exists()) {
                    fileDeleted = file.delete();
                }
                storageProgressCallback.onFail("Canceled by user. File deleted: " + fileDeleted);
                return;
            }

            if (activityStopped) {
                if (file.exists()) {
                    file.delete();
                }
                return;
            }
            storageProgressCallback.onValidation();

            // Init Verification process
            try (RandomAccessFile reader = new RandomAccessFile(file, "r")) {

                FileChannel channelRead = reader.getChannel();
                int readRate = Math.round(availableBlocks / 1000);

                boolean success = true;

                long lastTimeUpdatedTime = System.currentTimeMillis();
                for (long readStorage = 1; readStorage <= availableBlocks && !forceCancel && !activityStopped; readStorage++) {
                    int bytesRead = channelRead.read(buffer);
                    buffer.flip();
                    byte[] bytesReadArray = new byte[bytesRead];
                    buffer.get(bytesReadArray, 0, bytesRead);

                    if (!Arrays.equals(bytesPatternArray, bytesReadArray)) {
                        Log.d(TAG, "Buffer different");
                        success = false;
                        break;
                    }

                    buffer.clear();

                    if (readStorage % readRate == 0) {
                        long currentTime = System.currentTimeMillis();
                        long remainingTime = (availableBlocks * (currentTime - initialTime) / readStorage) - (currentTime - initialTime);

                        if (currentTime - lastTimeUpdatedTime >= TimeUnit.SECONDS.toMillis(1)) { /*Each second*/
                            lastTimeUpdatedTime = currentTime;
                            setImagesToShowFromRemainingTime(remainingTime);
                        }

                        storageProgressCallback.onProgress(initialProgressChecking + (readStorage / divider), (int) ((availableBlocks + readStorage) / partsNumber));
                    }
                }

                reader.close();
                channelRead.close();

                //Always try to delete file if it exists
                if (file.exists()) {
                    file.delete();
                }

                if (forceCancel) {
                    storageProgressCallback.onFail("Canceled by user.");
                    return;
                }

                if (!activityStopped) {
                    if (success) {
                        storageProgressCallback.onSucceed();
                    } else {
                        storageProgressCallback.onPatternFail();
                    }
                }

            } catch (IOException e) {
                storageProgressCallback.onFail(e.getMessage());
            }
        }).start();
    }

    public void cancel() {
        forceCancel = true;
    }

    public void stop() {
        activityStopped = true;
    }

    private void logValues() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long availableBytes = statFs.getAvailableBytes();
        long availBlocks = statFs.getAvailableBlocksLong();
        long blockSize = statFs.getBlockSizeLong();
        long free_memory = availBlocks * blockSize;
        Log.d(TAG, "availBlocks: " + availBlocks);
        Log.d(TAG, "blockSize: " + blockSize);
        Log.d(TAG, "blockSize: " + GenericUtils.getDynamicSpace(blockSize));
        Log.d(TAG, "free memory: " + free_memory);
        Log.d(TAG, "free memory: " + GenericUtils.getDynamicSpace(free_memory));
        Log.d(TAG, "availableBytes: " + availableBytes);
        Log.d(TAG, "availableBytes: " + GenericUtils.getDynamicSpace(availableBytes));
    }

    private void setImagesToShowFromRemainingTime(long remainingTime) {
        long hour = TimeUnit.MILLISECONDS.toHours(remainingTime);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(remainingTime));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(remainingTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime));
        String hourStr = String.valueOf(hour);
        String minutesStr = String.valueOf(minutes);
        String secondsStr = String.valueOf(seconds);
        String hourOne = "0";
        String hourTwo = "0";
        String minOne = "0";
        String minTwo = "0";
        String secOne = "0";
        String secTwo = "0";

        if (hourStr.length() == 2) {
            hourOne = hourStr.substring(0, 1);
            hourTwo = hourStr.substring(1, 2);
        } else {
            hourTwo = hourStr.substring(0, 1);
        }

        if (minutesStr.length() == 2) {
            minOne = minutesStr.substring(0, 1);
            minTwo = minutesStr.substring(1, 2);
        } else {
            minTwo = minutesStr.substring(0, 1);
        }

        if (secondsStr.length() == 2) {
            secOne = secondsStr.substring(0, 1);
            secTwo = secondsStr.substring(1, 2);
        } else {
            secTwo = secondsStr.substring(0, 1);
        }

        storageProgressCallback.onRemainingTime(imagesNumbers[Integer.valueOf(hourOne)], imagesNumbers[Integer.valueOf(hourTwo)],
                imagesNumbers[Integer.valueOf(minOne)], imagesNumbers[Integer.valueOf(minTwo)],
                imagesNumbers[Integer.valueOf(secOne)], imagesNumbers[Integer.valueOf(secTwo)]);
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
       permissionHandler.requestPermission();
    }

    public interface StorageProgress {
        void onSetup();

        void onStart(long limit);

        void onValidation();

        void onSucceed();

        void onFail(String message);

        void onPatternFail();

        void onProgress(float progress, int position);

        void onRemainingTime(int hourOne, int hourTwo, int minOne, int minTwo, int secOne, int secTwo);
    }
}
