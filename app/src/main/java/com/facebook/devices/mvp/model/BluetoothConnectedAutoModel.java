package com.facebook.devices.mvp.model;

import android.app.Activity;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.ParcelUuid;
import android.util.Log;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class BluetoothConnectedAutoModel extends ServiceModel {

    private static final String TAG = BluetoothConnectedAutoModel.class.getSimpleName();
    public static final int REQUEST_COARSE_LOCATION_PERMISSION = 1;
    public static final ParcelUuid HSP =
            ParcelUuid.fromString("00001108-0000-1000-8000-00805F9B34FB");
    public static final ParcelUuid HANDS_FREE =
            ParcelUuid.fromString("0000111E-0000-1000-8000-00805F9B34FB");
    public static final int REQUEST_ENABLE_BT = 1;
    public static final String PERMISSION_BLUETOOTH_ACCEPTED = "permissionAccepted";

    private String deviceName = "";
    private String deviceAddress = "";
    private Uri audioResource;
    private Activity activity;
    private boolean permissionBluetoothAccepted = false;
    private boolean shouldPlayAudio;
    private BroadcastReceiver mReceiver;
    private BluetoothDevice device;
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothA2dp bluetoothA2dp;
    private BluetoothHeadset bluetoothHeadset;
    private MediaPlayer mPlayer;
    private final BusProvider.Bus bus;
    private Visualizer visualizer;
    private RuntimePermissionHandler permissionHandler;

    public BluetoothConnectedAutoModel(Activity activity, BusProvider.Bus bus, BluetoothAdapter bluetoothAdapter,
            boolean shouldPlayAudio, String deviceName, String deviceAddress,
            boolean permissionBluetoothAccepted, Uri audioResource,
            RuntimePermissionHandler permissionHandler) {
        this.activity = activity;
        this.bus = bus;
        this.shouldPlayAudio = shouldPlayAudio;
        this.deviceName = deviceName;
        this.bluetoothAdapter = bluetoothAdapter;
        this.deviceAddress = deviceAddress;
        this.permissionBluetoothAccepted = permissionBluetoothAccepted;
        this.audioResource = audioResource;


        this.permissionHandler = permissionHandler;
    }

    /**
     * The BroadcastReceiver that listens for discovered devices and when discovery is finished
     */
    public void registerReceiver() {
        Log.d(TAG, "registerReceiver()");
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action == null) {
                    return;
                }

                switch (action) {
                    case BluetoothDevice.ACTION_FOUND:
                        device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        Log.d(TAG, "ACTION_FOUND Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                        if (validDevice(device, deviceAddress)) {
                            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                                //Device was paired or bonded previously
                                bus.post(new SuccessBluetoothPaired(device));
                            } else {
                                connectDevice(device.getAddress());
                            }
                        }
                        break;
                    case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                        if (device != null) {
                            Log.d(TAG, "ACTION_BOND_STATE_CHANGED, Bond State: " + device.getBondState());
                            device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                            if (validDevice(device, deviceAddress)) {

                                if (device.getBondState() == BluetoothDevice.BOND_BONDED) { //remote device is bonded (paired), not necessarily connected.

                                    Log.d(TAG, "ACTION_BOND_STATE_CHANGED === BOND_BONDED");
                                    Log.d(TAG, "ACTION_FOUND Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                                    bus.post(new SuccessBluetoothPaired(device));

                                } else if (device.getBondState() == BluetoothDevice.BOND_NONE) { //remote device is not bonded (paired).

                                    Log.d(TAG, "ACTION_BOND_STATE_CHANGED === BOND_NONE");
                                    Log.d(TAG, "ACTION_FOUND Device Name: " + device.getName() + ", Device Address: " + device.getAddress());
                                    bus.post(new FailedBluetoothPaired());
                                }
                            }
                        }
                        break;
                    case BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED:

                        int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_DISCONNECTED);
                        if (state == BluetoothA2dp.STATE_CONNECTED) {
                            Log.d(TAG, "BluetoothA2dp ACTION_CONNECTION_STATE_CHANGED === STATE_CONNECTED");
                            bus.post(new SuccessBluetoothConnected());
                            try {
                                playMediaPlayer();
                                setupVisualizer();
                                bus.post(new PlaybackSuccessEvent());
                            } catch (Exception e) {
                                bus.post(new PlaybackErrorEvent());
                            }
                        } else if (state == BluetoothA2dp.STATE_DISCONNECTED) {
                            Log.d(TAG, "BluetoothA2dp ACTION_CONNECTION_STATE_CHANGED === STATE_DISCONNECTED");
                            stopMediaPlayer();
                        }
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        filter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);
        activity.registerReceiver(mReceiver, filter);
    }

    private void unregisterReceiver() {
        Log.d(TAG, "unregisterReceiver()");
        try {
            activity.unregisterReceiver(mReceiver);
        } catch (IllegalArgumentException ex) {
            Log.w(TAG, "Tried to unregister Receiver that was not registered.");
        }
    }

    public void onDestroy() {
        stopMediaPlayer();
        cancelDiscoveryProcess();
        unregisterReceiver();
        closeProfileProxy();
        if (device != null) {
            disconnectDevice(device.getAddress());
        }
        if (visualizer != null) {
            visualizer.release();
        }
    }

    private void connectDevice(String macAddress) {
        Log.d(TAG, "connectDevice()");
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(macAddress);
        try {
            bluetoothAdapter.cancelDiscovery();
            device.createBond();
        } catch (Exception e) {
            bus.post(new ConnectedBluetoothErrorEvent());
            Log.d(TAG, "connectDevice Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void disconnectDevice(String macAddress) {
        Log.d(TAG, "disconnectDevice()");
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(macAddress);
        try {
            removeBond(device);
        } catch (Exception e) {
            Log.d(TAG, "disconnectDevice Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean removeBond(BluetoothDevice btDevice) throws Exception {
        Log.d(TAG, "removeBond()");
        Class btClass = Class.forName("android.bluetooth.BluetoothDevice");
        Method removeBondMethod = btClass.getMethod("removeBond");
        return (Boolean) removeBondMethod.invoke(btDevice);
    }

    /**
     * If the device was not found or the device was found but is not bonded or paired we start
     * discovery again
     */
    public void doDiscovery() {
        Log.d(TAG, "doDiscovery()");
        //todo ojo
        if (device == null || device.getBondState() != BluetoothDevice.BOND_BONDED) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
            // Request discover from BluetoothAdapter
            bluetoothAdapter.startDiscovery();
        }
    }

    public void cancelDiscoveryProcess() {
        Log.d(TAG, "cancelDiscoveryProcess()");
        if ((bluetoothAdapter != null) && (bluetoothAdapter.isDiscovering())) {
            bluetoothAdapter.cancelDiscovery();
        }
    }

    private boolean validDevice(BluetoothDevice device, String address) {
        return device.getAddress().equals(address);
    }

    public boolean isAvailableBluetoothAdapter() {
        return bluetoothAdapter != null;
    }

    public boolean isEnabledBluetoothAdapter() {
        return bluetoothAdapter.isEnabled();
    }

    public void enabledBluetoothAdapter() {
        bluetoothAdapter.enable();
    }

    public boolean isPermissionGranted() {
        Log.d("BT_FACE", "Model isPermissionGranted");
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        Log.d("BT_FACE", "Model shouldRequestPermissions");
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        Log.d("BT_FACE", "Model requestPermissions");
        permissionHandler.requestPermission();
    }

    public boolean shouldPlayAudio() {
        return shouldPlayAudio;
    }

    public boolean wasPermissionBluetoothAccepted() {
        return permissionBluetoothAccepted;
    }

    public void permissionBluetoothAccepted() {
        this.permissionBluetoothAccepted = true;
    }

    public boolean isPermissionGranted(int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Static classes for BUS
     */
    public static class SuccessBluetoothPaired {/*Nothing to do here*/
        BluetoothDevice device;

        public SuccessBluetoothPaired(BluetoothDevice device) {
            this.device = device;
        }

        public BluetoothDevice getDevice() {
            return device;
        }
    }

    public static class FailedBluetoothPaired {/*Nothing to do here*/
    }

    public static class SuccessBluetoothConnected {/*Nothing to do here*/
    }

    public static class WaveFormDataCapture {
        byte[] waveform;

        public WaveFormDataCapture(byte[] waveform) {
            this.waveform = waveform;
        }

        public byte[] getWaveform() {
            return waveform;
        }
    }

    public static class PlaybackErrorEvent {/*Nothing to do here*/
    }

    public static class PlaybackSuccessEvent {/*Nothing to do here*/
    }

    public static class ConnectedBluetoothErrorEvent {/*Nothing to do here*/
    }

    public static class ProfileBluetoothSuccessEvent {/*Nothing to do here*/
    }

    public static class ProfileBluetoothErrorEvent {/*Nothing to do here*/
    }

    /**
     * Method related to connect and stream audio from phone to speaker
     */
    public void setUpProfileProxy() {
        Log.d(TAG, "setUpProfileProxy()");
        if (bluetoothAdapter != null) {
            if (device.fetchUuidsWithSdp()) {
                ParcelUuid[] uuidDevice = device.getUuids();
                boolean hasProfile = false;
                for (ParcelUuid uuid : uuidDevice) {
                    if ((uuid.equals(HSP) || uuid.equals(HANDS_FREE)) &&
                            device.getBluetoothClass().getDeviceClass() == BluetoothClass.Device.AUDIO_VIDEO_WEARABLE_HEADSET) {
                        bluetoothAdapter.getProfileProxy(activity, profileServiceListener, BluetoothProfile.HEADSET);
                        hasProfile = true;
                        bus.post(new ProfileBluetoothSuccessEvent());
                        break;
                    }
                }
                if (!hasProfile) {
                    bus.post(new ProfileBluetoothErrorEvent());
                }
            } else {
                bus.post(new ProfileBluetoothErrorEvent());
            }
        } else {
            bus.post(new ProfileBluetoothErrorEvent());
        }
    }

    private void closeProfileProxy() {
        Log.d(TAG, "closeProfileProxy()");
        if (bluetoothAdapter != null) {
            //bluetoothAdapter.closeProfileProxy(BluetoothProfile.A2DP, bluetoothA2dp);
            bluetoothAdapter.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset);

        }
    }

    /**
     * Wrapper around some reflection code to get the hidden 'connect()' method
     *
     * @return the connect(BluetoothDevice) method, or null if it could not be found
     */
    private Method getConnectMethod() {
        try {
            return BluetoothA2dp.class.getDeclaredMethod("connect", BluetoothDevice.class);
        } catch (NoSuchMethodException ex) {
            Log.e(TAG, "Unable to find connect(BluetoothDevice) method in BluetoothA2dp proxy.");
            return null;
        }
    }

    private Method getConnectMethodHeadset() {
        try {
            return BluetoothHeadset.class.getDeclaredMethod("connect", BluetoothDevice.class);
        } catch (NoSuchMethodException ex) {
            Log.e(TAG, "Unable to find connect(BluetoothDevice) method in BluetoothA2dp proxy.");
            return null;
        }
    }

    /**
     * Search the set of bonded devices in the BluetoothAdapter for one that matches
     * the given name
     *
     * @param adapter       the BluetoothAdapter whose bonded devices should be queried
     * @param deviceAddress the address of the device to search for
     * @return the BluetoothDevice by the given name (if found); null if it was not found
     */
    private static BluetoothDevice findBondedDeviceByAddress(BluetoothAdapter adapter, String deviceAddress) {
        for (BluetoothDevice device : getBondedDevices(adapter)) {
            if (deviceAddress.matches(device.getAddress())) {
                Log.v(TAG, String.format("Found device with name %s and address %s.", device.getName(), device.getAddress()));
                return device;
            }
        }
        Log.w(TAG, String.format("Unable to find device with address %s.", deviceAddress));
        return null;
    }

    /**
     * Safety wrapper around BluetoothAdapter#getBondedDevices() that is guaranteed
     * to return a non-null result
     *
     * @param adapter the BluetoothAdapter whose bonded devices should be obtained
     * @return the set of all bonded devices to the adapter; an empty set if there was an error
     */
    private static Set<BluetoothDevice> getBondedDevices(BluetoothAdapter adapter) {
        Set<BluetoothDevice> results = adapter.getBondedDevices();
        if (results == null) {
            results = new HashSet<>();
        }
        return results;
    }

    private BluetoothProfile.ServiceListener profileServiceListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile bluetoothProfile) {
            Log.d(TAG, "a2dp service connected. profile = " + profile);

            BluetoothDevice device = findBondedDeviceByAddress(bluetoothAdapter, deviceAddress);
            switch (profile) {
                case BluetoothProfile.A2DP:
                    bluetoothA2dp = (BluetoothA2dp) bluetoothProfile;
                    Method connect = getConnectMethod();
                    if (connect == null || device == null) {
                        return;
                    }

                    try {
                        connect.setAccessible(true);
                        connect.invoke(bluetoothA2dp, device);
                    } catch (InvocationTargetException ex) {
                        Log.e(TAG, "Unable to invoke connect(BluetoothDevice) method on proxy. " + ex.toString());
                    } catch (IllegalAccessException ex) {
                        Log.e(TAG, "Illegal Access! " + ex.toString());
                    }
                    break;
                case BluetoothProfile.HEADSET:
                    bluetoothHeadset = (BluetoothHeadset) bluetoothProfile;
                    Method connectHeadset = getConnectMethodHeadset();
                    if (connectHeadset == null || device == null) {
                        return;
                    }

                    try {
                        connectHeadset.setAccessible(true);
                        connectHeadset.invoke(bluetoothHeadset, device);
                    } catch (InvocationTargetException ex) {
                        Log.e(TAG, "Unable to invoke connect(BluetoothDevice) method on proxy. " + ex.toString());
                    } catch (IllegalAccessException ex) {
                        Log.e(TAG, "Illegal Access! " + ex.toString());
                    }
                    break;
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
        }

    };

    private void playMediaPlayer() throws IOException {
        mPlayer = MediaPlayer.create(activity, audioResource);
        mPlayer.start();
    }

    private void stopMediaPlayer() {
        Log.d(TAG, "stopMediaPlayer()");
        if ((mPlayer != null) && (mPlayer.isPlaying())) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    private void setupVisualizer() {
        visualizer = new Visualizer(mPlayer.getAudioSessionId());
        visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        visualizer.setDataCaptureListener(visualizerCapture, Visualizer.getMaxCaptureRate() / 2, true, false);
        visualizer.setEnabled(true);
    }

    private Visualizer.OnDataCaptureListener visualizerCapture = new Visualizer.OnDataCaptureListener() {
        @Override
        public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
            if (waveform.length > 0) {
                bus.post(new WaveFormDataCapture(waveform));
            }
        }

        @Override
        public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
            /*Nothing to do here*/
        }
    };
}