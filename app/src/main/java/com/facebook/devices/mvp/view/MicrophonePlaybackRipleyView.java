package com.facebook.devices.mvp.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.WaveGraphView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MicrophonePlaybackRipleyView extends TestViewBase {

    @BindView(R.id.wave_view) WaveGraphView waveGraphView;
    @BindView(R.id.iv_floating_state) ImageView ivFloatingState;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public MicrophonePlaybackRipleyView(TestBaseActivity baseActivity, BusProvider.Bus bus) {
        super(baseActivity, bus);
        ButterKnife.bind(this, baseActivity);
    }

    public Handler getUIHandler() {
        return uiHandler;
    }

    @Override
    public void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.microphone_playback_ripley_dialog_help_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showFloatingTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    private void changeStateImage(StateInfo stateInfo) {
        if (ivFloatingState.getVisibility() == View.VISIBLE) {
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(
                    ObjectAnimator.ofFloat(ivFloatingState, "scaleX", 1.3f),
                    ObjectAnimator.ofFloat(ivFloatingState, "scaleY", 1.3f));
            animatorSet.setDuration(300);
            animatorSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    ivFloatingState.setImageResource(stateInfo.drawRes);
                    ivFloatingState.setBackgroundTintList(getContext().getResources().getColorStateList(stateInfo.colorRes));
                    AnimatorSet animatorSet = new AnimatorSet();
                    animatorSet.playTogether(
                            ObjectAnimator.ofFloat(ivFloatingState, "scaleX", 1f),
                            ObjectAnimator.ofFloat(ivFloatingState, "scaleY", 1f));
                    animatorSet.setDuration(300);
                    animatorSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animatorSet.start();
        } else {
            ivFloatingState.setImageResource(stateInfo.drawRes);
            ivFloatingState.setBackgroundTintList(getContext().getResources().getColorStateList(stateInfo.colorRes));
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(
                    ObjectAnimator.ofFloat(ivFloatingState, "scaleX", 1f),
                    ObjectAnimator.ofFloat(ivFloatingState, "scaleY", 1f));
            animatorSet.setDuration(300);
            animatorSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    ivFloatingState.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animatorSet.start();
        }
    }

    public void showRecordingCircle() {
        ivFloatingState.setOnClickListener(null);
        changeStateImage(new StateInfo(R.drawable.mic_rec, R.color.red_one));
    }

    public void showPlayingCircle() {
        ivFloatingState.setOnClickListener(null);
        changeStateImage(new StateInfo(R.drawable.reproduce_audio, R.color.bright_aqua));
    }

    public void showRepeatPlaybackCircle() {
        ivFloatingState.setOnClickListener(v -> post(new RepeatEvent()));
        changeStateImage(new StateInfo(R.drawable.replay_audio, R.color.bright_aqua));
    }

    public void drawAudioSample(short[] samples) {
        try {
            waveGraphView.setSamples(samples);
        } catch (WaveGraphView.WaveViewException e) {
            Log.e("WaveView", e.getMessage());
        }
    }

    public void showRecordingDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.microphone_playback_ripley_record_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new RecordPassEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new RecordFailEvent()))
                        .build()
        );
    }

    public void showPlaybackDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.microphone_playback_ripley_play_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new PlaybackPassEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new PlaybackFailEvent()))
                        .build()
        );
    }

    private class StateInfo {
        @DrawableRes int drawRes;
        @ColorRes int colorRes;

        public StateInfo(@DrawableRes int drawRes, @ColorRes int colorRes) {
            this.drawRes = drawRes;
            this.colorRes = colorRes;
        }
    }

    /**
     * Static classes for BUS
     */
    public static class RecordPassEvent {
        // Nothing to do here
    }

    public static class RecordFailEvent {
        // Nothing to do here
    }

    public static class PlaybackPassEvent {
        // Nothing to do here
    }

    public static class PlaybackFailEvent {
        // Nothing to do here
    }

    public static class RepeatEvent {
        // Nothing to do here
    }
}
