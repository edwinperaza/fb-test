package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.BrightnessModel;
import com.facebook.devices.mvp.view.BrightnessView;

import org.greenrobot.eventbus.Subscribe;

public class BrightnessPresenter extends PresenterTestBase<BrightnessModel, BrightnessView> {

    public BrightnessPresenter(BrightnessModel model, BrightnessView view) {
        super(model, view);
    }

    public void onResume() {
        /*Get currently system brightness*/
        model.setBrightness(model.getMaxBrightness() / 2);
        view.setSeekBarProgress(model.getBrightness());
        view.showTutorialButton();
        view.showInitDialog();
    }

    @Subscribe
    public void onSucceededBtnPressed(BrightnessView.OnSucceededBtnPressed event) {
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onFailedBtnPressed(BrightnessView.OnFailedBtnPressed event) {
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void onProgressSeekBarChanged(BrightnessView.OnProgressChanged event) {
        model.setBrightness(event.getProgress());
        /*Apply to window the correct brightness with latest data*/
        view.applyWindowBrightness(model.getBrightness());
    }

}