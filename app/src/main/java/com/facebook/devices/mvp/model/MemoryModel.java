package com.facebook.devices.mvp.model;

import android.app.Activity;
import android.app.ActivityManager;
import android.support.annotation.NonNull;

import com.facebook.devices.R;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ACTIVITY_SERVICE;

public class MemoryModel extends ServiceModel {

    private Activity context;
    private byte pattern;
    private MemoryProgress memoryProgressCallback;
    private volatile boolean forceCancel = false;
    private volatile boolean activityStopped = false;
    private int partsNumber = 2;
    private int initialProgressChecking = 50;
    private long initialTime;

    private Integer[] imagesNumbers = {R.drawable.num_cero, R.drawable.num_one, R.drawable.num_two,
                                       R.drawable.num_three, R.drawable.num_four, R.drawable.num_five,
                                       R.drawable.num_six, R.drawable.num_seven, R.drawable.num_eight,
                                       R.drawable.num_nine};

    public MemoryModel(byte pattern, Activity activity) {
        this.pattern = pattern;
        this.context = activity;
    }

    public void setMemoryProgressCallback(@NonNull MemoryProgress memoryProgressCallback) {
        this.memoryProgressCallback = memoryProgressCallback;
    }

    public void runMemoryTest() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                activityStopped = false;

                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();
                initialTime = System.currentTimeMillis();
                memoryProgressCallback.onSetup(memoryInfo.threshold);

                ByteBuffer out = ByteBuffer.allocateDirect((int) memoryInfo.threshold);
                int limit = out.remaining();
                memoryProgressCallback.onStart(limit);

                int rate = Math.round(memoryInfo.threshold / 1000);
                float divider = limit / 100 * partsNumber; //Split progress Bar in two parts

                /* Memory write */
                long lastTimeUpdatedTime = System.currentTimeMillis();

                for (int writtenMemory = 1; writtenMemory <= limit && !forceCancel && !activityStopped; writtenMemory++) {
                    out.put(pattern);

                    if (writtenMemory % rate == 0) {
                        long currentTime = System.currentTimeMillis();
                        long remainingTime = (limit * (currentTime - initialTime) / writtenMemory) - (currentTime - initialTime);

                        if (currentTime - lastTimeUpdatedTime >= TimeUnit.SECONDS.toMillis(1)) { /*Each second*/
                            lastTimeUpdatedTime = currentTime;
                            setImagesToShowFromRemainingTime(remainingTime);
                        }
                        memoryProgressCallback.onProgress(writtenMemory / divider, writtenMemory / partsNumber);
                    }
                }

                if (forceCancel) {
                    memoryProgressCallback.onFail("Canceled by the user", (byte) 0);
                    return;
                }

                if (activityStopped) {
                    return;
                }

                memoryProgressCallback.onValidation();

                /* Memory validation */
                lastTimeUpdatedTime = System.currentTimeMillis();
                for (int readMemory = 1; readMemory <= limit && !forceCancel && !activityStopped; readMemory++) {
                    byte m = out.get(readMemory - 1);

                    if (m != pattern) {
                        // Memory failure
                        memoryProgressCallback.onFail(String.format("Error, Excepted pattern 0x%2x but found 0x%2x %s", pattern, m, buildMemoryInfo(memoryInfo)), m);
                        return;
                    }

                    if (readMemory % rate == 0) {
                        long currentTime = System.currentTimeMillis();
                        long remainingTime = (limit * (currentTime - initialTime) / readMemory) - (currentTime - initialTime);

                        if (currentTime - lastTimeUpdatedTime >= TimeUnit.SECONDS.toMillis(1)) { /*Each second*/
                            lastTimeUpdatedTime = currentTime;
                            setImagesToShowFromRemainingTime(remainingTime);
                        }
                        memoryProgressCallback.onProgress((initialProgressChecking + (readMemory / divider)), ((limit + readMemory) / partsNumber));
                    }
                }

                if (forceCancel) {
                    memoryProgressCallback.onFail("Canceled by the user", (byte) 0);
                    return;
                }

                if (activityStopped) {
                    return;
                }

                memoryProgressCallback.onSucceed(buildMemoryInfo(memoryInfo));
            }
        }.start();
    }

    public void cancel() {
        forceCancel = true;
    }

    public void stop() {
        activityStopped = true;
    }

    // Get a MemoryInfo object for the device's current memory status.
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
        }
        return memoryInfo;
    }

    private String buildMemoryInfo(ActivityManager.MemoryInfo memoryInfo) {
        return String.format("Available Memory: %s, Threshold: %s , Total memory: %s, Low memory: %s",
                GenericUtils.getDynamicSpace(memoryInfo.availMem),
                GenericUtils.getDynamicSpace(memoryInfo.threshold),
                GenericUtils.getDynamicSpace(memoryInfo.totalMem),
                memoryInfo.lowMemory);
    }

    public byte getPattern() {
        return pattern;
    }

    private void setImagesToShowFromRemainingTime(long remainingTime) {
        long hour = TimeUnit.MILLISECONDS.toHours(remainingTime);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(remainingTime));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(remainingTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime));
        String hourStr = String.valueOf(hour);
        String minutesStr = String.valueOf(minutes);
        String secondsStr = String.valueOf(seconds);
        String hourOne = "0";
        String hourTwo = "0";
        String minOne = "0";
        String minTwo = "0";
        String secOne = "0";
        String secTwo = "0";

        if (hourStr.length() == 2) {
            hourOne = hourStr.substring(0, 1);
            hourTwo = hourStr.substring(1, 2);
        } else {
            hourTwo = hourStr.substring(0, 1);
        }

        if (minutesStr.length() == 2) {
            minOne = minutesStr.substring(0, 1);
            minTwo = minutesStr.substring(1, 2);
        } else {
            minTwo = minutesStr.substring(0, 1);
        }

        if (secondsStr.length() == 2) {
            secOne = secondsStr.substring(0, 1);
            secTwo = secondsStr.substring(1, 2);
        } else {
            secTwo = secondsStr.substring(0, 1);
        }

        memoryProgressCallback.onRemainingTime(imagesNumbers[Integer.valueOf(hourOne)], imagesNumbers[Integer.valueOf(hourTwo)],
                imagesNumbers[Integer.valueOf(minOne)], imagesNumbers[Integer.valueOf(minTwo)],
                imagesNumbers[Integer.valueOf(secOne)], imagesNumbers[Integer.valueOf(secTwo)]);
    }

    public interface MemoryProgress {
        void onSetup(long threshold);

        void onStart(int limit);

        void onValidation();

        void onSucceed(String output);

        void onFail(String s, byte cell);

        void onProgress(float progress, int position);

        void onRemainingTime(int hourOne, int hourTwo, int minOne, int minTwo, int secOne, int secTwo);
    }
}