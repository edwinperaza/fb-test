package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StorageView extends TestViewBase {

    @BindView(R.id.progress_layout) ConstraintLayout progressLayout;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.tv_percentage) TextView percentage;
    @BindView(R.id.tv_label) TextView label;
    @BindView(R.id.iv_hour_digit_one) ImageView ivHourOne;
    @BindView(R.id.iv_hour_digit_two) ImageView ivHourTwo;
    @BindView(R.id.iv_minutes_digit_one) ImageView ivMinOne;
    @BindView(R.id.iv_minutes_digit_two) ImageView ivMinTwo;
    @BindView(R.id.iv_seconds_digit_one) ImageView ivSecOne;
    @BindView(R.id.iv_seconds_digit_two) ImageView ivSecTwo;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public StorageView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    public void updateProgressBar(int progress) {
        progressBar.setProgress(progress);
    }

    public void updatePercentage(double percentageValue) {
        percentage.setText(String.format("%.1f%%", percentageValue));
    }

    public void showValidation() {
        label.setText(R.string.storage_validation);
    }

    public void patternFail() {
        label.setText(R.string.storage_fail);
    }

    public void setupView() {
        label.setText(R.string.allocate_storage);
    }

    public void setMax(long max) {
        progressBar.setMax((int) max);
    }

    public void showStart() {
        label.setText(R.string.start_storage_test);
    }

    public void setMin(int min) {
        progressBar.setMax(min);
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void showRemainingTime(int hourOne, int hourTwo,
                                  int minOne, int minTwo,
                                  int secOne, int secTwo) {
        ivHourOne.setImageDrawable(getContext().getDrawable(hourOne));
        ivHourTwo.setImageDrawable(getContext().getDrawable(hourTwo));
        ivMinOne.setImageDrawable(getContext().getDrawable(minOne));
        ivMinTwo.setImageDrawable(getContext().getDrawable(minTwo));
        ivSecOne.setImageDrawable(getContext().getDrawable(secOne));
        ivSecTwo.setImageDrawable(getContext().getDrawable(secTwo));
    }

    @Override
    public void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.storage_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showWritingDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.storage_writing_question))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showValidationDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.storage_validation_question))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public static class OnButtonFailurePressed {/*Nothing to do here*/
    }

}