package com.facebook.devices.mvp.model;


import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.provider.Settings;
import android.support.annotation.IntDef;

import com.facebook.devices.R;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class VolumeButtonsAndSoundModel extends ServiceModel {

    @IntDef({MODE_UP, MODE_DOWN})
    @interface State {
    }

    public static final int MODE_UP = 0;
    public static final int MODE_DOWN = 1;

    public static final int MAX_TIME_PRESSING = 3;
    private static final int SPECIFIC_STREAM = AudioManager.STREAM_RING;

    @State private int currentMode;
    private Context context;
    private int initVolume = 0;
    private int touchedTimes = 0;

    private boolean audioLoaded = false;
    private int soundId = 0;
    private AudioManager audioManager;
    private SoundPool soundPool;

    public VolumeButtonsAndSoundModel(Context context) {
        this.context = context;
        this.currentMode = MODE_UP;
    }

    public void init() {
        /*Audio manager*/
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        try {
            initVolume = getCurrentVolume();
            audioManager.setStreamVolume(SPECIFIC_STREAM, getMaxVolume() / 2, AudioManager.FLAG_VIBRATE); /*Start at aprox 50%*/

            /*Sound pool*/
            AudioAttributes audioAttr = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();

            SoundPool.Builder builder = new SoundPool.Builder();
            builder.setAudioAttributes(audioAttr).setMaxStreams(1);

            soundPool = builder.build();
            soundPool.setOnLoadCompleteListener((soundPool, sampleId, status) -> audioLoaded = true);
            soundId = soundPool.load(context, R.raw.sound_volume, 1);
        } catch (SecurityException e) {
            context.startActivity(new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS));
        }
    }

    public boolean isModeUp() {
        return currentMode == MODE_UP;
    }

    public boolean isModeDown() {
        return currentMode == MODE_DOWN;
    }

    private void setNextMode() {
        currentMode = (currentMode + 1 > MODE_DOWN) ? MODE_DOWN : currentMode + 1;
    }

    public int getTouchedTimes() {
        return touchedTimes;
    }

    public int incrementTouchCount() {
        if (touchedTimes < MAX_TIME_PRESSING) {
            touchedTimes++;
        }
        return touchedTimes;
    }

    public int decrementTouchCount() {
        if (touchedTimes > 0) {
            touchedTimes--;
        }
        return touchedTimes;
    }

    public boolean checkForNextStep() {
        if (touchedTimes == MAX_TIME_PRESSING || touchedTimes == 0) {
            setNextMode();
            return true;
        }
        return false;
    }

    @State
    public int getCurrentMode() {
        return currentMode;
    }

    /* Methods related to volume */
    public int getSpecificStream() {
        return SPECIFIC_STREAM;
    }

    public int getCurrentVolume() {
        return audioManager.getStreamVolume(SPECIFIC_STREAM);
    }

    private int getMaxVolume() {
        return audioManager.getStreamMaxVolume(SPECIFIC_STREAM);
    }

    public void volumeUp() {
        int maxVolume = getMaxVolume();
        int currentVolume = getCurrentVolume();
        double valueToRaise = (currentVolume + maxVolume * 0.1 > maxVolume) ? maxVolume : (currentVolume + maxVolume * 0.1);
        audioManager.setStreamVolume(SPECIFIC_STREAM, (int) Math.round(valueToRaise), AudioManager.FLAG_VIBRATE);
    }

    public void volumeDown() {
        int maxVolume = getMaxVolume();
        int currentVolume = getCurrentVolume();
        double valueToLower = (currentVolume - maxVolume * 0.1 < 0) ? 0 : (currentVolume - maxVolume * 0.1);
        audioManager.setStreamVolume(SPECIFIC_STREAM, (int) Math.round(valueToLower), AudioManager.FLAG_VIBRATE);
    }

    public void playSound() {
        if (audioLoaded) {
            float perc = (float) getCurrentVolume() / getMaxVolume();
            float leftVolume, rightVolume;
            leftVolume = rightVolume = perc;
            soundPool.play(soundId, leftVolume, rightVolume, 1, 0, 1f);
        }
    }

    public void resetDefaultValues() {
        audioManager.setStreamVolume(SPECIFIC_STREAM, initVolume, AudioManager.FLAG_VIBRATE);
    }
}
