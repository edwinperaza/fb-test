package com.facebook.devices.mvp.model;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class PreviewImagesModel extends ServiceModel {

    private final static long PAUSE_TIME_MILLIS = 5000;
    private final static long PAUSE_TUTORIAL_TIME_MILLIS = 3000;

    private int index = 0;
    private Uri[] images;
    private Context context;
    private PermissionHandler permissionHandler;

    public PreviewImagesModel(Context context, PermissionHandler permissionHandler, Uri... images) {
        this.context = context;
        this.permissionHandler = permissionHandler;
        this.images = images;
    }

    public int getNumberOfImages() {
        return images.length;
    }

    public void resetPosition() {
        index = 0;
    }

    public long getPauseTimeMillis() {
        return PAUSE_TIME_MILLIS;
    }

    public long getPauseTutorialTimeMillis() {
        return PAUSE_TUTORIAL_TIME_MILLIS;
    }

    public boolean isLastImage() {
        return index == images.length - 1;
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    public void setImagePosition(int resourcePosition) {
        this.index = resourcePosition;
    }

    public int getImagePosition() {
        return index;
    }

    public Uri[] getImages() {
        return images;
    }

    public void moveNextImagePosition() {
        if (!isLastImage()) {
            index++;
        }
    }

    public void movePreviousImagePosition() {
        if (index > 0) {
            index--;
        }
    }

    public boolean shouldAskResult() {
        return index + 1 == images.length;
    }
}
