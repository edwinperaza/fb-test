package com.facebook.devices.mvp.presenter.utils;

import android.content.ServiceConnection;
import android.support.annotation.NonNull;

public interface AugmentedRealityPresenterInterface {
    ServiceConnection getServiceConnection();

    void onResume();

    void onPause();
}
