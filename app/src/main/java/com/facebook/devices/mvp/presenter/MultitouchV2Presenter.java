package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.MultitouchV2Model;
import com.facebook.devices.mvp.view.MultitouchV2View;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.utils.customviews.MultitouchCirclesView.MultiTouchDetectorViewCallback.FINISHED_DROPPED;
import static com.facebook.devices.utils.customviews.MultitouchCirclesView.MultiTouchDetectorViewCallback.FINISHED_SUCCESS;

public class MultitouchV2Presenter extends PresenterTestBase<MultitouchV2Model, MultitouchV2View> {

    public MultitouchV2Presenter(MultitouchV2Model model, MultitouchV2View view) {
        super(model, view);

        /*Init*/
        view.disableMultitouchView();
        view.showInitBanner();
        view.showFloatingTutorialButton();
    }

    @Subscribe
    public void onIntroAccepted(MultitouchV2View.OnIntroAccepted event) {
        view.hideBannerTop();
        view.hideFloatingTutorialButton();
        view.enableMultitouchView();
    }

    @Subscribe
    public void onMultitouchReplies(MultitouchV2View.OnMultitouchReplies event) {
        switch (event.getResultMode()) {
            case FINISHED_SUCCESS:
                model.pass();
                view.finishActivity();
                break;

            case FINISHED_DROPPED:
                view.showDroppedDialog();
                break;
        }
    }

    @Subscribe
    public void onFailureDialogOptionSelectedEvent(MultitouchV2View.OnFailureDialogOptionSelectedEvent event) {
        switch (event.getOptionSelected()) {
            case MultitouchV2View.OnFailureDialogOptionSelectedEvent.OPTION_FAIL:
                model.fail();
                view.finishActivity();
                break;
            case MultitouchV2View.OnFailureDialogOptionSelectedEvent.OPTION_RETRY:
                view.resetMultitouchView();
                break;
        }
    }
}
