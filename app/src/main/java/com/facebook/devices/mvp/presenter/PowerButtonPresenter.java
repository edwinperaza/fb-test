package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.PowerButtonModel;
import com.facebook.devices.mvp.view.PowerButtonView;

import org.greenrobot.eventbus.Subscribe;

public class PowerButtonPresenter extends PresenterTestBase<PowerButtonModel, PowerButtonView> {

    public PowerButtonPresenter(PowerButtonModel model, PowerButtonView view) {
        super(model, view);
        view.showTutorialButton(true);
        view.showInitDialog();
    }

    public void onResume() {
        if (model.wasStartedAlready()) {  /* Return from stand by */
            pass();
        } else {
            model.registerReceiver();
        }
    }

    public void onStop() {
        if (model.isReceiverRegistered()) {
            model.setAlreadyStarted();
        }
    }

    public void onDestroy() {
        if (model.isReceiverRegistered()) {
            model.unregisterReceiver();
        }
    }

    private void pass() {
        model.pass();
        view.finishActivity();
    }

    private void fail() {
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void onFailTestEvent(PowerButtonView.FailTestEvent event) {
        fail();
    }
}
