package com.facebook.devices.mvp.presenter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.facebook.devices.mvp.model.CameraCaptureModel;
import com.facebook.devices.mvp.model.CameraPrivacyModel;
import com.facebook.devices.mvp.view.CameraPrivacyView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

public class CameraPrivacyPresenter extends PresenterTestBase<CameraPrivacyModel, CameraPrivacyView> {

    public CameraPrivacyPresenter(CameraPrivacyModel model, CameraPrivacyView view) {
        super(model, view);
    }

    public void onStart() {
        if (model.isPermissionGranted()) {
            view.hideCameraAccessSpinner();
        }
    }

    public void onStop(boolean isChangingConfigurations) {
        model.getUiHandler().removeCallbacks();
        model.stopCamera(!isChangingConfigurations);
    }

    public void onDestroy() {
        /*Clear resources*/
        model.getResourcesManager().setSurfaceTextureUnavailable();
    }

    /* Called when view attached is resumed */
    public void onViewResumed() {
        /* Check for permissions */
        if (!model.isPermissionGranted()) {
            model.requestPermissions();
            return;
        }

        /*UI components*/
        view.showDialogOne();
        view.showFloatingTutorialButton();

        /* Run camera preview */
        openCamera();
    }

    /* Called when view attached is paused */
    public void onViewPaused() {
        if (model.isPermissionGranted()) {
            model.pauseCamera();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (model.isStopCameraBroadcastSent()) {
            outState.putString(CameraPrivacyModel.CAMERA_ID_RECEIVED, model.getResourcesManager().getCameraId());
        }
        outState.putBoolean(CameraPrivacyModel.PRIVACY_BUTTON_WORKING, model.isPrivacyWorking());
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CameraPrivacyModel.CAMERA_ID_RECEIVED)) {
            model.getResourcesManager().setCameraResourceReady(savedInstanceState.getString(CameraPrivacyModel.CAMERA_ID_RECEIVED));
        }

        if (savedInstanceState.containsKey(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)) {
            if (savedInstanceState.getBoolean(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)) {
                model.privacyButtonWorking();
            } else {
                model.privacyButtonFailed();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void openCamera() {
        view.showCameraAccessSpinner();
        model.openCamera(view.getTextureView());
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        view.hideCameraAccessSpinner();
        view.showFloatingTutorialButton();
        view.showDialogOne();
    }

    /**
     * SurfaceTextureView callbacks
     */
    @Subscribe
    public void onSurfaceTextureAvailable(CameraPrivacyView.OnSurfaceTextureAvailable event) {
        model.getResourcesManager().setSurfaceTextureReady();
        if (model.isPermissionGranted()) {
            openCamera();
        }
    }

    @Subscribe
    public void onCameraOutputSetEvent(CameraPrivacyModel.CameraOutputSetEvent event) {
        /*Mirror effect*/
        view.setMirrorEffect(model.isMirrored());
        view.setAspectRatio(event.getPreviewRatio().getWidth(), event.getPreviewRatio().getHeight());
    }

    @Subscribe
    public void onCameraPreviewSetEvent(CameraPrivacyModel.OnCameraPreviewSetEvent event) {
        /*UI changes*/
        view.hideCameraAccessSpinner();
    }

    @Subscribe
    public void onOpenCameraError(CameraPrivacyModel.OnCameraError event) {
        Log.d("CameraCaptureModel", "Error " + event.getMessage());
        fail(event.getMessage());
    }

    @Subscribe
    public void onCameraTargetAvailable(CameraPrivacyModel.OnCameraTargetAvailable event) {
        model.openCamera(view.getTextureView());
    }

    @Subscribe
    public void onFailButtonClicked(CameraPrivacyView.DialogOneEvent event) {
        fail();
    }

    @Subscribe
    public void onDialogTwoPassEvent(CameraPrivacyView.DialogTwoPassEvent event) {
        view.showDialogTurnOffPrivacy();
        model.privacyButtonWorking();
    }

    @Subscribe
    public void onPrivacyButtonPressed(CameraPrivacyModel.OnPrivacyButtonPressed event) {
        view.showDialogTwo();
    }

    @Subscribe
    public void onDialogTwoFailEvent(CameraPrivacyView.DialogTwoFailEvent event) {
        view.showDialogTurnOffPrivacy();
        model.privacyButtonFailed();
    }

    @Subscribe
    public void onDialogTurnOffPrivacyEvent(CameraPrivacyView.DialogTurnOffPrivacyEvent event) {
        if (model.isPrivacyWorking()) {
            pass();
        } else {
            fail();
        }
    }
    
    private void pass(String... message) {
        model.pass(message);
        view.finishActivity();
    }

    private void fail(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}
