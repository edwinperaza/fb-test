package com.facebook.devices.mvp.model;


import com.facebook.hwtp.mvp.model.ServiceModel;

public class BrightnessModel extends ServiceModel {

    private final int MAX_BRIGHTNESS = 255;
    private int brightness = 0; /*0-255*/

    public BrightnessModel() {
    }

    /**
     * @param brightness - must be between 0 and 255, otherwise it's ignored
     *                   according {@link android.provider.Settings.System#SCREEN_BRIGHTNESS}
     */
    public void setBrightness(int brightness) {
        if (brightness >= 0 && brightness <= MAX_BRIGHTNESS) {
            this.brightness = brightness;
        }
    }

    public int getBrightness() {
        return brightness;
    }

    public int getMaxBrightness() {
        return MAX_BRIGHTNESS;
    }
}
