package com.facebook.devices.mvp.view;

import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AugmentedRealityNoOpenglView extends TestViewBase {

    @BindView(R.id.tv_message)
    public TextView tvMessage;

    public AugmentedRealityNoOpenglView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);

        ButterKnife.bind(this, activity);
    }

    public void setMessage(String version) {
        String openglVersionText = getContext().getString(R.string.augmented_reality_opengl_version, version);
        String message = getContext().getString(R.string.augmented_reality_no_opengl_message, openglVersionText);
        int initSpan = message.indexOf(openglVersionText);
        int endSpan = initSpan + openglVersionText.length();
        SpannableString span = new SpannableString(message);
        span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.bright_aqua)), initSpan, endSpan, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        tvMessage.setText(span);
    }

    @OnClick(R.id.btn_finish_test)
    public void finishTest() {
        post(new FinishBtnEvent());
    }


    /**
     * Static classes for BUS
     */
    public static class FinishBtnEvent {
        /*Nothing to do here*/
    }
}
