package com.facebook.devices.mvp.model;


import android.app.Activity;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.IntDef;
import android.util.Log;

import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.models.WifiDataContainer;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.net.NetworkInfo.DetailedState.IDLE;
import static android.net.wifi.WifiManager.EXTRA_NETWORK_INFO;
import static android.net.wifi.WifiManager.EXTRA_WIFI_STATE;
import static android.net.wifi.WifiManager.NETWORK_STATE_CHANGED_ACTION;
import static android.net.wifi.WifiManager.WIFI_STATE_CHANGED_ACTION;
import static android.net.wifi.WifiManager.WIFI_STATE_ENABLED;

public class WifiModel extends ServiceModel {

    @IntDef({STATE_CONNECTION_IDLE, STATE_CONNECTING, STATE_CONNECTED, STATE_FAILED})
    @interface State {
    }

    public static final int SATE_WAITING_PERMISSION_RESOLUTION = -2;
    public static final int STATE_CONNECTION_IDLE = -1;
    public static final int STATE_CONNECTING = 0;
    public static final int STATE_CONNECTED = 1;
    public static final int STATE_FAILED = 2;

    @State
    private int modelState;

    private NetworkInfo.DetailedState wifiState = IDLE;

    private static final int BUFFER_SIZE = 4096;
    private static final int BUFFER_UPLOAD_SIZE = 1024 * 1024;
    private static final long MILLIS_UNTIL_FAIL = 10000; /*10 seconds*/
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private Runnable runnableUIInQueue;
    private Activity activity;
    private WifiManager wm = null;
    private boolean wifiBroadcastRegistered = false;
    private WifiModelCallback callback;
    private List<WifiDataContainer> wifiList;
    private boolean keepWifiConnected;
    private boolean measureTransferRate;
    private RuntimePermissionHandler permissionHandler;
    private NetworkStatsManager networkStatsManager;
    private String baseUrl;
    private int fileSize;
    private String ssidConnected = "";
    private byte[] buffer = new byte[BUFFER_SIZE];
    private byte[] postData = new byte[BUFFER_UPLOAD_SIZE];

    public interface WifiModelCallback {

        void onReceiverRegistered(boolean wifiEnabled);

        void onWifiSateEnabled(Intent intent);

        void onWifiConnecting(String ssid);

        void onWifiConnected(String ssid, int speed, int frequency);

        void onQueueFailed();
    }

    private static class ThreadWifiConnection extends Thread {

        private static interface ThreadWifiCallback {
            void onTaskCompleted(int wifiPositionProcessed);
        }

        private int wifiPosition;
        private String ssID, pass;
        private WeakReference<WifiManager> wifiManagerWRef;
        private WeakReference<ThreadWifiCallback> threadCallbackWRef;

        public ThreadWifiConnection(int wifiListPosition, String ssId, String password, WifiManager wifiManager, ThreadWifiCallback threadWifiCallback) {
            wifiPosition = wifiListPosition;
            ssID = ssId;
            pass = password;
            wifiManagerWRef = new WeakReference<>(wifiManager);
            threadCallbackWRef = new WeakReference<>(threadWifiCallback);
        }

        @Override
        public void run() {
            WifiConfiguration wifiConfiguration = new WifiConfiguration();
            wifiConfiguration.SSID = String.format("\"%s\"", ssID);
            wifiConfiguration.preSharedKey = String.format("\"%s\"", pass);

            if (wifiManagerWRef.get() != null) {
                /* Add network to the manager */
                wifiManagerWRef.get().addNetwork(wifiConfiguration);
                /* Testing */
                Iterator<WifiConfiguration> iterator = wifiManagerWRef.get().getConfiguredNetworks().iterator();
                while (iterator.hasNext()) {
                    WifiConfiguration wifiConfig = iterator.next();
                    if (wifiConfig.SSID.equals(wifiConfiguration.SSID)) {
                        wifiManagerWRef.get().enableNetwork(wifiConfig.networkId, true);
                    } else {
                        wifiManagerWRef.get().disableNetwork(wifiConfig.networkId);
                    }
                }
            }

            if (threadCallbackWRef.get() != null) {
                threadCallbackWRef.get().onTaskCompleted(wifiPosition);
            }
        }
    }

    private static class RegisterWifiReceiverThread extends Thread {

        private static interface ThreadWifiReceivedCallback {
            void onTaskCompleted();
        }

        private WeakReference<BroadcastReceiver> wifiBroadcastWRef;
        private WeakReference<Handler> uiHandlerWRef;
        private WeakReference<Activity> activityWRef;
        private WeakReference<ThreadWifiReceivedCallback> threadWifiReceivedCallbackWRef;

        public RegisterWifiReceiverThread(BroadcastReceiver wifiBroadcast, Handler uiHandler, Activity activity, ThreadWifiReceivedCallback threadWifiReceivedCallback) {
            wifiBroadcastWRef = new WeakReference<>(wifiBroadcast);
            uiHandlerWRef = new WeakReference<>(uiHandler);
            activityWRef = new WeakReference<>(activity);
            threadWifiReceivedCallbackWRef = new WeakReference<>(threadWifiReceivedCallback);
        }

        @Override
        public void run() {
            IntentFilter wifiIntentFilter = new IntentFilter();
            wifiIntentFilter.addAction(WIFI_STATE_CHANGED_ACTION);
            wifiIntentFilter.addAction(NETWORK_STATE_CHANGED_ACTION);

            if (wifiBroadcastWRef.get() != null && uiHandlerWRef.get() != null &&
                    activityWRef.get() != null && threadWifiReceivedCallbackWRef.get() != null) {
                activityWRef.get().registerReceiver(wifiBroadcastWRef.get(), wifiIntentFilter);
                /*Callback Receiver registered successfully*/
                threadWifiReceivedCallbackWRef.get().onTaskCompleted();
            }
        }
    }

    private WifiBroadcast wifiBroadcast = new WifiBroadcast((context, intent) -> {
        switch (intent.getAction()) {
            case WIFI_STATE_CHANGED_ACTION:
                if (intent.getIntExtra(EXTRA_WIFI_STATE, -1) == WIFI_STATE_ENABLED) {
                    onWifiStateEnabledCallback(intent);
                }
                break;


            case NETWORK_STATE_CHANGED_ACTION:

                NetworkInfo networkInfo = intent.getParcelableExtra(EXTRA_NETWORK_INFO);
                WifiInfo wifiInfo = wm.getConnectionInfo();
                if ((networkInfo.getDetailedState() != wifiState) &&
                        (networkInfo.getState() == NetworkInfo.State.CONNECTED && ssidIsListed(wifiInfo.getSSID().replace("\"", "")))) {
                    /** Obtain {@link WifiInfo} through {@link WifiManager.EXTRA_WIFI_INFO} if needed to get wifi information */
                    wifiState = networkInfo.getDetailedState();
                    modelState = WifiModel.STATE_CONNECTED;
                    /* Connection established callback */
                    ssidConnected = wifiInfo.getSSID();
                    onSupplicantConnectionEstablished(wifiInfo);
                }
                break;
        }
    });

    public WifiModel(Activity activity, boolean keepWifiConnected,
            boolean measureTransferRate, NetworkStatsManager networkStatsManager,
            String baseUrl, int fileSize,
            RuntimePermissionHandler permissionHandler, WifiDataContainer... wifiList) {
        this.activity = activity;
        this.keepWifiConnected = keepWifiConnected;
        this.measureTransferRate = measureTransferRate;
        this.networkStatsManager = networkStatsManager;
        this.baseUrl = baseUrl;
        this.fileSize = fileSize;
        this.permissionHandler = permissionHandler;
        this.wifiList = Arrays.asList(wifiList);
        this.modelState = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? SATE_WAITING_PERMISSION_RESOLUTION : STATE_CONNECTION_IDLE;
        wm = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    @Override
    public void pass(String... message) {
        if (!shouldKeepWifiConnected()) {
            disableWifi();
        }
        super.pass(message);
    }

    @Override
    public void fail(String... message) {
        interruptBackgroundTasks();
        if (!shouldKeepWifiConnected()) {
            disableWifi();
        }
        super.fail(message);
    }

    public boolean isWifiListEmpty() {
        return wifiList.size() == 0;
    }

    public boolean shouldKeepWifiConnected() {
        return keepWifiConnected;
    }

    public boolean shouldMeasureTransferRate() {
        return measureTransferRate;
    }

    @State
    public int getModelState() {
        return modelState;
    }

    public void setModelState(@State int modelState) {
        this.modelState = modelState;
    }

    public void setModelCallback(WifiModelCallback callback) {
        this.callback = callback;
    }

    private void onWifiStateEnabledCallback(Intent intent) {
        if (callback != null)
            callback.onWifiSateEnabled(intent);
    }

    private void onSupplicantConnectionEstablished(WifiInfo wifiInfo) {
        /* Remove waiting runnable */
        removeWaitingConnectionRunnable();
        /* Perform callback action */
        if (callback != null) {
            callback.onWifiConnected(wifiInfo.getSSID(), wifiInfo.getLinkSpeed(), wifiInfo.getFrequency());
        }
    }

    public boolean isWifiEnabled() {
        return wm.isWifiEnabled();
    }

    public void startConnectionQueueInRangeOrder() {
        /* Update wifi list based on the RSSI */

        List<ScanResult> wifiScanList = wm.getScanResults();
        for (WifiDataContainer wifiContainer : wifiList) {
            for (ScanResult result : wifiScanList) {
                if (wifiContainer.getSsID().equals(result.SSID)) {
                    wifiContainer.setLevel(result.level);
                }
            }
        }
        /*Order wifi list*/
        Collections.sort(wifiList, (wifi1, wifi2) -> {
            Integer wifi1Level = wifi1.getLevel();
            Integer wifi2Level = wifi2.getLevel();
            return wifi1Level.compareTo(wifi2Level);
        });
        Collections.reverse(wifiList);
        /*Connection queue*/
        startConnectionQueue();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }
    public void startConnectionQueue() {
        if (modelState == STATE_CONNECTION_IDLE) {
            modelState = STATE_CONNECTING;
            connectionQueue(0);
        } else {
            Log.e("WifiState", "Cannot start a connection queue - it's already started.");
        }
    }

    private void connectionQueue(int wifiPosition) {
        /* Check for a next wifi to connect if possible */
        if (wifiPosition < wifiList.size() && modelState != STATE_CONNECTED) {
            WifiDataContainer wifiData = wifiList.get(wifiPosition);
            callback.onWifiConnecting(wifiData.getSsID());
            /* Trigger connection method */
            connectToSSID(wifiPosition, wifiData.getSsID(), wifiData.getPassword());
        } else {
            modelState = STATE_FAILED;
            callback.onQueueFailed();
        }
    }

    /**
     * Automatically connects with the next wifi in the queue
     * NOTE: enableNetwork blocks the UI ( in some devices like nexus 5 ) / Because of that a thread is required
     */
    private void connectToSSID(int wifiPosition, String ssId, String password) {
        new ThreadWifiConnection(wifiPosition, ssId, password, wm,
                wifiPositionProcessed -> {
                    /* Save last runnable state */
                    runnableUIInQueue = () -> {
                        /* Clear latest runnable completed */
                        runnableUIInQueue = null;
                        connectionQueue(wifiPositionProcessed + 1);
                    };
                    uiHandler.postDelayed(runnableUIInQueue, MILLIS_UNTIL_FAIL);
                })
                .start();
    }

    public void clearAllData() {
        /*Remove waiting runnable*/
        removeWaitingConnectionRunnable();
    }

    private void removeWaitingConnectionRunnable() {
        if (runnableUIInQueue != null) {
            uiHandler.removeCallbacks(runnableUIInQueue);
            runnableUIInQueue = null;
        }
    }

    public void disconnect() {
        if (wm != null) {
            wm.disconnect();
        }
    }

    public void disableWifi() {
        if (wm != null) {
            wm.setWifiEnabled(false);
        }
    }

    public void enableWifi() {
        if (wm != null) {
            wm.setWifiEnabled(true);
        }
    }

    public void registerReceiver() {
        // NOTE: registerReceiver blocks the UI
        if (wifiBroadcastRegistered) {
            /*Callback for Receiver already registered*/
            callback.onReceiverRegistered(wm.isWifiEnabled());
        } else {
            new RegisterWifiReceiverThread(wifiBroadcast, uiHandler, activity, () -> {
                wifiBroadcastRegistered = true;
                uiHandler.post(() -> callback.onReceiverRegistered(wm.isWifiEnabled()));
            }).start();
        }
    }

    public boolean isBroadcastRegistered() {
        return wifiBroadcastRegistered;
    }

    /**
     * Check if the specific ssId is contained on our
     * list of wifi desire connections
     */
    private boolean ssidIsListed(String specificSSID) {
        for (WifiDataContainer data : wifiList) {
            if (data.getSsID().equals(specificSSID))
                return true;
        }
        return false;
    }

    public void unRegisterReceiver() {
        if (wifiBroadcastRegistered) {
            wifiBroadcastRegistered = false;
            activity.unregisterReceiver(wifiBroadcast);
        }
    }

    public boolean checkCoarseLocationPermissionsAvailable() {
        if (!permissionHandler.isPermissionGranted()) {
            permissionHandler.requestPermission();
            return false;
        } else {
            modelState = STATE_CONNECTION_IDLE;
            return true;
        }
    }

    private static class WifiBroadcast extends BroadcastReceiver {

        interface WifiBroadcastListener {
            void onReceive(Context context, Intent intent);
        }

        private WifiBroadcastListener listener;


        public WifiBroadcast(WifiBroadcastListener listener) {
            this.listener = listener;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            listener.onReceive(context, intent);
        }
    }

    // ---------------------- Download and Upload Transfer rate --------------------------------
    private UploadThread uploadThread;
    private DownloadThread downloadThread;

    public void interruptBackgroundTasks() {
        if (uploadThread != null && uploadThread.isRunning()) {
            uploadThread.terminate();
        }
        if (downloadThread != null && downloadThread.isRunning()) {
            downloadThread.terminate();
        }
    }

    public void uploadFile(WifiTransferProgress wifiTransferCallback) {
        uploadThread = new UploadThread(wifiTransferCallback);
        uploadThread.start();
    }

    public void downloadFile(WifiTransferProgress wifiTransferCallback) {
        downloadThread = new DownloadThread(wifiTransferCallback);
        downloadThread.start();
    }

    public class UploadThread extends Thread {
        private WeakReference<WifiTransferProgress> wifiTransferCallback;
        private boolean isRunning;

        public UploadThread(WifiTransferProgress wifiTransferCallback) {
            this.wifiTransferCallback = new WeakReference<>(wifiTransferCallback);
        }

        @Override
        public void run() {
            isRunning = true;
            double initialBytesRead;
            double totalMbUploadStat = 0;
            double progress;
            NetworkStats.Bucket bucket;
            HttpURLConnection httpConn = null;
            try {
                URL url = new URL(baseUrl);
                httpConn = (HttpURLConnection) url.openConnection();

                httpConn.setRequestMethod("POST");
                httpConn.setDoOutput(true);
                httpConn.setChunkedStreamingMode(0);

                OutputStream outputStream = httpConn.getOutputStream();
                long startTime = System.currentTimeMillis();

                bucket = networkStatsManager.querySummaryForUser(ConnectivityManager.TYPE_WIFI,
                        "", 0, System.currentTimeMillis());
                initialBytesRead = bucket.getTxBytes();

                while (totalMbUploadStat < fileSize && isRunning) {
                    outputStream.write(postData);
                    outputStream.flush();
                    long currentTime = System.currentTimeMillis();
                    long seconds = TimeUnit.MILLISECONDS.toSeconds((currentTime - startTime));

                    bucket = networkStatsManager.querySummaryForUser(ConnectivityManager.TYPE_WIFI, "",
                            0, currentTime);
                    totalMbUploadStat = (bucket.getTxBytes() - initialBytesRead) / (Math.pow(1024, 2));

                    if (seconds > 0) {
                        progress = (totalMbUploadStat * 100) / fileSize;
                        wifiTransferCallback.get().onUpStream((totalMbUploadStat / seconds), progress);

                    }
                }
                outputStream.close();
                if (isRunning) {
                    int responseCode = httpConn.getResponseCode();
                    if (responseCode != HttpURLConnection.HTTP_OK) {
                        wifiTransferCallback.get().onErrorStream("Upload error. Server response HTTP code: " + responseCode);
                    } else {
                        wifiTransferCallback.get().onUploadSuccess();
                    }
                }
            } catch (RemoteException | IOException e) {
                wifiTransferCallback.get().onErrorStream(e.getMessage());
            } finally {
                if (httpConn != null) {
                    httpConn.disconnect();
                }
            }
        }

        public void terminate() {
            isRunning = false;
        }

        public boolean isRunning() {
            return isRunning;
        }
    }

    public class DownloadThread extends Thread {
        private WeakReference<WifiTransferProgress> wifiTransferCallback;
        private boolean isRunning;

        public DownloadThread(WifiTransferProgress wifiTransferCallback) {
            this.wifiTransferCallback = new WeakReference<>(wifiTransferCallback);
        }

        @Override
        public void run() {
            isRunning = true;
            double initialBytesRead;
            double totalMbReadStat;
            double progress;
            NetworkStats.Bucket bucket;
            HttpURLConnection httpConn = null;
            try {
                URL url = new URL(baseUrl + fileSize);
                httpConn = (HttpURLConnection) url.openConnection();
                int responseCode = httpConn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = httpConn.getInputStream();
                    long startTime = System.currentTimeMillis();
                    bucket = networkStatsManager.querySummaryForUser(ConnectivityManager.TYPE_WIFI,
                            "", 0, System.currentTimeMillis());
                    initialBytesRead = bucket.getRxBytes();

                    while (inputStream.read(buffer) != -1 && isRunning) {
                        long currentTime = System.currentTimeMillis();
                        long seconds = TimeUnit.MILLISECONDS.toSeconds((currentTime - startTime));
                        bucket = networkStatsManager.querySummaryForUser(ConnectivityManager.TYPE_WIFI,
                                "", 0, currentTime);
                        totalMbReadStat = (bucket.getRxBytes() - initialBytesRead) / (Math.pow(1024, 2));
                        if (seconds > 0) {
                            progress = (totalMbReadStat * 100) / fileSize;
                            wifiTransferCallback.get().onDownStream((totalMbReadStat / seconds), progress);
                        }
                    }
                    inputStream.close();
                    if (isRunning) {
                        wifiTransferCallback.get().onDownLoadSuccess();
                    }
                } else {
                    wifiTransferCallback.get().onErrorStream("No file to download. Server response HTTP code: " + responseCode);
                }
            } catch (RemoteException | IOException e) {
                wifiTransferCallback.get().onErrorStream(e.getMessage());
            } finally {
                if (httpConn != null) {
                    httpConn.disconnect();
                }
            }
        }

        public void terminate() {
            isRunning = false;
        }

        public boolean isRunning() {
            return isRunning;
        }
    }

    public String getSsidConnected() {
        return ssidConnected;
    }

    public interface WifiTransferProgress {

        void onUpStream(double rate, double progress);

        void onDownStream(double rate, double progress);

        void onErrorStream(String message);

        void onUploadSuccess();

        void onDownLoadSuccess();
    }
}