package com.facebook.devices.mvp.view;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.facebook.devices.R;
import com.facebook.devices.activities.PreferencesActivity;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.listeners.ArgumentRecyclerViewListener;
import com.facebook.devices.utils.recyclers.ArgumentsRecyclerAdapter;
import com.facebook.hwtp.mvp.view.ViewBase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreferencesView extends ViewBase<PreferencesActivity> {

    private final BusProvider.Bus bus;
    @BindView(R.id.recycler_arguments) RecyclerView recyclerArguments;
    private ArgumentsRecyclerAdapter adapter;

    public PreferencesView(PreferencesActivity activity, BusProvider.Bus bus) {
        super(activity);
        ButterKnife.bind(this, activity);
        this.bus = bus;
        setupRecycler();
    }

    private void setupRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerArguments.setLayoutManager(layoutManager);
        recyclerArguments.setHasFixedSize(true);

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerArguments.addItemDecoration(itemDecoration);

        /* Adapter creation */
        adapter = new ArgumentsRecyclerAdapter(getContext(), new ArrayList<>());
        adapter.setListener(new ArgumentRecyclerViewListener<ArgumentEntity>() {
            @Override
            public void recyclerViewOnItemClickListener(View view, int position, ArgumentEntity argumentEntity) {
                post(new ItemClickListenerEvent(argumentEntity));
            }

            @Override
            public void recyclerViewOnItemTextChange(int position, ArgumentEntity argumentEntity) {
                post(new ItemClickListenerEvent(argumentEntity));
            }

            @Override
            public void recyclerViewOnItemLongClickListener(View view, int position, ArgumentEntity argumentEntity) {
                /*Nothing to do here*/
            }
        });
        recyclerArguments.setAdapter(adapter);
    }

    public void updateRecyclerItems(List<ArgumentEntity> groups, Map<String, TestInfoEntity> testDefinitions, List<ColorEntity> colorEntityList) {
        if (adapter != null) {
            adapter.updateItems(groups, testDefinitions, colorEntityList);
        }
    }

    protected void post(Object event) {
        bus.post(event);
    }

    /**
     * Static classes for BUS
     */
    public static class ItemClickListenerEvent {
        public ArgumentEntity argumentEntity;

        public ItemClickListenerEvent(ArgumentEntity argumentEntity) {
            this.argumentEntity = argumentEntity;
        }
    }
}
