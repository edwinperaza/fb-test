package com.facebook.devices.mvp.model;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.Uri;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class VideoModel extends ServiceModel {

    public static final String FIRST_QUESTION_IS_BEING_SHOWN = "firstQuestionIsBeingShown";
    public static final String SECOND_QUESTION_IS_BEING_SHOWN = "secondQuestionIsBeingShown";

    public String videoPath;
    private Context context;
    private PermissionHandler permissionHandler;
    private VolumeManager volumeManager;

    /*Instance data*/
    private boolean renderReady;
    private boolean firstQuestionIsBeingShown;
    private boolean secondQuestionBeingShown;

    public VideoModel(Context context, PermissionHandler permissionHandler,
                      String videoName, Boolean firstQuestionIsBeingShown,
                      Boolean secondQuestionBeingShown, VolumeManager volumeManager) {
        this.context = context;
        this.permissionHandler = permissionHandler;
        this.volumeManager = volumeManager;
        this.videoPath = videoName;
        this.firstQuestionIsBeingShown = firstQuestionIsBeingShown;
        this.secondQuestionBeingShown = secondQuestionBeingShown;
    }

    public void setRenderReady() {
        renderReady = true;
    }

    public boolean isRenderReady() {
        return renderReady;
    }

    /**
     * @return a valid URI only if it's a raw resource
     */
    public Uri getVideoPathUri() {
        return getUriLocalResource(videoPath);
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setDefaultMediaVolume() {
        volumeManager.setDefaultMediaVolume();
    }

    private Uri getUriLocalResource(String resourceName) {
        int resource = context.getResources().getIdentifier(resourceName, "raw", context.getPackageName());

        if (context != null && resource != 0) {
            return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + resource);
        }
        return null;
    }

    public Point calculateBestSize(int originalWidth, int originalHeight, Point screenSize) {
        boolean needScale = (originalWidth != screenSize.x || originalHeight != screenSize.y);
        int scaledWidth = originalWidth;
        int scaledHeight = originalHeight;

        if (needScale) {
            int originalOrientation = (originalWidth > originalHeight) ? Configuration.ORIENTATION_LANDSCAPE : Configuration.ORIENTATION_PORTRAIT;

            if (originalOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                /*Landscape*/
                /*Set new width*/
                scaledWidth = screenSize.x;
                /*Get difference percentage*/
                float diffPerc = (float) (originalWidth - screenSize.x) / originalWidth;
                /*Set relation*/
                scaledHeight = (int) ((originalHeight) - (originalHeight * diffPerc));
            } else {
                /* Portrait */
                /*Set new width*/
                scaledHeight = screenSize.y;
                /*Get difference percentage*/
                float diffPerc = (float) (originalHeight - screenSize.y) / originalHeight;
                /*Set relation*/
                scaledWidth = (int) ((originalWidth) - (originalWidth * diffPerc));
            }
        }

        //NOTE: Do not use new Point(scaledWidth, scaledHeight) for unknown reason junit do not
        //assign these parameters by constructor.
        Point p = new Point();
        p.x = scaledWidth;
        p.y = scaledHeight;
        return p;
    }

    public void firstQuestionIsBeingShown() {
        firstQuestionIsBeingShown = true;
    }

    public void firstQuestionIsNotBeingShown() {
        firstQuestionIsBeingShown = false;
    }

    public boolean isFirstQuestionBeingShown() {
        return firstQuestionIsBeingShown;
    }

    public void secondQuestionIsBeingShown() {
        secondQuestionBeingShown = true;
    }

    public void secondQuestionIsNotBeingShown() {
        secondQuestionBeingShown = false;
    }

    public boolean isSecondQuestionBeingShown() {
        return secondQuestionBeingShown;
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

}
