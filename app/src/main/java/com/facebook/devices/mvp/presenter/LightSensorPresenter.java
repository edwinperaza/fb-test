package com.facebook.devices.mvp.presenter;

import android.content.ComponentName;
import android.os.IBinder;

import com.facebook.devices.mvp.model.LightSensorModel;
import com.facebook.devices.mvp.view.LightSensorView;

import org.greenrobot.eventbus.Subscribe;

public class LightSensorPresenter extends PresenterTestBase<LightSensorModel, LightSensorView> {

    public LightSensorPresenter(LightSensorModel model, LightSensorView view) {
        super(model, view);
        view.showInitDialog();
        view.showTutorialButton();
    }

    public void onPause() {
        model.stopSensing();
    }

    @Subscribe
    public void onLightSensorUncover(LightSensorModel.LightSensorUncoverEvent event) {
        if (model.wasMeasurementSuccess()) {
            model.pass();
            view.finishActivity();
        }
    }

    @Subscribe
    public void onLightSensorCover(LightSensorModel.LightSensorCoverEvent event) {
        if (model.wasMeasurementSuccess()) {
            model.pass();
            view.finishActivity();
        }
    }

    @Subscribe
    public void onLightSensorEvent(LightSensorModel.LightSensorEvent event) {
        view.showLuxValue(event.value);
    }

    @Subscribe
    public void onButtonFailurePressed(LightSensorView.OnButtonFailurePressed event) {
        model.fail();
        view.finishActivity();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        model.startSensing();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        model.stopSensing();
        super.onServiceDisconnected(name);
    }
}