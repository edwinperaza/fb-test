package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.FinalScreenModel;
import com.facebook.devices.mvp.view.FinalScreenView;
import com.facebook.hwtp.mvp.presenter.PresenterBase;

import org.greenrobot.eventbus.Subscribe;

public class FinalScreenPresenter extends PresenterBase<FinalScreenModel, FinalScreenView> {

    public FinalScreenPresenter(FinalScreenModel model, FinalScreenView view) {
        super(model, view);

        /*Rendering*/
        renderingUI();
    }

    public void renderingUI() {
        switch (model.getMode()) {
            case FinalScreenModel.MODE_FAILURE:
                view.failureMode();
                break;

            case FinalScreenModel.MODE_SUCCESS:
                view.successMode();
                break;
        }
    }

    /* Bus Events */
    @Subscribe
    public void onAnimationEndEvent(FinalScreenView.OnAnimationEndEvent event) {
        /*Starting timer*/
        view.getUIHandler().postDelayed(() -> {
            model.pass();
            view.finishActivity();
        }, model.getMillisToClose());
    }
}
