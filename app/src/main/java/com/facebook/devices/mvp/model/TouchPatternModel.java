package com.facebook.devices.mvp.model;

import android.support.annotation.IntDef;

import com.facebook.devices.utils.customviews.TouchDetectorView;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.facebook.devices.utils.customviews.TouchDetectorView.ORIENTATION_HORIZONTAL;
import static com.facebook.devices.utils.customviews.TouchDetectorView.ORIENTATION_VERTICAL;

public class TouchPatternModel extends ServiceModel {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TWO_TWO, THREE_THREE, FOUR_FOUR, FIVE_FIVE, SIX_SIX, SEVEN_SEVEN, EIGHT_EIGHT, NINE_NINE, TEN_TEN})
    public @interface TouchDetectorSize {
    }

    /*Size modes*/
    public static final int TWO_TWO = 2;
    public static final int THREE_THREE = 3;
    public static final int FOUR_FOUR = 4;
    public static final int FIVE_FIVE = 5;
    public static final int SIX_SIX = 6;
    public static final int SEVEN_SEVEN = 7;
    public static final int EIGHT_EIGHT = 8;
    public static final int NINE_NINE = 9;
    public static final int TEN_TEN = 10;

    @TouchDetectorView.TouchDetectorOrientation
    private int touchOrientation;
    @TouchDetectorSize
    private int touchSize;

    public TouchPatternModel(@TouchDetectorView.TouchDetectorOrientation int touchOrientation, @TouchDetectorSize int touchSize) {
        this.touchOrientation = touchOrientation;
        this.touchSize = touchSize;
    }

    @TouchDetectorSize
    public int getTouchSize() {
        return touchSize;
    }

    @TouchDetectorView.TouchDetectorOrientation
    public int getTouchOrientation() {
        return touchOrientation;
    }

    public int[] generateRespectivePath() {

        int[] correctPath;
        int squareNumberBase = getTouchSize();
        int position = 0;

        switch (getTouchOrientation()) {
            case ORIENTATION_HORIZONTAL:

                correctPath = new int[(int) Math.pow(squareNumberBase, 2)];
                for (int i = 0; i < squareNumberBase; i++) {

                    if (i % 2 == 0) {

                        int startPoint = (position == 0) ? 0 : correctPath[position - squareNumberBase] + 1;
                        int endPoint = startPoint + squareNumberBase;
                        for (int j = startPoint; j < endPoint; j++) {
                            correctPath[position] = j;
                            position++;
                        }

                    } else {

                        int startPoint = correctPath[position - 1] + squareNumberBase;
                        int endPoint = correctPath[position - 1];
                        for (int j = startPoint; j > endPoint; j--) {
                            correctPath[position] = j;
                            position++;
                        }
                    }
                }

                return correctPath;

            case ORIENTATION_VERTICAL:

                int total = (int) Math.pow(squareNumberBase, 2);
                correctPath = new int[total];

                for (int i = 0; i < squareNumberBase; i++) {
                    if (i % 2 == 0) {
                        int startPoint = (position == 0) ? 0 : correctPath[position - 1] + 1;
                        int endPoint = startPoint + squareNumberBase * (squareNumberBase - 1); //correctPath[position - squareNumberBase] + 1;
                        for (int j = startPoint; j <= endPoint; j += squareNumberBase) {
                            correctPath[position] = j;
                            position++;
                        }
                    } else {
                        int startPoint = correctPath[position - 1] + 1;
                        int endPoint = startPoint % squareNumberBase;
                        for (int j = startPoint; j >= endPoint; j -= squareNumberBase) {
                            correctPath[position] = j;
                            position++;
                        }
                    }
                }

                return correctPath;
        }

        return null;
    }
}
