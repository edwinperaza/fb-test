package com.facebook.devices.mvp.presenter;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.facebook.devices.mvp.model.BluetoothSpeakerRipleyModel;
import com.facebook.devices.mvp.view.BluetoothSpeakerRipleyView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

import static android.app.Activity.RESULT_OK;

public class BluetoothSpeakerRipleyPresenter extends PresenterTestBase<BluetoothSpeakerRipleyModel, BluetoothSpeakerRipleyView> {

    public BluetoothSpeakerRipleyPresenter(BluetoothSpeakerRipleyModel model, BluetoothSpeakerRipleyView view) {
        super(model, view);
    }

    public void onActivityResult(int requestCode, int resultCode) {
        Log.d("BT_FACE", "onActivityResult requestCode: " + requestCode + ", resultCode: " + resultCode);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case BluetoothSpeakerRipleyModel.REQUEST_ENABLE_BT:
                    Log.d("BT_FACE", "onActivityResult REQUEST_ENABLE_BT Accepted");
                    model.permissionBluetoothAccepted();
                    view.showPairingBluetoothLayout(model.getBluetoothName());
                    if (!model.wasDiscoverableBluetoothShown() && model.showDiscoverableView()) {
                        Log.d("BT_FACE", "onActivityResult requestDiscoverableBluetooth");
                        model.discoverableBluetoothDialogShown();
                        view.requestDiscoverableBluetooth();
                    }
                    break;
            }
        } else if (resultCode == BluetoothSpeakerRipleyModel.REQUEST_DISCOVERABLE_BT_SECONDS) {
            Log.d("BT_FACE", "onActivityResult bluetoothDiscoverableAccepted");
        } else {
            Log.d("BT_FACE", "onActivityResult testFailed");
            testFailed();
        }
    }

    public void onDestroy() {
        model.onDestroy();
    }

    public void onSaveInstantState(Bundle savedInstantState) {
        savedInstantState.putBoolean(BluetoothSpeakerRipleyModel.PERMISSION_BLUETOOTH_ACCEPTED, model.wasPermissionBluetoothAccepted());
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        preCheck();
    }

    public void preCheck() {
        Log.d("BT_FACE", "preCheck()");
        if (!model.isAvailableBluetoothAdapter()) {
            Log.d("BT_FACE", "preCheck showNoAvailableBluetooth");
            view.showNoAvailableBluetooth();
            testFailed();
            return;
        }

        if (!model.isPermissionGranted()) {
            Log.d("BT_FACE", "preCheck NOT Permission Granted, requestPermissions");
            model.requestPermissions();
        } else {
            Log.d("BT_FACE", "preCheck");
            view.showTutorialButton();

            if (!model.wasPermissionBluetoothAccepted()) {
                Log.d("BT_FACE", "preCheck registerReceiver");
                model.registerReceiver();
            }

            model.permissionBluetoothAccepted();
            Log.d("BT_FACE", "preCheck showInitDialog");
            view.showInitDialog();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
            return;
        }
        preCheck();
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        if (model.isPermissionGranted()) {
            view.showTutorialButton();

            if (!model.wasPermissionBluetoothAccepted()) {
                Log.d("BT_FACE", "preCheck registerReceiver");
                model.registerReceiver();
            }

            model.permissionBluetoothAccepted();
            Log.d("BT_FACE", "preCheck showInitDialog");
            view.showInitDialog();
        }
    }

    @Subscribe
    public void onInitDialogPressedEvent(BluetoothSpeakerRipleyView.OnInitDialogPressedEvent event) {
        Log.d("BT_FACE", "onInitDialogPressed after isEnabledBluetoothAdapter and model.wasDiscoverableBluetoothShown()");
        if (!model.isEnabledBluetoothAdapter()) {
            Log.d("BT_FACE", "onInitDialogPressed requestEnableBluetooth");
            view.requestEnableBluetooth();
            return;
        } else if (!model.wasDiscoverableBluetoothShown() && model.showDiscoverableView()) {
            Log.d("BT_FACE", "onInitDialogPressed requestDiscoverableBluetooth");
            model.discoverableBluetoothDialogShown();
            view.requestDiscoverableBluetooth();
        }

        view.showPairingBluetoothLayout(model.getBluetoothName());
    }

    @Subscribe
    public void onFailedBtnClicked(BluetoothSpeakerRipleyView.FailedBtnPressedEvent event) {
        Log.d("BT_FACE", "onFailedBtnPressed");
        testFailed();
    }

    @Subscribe
    public void onFailedBluetoothBonded(BluetoothSpeakerRipleyModel.FailedBluetoothBonded event) {
        Log.d("BT_FACE", "onFailedBluetoothBonded");
        view.showFailedBluetoothPairedMessage();
    }

    @Subscribe
    public void onSuccessBluetoothConnected(BluetoothSpeakerRipleyModel.ConnectedBluetoothSuccess event) {
        Log.d("BT_FACE", "onSuccessBluetoothConnected");
        view.showConnectedMessage(event.getDeviceName(), event.getDeviceAddress());
    }

    @Subscribe
    public void onDisconnectedBluetoothSuccess(BluetoothSpeakerRipleyModel.DisconnectedBluetoothSuccess event) {
        Log.d("BT_FACE", "onDisconnectedBluetoothSuccess");
        view.showDisconnectedMessage();
    }

    @Subscribe
    public void onResultPatterMessageInteracted(BluetoothSpeakerRipleyView.ResultOptionPressedEvent event) {
        switch (event.getOptionSelected()) {
            case BluetoothSpeakerRipleyView.ResultOptionPressedEvent.NO:
                Log.d("BT_FACE", "onResultPatterMessageInteracted NO");
                testFailed();
                break;

            case BluetoothSpeakerRipleyView.ResultOptionPressedEvent.YES:
                Log.d("BT_FACE", "onResultPatterMessageInteracted YES");
                model.pass();
                view.finishActivity();
                break;
        }
    }

    private void testFailed() {
        model.fail();
        view.finishActivity();
    }
}