package com.facebook.devices.mvp.presenter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import com.facebook.devices.mvp.model.CameraCaptureModel;
import com.facebook.devices.mvp.view.CameraCaptureView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.customcomponents.CancellableHandler;

import org.greenrobot.eventbus.Subscribe;

public class CameraCapturePresenter extends PresenterTestBase<CameraCaptureModel, CameraCaptureView> {

    public CameraCapturePresenter(CameraCaptureModel model, CameraCaptureView view) {
        super(model, view);
    }

    public void onStart() {
        if (model.isPermissionGranted()) {
            view.hideCameraAccessSpinner();
        }
    }

    public void onStop(boolean isChangingConfigurations) {
        model.getUiHandler().removeCallbacks();
        model.stopCamera(!isChangingConfigurations);
    }

    public void onDestroy() {
        /*Set destroyed state*/
        model.setFlowActive(false);
        /*Clear resources*/
        model.getResourcesManager().setSurfaceTextureUnavailable();
    }

    /* Called when view attached is resumed */
    public void onViewResumed() {

        /* Photo already taken */
        if (model.wasImageTaken()) {
            model.getImageProcessorAsyncTask(bitmap -> view.setStatePhotoTaken(bitmap)).execute(model.getPhotoAbsoluteFilePath());
            return;
        }

        /* Check for permissions */
        if (!model.isPermissionGranted()) {
            model.requestPermissions();
            return;
        }

        /*UI components*/
        view.showInitDialog();
        view.showFloatingButtonTutorial();

        /* Run camera preview */
        openCamera();
    }

    /* Called when view attached is paused */
    public void onViewPaused() {
        if (model.isPermissionGranted()) {
            model.pauseCamera();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (model.isStopCameraBroadcastSent()) {
            outState.putString(CameraCaptureModel.CAMERA_ID_RECEIVED, model.getResourcesManager().getCameraId());
        }
        /*Image taken scenario*/
        if (model.wasImageTaken()) {
            outState.putString(CameraCaptureModel.IMAGE_FILE_PATH, model.getPhotoAbsoluteFilePath());
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CameraCaptureModel.CAMERA_ID_RECEIVED) &&
                !savedInstanceState.containsKey(CameraCaptureModel.IMAGE_FILE_PATH)) {
            model.getResourcesManager().setCameraResourceReady(savedInstanceState.getString(CameraCaptureModel.CAMERA_ID_RECEIVED));
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        view.hideCameraAccessSpinner();
    }


    @Subscribe
    public void onSurfaceTextureAvailable(CameraCaptureView.OnSurfaceTextureAvailable event) {
        model.getResourcesManager().setSurfaceTextureReady();
        if (model.isPermissionGranted() && !model.wasImageTaken()) {
            openCamera();
        }
    }

    /*Open Camera*/
    @SuppressLint("MissingPermission")
    private void openCamera() {
        view.showCameraAccessSpinner();
        model.openCamera(view.getTextureView());
    }

    @Subscribe
    public void onCameraTriggerClicked(CameraCaptureView.CameraTriggerEvent event) {
        model.captureImage();
    }

    @Subscribe
    public void onSuccessButtonClicked(CameraCaptureView.SuccessEvent event) {
        pass();
    }

    @Subscribe
    public void onFailureButtonClicked(CameraCaptureView.FailureEvent event) {
        fail();
    }

    @Subscribe
    public void onInitDialogEventClicked(CameraCaptureView.InitDialogEvent event) {
        fail();
    }

    @Subscribe
    public void onCameraOutputSetEvent(CameraCaptureModel.CameraOutputSetEvent event) {
        /*Mirror effect*/
        view.setMirrorEffect(model.isMirrored());
        view.setAspectRatio(event.getPreviewRatio().getWidth(), event.getPreviewRatio().getHeight());
    }

    @Subscribe
    public void onCameraPreviewSetEvent(CameraCaptureModel.OnCameraPreviewSetEvent event) {
        /*UI changes*/
        view.hideCameraAccessSpinner();
        view.setStatePreview();
    }

    @Subscribe
    public void onOpenCameraError(CameraCaptureModel.OnCameraError event) {
        Log.d(CameraCaptureModel.class.getName(), "Error " + event.getMessage());
        fail(event.getMessage());
    }

    @Subscribe
    public void onCameraTargetAvailable(CameraCaptureModel.OnCameraTargetAvailable event) {
        if (!model.wasImageTaken()) {
            model.openCamera(view.getTextureView());
        }
    }

    @Subscribe
    public void onImageAvailableEvent(CameraCaptureModel.OnImageAvailableEvent event) {
        if (event.getImageCaptured() == null) {
            fail();
            return;
        }

        view.hideDialog();
        view.hideFloatingButton();
        view.setStatePhotoTaken(event.getImageCaptured());
        model.getUiHandler().postDelayedCustom(view::showResultDialog, model.getWaitTimeMillis());
    }

    private void pass(String... message) {
        model.pass(message);
        view.finishActivity();
    }

    private void fail(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}
