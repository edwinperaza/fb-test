package com.facebook.devices.mvp.model;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;

import com.facebook.hwtp.mvp.model.ServiceModel;

public class PowerButtonModel extends ServiceModel {

    private Activity activity;
    private boolean alreadyStarted = false;
    private boolean receiverRegistered = false;
    private ScreenBroadcast screenStateBroadcast = new ScreenBroadcast();

    public PowerButtonModel(Activity activity) {
        this.activity = activity;
    }

    public void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_USER_PRESENT);
        activity.registerReceiver(screenStateBroadcast, intentFilter);
        receiverRegistered = true;
    }

    public void unregisterReceiver() {
        activity.unregisterReceiver(screenStateBroadcast);
    }

    public boolean isReceiverRegistered() {
        return receiverRegistered;
    }

    public void setAlreadyStarted() {
        alreadyStarted = true;
    }

    public boolean wasStartedAlready() {
        return alreadyStarted;
    }

    public class ScreenBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                PowerManager newPM = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                PowerManager.WakeLock wakeLock = newPM.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "Wake");
                wakeLock.acquire();

                AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                PendingIntent pi = PendingIntent.getActivity(context, 0, new Intent(), 0);
                alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1, pi);
            }
        }
    }
}
