package com.facebook.devices.mvp.presenter;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.ColorModel;
import com.facebook.devices.mvp.view.ColorView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.mvp.model.ColorModel.QUESTION_ONE;
import static com.facebook.devices.mvp.model.ColorModel.QUESTION_TWO;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEGATIVE;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEUTRAL;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_POSITIVE;

public class ColorPresenter extends PresenterTestBase<ColorModel, ColorView> {

    private Runnable finalDialogRunnable = () -> {
        switch (model.getState()) {
            case QUESTION_ONE:
                view.showTutorialFloatingButton();
                view.showFirstQuestion();
                break;
            case QUESTION_TWO:
                view.showTutorialFloatingButton();
                view.showSecondQuestion();
                break;
        }
    };

    public ColorPresenter(ColorModel model, ColorView view) {
        super(model, view);
        view.setColor(model.getColorCode());
    }

    public void onResume() {
        if (model.shouldShowTutorial()) {
            view.showTutorialOnBannerTop();
        } else {
            startTimer();
        }
    }

    public void onPause() {
        view.getUiHandler().removeCallbacks(finalDialogRunnable);
    }

    private void startTimer() {
        view.getUiHandler().postDelayed(finalDialogRunnable, BuildConfig.WAITING_TIME_COLOR_MILLIS);
    }

    @Subscribe
    public void onQuestionOneEvent(ColorView.QuestionOneEvent event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                if (model.shouldShowMuraQuestion()) {
                    model.nextState();
                    view.showSecondQuestion();
                } else {
                    model.pass();
                    view.finishActivity();
                }
                break;
            case OPTION_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;
            case OPTION_NEUTRAL:
                startTimer();
                view.hideBannerTop();
                view.hideTutorialFloatingButton();
                break;
        }
    }

    @Subscribe
    public void onQuestionTwoEvent(ColorView.QuestionTwoEvent event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                model.pass();
                view.finishActivity();
                break;
            case OPTION_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;
            case OPTION_NEUTRAL:
                startTimer();
                view.hideBannerTop();
                view.hideTutorialFloatingButton();
                break;
        }
    }

    @Subscribe
    public void onTutorialTopBannerEvent(ColorView.OnTutorialTopBannerEvent event) {
        view.hideBannerTop();
        startTimer();
    }
}