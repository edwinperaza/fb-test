package com.facebook.devices.mvp.presenter;


import android.view.KeyEvent;

import com.facebook.devices.mvp.model.VolumeButtonsModel;
import com.facebook.devices.mvp.view.VolumeButtonsView;

import org.greenrobot.eventbus.Subscribe;


public class VolumeButtonsPresenter extends PresenterTestBase<VolumeButtonsModel, VolumeButtonsView> {

    public VolumeButtonsPresenter(VolumeButtonsModel model, VolumeButtonsView view) {
        super(model, view);

        /*Init*/
        view.showFloatingTutorial();
        view.showQuestionUp();
    }

    public boolean onKeyDown(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (model.getCurrentMode() == VolumeButtonsModel.MODE_UP) {
                    model.increaseTouchedTimes();
                    view.updateCountValuePositive(model.getTouchedTimes());
                    if (model.isStepUpCompleted()) {
                        view.completeUpKeyDelay();
                    }
                }
                return true;

            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (model.getCurrentMode() == VolumeButtonsModel.MODE_DOWN) {
                    model.increaseTouchedTimes();
                    view.updateCountValueNegative(model.getTouchedTimes());
                    if (model.isStepDownCompleted()) {
                        passTest();
                        return true;
                    }
                }
                return true;
        }
        return false;
    }

    private void passTest() {
        model.pass();
        view.finishActivity();
    }

    private void failTest() {
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void onHandlerUiUpCompleteEvent(VolumeButtonsView.OnHandlerUiUpCompleteEvent event) {
        view.showQuestionDown();
        view.updateCountValuePositive(model.getTouchedTimes());
    }

    @Subscribe
    public void onButtonFailureClicked(VolumeButtonsView.OnButtonFailureClicked event) {
        failTest();
    }
}
