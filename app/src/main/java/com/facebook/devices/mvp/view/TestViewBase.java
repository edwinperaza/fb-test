package com.facebook.devices.mvp.view;

import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.mvp.view.ViewBase;

public class TestViewBase extends ViewBase<TestBaseActivity> {

    private BusProvider.Bus bus;
    private BannerView bannerView;
    private FloatingTutorialView floatingTutorialView;

    public TestViewBase(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity);
        this.bus = bus;
        this.bannerView = new BannerView(activity);
        this.floatingTutorialView = new FloatingTutorialView(activity);
        this.floatingTutorialView.setButtonListener((v) -> onFloatingTutorialClicked());
    }

    protected BannerView getBannerView() {
        return bannerView;
    }

    protected FloatingTutorialView getFloatingTutorialView() {
        return floatingTutorialView;
    }

    /* Add new shared functionality if needed */
    protected void onFloatingTutorialClicked() {
        /* Usually used to show the test tutorial */
    }

    public void post(Object event) {
        bus.post(event);
    }
}