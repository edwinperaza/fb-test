package com.facebook.devices.mvp.presenter;

import android.content.ComponentName;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.facebook.devices.mvp.model.BluetoothConnectedAutoModel;
import com.facebook.devices.mvp.view.BluetoothConnectedAutoView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

import static android.app.Activity.RESULT_OK;

public class BluetoothConnectedAutoPresenter extends PresenterTestBase<BluetoothConnectedAutoModel, BluetoothConnectedAutoView> {

    public BluetoothConnectedAutoPresenter(BluetoothConnectedAutoModel model, BluetoothConnectedAutoView view) {
        super(model, view);
    }

    public void onDestroy() {
        model.onDestroy();
    }

    public void onSaveInstantState(Bundle savedInstantState) {
        savedInstantState.putBoolean(BluetoothConnectedAutoModel.PERMISSION_BLUETOOTH_ACCEPTED, model.wasPermissionBluetoothAccepted());
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        preCheck();
    }

    private void preCheck() {
        Log.d("BT_FACE", "preCheck()");
        if (!model.isAvailableBluetoothAdapter()) {
            Log.d("BT_FACE", "preCheck showNoAvailableBluetooth");
            view.showNoAvailableBluetooth();
            testFailed();
            return;
        }

        if (!model.isPermissionGranted()) {
            Log.d("BT_FACE", "preCheck NOT Permission Granted, requestPermissions");
            model.requestPermissions();
            return;
        }

        if (!model.isEnabledBluetoothAdapter()) {
            Log.d("BT_FACE", "onInitDialogPressed requestEnableBluetooth");
            view.requestEnableBluetooth();
            return;
        }

        view.showTutorialButton();
        if (!model.wasPermissionBluetoothAccepted()) {
            Log.d("BT_FACE", "preCheck registerReceiver");
            model.registerReceiver();
        }
        model.permissionBluetoothAccepted();
        Log.d("BT_FACE", "preCheck showInitDialog");
        view.showInitDialog();
    }

    public void onActivityResult(int requestCode, int resultCode) {
        if (resultCode == RESULT_OK && requestCode == BluetoothConnectedAutoModel.REQUEST_ENABLE_BT) {
            preCheck();
        } else {
            testFailed();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            testFailed("User denied permissions");
            return;
        }
        preCheck();
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        if (model.isPermissionGranted()) {
            preCheck();
        }
    }

    @Subscribe
    public void onInitDialogPressedEvent(BluetoothConnectedAutoView.OnInitDialogPressedEvent event) {
        Log.d("BT_FACE", "onInitDialogPressed after isEnabledBluetoothAdapter and model.wasDiscoverableBluetoothShown()");
        view.hideDialog();
        view.showPairingBluetoothLayout();
        model.doDiscovery();
    }

    @Subscribe
    public void onSuccessBluetoothBonded(BluetoothConnectedAutoModel.SuccessBluetoothPaired event) {
        model.cancelDiscoveryProcess();
        if (model.shouldPlayAudio()) {
            view.showConnectToDeviceDialog(event.getDevice());
        } else {
            view.showConnectToDeviceWithoutPlayAudioDialog(event.getDevice());
        }
    }

    @Subscribe
    public void onFailedBluetoothBonded(BluetoothConnectedAutoModel.FailedBluetoothPaired event) {
        model.cancelDiscoveryProcess();
        view.showFailedBluetoothPairedLayout();
        testFailed();
    }

    @Subscribe
    public void onConnectDialogOptionPressed(BluetoothConnectedAutoView.ConnectOptionPressedEvent event) {
        switch (event.getOptionSelected()) {
            case BluetoothConnectedAutoView.ConnectOptionPressedEvent.NO:
                testFailed();
                break;

            case BluetoothConnectedAutoView.ConnectOptionPressedEvent.CONNECT:
                view.hideDialog();
                model.setUpProfileProxy();
                break;
        }
    }

    @Subscribe
    public void onSuccessBluetoothConnected(BluetoothConnectedAutoModel.SuccessBluetoothConnected event) {
        view.showConnectedMessage();
        view.showWaitingForPlayer();
        view.showWaitingDialogView();
    }

    @Subscribe
    public void onIncorrectDialogOptionPressed(BluetoothConnectedAutoView.IncorrectPairedOptionPressedEvent event) {
        switch (event.getOptionSelected()) {
            case BluetoothConnectedAutoView.IncorrectPairedOptionPressedEvent.NO:
                testFailed();
                break;

            case BluetoothConnectedAutoView.IncorrectPairedOptionPressedEvent.TRY_AGAIN:
                preCheck();
                break;
        }
    }

    @Subscribe
    public void onResultPatterMessageInteracted(BluetoothConnectedAutoView.ResultOptionPressedEvent event) {
        switch (event.getOptionSelected()) {
            case BluetoothConnectedAutoView.ResultOptionPressedEvent.NO:
                testFailed();
                break;

            case BluetoothConnectedAutoView.ResultOptionPressedEvent.YES:
                model.pass();
                view.finishActivity();
                break;
        }
    }

    @Subscribe
    public void onWaveFormDataCapture(BluetoothConnectedAutoModel.WaveFormDataCapture event) {
        view.loadingAndFailureLayoutVisibilityGone();
        view.updateWaveView(event.getWaveform());
    }

    @Subscribe
    public void onPlaybackError(BluetoothConnectedAutoModel.PlaybackErrorEvent event) {
        testFailed();
    }

    @Subscribe
    public void onPlaybackSuccessEvent(BluetoothConnectedAutoModel.PlaybackSuccessEvent event) {
        //TODO Show message delay
        view.showAudioPlayingDialogView();
    }

    @Subscribe
    public void onConnectedBluetoothErrorEvent(BluetoothConnectedAutoModel.ConnectedBluetoothErrorEvent event) {
        testFailed();
    }

    @Subscribe
    public void onProfileBluetoothErrorEvent(BluetoothConnectedAutoModel.ProfileBluetoothErrorEvent event) {
        testFailed();
    }

    @Subscribe
    public void onUserFailedEvent(BluetoothConnectedAutoView.OnUserFailedEvent event) {
        testFailed();
    }

    @Subscribe
    public void onUserPassedEvent(BluetoothConnectedAutoView.OnUserPassedEvent event) {
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onProfileBluetoothSuccessEvent(BluetoothConnectedAutoModel.ProfileBluetoothSuccessEvent event) {
        view.showWaitingForPlayer();
        view.showWaitingDialogView();
    }

    private void testFailed(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}