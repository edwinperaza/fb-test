package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.DecibelModel;
import com.facebook.devices.mvp.view.DecibelView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;

import org.greenrobot.eventbus.Subscribe;

public class DecibelPresenter extends PresenterTestBase<DecibelModel, DecibelView> {

    private AudioRecorder.AudioRecorderListener audioRecorderListener = new AudioRecorder.AudioRecorderListener() {

        @Override
        public void onAmplitudeUpdated(float amplitude) {
            model.updateAmplitude(amplitude);
        }
    };

    private final Runnable updater = new Runnable() {
        public void run() {
            view.getUIHandler().postDelayed(this, DecibelModel.DELAY_MEASUREMENT);
            double decibels = model.getDecibel();
            view.getUIHandler().post(() -> view.showDecibel(model.getLevel(decibels), decibels));
            model.updateCurrentDecibelsOrValidate(decibels);
        }
    };

    private Runnable handlerMediaRunnable = model::calculateEnvironmentMedia;

    public DecibelPresenter(DecibelModel model, DecibelView view) {
        super(model, view);
        view.showTutorialButton();
    }

    public void onResume() {
        if (model.isPermissionGranted()) {
            init();
        } else {
            model.requestPermissions();
        }
    }

    public void init() {
        if (model.getActualState() == DecibelModel.STATE_INIT) {
            view.permissionAccepted();
            view.showEnvironmentMeasurementInitDialog();
            model.setMediaState();
        }

        if (model.isRecording()) {
            switch (model.getActualState()) {
                case DecibelModel.STATE_MEDIA:
                    model.clearMediaEnvironmentValues();
                    view.getUIHandler().postDelayed(handlerMediaRunnable, model.getWaitingMeasurementTime());
                    break;
                case DecibelModel.STATE_MEASUREMENT:
                    view.showMeasurementDialog();
                    break;
            }
            return;
        }

        model.startRecording(audioRecorderListener);
        view.getUIHandler().post(updater);
        switch (model.getActualState()) {
            case DecibelModel.STATE_MEDIA:
                view.getUIHandler().postDelayed(handlerMediaRunnable, model.getWaitingMeasurementTime());
                break;
            case DecibelModel.STATE_MEASUREMENT:
                view.showMeasurementDialog();
                break;
        }
    }

    public void onPause() {
        view.getUIHandler().removeCallbacks(updater);
        view.getUIHandler().removeCallbacks(handlerMediaRunnable);
        model.stopRecording();
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        view.permissionAccepted();
    }

    @Subscribe
    public void onMediaTimeUp(DecibelModel.OnMediaTimeUp event) {
        view.getUIHandler().removeCallbacks(handlerMediaRunnable);
        view.getUIHandler().post(() -> view.showEnvironmentMediaResult(event.getMedia(), event.getDifferenceDb()));
        model.setMeasurementState();
        init();
    }

    @Subscribe
    public void onMediaTimeUpFail(DecibelModel.OnMediaTimeUpFail event) {
        view.showEnvironmentMeasurementFailDialog();
    }

    @Subscribe
    public void onValidNearDecibel(DecibelModel.OnValidMeasurementDecibel event) {
        model.stopRecording();
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onButtonFailurePressed(DecibelView.OnButtonFailurePressed event) {
        model.stopRecording();
        model.fail();
        view.finishActivity();
    }

    @Subscribe
    public void onButtonRetryPressed(DecibelView.OnButtonRetryPressed event) {
        init();
    }

}