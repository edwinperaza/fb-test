package com.facebook.devices.mvp.presenter;

import android.content.ComponentName;
import android.os.IBinder;
import android.util.Log;

import com.facebook.aloha.bluetoothremote.IBluetoothRemotePairingService;
import com.facebook.devices.mvp.model.RemoteControlModel;
import com.facebook.devices.mvp.view.RemoteControlView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.mvp.model.RemoteControlModel.*;

public class RemoteControlPresenter extends PresenterTestBase<RemoteControlModel, RemoteControlView> {

    private boolean shouldStartPairTimer = true;
    private boolean shouldStartConnectTimer = true;
    private IBluetoothRemotePairingService iBluetoothRemotePairingService;

    private final Runnable pairingThreadRunnable = model::runPairingThread;
    private final Runnable failTestRunnable = this::failTest;

    private RemotePairingCallback remotePairingCallback = new RemotePairingCallback() {

        /*Paired but not connected*/
        @Override
        public void isRemoteConnected(boolean isRemoteConnected) {
            if (!isRemoteConnected) {
                model.setActualState(PRE_PAIR_AND_CONNECTED);
                view.getUiHandler().post(view::showShouldConnectMessage);
                if (shouldStartConnectTimer) {
                    shouldStartConnectTimer = false;
                    startFailTestRunnableTimer();
                }
            } else {
                removeCallbacks();
                shouldStartConnectTimer = true;
                model.setActualState(KEYCODE_DPAD_CENTER);
                view.getUiHandler().post(view::showConnectedAndStartTestMessage);
            }
        }

        /*Paired state*/
        @Override
        public void isRemotePaired(boolean isRemotePaired) {
            if (!isRemotePaired) {
                model.setActualState(PRE_PAIR_AND_CONNECTED);
                view.getUiHandler().post(view::showShouldPairAndConnectMessage);
                if (shouldStartPairTimer) {
                    shouldStartPairTimer = false;
                    startFailTestRunnableTimer();
                }
                view.getUiHandler().postDelayed(pairingThreadRunnable, model.getTimeToRetryPairing());
            } else {
                removeCallbacks();
                shouldStartPairTimer = true;
            }
        }
    };

    public RemoteControlPresenter(RemoteControlModel model, RemoteControlView view) {
        super(model, view);
        view.showTutorialFloatingButton();
        view.getUiHandler().post(view::showShouldPairAndConnectMessage);
    }

    private void startFailTestRunnableTimer() {
        view.getUiHandler().postDelayed(failTestRunnable, model.getTimeToFail());
    }

    private void removeCallbacks() {
        view.getUiHandler().removeCallbacks(failTestRunnable);
    }

    public void onPause() {
        view.getUiHandler().removeCallbacks(failTestRunnable);
        view.getUiHandler().removeCallbacks(pairingThreadRunnable);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        if (name.getClassName().equals("com.facebook.aloha.bluetoothremote.BluetoothRemotePairingService")) {
            iBluetoothRemotePairingService = IBluetoothRemotePairingService.Stub.asInterface(service);
            model.onServiceBluetoothRemotePairingConnected(iBluetoothRemotePairingService, remotePairingCallback);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        super.onServiceDisconnected(name);
        model.onServiceBluetoothRemotePairingDisconnected();
    }

    @Subscribe
    public void OnButtonPressed(RemoteControlView.OnButtonPressed event) {
        if (model.getActualState() != PRE_PAIR_AND_CONNECTED) {
            Log.d("RemoteControl", "OnButtonPressed != PRE_PAIR_AND_CONNECTED");
            if (model.shouldGoToNextKeyButton(event.getButtonText())) {
                Log.d("RemoteControl", "OnButtonPressed getActualState: " + model.getActualState());
                switch (model.getActualState()) {
                    case KEYCODE_ESCAPE:
                        view.showPressBackButtonMessage();
                        break;
                    case KEYCODE_SEARCH:
                        view.showPressSearchButtonMessage();
                        break;
                    case KEYCODE_MEDIA_PLAY_PAUSE:
                        view.showPressPlayAndPauseButtonMessage();
                        break;
                    case KEYCODE_MENU:
                        view.showPressMenuButtonMessage();
                        break;
                    default:
                        Log.d("RemoteControl", "OnButtonPressed unHandledState default actualState: " + model.getActualState());
                }

                if (model.validateButtons()) {
                    Log.d("RemoteControl", "OnButtonPressed validateButtons(): true");
                    model.pass();
                    view.finishActivity();
                }
            }
        }
    }

    @Subscribe
    public void onFailTestEvent(RemoteControlView.OnFailTestEvent event) {
        failTest();
    }

    @Subscribe
    public void onPassTestEvent(RemoteControlView.OnPassTestEvent event) {
        model.pass();
        view.finishActivity();
    }

    private void failTest(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}