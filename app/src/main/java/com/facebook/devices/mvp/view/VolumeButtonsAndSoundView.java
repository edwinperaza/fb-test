package com.facebook.devices.mvp.view;


import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.ImageView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.CenteredImageSpan;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VolumeButtonsAndSoundView extends TestViewBase {

    @BindView(R.id.iv_volume_icon) ImageView volumeIcon;

    public VolumeButtonsAndSoundView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void showTouchCount(int touchedTimes) {
        switch (touchedTimes) {
            case 0:
                volumeIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.vol_sound_off));
                break;
            case 1:
                volumeIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.vol_sound_level_1));
                break;
            case 2:
                volumeIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.vol_sound_level_2));
                break;
            case 3:
                volumeIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.vol_sound_level_3));
                break;
        }
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();

        CenteredImageSpan imagePlusSpan = new CenteredImageSpan(getContext(), R.drawable.plus_button_small);
        CenteredImageSpan imageLessSpan = new CenteredImageSpan(getContext(), R.drawable.less_button_small);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_tutorial_sound_tutorial_message));

        spannableString.setSpan(imagePlusSpan, 69, 70, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(imageLessSpan, 72, 73, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        DialogFactory.createMaterialDialog(
                getContext(),
                getContext().getString(R.string.more_help_base),
                spannableString)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showQuestionUp() {
        CenteredImageSpan imageSpan = new CenteredImageSpan(getContext(), R.drawable.plus_button);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_up_sound_question_message));

        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), spannableString))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new FailureButtonEvent()))
                        .build()
        );
    }

    public void showQuestionDown() {
        CenteredImageSpan imageSpan = new CenteredImageSpan(getContext(), R.drawable.less_button);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_down_sound_question_message));
        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), spannableString))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new FailureButtonEvent()))
                        .build()
        );
    }

    public void showTutorialButton(boolean animated) {
        getFloatingTutorialView().showButton(animated);
    }

    /**
     * Static classes for BUS
     */
    public static class FailureButtonEvent {
        /*Nothing to do here*/
    }
}