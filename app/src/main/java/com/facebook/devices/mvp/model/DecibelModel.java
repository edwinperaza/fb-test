package com.facebook.devices.mvp.model;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class DecibelModel extends ServiceModel {

    public static final long DELAY_MEASUREMENT = 100;
    private static final int DECIBEL_THRESHOLD_LEVEL = 5;
    private static final int DECIBEL_MAX = 75;
    private static final int SAMPLE_RATE = 44100; /*khz*/

    private final BusProvider.Bus bus;
    private AudioRecorder audioRecorder;
    private PermissionHandler permissionHandler;

    private double referenceAmp;
    private boolean near = false;
    private boolean isRecording = false;
    private double goalDb;
    private long waitingMeasurementTime;
    private double totalDecibels = 0;
    private float amplitude = 0;
    private int count = 0;

    public void updateCurrentDecibelsOrValidate(double decibels) {
        switch (getActualState()) {
            case STATE_MEDIA:
                sumEnvironmentMedia(decibels);
                break;
            case STATE_MEASUREMENT:
                validateNearAudio(decibels);
                break;
        }
    }

    public DecibelModel(BusProvider.Bus bus,
            PermissionHandler permissionHandler,
            double referenceAmp,
            double goalDb,
            long waitingMeasurementTime,
            @NonNull AudioRecorder audioRecorder) {

        this.bus = bus;
        this.permissionHandler = permissionHandler;
        this.referenceAmp = referenceAmp;
        this.goalDb = goalDb;
        this.waitingMeasurementTime = waitingMeasurementTime;

        /*Audio Recorder*/
        this.audioRecorder = audioRecorder;
        this.audioRecorder.setSampleRate(SAMPLE_RATE);
        this.audioRecorder.setMode(AudioRecorder.MODE_DETECTOR);
    }

    @IntDef({STATE_INIT, STATE_MEDIA, STATE_MEASUREMENT})
    @interface State {
    }

    public static final int STATE_INIT = 0;
    public static final int STATE_MEDIA = 1;
    public static final int STATE_MEASUREMENT = 2;

    @State private int actualState = STATE_INIT;

    @State
    public int getActualState() {
        return actualState;
    }

    public void setInitState() {
        actualState = STATE_INIT;
    }

    public void setMediaState() {
        actualState = STATE_MEDIA;
    }

    public void setMeasurementState() {
        actualState = STATE_MEASUREMENT;
    }

    public boolean isRecording() {
        return (audioRecorder != null && isRecording);
    }

    public void startRecording(AudioRecorder.AudioRecorderListener recorderListener) {
        if (!audioRecorder.isRunning()) {
            isRecording = true;
            audioRecorder.startRecording(recorderListener);
        }
    }

    public void stopRecording() {
        if (audioRecorder != null && audioRecorder.isRunning()) {
            audioRecorder.stopRecording();
            isRecording = false;
        }
    }
    public void clearMediaEnvironmentValues() {
        totalDecibels = 0;
        count = 0;
    }

    private void sumEnvironmentMedia(double decibels) {
        if (!Double.isNaN(decibels) && !Double.isInfinite(decibels)) {
            totalDecibels = totalDecibels + decibels;
            count++;
        }
    }

    private void validateNearAudio(double decibels) {
        if (decibels > goalDb && !near) {
            near = true;
            post(new OnValidMeasurementDecibel());
        }
    }

    public void calculateEnvironmentMedia() {
        double environmentMedia = totalDecibels;

        if (count > 0) {
            environmentMedia = totalDecibels / count;
        }

        goalDb = goalDb + environmentMedia;

        if (Double.isNaN(environmentMedia) || Double.isInfinite(environmentMedia) || environmentMedia == 0) {
            post(new OnMediaTimeUpFail());
        } else {
            post(new OnMediaTimeUp(environmentMedia, goalDb));
        }
    }

    public void updateAmplitude(float amplitude) {
        this.amplitude = amplitude;
    }

    /**
     * @return decibel of input signal
     * @see <a href="https://en.wikipedia.org/wiki/Decibel">https://en.wikipedia.org/wiki/Decibel</a>
     * for references see Acustics section
     */
    public double getDecibel() {
        return 20 * Math.log10(amplitude / referenceAmp);
    }

    public int getLevel(Double decibels) {
        if (decibels <= DECIBEL_THRESHOLD_LEVEL) {
            return 1;
        } else {
            int dbValue = (int) (decibels / DECIBEL_THRESHOLD_LEVEL);
            return decibels <= DECIBEL_MAX ? dbValue : 16;
        }
    }

    private void post(Object event) {
        bus.post(event);
    }

    //Methods related to runtime permissions
    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    //Getter and Setter
    public double getTotalDecibels() {
        return totalDecibels;
    }

    public void setTotalDecibels(double totalDecibels) {
        this.totalDecibels = totalDecibels;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setRecording(boolean value) {
        isRecording = value;
    }

    public long getWaitingMeasurementTime() {
        return waitingMeasurementTime;
    }

    //Bus Events
    public static class OnMediaTimeUp {
        double media;
        double differenceDb;

        public OnMediaTimeUp(double media, double differenceDb) {
            this.media = media;
            this.differenceDb = differenceDb;
        }

        public double getMedia() {
            return media;
        }

        public double getDifferenceDb() {
            return differenceDb;
        }
    }

    public static class OnMediaTimeUpFail {
    }

    public static class OnValidMeasurementDecibel {
    }
}