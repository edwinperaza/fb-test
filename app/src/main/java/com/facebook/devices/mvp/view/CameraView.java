package com.facebook.devices.mvp.view;

import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.view.TextureView;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.AutoFitTextureView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CameraView extends TestViewBase {

    @BindView(R.id.texture_camera) AutoFitTextureView textureView;
    @BindView(R.id.camera_access_spinner) ProgressBar cameraAccessSpinner;

    public CameraView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        setupTextureView();
    }

    private void setupTextureView() {
        /*Listener*/
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                post(new OnSurfaceTextureAvailable(surface, width, height));
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                /* This is invoked every time a new camera preview frame is showed */
            }
        });
    }

    /**
     * Apply mirror effect to the camera preview
     */
    public void setMirrorEffect(boolean isMirrorEffect) {
        Matrix matrix = new Matrix();
        if (textureView.getWidth() > textureView.getHeight()) { /*Horizontal*/
            textureView.setScaleX((isMirrorEffect) ? -1 : 1);
        } else { /*Vertical*/
            matrix.postScale((isMirrorEffect) ? -1 : 1, 1, textureView.getWidth() * 0.85f, textureView.getHeight());
            textureView.setTransform(matrix);
        }
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        /* Tutorial */
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.camera_dialog_tutorial_message)
                .setPositiveButton(R.string.ok_base, (dialog, which) -> post(new TutorialEvent(AlertDialogBaseEvent.OPTION_POSITIVE)))
                .show();
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.init_dialog_text_camera))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new SuccessEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new FailureEvent()))
                        .setCollapsingEnabled(true)
                        .build()
        );
    }

    public void showFloatingButtonTutorial() {
        getFloatingTutorialView().showButton();
    }

    public void hideDialog() {
        getBannerView().hideBanner();
    }

    public void hideFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    public TextureView getTextureView() {
        return textureView;
    }

    public void setAspectRatio(int width, int height) {
        textureView.setAspectRatio(width, height);
    }

    public boolean isAvailable() {
        return textureView.isAvailable();
    }

    /**
     * Available states
     */
    public void showCameraAccessSpinner() {
        cameraAccessSpinner.setVisibility(View.VISIBLE);
    }

    public void hideCameraAccessSpinner() {
        cameraAccessSpinner.setVisibility(View.GONE);
    }

    /**
     * Static classes for BUS
     */
    public static class TutorialEvent extends AlertDialogBaseEvent {
        public TutorialEvent(int optionSelected) {
            super(optionSelected);
        }
    }

    public static class SuccessEvent {/*Nothing to do here*/
    }

    public static class FailureEvent {/*Nothing to do here*/
    }

    /* SurfaceTextureView available callbacks */
    public static class OnSurfaceTextureAvailable {
        public SurfaceTexture surface;
        public int width;
        public int height;

        public OnSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            this.surface = surfaceTexture;
            this.width = width;
            this.height = height;
        }
    }
}