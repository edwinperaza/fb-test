package com.facebook.devices.mvp.view;


import android.support.annotation.IntDef;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.TouchDetectorView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TouchPatternView extends TestViewBase {

    @BindView(R.id.touch_detector) TouchDetectorView touchDetectorView;

    public TouchPatternView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        init();
    }

    private void init() {
        /* Touch detector config */
        touchDetectorView.setTouchDetectorViewCallback(resultMode ->
                post(new OnTouchDetectorViewCallback(resultMode))
        );
    }

    public void showBanner() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.touch_pattern_banner_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnBannerOkPressedEvent()))
                        .build()
        );
    }

    public void hideBanner() {
        getBannerView().hideBanner();
    }

    public void showFloatingTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    public void setTouchDetectorEnabled(boolean isEnabled) {
        touchDetectorView.setEnabled(isEnabled);
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.touch_pattern_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void resetTouchDetectorView() {
        touchDetectorView.resetBehavior();
    }

    public void showIncorrectPatternDialog() {
        DialogFactory.createMaterialDialog(
                getContext(),
                0,
                R.string.touch_pattern_line_incorrect_message)
                .setPositiveButton(R.string.try_again_base, (dialog, which) -> post(new IncorrectDialogOptionPressedEvent(IncorrectDialogOptionPressedEvent.TRY_AGAIN)))
                .setNegativeButton(R.string.no_base, (dialog, which) -> post(new IncorrectDialogOptionPressedEvent(IncorrectDialogOptionPressedEvent.NO)))
                .show();
    }

    public void showIncompletePatternDialog() {
        DialogFactory.createMaterialDialog(
                getContext(),
                0,
                R.string.touch_pattern_line_incomplete_message)
                .setPositiveButton(R.string.try_again_base, (dialog, which) -> post(new IncorrectDialogOptionPressedEvent(IncorrectDialogOptionPressedEvent.TRY_AGAIN)))
                .setNegativeButton(R.string.no_base, (dialog, which) -> post(new IncorrectDialogOptionPressedEvent(IncorrectDialogOptionPressedEvent.NO)))
                .show();
    }

    public void setTouchDetectorOrientationMode(@TouchDetectorView.TouchDetectorOrientation int orientationMode, int[] correctPath) {
        touchDetectorView.setOrientationMode(orientationMode, correctPath);
    }

    public void setTouchDetectorSize(@TouchPatternModel.TouchDetectorSize int sizeMode, int[] correctPath) {
        touchDetectorView.setSizeMode(sizeMode, correctPath);
    }

    /**
     * This method should be used to prepare the view
     */
    public void initTouchDetectorView(@TouchDetectorView.TouchDetectorOrientation int orientationMode,
                                      @TouchPatternModel.TouchDetectorSize int sizeMode, int[] correctPath) {
        touchDetectorView.initView(orientationMode, sizeMode, correctPath);
    }

    /**
     * Static classes for BUS
     */
    public static class OnTouchDetectorViewCallback {
        private int result;

        public OnTouchDetectorViewCallback(@TouchDetectorView.TouchDetectorViewCallback.TouchDetectorCallbackResult int result) {
            this.result = result;
        }

        @TouchDetectorView.TouchDetectorViewCallback.TouchDetectorCallbackResult
        public int getResult() {
            return result;
        }
    }

    public static class IncorrectDialogOptionPressedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({NO, TRY_AGAIN})
        @interface OnIncorrectDialogCallbackOptions {
        }

        public static final int NO = 0;
        public static final int TRY_AGAIN = 1;

        private int option;

        public IncorrectDialogOptionPressedEvent(@OnIncorrectDialogCallbackOptions int option) {
            this.option = option;
        }

        @OnIncorrectDialogCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }

    public static class OnBannerOkPressedEvent {/*Nothing to do here*/
    }
}
