package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntRange;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.CenteredImageSpan;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VolumeButtonsView extends TestViewBase {

    public static final float ALPHA_ENABLED = 1;
    public static final float ALPHA_DISABLED = 0.45f;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    @BindView(R.id.tv_count_zero) TextView tvCountZero;
    @BindView(R.id.tv_up_count_one) TextView tvUpCountOne;
    @BindView(R.id.tv_up_count_two) TextView tvUpCountTwo;
    @BindView(R.id.tv_up_count_three) TextView tvUpCountThree;
    @BindView(R.id.tv_down_count_one) TextView tvDownCountOne;
    @BindView(R.id.tv_down_count_two) TextView tvDownCountTwo;
    @BindView(R.id.tv_down_count_three) TextView tvDownCountThree;

    public VolumeButtonsView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void completeUpKeyDelay() {
        uiHandler.postDelayed(() -> post(new OnHandlerUiUpCompleteEvent()), 500);
    }

    public void updateCountValuePositive(@IntRange(from = 0, to = 3) int count) {
        switch (count) {
            case 3:
                tvUpCountThree.setAlpha(ALPHA_ENABLED);
                /*Falls through*/
            case 2:
                tvUpCountTwo.setAlpha(ALPHA_ENABLED);
                /*Falls through*/
            case 1:
                tvUpCountOne.setAlpha(ALPHA_ENABLED);
                break;
            case 0:
                resetCountValue();
                break;
        }
    }

    public void updateCountValueNegative(@IntRange(from = 0, to = 3) int count) {
        switch (count) {
            case 3:
                tvDownCountThree.setAlpha(ALPHA_ENABLED);
                /*Falls through*/
            case 2:
                tvDownCountTwo.setAlpha(ALPHA_ENABLED);
                /*Falls through*/
            case 1:
                tvDownCountOne.setAlpha(ALPHA_ENABLED);
                break;
            case 0:
                resetCountValue();
                break;
        }
    }

    public void resetCountValue() {
        tvCountZero.setAlpha(ALPHA_ENABLED);
        tvUpCountOne.setAlpha(ALPHA_DISABLED);
        tvUpCountTwo.setAlpha(ALPHA_DISABLED);
        tvUpCountThree.setAlpha(ALPHA_DISABLED);
        tvDownCountOne.setAlpha(ALPHA_DISABLED);
        tvDownCountTwo.setAlpha(ALPHA_DISABLED);
        tvDownCountThree.setAlpha(ALPHA_DISABLED);
    }

    public void showFloatingTutorial() {
        getFloatingTutorialView().showButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        CenteredImageSpan imagePlusSpan = new CenteredImageSpan(getContext(), R.drawable.plus_button_small);
        CenteredImageSpan imageLessSpan = new CenteredImageSpan(getContext(), R.drawable.less_button_small);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_tutorial_no_sound_tutorial_message));

        spannableString.setSpan(imagePlusSpan, 13, 14, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(imageLessSpan, 15, 16, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

        DialogFactory.createMaterialDialog(
                getContext(),
                getContext().getString(R.string.more_help_base),
                spannableString)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showQuestionUp() {
        CenteredImageSpan imageSpan = new CenteredImageSpan(getContext(), R.drawable.plus_button);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_up_no_sound_question_message));

        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), spannableString))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailureClicked()))
                        .build()
        );
    }

    public void showQuestionDown() {
        CenteredImageSpan imageSpan = new CenteredImageSpan(getContext(), R.drawable.less_button);
        SpannableString spannableString = new SpannableString(getContext().getString(R.string.volume_down_no_sound_question_message));

        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), spannableString))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailureClicked()))
                        .build()
        );
    }

    /**
     * Static classes for BUS
     */
    public static class OnButtonFailureClicked {/*Nothing to do here*/
    }

    public static class OnHandlerUiUpCompleteEvent { /*Nothing to do here*/
    }

}