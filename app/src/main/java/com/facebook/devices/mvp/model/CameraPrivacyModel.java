package com.facebook.devices.mvp.model;

import android.annotation.SuppressLint;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.CameraPresenter;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.devices.utils.Size;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.devices.utils.customcomponents.CancellableHandler;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CameraPrivacyModel extends ServiceModel {

    public static final String CAMERA_ID_RECEIVED = "CameraID";
    public static final String PRIVACY_BUTTON_WORKING = "PrivacyButton";
    private static final float THRESHOLD = 1.03f; //adding 3% of acceptable range

    /*Fixed resolution*/
    private static final int DEFAULT_FIX_RES_WIDTH = 1600;
    private static final int DEFAULT_FIX_RES_HEIGHT = 1200;

    private ResourcesManager resourcesManager = new ResourcesManager();

    private boolean stopCameraBroadcastSent = false;

    /* Ratios */
    private static final float RATIO_16_9 = (float) 16 / 9;
    private static final float RATIO_4_3 = (float) 4 / 3;
    private static final float RATIO_3_2 = (float) 3 / 2;
    private static final float RATIO_21_9 = (float) 21 / 9;

    private CameraManager cameraManager;
    private BusProvider.Bus bus;
    private CameraUtils cameraUtils;
    private RuntimePermissionHandler permissionHandler;
    private boolean privacyWorking = false;
    private CancellableHandler uiHandler;
    private boolean isMirrored;

    /* Running task in the background stuffs */
    private HandlerThread backgroundThread;
    private Handler backgroundHandler;

    /* Camera Data Objects */
    private CameraCaptureSession cameraCaptureSession;
    private CameraDevice cameraDevice;
    private Size cameraPreviewSize;

    /* Handles still image capture */
    private ImageReader imageReader;

    /* Builder for the camera preview */
    private CaptureRequest.Builder previewRequestBuilder;
    private int sensorOrientation;

    /*Camera flow callbacks */
    private boolean isCameraAvailabilityRegistered;

    private CameraManager.AvailabilityCallback availabilityCallback = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            try {
                if (isCameraTarget(cameraId) && !getResourcesManager().isCameraResourceReady()) {
                    getResourcesManager().setCameraResourceReady(cameraId);
                    post(new OnCameraTargetAvailable());
                }
            } catch (CameraAccessException e) {
                Log.e("CameraError", e.getMessage());
            }
        }

        @Override
        public void onCameraUnavailable(@NonNull String cameraId) {
            /* Nothing to do here for now */
        }
    };

    public CameraPrivacyModel(CameraManager cameraManager, BusProvider.Bus bus,
            RuntimePermissionHandler permissionHandler, CancellableHandler uiHandler,
            CameraUtils cameraUtils, boolean isMirrored) {
        this.cameraManager = cameraManager;
        this.bus = bus;
        this.permissionHandler = permissionHandler;
        this.cameraUtils = cameraUtils;
        this.uiHandler = uiHandler;
        this.isMirrored = isMirrored;
    }

    public CancellableHandler getUiHandler() {
        return uiHandler;
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    public boolean isMirrored() {
        return isMirrored;
    }

    public ResourcesManager getResourcesManager() {
        return resourcesManager;
    }

    /*Check for specific camera*/
    private boolean isCameraTarget(String cameraId) throws CameraAccessException {
        Integer facing = getCameraManager().getCameraCharacteristics(cameraId).get(CameraCharacteristics.LENS_FACING);
        return facing != null && facing == CameraCharacteristics.LENS_FACING_BACK;
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    private void unregisterAvailabilityCallback() {
        getCameraManager().unregisterAvailabilityCallback(availabilityCallback);
        isCameraAvailabilityRegistered = false;
    }

    private void registerAvailabilityCallback() {
        getCameraManager().registerAvailabilityCallback(availabilityCallback, null);
        isCameraAvailabilityRegistered = true;
    }

    @SuppressLint("MissingPermission")
    public void openCamera(TextureView textureView) {

        /* Camera Availability listener */
        if (!isCameraAvailabilityRegistered) {
            registerAvailabilityCallback();
        }

        /* Send unlock camera broadcast */
        if (!getResourcesManager().isCameraResourceReady()) {
            //sendCameraUnlockRequest();
        }

        /* Thread state creation if needed */
        if (backgroundThread == null && backgroundHandler == null) {
            backgroundThread = new HandlerThread("Camera BackgroundThread");
            backgroundThread.start();
            backgroundHandler = new Handler(backgroundThread.getLooper());
        }

        /* Check resources availability */
        if (!getResourcesManager().allResourcesReady()) {
            /* Resources not available yet */
            return;
        }

        /* All resource ready - Open camera */
        getResourcesManager().setCameraInControl();
        try {
            /*Basic configuration*/
            String cameraId = getResourcesManager().getCameraId();
            Size previewRatio = setupCameraOutputs(textureView, cameraId);

            /*Camera opening*/
            getCameraManager().openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice camera) {
                    cameraDevice = camera;
                    createCameraPreview(camera, textureView);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice camera) {
                    camera.close();
                    cameraDevice = null;
                }

                @Override
                public void onError(@NonNull CameraDevice camera, int error) {
                    camera.close();
                    cameraDevice = null;
                    post(new OnPrivacyButtonPressed());
                }
            }, null);

            post(new CameraOutputSetEvent(previewRatio));
        } catch (Throwable e) {
            post(new OnCameraError("OpenCameraException: " + e.getMessage()));
        }
    }

    private Size setupCameraOutputs(TextureView textureView, String cameraId) throws CameraAccessException {
        /* Check stream config map */
        StreamConfigurationMap map = cameraUtils.getCameraStreamConfigurationMap(getCameraManager(), cameraId);
        if (map != null) {
            sensorOrientation = cameraUtils.getSensorOrientation(getCameraManager(), cameraId);

            // Size largest = model.getLargesSize(GenericUtils.arrayToSizeArray(map.getOutputSizes(ImageFormat.JPEG)); - NOT REMOVE PLEASE -
            Size largest = getDefaultFixedResolution();

            Size previewRatio = getRatio(largest);

            cameraPreviewSize = chooseOptimalPreviewResolution(GenericUtils.arrayToSizeArray(map.getOutputSizes(ImageFormat.JPEG)),
                    new Size(textureView.getWidth(), textureView.getHeight()), previewRatio);

            return previewRatio;
        }
        return null;
    }

    private void createCameraPreview(CameraDevice camera, TextureView textureView) {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;

            texture.setDefaultBufferSize(cameraPreviewSize.getWidth(), cameraPreviewSize.getHeight());
            Surface surface = new Surface(texture);

            // Set FOV to 140 degree
            disableCameraSourceCropping(surface);

            previewRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(surface);

            camera.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    cameraCaptureSession = session;
                    try {
                        /*Update preview*/
                        if (cameraDevice != null) {
                            previewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
                            cameraCaptureSession.setRepeatingRequest(previewRequestBuilder.build(), null, backgroundHandler);
                            post(new OnCameraPreviewSetEvent());
                        }
                    } catch (Throwable e) {
                        post(new OnCameraError("CreateCaptureSession -  onConfigured: " + e.getMessage()));
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    post(new OnCameraError("CreateCaptureSession -  onConfigureFailed: " + session.toString()));
                }
            }, null);
        } catch (Throwable e) {
            post(new OnCameraError("CreateCameraPreview - exception: " + e.getMessage()));
        }
    }

    private static void disableCameraSourceCropping(Surface surfaceHandle) {
        try {
            Method methodHandle = Surface.class.getMethod("disableCameraSourceCropping", null);
            methodHandle.invoke(surfaceHandle, null);
        } catch (NoSuchMethodException e) {
            Log.i(CameraPresenter.class.getName(), "Disabling of Camera source cropping unavailable on this platform:", e);
        } catch (SecurityException e) {
            Log.w(CameraPresenter.class.getName(), "Reflection access denied:", e);
        } catch (IllegalAccessException | InvocationTargetException e) {
            Log.w(CameraPresenter.class.getName(), e.getMessage());
        }
    }

    public boolean isCameraAvailabilityRegistered() {
        return isCameraAvailabilityRegistered;
    }

    public boolean isStopCameraBroadcastSent() {
        return stopCameraBroadcastSent;
    }

    public void sendCameraUnlockRequest() {
        cameraUtils.sendCameraUnlockRequest();
        stopCameraBroadcastSent = true;
    }

    public void sendCameraLockRequest() {
        cameraUtils.sendCameraLockRequest();
    }

    /**
     * Must be called when internal process of camera recorded needs to be
     * paused due they are no longer necessary
     * <p>
     * {@link #openCamera(TextureView)} must be called if you want to operate it again after this
     * method is called
     */
    public void pauseCamera() {
        if (backgroundThread != null) {
            backgroundThread.quitSafely();
            try {
                backgroundThread.join();
                backgroundThread = null;
                backgroundHandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*Clear capture session*/
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        /*Clear camera device*/
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }

        /*Clear image reader*/
        if (imageReader != null) {
            imageReader.close();
            imageReader = null;
        }
    }

    /**
     * @param releaseAllResources true only if camera should be locked again and the activity
     *                            will be destroyed
     */
    public void stopCamera(boolean releaseAllResources) {
        /*Unregister availability callback*/
        unregisterAvailabilityCallback();
        /*Clear resources*/
        getResourcesManager().setAllResourcesUnavailable();

        if (releaseAllResources) { /* Activity in on destroying flow */
            //sendCameraLockRequest();
        }
    }

    public Size getRatio(Size resolution) {

        float ratio = (float) resolution.getWidth() / resolution.getHeight();

        if (ratio == RATIO_16_9) {
            return new Size(16, 9);
        }

        if (ratio == RATIO_4_3) {
            return new Size(4, 3);
        }

        if (ratio == RATIO_3_2) {
            return new Size(3, 2);
        }

        if (ratio == RATIO_21_9) {
            return new Size(21, 9);
        }

        return new Size(16, 9); //Default value 16:9
    }

    public Size chooseOptimalPreviewResolution(Size[] choices, Size viewSize, Size aspectRatio) {

        List<Size> resInRatio = new ArrayList<>();
        double ratio = (double) aspectRatio.getWidth() / aspectRatio.getHeight();

        int areaView = (int) ((viewSize.getWidth() * viewSize.getHeight()) * THRESHOLD);
        int areaDifference = Integer.MAX_VALUE;
        Size choice = null;

        for (Size option : choices) {
            /*Check for correct ratio*/
            if (ratio == (double) option.getWidth() / option.getHeight()) {
                resInRatio.add(option);

                int optionArea = option.getWidth() * option.getHeight();
                int optionAreaDifference = areaView - optionArea;
                if (areaView >= optionArea && areaDifference > optionAreaDifference) {
                    areaDifference = optionAreaDifference;
                    choice = option;
                }
            }
        }

        return (choice != null) ? choice : Collections.min(resInRatio, new CompareSizesByArea());
    }

    /* Compares two sizes based on their areas */
    private class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    public Size getLargesSize(Size[] sizes) {
        return Collections.max(Arrays.asList(sizes), new CompareSizesByArea());
    }

    /**
     * Only use this method for specific needs, otherwise use {@link CameraPrivacyModel#getLargesSize(Size[])}
     * in combination with {@link CameraPrivacyModel#getRatio(Size)} and {@link CameraPrivacyModel#chooseOptimalPreviewResolution(Size[], Size, Size)} to get the perfect fit
     *
     * @return the pre-set size according to specifications
     */
    public Size getDefaultFixedResolution() {
        return new Size(DEFAULT_FIX_RES_WIDTH, DEFAULT_FIX_RES_HEIGHT);
    }

    public class ResourcesManager {

        private String cameraId = null;
        private boolean surfaceTextureReady = false;
        private boolean cameraInControl = false;

        public ResourcesManager() {
        }

        public void setCameraResourceReady(String cameraID) {
            this.cameraId = cameraID;
        }

        public void setCameraResourceUnavailable() {
            cameraId = null;
        }

        public void setSurfaceTextureReady() {
            surfaceTextureReady = true;
        }

        public void setSurfaceTextureUnavailable() {
            surfaceTextureReady = false;
        }

        public boolean allResourcesReady() {
            return cameraId != null && surfaceTextureReady && !cameraInControl;
        }

        public String getCameraId() {
            return cameraId;
        }

        public void setCameraInControl() {
            cameraInControl = true;
        }

        public void setCameraReleased() {
            cameraInControl = false;
        }

        public boolean isCameraInControl() {
            return cameraInControl;
        }

        public boolean isCameraResourceReady() {
            return cameraId != null;
        }

        public void setAllResourcesUnavailable() {
            setCameraResourceUnavailable();
            setCameraReleased();
        }
    }

    public void post(Object object) {
        bus.post(object);
    }

    public static class CameraOutputSetEvent {

        private Size previewRatio;

        public CameraOutputSetEvent(Size previewRatio) {
            this.previewRatio = previewRatio;
        }

        public Size getPreviewRatio() {
            return previewRatio;
        }
    }

    public static class OnCameraTargetAvailable {
        /*Nothing to do here*/
    }

    public static class OnPrivacyButtonPressed {
        /*Nothing to do here*/
    }


    public static class OnCameraError {
        private String message;

        public OnCameraError(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class OnCameraPreviewSetEvent {
        /*Nothing to add here*/
    }

    public void privacyButtonWorking() {
        privacyWorking = true;
    }

    public void privacyButtonFailed() {
        privacyWorking = false;
    }

    public boolean isPrivacyWorking() {
        return privacyWorking;
    }

}
