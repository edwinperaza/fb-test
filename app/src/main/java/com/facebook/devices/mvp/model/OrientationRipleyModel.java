package com.facebook.devices.mvp.model;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class OrientationRipleyModel extends ServiceModel {

    private SensorManager sensorManager;
    private BusProvider.Bus bus;

    /*States*/
    private boolean portraitExecuted = false;
    private boolean landscapeExecuted = false;

    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                float axisX = Math.abs(event.values[0]);
                float axisY = Math.abs(event.values[1]);
                float axisZ = Math.abs(event.values[2]);
                Log.d("Ripley",
                        "axisX: " + axisX + ", axisY: " + axisY + ", axisZ: " + axisZ);

                /*Check landscape state*/
                if ((axisY > 8) && (axisY > axisZ) && (axisY > axisX)) {
                    landscapeExecuted = true;
                    bus.post(new OnLandscapeMode());
                    return;
                }

                /*Check portrait state*/
                if ((axisX > 8) && (axisX > axisZ) && (axisX > axisY)) {
                    portraitExecuted = true;
                    bus.post(new OnPortraitMode());
                    return;
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            /*Nothing to do here*/
        }
    };

    public OrientationRipleyModel(SensorManager sensorManager, BusProvider.Bus bus) {
        this.sensorManager = sensorManager;
        this.bus = bus;
    }

    public boolean allPositionsCovered(){
        return portraitExecuted && landscapeExecuted;
    }

    public void startSensing() {
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer != null) {
            sensorManager.registerListener(sensorEventListener,
                    accelerometer,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void stopSensing() {
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        if (accelerometer != null) {
            sensorManager.unregisterListener(sensorEventListener, accelerometer);
        }
    }

    /**
     * Static classes for BUS
     */
    public static class OnPortraitMode {
        /*Nothing to do here*/
    }

    public static class OnLandscapeMode {
        /*Nothing to do here*/
    }
}
