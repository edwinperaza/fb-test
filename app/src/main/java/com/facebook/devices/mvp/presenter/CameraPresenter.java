package com.facebook.devices.mvp.presenter;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.facebook.devices.mvp.model.CameraModel;
import com.facebook.devices.mvp.view.CameraView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

public class CameraPresenter extends PresenterTestBase<CameraModel, CameraView> {

    public CameraPresenter(CameraModel model, CameraView view) {
        super(model, view);
    }

    public void onStart() {
        if (model.isPermissionGranted()) {
            view.hideCameraAccessSpinner();
        }
    }

    public void onStop(boolean isChangingConfigurations) {
        model.getUiHandler().removeCallbacks();
        model.stopCamera(!isChangingConfigurations);
    }

    public void onDestroy() {
        /*Clear resources*/
        model.getResourcesManager().setSurfaceTextureUnavailable();
    }

    /* Called when view attached is resumed */
    public void onViewResumed() {
        /* Check for permissions */
        if (!model.isPermissionGranted()) {
            model.requestPermissions();
            return;
        }

        /*UI components*/
        view.showInitDialog();
        view.showFloatingButtonTutorial();

        /* Run camera preview */
        openCamera();
    }

    /* Called when view attached is paused */
    public void onViewPaused() {
        if (model.isPermissionGranted()) {
            model.pauseCamera();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (model.isStopCameraBroadcastSent()) {
            outState.putString(CameraModel.CAMERA_ID_RECEIVED, model.getResourcesManager().getCameraId());
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CameraModel.CAMERA_ID_RECEIVED)) {
            model.getResourcesManager().setCameraResourceReady(savedInstanceState.getString(CameraModel.CAMERA_ID_RECEIVED));
        }
    }

    @SuppressLint("MissingPermission")
    private void openCamera() {
        view.showCameraAccessSpinner();
        model.openCamera(view.getTextureView());
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionGrantedEvent event) {
        view.hideCameraAccessSpinner();
    }

    /**
     * SurfaceTextureView callbacks
     */
    @Subscribe
    public void onSurfaceTextureAvailable(CameraView.OnSurfaceTextureAvailable event) {
        model.getResourcesManager().setSurfaceTextureReady();
        if (model.isPermissionGranted()) {
            openCamera();
        }
    }

    @Subscribe
    public void onCameraOutputSetEvent(CameraModel.CameraOutputSetEvent event) {
        /*Mirror effect*/
        view.setMirrorEffect(model.isMirrored());
        view.setAspectRatio(event.getPreviewRatio().getWidth(), event.getPreviewRatio().getHeight());
    }

    @Subscribe
    public void onCameraPreviewSetEvent(CameraModel.OnCameraPreviewSetEvent event) {
        /*UI changes*/
        view.hideCameraAccessSpinner();
    }

    @Subscribe
    public void onOpenCameraError(CameraModel.OnCameraError event) {
        Log.d(CameraPresenter.class.getName(), "Error " + event.getMessage());
        fail(event.getMessage());
    }

    @Subscribe
    public void onCameraTargetAvailable(CameraModel.OnCameraTargetAvailable event) {
        model.openCamera(view.getTextureView());
    }

    @Subscribe
    public void onSuccessButtonClicked(CameraView.SuccessEvent event) {
        pass();
    }

    @Subscribe
    public void onFailureButtonClicked(CameraView.FailureEvent event) {
        fail();
    }

    private void pass(String... message) {
        model.pass(message);
        view.finishActivity();
    }

    private void fail(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}
