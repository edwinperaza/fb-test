package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemoryView extends TestViewBase {

    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.tv_percentage) TextView percentage;
    @BindView(R.id.tv_label) TextView label;
    @BindView(R.id.iv_hour_digit_one) ImageView ivHourOne;
    @BindView(R.id.iv_hour_digit_two) ImageView ivHourTwo;
    @BindView(R.id.iv_minutes_digit_one) ImageView ivMinOne;
    @BindView(R.id.iv_minutes_digit_two) ImageView ivMinTwo;
    @BindView(R.id.iv_seconds_digit_one) ImageView ivSecOne;
    @BindView(R.id.iv_seconds_digit_two) ImageView ivSecTwo;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public MemoryView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    @Override
    public void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.memory_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showSetup() {
        label.setText(R.string.allocate_memory);
    }

    public void showStart() {
        label.setText(R.string.start_memory_test);
    }

    public void showValidation() {
        label.setText(R.string.memory_validation);
    }

    public void updateProgressLabel(float progress) {
        percentage.setText(String.format("%.1f%%", progress));
    }

    public void updateProgressBar(int progress) {
        progressBar.setProgress(progress);
    }

    public void setMax(int max) {
        progressBar.setMax(max);
    }

    public void setMin(int min) {
        progressBar.setMax(min);
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void showWritingDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.memory_writing_question))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showValidationDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.memory_validation_question))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showRemainingTime(int hourOne, int hourTwo,
                                  int minOne, int minTwo,
                                  int secOne, int secTwo) {
        ivHourOne.setImageDrawable(getContext().getDrawable(hourOne));
        ivHourTwo.setImageDrawable(getContext().getDrawable(hourTwo));
        ivMinOne.setImageDrawable(getContext().getDrawable(minOne));
        ivMinTwo.setImageDrawable(getContext().getDrawable(minTwo));
        ivSecOne.setImageDrawable(getContext().getDrawable(secOne));
        ivSecTwo.setImageDrawable(getContext().getDrawable(secTwo));
    }

    public static class OnButtonFailurePressed {/*Nothing to do here*/
    }
}
