package com.facebook.devices.mvp.model;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class LightSensorModel extends ServiceModel {

    private SensorManager sensorManager;
    private BusProvider.Bus bus;
    private float minLux;
    private float maxLux;
    private boolean wasMaxLuxReached = false;
    private boolean wasMinLuxReached = false;

    public LightSensorModel(SensorManager sensorManager, BusProvider.Bus bus, float minLux, float maxLux) {
        this.sensorManager = sensorManager;
        this.bus = bus;
        this.minLux = minLux;
        this.maxLux = maxLux;
    }

    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                bus.post(new LightSensorEvent(event.values[0]));
                if (event.values[0] >= maxLux) {
                    wasMaxLuxReached = true;
                    bus.post(new LightSensorUncoverEvent());
                } else if (event.values[0] <= minLux) {
                    wasMinLuxReached = true;
                    bus.post(new LightSensorCoverEvent());
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public boolean wasMeasurementSuccess() {
        return wasMaxLuxReached && wasMinLuxReached;
    }

    public void startSensing() {
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (lightSensor != null) {
            sensorManager.registerListener(sensorEventListener,
                    lightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void stopSensing() {
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (lightSensor != null) {
            sensorManager.unregisterListener(sensorEventListener, lightSensor);
        }
    }

    public static class LightSensorUncoverEvent { /*Nothing to do here*/
    }

    public static class LightSensorCoverEvent { /*Nothing to do here*/
    }

    public static class LightSensorEvent {
        public float value;

        public LightSensorEvent(float value) {
            this.value = value;
        }
    }


}