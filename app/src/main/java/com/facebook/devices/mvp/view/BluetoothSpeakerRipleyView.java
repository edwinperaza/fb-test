package com.facebook.devices.mvp.view;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.annotation.IntDef;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BluetoothSpeakerRipleyView extends TestViewBase {

    @BindView(R.id.cl_animation_container) ConstraintLayout informationLayout;
    @BindView(R.id.tv_waiting_message) TextView tvWaitingMessage;
    @BindView(R.id.lottie_animation) LottieAnimationView lottieAnimationView;

    private AlertDialog tutorialDialog;

    public BluetoothSpeakerRipleyView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        setup();
    }

    private void setup() {
        /*Tutorial dialog setup*/
        tutorialDialog = DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.bluetooth_tutorial_dialog_message)
                .setPositiveButton(R.string.ok_base, null)
                .create();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        if (tutorialDialog != null && !tutorialDialog.isShowing()) {
            tutorialDialog.show();
        }
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton(true);
    }

    public void showNoAvailableBluetooth() {
        Toast.makeText(getContext(), R.string.bluetooth_not_available, Toast.LENGTH_LONG).show();
    }

    public void showFailedBluetoothPairedMessage() {
        Toast.makeText(getContext(), getActivity().getString(R.string.bluetooth_device_paired_failed), Toast.LENGTH_LONG).show();
        informationLayout.setVisibility(View.GONE);
    }

    public void requestEnableBluetooth() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        getActivity().startActivityForResult(enableBtIntent, BluetoothSpeakerModel.REQUEST_ENABLE_BT);
    }

    public void requestDiscoverableBluetooth() {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, BluetoothSpeakerModel.REQUEST_DISCOVERABLE_BT_SECONDS);
        getActivity().startActivityForResult(discoverableIntent, BluetoothSpeakerModel.REQUEST_DISCOVERABLE_BT);
    }

    public void showDisconnectedMessage() {
        Toast.makeText(getContext(), getActivity().getString(R.string.bluetooth_device_disconnected), Toast.LENGTH_LONG).show();
        informationLayout.setVisibility(View.GONE);
    }

    public void showPairingBluetoothLayout(String bluetoothName) {
        informationLayout.setVisibility(View.VISIBLE);
        lottieAnimationView.setVisibility(View.VISIBLE);

        tvWaitingMessage.setText(getContext().getString(R.string.bluetooth_waiting_message, bluetoothName));

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_pairing_dialog_message))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new FailedBtnPressedEvent()))
                        .build());
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_init_dialog_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new OnInitDialogPressedEvent()))
                        .build()
        );
    }

    public void showConnectedMessage(String deviceName, String deviceAddress) {
        lottieAnimationView.setVisibility(View.GONE);

        /* Close tutorial if showed*/
        if (tutorialDialog != null && tutorialDialog.isShowing()) {
            tutorialDialog.dismiss();
        }
        tvWaitingMessage.setText(getContext().getString(R.string.bluetooth_connection_string, deviceName, deviceAddress));

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.bluetooth_ripley_test_result_title))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new ResultOptionPressedEvent(ResultOptionPressedEvent.YES)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new ResultOptionPressedEvent(ResultOptionPressedEvent.NO)))
                        .build()
        );
    }

    /**
     * Static classes for BUS
     */
    public static class FailedBtnPressedEvent {/*Nothing to do here*/
    }

    public static class OnInitDialogPressedEvent {/*Nothing to do here*/
    }

    public static class ResultOptionPressedEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({NO, YES})
        @interface ResultCallbackOptions {
        }

        public static final int NO = 0;
        public static final int YES = 1;

        private int option;

        public ResultOptionPressedEvent(@ResultCallbackOptions int option) {
            this.option = option;
        }

        @ResultCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }
}