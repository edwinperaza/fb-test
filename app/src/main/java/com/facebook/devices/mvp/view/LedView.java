package com.facebook.devices.mvp.view;


import android.support.annotation.IntDef;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LedView extends TestViewBase {

    @BindView(R.id.tv_user_message) TextView tvUserMessage;

    public LedView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);

        /*Hide tutorial icon*/
        getFloatingTutorialView().hideButton(false);
    }

    public void changeLedLocationMessage(String location) {
        tvUserMessage.setText(getContext().getString(R.string.led_location_message, location));
    }

    public void showQuestionDialog(String location) {
        DialogFactory.createMaterialDialog(
                getContext(),
                null,
                getContext().getString(R.string.led_dialog_title, location))
                .setPositiveButton(R.string.yes_base, ((dialog, which) -> post(new OnDialogOptionSelected(OnDialogOptionSelected.OPTION_POSITIVE))))
                .setNegativeButton(R.string.no_base, (dialog, which) -> post(new OnDialogOptionSelected(OnDialogOptionSelected.OPTION_NEGATIVE)))
                .setNeutralButton(R.string.try_again_base, ((dialog, which) -> post(new OnDialogOptionSelected(OnDialogOptionSelected.OPTION_NEUTRAL))))
                .show();
    }

    /**
     * Static classes for BUS
     */
    public static class OnDialogOptionSelected {

        @IntDef({OPTION_POSITIVE, OPTION_NEGATIVE, OPTION_NEUTRAL})
        @Retention(RetentionPolicy.SOURCE)
        @interface Options {
        }

        public static final int OPTION_POSITIVE = 0;
        public static final int OPTION_NEGATIVE = 1;
        public static final int OPTION_NEUTRAL = 2;

        @Options private int optionSelected;

        public OnDialogOptionSelected(@Options int optionSelected) {
            this.optionSelected = optionSelected;
        }

        @Options
        public int getOptionSelected() {
            return optionSelected;
        }
    }

}
