package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.devices.mvp.view.GridTouchPatternView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.mvp.view.GridTouchPatternView.TimeUpDialogEvent.FAIL_TEST;
import static com.facebook.devices.mvp.view.GridTouchPatternView.TimeUpDialogEvent.KEEP_TOUCHING;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_RELEASED;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_TOUCHING;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS;

public class GridTouchPatternPresenter extends PresenterTestBase<GridTouchPatternModel, GridTouchPatternView> {

    private GridTouchPatternModel.TouchPatternSimpleModelCallback modelCallback = view::showTimeUpPatternDialog;

    public GridTouchPatternPresenter(GridTouchPatternModel model, GridTouchPatternView view) {
        super(model, view);
        init();
    }

    public void init() {
        model.setCallback(modelCallback);

        view.initTouchDetectorView(model.getTouchSize());

        view.disableTouch();
        view.showInitDialog();
        view.showTutorialFloatingButton();
    }

    public void onStop() {
        model.cancelTimer();
    }

    @Subscribe
    public void onInitDialogEvent(GridTouchPatternView.InitDialogEvent event) {
        view.hideInitDialog();
        view.hideTutorialFloatingButton();
        view.enableTouch();
        model.startTimer();
    }

    @Subscribe
    public void onTutorialShowed(GridTouchPatternView.TutorialFloatingButtonClicked event) {
        model.cancelTimer();
    }

    @Subscribe
    public void onTouchDetectorStateChanged(GridTouchPatternView.OnTouchDetectorStateChanged event) {
        switch (event.getResult()) {
            case STATE_RELEASED:
                model.startTimer();
                break;

            case STATE_TOUCHING:
                model.cancelTimer();
                break;
        }
    }

    @Subscribe
    public void onTouchDetectorCallback(GridTouchPatternView.TouchDetectorSimpleResult event) {
        switch (event.getResult()) {
            case FINISHED_PATTERN_SUCCESS:
                model.pass();
                view.finishActivity();
                break;
        }
    }

    @Subscribe
    public void onTimeUpDialogOptionEvent(GridTouchPatternView.TimeUpDialogEvent event) {
        switch (event.getOptionSelected()) {
            case KEEP_TOUCHING:
                model.startTimer();
                break;

            case FAIL_TEST:
                model.fail();
                view.finishActivity();
                break;
        }
    }
}
