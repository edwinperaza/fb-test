package com.facebook.devices.mvp.model;


import android.support.annotation.IntDef;

import com.facebook.hwtp.mvp.model.ServiceModel;

public class VolumeButtonsModel extends ServiceModel {

    @IntDef({MODE_UP, MODE_DOWN})
    @interface Mode {
    }

    public static final int MODE_UP = 1;
    public static final int MODE_DOWN = 2;
    public static final int MAX_TIME_PRESSING = 3;

    @Mode private int currentMode = MODE_UP;

    private int touchedTimes = 0;

    public VolumeButtonsModel() {
    }

    private boolean checkForNextMode() {
        if (touchedTimes == MAX_TIME_PRESSING){
            currentMode = (currentMode + 1 > MODE_DOWN)? MODE_DOWN : currentMode + 1;
            touchedTimes = 0;
            return true;
        }

        return false;
    }

    public void increaseTouchedTimes(){
        touchedTimes++;
    }

    public boolean isStepUpCompleted() {
        return currentMode == VolumeButtonsModel.MODE_UP && checkForNextMode();
    }

    public boolean isStepDownCompleted() {
        return currentMode == VolumeButtonsModel.MODE_DOWN && checkForNextMode();
    }

    @Mode
    public int getCurrentMode() {
        return currentMode;
    }

    public int getTouchedTimes() {
        return touchedTimes;
    }
}
