package com.facebook.devices.mvp.presenter;

import android.util.Log;

import com.facebook.devices.mvp.model.MemoryModel;
import com.facebook.devices.mvp.view.MemoryView;

import org.greenrobot.eventbus.Subscribe;

public class MemoryPresenter extends PresenterTestBase<MemoryModel, MemoryView> {

    public MemoryPresenter(MemoryModel model, MemoryView view) {
        super(model, view);
        model.setMemoryProgressCallback(memoryProgressCallback);
        view.showTutorialButton();
    }

    public void onResume() {
        model.runMemoryTest();
    }

    private MemoryModel.MemoryProgress memoryProgressCallback = new MemoryModel.MemoryProgress() {
        @Override
        public void onSetup(long threshold) {
            Log.d("Memory", "Threshold " + threshold);
            view.getUiHandler().post(view::showSetup);
        }

        @Override
        public void onStart(int limit) {
            Log.d("Memory", "Start ");
            view.getUiHandler().post(() -> {
                view.setMin(0);
                view.setMax(limit);
                view.showStart();
                view.showWritingDialog();
            });
        }

        @Override
        public void onValidation() {
            Log.d("Memory", "Validation");
            view.getUiHandler().post(() -> {
                view.showValidation();
                view.showValidationDialog();
            });
        }

        @Override
        public void onSucceed(String output) {
            Log.d("Memory", "Succeed");
            model.pass(output);
            view.finishActivity();
        }

        @Override
        public void onFail(String s, byte cell) {
            Log.d("Memory", s);
            model.fail(s);
            view.finishActivity();
        }

        @Override
        public void onProgress(float progress, int position) {
            view.getUiHandler().post(() -> {
                view.updateProgressLabel(progress);
                view.updateProgressBar(position);
            });
        }

        @Override
        public void onRemainingTime(int hourOne, int hourTwo, int minOne, int minTwo, int secOne, int secTwo) {
            view.getUiHandler().post(() -> view.showRemainingTime(hourOne, hourTwo, minOne, minTwo, secOne, secTwo));
        }
    };

    @Subscribe
    public void onButtonFailurePressed(MemoryView.OnButtonFailurePressed event) {
        model.cancel();
    }

    public void onStop() {
        model.stop();
    }
}