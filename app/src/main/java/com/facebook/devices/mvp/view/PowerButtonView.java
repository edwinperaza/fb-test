package com.facebook.devices.mvp.view;


import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.ButterKnife;

public class PowerButtonView extends TestViewBase {

    public PowerButtonView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.power_button_intro_message))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new FailTestEvent()))
                        .build()
        );
    }

    public void showTutorialButton(boolean isAnimated) {
        getFloatingTutorialView().showButton(isAnimated);
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.power_button_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    /* Bus classes */
    public static class FailTestEvent {
        /* Nothing to do here */
    }
}
