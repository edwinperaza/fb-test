package com.facebook.devices.mvp.view;

import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoView extends TestViewBase {

    @BindView(R.id.video_surface) SurfaceView surfaceView;
    @BindView(R.id.iv_play) ImageView ivPlay;
    @BindView(R.id.iv_replay) ImageView ivReplay;
    @BindView(R.id.view_layer) View opaqueLayer;
    @BindView(R.id.video_container) View videoContainer;

    private MediaPlayer mediaPlayer;

    private SurfaceHolder.Callback surfaceHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mediaPlayer.setDisplay(holder);
            post(new OnRenderingCompleted());
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {

        }
    };

    public VideoView(TestBaseActivity activity, BusProvider.Bus bus, MediaPlayer mediaPlayer) {
        super(activity, bus);
        this.mediaPlayer = mediaPlayer;

        ButterKnife.bind(this, activity);
    }

    public void setup() {

        /* Media player callbacks */
        mediaPlayer.setOnPreparedListener(l -> post(new OnVideoLoaded()));
        mediaPlayer.setOnCompletionListener(l -> post(new CompletionPlayerEvent()));
        mediaPlayer.setOnVideoSizeChangedListener((mp, width, height) -> post(new VideoSizeChangedEvent(width, height)));

        surfaceView.getHolder().addCallback(surfaceHolderCallback);

        /*Landscape modification*/
        Point screenData = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(screenData);
        if (screenData.x > screenData.y) { /*Landscape*/
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, screenData.y);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            surfaceView.setLayoutParams(layoutParams);
        }
    }

    public void setVideoDataSource(Uri videoUri) throws IOException {
        mediaPlayer.setDataSource(getContext(), videoUri);
        mediaPlayer.prepareAsync();
    }

    public void setVideoDataSource(String videoPath) throws IOException {
        mediaPlayer.setDataSource(videoPath);
        mediaPlayer.prepareAsync();
    }

    public void playVideo() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
    }

    public void seekVideoTo(int msec) {
        mediaPlayer.seekTo(msec);
    }

    public void releaseResources() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        /*Tutorial setup*/
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.video_tutorial_text)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showSurfaceView() {
        opaqueLayer.setVisibility(View.GONE);
    }

    public void showVideoLayout() {
        videoContainer.setVisibility(View.VISIBLE);
    }

    public void showPlayButton() {
        ivPlay.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            surfaceView.setForeground(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.play_extra_layer_color)));
        }
    }

    public void showReplayButton() {
        ivPlay.setVisibility(View.GONE);
        ivReplay.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            surfaceView.setForeground(null);
        }
    }

    public void showInitialDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.video_initial_message))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    public void showFirstQuestion() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.video_first_question_text))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new OnFirstQuestionReply(OnFirstQuestionReply.SUCCESS)))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new OnFirstQuestionReply(OnFirstQuestionReply.FAILED)))
                        .build()
        );
    }

    public void showSecondQuestion() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.video_second_question_text))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new OnSecondQuestionReply(OnSecondQuestionReply.SUCCESS)))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new OnSecondQuestionReply(OnSecondQuestionReply.FAILED)))
                        .build()
        );
    }

    public void hidePlayButton() {
        ivPlay.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            surfaceView.setForeground(null);
        }
    }

    public void hideReplayButton() {
        ivReplay.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            surfaceView.setForeground(null);
        }
    }

    public void hideTutorialButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideDialogTop() {
        getBannerView().hideBanner();
    }

    public SurfaceHolder.Callback getSurfaceHolderCallback() {
        return surfaceHolderCallback;
    }

    @OnClick(R.id.iv_play)
    public void onPlayButtonPressed() {
        post(new OnPlayButtonPressed());
    }

    @OnClick(R.id.iv_replay)
    public void onRePlayButtonPressed() {
        post(new OnReplayButtonPressed());
    }

    public void setSize(int width, int height) {
        surfaceView.getHolder().setFixedSize(width, height);
    }

    public Point getScreenSize() {
        Point screenSize = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(screenSize);
        return screenSize;
    }

    /**
     * Static classes for BUS
     */
    public static class OnPlayButtonPressed {
        /*Nothing to do here*/
    }

    public static class OnReplayButtonPressed {
        /*Nothing to do here*/
    }

    public static class OnButtonFailurePressed {
        /*Nothing to do here*/
    }

    public static class OnRenderingCompleted {
        /*Nothing to do here*/
    }
    public static class OnVideoLoaded {
        /*Nothing to do here*/
    }

    public static class OnFirstQuestionReply {

        @IntDef({SUCCESS, FAILED})
        @interface Options {
        }

        public static final int SUCCESS = 0;
        public static final int FAILED = 1;

        @Options private int optionSelected;

        public OnFirstQuestionReply(@Options int optionSelected) {
            this.optionSelected = optionSelected;
        }

        @Options
        public int getOptionSelected() {
            return optionSelected;
        }
    }

    public static class OnSecondQuestionReply {

        @IntDef({SUCCESS, FAILED})
        @interface Options {
        }

        public static final int SUCCESS = 0;
        public static final int FAILED = 1;

        @Options private int optionSelected;

        public OnSecondQuestionReply(@Options int optionSelected) {
            this.optionSelected = optionSelected;
        }

        @Options
        public int getOptionSelected() {
            return optionSelected;
        }
    }

    public class CompletionPlayerEvent {
    }

    public static class VideoSizeChangedEvent {
        public int width;
        public int height;

        public VideoSizeChangedEvent(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }

}