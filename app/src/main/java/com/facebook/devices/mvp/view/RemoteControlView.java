package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.RemoteControlCustomView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RemoteControlView extends TestViewBase {

    @BindView(R.id.remote_control_custom_view) RemoteControlCustomView remoteControlCustomView;
    @BindView(R.id.tv_description_event) TextView descriptionEvent;
    @BindView(R.id.tv_remote_control_description) TextView descriptionTextView;
    @BindView(R.id.tv_remote_control_title) TextView titleTextView;
    @BindView(R.id.btn_remote_control_fail) Button failBtn;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public RemoteControlView(TestBaseActivity activity, BusProvider.Bus eventBus) {
        super(activity, eventBus);
        ButterKnife.bind(this, activity);
        setListener();
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    public void setListener() {
        remoteControlCustomView.setListener(message -> {
            post(new OnButtonPressed(message));
            descriptionEvent.setText(message);
        });
        failBtn.setOnClickListener(v -> post(new OnFailTestEvent()));
    }

    public void hideTutorialFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showTutorialFloatingButton() {
        getFloatingTutorialView().showButton();
    }

    public void showShouldPairAndConnectMessage() {
        titleTextView.setText(R.string.remote_controller_should_pair_and_connect_title);
        descriptionTextView.setText(R.string.remote_controller_should_pair_and_connect_description);
    }

    public void showShouldConnectMessage() {
        titleTextView.setText(R.string.remote_controller_should_connect_title);
        descriptionTextView.setText(R.string.remote_controller_should_connect_description);
    }

    public void showConnectedAndStartTestMessage() {
        titleTextView.setText(R.string.remote_controller_connected_and_paired_title);
        descriptionTextView.setText(R.string.remote_controller_connected_and_paired_description);
    }

    public void showPressBackButtonMessage() {
        titleTextView.setText(R.string.remote_controller_back_button_title);
        descriptionTextView.setText(R.string.remote_controller_back_button_description);
    }

    public void showPressSearchButtonMessage() {
        titleTextView.setText(R.string.remote_controller_search_button_title);
        descriptionTextView.setText(R.string.remote_controller_search_button_description);
    }

    public void showPressPlayAndPauseButtonMessage() {
        titleTextView.setText(R.string.remote_controller_play_and_pause_button_title);
        descriptionTextView.setText(R.string.remote_controller_play_and_pause_button_description);
    }

    public void showPressMenuButtonMessage() {
        titleTextView.setText(R.string.remote_controller_menu_button_title);
        descriptionTextView.setText(R.string.remote_controller_menu_button_description);
    }

    @Override
    protected void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.remote_controller_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    /**
     * Static classes for BUS
     */
    public static class OnButtonPressed{
        private String buttonText;

        public OnButtonPressed(String buttonText) {
            this.buttonText = buttonText;
        }

        public String getButtonText() {
            return buttonText;
        }
    }

    public static class OnFailTestEvent{}

    public static class OnPassTestEvent{}

}
