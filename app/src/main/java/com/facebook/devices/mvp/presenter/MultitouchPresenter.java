package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.MultitouchModel;
import com.facebook.devices.mvp.view.MultitouchView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_DROPPED;
import static com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_SUCCESS;

public class MultitouchPresenter extends PresenterTestBase<MultitouchModel, MultitouchView> {


    public MultitouchPresenter(MultitouchModel model, MultitouchView view) {
        super(model, view);
        init();
    }

    private void init() {
        view.showDialogOne();
        view.showFloatingTutorialButton();
        view.disableMultitouchComponent();
    }

    @Subscribe
    public void onBannerCVEvent(MultitouchView.InitDialogEvent event) {
        view.hideDialogOne();
        view.hideFloatingButton();
        view.enableMultitouchComponent();
    }

    @Subscribe
    public void onMultitouchReplies(MultitouchView.OnMultitouchReplies event) {
        switch (event.getResultMode()) {
            case FINISHED_SUCCESS:
                model.pass();
                view.finishActivity();
                break;

            case FINISHED_DROPPED:
                view.showDroppedDialog();
                break;
        }
    }

    @Subscribe
    public void onFailureDialogOptionSelected(MultitouchView.OnFailureDialogOptionSelected event) {
        switch (event.getOptionSelected()) {
            case MultitouchView.OnFailureDialogOptionSelected.OPTION_RETRY:
                view.resetMultitouch();
                break;

            case MultitouchView.OnFailureDialogOptionSelected.OPTION_FAIL:
                model.fail();
                view.finishActivity();
                break;
        }
    }
}
