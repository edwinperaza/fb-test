package com.facebook.devices.mvp.model;


import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;

import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class GridTouchPatternModel extends ServiceModel {

    public final long TIMER_MILLIS = 6000;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TWO_TWO, THREE_THREE, FOUR_FOUR, FIVE_FIVE, SIX_SIX, SEVEN_SEVEN, EIGHT_EIGHT, NINE_NINE, TEN_TEN})
    public @interface TouchDetectorSize {
    }

    /*Size modes*/
    public static final int TWO_TWO = 2;
    public static final int THREE_THREE = 3;
    public static final int FOUR_FOUR = 4;
    public static final int FIVE_FIVE = 5;
    public static final int SIX_SIX = 6;
    public static final int SEVEN_SEVEN = 7;
    public static final int EIGHT_EIGHT = 8;
    public static final int NINE_NINE = 9;
    public static final int TEN_TEN = 10;

    public interface TouchPatternSimpleModelCallback {
        void onTimeUp();
    }

    private TouchPatternSimpleModelCallback callBack = () -> {/*Dummy callback*/};
    private boolean timerInitAvailable = true;

    @GridTouchPatternModel.TouchDetectorSize private int touchSize;

    private Handler uiHandler;
    private Runnable timeUpRunnable = () -> {
        callBack.onTimeUp();
        timerInitAvailable = true;
    };

    public GridTouchPatternModel(@TouchPatternModel.TouchDetectorSize int touchSize, Handler uiHandler) {
        this.touchSize = touchSize;
        this.uiHandler = uiHandler;
    }

    @TouchPatternModel.TouchDetectorSize
    public int getTouchSize() {
        return touchSize;
    }

    public void setCallback(TouchPatternSimpleModelCallback callBack) {
        this.callBack = callBack;
    }

    public void startTimer() {
        if (timerInitAvailable) {
            timerInitAvailable = false;
            uiHandler.postDelayed(timeUpRunnable, TIMER_MILLIS);
        }
    }

    public void cancelTimer() {
        timerInitAvailable = true;
        uiHandler.removeCallbacks(timeUpRunnable);
    }

    public boolean isTimerInitAvailable() {
        return timerInitAvailable;
    }
}
