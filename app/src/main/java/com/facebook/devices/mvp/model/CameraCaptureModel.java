package com.facebook.devices.mvp.model;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.devices.utils.Size;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.devices.utils.customcomponents.CancellableHandler;
import com.facebook.hwtp.mvp.model.ServiceModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CameraCaptureModel extends ServiceModel {

    public static final String IMAGE_FILE_PATH = "ImageFilePath";
    public static final String CAMERA_ID_RECEIVED = "CameraID";

    /*Fixed resolution*/
    private static final int DEFAULT_FIX_RES_WIDTH = 1600;
    private static final int DEFAULT_FIX_RES_HEIGHT = 1200;

    private static final int NO_ROTATION = -1;
    private static final float THRESHOLD = 1.03f; //adding 3% of acceptable range

    private final String PHOTO_EXTENSION = ".jpeg";
    private final String PHOTO_DEFAULT_NAME = "CameraTestPhoto" + PHOTO_EXTENSION;

    private boolean stopCameraBroadcastSent = false;

    private ResourcesManager resourcesManager = new ResourcesManager();

    private static final long WAIT_TIME_MILLIS = 3000;

    /* Camera orientations */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray() {{
        append(Surface.ROTATION_0, 90);
        append(Surface.ROTATION_90, 0);
        append(Surface.ROTATION_180, 270);
        append(Surface.ROTATION_270, 180);
    }};

    /* Ratios */
    private static final float RATIO_16_9 = (float) 16 / 9;
    private static final float RATIO_4_3 = (float) 4 / 3;
    private static final float RATIO_3_2 = (float) 3 / 2;
    private static final float RATIO_21_9 = (float) 21 / 9;

    /* Camera Data Objects */
    private CameraCaptureSession cameraCaptureSession;
    private CameraDevice cameraDevice;
    private Size cameraPreviewSize;

    /* Running task in the background stuffs */
    private HandlerThread backgroundThread;
    private Handler backgroundHandler;

    /* Handles still image capture */
    private ImageReader imageReader;

    /* Builder for the camera preview */
    private CaptureRequest.Builder previewRequestBuilder;
    private int sensorOrientation;

    private boolean photoInProcess = false;
    private CameraManager cameraManager;
    private Point screenSize;
    private BusProvider.Bus bus;
    private CameraUtils cameraUtils;
    private String photoAbsoluteFilePath;
    private RuntimePermissionHandler permissionHandler;
    private boolean isMirrored;
    private boolean isFlowActive = true;
    private CancellableHandler uiHandler;


    /*Camera flow callbacks */
    private boolean isCameraAvailabilityRegistered;

    private CameraManager.AvailabilityCallback availabilityCallback = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            try {
                if (isCameraTarget(cameraId) && !getResourcesManager().isCameraResourceReady()) {
                    getResourcesManager().setCameraResourceReady(cameraId);
                    post(new OnCameraTargetAvailable());
                }
            } catch (CameraAccessException e) {
                Log.e("CameraError", e.getMessage());
            }
        }

        @Override
        public void onCameraUnavailable(@NonNull String cameraId) {
            /* Nothing to do here for now */
        }
    };

    private CameraCaptureModel.ImageProcessorAsyncTask.ImageProcessorListener imageProcessorListener = (bitmap) -> post(new OnImageAvailableEvent(bitmap));

    /* Image available listener */
    private final ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            backgroundHandler.post(
                    new SaveImageRunnable(reader.acquireLatestImage(),
                            generateFile(),
                            (file) -> {
                                setActualPhotoFile(file.getAbsolutePath());
                                getImageProcessorAsyncTask(imageProcessorListener)
                                        .execute(file.getAbsolutePath());
                            }));
        }
    };

    public CameraCaptureModel(CameraManager cameraManager, Point screenSize, BusProvider.Bus bus, RuntimePermissionHandler permissionHandler,
                              CameraUtils cameraUtils, CancellableHandler uiHandler, String photoFilePath, boolean isMirrored) {
        this.cameraManager = cameraManager;
        this.screenSize = screenSize;
        this.bus = bus;
        this.permissionHandler = permissionHandler;
        this.cameraUtils = cameraUtils;
        this.uiHandler = uiHandler;
        this.photoAbsoluteFilePath = photoFilePath;
        this.isMirrored = isMirrored;
    }

    /**
     * Indicates if the lifecycle is still in an "active" state and not in a "closing" state
     */
    public void setFlowActive(boolean isFlowActive) {
        this.isFlowActive = isFlowActive;
    }

    public boolean isFlowActive() {
        return isFlowActive;
    }

    public CancellableHandler getUiHandler() {
        return uiHandler;
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    public boolean isMirrored() {
        return isMirrored;
    }

    public ResourcesManager getResourcesManager() {
        return resourcesManager;
    }

    public String getPhotoAbsoluteFilePath() {
        return photoAbsoluteFilePath;
    }

    public void setActualPhotoFile(@NonNull String photoFilePath) {
        this.photoAbsoluteFilePath = photoFilePath;
    }

    public boolean wasImageTaken() {
        return photoAbsoluteFilePath != null && photoAbsoluteFilePath.length() > 0;
    }

    public long getWaitTimeMillis() {
        return WAIT_TIME_MILLIS;
    }

    private boolean isCameraTarget(String cameraId) throws CameraAccessException {
        Integer facing = cameraUtils.getCameraLensFacing(getCameraManager(), cameraId);
        return facing != null && facing == CameraCharacteristics.LENS_FACING_BACK;
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    private void unregisterAvailabilityCallback() {
        getCameraManager().unregisterAvailabilityCallback(availabilityCallback);
        isCameraAvailabilityRegistered = false;
    }

    private void registerAvailabilityCallback() {
        getCameraManager().registerAvailabilityCallback(availabilityCallback, null);
        isCameraAvailabilityRegistered = true;
    }

    public boolean isCameraAvailabilityRegistered() {
        return isCameraAvailabilityRegistered;
    }

    @SuppressLint("MissingPermission")
    public void openCamera(TextureView textureView) {

        /* Camera Availability listener */
        if (!isCameraAvailabilityRegistered) {
            registerAvailabilityCallback();
        }

        /* Send unlock camera broadcast */
        if (!getResourcesManager().isCameraResourceReady()) {
            //sendCameraUnlockRequest();
        }

        /* Thread state creation if needed */
        if (backgroundThread == null && backgroundHandler == null) {
            backgroundThread = new HandlerThread("Camera BackgroundThread");
            backgroundThread.start();
            backgroundHandler = new Handler(backgroundThread.getLooper());
        }

        /* Check resources availability */
        if (!getResourcesManager().allResourcesReady()) {
            /* Resources not available yet */
            return;
        }


        /* All resource ready - Open camera */
        getResourcesManager().setCameraInControl();
        try {
            /*Basic configuration*/
            String cameraId = getResourcesManager().getCameraId();
            Size previewRatio = setupCameraOutputs(textureView, cameraId);

            /*Camera opening*/
            getCameraManager().openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice camera) {
                    cameraDevice = camera;
                    createCameraPreview(camera, textureView);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice camera) {
                    camera.close();
                    cameraDevice = null;
                }

                @Override
                public void onError(@NonNull CameraDevice camera, int error) {
                    camera.close();
                    cameraDevice = null;
                    post(new OnCameraError("CameraDevice.StateCallback - onError: " + String.valueOf(error)));
                }
            }, null);

            post(new CameraOutputSetEvent(previewRatio));
        } catch (Throwable e) {
            post(new OnCameraError("OpenCameraException: " + e.getMessage()));
        }
    }

    private Size setupCameraOutputs(TextureView textureView, String cameraId) throws CameraAccessException {
        /* Check stream config map */
        StreamConfigurationMap map = cameraUtils.getCameraStreamConfigurationMap(getCameraManager(), cameraId);
        if (map != null) {

            sensorOrientation = cameraUtils.getSensorOrientation(getCameraManager(), cameraId);

//            Size largest = model.getLargesSize(GenericUtils.arrayToSizeArray(map.getOutputSizes(ImageFormat.JPEG))); - NOT REMOVE PLEASE -
            Size largest = getDefaultFixedResolution();
            imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 1);
            imageReader.setOnImageAvailableListener(onImageAvailableListener, backgroundHandler);

            Size previewRatio = getRatio(largest);

            cameraPreviewSize = chooseOptimalPreviewResolution(GenericUtils.arrayToSizeArray(map.getOutputSizes(ImageFormat.JPEG)),
                    new Size(textureView.getWidth(), textureView.getHeight()), previewRatio);

            return previewRatio;
        }
        return null;
    }

    private void createCameraPreview(CameraDevice camera, TextureView textureView) {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;

            texture.setDefaultBufferSize(cameraPreviewSize.getWidth(), cameraPreviewSize.getHeight());
            Surface surface = new Surface(texture);

            previewRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(surface);

            camera.createCaptureSession(Arrays.asList(surface, imageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    cameraCaptureSession = session;
                    try {
                        /*Update preview*/
                        if (cameraDevice != null) {
                            previewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
                            cameraCaptureSession.setRepeatingRequest(previewRequestBuilder.build(), null, backgroundHandler);
                            post(new OnCameraPreviewSetEvent());
                        }
                    } catch (Throwable e) {
                        post(new OnCameraError("CreateCaptureSession -  onConfigured: " + e.getMessage()));
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    post(new OnCameraError("CreateCaptureSession -  onConfigureFailed: " + session.toString()));
                }
            }, null);
        } catch (Throwable e) {
            post(new OnCameraError("CreateCameraPreview - exception: " + e.getMessage()));
        }
    }

    public boolean isStopCameraBroadcastSent() {
        return stopCameraBroadcastSent;
    }

    public void sendCameraUnlockRequest() {
        cameraUtils.sendCameraUnlockRequest();
        stopCameraBroadcastSent = true;
    }

    public void sendCameraLockRequest() {
        cameraUtils.sendCameraLockRequest();
    }

    /**
     * Must be called when internal process of camera recorded needs to be
     * paused due they are no longer necessary
     * <p>
     * {@link #openCamera(TextureView)} must be called if you want to operate it again after this
     * method is called
     */
    public void pauseCamera() {
        if (backgroundThread != null) {
            backgroundThread.quitSafely();
            try {
                backgroundThread.join();
                backgroundThread = null;
                backgroundHandler = null;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*Clear capture session*/
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        /*Clear camera device*/
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }

        /*Clear image reader*/
        if (imageReader != null) {
            imageReader.close();
            imageReader = null;
        }
    }

    /**
     * @param releaseAllResources true only if camera should be locked again and the activity
     *                            will be destroyed
     */
    public void stopCamera(boolean releaseAllResources) {
        /*Unregister availability callback*/
        unregisterAvailabilityCallback();
        /*Clear resources*/
        getResourcesManager().setAllResourcesUnavailable();

        if (releaseAllResources) { /* Activity in on destroying flow */
            removeAllData();
            //sendCameraLockRequest();
        }
    }

    public void captureImage() {
        if (cameraDevice != null && !photoInProcess) {
            try {

                photoInProcess = true;

                CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                captureBuilder.addTarget(imageReader.getSurface());
                captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

                int rotation = 0;/*Always 0 due device never rotates it's camera with the screen*/
                captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientationRotationBased(sensorOrientation, rotation));

                /*Callbacks*/
                CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
                    @Override
                    public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                        super.onCaptureCompleted(session, request, result);
                        photoInProcess = false;
                    }
                };

                if (cameraCaptureSession != null) {
                    cameraCaptureSession.stopRepeating();
                    cameraCaptureSession.abortCaptures();
                    cameraCaptureSession.capture(captureBuilder.build(), captureCallback, null);
                }

            } catch (Throwable ex) {
                post(new OnCameraError("CaptureImage - exception: " + ex.getMessage()));
            }
        }
    }

    private File getPhotoParentFilePath() {
        return Environment.getExternalStorageDirectory();
    }

    private void removeAllData() {
        File file = new File(getPhotoParentFilePath(), PHOTO_DEFAULT_NAME);
        Log.i("Remove:", "File " + file.getName() + " removed successfully -> " + file.delete());
    }

    public File generateFile() {
        return new File(getPhotoParentFilePath(), PHOTO_DEFAULT_NAME);
    }

    public Bitmap getImageRotated(String filePath, boolean isMirrored) {
        try {
            int rotate = getImageOrientation(new File(filePath));

            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            Bitmap original = BitmapFactory.decodeFile(filePath, new BitmapFactory.Options());

            /*Mirroring*/
            if (isMirrored) {
                matrix.postScale(-1, 1, original.getWidth() / 2, original.getHeight() / 2);
            }

            /* Rotate and scaled the bitmap based on the screen size and bitmap orientation */
            Size scaledSize = getScaledSizesScreenBased(original.getWidth(), original.getHeight(), rotate);

            Bitmap bitmapRescaled = Bitmap.createScaledBitmap(original, scaledSize.getWidth(), scaledSize.getHeight(), true);
            return Bitmap.createBitmap(bitmapRescaled, 0, 0, bitmapRescaled.getWidth(), bitmapRescaled.getHeight(), matrix, true);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Size getScaledSizesScreenBased(int originalImgWidth, int originalImgHeight, int rotate) {

        boolean needScale = (originalImgWidth > screenSize.x || originalImgHeight > screenSize.y);
        int scaledWidth = originalImgWidth;
        int scaledHeight = originalImgHeight;

        if (needScale) {
            /*Original Data*/
            int originalOrientation = (originalImgWidth > originalImgHeight) ? Configuration.ORIENTATION_LANDSCAPE : Configuration.ORIENTATION_PORTRAIT;

            /*Fixed values based on the photo rotation needs*/
            int fixedX = ((rotate != NO_ROTATION) && (rotate == 90 || rotate == 270)) ? screenSize.y : screenSize.x;
            int fixedY = ((rotate != NO_ROTATION) && (rotate == 90 || rotate == 270)) ? screenSize.x : screenSize.y;

            if (originalOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                //Landscape
                /*Set new width*/
                scaledWidth = fixedX;
                /*Get difference percentage*/
                float diffPerc = (float) (originalImgWidth - fixedX) / originalImgWidth;
                /*Set relation*/
                scaledHeight = (int) ((originalImgHeight) - (originalImgHeight * diffPerc));
            } else {
                //Portrait
                /*Get difference percentage*/
                float diffPerc = (float) (originalImgHeight - fixedY) / originalImgHeight;
                /*Set relation*/
                scaledHeight = (int) ((originalImgWidth) - (originalImgWidth * diffPerc));
            }

            return new Size(scaledWidth, scaledHeight);

        } else {
            return new Size(originalImgWidth, originalImgHeight);
        }
    }

    private int getImageOrientation(File file) throws IOException {
        if (file.exists()) {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270;
                default: /** Normal orientation*/
                    return 0;
            }
        } else {
            return 0;
        }
    }

    public Size getRatio(Size resolution) {

        float ratio = (float) resolution.getWidth() / resolution.getHeight();

        if (ratio == RATIO_16_9) {
            return new Size(16, 9);
        }

        if (ratio == RATIO_4_3) {
            return new Size(4, 3);
        }

        if (ratio == RATIO_3_2) {
            return new Size(3, 2);
        }

        if (ratio == RATIO_21_9) {
            return new Size(21, 9);
        }

        return new Size(16, 9); //Default value 16:9
    }

    public Size chooseOptimalPreviewResolution(Size[] choices, Size viewSize, Size aspectRatio) {

        List<Size> resInRatio = new ArrayList<>();
        double ratio = (double) aspectRatio.getWidth() / aspectRatio.getHeight();

        int areaView = (int) ((viewSize.getWidth() * viewSize.getHeight()) * THRESHOLD);
        int areaDifference = Integer.MAX_VALUE;
        Size choice = null;

        for (Size option : choices) {
            /*Check for correct ratio*/
            if (ratio == (double) option.getWidth() / option.getHeight()) {
                resInRatio.add(option);

                int optionArea = option.getWidth() * option.getHeight();
                int optionAreaDifference = areaView - optionArea;
                if (areaView >= optionArea && areaDifference > optionAreaDifference) {
                    areaDifference = optionAreaDifference;
                    choice = option;
                }
            }
        }

        return (choice != null) ? choice : Collections.min(resInRatio, new CompareSizesByArea());
    }

    /* Compares two sizes based on their areas */
    private class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    public Size getLargesSize(Size[] sizes) {
        return Collections.max(Arrays.asList(sizes), new CompareSizesByArea());
    }

    /**
     * Only use this method for specific needs, otherwise use {@link CameraCaptureModel#getLargesSize(Size[])}
     * in combination with {@link CameraCaptureModel#getRatio(Size)} and {@link CameraCaptureModel#chooseOptimalPreviewResolution(Size[], Size, Size)} to get the perfect fit
     *
     * @return the pre-set size according to specifications
     */
    public Size getDefaultFixedResolution() {
        return new Size(DEFAULT_FIX_RES_WIDTH, DEFAULT_FIX_RES_HEIGHT);
    }

    /* Retrieves the orientation from the specified screen rotation */
    public int getOrientationRotationBased(int sensorOrientation, int rotation) {
        /*For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
         For devices with orientation of 270, we need to rotate the JPEG 180 degrees.*/
        return (ORIENTATIONS.get(rotation) + sensorOrientation + 270) % 360;
    }

    /* Image processor */
    public ImageProcessorAsyncTask getImageProcessorAsyncTask(ImageProcessorAsyncTask.ImageProcessorListener listener) {
        return new ImageProcessorAsyncTask(listener, this);
    }

    private static class SaveImageRunnable implements Runnable {

        public interface SaveImageRunnableListener {
            void onSaveCompleted(File file);
        }

        private Image image;
        private File file;
        private WeakReference<SaveImageRunnableListener> listener;

        public SaveImageRunnable(Image image,
                                 File file,
                                 SaveImageRunnableListener listener) {
            this.image = image;
            this.file = file;
            this.listener = new WeakReference<>(listener);
        }

        @Override
        public void run() {
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            try (FileOutputStream output = new FileOutputStream(file)) {
                output.write(bytes);
            } catch (IOException e) {
                file.delete();
            } finally {
                image.close();
                if (listener.get() != null) {
                    listener.get().onSaveCompleted(file);
                }
            }
        }
    }

    public static class ImageProcessorAsyncTask extends AsyncTask<String, Void, Bitmap> {

        public interface ImageProcessorListener {
            void onPostExecute(Bitmap bitmap);
        }

        private WeakReference<ImageProcessorListener> listener;
        private WeakReference<CameraCaptureModel> model;

        public ImageProcessorAsyncTask(ImageProcessorListener listener, CameraCaptureModel model) {
            this.listener = new WeakReference<>(listener);
            this.model = new WeakReference<>(model);
        }

        @Override
        protected Bitmap doInBackground(String... imagesPath) {
            String imagePath = imagesPath[0];
            return (model.get() != null) ? model.get().getImageRotated(imagePath, model.get().isMirrored()) : null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (listener.get() != null && model.get() != null && model.get().isFlowActive()) {
                listener.get().onPostExecute(bitmap);
            }
        }
    }


    public class ResourcesManager {

        private String cameraId = null;
        private boolean surfaceTextureReady = false;
        private boolean cameraInControl = false;

        public ResourcesManager() {
        }

        public void setCameraResourceReady(String cameraID) {
            this.cameraId = cameraID;
        }

        public void setCameraResourceUnavailable() {
            cameraId = null;
        }

        public void setSurfaceTextureReady() {
            surfaceTextureReady = true;
        }

        public void setSurfaceTextureUnavailable() {
            surfaceTextureReady = false;
        }

        public boolean allResourcesReady() {
            return cameraId != null && surfaceTextureReady && !cameraInControl;
        }

        public String getCameraId() {
            return cameraId;
        }

        public boolean isCameraResourceReady() {
            return cameraId != null;
        }

        public void setCameraInControl() {
            cameraInControl = true;
        }

        public void setCameraReleased() {
            cameraInControl = false;
        }

        public boolean isCameraInControl() {
            return cameraInControl;
        }

        public void setAllResourcesUnavailable() {
            setCameraResourceUnavailable();
            setCameraReleased();
        }
    }

    public void post(Object object) {
        bus.post(object);
    }

    /**
     * Static classes for BUS
     */
    public static class OnCameraTargetAvailable {
        /*Nothing to do here*/
    }

    public static class CameraOutputSetEvent {

        private Size previewRatio;

        public CameraOutputSetEvent(Size previewRatio) {
            this.previewRatio = previewRatio;
        }

        public Size getPreviewRatio() {
            return previewRatio;
        }
    }

    public static class OnCameraError {
        private String message;

        public OnCameraError(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class OnCameraPreviewSetEvent {
        /*Nothing to add here*/
    }

    public static class OnImageAvailableEvent {

        private Bitmap capture;

        public OnImageAvailableEvent(Bitmap capture) {
            this.capture = capture;
        }

        public Bitmap getImageCaptured() {
            return capture;
        }
    }
}
