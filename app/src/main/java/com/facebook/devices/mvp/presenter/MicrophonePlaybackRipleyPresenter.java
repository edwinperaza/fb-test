package com.facebook.devices.mvp.presenter;

import android.os.Bundle;

import com.facebook.devices.mvp.model.MicrophonePlaybackRipleyModel;
import com.facebook.devices.mvp.view.MicrophonePlaybackRipleyView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioPlayer;
import com.facebook.devices.utils.AudioRecorder;

import org.greenrobot.eventbus.Subscribe;

public class MicrophonePlaybackRipleyPresenter extends PresenterTestBase<MicrophonePlaybackRipleyModel, MicrophonePlaybackRipleyView> {

    private AudioRecorder.AudioRecorderListener audioRecorderListener = new AudioRecorder.AudioRecorderListener() {
        @Override
        public void onAvailableData(short[] data) {
            view.getUIHandler().post(() -> view.drawAudioSample(data));
        }

        @Override
        public void onRecordCompleted(short[] data) {
            model.setRecordedBuffer(data);
            model.startPlayback(audioPlayerListener, model.getRecordedBuffer());

            view.getUIHandler().post(() -> {
                view.showPlaybackDialog();
                view.showPlayingCircle();
            });
        }
    };

    private AudioPlayer.AudioPlayerListener audioPlayerListener = new AudioPlayer.AudioPlayerListener() {
        @Override
        public void onPlayingDataAvailable(short[] data) {
            view.getUIHandler().post(() -> view.drawAudioSample(data));
        }

        @Override
        public void onPlaybackCompleted() {
            view.getUIHandler().post(view::showRepeatPlaybackCircle);
        }
    };

    public MicrophonePlaybackRipleyPresenter(MicrophonePlaybackRipleyModel model, MicrophonePlaybackRipleyView view) {
        super(model, view);
    }

    public void onSaveInstanceState(Bundle outState) {
        if (model.getActualState() == MicrophonePlaybackRipleyModel.STATE_PLAYBACK) {
            outState.putShortArray(MicrophonePlaybackRipleyModel.RECORDED_BUFFER, model.getRecordedBuffer());
        }
    }

    public void onResumed() {
        if (model.isPermissionGranted()) {
            view.showFloatingTutorialButton();
            switch (model.getActualState()) {
                case MicrophonePlaybackRipleyModel.STATE_RECORDING:
                    view.showRecordingDialog();
                    view.showRecordingCircle();
                    model.startRecording(audioRecorderListener);
                    break;

                case MicrophonePlaybackRipleyModel.STATE_PLAYBACK:
                    view.showPlaybackDialog();
                    view.showPlayingCircle();
                    model.startPlayback(audioPlayerListener, model.getRecordedBuffer());
                    break;
            }
        } else {
            model.requestPermissions();
        }
    }

    public void onPaused() {
        switch (model.getActualState()) {
            case MicrophonePlaybackRipleyModel.STATE_RECORDING:
                model.stopRecording();
                break;

            case MicrophonePlaybackRipleyModel.STATE_PLAYBACK:
                model.stopPlayback();
                break;
        }
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onButtonRecordYesPressed(MicrophonePlaybackRipleyView.RecordPassEvent event) {
        model.stopRecording();
    }

    @Subscribe
    public void onButtonRecordFailedPressed(MicrophonePlaybackRipleyView.RecordFailEvent event) {
        model.stopRecording();
        fail();
    }

    @Subscribe
    public void onButtonPlaybackYesPressed(MicrophonePlaybackRipleyView.PlaybackPassEvent event) {
        model.stopPlayback();
        pass();
    }

    @Subscribe
    public void onButtonPlaybackFailedPressed(MicrophonePlaybackRipleyView.PlaybackFailEvent event) {
        model.stopPlayback();
        fail();
    }

    @Subscribe
    public void onRepeatEvent(MicrophonePlaybackRipleyView.RepeatEvent event) {
        view.showPlayingCircle();
        model.startPlayback(audioPlayerListener, model.getRecordedBuffer());
    }

    private void pass(String... message) {
        model.pass(message);
        view.finishActivity();
    }

    private void fail(String... message) {
        model.fail(message);
        view.finishActivity();
    }
}
