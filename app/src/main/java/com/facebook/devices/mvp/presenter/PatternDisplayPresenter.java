package com.facebook.devices.mvp.presenter;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.PatternDisplayModel;
import com.facebook.devices.mvp.view.PatternDisplayView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.mvp.model.PatternDisplayModel.QUESTION_ONE;
import static com.facebook.devices.mvp.model.PatternDisplayModel.QUESTION_TWO;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEGATIVE;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEUTRAL;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_POSITIVE;

public class PatternDisplayPresenter extends PresenterTestBase<PatternDisplayModel, PatternDisplayView> {

    private Runnable finalDialogRunnable = () -> {
        switch (model.getState()) {
            case QUESTION_ONE:
                view.showTutorialFloatingButton();
                view.showFirstQuestion();
                break;
            case QUESTION_TWO:
                view.showTutorialFloatingButton();
                view.showSecondQuestion();
                break;
        }
    };

    public PatternDisplayPresenter(PatternDisplayModel model, PatternDisplayView view) {
        super(model, view);
        view.setPatternSize(model.getSquaresNumber());
    }

    public void onResume() {
        if (model.shouldShowTutorial()) {
            view.showTutorialOnBannerTop();
        } else {
            startTimer();
        }
    }

    private void startTimer() {
        view.getUiHandler().postDelayed(finalDialogRunnable, BuildConfig.WAITING_TIME_COLOR_MILLIS);
    }

    public void onPause() {
        view.getUiHandler().removeCallbacks(finalDialogRunnable);
    }

    @Subscribe
    public void onQuestionOneEvent(PatternDisplayView.QuestionOneEvent event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                if (model.shouldShowMuraQuestion()) {
                    model.nextState();
                    view.showSecondQuestion();
                    break;
                } else {
                    model.pass();
                    view.finishActivity();
                    break;
                }
            case OPTION_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;
            case OPTION_NEUTRAL:
                startTimer();
                view.hideBannerTop();
                view.hideTutorialFloatingButton();
                break;
        }
    }

    @Subscribe
    public void onQuestionTwoEvent(PatternDisplayView.QuestionTwoEvent event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                model.pass();
                view.finishActivity();
                break;
            case OPTION_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;
            case OPTION_NEUTRAL:
                startTimer();
                view.hideBannerTop();
                view.hideTutorialFloatingButton();
                break;
        }
    }

    @Subscribe
    public void onTutorialTopBannerEvent(PatternDisplayView.OnTutorialTopBannerEvent event) {
        view.hideBannerTop();
        startTimer();
    }
}