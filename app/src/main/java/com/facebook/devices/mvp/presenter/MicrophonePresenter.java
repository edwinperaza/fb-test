package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.model.MicrophoneModel;
import com.facebook.devices.mvp.view.MicrophoneView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;

import org.greenrobot.eventbus.Subscribe;

public class MicrophonePresenter extends PresenterTestBase<MicrophoneModel, MicrophoneView> {

    private AudioRecorder.AudioRecorderListener recorderListener = new AudioRecorder.AudioRecorderListener() {
        @Override
        public void onAvailableData(short[] data) {
            view.getUiHandler().post(() -> view.drawAudioSample(data));
        }

        @Override
        public void onRecordCompleted(short[] data) {
            /*Nothing to do here*/
        }
    };

    public MicrophonePresenter(MicrophoneModel model, MicrophoneView view) {
        super(model, view);
        view.showFloatingTutorialButton(true);
    }

    public void onResumed() {
        if (model.isPermissionGranted()) {
            startRecording(recorderListener);
        } else {
            model.requestPermissions();
        }
    }

    public void onPaused() {
        model.stopRecording();
    }

    private void startRecording(AudioRecorder.AudioRecorderListener listener) {
        model.startRecording(listener);
        view.showInitialDialog();
        view.setStateMicroRecording(true);
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onButtonSucceededPressed(MicrophoneView.ButtonSucceededClicked event) {
        model.stopRecording();
        model.pass();
        view.finishActivity();
    }

    @Subscribe
    public void onButtonFailedPressed(MicrophoneView.ButtonFailedPressedEvent event) {
        model.stopRecording();
        model.fail();
        view.finishActivity();
    }
}
