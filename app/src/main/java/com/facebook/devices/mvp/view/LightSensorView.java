package com.facebook.devices.mvp.view;

import android.annotation.SuppressLint;
import android.support.constraint.ConstraintLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LightSensorView extends TestViewBase {

    @BindView(R.id.base_layout) ConstraintLayout container;
    @BindView(R.id.tv_sensor_value) TextView luxValue;
    @BindView(R.id.light_sensor_seek_bar) SeekBar seekBarLuxValue;

    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    @SuppressLint("ClickableViewAccessibility")
    public LightSensorView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        seekBarLuxValue.setOnTouchListener((v, event) -> true);
    }

    public void showLuxValue(float value) {
        luxValue.setText(decimalFormat.format(value));
        seekBarLuxValue.setProgress((int) value);
    }

    public void showInitDialog() {

        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.sensor_init_dialog_title))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new OnButtonFailurePressed()))
                        .build()
        );
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();

        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.sensor_tutorial_dialog_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    /**
     * Static classes for BUS
     */
    public static class OnButtonFailurePressed {/*Nothing to do here*/
    }
}