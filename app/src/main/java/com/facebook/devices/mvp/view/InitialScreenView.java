package com.facebook.devices.mvp.view;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.hwtp.mvp.view.ViewBase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InitialScreenView extends ViewBase<Activity> {

    @BindView(R.id.tv_device_name) TextView tvDeviceName;
    @BindView(R.id.tv_serial_number) TextView tvSerialNumber;
    @BindView(R.id.tv_apk_name) TextView tvApkName;
    @BindView(R.id.tv_version_number) TextView tvVersionNumber;
    @BindView(R.id.tv_device_status) TextView tvDeviceStatus;
    @BindView(R.id.download_resource_container) View downloadResourceContainer;
    @BindView(R.id.progress_label) TextView progressLabel;

    public InitialScreenView(Activity activity) {
        super(activity);
        ButterKnife.bind(this, activity);
    }

    public void setDeviceName(String deviceName) {
        tvDeviceName.setText(deviceName);
    }

    public void setSerialNumber(String serialNumber) {
        tvSerialNumber.setText(serialNumber);
        tvSerialNumber.setVisibility(View.VISIBLE);
    }

    public void setApkName(String apkName) {
        tvApkName.setText(apkName);
    }

    public void setVersionNumber(String versionNumber) {
        tvVersionNumber.setText(versionNumber);
    }

    public void showConnected() {
        tvDeviceStatus.setText(getContext().getString(R.string.connected));
    }

    public void showDisconnected() {
        tvDeviceStatus.setText(getContext().getString(R.string.disconnected));
    }

    public void showDownloading() {
        progressLabel.setText(R.string.downloading_resources);
        downloadResourceContainer.setVisibility(View.VISIBLE);
    }

    public void hideDownloading() {
        downloadResourceContainer.setVisibility(View.GONE);
    }

    public void showTransferring() {
        progressLabel.setText(R.string.transferring_resources);
        downloadResourceContainer.setVisibility(View.VISIBLE);
    }

    public void hideTransferring() {
        downloadResourceContainer.setVisibility(View.GONE);
    }
}
