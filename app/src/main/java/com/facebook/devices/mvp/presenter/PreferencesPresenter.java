package com.facebook.devices.mvp.presenter;

import android.os.Handler;
import android.os.Looper;

import com.facebook.devices.HwtpApplication;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.mvp.model.PreferencesModel;
import com.facebook.devices.mvp.view.PreferencesView;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class PreferencesPresenter {

    private PreferencesModel model;
    private PreferencesView view;
    private Executor databaseIO = HwtpApplication.getDatabaseIO();

    public PreferencesPresenter(PreferencesModel model, PreferencesView view) {
        this.model = model;
        this.view = view;

        databaseIO.execute(() -> {
            List<ArgumentEntity> argumentEntityList = model.getAllArguments();
            Map<String, TestInfoEntity> testDefinitions = model.getAllTestInfo();
            List<ColorEntity> colorEntityList = model.getColors();

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> view.updateRecyclerItems(argumentEntityList, testDefinitions, colorEntityList));
        });
    }

    @Subscribe
    public void onArgumentValueChanged(PreferencesView.ItemClickListenerEvent event) {
        databaseIO.execute(() -> model.updateArgument(event.argumentEntity));
    }
}
