package com.facebook.devices.mvp.model;

import android.app.Activity;
import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.hwtp.mvp.model.ServiceModel;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

public class AugmentedRealityModel extends ServiceModel {

    private static final String OBJECTIVE_DB_NAME = "objective";

    private Activity activity;
    private PermissionHandler permissionHandler;

    private String modelFileName;
    private String objectiveImgPath;
    private CameraUtils cameraUtils;

    public AugmentedRealityModel(Activity activity, PermissionHandler permissionHandler,
                                 String modelFileName, String objectiveImgPath, CameraUtils cameraUtils) {
        this.permissionHandler = permissionHandler;
        this.activity = activity;
        this.modelFileName = modelFileName;
        this.objectiveImgPath = objectiveImgPath;
        this.cameraUtils = cameraUtils;
    }

    public String getObjectiveDbName() {
        return OBJECTIVE_DB_NAME;
    }

    public String getModelFileName() {
        return modelFileName;
    }

    public String getObjectiveImgPath() {
        return objectiveImgPath;
    }

    public boolean checkArCoreInstallation() throws UnavailableUserDeclinedInstallationException, UnavailableDeviceNotCompatibleException {
        switch (ArCoreApk.getInstance().requestInstall(activity, true)) {
            case INSTALL_REQUESTED:
                return false;

            case INSTALLED:
                return true;
        }
        return false;
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public CameraManager getCameraManager() {
        return (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
    }

    /*Check for specific camera*/
    public boolean isCameraTarget(String cameraId) {
        try {
            Integer facing = getCameraManager().getCameraCharacteristics(cameraId).get(CameraCharacteristics.LENS_FACING);
            return facing != null && facing == CameraCharacteristics.LENS_FACING_BACK;
        } catch (CameraAccessException e) {
            return false;
        }
    }

    public void sendCameraUnlockRequest() {
        cameraUtils.sendCameraUnlockRequest();
    }

    public void sendCameraLockRequest() {
        cameraUtils.sendCameraLockRequest();
    }
}
