package com.facebook.devices.mvp.view;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.augmentedreality.rendering.AugmentedImageNode;
import com.facebook.devices.utils.customviews.BannerEventBase;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.sceneform.ArSceneView;

import java.io.IOException;
import java.lang.ref.SoftReference;

public class AugmentedRealityView extends TestViewBase {

    private ArSceneView arSceneView;
    private Session session;
    private AugmentedImageNode modelBaseNode;

    private Handler uiConfigHandler = new Handler(Looper.getMainLooper(), msg -> {
        switch (msg.arg1) {
            case ArConfigThread.ID:
                switch (msg.arg2) {
                    case ArConfigThread.SUCCESS:
                        session.configure(((ArConfigThread.ProcessedResult) msg.obj).config);
                        arSceneView.setupSession(session);
                        post(new SessionCreatedAndConfigured());
                        break;

                    case ArConfigThread.FAILURE:
                        post(new SessionCreationOrConfiguredErrorEvent((String) msg.obj));
                        break;
                }
                break;
        }
        return false;
    });

    public AugmentedRealityView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        arSceneView = activity.findViewById(R.id.surfaceview);
    }

    public void showIntroBanner() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.augmented_reality_intro_message))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new IntroEvent(BannerEventBase.OPTION_ONE)))
                        .build()
        );
    }

    public void showQuestionBanner() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.augmented_reality_question_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new QuestionEvent(BannerEventBase.OPTION_ONE)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new QuestionEvent(BannerEventBase.OPTION_TWO)))
                        .setCollapsingEnabled(true)
                        .build()
        );
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideTutorialButton() {
        getFloatingTutorialView().hideButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.augmented_reality_tutorial_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    /*Init scene view*/
    public void initArSceneView(String modelFileName, String objectiveDBName) {

        /*Clean if needed*/
        if (modelBaseNode != null) {
            arSceneView.getScene().removeChild(modelBaseNode);
        }

        modelBaseNode = new AugmentedImageNode(getContext(), modelFileName);
        arSceneView.getScene().addOnUpdateListener(frameTime -> {
            for (AugmentedImage augmentedImage : arSceneView.getArFrame().getUpdatedTrackables(AugmentedImage.class)) {
                if (augmentedImage.getTrackingState() == TrackingState.TRACKING &&
                        augmentedImage.getName().equals(objectiveDBName) && modelBaseNode.getImage() == null) {
                    modelBaseNode.setImage(augmentedImage);
                    arSceneView.getScene().addChild(modelBaseNode);
                }
            }
        });
    }

    public void checkSessionStateAndConfigure(String objectiveImgDBName, String objectiveImgPath) {
        if (session == null) {

            String message = null;

            try {
                session = new Session(getContext());
            } catch (UnavailableArcoreNotInstalledException e) {
                message = "ArCore not installed";
            } catch (UnavailableApkTooOldException e) {
                message = "Update ArCore";
            } catch (UnavailableSdkTooOldException e) {
                message = "ArCore SDK too old";
            } catch (Exception e) {
                message = "This device does not support AR";
            }

            if (message == null) {
                configureSession(session, objectiveImgDBName, objectiveImgPath);
            } else {
                post(new SessionCreationOrConfiguredErrorEvent(message));
            }
        } else {
            post(new SessionCreatedAndConfigured());
        }
    }

    private void configureSession(Session session, String objectiveImgDBName, String objectiveImgPath) {
        new ArConfigThread(
                new AugmentedImageDatabase(session),
                objectiveImgDBName,
                uiConfigHandler,
                getActivity().getAssets(),
                objectiveImgPath,
                new Config(session))
                .start();
    }

    /* Use reverse order based in pauseArComponents() method */
    public void resumeArComponents() {
        if (session != null) {
            try {
                session.resume();
                arSceneView.resume();
            } catch (CameraNotAvailableException e) {
                session = null;
                /* Camera not available - maybe some other app is using it */
                Log.e("AugmentedRealityView", "Camera in use");
            }
        }
    }

    /* Reverse order based in resumeArComponents() method */
    public void pauseArComponents() {
        if (session != null) {
            arSceneView.pause();
            session.pause();
        }
    }

    /*Configuration thread helper*/
    private static class ArConfigThread extends Thread {

        public static final int ID = 0;
        public static final int SUCCESS = 0;
        public static final int FAILURE = 1;

        private static class ProcessedResult {
            public Config config;

            public ProcessedResult(Config config) {
                this.config = config;
            }
        }

        private Config config;
        private SoftReference<AugmentedImageDatabase> augmentedImageDBRef;
        private SoftReference<Handler> uiHandlerRef;
        private SoftReference<AssetManager> assetManagerRef;
        private String objectiveImgPath;
        private String objectiveName;

        public ArConfigThread(AugmentedImageDatabase augmentedImageDB, String objectiveName,
                              Handler uiHandler, AssetManager assetManager, String objectiveImgPath, Config config) {
            this.config = config;
            this.uiHandlerRef = new SoftReference<>(uiHandler);
            this.assetManagerRef = new SoftReference<>(assetManager);
            this.objectiveImgPath = objectiveImgPath;
            this.objectiveName = objectiveName;
            this.augmentedImageDBRef = new SoftReference<>(augmentedImageDB);
        }

        @Override
        public void run() {
            super.run();

            Message msg = Message.obtain();
            msg.arg1 = ID;
            try {
                if (uiHandlerRef.get() != null && assetManagerRef.get() != null && augmentedImageDBRef.get() != null) {

                    Bitmap augmentedImageBitmap = BitmapFactory.decodeStream(assetManagerRef.get().open(objectiveImgPath));
                    if (augmentedImageBitmap != null) {
                        augmentedImageDBRef.get().addImage(objectiveName, augmentedImageBitmap);
                        config.setAugmentedImageDatabase(augmentedImageDBRef.get());
                        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);

                        msg.obj = new ProcessedResult(config);
                        msg.arg2 = SUCCESS;
                        uiHandlerRef.get().sendMessage(msg);
                    }
                }
            } catch (IOException e) {
                Log.e("ArConfigThreadError", e.getMessage());
                msg.arg2 = FAILURE;
                msg.obj = e.getMessage();
                uiHandlerRef.get().sendMessage(msg);
            }
        }
    }

    /**
     * Static classes for BUS
     */
    public static class SessionCreatedAndConfigured {
        /*Nothing to do here*/
    }

    public static class SessionCreationOrConfiguredErrorEvent {
        public String message;

        public SessionCreationOrConfiguredErrorEvent(String message) {
            this.message = message;
        }
    }

    public static class IntroEvent extends BannerEventBase {
        public IntroEvent(int option) {
            super(option);
        }
    }

    public static class QuestionEvent extends BannerEventBase {
        public QuestionEvent(int option) {
            super(option);
        }
    }
}
