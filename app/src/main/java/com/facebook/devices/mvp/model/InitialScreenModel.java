package com.facebook.devices.mvp.model;

import android.os.Build;

import com.facebook.devices.BuildConfig;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class InitialScreenModel extends ServiceModel {

    private final String deviceId;
    private final String sessionId;

    public InitialScreenModel(String deviceId, String sessionId) {
        this.deviceId = deviceId;
        this.sessionId = sessionId;
    }

    public String getDeviceName() {
        return Build.MODEL;
    }

    public String getSerialNumber() {
        return deviceId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getApkName() {
        return BuildConfig.APK_NAME;
    }

    public String getVersionNumber() {
        return BuildConfig.VERSION_NAME;
    }
}
