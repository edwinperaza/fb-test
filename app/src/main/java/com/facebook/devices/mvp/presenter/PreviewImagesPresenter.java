package com.facebook.devices.mvp.presenter;

import android.annotation.SuppressLint;
import android.view.MotionEvent;

import com.facebook.devices.mvp.model.PreviewImagesModel;
import com.facebook.devices.mvp.view.PreviewImagesView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.mvp.view.PreviewImagesView.ResultDialogEvent.RESULT_NEGATIVE;
import static com.facebook.devices.mvp.view.PreviewImagesView.ResultDialogEvent.RESULT_POSITIVE;

public class PreviewImagesPresenter extends PresenterTestBase<PreviewImagesModel, PreviewImagesView> {

    private Runnable imagesRunnable = () -> {
        if (model.shouldAskResult()) {
            stopSlideshow();
            view.showResultDialog();
            return;
        }

        model.moveNextImagePosition();
        view.changeImage(model.getImagePosition());
    };

    private Runnable tutorialRunnable = view::hideTutorialButton;

    @SuppressLint("ClickableViewAccessibility")
    public PreviewImagesPresenter(PreviewImagesModel model, PreviewImagesView view) {
        super(model, view);
        view.setupView(model.getImages());
    }

    public void onResume() {
        if (model.isPermissionGranted()) {
            model.resetPosition();
            view.setViewPagerAdapter();

            view.changeImage(model.getImagePosition());
            arrowsVisibility();

            startSlideshow();
            return;
        }

        model.requestPermissions();
    }

    public void onStop() {
        stopSlideshow();
        view.getUiHandler().removeCallbacks(tutorialRunnable);
    }

    private void startSlideshow() {
        view.getUiHandler().postDelayed(imagesRunnable, model.getPauseTimeMillis());
    }

    private void stopSlideshow() {
        view.getUiHandler().removeCallbacks(imagesRunnable);
    }

    private void arrowsVisibility() {
        int position = model.getImagePosition();
        if (position == 0) {
            view.previousImageIsVisible(false);
            view.nextImageIsVisible(true);
            return;
        }

        if (model.isLastImage()) {
            view.previousImageIsVisible(true);
            view.nextImageIsVisible(false);
            return;
        }

        if (position > 0 && !model.isLastImage()) {
            view.previousImageIsVisible(true);
            view.nextImageIsVisible(true);
            return;
        }

        view.previousImageIsVisible(false);
        view.nextImageIsVisible(false);
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            model.fail("User denied permissions");
            view.finishActivity();
        }
    }

    @Subscribe
    public void onResultDialogEvent(PreviewImagesView.ResultDialogEvent event) {
        switch (event.getResult()) {
            case RESULT_POSITIVE:
                model.pass();
                view.finishActivity();
                break;

            case RESULT_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;
        }
    }

    @Subscribe
    public void onPreviousButtonPressedEvent(PreviewImagesView.PreviousButtonPressedEvent event) {
        stopSlideshow();
        model.movePreviousImagePosition();
        view.changeImage(model.getImagePosition());
        arrowsVisibility();
        startSlideshow();
    }

    @Subscribe
    public void onNextButtonPressedEvent(PreviewImagesView.NextButtonPressedEvent event) {
        stopSlideshow();
        model.moveNextImagePosition();
        view.changeImage(model.getImagePosition());
        arrowsVisibility();
        startSlideshow();
    }

    @Subscribe
    public void onPageScrolledEvent(PreviewImagesView.PageSelectedEvent event) {
        stopSlideshow();
        model.setImagePosition(event.position);
        arrowsVisibility();
        startSlideshow();
    }

    @Subscribe
    public void onPageTouchedEvent(PreviewImagesView.PageTouchedEvent event) {
        if (event.event.getAction() == MotionEvent.ACTION_UP) {
            if (!view.isTutorialVisible()) {
                view.showTutorialButton();
                view.getUiHandler().postDelayed(tutorialRunnable, model.getPauseTutorialTimeMillis());
            }
        }
    }
}
