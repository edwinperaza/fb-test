package com.facebook.devices.mvp.view;


import android.animation.Animator;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.mvp.view.ViewBase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FinalScreenView extends ViewBase<Activity> {

    private BusProvider.Bus bus;
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    @BindView(R.id.iv_icon) ImageView ivIcon;
    @BindView(R.id.tv_final_message) TextView tvFinalMessage;
    @BindView(R.id.main_layout) LinearLayout mainLayout;
    @BindView(android.R.id.content) View rootLayout;

    public FinalScreenView(Activity activity,  BusProvider.Bus bus) {
        super(activity);
        this.bus = bus;

        ButterKnife.bind(this, activity);

        setup();
    }

    public Handler getUIHandler(){
        return uiHandler;
    }

    private void setup() {
        ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(() -> {
                revealActivity((int) (ivIcon.getX() + ivIcon.getWidth() / 2), (int) (ivIcon.getY() + ivIcon.getHeight() / 2));
                rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this::setup);
            });
        }
    }

    private void revealActivity(int x, int y) {
        float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.05); /*adding 5% extra radius*/

        /*Animator creation*/
        Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
        circularReveal.setDuration(700);
        circularReveal.setInterpolator(new AccelerateInterpolator());
        circularReveal.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                revealInfoViews();
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        /*Start animation*/
        rootLayout.setVisibility(View.VISIBLE);
        circularReveal.start();
    }

    private void revealInfoViews() {
        Animation revealAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.translate_down_to_top_final_screen);
        revealAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                post(new OnAnimationEndEvent());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        /*Start animation*/
        ivIcon.startAnimation(revealAnimation);
        tvFinalMessage.startAnimation(revealAnimation);
    }

    public void successMode() {
        mainLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.final_screen_success_bg));
        ivIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.success));
        /*Text*/
        String boldMessage = getContext().getString(R.string.final_success_message_bold);
        String message = getContext().getString(R.string.final_success_message) + "\n " + boldMessage;
        SpannableString span = new SpannableString(message);
        span.setSpan(new StyleSpan(Typeface.BOLD), message.indexOf(boldMessage), message.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tvFinalMessage.setText(span);
    }

    public void failureMode() {
        mainLayout.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.final_screen_failure_bg));
        ivIcon.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.unsuccess));
        /*Text*/
        String boldMessage = getContext().getString(R.string.final_failure_message_bold);
        String message = getContext().getString(R.string.final_failure_message) + "\n " + boldMessage;
        SpannableString span = new SpannableString(message);
        span.setSpan(new StyleSpan(Typeface.BOLD), message.indexOf(boldMessage), message.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tvFinalMessage.setText(span);
    }

    /* Bus events */
    protected void post(Object event) {
        bus.post(event);
    }

    public static class OnAnimationEndEvent {
        /*Nothing to do here*/
    }
}
