package com.facebook.devices.mvp.model;

import android.support.annotation.IntDef;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class ColorModel extends ServiceModel {

    @IntDef({QUESTION_ONE, QUESTION_TWO})
    @interface QuestionState{}

    public static final int QUESTION_ONE = 0;
    public static final int QUESTION_TWO = 1;

    private @QuestionState int state = QUESTION_ONE;
    private String colorCodes[];
    private boolean showTutorial;
    private boolean showMuraQuestion;

    public ColorModel(boolean showTutorial, boolean showMuraQuestion, String... colorCodes) {
        this.showTutorial = showTutorial;
        this.showMuraQuestion = showMuraQuestion;
        this.colorCodes = colorCodes;
    }

    public void nextState(){
        state = QUESTION_TWO;
    }

    @QuestionState
    public int getState(){
        return state;
    }

    public void setColorCode(String... colorCodes) {
        this.colorCodes = colorCodes;
    }

    public String[] getColorCode() {
        return colorCodes;
    }

    public boolean shouldShowTutorial() {
        return showTutorial;
    }

    public boolean shouldShowMuraQuestion() {
        return showMuraQuestion;
    }

}