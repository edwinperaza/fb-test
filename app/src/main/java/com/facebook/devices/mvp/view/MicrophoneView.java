package com.facebook.devices.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.WaveGraphView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MicrophoneView extends TestViewBase {

    @BindView(R.id.wave_view) WaveGraphView waveGraphView;
    @BindView(R.id.rl_wave_layout) RelativeLayout waveLayout;

    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public MicrophoneView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.microphone_tutorial_dialog_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showFloatingTutorialButton(boolean isAnimated) {
        getFloatingTutorialView().showButton(isAnimated);
    }

    public void showInitialDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.microphone_sound_wave_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new ButtonSucceededClicked()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new ButtonFailedPressedEvent()))
                        .build()
        );
    }

    public void setStateMicroRecording(boolean isRecording) {
        waveGraphView.setVisibility(isRecording ? View.VISIBLE : View.GONE);
        waveLayout.setVisibility(isRecording ? View.VISIBLE : View.GONE);
    }

    public void drawAudioSample(short[] samples) {
        try {
            waveGraphView.setSamples(samples);
        } catch (WaveGraphView.WaveViewException e) {
            Log.e("WaveView", e.getMessage());
        }
    }

    /**
     * Static classes for BUS
     */
    public static class ButtonSucceededClicked {/*Nothing to do here*/
    }

    public static class ButtonFailedPressedEvent {/*Nothing to do here*/
    }

}