package com.facebook.devices.mvp.presenter;


import android.content.res.Configuration;
import android.os.Bundle;

import com.facebook.devices.mvp.model.OrientationModel;
import com.facebook.devices.mvp.view.OrientationView;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEGATIVE;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_POSITIVE;

public class OrientationPresenter extends PresenterTestBase<OrientationModel, OrientationView> {

    public OrientationPresenter(OrientationModel model, OrientationView view) {
        super(model, view);

        view.showTutorialFloatingButton();
        processUIState(model.getActualState());
    }

    public void saveInstanceState(Bundle state) {
        state.putInt(OrientationModel.ORIENTATION_STATE_SAVED, model.getActualState());
    }

    /**
     * Configuration changed callback
     */
    public void onConfigurationChanged(Configuration newConfig) {
        /*Resize ui - Needed due activity layout is not recreated*/
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            view.resizeComponentsForPortrait();
        } else {
            /*Landscape*/
            view.resizeComponentsForLandscape();
        }
        /*Update ui*/
        view.showTutorialFloatingButton();
        model.nextState();
        processUIState(model.getActualState());
    }

    private void processUIState(@OrientationModel.OrientationStateMode int state) {
        switch (state) {
            case OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT:
                view.showTutorialWrongOrientationMessage();
                break;

            case OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_ONE:
                view.showTutorialPortrait();
                break;

            case OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_ONE:
                view.showQuestionDialogLandscape();
                break;

            case OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_TWO:
                view.showQuestionDialogPortraitOne();
                break;
        }
    }

    private void failTest() {
        model.fail();
        view.finishActivity();
    }

    /**
     * Dialog events
     */

    @Subscribe
    public void onDialogQuestionLandscape(OrientationView.DialogQuestionLandscape event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                view.showLandscapeNextStep();
                break;

            case OPTION_NEGATIVE:
                failTest();
                break;
        }
    }

    @Subscribe
    public void onDialogQuestionPortraitOne(OrientationView.DialogQuestionPortraitOne event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                view.showQuestionDialogPortraitTwo();
                break;

            case OPTION_NEGATIVE:
                failTest();
                break;
        }
    }

    @Subscribe
    public void onDialogQuestionPortraitTwo(OrientationView.DialogQuestionPortraitTwo event) {
        switch (event.optionSelected) {
            case OPTION_POSITIVE:
                model.pass();
                view.finishActivity();
                break;

            case OPTION_NEGATIVE:
                failTest();
                break;
        }
    }

    @Subscribe
    public void onWrongPositionEvent(OrientationView.WrongPositionEvent event) {
        failTest();
    }

    @Subscribe
    public void onTutorialPortraitEvent(OrientationView.TutorialPortraitEvent event) {
        failTest();
    }

    @Subscribe
    public void onTutorialLandscapeEvent(OrientationView.TutorialLandscapeEvent event) {
        failTest();
    }

    public void onDestroy() {
        view.onDestroy();
    }
}
