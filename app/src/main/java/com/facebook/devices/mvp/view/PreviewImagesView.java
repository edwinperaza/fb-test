package com.facebook.devices.mvp.view;


import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.ImagePagerAdapter;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreviewImagesView extends TestViewBase {

    @BindView(R.id.rl_container) RelativeLayout layoutContainer;
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.iv_next_button) ImageView nextImage;
    @BindView(R.id.iv_prev_button) ImageView previousImage;

    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ImagePagerAdapter adapter;

    @SuppressLint("ClickableViewAccessibility")
    public PreviewImagesView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                post(new PageSelectedEvent(position));
            }
        });

        viewPager.setOnTouchListener((v, event) -> {
            post(new PageTouchedEvent(event));
            return false;
        });
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        /*Tutorial setup*/
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.images_preview_tutorial_text)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void setupView(Uri... images) {
        viewPager.setVisibility(View.VISIBLE);
        adapter = new ImagePagerAdapter(getContext(), images);
    }

    public Handler getUiHandler() {
        return uiHandler;
    }

    public void setViewPagerAdapter() {
        viewPager.setVisibility(View.VISIBLE);
        if (adapter != null) {
            viewPager.setAdapter(adapter);
        }
    }

    public void previousImageIsVisible(boolean isVisible) {
        previousImage.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public void nextImageIsVisible(boolean isVisible) {
        nextImage.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    public void changeImage(int position) {
        viewPager.setCurrentItem(position);
    }

    public void showResultDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.images_preview_dialog_title))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new ResultDialogEvent(ResultDialogEvent.RESULT_POSITIVE)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new ResultDialogEvent(ResultDialogEvent.RESULT_NEGATIVE)))
                        .build()
        );
    }

    @OnClick(R.id.iv_prev_button)
    public void onPreviousButtonPressed() {
        post(new PreviousButtonPressedEvent());
    }

    @OnClick(R.id.iv_next_button)
    public void onNextButtonPressed() {
        post(new NextButtonPressedEvent());
    }

    public void hideTutorialButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showTutorialButton() {
        getFloatingTutorialView().showButton();
    }

    public boolean isTutorialVisible() {
        return getFloatingTutorialView().isButtonVisible();
    }

    public ImagePagerAdapter getAdapter() {
        return adapter;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    /**
     * Static classes for BUS
     */
    public static class ResultDialogEvent {

        @IntDef({RESULT_POSITIVE, RESULT_NEGATIVE})
        @Retention(RetentionPolicy.SOURCE)
        @interface DialogResult {
        }

        public static final int RESULT_POSITIVE = 0;
        public static final int RESULT_NEGATIVE = 1;

        @DialogResult private int result;

        public ResultDialogEvent(@DialogResult int result) {
            this.result = result;
        }

        @DialogResult
        public int getResult() {
            return result;
        }
    }

    public static class PreviousButtonPressedEvent {/*Nothing to do here*/
    }

    public static class NextButtonPressedEvent {/*Nothing to do here*/
    }

    public static class PageSelectedEvent {/*Nothing to do here*/
        public int position;

        public PageSelectedEvent(int position) {
            this.position = position;
        }
    }

    public static class PageTouchedEvent {
        public MotionEvent event;

        public PageTouchedEvent(MotionEvent event) {
            this.event = event;
        }
    }
}