package com.facebook.devices.mvp.view;


import android.support.annotation.IntDef;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.TouchDetectorGridView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridTouchPatternView extends TestViewBase {

    @BindView(R.id.touch_detector) TouchDetectorGridView touchDetectorSimple;

    public GridTouchPatternView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
        init();
    }

    private void init() {
        /* Touch detector setup */
        touchDetectorSimple.setTouchDetectorViewCallback(new TouchDetectorGridView.TouchDetectorViewCallback() {
            @Override
            public void onPatternDetectionFinishes(int resultMode) {
                post(new TouchDetectorSimpleResult(resultMode));
            }

            @Override
            public void onViewStateChanged(int state) {
                post(new OnTouchDetectorStateChanged(state));
            }
        });
    }

    public void enableTouch() {
        touchDetectorSimple.setEnabled(true);
    }

    public void disableTouch() {
        touchDetectorSimple.setEnabled(false);
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();

        post(new TutorialFloatingButtonClicked());
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.touch_pattern_simple_intro_message)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void showInitDialog() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.touch_pattern_simple_init_dialog_text))
                        .setOptionTextPositive(R.string.ok_base)
                        .setOptionPositiveListener(() -> post(new InitDialogEvent()))
                        .build()
        );
    }

    public void showTutorialFloatingButton() {
        getFloatingTutorialView().showButton();
    }

    public void hideInitDialog() {
        getBannerView().hideBanner();
    }

    public void hideTutorialFloatingButton() {
        getFloatingTutorialView().hideButton();
    }

    public void showTimeUpPatternDialog() {
        DialogFactory.createMaterialDialog(getContext(),
                R.string.touch_pattern_simple_time_up_title,
                R.string.touch_pattern_simple_time_up_message)
                .setPositiveButton(R.string.time_up_positive_option, (dialog, which) -> post(new TimeUpDialogEvent(TimeUpDialogEvent.KEEP_TOUCHING)))
                .setNegativeButton(R.string.time_up_negative_option, (dialog, which) -> post(new TimeUpDialogEvent(TimeUpDialogEvent.FAIL_TEST)))
                .show();
    }

    public void setTouchDetectorSize(@GridTouchPatternModel.TouchDetectorSize int sizeMode) {
        touchDetectorSimple.setSizeMode(sizeMode);
    }

    /**
     * This method should be used to prepare the view
     */
    public void initTouchDetectorView(@TouchPatternModel.TouchDetectorSize int sizeMode) {
        touchDetectorSimple.initView(sizeMode);
    }

    /**
     * Static classes for BUS
     */
    public static class InitDialogEvent {
        /*Nothing to do here*/
    }

    public static class TutorialFloatingButtonClicked {/* Nothing to do here*/
    }

    public static class OnTouchDetectorStateChanged {
        @TouchDetectorGridView.TouchDetectorState private int state;

        public OnTouchDetectorStateChanged(@TouchDetectorGridView.TouchDetectorViewCallback.TouchDetectorCallbackResult int state) {
            this.state = state;
        }

        @TouchDetectorGridView.TouchDetectorState
        public int getResult() {
            return state;
        }
    }

    public static class TouchDetectorSimpleResult {
        @TouchDetectorGridView.TouchDetectorViewCallback.TouchDetectorCallbackResult private int result;

        public TouchDetectorSimpleResult(@TouchDetectorGridView.TouchDetectorViewCallback.TouchDetectorCallbackResult int result) {
            this.result = result;
        }

        @TouchDetectorGridView.TouchDetectorViewCallback.TouchDetectorCallbackResult
        public int getResult() {
            return result;
        }
    }

    public static class TimeUpDialogEvent {

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({KEEP_TOUCHING, FAIL_TEST})
        @interface OnIncorrectDialogCallbackOptions {
        }

        public static final int KEEP_TOUCHING = 0;
        public static final int FAIL_TEST = 1;

        @OnIncorrectDialogCallbackOptions private int option;

        public TimeUpDialogEvent(@OnIncorrectDialogCallbackOptions int option) {
            this.option = option;
        }

        @OnIncorrectDialogCallbackOptions
        public int getOptionSelected() {
            return option;
        }
    }
}
