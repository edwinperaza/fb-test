package com.facebook.devices.mvp.presenter;

import com.facebook.devices.mvp.view.TestViewBase;
import com.facebook.hwtp.mvp.model.ServiceModel;
import com.facebook.hwtp.mvp.presenter.PresenterBase;

abstract class PresenterTestBase<M extends ServiceModel, V extends TestViewBase> extends PresenterBase<M, V> {

    public PresenterTestBase(M model, V view) {
        super(model, view);
    }
}
