package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.LedModel;
import com.facebook.devices.mvp.view.LedView;

import org.greenrobot.eventbus.Subscribe;

public class LedPresenter extends PresenterTestBase<LedModel, LedView> {

    public LedPresenter(LedModel model, LedView view) {
        super(model, view);
        executeLEDBehavior(true);
    }

    public void executeLEDBehavior(boolean shouldContinue) {
        if (shouldContinue) {
            model.startLEDBehavior(model.getCurrentState(), ledState -> {
                /*LED color lifecycle completed - check for the next one */
                executeLEDBehavior(model.jumpNextColor());
            });
            view.changeLedLocationMessage(model.getPrettyLEDLocation());
        } else {
            view.showQuestionDialog(model.getPrettyLEDLocation());
        }
    }

    @Subscribe
    public void onDialogOptionSelected(LedView.OnDialogOptionSelected event) {
        switch (event.getOptionSelected()) {
            case LedView.OnDialogOptionSelected.OPTION_POSITIVE:
                model.pass();
                view.finishActivity();
                break;

            case LedView.OnDialogOptionSelected.OPTION_NEGATIVE:
                model.fail();
                view.finishActivity();
                break;

            case LedView.OnDialogOptionSelected.OPTION_NEUTRAL:
                model.resetLedState();
                executeLEDBehavior(true);
                break;
        }
    }
}
