package com.facebook.devices.mvp.model;


import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.support.annotation.IntDef;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class SpeakerFileModel extends ServiceModel {

    public static final int QUESTION_STATE_1 = 0;
    public static final int QUESTION_STATE_2 = 1;


    /*States*/
    @IntDef({QUESTION_STATE_1, QUESTION_STATE_2})
    public @interface QuestionState {

    }

    @QuestionState private int questionState = QUESTION_STATE_1;
    private Context context;
    private MediaPlayer mediaPlayer;
    private Visualizer visualizer;
    private PermissionHandler permissionHandler;
    private Uri uriSong;
    private VolumeManager volumeManager;

    public SpeakerFileModel(Context context, PermissionHandler permissionHandler, Uri uriSong, VolumeManager volumeManager) {
        this.context = context;
        this.permissionHandler = permissionHandler;
        this.uriSong = uriSong;
        this.volumeManager = volumeManager;
    }

    public void setDefaultVolume() {
        volumeManager.setDefaultMediaVolume();
    }

    @QuestionState
    public int getQuestionState() {
        return questionState;
    }

    public void nextQuestion() {
        if (questionState == QUESTION_STATE_1) {
            questionState = SpeakerFileModel.QUESTION_STATE_2;
        }
    }

    public void startPlayer(Visualizer.OnDataCaptureListener visualizerCapture) {
        if (uriSong == null) {
            return;
        }

        mediaPlayer = MediaPlayer.create(context, uriSong);

        if (mediaPlayer == null) {
            return;
        }

        mediaPlayer.setOnCompletionListener(mp -> {
            mp.reset();
            mp.release();
        });
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        visualizer = new Visualizer(mediaPlayer.getAudioSessionId());
        visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        visualizer.setDataCaptureListener(visualizerCapture, Visualizer.getMaxCaptureRate() / 2, true, false);
        visualizer.setEnabled(true);
    }

    public void stopPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
            } catch (Exception e) {
                //  Nothing to do
            }
            mediaPlayer = null;
        }

        if (visualizer != null) {
            visualizer.release();
            visualizer = null;
        }
    }

    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }
}