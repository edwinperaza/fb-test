package com.facebook.devices.mvp.view;

import android.app.AlertDialog;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrientationRipleyView extends TestViewBase {

    @BindView(R.id.tv_orientation_title) TextView tvMessage;
    private AlertDialog tutorialDialog;

    public OrientationRipleyView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void onDestroy() {
        if (tutorialDialog != null) {
            tutorialDialog.dismiss();
            tutorialDialog = null;
        }
    }

    @OnClick(R.id.btn_fail)
    public void onFailButtonClicked(){
        post(new OrientationFailEvent());
    }

    @Override
    public void onFloatingTutorialClicked() {
        if (tutorialDialog == null) {
            tutorialDialog = DialogFactory.createMaterialDialog(
                    getContext(),
                    R.string.more_help_base,
                    R.string.orientation_ripley_tutorial_dialog_text)
                    .setPositiveButton(R.string.ok_base, null)
                    .create();
        }
        tutorialDialog.show();
    }

    /**
     * Static classes for BUS
     */

    public static class OrientationFailEvent{
        /*Nothing to do here*/
    }
}
