package com.facebook.devices.mvp.presenter;


import com.facebook.devices.mvp.model.InitialScreenModel;
import com.facebook.devices.mvp.view.InitialScreenView;
import com.facebook.hwtp.mvp.presenter.PresenterBase;


public class InitialScreenPresenter extends PresenterBase<InitialScreenModel, InitialScreenView> {

    public InitialScreenPresenter(InitialScreenModel model, InitialScreenView view) {
        super(model, view);

        onResume();
    }

    public void onResume() {
        view.setDeviceName(model.getDeviceName());
        view.setApkName(model.getApkName());
        view.setVersionNumber(model.getVersionNumber());

        if (model.isConnected()) {
            view.showConnected();
        } else {
            view.showDisconnected();
        }

        /*Serial Number*/
        view.setSerialNumber(model.getSerialNumber());
    }

    public void onConnected(boolean connected) {
        if (connected) {
            view.showConnected();
        } else {
            view.showDisconnected();
        }
    }

    public void showDownloading(boolean show) {
        if (show) {
            view.showDownloading();
        } else {
            view.hideDownloading();
        }
    }

    public void showTransferring(boolean show) {
        if (show) {
            view.showTransferring();
        } else {
            view.hideTransferring();
        }
    }
}
