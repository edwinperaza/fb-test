package com.facebook.devices.mvp.view;


import android.util.Log;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;
import com.facebook.devices.utils.customviews.WaveGraphView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpeakerFileView extends TestViewBase {

    @BindView(R.id.wave_view) WaveGraphView waveGraphView;

    public SpeakerFileView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void showTutorialFloatingButton() {
        getFloatingTutorialView().showButton();
    }

    @Override
    public void onFloatingTutorialClicked() {
        super.onFloatingTutorialClicked();
        DialogFactory.createMaterialDialog(
                getContext(),
                R.string.more_help_base,
                R.string.speaker_file_tutorial_text)
                .setPositiveButton(R.string.ok_base, null)
                .show();
    }

    public void updateWaveView(byte[] bytes) {
        try {
            waveGraphView.setBytes(bytes);
        } catch (WaveGraphView.WaveViewException e) {
            Log.e("WaveView", e.getMessage());
        }
    }

    public void showDialogStateOne() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.speaker_test_sound_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new DialogOnePassEvent()))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new DialogFailedPressedEvent()))
                        .build()
        );
    }

    public void showDialogStateTwo() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.speaker_test_distortion_message))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new DialogTwoPassEvent()))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new DialogFailedPressedEvent()))
                        .build()
        );
    }

    /**
     * Static classes for BUS
     */
    public static class DialogOnePassEvent {/*Nothing to do here*/
    }

    public static class DialogTwoPassEvent {/*Nothing to do here*/
    }

    public static class DialogFailedPressedEvent {/*Nothing to do here*/
    }
}