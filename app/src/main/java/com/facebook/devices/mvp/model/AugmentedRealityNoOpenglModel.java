package com.facebook.devices.mvp.model;

import com.facebook.hwtp.mvp.model.ServiceModel;

public class AugmentedRealityNoOpenglModel extends ServiceModel {

    private String openglVersion;

    public AugmentedRealityNoOpenglModel(String openglVersion){
        this.openglVersion = openglVersion;
    }

    public String getOpenglVersion(){
        return openglVersion;
    }
}
