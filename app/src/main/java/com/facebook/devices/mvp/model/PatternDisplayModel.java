package com.facebook.devices.mvp.model;

import android.support.annotation.IntDef;
import com.facebook.hwtp.mvp.model.ServiceModel;

public class PatternDisplayModel extends ServiceModel {

    @IntDef({QUESTION_ONE, QUESTION_TWO})
    @interface QuestionState{}

    public static final int QUESTION_ONE = 0;
    public static final int QUESTION_TWO = 1;
    private @QuestionState int state = QUESTION_ONE;

    private int squaresNumber;
    private boolean showTutorial;
    private boolean showMuraQuestion;

    public PatternDisplayModel(boolean showTutorial, int squaresNumber, boolean showMuraQuestion) {
        this.showTutorial = showTutorial;
        this.squaresNumber = squaresNumber;
        this.showMuraQuestion = showMuraQuestion;
    }

    public int getSquaresNumber(){
        return squaresNumber;
    }

    public void nextState(){
        state = QUESTION_TWO;
    }

    @QuestionState
    public int getState(){
        return state;
    }

    public boolean shouldShowMuraQuestion() {
        return showMuraQuestion;
    }

    public boolean shouldShowTutorial() {
        return showTutorial;
    }
}
