package com.facebook.devices.mvp.model;


import android.support.annotation.IntDef;

import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class FinalScreenModel extends ServiceModel {
    public static final long DEFAULT_MILLIS_TO_CLOSE = 3000;

    @IntDef({MODE_FAILURE, MODE_SUCCESS})
    @Retention(RetentionPolicy.SOURCE)
    @interface Mode {

    }
    public static final int MODE_FAILURE = 0;
    public static final int MODE_SUCCESS = 1;


    @Mode private int launchMode;
    private long millisToClose;

    public FinalScreenModel(@Mode int launchMode, long millisToClose) {
        this.launchMode = launchMode;
        this.millisToClose = (millisToClose <= 0) ? DEFAULT_MILLIS_TO_CLOSE : millisToClose;
    }

    @Mode
    public int getMode() {
        return launchMode;
    }

    public void setMode(@Mode int launchMode) {
        this.launchMode = launchMode;
    }

    public long getMillisToClose() {
        return millisToClose;
    }
}
