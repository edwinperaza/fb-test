package com.facebook.devices.mvp.view;

import android.app.AlertDialog;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.devices.R;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.DialogFactory;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.DialogOneBaseTextBuilder;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrientationView extends TestViewBase {

    @BindView(R.id.image) ImageView ivImg;
    private AlertDialog tutorialDialog;

    public OrientationView(TestBaseActivity activity, BusProvider.Bus bus) {
        super(activity, bus);
        ButterKnife.bind(this, activity);
    }

    public void showTutorialFloatingButton() {
        getFloatingTutorialView().showButton();
    }

    public void resizeComponentsForLandscape() {
        getBannerView().resizeComponentsForLandscape();
    }

    public void resizeComponentsForPortrait() {
        getBannerView().resizeComponentsForPortrait();
    }

    public void showTutorialWrongOrientationMessage() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_dialog_text_wrong))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new WrongPositionEvent()))
                        .build()
        );
    }

    public void showTutorialPortrait() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_intro_dialog_message_portrait))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new TutorialPortraitEvent()))
                        .build()
        );
    }

    public void showLandscapeNextStep() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_intro_dialog_message_landscape))
                        .setOptionTextPositive(R.string.fail_base)
                        .setOptionPositiveListener(() -> post(new TutorialLandscapeEvent()))
                        .build()
        );
    }

    public void onDestroy() {
        if (tutorialDialog != null) {
            tutorialDialog.dismiss();
            tutorialDialog = null;
        }
    }

    @Override
    public void onFloatingTutorialClicked() {
        if (tutorialDialog == null) {
            tutorialDialog = DialogFactory.createMaterialDialog(
                    getContext(),
                    R.string.more_help_base,
                    R.string.orientation_tutorial_dialog_text)
                    .setPositiveButton(R.string.ok_base, null)
                    .create();
        }
        tutorialDialog.show();
    }

    public void showQuestionDialogLandscape() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_landscape_question_dialog_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new DialogQuestionLandscape(AlertDialogBaseEvent.OPTION_POSITIVE)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new DialogQuestionLandscape(AlertDialogBaseEvent.OPTION_NEGATIVE)))
                        .build()
        );
    }

    public void showQuestionDialogPortraitOne() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_portrait_question_dialog_one_message))
                        .setOptionTextPositive(R.string.yes_base)
                        .setOptionPositiveListener(() -> post(new DialogQuestionPortraitOne(AlertDialogBaseEvent.OPTION_POSITIVE)))
                        .setOptionTextNegative(R.string.no_base)
                        .setOptionNegativeListener(() -> post(new DialogQuestionPortraitOne(AlertDialogBaseEvent.OPTION_NEGATIVE)))
                        .build()
        );
    }

    public void showQuestionDialogPortraitTwo() {
        getBannerView().renderNewBanner(
                new BannerView.BannerBuilder(getContext(), DialogOneBaseTextBuilder.buildView(getContext(), R.string.orientation_portrait_question_dialog_two_message))
                        .setOptionTextPositive(R.string.no_base)
                        .setOptionPositiveListener(() -> post(new DialogQuestionPortraitTwo(AlertDialogBaseEvent.OPTION_POSITIVE)))
                        .setOptionTextNegative(R.string.yes_base)
                        .setOptionNegativeListener(() -> post(new DialogQuestionPortraitTwo(AlertDialogBaseEvent.OPTION_NEGATIVE)))
                        .build()
        );
    }

    /**
     * Static classes for BUS
     */

    public static class WrongPositionEvent {
        /*Nothing to do here*/
    }

    public static class TutorialPortraitEvent {
        /*Nothing to do here*/
    }

    public static class TutorialLandscapeEvent {
        /*Nothing to do here*/
    }

    public static class DialogQuestionLandscape extends AlertDialogBaseEvent {
        public DialogQuestionLandscape(int optionSelected) {
            super(optionSelected);
        }
    }

    public static class DialogQuestionPortraitOne extends AlertDialogBaseEvent {
        public DialogQuestionPortraitOne(int optionSelected) {
            super(optionSelected);
        }
    }

    public static class DialogQuestionPortraitTwo extends AlertDialogBaseEvent {
        public DialogQuestionPortraitTwo(int optionSelected) {
            super(optionSelected);
        }
    }
}
