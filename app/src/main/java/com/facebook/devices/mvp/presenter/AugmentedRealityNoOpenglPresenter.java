package com.facebook.devices.mvp.presenter;

import android.content.ServiceConnection;

import com.facebook.devices.mvp.model.AugmentedRealityNoOpenglModel;
import com.facebook.devices.mvp.presenter.utils.AugmentedRealityPresenterInterface;
import com.facebook.devices.mvp.view.AugmentedRealityNoOpenglView;

import org.greenrobot.eventbus.Subscribe;

public class AugmentedRealityNoOpenglPresenter extends PresenterTestBase<AugmentedRealityNoOpenglModel, AugmentedRealityNoOpenglView> implements AugmentedRealityPresenterInterface {

    public AugmentedRealityNoOpenglPresenter(AugmentedRealityNoOpenglModel model, AugmentedRealityNoOpenglView view) {
        super(model, view);

        /*Init message*/
        view.setMessage(model.getOpenglVersion());
    }

    @Subscribe
    public void onFinishBtnEvent(AugmentedRealityNoOpenglView.FinishBtnEvent event) {
        model.fail();
        view.finishActivity();
    }

    @Override
    public ServiceConnection getServiceConnection() {
        return this;
    }

    @Override
    public void onResume() {
        /*Nothing to do here*/
    }

    @Override
    public void onPause() {
        /*Nothing to do here*/
    }
}
