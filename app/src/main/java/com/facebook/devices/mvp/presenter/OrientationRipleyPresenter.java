package com.facebook.devices.mvp.presenter;


import android.content.ComponentName;
import android.os.IBinder;

import com.facebook.devices.mvp.model.OrientationRipleyModel;
import com.facebook.devices.mvp.view.OrientationRipleyView;

import org.greenrobot.eventbus.Subscribe;

public class OrientationRipleyPresenter extends PresenterTestBase<OrientationRipleyModel, OrientationRipleyView> {

    public OrientationRipleyPresenter(OrientationRipleyModel model, OrientationRipleyView view) {
        super(model, view);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
        model.startSensing();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        model.stopSensing();
        super.onServiceDisconnected(name);
    }

    @Subscribe
    public void onPortraitMode(OrientationRipleyModel.OnPortraitMode event) {
        if (model.allPositionsCovered()) {
            model.pass();
            view.finishActivity();
        }
    }

    @Subscribe
    public void onLandscapeMode(OrientationRipleyModel.OnLandscapeMode event) {
        if (model.allPositionsCovered()) {
            model.pass();
            view.finishActivity();
        }
    }

    /**
     * Dialog events
     */
    @Subscribe
    public void onOrientationFailEvent(OrientationRipleyView.OrientationFailEvent event) {
        model.fail();
        view.finishActivity();
    }

    public void onPause() {
        model.stopSensing();
    }

    public void onDestroy() {
        view.onDestroy();
    }
}
