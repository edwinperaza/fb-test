package com.facebook.devices.mvp.presenter;

import android.content.ServiceConnection;
import android.hardware.camera2.CameraManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.devices.mvp.model.AugmentedRealityModel;
import com.facebook.devices.mvp.presenter.utils.AugmentedRealityPresenterInterface;
import com.facebook.devices.mvp.view.AugmentedRealityView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import org.greenrobot.eventbus.Subscribe;

import static com.facebook.devices.utils.customviews.BannerEventBase.OPTION_ONE;
import static com.facebook.devices.utils.customviews.BannerEventBase.OPTION_TWO;

public class AugmentedRealityPresenter extends PresenterTestBase<AugmentedRealityModel, AugmentedRealityView> implements AugmentedRealityPresenterInterface {

    private CameraManager.AvailabilityCallback availabilityCallback = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            if (model.isCameraTarget(cameraId)) {
                view.checkSessionStateAndConfigure(model.getObjectiveDbName(), model.getObjectiveImgPath());
            }
        }
    };

    public AugmentedRealityPresenter(AugmentedRealityModel model, AugmentedRealityView view) {
        super(model, view);

        /*Init ArScene*/
        view.initArSceneView(model.getModelFileName(), model.getObjectiveDbName());
    }

    public void onResume() {
        try {
            if (model.checkArCoreInstallation()) { /*ArCore already installed*/

                /*Check permissions needed*/
                if (!model.isPermissionGranted()) {
                    model.requestPermissions();
                    return;
                }

                /*Camera Available callback*/
                model.getCameraManager().registerAvailabilityCallback(availabilityCallback, null);

                /*Send stop camera broadcast*/
                model.sendCameraUnlockRequest();
            }
        } catch (UnavailableUserDeclinedInstallationException | UnavailableDeviceNotCompatibleException e) {
            fail(e.getMessage());
        }
    }

    public void onPause() {
        view.pauseArComponents();
    }

    @Override
    public ServiceConnection getServiceConnection() {
        return this;
    }

    /*Subscribers*/
    @Subscribe
    public void onIntroEvent(AugmentedRealityView.IntroEvent event) {
        view.showQuestionBanner();
        view.hideTutorialButton();
    }

    @Subscribe
    public void onQuestionEvent(AugmentedRealityView.QuestionEvent event) {
        switch (event.optionSelected) {
            case OPTION_ONE:
                pass();
                break;

            case OPTION_TWO:
                fail();
                break;
        }
    }

    @Subscribe
    public void onSessionCreatedAndConfiguredEvent(AugmentedRealityView.SessionCreatedAndConfigured event) {
        view.resumeArComponents();
        view.showIntroBanner();
        view.showTutorialButton();
    }

    @Subscribe
    public void onSessionCreationErrorEvent(AugmentedRealityView.SessionCreationOrConfiguredErrorEvent event) {
        Log.e("AugmentedRealityActivity", event.message);
        fail(event.message);
    }

    @Subscribe
    public void onRequestPermissionResult(RuntimePermissionHandler.PermissionDeniedEvent event) {
        if (!model.shouldRequestPermissions()) {
            fail("User denied permissions");
        }
    }

    private void pass() {
        finalTasks();
        model.pass();
        view.finishActivity();
    }

    private void fail(String... message) {
        finalTasks();
        model.fail(message);
        view.finishActivity();
    }

    private void finalTasks() {
        /*Unregister availability callback*/
        model.getCameraManager().unregisterAvailabilityCallback(availabilityCallback);
        /*Send stat camera broadcast */
        model.sendCameraLockRequest();
    }
}
