package com.facebook.devices.mvp.model;


import android.support.annotation.IntDef;

import com.facebook.hwtp.mvp.model.ServiceModel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class OrientationModel extends ServiceModel {

    public static final String ORIENTATION_STATE_SAVED = "OrientationStateSaved";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ORIENTATION_STATE_MODE_PORTRAIT_ONE, ORIENTATION_STATE_MODE_LANDSCAPE_ONE,
                    ORIENTATION_STATE_MODE_PORTRAIT_TWO, ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT})
    public @interface OrientationStateMode {
    }

    public static final int ORIENTATION_STATE_MODE_PORTRAIT_ONE = 0;
    public static final int ORIENTATION_STATE_MODE_LANDSCAPE_ONE = 1;
    public static final int ORIENTATION_STATE_MODE_PORTRAIT_TWO = 2;
    public static final int ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT = -1;

    @IntDef({DIALOG_INTRO_WRONG_ORIENTATION, DIALOG_INTRO_PORTRAIT, DIALOG_INTRO_LANDSCAPE, DIALOG_QUESTION_LANDSCAPE,
                    DIALOG_QUESTION_PORTRAIT_ONE, DIALOG_QUESTION_PORTRAIT_TWO})
    @interface AvailableDialog {
    }

    public static final int DIALOG_INTRO_WRONG_ORIENTATION = -1;
    public static final int DIALOG_INTRO_PORTRAIT = 0;
    public static final int DIALOG_INTRO_LANDSCAPE = 1;
    public static final int DIALOG_QUESTION_LANDSCAPE = 2;
    public static final int DIALOG_QUESTION_PORTRAIT_ONE = 3;
    public static final int DIALOG_QUESTION_PORTRAIT_TWO = 4;

    @OrientationStateMode
    private int actualState;

    public OrientationModel(@OrientationStateMode int actualState) {
        this.actualState = actualState;
    }

    @OrientationStateMode
    public int getActualState() {
        return actualState;
    }

    public void nextState() {
        if (actualState < ORIENTATION_STATE_MODE_PORTRAIT_TWO) {
            actualState++;
        } else {
            actualState--;
        }
    }
}
