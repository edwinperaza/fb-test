package com.facebook.devices.aloha.diagnostics.standalonemainscreen;

import android.os.Handler;

import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.aloha.diagnostics.mvp.model.StandaloneModel;
import com.facebook.devices.aloha.diagnostics.mvp.presenter.StandalonePresenter;
import com.facebook.devices.aloha.diagnostics.mvp.view.StandaloneView;
import com.facebook.devices.aloha.diagnostics.mvp.view.StandaloneView.ItemClickListenerEvent;
import com.facebook.devices.db.entity.TestInfoEntity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StandalonePresenterTest {

    private StandalonePresenter presenter;

    @Mock
    private StandaloneModel model;
    @Mock
    private StandaloneView view;
    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getUIHandler()).thenReturn(mockHandler);
        when(model.shouldReadPlan()).thenReturn(true);
        presenter = new StandalonePresenter(model, view);
    }

    @Test
    public void init() {
        //Assert
        verify(model).requestPermissions();
        verify(model).readPlan();
        verify(model).getAllTestAndGroupInfo();
    }

    @Test
    public void onRunAllClicked() {
        //Act
        presenter.onRunAllClicked(null);
        //Assert
        verify(model).resetTutorialTracker();
        verify(model).runAllTests();
    }

    @Test
    public void onItemClickListener() {
        //Arrange
        ItemClickListenerEvent event = new ItemClickListenerEvent(1, new TestInfoEntity());
        //Act
        presenter.onItemClickListener(event);
        //Assert
        verify(model).resetTutorialTracker();
        verify(model).runTest(any(TestInfoEntity.class));
    }

    @Test
    public void onTestAndGroupEventTest() {
        //Arrange
        TestInfoEntity testInfo1 = new TestInfoEntity("0", "TestOne", "TestOne", "Some Desc", "Type1", TestInfoEntity.PASS,  0, 0);
        Map<String, TestInfoEntity> map = new HashMap<>();
        map.put("one", testInfo1);

        GroupTestsEntity group = new GroupTestsEntity("icon", "Type1");
        List<GroupTestsEntity> groupTestsEntityList = new ArrayList<>();
        groupTestsEntityList.add(group);

        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        StandaloneModel.TestAndGroupEvent event = new StandaloneModel.TestAndGroupEvent(map, groupTestsEntityList);
        //Act
        presenter.onTestAndGroupEvent(event);
        //Assert
        verify(view.getUIHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).updateRecyclerItems(groupTestsEntityList, map);
    }
}