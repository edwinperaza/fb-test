package com.facebook.devices.aloha.diagnostics.standalonemainscreen;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.dao.TestInfoDao;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.aloha.diagnostics.mvp.model.StandaloneModel;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.Executor;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StandaloneModelTest {

    private StandaloneModel model;

    @Mock
    private Context context;
    @Mock
    private RuntimePermissionHandler runtimePermissionHandler;
    @Mock
    private BusProvider.Bus bus;
    @Mock
    private TestDatabase testDatabase;
    @Mock
    private SharedPreferences sharedPreferences;
    @Mock
    private Executor executor;
    @Mock
    private TestInfoDao testInfoDao;

    @Captor
    private ArgumentCaptor<Runnable> runnableArgumentCaptor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        model = new StandaloneModel(context, bus, runtimePermissionHandler,
                testDatabase, sharedPreferences, executor);
    }

    @Test
    public void runAllTestTest() {
        //Arrange
        when(testDatabase.testInfoDao()).thenReturn(testInfoDao);
        //Act
        model.runAllTests();
        //Assert
        verify(executor).execute(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(testDatabase.testInfoDao()).updateAllTestStatus(TestInfoEntity.RUNNING);
        verify(testDatabase.testInfoDao()).resetAllTestTimeSpent();
    }

    @Test
    public void runTestTest() {
        //Arrange
        when(testDatabase.testInfoDao()).thenReturn(testInfoDao);
        TestInfoEntity testInfo= new TestInfoEntity("1", "TestTwo", "TestTwo",
                "Some Desc", "Type1", TestInfoEntity.PASS, 1, 0);
        //Act
        model.runTest(testInfo);
        //Assert
        verify(executor).execute(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(testDatabase.testInfoDao()).updateAllTestStatus(TestInfoEntity.IDLE);
        verify(testDatabase.testInfoDao()).resetAllTestTimeSpent();
        verify(testDatabase.testInfoDao()).updateStatusByTestId(testInfo.id, TestInfoEntity.RUNNING);
    }

    @Test
    public void permissionGranted() {
        /*Arrange*/
        when(runtimePermissionHandler.isPermissionGranted()).thenReturn(true);
        /*Assert*/
        assertTrue(model.isPermissionGranted());
    }

    @Test
    public void shouldRequestPermission() {
        /*Arrange*/
        when(runtimePermissionHandler.shouldRequestPermission()).thenReturn(false);
        /*Assert*/
        assertFalse(model.shouldRequestPermissions());
    }

    @Test
    public void requestPermission() {
        /*Act*/
        model.requestPermissions();
        /*Assert*/
        verify(runtimePermissionHandler).requestPermission();
    }
}