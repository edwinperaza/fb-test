package com.facebook.devices.aloha.diagnostics.testsresults;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.aloha.diagnostics.activities.TestsResultsActivity;
import com.facebook.devices.aloha.diagnostics.mvp.view.TestsResultsView;
import com.facebook.devices.utils.customviews.CustomToolbar;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class TestsResultsViewTest {

    private TestsResultsActivity activity;
    private TestsResultsView view;

    /*Bindings*/
    private CustomToolbar toolbar;
    private ImageView ivResultOverall;
    private TextView tvResultOverall;
    private LinearLayout llResultOverallContainer;
    private TextView tvSerialNumber;
    private TextView tvTotalFailedTest;
    private TextView tvTotalPassedTest;
    private TextView tvTotalTimeSpent;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, TestsResultsActivity.class);
        activity = Robolectric.buildActivity(TestsResultsActivity.class, intent).create().start().resume().get();
        view = new TestsResultsView(activity);

        /*Bindings*/
        toolbar = activity.findViewById(R.id.toolbar);
        llResultOverallContainer = activity.findViewById(R.id.ll_result_header_container);
        ivResultOverall = activity.findViewById(R.id.iv_overall_result);
        tvResultOverall = activity.findViewById(R.id.tv_overall_result);
        tvSerialNumber = activity.findViewById(R.id.tv_result_serial_number);
        tvTotalFailedTest = activity.findViewById(R.id.tv_total_failed_test_value);
        tvTotalPassedTest = activity.findViewById(R.id.tv_total_passed_test_value);
        tvTotalTimeSpent = activity.findViewById(R.id.tv_result_total_time_spent);
    }

    @Test
    public void init() {
        /*Arrange*/
        String titleText = activity.getString(R.string.multiple_test_result_toolbar_title);
        /*Assert*/
        assertEquals(titleText, toolbar.getTitle());
    }

    @Test
    public void setToolbarSuccessTest() {
        //Act
        view.setToolbarSuccess();
        //Assert
        assertEquals(ContextCompat.getColor(activity, R.color.light_sage), ((ColorDrawable) llResultOverallContainer.getBackground()).getColor());
        assertEquals(ContextCompat.getDrawable(activity, R.drawable.passed_bar), ivResultOverall.getDrawable());
        assertEquals(ContextCompat.getColor(activity, R.color.turtle_green), tvResultOverall.getCurrentTextColor());
        assertEquals(activity.getResources().getString(R.string.overall_test_passed), tvResultOverall.getText().toString());
    }

    @Test
    public void setToolbarFailTest() {
        //Act
        view.setToolbarFail();
        //Assert
        assertEquals(ContextCompat.getColor(activity, R.color.beige), ((ColorDrawable) llResultOverallContainer.getBackground()).getColor());
        assertEquals(ContextCompat.getDrawable(activity, R.drawable.failed_bar), ivResultOverall.getDrawable());
        assertEquals(ContextCompat.getColor(activity, R.color.warm_pink), tvResultOverall.getCurrentTextColor());
        assertEquals(activity.getResources().getString(R.string.overall_test_failed), tvResultOverall.getText().toString());
    }

    @Test
    public void setResumeTestValuesTest() {
        //Act
        view.setResumeTestValues("Serial", 0, 1, 1000);
        //Assert
        assertEquals("Serial", tvSerialNumber.getText().toString());
        assertEquals("0", tvTotalFailedTest.getText().toString());
        assertEquals("1", tvTotalPassedTest.getText().toString());
        assertEquals("00:00:01", tvTotalTimeSpent.getText().toString());
    }
}