package com.facebook.devices.ripley.diagnostics.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.facebook.devices.HwtpApplication;
import com.facebook.devices.R;
import com.facebook.devices.activities.ToolBarActivity;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.ripley.diagnostics.mvp.model.StandaloneModel;
import com.facebook.devices.ripley.diagnostics.mvp.presenter.StandalonePresenter;
import com.facebook.devices.ripley.diagnostics.mvp.view.StandaloneView;
import com.facebook.devices.ripley.diagnostics.service.HwtpServer;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;

public class StandaloneActivity extends ToolBarActivity {

    private StandalonePresenter presenter;
    private HwtpServerBroadcasterReceiver broadcasterReceiver;
    private RuntimePermissionHandler permissionHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quality);

        broadcasterReceiver = new HwtpServerBroadcasterReceiver();

        permissionHandler = new RuntimePermissionHandler(
                this,
                BusProvider.getInstance(),
                Manifest.permission.READ_PHONE_STATE);

        presenter = new StandalonePresenter(
                new StandaloneModel(
                        this,
                        BusProvider.getInstance(),
                        permissionHandler,
                        TestDatabase.getDatabase(this),
                        PreferenceManager.getDefaultSharedPreferences(this),
                        HwtpApplication.getDatabaseIO()),
                new StandaloneView(
                        this,
                        BusProvider.getInstance()));

        IntentFilter intentFilter = new IntentFilter(HwtpServer.HWTP_SERVER_SERVICE_FILTER);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcasterReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        BusProvider.register(presenter);
    }

    @Override
    protected void onStop() {
        BusProvider.unregister(presenter);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcasterReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    private class HwtpServerBroadcasterReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent resultIntent = new Intent(context, TestsResultsActivity.class);
            resultIntent.putExtras(intent);
            startActivity(resultIntent);
        }
    }
}