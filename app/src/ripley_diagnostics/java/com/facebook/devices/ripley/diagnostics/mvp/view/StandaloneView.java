package com.facebook.devices.ripley.diagnostics.mvp.view;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.facebook.devices.R;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.ripley.diagnostics.activities.StandaloneActivity;
import com.facebook.devices.ripley.diagnostics.utils.listeners.RecyclerViewListener;
import com.facebook.devices.ripley.diagnostics.utils.recyclers.GroupsRecyclerAdapter;
import com.facebook.hwtp.mvp.view.ViewBase;
import com.facebook.devices.bus.BusProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StandaloneView extends ViewBase<StandaloneActivity> {

    @BindView(R.id.recycler_tests) RecyclerView recyclerTests;

    private final BusProvider.Bus bus;
    private GroupsRecyclerAdapter adapter;
    private Handler uiHandler = new Handler(Looper.getMainLooper());

    public StandaloneView(StandaloneActivity activity, BusProvider.Bus bus) {
        super(activity);
        this.bus = bus;
        ButterKnife.bind(this, activity);

        ViewCompat.setNestedScrollingEnabled(recyclerTests, false);

        setupToolbar();
        setupRecycler();
    }

    public Handler getUIHandler() {
        return uiHandler;
    }

    private void setupToolbar() {
        getActivity().setToolbarTitle(getContext().getString(R.string.quality_toolbar_title));
        getActivity().setToolbarBackIconGone();
    }

    private void setupRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setAutoMeasureEnabled(true);
        recyclerTests.setLayoutManager(layoutManager);
        recyclerTests.setHasFixedSize(true);
        /* Adapter creation */
        adapter = new GroupsRecyclerAdapter(getContext(), new ArrayList<>(), new HashMap<>());
        adapter.setListener(new RecyclerViewListener<TestInfoEntity>() {
            @Override
            public void recyclerViewOnItemClickListener(View view, int position, TestInfoEntity test) {
                post(new ItemClickListenerEvent(position, test));
            }

            @Override
            public void recyclerViewOnItemLongClickListener(View view, int position, TestInfoEntity test) {
                /*Nothing to do here*/
            }
        });
        recyclerTests.setAdapter(adapter);
    }

    public void updateRecyclerItems(List<GroupTestsEntity> groupsEntity, Map<String, TestInfoEntity> testDefinitions) {
        if (adapter != null) {
            adapter.updateItems(groupsEntity, testDefinitions);
        }
    }

    @OnClick(R.id.card_run_all)
    public void onRunAllClicked() {
        post(new RunAllClickEvent());
    }

    protected void post(Object event) {
        bus.post(event);
    }

    /**
     * Static classes for BUS
     */
    public static class ItemClickListenerEvent {

        public int position;
        public TestInfoEntity test;

        public ItemClickListenerEvent(int position, TestInfoEntity test) {
            this.position = position;
            this.test = test;
        }
    }

    public static class RunAllClickEvent {
        /*Nothing to do here*/
    }
}