package com.facebook.devices.ripley.diagnostics.utils.listeners;


import android.view.View;

public interface RecyclerViewListener<T> {

    void recyclerViewOnItemClickListener(View view, int position, T object);

    void recyclerViewOnItemLongClickListener(View view, int position, T object);
}
