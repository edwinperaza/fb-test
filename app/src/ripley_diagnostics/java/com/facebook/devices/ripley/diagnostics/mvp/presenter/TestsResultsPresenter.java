package com.facebook.devices.ripley.diagnostics.mvp.presenter;

import com.facebook.devices.ripley.diagnostics.mvp.model.TestsResultsModel;
import com.facebook.devices.ripley.diagnostics.mvp.view.TestsResultsView;

import org.greenrobot.eventbus.Subscribe;

public class TestsResultsPresenter {

    private TestsResultsModel model;
    private TestsResultsView view;

    public TestsResultsPresenter(TestsResultsModel model, TestsResultsView view) {
        this.model = model;
        this.view = view;
    }

    public void onResume() {
        model.getTestResults();
    }

    @Subscribe
    public void onTestResultsEvent(TestsResultsModel.TestResultsEvent event) {
        view.getUIHandler().post(() -> {
            view.setResumeTestValues(event.serialNumber, event.totalFailedTest, event.totalPassedTest, event.totalTime);
            view.updateRecyclerItems(event.testInfoList);
            if (event.someTestsFailed) {
                view.setToolbarFail();
            } else {
                view.setToolbarSuccess();
            }
            view.drawDataMatrix(event.data);
        });
    }
}
