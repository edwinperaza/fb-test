package com.facebook.devices.ripley.diagnostics.mvp.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.R;
import com.facebook.devices.activities.TutorialTracker;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.ripley.diagnostics.service.HwtpServer;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.PlanXmlParser;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

public class StandaloneModel {

    private static String SHOULD_READ_PLAN = "should_read_plan";
    private Context context;
    private TestDatabase testDatabase;
    private SharedPreferences preferences;
    private PermissionHandler permissionHandler;
    private BusProvider.Bus bus;
    private boolean readPlanResult = false;
    private Executor executor;

    public StandaloneModel(Context context, BusProvider.Bus bus, PermissionHandler permissionHandler,
            TestDatabase testDatabase, SharedPreferences preferences, Executor executor) {
        this.context = context;
        this.bus = bus;
        this.permissionHandler = permissionHandler;
        this.testDatabase = testDatabase;
        this.preferences = preferences;
        this.executor = executor;
    }

    public List<GroupTestsEntity> getGroupsTestList() {
        return testDatabase.groupTestDao().getAllGroupTests();
    }

    public Map<String, TestInfoEntity> getAllTestInfo() {
        return testDatabase.testInfoDao().getAllTestInfo().stream()
                .collect(Collectors.toMap(TestInfoEntity::getId,
                        p -> p));
    }

    public void getAllTestAndGroupInfo() {
        executor.execute(() -> bus.post(new TestAndGroupEvent(getAllTestInfo(), getGroupsTestList())));
    }

    public void runAllTests() {
        executor.execute(() -> {
            testDatabase.testInfoDao().updateAllTestStatus(TestInfoEntity.RUNNING);
            testDatabase.testInfoDao().resetAllTestTimeSpent();
            testRunner();
        });
    }

    public void runTest(TestInfoEntity test) {
        executor.execute(() -> {
            testDatabase.testInfoDao().updateAllTestStatus(TestInfoEntity.IDLE);
            testDatabase.testInfoDao().resetAllTestTimeSpent();
            testDatabase.testInfoDao().updateStatusByTestId(test.id, TestInfoEntity.RUNNING);
            testRunner();
        });
    }

    private void testRunner() {
        Intent intent = new Intent(context, HwtpServer.class);
        intent.putExtra(HwtpServer.ACTION_PARAM, HwtpServer.RUN_TESTS);
        context.startService(intent);
    }

    public boolean shouldReadPlan() {
        return preferences.getBoolean(SHOULD_READ_PLAN, true);
    }

    public boolean readPlan() {
        executor.execute(() -> {
            readPlanResult = PlanXmlParser.getPlanTests(
                    testDatabase,
                    new InputStreamReader(context.getResources().openRawResource(R.raw.plan)),
                    BuildConfig.ENVIRONMENT);

            if (readPlanResult) {
                preferences.edit().putBoolean(SHOULD_READ_PLAN, false).apply();
            }

        });
        return readPlanResult;
    }

    public void resetTutorialTracker(){
        TutorialTracker.of(context).reset();
    }

    //Methods related to runtime permissions
    public boolean isPermissionGranted() {
        return permissionHandler.isPermissionGranted();
    }

    public boolean shouldRequestPermissions() {
        return permissionHandler.shouldRequestPermission();
    }

    public void requestPermissions() {
        permissionHandler.requestPermission();
    }

    public static class TestAndGroupEvent {

        public Map<String, TestInfoEntity> testInfoEntityMap;
        public List<GroupTestsEntity> groupTestList;

        public TestAndGroupEvent(Map<String, TestInfoEntity> testInfoEntityMap,
                List<GroupTestsEntity> groupTestList) {
            this.testInfoEntityMap = testInfoEntityMap;
            this.groupTestList = groupTestList;
        }
    }
}
