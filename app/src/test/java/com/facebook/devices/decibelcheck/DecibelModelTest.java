package com.facebook.devices.decibelcheck;


import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.DecibelModel;
import com.facebook.devices.mvp.model.DecibelModel.OnMediaTimeUp;
import com.facebook.devices.mvp.model.DecibelModel.OnMediaTimeUpFail;
import com.facebook.devices.mvp.model.DecibelModel.OnValidMeasurementDecibel;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.AudioRecorder.AudioRecorderListener;
import com.facebook.devices.bus.BusProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.facebook.devices.mvp.model.DecibelModel.STATE_INIT;
import static com.facebook.devices.mvp.model.DecibelModel.STATE_MEASUREMENT;
import static com.facebook.devices.mvp.model.DecibelModel.STATE_MEDIA;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class DecibelModelTest {

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private AudioRecorder audioRecorder;

    @Mock
    private AudioRecorderListener audioRecorderListener;

    private DecibelModel model;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = new DecibelModel(bus, permissionHandler,
                12, 20,
                5000, audioRecorder);
    }

    @Test
    public void getImageToShowTest() {
        //Arrange
        Double decibels = 20d;
        int valueExpected = 4;
        //Assert
        assertEquals(valueExpected, model.getLevel(decibels));
    }

    @Test
    public void getImageToShowOneTest() {
        //Arrange
        Double decibels = 1d;
        int valueExpected = 1;
        //Assert
        assertEquals(valueExpected, model.getLevel(decibels));
    }

    @Test
    public void onMediaTimeUpTest() {
        //Arrange
        double media = 2;
        double differenceDb = 20;
        //Act
        OnMediaTimeUp event = new OnMediaTimeUp(media, differenceDb);
        //Assert
        assertEquals(event.getMedia(), media);
        assertEquals(event.getDifferenceDb(), differenceDb);
    }

    @Test
    public void setInitStateTest() {
        //Act
        model.setInitState();
        //Assert
        assertEquals(model.getActualState(), STATE_INIT);
    }

    @Test
    public void setMediaStateTest() {
        //Act
        model.setMediaState();
        //Assert
        assertEquals(model.getActualState(), STATE_MEDIA);
    }

    @Test
    public void setMeasurementStateTest() {
        //Act
        model.setMeasurementState();
        //Assert
        assertEquals(model.getActualState(), STATE_MEASUREMENT);
    }

    @Test
    public void clearMediaEnvironmentValuesTest() {
        //Act
        model.clearMediaEnvironmentValues();
        //Assert
        assertEquals(0.0, model.getTotalDecibels());
        assertEquals(0, model.getCount());
    }

    @Test
    public void shouldRequestPermissionsTest() {
        //Arrange
        when(permissionHandler.shouldRequestPermission()).thenReturn(false);
        //Assert
        assertFalse(model.shouldRequestPermissions());
    }

    @Test
    public void calculateEnvironmentMediaTest() {
        //Arrange
        model.setTotalDecibels(20);
        model.setCount(10);
        //Act
        model.calculateEnvironmentMedia();
        //Assert
        verify(bus).post(any(OnMediaTimeUp.class));
    }

    @Test
    public void calculateEnvironmentMediaFailTest() {
        //Arrange
        model.setTotalDecibels(0);
        model.setCount(0);
        //Act
        model.calculateEnvironmentMedia();
        //Assert
        verify(bus).post(any(OnMediaTimeUpFail.class));
    }

    @Test
    public void updateCurrentDecibelsTest() {
        //Arrange
        double decibels = 20;
        model.setMediaState();
        //Act
        model.updateCurrentDecibelsOrValidate(decibels);
        //Assert
        assertEquals(decibels, model.getTotalDecibels());
    }

    @Test
    public void validateDecibelsTest() {
        //Arrange
        double decibels = 80;
        model.setMeasurementState();
        //Act
        model.updateCurrentDecibelsOrValidate(decibels);
        //Assert
        verify(bus).post(any(OnValidMeasurementDecibel.class));
    }

    @Test
    public void startTest() {
        //Act
        model.startRecording(audioRecorderListener);
        //Assert
        assertEquals(true, model.isRecording());
    }

    @Test
    public void isRecordingTest() {
        //Arrange
        model.setRecording(true);
        //Act
        assertEquals(true, model.isRecording());
    }
}
