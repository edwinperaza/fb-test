package com.facebook.devices.decibelcheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.DecibelActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.DecibelView;
import com.facebook.devices.mvp.view.DecibelView.OnButtonFailurePressed;
import com.facebook.devices.mvp.view.DecibelView.OnButtonRetryPressed;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class DecibelViewTest {

    private DecibelActivity activity;
    private DecibelView view;

    @Mock
    private BusProvider.Bus bus;

    private DecimalFormat df;

    /*UI stuffs*/
    private FloatingTutorialView floatingTutorialView;
    private RelativeLayout decibelLayout;
    private TextView decibelEnvironmentValue;
    private TextView decibelGoalValue;
    private TextView decibelCurrentValue;
    private ImageView ivDecibelOne;
    private ImageView ivDecibelTwo;
    private ImageView ivDecibelThree;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, DecibelActivity.class);
        activity = Robolectric.buildActivity(DecibelActivity.class, intent).create().start().resume().get();

        view = new DecibelView(activity, bus);

        df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        /*Utils*/
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        decibelLayout = activity.findViewById(R.id.cl_decibel_layout);
        decibelEnvironmentValue = activity.findViewById(R.id.tv_decibel_environment_value);
        decibelGoalValue = activity.findViewById(R.id.tv_decibel_goal_value);
        decibelCurrentValue = activity.findViewById(R.id.tv_decibel_current_value);
        ivDecibelOne = activity.findViewById(R.id.iv_decibel_01);
        ivDecibelTwo = activity.findViewById(R.id.iv_decibel_02);
        ivDecibelThree = activity.findViewById(R.id.iv_decibel_03);
    }

    @Test
    public void init() {
        assertNotNull(view.getUIHandler());
    }

    @Test
    public void showTutorialButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialTest() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.decibel_tutorial_dialog);
        String option = activity.getString(R.string.ok_base);

        /*Act*/
        view.onFloatingTutorialClicked();

        /*Assert*/
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(alertDialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), option);
    }

    @Test
    public void showEnvironmentMeasurementInitDialogTest() {
        //Arrange
        String message = activity.getString(R.string.decibel_init_dialog_reference_value);
        String failText = activity.getString(R.string.fail_base);

        //Act
        view.showEnvironmentMeasurementInitDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), failText);
    }

    @Test
    public void showEnvironmentMeasurementInitDialogOptionOneTest() {
        //Act
        view.showEnvironmentMeasurementInitDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showEnvironmentMeasurementFailDialogTest() {
        //Arrange
        String message = activity.getString(R.string.decibel_environment_value_fail);
        String failText = activity.getString(R.string.fail_base);
        String tryAgain = activity.getString(R.string.try_again_base);

        //Act
        view.showEnvironmentMeasurementFailDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), tryAgain);
        assertEquals(bannerView.getBannerTextOptionTwo(), failText);
    }

    @Test
    public void showEnvironmentMeasurementFailDialogOptionOneTest() {
        //Act
        view.showEnvironmentMeasurementFailDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(OnButtonRetryPressed.class));
    }

    @Test
    public void showEnvironmentMeasurementFailDialogOptionTwoTest() {
        //Act
        view.showEnvironmentMeasurementFailDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        //Assert
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showMeasurementDialogTest() {
        //Arrange
        String message = activity.getString(R.string.decibel_check_measurement_dialog_title);
        String failText = activity.getString(R.string.fail_base);

        //Act
        view.showMeasurementDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), failText);
    }

    @Test
    public void showMeasurementDialogOptionOneTest() {
        //Act
        view.showMeasurementDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void permissionAcceptedTest() {
        //Act
        view.permissionAccepted();
        //Assert
        assertEquals(decibelLayout.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showEnvironmentMediaResultTest() {
        //Arrange
        double value = 20;
        double goal = 50;
        String valueExpected = activity.getString(R.string.decibel_value, df.format(value));
        String goalExpected = activity.getString(R.string.decibel_value, df.format(goal));
        //Act
        view.showEnvironmentMediaResult(value, goal);
        //Assert
        assertEquals(valueExpected, decibelEnvironmentValue.getText().toString());
        assertEquals(goalExpected, decibelGoalValue.getText().toString());
    }

    @Test
    public void showDecibelTest() {
        //Arrange
        int image = 2;
        double decibels = 30;
        String decibelsExpected = activity.getString(R.string.decibel_value, df.format(decibels));
        //Act
        view.showDecibel(image, decibels);
        //Assert
        assertEquals(decibelsExpected, decibelCurrentValue.getText().toString());
        assertEquals(ivDecibelOne.getVisibility(), View.VISIBLE);
        assertEquals(ivDecibelTwo.getVisibility(), View.VISIBLE);
        assertEquals(ivDecibelThree.getVisibility(), View.INVISIBLE);
    }
}