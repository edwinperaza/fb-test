package com.facebook.devices.decibelcheck;

import android.os.Handler;

import com.facebook.devices.mvp.model.DecibelModel;
import com.facebook.devices.mvp.model.DecibelModel.OnMediaTimeUpFail;
import com.facebook.devices.mvp.presenter.DecibelPresenter;
import com.facebook.devices.mvp.view.DecibelView;
import com.facebook.devices.mvp.view.DecibelView.OnButtonFailurePressed;
import com.facebook.devices.permission.RuntimePermissionHandler.PermissionDeniedEvent;
import com.facebook.devices.permission.RuntimePermissionHandler.PermissionGrantedEvent;
import com.facebook.devices.utils.AudioRecorder.AudioRecorderListener;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.mvp.model.DecibelModel.OnValidMeasurementDecibel;
import static com.facebook.devices.mvp.model.DecibelModel.STATE_INIT;
import static com.facebook.devices.mvp.model.DecibelModel.STATE_MEASUREMENT;
import static com.facebook.devices.mvp.model.DecibelModel.STATE_MEDIA;
import static com.facebook.devices.mvp.view.DecibelView.OnButtonRetryPressed;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DecibelPresenterTest {

    private DecibelPresenter presenter;

    @Mock
    private DecibelModel model;

    @Mock
    private DecibelView view;

    @Mock
    private Handler mockHandler;

    @Mock
    private AudioRecorderListener audioRecorderListener;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getUIHandler()).thenReturn(mockHandler);
        presenter = new DecibelPresenter(model, view);
    }

    @Test
    public void initTest() {
        //Assert
        verify(view).showTutorialButton();
    }

    @Test
    public void onResumeTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(model).requestPermissions();
        verify(model, never()).startRecording(audioRecorderListener);
    }

    @Test
    public void onResumeAndPermissionGrantedAndStateInitTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_INIT);
        //Act
        presenter.onResume();
        //Assert
        verify(view).permissionAccepted();
        verify(view).showEnvironmentMeasurementInitDialog();
        verify(model).setMediaState();
    }

    @Test
    public void onResumeAndPermissionGrantedStateMediaAndIsRecordingTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEDIA);
        when(model.isRecording()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(view, never()).permissionAccepted();
        verify(model).clearMediaEnvironmentValues();
        verify(view.getUIHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view, never()).showMeasurementDialog();
        verify(model, never()).startRecording(audioRecorderListener);
    }

    @Test
    public void onResumeAndPermissionGrantedStateMeasurementAndIsRecordingTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEASUREMENT);
        when(model.isRecording()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(view, never()).permissionAccepted();
        verify(model, never()).clearMediaEnvironmentValues();
        verify(view).showMeasurementDialog();
        verify(model, never()).startRecording(audioRecorderListener);
    }

    @Test
    public void onResumeAndPermissionGrantedStateMediaAndNotRecordingTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEDIA);
        when(model.isRecording()).thenReturn(false);
        float amplitude = 10;
        //Act
        presenter.init();
        //Assert
        ArgumentCaptor<AudioRecorderListener> listenerCaptor = ArgumentCaptor.forClass(AudioRecorderListener.class);
        verify(model).startRecording(listenerCaptor.capture());
        listenerCaptor.getValue().onAmplitudeUpdated(amplitude);
        verify(model).updateAmplitude(amplitude);

        verify(view.getUIHandler()).post(any(Runnable.class));
        verify(view.getUIHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view, never()).showMeasurementDialog();
    }

    @Test
    public void onResumeAndPermissionGrantedStateMeasurementAndNotRecordingTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEASUREMENT);
        when(model.isRecording()).thenReturn(false);
        float amplitude = 10;
        //Act
        presenter.init();
        //Assert
        verify(view, never()).permissionAccepted();

        ArgumentCaptor<AudioRecorderListener> listenerCaptor = ArgumentCaptor.forClass(AudioRecorderListener.class);
        verify(model).startRecording(listenerCaptor.capture());
        listenerCaptor.getValue().onAmplitudeUpdated(amplitude);
        verify(model).updateAmplitude(amplitude);

        verify(view.getUIHandler()).post(any(Runnable.class));
        verify(view.getUIHandler(), never()).postDelayed(any(Runnable.class), anyLong());
        verify(view).showMeasurementDialog();
    }

    @Test
    public void onResumeAndPermissionGrantedStateMeasurementNotRecordingAndRunnableTest() {
        //Arrange
        double decibel = 100d;
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEASUREMENT);
        when(model.isRecording()).thenReturn(false);
        when(model.getDecibel()).thenReturn(decibel);
        //Act
        presenter.init();
        //Assert
        verify(view, never()).permissionAccepted();

        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(view.getUIHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();

        verify(view.getUIHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view.getUIHandler(), times(2)).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(view).showDecibel(anyInt(), anyDouble());
        verify(model).updateCurrentDecibelsOrValidate(decibel);

        verify(view).showMeasurementDialog();
    }

    @Test
    public void onPauseTest() {
        //Act
        presenter.onPause();
        //Assert
        verify(view.getUIHandler(), times(2)).removeCallbacks(any(Runnable.class));
    }

    @Test
    public void onMediaTimeUpTest() {
        //Arrange
        when(model.isRecording()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_MEASUREMENT);
        double media = 20d;
        double differenceDb = 10d;
        DecibelModel.OnMediaTimeUp event = new DecibelModel.OnMediaTimeUp(media, differenceDb);
        //Act
        presenter.onMediaTimeUp(event);
        //Assert
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(view.getUIHandler()).removeCallbacks(any(Runnable.class));

        verify(mockHandler).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();

        verify(view).showEnvironmentMediaResult(event.getMedia(), event.getDifferenceDb());
        verify(model).setMeasurementState();
        verify(view).showMeasurementDialog();
    }

    @Test
    public void onMediaTimeUpFailTest() {
        //Arrange
        OnMediaTimeUpFail event = new OnMediaTimeUpFail();
        //Act
        presenter.onMediaTimeUpFail(event);
        //Assert
        verify(view).showEnvironmentMeasurementFailDialog();
    }

    @Test
    public void onValidNearDecibelTest() {
        //Arrange
        OnValidMeasurementDecibel event = new OnValidMeasurementDecibel();
        //Act
        presenter.onValidNearDecibel(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onButtonFailurePressedTest() {
        //Arrange
        OnButtonFailurePressed event = new OnButtonFailurePressed();
        //Act
        presenter.onButtonFailurePressed(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onButtonRetryPressedTest() {
        //Arrange
        when(model.getActualState()).thenReturn(STATE_INIT);
        OnButtonRetryPressed event = new OnButtonRetryPressed();
        //Act
        presenter.onButtonRetryPressed(event);
        //Assert
        verify(view).permissionAccepted();
        verify(view).showEnvironmentMeasurementInitDialog();
        verify(model).setMediaState();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        PermissionDeniedEvent event = new PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResultShouldRequestTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(true);
        PermissionDeniedEvent event = new PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model, never()).fail("User denied permissions");
        verify(view, never()).finishActivity();
    }

    @Test
    public void onRequestPermissionResultGrantedTest() {
        //Arrange
        PermissionGrantedEvent event = new PermissionGrantedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(view).permissionAccepted();
    }
}