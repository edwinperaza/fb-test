package com.facebook.devices;

import android.widget.TextView;

import org.robolectric.shadows.ShadowAlertDialog;

public class ShadowMaterialDialog {

    private ShadowAlertDialog shadowAlertDialog;
    private TextView tvTitle;
    private TextView tvMessage;

    public ShadowMaterialDialog(ShadowAlertDialog shadowAlertDialog){
        this.shadowAlertDialog = shadowAlertDialog;
        tvTitle = shadowAlertDialog.getView().findViewById(R.id.tv_material_dialog_title);
        tvMessage = shadowAlertDialog.getView().findViewById(R.id.tv_material_dialog_message);
    }

    public String getTitle(){
        return tvTitle.getText().toString();
    }

    public String getMessage(){
        return tvMessage.getText().toString();
    }
}
