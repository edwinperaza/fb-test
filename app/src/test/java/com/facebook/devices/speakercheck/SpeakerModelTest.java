package com.facebook.devices.speakercheck;


import android.app.Activity;
import android.content.Intent;
import android.media.AudioFormat;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.SpeakerActivity;
import com.facebook.devices.mvp.model.SpeakerModel;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class SpeakerModelTest {

    private SpeakerModel model;
    private Activity activity;
    private BusProvider.Bus bus;

    @Mock
    private VolumeManager volumeManager;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, SpeakerActivity.class);
        activity = Robolectric.buildActivity(SpeakerActivity.class, intent).create().start().resume().get();
        bus = BusProvider.getInstance();

        model = new SpeakerModel(440, SpeakerModel.MONO, volumeManager);
    }

    @Test
    public void checkInit() {
        //Assert
        assertThat(model.getQuestionState(), equalTo(SpeakerModel.QUESTION_STATE_1));
        assertThat(model.getSampleRate(), equalTo(44100));
        assertThat(model.getPCMEncoding(), equalTo(AudioFormat.ENCODING_PCM_16BIT));
        assertThat(model.getChannelConfig(), equalTo(AudioFormat.CHANNEL_OUT_MONO));
    }

    @Test
    public void setQuestionStateCheck() {
        //Arrange
        int questionState = SpeakerModel.QUESTION_STATE_2;
        //Act
        model.setQuestionState(questionState);
        //Assert
        assertThat(model.getQuestionState(), equalTo(questionState));
    }
}
