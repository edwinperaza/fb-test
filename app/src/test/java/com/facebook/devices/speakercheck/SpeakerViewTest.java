package com.facebook.devices.speakercheck;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.SpeakerActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.SpeakerView;
import com.facebook.devices.mvp.view.SpeakerView.DialogOneFailEvent;
import com.facebook.devices.mvp.view.SpeakerView.DialogOnePassEvent;
import com.facebook.devices.mvp.view.SpeakerView.DialogTwoFailEvent;
import com.facebook.devices.mvp.view.SpeakerView.DialogTwoPassEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.WaveGraphView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class SpeakerViewTest {

    private SpeakerActivity activity;
    private SpeakerView view;

    @Mock
    private BusProvider.Bus bus;

    /*UI stuffs*/
    private WaveGraphView waveView;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, SpeakerActivity.class);

        activity = Robolectric.buildActivity(SpeakerActivity.class, intent).create().start().resume().get();
        view = new SpeakerView(activity, bus);

        /*Utils*/
        waveView = activity.findViewById(R.id.wave_view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void showTutorialTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.speaker_tutorial_text);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(ShadowAlertDialog.getLatestAlertDialog()));
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        assertNotNull(shadowAlertDialog);
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void setAudioSamplesTest() {
        //Arrange
        short[] samples = new short[]{2, 3, 4, 1, 12, 3, 2};
        //Act
        view.drawAudioSample(samples);
        //Assert
        assertThat(waveView.getSamples(), equalTo(samples));
    }

    @Test
    public void showTutorialButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showDialogOneTest() {
        //Arrange
        String message = activity.getString(R.string.speaker_test_sound_message);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);

        //Act
        view.showDialogOne();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showDialogOneOptionOneTest() {
        //Act
        view.showDialogOne();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogOnePassEvent.class));
    }

    @Test
    public void showDialogOneOptionTwoTest() {
        //Act
        view.showDialogOne();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(any(DialogOneFailEvent.class));
    }

    @Test
    public void showDialogTwoTest() {
        //Arrange
        String message = activity.getString(R.string.speaker_test_distortion_message);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);

        //Act
        view.showDialogTwo();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showDialogTwoOptionOneTest() {
        //Act
        view.showDialogTwo();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogTwoPassEvent.class));
    }

    @Test
    public void showDialogTwoOptionTwoTest() {
        //Act
        view.showDialogTwo();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(any(DialogTwoFailEvent.class));
    }

}