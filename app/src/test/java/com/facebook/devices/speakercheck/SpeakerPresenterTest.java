
package com.facebook.devices.speakercheck;


import com.facebook.devices.activities.SpeakerActivity;
import com.facebook.devices.mvp.model.SpeakerModel;
import com.facebook.devices.mvp.presenter.SpeakerPresenter;
import com.facebook.devices.mvp.view.SpeakerView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpeakerPresenterTest {

    private SpeakerPresenter presenter;
    private SpeakerModel model;
    private SpeakerView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = mock(SpeakerModel.class);
        view = mock(SpeakerView.class);

        presenter = new SpeakerPresenter(model, view);
    }

    @Test
    public void onResume() {
        //Act
        presenter.onResume();
        //Assert
        verify(model).play();
    }

    @Test
    public void onPaused() {
        //Act
        presenter.onPaused();
        //Assert
        verify(model).stop();
    }

    @Test
    public void onDialogOnePassEvent() {
        //Act
        presenter.onDialogOnePassEvent(null);
        //Assert
        verify(model).setQuestionState(SpeakerModel.QUESTION_STATE_2);
        verify(view).showDialogTwo();
    }

    @Test
    public void onDialogOneFailEvent() {
        //Act
        presenter.onDialogOneFailEvent(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogTwoPassEvent() {
        //Act
        presenter.onDialogTwoPassEvent(null);
        //Assert
        verify(model).stop();
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogTwoFailEvent() {
        //Act
        presenter.onDialogTwoFailEvent(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }
}
