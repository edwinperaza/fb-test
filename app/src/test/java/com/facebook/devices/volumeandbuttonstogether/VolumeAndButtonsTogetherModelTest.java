package com.facebook.devices.volumeandbuttonstogether;


import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.VolumeButtonsAndSoundActivity;
import com.facebook.devices.mvp.model.VolumeButtonsAndSoundModel;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VolumeAndButtonsTogetherModelTest {

    private Context context;
    private BusProvider.Bus bus;

    private VolumeButtonsAndSoundModel model;
    private AudioManager audioManager;
    private int specificStream = AudioManager.STREAM_RING;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, VolumeButtonsAndSoundActivity.class);
        context = Robolectric.buildActivity(VolumeButtonsAndSoundActivity.class, intent).create().start().resume().get();
        bus = BusProvider.getInstance();

        MockitoAnnotations.initMocks(this);

        model = new VolumeButtonsAndSoundModel(context);
        model.init();

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Test
    public void verifyStream() {
        //Assert
        assertEquals(model.getSpecificStream(), specificStream);
    }

    @Test
    public void raiseVolume() {
        //Arrange
        int previousVolume = audioManager.getStreamVolume(specificStream);
        //Act
        model.volumeUp();
        //Assert
        assertTrue(previousVolume < audioManager.getStreamVolume(specificStream));
    }

    @Test
    public void lowerVolume() {
        //Arrange
        int previousVolume = audioManager.getStreamVolume(specificStream);
        //Act
        model.volumeDown();
        //Assert
        assertTrue(previousVolume > audioManager.getStreamVolume(specificStream));
    }

    @Test
    public void checkForNextStepNormal() {
        //Assert
        assertTrue(model.checkForNextStep());
    }

    @Test
    public void checkForNextStepModeChange() {
        //Arrange
        int previousMode = model.getCurrentMode();
        //Act
        for (int i = 0; i < model.MAX_TIME_PRESSING; i++) {
            model.checkForNextStep();
        }
        //Assert
        assertTrue(previousMode != model.getCurrentMode());
    }
}
