package com.facebook.devices.volumeandbuttonstogether;

import android.view.KeyEvent;

import com.facebook.devices.mvp.model.VolumeButtonsAndSoundModel;
import com.facebook.devices.mvp.presenter.VolumeButtonsAndSoundPresenter;
import com.facebook.devices.mvp.view.VolumeButtonsAndSoundView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.mvp.model.VolumeButtonsAndSoundModel.MODE_UP;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VolumeAndButtonsTogetherPresenterTest {

    private VolumeButtonsAndSoundPresenter presenter;

    @Mock
    private VolumeButtonsAndSoundModel model;

    @Mock
    private VolumeButtonsAndSoundView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new VolumeButtonsAndSoundPresenter(model, view);
    }

    @Test
    public void setUp() {
        //Assert
        verify(view).showQuestionUp();
        verify(view).showTutorialButton(true);
    }
    @Test
    public void onResumed() {
        //Act
        presenter.onResumed();
        //Assert
        verify(model).init();
        verify(view).showTouchCount(eq(model.getTouchedTimes()));
    }

    @Test
    public void onKeyDownUpModeUpAndNextStepTrueTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_UP;
        int count = 1;
        when(model.incrementTouchCount()).thenReturn(count);
        when(model.isModeUp()).thenReturn(true);
        when(model.checkForNextStep()).thenReturn(true);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(view).showQuestionDown();
        verify(model).volumeUp();
        verify(model).playSound();
        verify(model, never()).volumeDown();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onKeyDownUpModeUpFalseAndNextStepTrueTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_UP;
        int count = 1;
        when(model.incrementTouchCount()).thenReturn(count);
        when(model.isModeUp()).thenReturn(false);
        when(model.checkForNextStep()).thenReturn(true);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(view, never()).showQuestionDown();
        verify(model).volumeUp();
        verify(model).playSound();
        verify(model, never()).volumeDown();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onKeyDownUpModeUpTrueAndNextStepFalseTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_UP;
        int count = 1;
        when(model.incrementTouchCount()).thenReturn(count);
        when(model.isModeUp()).thenReturn(true);
        when(model.checkForNextStep()).thenReturn(false);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(view, never()).showQuestionDown();
        verify(model).volumeUp();
        verify(model).playSound();
        verify(model, never()).volumeDown();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onKeyDownUpModeUpFalseAndNextStepFalseTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_UP;
        int count = 1;
        when(model.incrementTouchCount()).thenReturn(count);
        when(model.isModeUp()).thenReturn(false);
        when(model.checkForNextStep()).thenReturn(false);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(view, never()).showQuestionDown();
        verify(model).volumeUp();
        verify(model).playSound();
        verify(model, never()).volumeDown();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onKeyDownDownModeUpAndNextStepTrueTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_DOWN;
        int count = 1;
        when(model.decrementTouchCount()).thenReturn(count);
        when(model.isModeDown()).thenReturn(true);
        when(model.checkForNextStep()).thenReturn(true);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(model).pass();
        verify(view).finishActivity();
        verify(model).volumeDown();
        verify(model).playSound();
        verify(model, never()).volumeUp();
        verify(view, never()).showQuestionDown();
    }

    @Test
    public void onKeyDownDownModeUpFalseAndNextStepTrueTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_DOWN;
        int count = 1;
        when(model.decrementTouchCount()).thenReturn(count);
        when(model.isModeDown()).thenReturn(false);
        when(model.checkForNextStep()).thenReturn(true);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
        verify(model).volumeDown();
        verify(model).playSound();
        verify(model, never()).volumeUp();
        verify(view, never()).showQuestionDown();
    }

    @Test
    public void onKeyDownDownModeUpFalseAndNextStepFalseTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_DOWN;
        int count = 1;
        when(model.decrementTouchCount()).thenReturn(count);
        when(model.isModeDown()).thenReturn(false);
        when(model.checkForNextStep()).thenReturn(false);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
        verify(model).volumeDown();
        verify(model).playSound();
        verify(model, never()).volumeUp();
        verify(view, never()).showQuestionDown();
    }

    @Test
    public void onKeyDownDownModeUpTrueAndNextStepFalseTest() {
        //Arrange
        int keycode = KeyEvent.KEYCODE_VOLUME_DOWN;
        int count = 1;
        when(model.decrementTouchCount()).thenReturn(count);
        when(model.isModeDown()).thenReturn(true);
        when(model.checkForNextStep()).thenReturn(false);
        //Act
        presenter.onKeyDown(keycode);
        //Assert
        verify(view).showTouchCount(count);
        verify(model).volumeDown();
        verify(model).playSound();
        verify(model, never()).volumeUp();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
        verify(view, never()).showQuestionDown();
    }

    @Test
    public void onKeyDownOther() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_MUTE;
        when(model.getCurrentMode()).thenReturn(MODE_UP);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model, never()).volumeUp();
        verify(model, never()).volumeDown();
    }

    @Test
    public void onDestroyTest() {
        //Act
        presenter.onDestroy();
        //Assert
        verify(model).resetDefaultValues();
    }

    @Test
    public void onFailureButtonEventTest() {
        //Act
        presenter.onFailureButtonEvent(new VolumeButtonsAndSoundView.FailureButtonEvent());
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

}