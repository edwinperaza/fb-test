package com.facebook.devices.volumeandbuttonstogether;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.VolumeButtonsAndSoundActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.VolumeButtonsAndSoundView;
import com.facebook.devices.utils.CenteredImageSpan;
import com.facebook.devices.utils.ShadowView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.view.VolumeButtonsAndSoundView.*;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VolumeAndButtonsTogetherViewTest {

    private VolumeButtonsAndSoundActivity activity;
    private VolumeButtonsAndSoundView view;

    private ImageView volumeIcon;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, VolumeButtonsAndSoundActivity.class);

        activity = Robolectric.buildActivity(VolumeButtonsAndSoundActivity.class, intent).create().start().resume().get();
        view = new VolumeButtonsAndSoundView(activity, bus);

        volumeIcon = activity.findViewById(R.id.iv_volume_icon);
    }

    @Test
    public void showTouchCountTest() {
        view.showTouchCount(0);
        assertThat(R.drawable.vol_sound_off, is(equalTo(Shadows.shadowOf(volumeIcon.getDrawable()).getCreatedFromResId())));
        view.showTouchCount(1);
        assertThat(R.drawable.vol_sound_level_1, is(equalTo(Shadows.shadowOf(volumeIcon.getDrawable()).getCreatedFromResId())));
        view.showTouchCount(2);
        assertThat(R.drawable.vol_sound_level_2, is(equalTo(Shadows.shadowOf(volumeIcon.getDrawable()).getCreatedFromResId())));
        view.showTouchCount(3);
        assertThat(R.drawable.vol_sound_level_3, is(equalTo(Shadows.shadowOf(volumeIcon.getDrawable()).getCreatedFromResId())));
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String positiveText = activity.getString(R.string.ok_base);
        CenteredImageSpan imagePlusSpan = new CenteredImageSpan(activity, R.drawable.plus_button);
        CenteredImageSpan imageLessSpan = new CenteredImageSpan(activity, R.drawable.less_button);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_tutorial_sound_tutorial_message));
        spannableString.setSpan(imagePlusSpan, 69, 70, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(imageLessSpan, 72, 73, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertEquals(shadowAlertDialog.getTitle().toString(), title);
        assertEquals(shadowAlertDialog.getMessage().toString(), spannableString.toString());
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showQuestionUpTest() {
        //Arrange
        CenteredImageSpan imageSpan = new CenteredImageSpan(activity, R.drawable.plus_button);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_up_sound_question_message));
        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showQuestionUp();

        //Assert
        //TODO Test spannable String
        //assertEquals(spannableString.toString(), messageDialog.getText());
        TextView optionOne = ShadowView.getCurrentBanner(view).getBannerOptionOneButton();
        assertEquals(optionOne.getText(), negativeText);
    }

    @Test
    public void showQuestionUpOptionOneTest() {
        //Act
        view.showQuestionUp();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        verify(bus).post(any(FailureButtonEvent.class));
    }

    @Test
    public void showQuestionDownTest() {
        //Arrange
        CenteredImageSpan imageSpan = new CenteredImageSpan(activity, R.drawable.less_button);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_down_sound_question_message));
        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showQuestionDown();

        //Assert
        //TODO Test spannable String
        //assertEquals(spannableString.toString(), messageDialog.getText());
        TextView optionOne = ShadowView.getCurrentBanner(view).getBannerOptionOneButton();
        assertEquals(optionOne.getText(), negativeText);
    }

    @Test
    public void showQuestionDownOptionOneTest() {
        //Act
        view.showQuestionDown();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        verify(bus).post(any(FailureButtonEvent.class));
    }
}