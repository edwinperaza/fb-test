package com.facebook.devices.wificheck;

import android.os.Handler;

import com.facebook.devices.mvp.model.WifiModel;
import com.facebook.devices.mvp.model.WifiModel.WifiModelCallback;
import com.facebook.devices.mvp.model.WifiModel.WifiTransferProgress;
import com.facebook.devices.mvp.presenter.WifiPresenter;
import com.facebook.devices.mvp.view.WifiView;
import com.facebook.devices.mvp.view.WifiView.OnOkResultClicked;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WifiCheckPresenterTest {

    private WifiPresenter presenter;

    @Mock
    private WifiView view;
    @Mock
    private WifiModel model;

    @Mock
    private Handler mockHandler;

    @Captor
    private ArgumentCaptor<WifiTransferProgress> wifiTransferCallbackCaptor;

    @Captor
    private ArgumentCaptor<WifiModelCallback> wifiModelCallbackCaptor;

    @Captor
    private ArgumentCaptor<Runnable> listenerCaptor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getUiHandler()).thenReturn(mockHandler);
        presenter = new WifiPresenter(model, view);
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        RuntimePermissionHandler.PermissionDeniedEvent event = new RuntimePermissionHandler.PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResultShouldRequestTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(true);
        RuntimePermissionHandler.PermissionDeniedEvent event = new RuntimePermissionHandler.PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).requestPermissions();
        verify(model, never()).fail("User denied permissions");
        verify(view, never()).finishActivity();
    }

    @Test
    public void onRequestPermissionResultPermissionGrantedIsBroadcastTest() {
        //Arrange
        when(model.isBroadcastRegistered()).thenReturn(false);
        RuntimePermissionHandler.PermissionGrantedEvent event = new RuntimePermissionHandler.PermissionGrantedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).registerReceiver();
        verify(model).setModelState(WifiModel.STATE_CONNECTION_IDLE);
        verify(model).startConnectionQueueInRangeOrder();
    }

    @Test
    public void onRequestPermissionResultPermissionGrantedIsBroadcastTrueTest() {
        //Arrange
        when(model.isBroadcastRegistered()).thenReturn(true);
        RuntimePermissionHandler.PermissionGrantedEvent event = new RuntimePermissionHandler.PermissionGrantedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model, never()).registerReceiver();
        verify(model).setModelState(WifiModel.STATE_CONNECTION_IDLE);
        verify(model).startConnectionQueueInRangeOrder();
    }

    @Test
    public void onFailureBtnClickedTest() {
        //Arrange
        WifiView.OnFailureBtnClicked event = new WifiView.OnFailureBtnClicked();
        //Act
        presenter.onFailureBtnClicked(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void OnOkTransferRateBtnClickedTest() {
        //Arrange
        WifiView.OnOkTransferRateBtnClicked event = new WifiView.OnOkTransferRateBtnClicked();
        //Act
        presenter.OnOkTransferRateBtnClicked(event);
        //Assert
        verify(view).hideBannerTop();
        verify(view).hideTutorialButton();
    }

    @Test
    public void onOkResultClickedTestShouldNotMeasureTransferRateTest() {
        //Arrange
        OnOkResultClicked event = new OnOkResultClicked();
        when(model.shouldMeasureTransferRate()).thenReturn(false);
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
        verify(view, never()).hideTutorialButton();
    }

    @Test
    public void onOkResultClickedTestShouldMeasureTransferRateTest() {
        //Arrange
        OnOkResultClicked event = new OnOkResultClicked();
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
        verify(view).hideTutorialButton();
        verify(view).setSsidValue(model.getSsidConnected());
        verify(view).showTransferContainer();
        verify(view).showTransferRateDialog();
        verify(model).uploadFile(any(WifiTransferProgress.class));
    }

    @Test
    public void onOkResultClickedTestWifiTransferCallbackOnUpStreamTest() {
        //Arrange
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        OnOkResultClicked event = new OnOkResultClicked();
        double rate = 10;
        double progress = 20;
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).uploadFile(wifiTransferCallbackCaptor.capture());
        wifiTransferCallbackCaptor.getValue().onUpStream(rate, progress);
        verify(view.getUiHandler()).post(listenerCaptor.capture());
        listenerCaptor.getValue().run();
        verify(view).showProgressUpStream(rate, progress);
    }

    @Test
    public void onOkResultClickedTestWifiTransferCallbackOnDownStreamTest() {
        //Arrange
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        OnOkResultClicked event = new OnOkResultClicked();
        double rate = 10;
        double progress = 20;
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).uploadFile(wifiTransferCallbackCaptor.capture());
        wifiTransferCallbackCaptor.getValue().onDownStream(rate, progress);
        verify(view.getUiHandler()).post(listenerCaptor.capture());
        listenerCaptor.getValue().run();
        verify(view).showProgressDownStream(rate, progress);
    }

    @Test
    public void onOkResultClickedTestWifiTransferCallbackOnErrorStreamTest() {
        //Arrange
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        OnOkResultClicked event = new OnOkResultClicked();
        String errorMessage = "Error";
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).uploadFile(wifiTransferCallbackCaptor.capture());
        wifiTransferCallbackCaptor.getValue().onErrorStream(errorMessage);
        verify(model).fail(errorMessage);
        verify(view.getUiHandler()).post(listenerCaptor.capture());
        listenerCaptor.getValue().run();
        verify(view).finishActivity();
    }

    @Test
    public void onOkResultClickedTestWifiTransferCallbackOnUploadSuccessTest() {
        //Arrange
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        OnOkResultClicked event = new OnOkResultClicked();
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).uploadFile(wifiTransferCallbackCaptor.capture());
        wifiTransferCallbackCaptor.getValue().onUploadSuccess();
        verify(model).downloadFile(any(WifiTransferProgress.class));
    }

    @Test
    public void onOkResultClickedTestWifiTransferCallbackOnDownLoadSuccessTest() {
        //Arrange
        when(model.shouldMeasureTransferRate()).thenReturn(true);
        OnOkResultClicked event = new OnOkResultClicked();
        //Act
        presenter.onOkResultClicked(event);
        //Assert
        verify(model).uploadFile(wifiTransferCallbackCaptor.capture());
        wifiTransferCallbackCaptor.getValue().onDownLoadSuccess();
        verify(model).pass();
        verify(view.getUiHandler()).post(listenerCaptor.capture());
        listenerCaptor.getValue().run();
        verify(view).finishActivity();
    }

    @Test
    public void onPauseTest() {
        //Act
        presenter.onPause();
        //Arrange
        verify(model).unRegisterReceiver();
        verify(model).clearAllData();
    }

    @Test
    public void onDestroyTest() {
        //Act
        presenter.onDestroy();
        //Arrange
        verify(view).onDestroy();
    }

    @Test
    public void onServiceConnectedWifiListEmptyTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(true);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onServiceConnectedWifiListWithValuesTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model, never()).fail();
        verify(view, never()).finishActivity();
        verify(model).registerReceiver();
        verify(model).setModelCallback(any(WifiModelCallback.class));
    }

    @Test
    public void onServiceConnectedWifiModelCallbackOnReceiverRegisteredTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setModelCallback(wifiModelCallbackCaptor.capture());

        wifiModelCallbackCaptor.getValue().onReceiverRegistered(true);
        verify(model).disconnect();

        wifiModelCallbackCaptor.getValue().onReceiverRegistered(false);
        verify(view).setLoadingEnablingWifiMessage();
        verify(model).enableWifi();
    }

    @Test
    public void onServiceConnectedWifiModelCallbackOnWifiSateEnabledWithCoarseLocationTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        when(model.checkCoarseLocationPermissionsAvailable()).thenReturn(true);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setModelCallback(wifiModelCallbackCaptor.capture());
        wifiModelCallbackCaptor.getValue().onWifiSateEnabled(null);
        verify(model).startConnectionQueueInRangeOrder();
    }

    @Test
    public void onServiceConnectedWifiModelCallbackOnWifiSateEnabledNotCoarseLocationTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        when(model.checkCoarseLocationPermissionsAvailable()).thenReturn(false);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setModelCallback(wifiModelCallbackCaptor.capture());
        wifiModelCallbackCaptor.getValue().onWifiSateEnabled(null);
        verify(model, never()).startConnectionQueueInRangeOrder();
    }

    @Test
    public void onServiceConnectedWifiModelCallbackOnWifiConnectingTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        String ssid = "ssid";
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setModelCallback(wifiModelCallbackCaptor.capture());
        wifiModelCallbackCaptor.getValue().onWifiConnecting(ssid);
        verify(view).setLoadingConnectingWifiMessage(ssid);
    }

    @Test
    public void onServiceConnectedWifiModelCallbackOnWifiConnectedTest() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        when(model.isWifiListEmpty()).thenReturn(false);
        String ssid = "ssid";
        int speed = 10;
        int frequency = 20;
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setModelCallback(wifiModelCallbackCaptor.capture());
        wifiModelCallbackCaptor.getValue().onWifiConnected(ssid, speed, frequency);
        verify(view).updateConnectionInfo(ssid, speed, frequency);
    }
}