package com.facebook.devices.wificheck;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.WifiActivity;
import com.facebook.devices.mvp.view.WifiView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class WifiViewTest {

    private WifiView view;
    private WifiActivity activity;

    @Mock
    private BusProvider.Bus bus;

    /*Bindings*/
    private ConstraintLayout loadingLayout;
    private ConstraintLayout wifiTransferLayout;
    private LottieAnimationView lottieAnimationView;
    private FloatingTutorialView floatingTutorialView;
    private BannerView bannerView;
    private ProgressBar upStream;
    private ProgressBar downStream;
    private TextView tvLoadingMsg;
    private TextView tvUpStream;
    private TextView tvDownStream;
    private TextView tvSsidConnected;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, WifiActivity.class);
        activity = Robolectric.buildActivity(WifiActivity.class, intent).create().start().resume().get();

        view = new WifiView(activity, bus);

        when(RuntimeEnvironment.application.getSystemService(Context.WIFI_SERVICE)).thenReturn(null);

        /*Bindings*/
        loadingLayout = activity.findViewById(R.id.cl_animation_container);
        wifiTransferLayout = activity.findViewById(R.id.wifi_transfer_container);
        tvLoadingMsg = activity.findViewById(R.id.tv_waiting_message);
        lottieAnimationView = activity.findViewById(R.id.lottie_animation);
        upStream = activity.findViewById(R.id.pb_wifi_transfer_upstream);
        tvUpStream = activity.findViewById(R.id.tv_wifi_transfer_upstream_value);
        downStream = activity.findViewById(R.id.pb_wifi_transfer_downstream);
        tvDownStream = activity.findViewById(R.id.tv_wifi_transfer_downstream_value);
        tvSsidConnected = activity.findViewById(R.id.tv_wifi_transfer_message);
        bannerView = ShadowView.getCurrentBanner(view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void init() {
        assertNotNull(view.getUiHandler());
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.wifi_tutorial_message);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);

        assertEquals(tutorialTitle.getText(), title);
        assertEquals(tutorialMessage.getText(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showInitialDialogTest() {
        //Arrange
        String message = activity.getString(R.string.wifi_dialog_message);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showInitialDialog();

        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showInitialDialogOptionOneTest() {
        //Act
        view.showInitialDialog();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(WifiView.OnFailureBtnClicked.class));
    }

    @Test
    public void setLoadingEnablingWifiMessage() {
        //Arrange
        String message = activity.getString(R.string.wifi_loading_message_enabling_wifi);
        //Act
        view.setLoadingEnablingWifiMessage();
        //Assert
        assertEquals(loadingLayout.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getVisibility(), View.VISIBLE);
        assertEquals(lottieAnimationView.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getText(), message);
    }

    @Test
    public void setLoadingConnectingWifiMessage() {
        //Arrange
        String message = activity.getString(R.string.wifi_loading_message_connecting_network);
        //Act
        view.setLoadingConnectingWifiMessage();
        //Assert
        assertEquals(loadingLayout.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getVisibility(), View.VISIBLE);
        assertEquals(lottieAnimationView.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getText(), message);
    }

    @Test
    public void setLoadingConnectingWifiMessageWithId() {
        //Arrange
        String id = "text";
        String message = activity.getString(R.string.wifi_loading_message_connecting_network_ssid, id);
        //Act
        view.setLoadingConnectingWifiMessage(id);
        //Assert
        assertEquals(loadingLayout.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getVisibility(), View.VISIBLE);
        assertEquals(lottieAnimationView.getVisibility(), View.VISIBLE);
        assertEquals(tvLoadingMsg.getText(), message);
    }

    @Test
    public void updateConnectionInfoTest() {
        //Arrange
        String ssid = "text";
        int linkSpeed = 200;
        int frequency = 200;
        String linkSpeedText = activity.getString(R.string.mbps_data, linkSpeed);
        String frequencyText = activity.getString(R.string.mhz_data, frequency);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.updateConnectionInfo(ssid, linkSpeed, frequency);

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowAlertDialog shadowAlertDialog = Shadows.shadowOf(dialog);
        TextView wifiId = shadowAlertDialog.getView().findViewById(R.id.tv_wifi_ssid_value);
        TextView wifiSpeed = shadowAlertDialog.getView().findViewById(R.id.tv_wifi_speed_value);
        TextView wifiFrequency = shadowAlertDialog.getView().findViewById(R.id.tv_wifi_frequency_value);

        assertEquals(wifiId.getText(), ssid);
        assertEquals(wifiSpeed.getText(), linkSpeedText);
        assertEquals(wifiFrequency.getText(), frequencyText);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
        assertEquals(loadingLayout.getVisibility(), View.GONE);
        assertEquals(tvLoadingMsg.getVisibility(), View.GONE);

        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        verify(bus).post(any(WifiView.OnOkResultClicked.class));
    }

    @Test
    public void showProgressUpStreamTest() {
        //Arrange
        double rate = 10;
        double progress = 20;
        String actualProgress = activity.getString(R.string.wifi_transfer_rate, formatDouble(rate));
        //Act
        view.showProgressUpStream(rate, progress);
        //Assert
        assertEquals((int) progress, upStream.getProgress());
        assertEquals(actualProgress, tvUpStream.getText());
    }

    @Test
    public void showProgressDownStreamTest() {
        //Arrange
        double rate = 10;
        double progress = 20;
        String actualProgress = activity.getString(R.string.wifi_transfer_rate, formatDouble(rate));
        //Act
        view.showProgressDownStream(rate, progress);
        //Assert
        assertEquals((int) progress, downStream.getProgress());
        assertEquals(actualProgress, tvDownStream.getText());
    }

    @Test
    public void showTransferRateDialogTest() {
        //Arrange
        String message = activity.getString(R.string.wifi_transfer_rate_dialog_message);
        String okText = activity.getString(R.string.ok_base);
        //Act
        view.showTransferRateDialog();
        //Assert
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(okText, bannerView.getBannerTextOptionOne());
        assertEquals(message, messageDialog.getText());
    }

    @Test
    public void showTransferRateDialogOptionOneTest() {
        //Act
        view.showTransferRateDialog();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(WifiView.OnOkTransferRateBtnClicked.class));
    }

    @Test
    public void hideBannerTopTest() {
        //Act
        view.showInitialDialog();
        view.hideBannerTop();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void showTransferContainerTest() {
        //Act
        view.showTransferContainer();
        //Assert
        assertEquals(View.VISIBLE, wifiTransferLayout.getVisibility());
        assertEquals(View.GONE, loadingLayout.getVisibility());
    }

    @Test
    public void onDestroy() {
        //Arrange
        view.onFloatingTutorialClicked();
        view.updateConnectionInfo("", 0, 0);
        //Act
        view.onDestroy();
        //Assert
        assertNull(view.getTutorialDialog());
        assertNull(view.getFinalDialog());
    }

    @Test
    public void setSsidValueTest() {
        //Arrange
        String value = "Ssid";
        String actualValue = activity.getString(R.string.wifi_transfer_connected_message, value);
        //Act
        view.setSsidValue(value);
        //Assert
        assertEquals(actualValue, tvSsidConnected.getText());
    }

    @Test
    public void showTutorialButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideTutorialButtonTest() {
        view.showTutorialButton();
        view.hideTutorialButton();
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    private String formatDouble(double db) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(db);
    }
}