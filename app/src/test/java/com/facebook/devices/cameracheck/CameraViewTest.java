package com.facebook.devices.cameracheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.CameraActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.CameraView;
import com.facebook.devices.mvp.view.CameraView.FailureEvent;
import com.facebook.devices.mvp.view.CameraView.SuccessEvent;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.AutoFitTextureView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class CameraViewTest {

    private CameraActivity activity;
    private CameraView view;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private SurfaceTexture surfaceTextureMock;

    /* Bindings */
    private AutoFitTextureView textureView;
    private ProgressBar cameraAccessSpinner;
    private BannerView bannerView;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, CameraActivity.class);
        activity = Robolectric.buildActivity(CameraActivity.class, intent).create().start().resume().get();

        view = new CameraView(activity, bus);

        /* Bindings */
        textureView = activity.findViewById(R.id.texture_camera);
        cameraAccessSpinner = activity.findViewById(R.id.camera_access_spinner);
        bannerView = ShadowView.getCurrentBanner(view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void setupTextureView(){
        /*Assert*/
        assertNotNull(textureView.getSurfaceTextureListener());
    }

    @Test
    public void setupTextureView_SurfaceAvailable(){
        /*Arrange*/
        int width = 100;
        int height = 200;
        /*Act*/
        textureView.getSurfaceTextureListener().onSurfaceTextureAvailable(surfaceTextureMock, width, height);
        /*Assert*/
        ArgumentCaptor<CameraView.OnSurfaceTextureAvailable> captor = ArgumentCaptor.forClass(CameraView.OnSurfaceTextureAvailable.class);
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().surface, surfaceTextureMock);
        assertEquals(captor.getValue().height, height);
        assertEquals(captor.getValue().width, width);
    }

    @Test
    public void setupTextureView_SurfaceTextureDestroyed() {
        //Act
        assertFalse(textureView.getSurfaceTextureListener().onSurfaceTextureDestroyed(surfaceTextureMock));
    }

    @Test
    public void showTutorial(){
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.camera_dialog_tutorial_message);
        /*Act*/
        floatingTutorialView.getButton().performClick();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        /*Assert*/
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
    }

    @Test
    public void showTutorial_Positive(){
        /*Act*/
        floatingTutorialView.getButton().performClick();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        /*Assert*/
        ArgumentCaptor<CameraView.TutorialEvent> captor = ArgumentCaptor.forClass(CameraView.TutorialEvent.class);
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void showInitDialog(){
        /*Arrange*/
        String message = activity.getString(R.string.init_dialog_text_camera);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);
        /*Act*/
        view.showInitDialog();
        /*Assert*/
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showInitDialog_Positive_Action(){
        /*Act*/
        view.showInitDialog();
        bannerView.getBannerOptionOneButton().performClick();
        /*Assert*/
        verify(bus).post(any(SuccessEvent.class));
    }

    @Test
    public void showInitDialog_Negative_Action(){
        /*Act*/
        view.showInitDialog();
        bannerView.getBannerOptionTwoButton().performClick();
        /*Assert*/
        verify(bus).post(any(FailureEvent.class));
    }

    @Test
    public void showFloatingButtonTutorial(){
        /*Act*/
        view.showFloatingButtonTutorial();
        /*Assert*/
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void setAspectRatio(){
        /*Arrange*/
        int aspectWidth = 4;
        int aspectHeight = 3;
        /*Act*/
        view.setAspectRatio(aspectWidth, aspectHeight);
        /*Assert*/
        assertEquals(textureView.getAspectRatio().getHeight(), aspectHeight);
        assertEquals(textureView.getAspectRatio().getWidth(), aspectWidth);
    }

    @Test
    public void getTextureView(){
        /*Assert*/
        assertNotNull(view.getTextureView());
    }

    @Test
    public void isAvailable(){
        /*Assert*/
        assertFalse(view.isAvailable());
    }

    @Test
    public void showCameraAccessSpinnerTest(){
        /*Act*/
        view.showCameraAccessSpinner();
        /*Assert*/
        assertEquals(View.VISIBLE, cameraAccessSpinner.getVisibility());
    }

    @Test
    public void hideCameraAccessSpinnerTest(){
        /*Act*/
        view.hideCameraAccessSpinner();
        /*Assert*/
        assertEquals(View.GONE, cameraAccessSpinner.getVisibility());
    }

    @Test
    public void hideDialogTest() {
        /*Act*/
        view.showInitDialog();
        view.hideDialog();
        /*Assert*/
        Assert.assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void hideFloatingButtonTutorialTest() {
        /*Act*/
        view.hideFloatingButton();
        /*Assert*/
        assertFalse(floatingTutorialView.isButtonVisible());
    }
}
