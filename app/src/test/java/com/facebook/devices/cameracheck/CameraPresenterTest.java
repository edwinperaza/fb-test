package com.facebook.devices.cameracheck;

import android.os.Bundle;

import com.facebook.devices.mvp.model.CameraModel;
import com.facebook.devices.mvp.presenter.CameraPresenter;
import com.facebook.devices.mvp.view.CameraView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.Size;
import com.facebook.devices.utils.customcomponents.CancellableHandler;
import com.facebook.devices.utils.customviews.AutoFitTextureView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

public class CameraPresenterTest {

    @Mock
    private CameraView view;

    @Mock
    private CameraModel model;

    private CameraPresenter presenter;

    @Mock
    private AutoFitTextureView mockTextureView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getTextureView()).thenReturn(mockTextureView);
        presenter = new CameraPresenter(model, view);
    }

    @Test
    public void onStart_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onStart_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view, never()).hideCameraAccessSpinner();
    }

    @Test
    public void onStop() {
        /*Arrange*/
        boolean isChangingConfig = true;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onStopFalse() {
        /*Arrange*/
        boolean isChangingConfig = false;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onDestroy() {
        /*Arrange*/
        CameraModel.ResourcesManager resManager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(resManager);
        /*Act*/
        presenter.onDestroy();
        /*Assert*/
        verify(resManager).setSurfaceTextureUnavailable();
    }

    @Test
    public void onViewResumed_PermissionsNeeded() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(view, never()).showInitDialog();
        verify(model).requestPermissions();
    }

    @Test
    public void onViewResumed_OpenPreview() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).requestPermissions();
        verify(view).showInitDialog();
        verify(view).showFloatingButtonTutorial();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onViewPaused_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewPaused();
        /*Assert*/
        verify(model).pauseCamera();
    }

    @Test
    public void onViewPaused_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).pauseCamera();
    }

    @Test
    public void onSaveInstanceState_CameraIDReceived() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        when(model.isStopCameraBroadcastSent()).thenReturn(true);
        String cameraId = "1";
        CameraModel.ResourcesManager manager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(manager.getCameraId()).thenReturn(cameraId);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle).putString(eq(CameraModel.CAMERA_ID_RECEIVED), eq(cameraId));
    }

    @Test
    public void onRestoreInstantState_CameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        String cameraId = "1";
        CameraModel.ResourcesManager manager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraModel.CAMERA_ID_RECEIVED)).thenReturn(true);
        when(savedState.getString(CameraModel.CAMERA_ID_RECEIVED)).thenReturn(cameraId);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager).setCameraResourceReady(cameraId);
    }

    @Test
    public void onRestoreInstantState_NotCameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        CameraModel.ResourcesManager manager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraModel.CAMERA_ID_RECEIVED)).thenReturn(false);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager, never()).setCameraResourceReady(any());
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_NoRequest() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(false);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model).fail(anyString());
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_Request() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(true);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model, never()).fail(anyString());
        verify(view, never()).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionGranted() {
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionGrantedEvent());
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onSurfaceTextureAvailable_PermissionGranted_ImgNotTaken() {
        /*Arrange*/
        CameraModel.ResourcesManager manager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onSurfaceTextureAvailable_NotPermission() {
        /*Arrange*/
        CameraModel.ResourcesManager manager = mock(CameraModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view, never()).showCameraAccessSpinner();
        verify(model, never()).openCamera(view.getTextureView());
    }

    @Test
    public void onCameraOutputSetEvent(){
        /*Arrange*/
        Size previewRatio = new Size(100,100);
        /*Act*/
        presenter.onCameraOutputSetEvent(new CameraModel.CameraOutputSetEvent(previewRatio));
        /*Assert*/
        verify(view).setMirrorEffect(model.isMirrored());
        verify(view).setAspectRatio(eq(previewRatio.getWidth()), eq(previewRatio.getHeight()));
    }

    @Test
    public void onCameraPreviewSetEvent(){
        /*Act*/
        presenter.onCameraPreviewSetEvent(null);
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onOpenCameraError(){
        /*Arrange*/
        String message = "msg";
        /*Act*/
        presenter.onOpenCameraError(new CameraModel.OnCameraError(message));
        /*Assert*/
        failCalled();
    }

    @Test
    public void onCameraTargetAvailableTest(){
        /*Act*/
        presenter.onCameraTargetAvailable(null);
        /*Assert*/
        verify(model).openCamera(view.getTextureView());
    }


    @Test
    public void onSuccessButtonClickedTest(){
        /*Act*/
        presenter.onSuccessButtonClicked(null);
        /*Assert*/
        passCalled();
    }

    @Test
    public void onFailureButtonClickedTest(){
        /*Act*/
        presenter.onFailureButtonClicked(null);
        /*Assert*/
        failCalled();
    }

    /* Pass or Fail Check */
    public void passCalled(){
        verify(model).pass(any());
        verify(view).finishActivity();
    }

    public void failCalled(){
        verify(model).fail(any());
        verify(view).finishActivity();
    }

}
