package com.facebook.devices.brightnesscheck;


import android.view.Window;
import android.view.WindowManager;

import com.facebook.devices.mvp.model.BrightnessModel;
import com.facebook.devices.mvp.presenter.BrightnessPresenter;
import com.facebook.devices.mvp.view.BrightnessView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BrightnessPresenterTest {

    private BrightnessModel model;
    private BrightnessPresenter presenter;
    private BrightnessView view;

    @Mock
    private Window windowMock;
    @Mock
    private WindowManager.LayoutParams layoutParamsMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = mock(BrightnessModel.class);
        view = mock(BrightnessView.class);

        when(view.getWindow()).thenReturn(windowMock);
        when(view.getWindow().getAttributes()).thenReturn(layoutParamsMock);

        presenter = new BrightnessPresenter(model, view);
    }


    @Test
    public void onSucceededBtnPressedTest() {
        //Act
        presenter.onSucceededBtnPressed(null);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onFailedBtnPressedTest() {
        //Act
        presenter.onFailedBtnPressed(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onProgressSeekBarChangedTest() {
        //Arrange
        BrightnessView.OnProgressChanged event = new BrightnessView.OnProgressChanged(200);
        //Act
        presenter.onProgressSeekBarChanged(event);
        //Assert
        verify(model).setBrightness(event.getProgress());
        verify(view).applyWindowBrightness(model.getBrightness());
    }

    @Test
    public void onServiceDisconnectedTest() {
        /*Act*/
        presenter.onServiceDisconnected(null);
        /*Assert*/
        verify(model).removeService();
    }

}