package com.facebook.devices.brightnesscheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.BrightnessActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.BrightnessView;
import com.facebook.devices.mvp.view.BrightnessView.OnFailedBtnPressed;
import com.facebook.devices.mvp.view.BrightnessView.OnProgressChanged;
import com.facebook.devices.mvp.view.BrightnessView.OnSucceededBtnPressed;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class BrightnessViewTest {

    private BrightnessActivity activity;
    private BrightnessView view;

    @Mock
    private BusProvider.Bus bus;

    /*UI stuffs*/
    private SeekBar brightnessSeekBar;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BrightnessActivity.class);
        activity = Robolectric.buildActivity(BrightnessActivity.class, intent).create().start().resume().get();

        view = new BrightnessView(activity, bus);

        /*Utils*/
        brightnessSeekBar = activity.findViewById(R.id.brightness_seek_bar);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void showTutorialButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialTest() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.brightness_tutorial_dialog_text);
        String option = activity.getString(R.string.ok_base);

        /*Act*/
        view.onFloatingTutorialClicked();

        /*Assert*/
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(alertDialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), option);
    }

    @Test
    public void showInitDialogTest() {
        //Arrange
        String message = activity.getString(R.string.brightness_init_dialog_message);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);

        //Act
        view.showInitDialog();
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showInitDialogOptionOneTest() {
        //Act
        view.showInitDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(OnSucceededBtnPressed.class));
    }

    @Test
    public void showInitDialogOptionTwoTest() {
        //Act
        view.showInitDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        //Assert
        verify(bus).post(any(OnFailedBtnPressed.class));
    }

    @Test
    public void setSeekBarProgressCheck() {
        //Arrange
        int progress = 200;
        //Act
        view.setSeekBarProgress(progress);
        //Assert
        assertThat(brightnessSeekBar.getProgress(), equalTo(progress));
    }

    @Test
    public void getWindowTest() {
        //Assert
        assertNotNull(view.getWindow());
    }

    @Test
    public void applyWindowBrightnessTest() {
        //Arrange
        int brightness = 100;
        float expected = (float) brightness / 255;
        //Act
        view.applyWindowBrightness(100);
        //Assert
        assertEquals(activity.getWindow().getAttributes().screenBrightness, expected);
    }

    @Test
    public void QuestionOneEventTest() {
        //Act
        OnProgressChanged onProgressChanged = new OnProgressChanged(10);
        //Assert
        assertEquals(onProgressChanged.getProgress(), 10);
    }
}