package com.facebook.devices.brightnesscheck;

import com.facebook.devices.mvp.model.BrightnessModel;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BrightnessModelTest {

    private BrightnessModel model;

    @Before
    public void setup() {
        model = new BrightnessModel();
    }

    @Test
    public void setBrightnessCheck() {
        //Arrange
        int level = 100;
        //Act
        model.setBrightness(level);
        //Arrange
        assertThat(model.getBrightness(), equalTo(level));
    }
}
