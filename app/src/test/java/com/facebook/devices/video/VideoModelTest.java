package com.facebook.devices.video;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer.OnVideoSizeChangedListener;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.VideoActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.VideoModel;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.VolumeManager;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VideoModelTest {

    private VideoModel model;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private VolumeManager volumeManager;

    @Captor
    private ArgumentCaptor<OnVideoSizeChangedListener> onVideoSizeChangedListener;

    private Activity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, VideoActivity.class);
        activity = Robolectric.buildActivity(VideoActivity.class, intent).create().start().resume().get();

        PermissionHandler permissionHandler = new RuntimePermissionHandler(activity, bus);

        model = new VideoModel(activity, permissionHandler, "hd_video", true,
                true, volumeManager);
    }

    @Test
    public void calculateBestSizeConstraintWith() {
        Point screenSize = new Point(1080, 1920);
        Point event = new Point(1100, 1920);
        Point bestScreenSize = model.calculateBestSize(event.x, event.y, screenSize);
        assertEquals(bestScreenSize.x, 1100);
        assertEquals(bestScreenSize.y, 1920);
    }

    @Test
    public void calculateBestSizeConstraintHeight() {
        Point screenSize = new Point(1080, 1920);
        Point event = new Point(1080, 2000);
        Point bestScreenSize = model.calculateBestSize(event.x, event.y, screenSize);
        assertEquals(bestScreenSize.x, 1036);
        assertEquals(bestScreenSize.y, 1920);
    }

    @Test
    public void firstQuestionIsBeingShownTest() {
        //Act
        model.firstQuestionIsBeingShown();
        //Assert
        assertTrue(model.isFirstQuestionBeingShown());
    }

    @Test
    public void firstQuestionIsNotBeingShownTest() {
        //Act
        model.firstQuestionIsNotBeingShown();
        //Assert
        assertFalse(model.isFirstQuestionBeingShown());
    }

    @Test
    public void secondQuestionIsBeingShownTest() {
        //Act
        model.secondQuestionIsBeingShown();
        //Assert
        assertTrue(model.isSecondQuestionBeingShown());
    }

    @Test
    public void secondQuestionIsNotBeingShownTest() {
        //Act
        model.secondQuestionIsNotBeingShown();
        //Assert
        assertFalse(model.isSecondQuestionBeingShown());
    }
}