package com.facebook.devices.video;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.VideoActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.VideoModel;
import com.facebook.devices.mvp.view.VideoView;
import com.facebook.devices.mvp.view.VideoView.OnButtonFailurePressed;
import com.facebook.devices.mvp.view.VideoView.OnFirstQuestionReply;
import com.facebook.devices.mvp.view.VideoView.OnPlayButtonPressed;
import com.facebook.devices.mvp.view.VideoView.OnRenderingCompleted;
import com.facebook.devices.mvp.view.VideoView.OnReplayButtonPressed;
import com.facebook.devices.mvp.view.VideoView.OnSecondQuestionReply;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VideoViewTest {

    private VideoActivity activity;
    private VideoView view;

    @Mock
    private VideoModel model;

    @Mock
    private MediaPlayer mediaPlayer;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private SurfaceHolder surfaceHolder;

    @Captor
    private ArgumentCaptor<OnFirstQuestionReply> argCaptorQuestionOne;

    @Captor
    private ArgumentCaptor<OnSecondQuestionReply> argCaptorQuestionTwo;

    @Captor
    private ArgumentCaptor<MediaPlayer.OnVideoSizeChangedListener> onVideoSizeChangedListenerArgumentCaptor;

    @Captor
    private ArgumentCaptor<MediaPlayer.OnCompletionListener> onCompletionListenerArgumentCaptor;

    @Captor
    private ArgumentCaptor<MediaPlayer.OnPreparedListener> onPreparedListenerArgumentCaptor;

    /*Bindings*/
    private SurfaceView surfaceView;
    private ImageView ivPlay;
    private ImageView ivReplay;
    private View videoContainer;
    private FloatingTutorialView floatingTutorialView;
    private View opaqueLayer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, VideoActivity.class);
        activity = Robolectric.buildActivity(VideoActivity.class, intent).create().start().resume().get();

        view = new VideoView(activity, bus, mediaPlayer);

        /*Bindings*/
        surfaceView = activity.findViewById(R.id.video_surface);
        ivPlay = activity.findViewById(R.id.iv_play);
        ivReplay = activity.findViewById(R.id.iv_replay);
        videoContainer = activity.findViewById(R.id.video_container);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        opaqueLayer = activity.findViewById(R.id.view_layer);
    }

    @Test
    public void init() {
        view.setup();

        verify(mediaPlayer).setOnPreparedListener(onPreparedListenerArgumentCaptor.capture());
        onPreparedListenerArgumentCaptor.getValue().onPrepared(mediaPlayer);
        verify(bus).post(any(VideoView.OnVideoLoaded.class));

        verify(mediaPlayer).setOnCompletionListener(onCompletionListenerArgumentCaptor.capture());
        onCompletionListenerArgumentCaptor.getValue().onCompletion(mediaPlayer);
        verify(bus).post(any(VideoView.CompletionPlayerEvent.class));

        verify(mediaPlayer).setOnVideoSizeChangedListener(onVideoSizeChangedListenerArgumentCaptor.capture());
        onVideoSizeChangedListenerArgumentCaptor.getValue().onVideoSizeChanged(mediaPlayer, 0,0);
        verify(bus).post(any(VideoView.VideoSizeChangedEvent.class));
    }

    @Test
    public void initCallback() {
        //Act
        view.getSurfaceHolderCallback().surfaceCreated(surfaceHolder);
        //Assert
        verify(mediaPlayer).setDisplay(surfaceHolder);
        verify(bus).post(any(OnRenderingCompleted.class));
    }

    @Test
    public void setPlayButtonVisible() {
        //Arrange
        Drawable foreground = new ColorDrawable(ContextCompat.getColor(activity, R.color.play_extra_layer_color));
        //Act
        view.showPlayButton();
        //Assert
        assertEquals(ivPlay.getVisibility(), View.VISIBLE);
        assertEquals(surfaceView.getForeground(), foreground);
    }

    @Test
    public void setPlayButtonInvisible() {
        //Act
        view.hidePlayButton();
        //Assert
        assertEquals(ivPlay.getVisibility(), View.GONE);
        assertNull(surfaceView.getForeground());
    }

    @Test
    public void showInitialDialogTest() {
        //Arrange
        String message = activity.getString(R.string.video_initial_message);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showInitialDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showInitialDialogOptionOneTest() {
        //Act
        view.showInitialDialog();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showFirstQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.video_first_question_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);

        //Act
        view.showFirstQuestion();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void firstQuestionOptionOneTest() {
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(argCaptorQuestionOne.capture());
        assertEquals(argCaptorQuestionOne.getValue().getOptionSelected(), OnFirstQuestionReply.SUCCESS);
    }

    @Test
    public void firstQuestionOptionTwoTest() {
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(argCaptorQuestionOne.capture());
        assertEquals(argCaptorQuestionOne.getValue().getOptionSelected(), OnFirstQuestionReply.FAILED);
    }

    @Test
    public void showSecondQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.video_second_question_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);

        //Act
        view.showSecondQuestion();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showSecondQuestionOptionOneTest() {
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(argCaptorQuestionTwo.capture());
        assertEquals(argCaptorQuestionTwo.getValue().getOptionSelected(), OnFirstQuestionReply.SUCCESS);
    }

    @Test
    public void showSecondQuestionOptionTwoTest() {
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(argCaptorQuestionTwo.capture());
        assertEquals(argCaptorQuestionTwo.getValue().getOptionSelected(), OnFirstQuestionReply.FAILED);
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.video_tutorial_text);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);

        assertEquals(tutorialTitle.getText(), title);
        assertEquals(tutorialMessage.getText(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showVideoLayoutTest() {
        //Act
        view.showVideoLayout();
        //Assert
        assertEquals(videoContainer.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showReplayButtonTest() {
        //Act
        view.showReplayButton();
        //Assert
        assertEquals(ivPlay.getVisibility(), View.GONE);
        assertEquals(ivReplay.getVisibility(), View.VISIBLE);
    }

    @Test
    public void hideReplayButtonTest() {
        //Act
        view.hideReplayButton();
        //Assert
        assertEquals(ivReplay.getVisibility(), View.GONE);
    }

    @Test
    public void hideTutorialFloatingButtonTest() {
        view.hideTutorialButton();
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialFloatingButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showSurfaceViewTest() {
        view.showSurfaceView();
        assertEquals(opaqueLayer.getVisibility(), View.GONE);
    }

    @Test
    public void hideBannerTopTest() {
        //Act
        view.showFirstQuestion();
        view.hideDialogTop();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void onPlayButtonPressedTest() {
        view.onPlayButtonPressed();
        verify(bus).post(any(OnPlayButtonPressed.class));
    }

    @Test
    public void onRePlayButtonPressedTest() {
        view.onRePlayButtonPressed();
        verify(bus).post(any(OnReplayButtonPressed.class));
    }

    @Test
    public void setVideoDataSourceTest() throws IOException {
        //Arrange
        Uri uri = model.getVideoPathUri();
        //Act
        view.setVideoDataSource(uri);
        //Assert
        verify(mediaPlayer).setDataSource(any(Context.class), eq(uri));
        verify(mediaPlayer).prepareAsync();
    }

    @Test
    public void setVideoDataSourceStringTest() throws IOException {
        //Arrange
        String videoPath = "";
        //Act
        view.setVideoDataSource(videoPath);
        //Assert
        verify(mediaPlayer).setDataSource(videoPath);
        verify(mediaPlayer).prepareAsync();
    }

    @Test
    public void playVideoTest() {
        //Arrange
        when(mediaPlayer.isPlaying()).thenReturn(false);
        //Act
        view.playVideo();
        //Assert
        verify(mediaPlayer).start();
    }

    @Test
    public void seekVideoToTest() {
        //Arrange
        int sec = 100;
        //Act
        view.seekVideoTo(sec);
        //Assert
        verify(mediaPlayer).seekTo(sec);
    }

    @Test
    public void releaseResourcesTest() {
        //Act
        view.releaseResources();
        //Assert
        verify(mediaPlayer).release();
    }

}