package com.facebook.devices.video;

import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;

import com.facebook.devices.mvp.model.VideoModel;
import com.facebook.devices.mvp.presenter.VideoPresenter;
import com.facebook.devices.mvp.view.VideoView;
import com.facebook.devices.mvp.view.VideoView.OnRenderingCompleted;
import com.facebook.devices.mvp.view.VideoView.OnReplayButtonPressed;
import com.facebook.devices.utils.GenericUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static com.facebook.devices.mvp.model.VideoModel.FIRST_QUESTION_IS_BEING_SHOWN;
import static com.facebook.devices.mvp.model.VideoModel.SECOND_QUESTION_IS_BEING_SHOWN;
import static com.facebook.devices.permission.RuntimePermissionHandler.PermissionGrantedEvent;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GenericUtils.class)
public class VideoPresenterTest {

    private VideoPresenter presenter;

    @Mock
    private VideoModel model;

    @Mock
    private VideoView view;

    @Mock
    private Bundle savedInstantState;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new VideoPresenter(model, view);
    }

    @Test
    public void initShowPlayButtonTest() {
        //Arrange
        when(model.isFirstQuestionBeingShown()).thenReturn(false);
        VideoView.OnVideoLoaded event = new VideoView.OnVideoLoaded();
        //Act
        presenter.onVideoLoaded(event);
        //Assert
        verify(view).seekVideoTo(100);
        verify(view, never()).showReplayButton();
        verify(view).showPlayButton();

        verify(view).showInitialDialog();
    }

    @Test
    public void initShowRePlayButtonTest() {
        //Arrange
        when(model.isFirstQuestionBeingShown()).thenReturn(true);
        VideoView.OnVideoLoaded event = new VideoView.OnVideoLoaded();
        //Act
        presenter.onVideoLoaded(event);
        //Assert
        verify(view).seekVideoTo(100);
        verify(view).showReplayButton();
        verify(view, never()).showPlayButton();

        verify(view).showInitialDialog();
    }

    @Test
    public void initOnRenderingCompletedTest() {
        //Arrange
        OnRenderingCompleted event = new OnRenderingCompleted();
        when(model.isPermissionGranted()).thenReturn(false);

        //Assert
        presenter.onRenderingCompleted(event);
        //Acy
        verify(model).setRenderReady();
    }

    @Test
    public void onResumeRequestPermissionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(model).requestPermissions();
    }

    @Test
    public void onResumeShowFirstQuestionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.shouldRequestPermissions()).thenReturn(false);
        when(model.isFirstQuestionBeingShown()).thenReturn(true);
        when(model.isSecondQuestionBeingShown()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(view).showVideoLayout();
        verify(view).showFirstQuestion();
        verify(view).showTutorialButton();
        verify(view).showReplayButton();
    }

    @Test
    public void onResumeShowSecondQuestionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.shouldRequestPermissions()).thenReturn(false);
        when(model.isFirstQuestionBeingShown()).thenReturn(false);
        when(model.isSecondQuestionBeingShown()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(view).showVideoLayout();
        verify(view).showSecondQuestion();
        verify(view).showTutorialButton();
        verify(view).showReplayButton();
    }


    @Test
    public void onPlayButtonPressedTest() {
        //Act
        presenter.onPlayButtonPressed(null);
        //Assert
        verify(view).playVideo();
        verify(view).hidePlayButton();
        verify(view).hideTutorialButton();
        verify(view).hideDialogTop();
    }

    @Test
    public void onButtonFailurePressedTest() {
        //Act
        presenter.onButtonFailurePressed(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onFirstQuestionReplySuccessTest() {
        //Arrange
        VideoView.OnFirstQuestionReply event = new VideoView.OnFirstQuestionReply(VideoView.OnFirstQuestionReply.SUCCESS);
        //Act
        presenter.onFirstQuestionReply(event);
        //Assert
        verify(view).showSecondQuestion();
        verify(model).firstQuestionIsNotBeingShown();
        verify(model).secondQuestionIsBeingShown();
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onFirstQuestionReplyFailureTest() {
        //Arrange
        VideoView.OnFirstQuestionReply event = new VideoView.OnFirstQuestionReply(VideoView.OnFirstQuestionReply.FAILED);
        //Act
        presenter.onFirstQuestionReply(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
        verify(model, never()).pass();
        verify(model, never()).secondQuestionIsBeingShown();
    }

    @Test
    public void onSecondQuestionReplySuccessTest() {
        //Arrange
        VideoView.OnSecondQuestionReply event = new VideoView.OnSecondQuestionReply(VideoView.OnSecondQuestionReply.SUCCESS);
        //Act
        presenter.onSecondQuestionReply(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
        verify(model, never()).fail();
    }

    @Test
    public void onSecondQuestionReplyFailureTest() {
        //Arrange
        VideoView.OnSecondQuestionReply event = new VideoView.OnSecondQuestionReply(VideoView.OnSecondQuestionReply.FAILED);
        //Act
        presenter.onSecondQuestionReply(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
        verify(model, never()).pass();
    }

    @Test
    public void onSaveInstantStateTest() {
        //Act
        presenter.onSaveInstantState(savedInstantState);
        //Assert
        verify(savedInstantState).putBoolean(FIRST_QUESTION_IS_BEING_SHOWN, model.isFirstQuestionBeingShown());
        verify(savedInstantState).putBoolean(SECOND_QUESTION_IS_BEING_SHOWN, model.isSecondQuestionBeingShown());
    }

    @Test
    public void onPermissionGranted_Raw() throws IOException {
        //Arrange
        when(model.isRenderReady()).thenReturn(true);
        Uri mockUri = mock(Uri.class);
        when(model.getVideoPathUri()).thenReturn(mockUri);
        //Act
        presenter.onPermissionGranted(null);
        //Assert
        verify(view).setVideoDataSource(mockUri);
    }

    @Test
    public void onPermissionGranted_Path() throws IOException {
        //Arrange
        String path = "/sdcard/downloads/video.mp4";
        when(model.isRenderReady()).thenReturn(true);
        when(model.getVideoPathUri()).thenReturn(null);
        when(model.getVideoPath()).thenReturn(path);
        PermissionGrantedEvent event = new PermissionGrantedEvent();
        //Act
        presenter.onPermissionGranted(event);
        //Assert
        verify(view).setVideoDataSource(path);
    }

    @Test
    public void onPermissionGranted_Exception() throws IOException {
        //Arrange
        when(model.isRenderReady()).thenReturn(true);
        Uri mockUri = mock(Uri.class);
        when(model.getVideoPathUri()).thenReturn(mockUri);
        doThrow(IOException.class).when(view).setVideoDataSource(mockUri);
        //Act
        presenter.onPermissionGranted(null);
        //Assert
        verify(model).fail(any());
        verify(view).releaseResources();
        verify(view).finishActivity();
    }


    @Test
    public void onReplayButtonPressedTest() {
        //Arrange
        OnReplayButtonPressed event = new OnReplayButtonPressed();
        //Act
        presenter.onReplayButtonPressed(event);
        //Assert
        verify(model).firstQuestionIsNotBeingShown();
        verify(model).secondQuestionIsNotBeingShown();
        verify(view).hideDialogTop();
        verify(view).hideTutorialButton();
        verify(view).hideReplayButton();
        verify(view).playVideo();
    }

    @Test
    public void onCompletionPlayer() {
        presenter.onCompletionPlayer(null);
        verify(model).firstQuestionIsBeingShown();
        verify(view).showFirstQuestion();
        verify(view).showReplayButton();
        verify(view).showTutorialButton();
    }

    @Test
    public void onDestroyTest() {
        //Act
        presenter.onDestroy();
        //Assert
        verify(view).releaseResources();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(null);
        //Assert
        verify(view).releaseResources();
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onVideoSizeChanged() {
        //Arrange
        Point bestScreenSize = mock(Point.class);
        VideoView.VideoSizeChangedEvent event = new VideoView.VideoSizeChangedEvent(1100, 1920);
        Point point = new Point();
        point.set(10, 20);
        when(view.getScreenSize()).thenReturn(point);
        when(model.calculateBestSize(eq(event.width), eq(event.height), eq(point))).thenReturn(bestScreenSize);
        //Act
        presenter.onVideoSizeChanged(event);
        //Assert
        verify(view).setSize(bestScreenSize.x, bestScreenSize.y);
        verify(view).showSurfaceView();
    }
}