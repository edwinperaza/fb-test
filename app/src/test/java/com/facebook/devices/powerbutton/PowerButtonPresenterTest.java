package com.facebook.devices.powerbutton;


import com.facebook.devices.mvp.model.PowerButtonModel;
import com.facebook.devices.mvp.presenter.PowerButtonPresenter;
import com.facebook.devices.mvp.view.PowerButtonView;
import com.facebook.devices.utils.AlertDialogBaseEvent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PowerButtonPresenterTest {

    private PowerButtonPresenter presenter;

    @Mock
    private PowerButtonView view;
    @Mock
    private PowerButtonModel model;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new PowerButtonPresenter(model, view);
    }

    @Test
    public void onResume_FirstTime() {
        /*Arrange*/
        when(model.wasStartedAlready()).thenReturn(false);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(model).registerReceiver();
    }

    @Test
    public void onResume_ComeFromStandby() {
        /*Arrange*/
        when(model.wasStartedAlready()).thenReturn(true);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onStop_ReceiverRegistered() {
        /*Arrange*/
        when(model.isReceiverRegistered()).thenReturn(true);
        /*Act*/
        presenter.onStop();
        /*Assert*/
        verify(model).setAlreadyStarted();
    }

    @Test
    public void onDestroy() {
        /*Arrange*/
        when(model.isReceiverRegistered()).thenReturn(true);
        /*Act*/
        presenter.onDestroy();
        /*Assert*/
        verify(model).unregisterReceiver();
    }

    @Test
    public void onFailTestEvent() {
        /*Act*/
        presenter.onFailTestEvent(null);
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }
}
