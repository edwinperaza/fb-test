package com.facebook.devices.powerbutton;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.PowerButtonActivity;
import com.facebook.devices.mvp.model.PowerButtonModel;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlarmManager;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class PowerButtonModelTest {

    private PowerButtonModel model;
    private PowerButtonActivity activity;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, PowerButtonActivity.class);
        activity = Robolectric.buildActivity(PowerButtonActivity.class, intent).create().start().resume().get();

        model = new PowerButtonModel(activity);
    }

    @Test
    public void registerReceiver() {
        /*Act*/
        model.registerReceiver();
        RuntimeEnvironment.application.sendBroadcast(new Intent(Intent.ACTION_SCREEN_OFF));
        /*Assert*/
        assertTrue(model.isReceiverRegistered());
        Robolectric.flushBackgroundThreadScheduler();
        AlarmManager alarmMgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        ShadowAlarmManager shadowAlarmManager = Shadows.shadowOf(alarmMgr);
        assertEquals(shadowAlarmManager.getNextScheduledAlarm().type, AlarmManager.ELAPSED_REALTIME_WAKEUP);
    }

    @Test
    public void alreadyStarted_getAndSet() {
        /*Act*/
        model.setAlreadyStarted();
        /*Assert*/
        assertTrue(model.wasStartedAlready());
    }
}
