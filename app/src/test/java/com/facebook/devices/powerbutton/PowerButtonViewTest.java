package com.facebook.devices.powerbutton;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.PowerButtonActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.PowerButtonView;
import com.facebook.devices.mvp.view.WifiView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class PowerButtonViewTest {

    private PowerButtonView view;
    private PowerButtonActivity activity;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, PowerButtonActivity.class);
        activity = Robolectric.buildActivity(PowerButtonActivity.class, intent).create().start().resume().get();

        view = new PowerButtonView(activity, bus);
    }

    @Test
    public void tutorialDialogTest() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.power_button_tutorial_message);
        String positiveText = activity.getString(R.string.ok_base);

        /*Act*/
        view.onFloatingTutorialClicked();

        /*Assert*/
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showInitialDialogTest() {
        //Arrange
        String message = activity.getString(R.string.power_button_intro_message);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showInitDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showInitDialogOptionOneTest() {
        //Act
        view.showInitDialog();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(PowerButtonView.FailTestEvent.class));
    }
}
