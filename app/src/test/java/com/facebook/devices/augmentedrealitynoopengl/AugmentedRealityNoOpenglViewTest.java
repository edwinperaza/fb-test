package com.facebook.devices.augmentedrealitynoopengl;

import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.AugmentedRealityActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.AugmentedRealityNoOpenglView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class AugmentedRealityNoOpenglViewTest {

    private AugmentedRealityActivity activity;
    private AugmentedRealityNoOpenglView view;

    @Mock
    private BusProvider.Bus bus;

    private String version = "1";

    private TextView tvMessage;
    private Button finishButton;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, AugmentedRealityActivity.class);
        activity = Robolectric.buildActivity(AugmentedRealityActivity.class, intent).create().start().resume().get();
        view = new AugmentedRealityNoOpenglView(activity, bus);

        /*Binding*/
        tvMessage = activity.findViewById(R.id.tv_message);
        finishButton = activity.findViewById(R.id.btn_finish_test);
    }

    @Test
    public void setMessage() {
        /*Arrange*/
        String message = activity.getString(R.string.augmented_reality_no_opengl_message, activity.getString(R.string.augmented_reality_opengl_version, version));
        /*Act*/
        view.setMessage(version);
        /*Assert*/
        assertEquals(tvMessage.getText().toString(), message);
    }

    @Test
    public void finishTest() {
        /*Act*/
        view.finishTest();
        /*Assert*/
        verify(bus).post(any(AugmentedRealityNoOpenglView.FinishBtnEvent.class));
    }
}
