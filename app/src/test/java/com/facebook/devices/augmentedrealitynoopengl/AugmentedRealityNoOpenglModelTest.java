package com.facebook.devices.augmentedrealitynoopengl;

import com.facebook.devices.mvp.model.AugmentedRealityNoOpenglModel;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class AugmentedRealityNoOpenglModelTest {

    private AugmentedRealityNoOpenglModel model;
    private String openglVersion = "1";


    @Before
    public void setup() {
        model = new AugmentedRealityNoOpenglModel(openglVersion);
    }

    @Test
    public void openglVersionCorrect() {
        /*Assert*/
        assertEquals(model.getOpenglVersion(), openglVersion);
    }
}
