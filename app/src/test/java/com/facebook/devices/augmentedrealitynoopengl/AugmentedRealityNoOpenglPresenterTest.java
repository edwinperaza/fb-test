package com.facebook.devices.augmentedrealitynoopengl;

import com.facebook.devices.mvp.model.AugmentedRealityNoOpenglModel;
import com.facebook.devices.mvp.presenter.AugmentedRealityNoOpenglPresenter;
import com.facebook.devices.mvp.view.AugmentedRealityNoOpenglView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class AugmentedRealityNoOpenglPresenterTest {

    private AugmentedRealityNoOpenglPresenter presenter;

    @Mock
    private AugmentedRealityNoOpenglModel model;
    @Mock
    private AugmentedRealityNoOpenglView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new AugmentedRealityNoOpenglPresenter(model, view);
    }

    @Test
    public void init() {
        /*Assert*/
        verify(view).setMessage(model.getOpenglVersion());
    }

    @Test
    public void onFinishBtnEvent() {
        /*Act*/
        presenter.onFinishBtnEvent(null);
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void getServiceConnection() {
        /*Assert*/
        assertEquals(presenter.getServiceConnection(), presenter);
    }

    @Test
    public void onResumeDoNothing() {
        /*Act*/
        presenter.onResume();
        /*Assert*/
    }

    @Test
    public void onPauseDoNothing() {
        /*Act*/
        presenter.onPause();
        /*Assert*/
    }
}
