package com.facebook.devices.patterndisplaycheck;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.PatternDisplayActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.PatternDisplayView;
import com.facebook.devices.mvp.view.PatternDisplayView.OnTutorialTopBannerEvent;
import com.facebook.devices.mvp.view.PatternDisplayView.QuestionOneEvent;
import com.facebook.devices.mvp.view.PatternDisplayView.QuestionTwoEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEGATIVE;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_NEUTRAL;
import static com.facebook.devices.utils.AlertDialogBaseEvent.OPTION_POSITIVE;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class PatternDisplayViewTest {

    private PatternDisplayView view;
    private PatternDisplayActivity activity;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, PatternDisplayActivity.class);
        activity = Robolectric.buildActivity(PatternDisplayActivity.class, intent).create().start().resume().get();

        view = new PatternDisplayView(activity, bus);

        /*Bindings*/
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void init() {
        assertNotNull(view.getUiHandler());
    }

    @Test
    public void showFirstQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_question_one_dialog_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);
        String neutralText = activity.getString(R.string.try_again_base);

        //Act
        view.showFirstQuestion();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
        assertEquals(bannerView.getBannerTextOptionThree(), neutralText);
    }

    @Test
    public void showFirstQuestionOptionOneTest() {
        //Arrange
        ArgumentCaptor<QuestionOneEvent> captor = ArgumentCaptor.forClass(QuestionOneEvent.class);
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_POSITIVE, captor.getValue().optionSelected);
    }

    @Test
    public void showFirstQuestionOptionTwoTest() {
        //Arrange
        ArgumentCaptor<QuestionOneEvent> captor = ArgumentCaptor.forClass(QuestionOneEvent.class);
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_NEGATIVE, captor.getValue().optionSelected);
    }

    @Test
    public void showFirstQuestionOptionThreeTest() {
        //Arrange
        ArgumentCaptor<QuestionOneEvent> captor = ArgumentCaptor.forClass(QuestionOneEvent.class);
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionThreeButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_NEUTRAL, captor.getValue().optionSelected);
    }

    @Test
    public void showSecondQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_question_two_dialog_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);
        String neutralText = activity.getString(R.string.try_again_base);

        //Act
        view.showSecondQuestion();


        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
        assertEquals(bannerView.getBannerTextOptionThree(), neutralText);
    }

    @Test
    public void showSecondQuestionOptionOneTest() {
        //Arrange
        ArgumentCaptor<QuestionTwoEvent> captor = ArgumentCaptor.forClass(QuestionTwoEvent.class);
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_POSITIVE, captor.getValue().optionSelected);
    }

    @Test
    public void showSecondQuestionOptionTwoTest() {
        //Arrange
        ArgumentCaptor<QuestionTwoEvent> captor = ArgumentCaptor.forClass(QuestionTwoEvent.class);
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_NEGATIVE, captor.getValue().optionSelected);
    }

    @Test
    public void showSecondQuestionOptionThreeTest() {
        //Arrange
        ArgumentCaptor<QuestionTwoEvent> captor = ArgumentCaptor.forClass(QuestionTwoEvent.class);
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionThreeButton().callOnClick();
        verify(bus).post(captor.capture());
        assertEquals(OPTION_NEUTRAL, captor.getValue().optionSelected);
    }

    @Test
    public void showTutorialOnBannerTopTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_tutorial_dialog_text);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.showTutorialOnBannerTop();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void showTutorialOnBannerTopOptionOneTest() {
        //Act
        view.showTutorialOnBannerTop();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(OnTutorialTopBannerEvent.class));
    }

    @Test
    public void hideBannerTopTest() {
        //Act
        view.showTutorialOnBannerTop();
        view.hideBannerTop();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void showTutorialTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.color_check_tutorial_dialog_text);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);

        Assert.assertEquals(tutorialTitle.getText(), title);
        Assert.assertEquals(tutorialMessage.getText(), message);
        Assert.assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void hideTutorialFloatingButtonTest() {
        //Act
        view.hideTutorialFloatingButton();
        //Assert
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialFloatingButtonTest() {
        //Act
        view.showTutorialFloatingButton();
        //Assert
        assertTrue(floatingTutorialView.isButtonVisible());
    }


}
