package com.facebook.devices.patterndisplaycheck;


import com.facebook.devices.mvp.model.PatternDisplayModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;

public class PatternDisplayModelTest {

    private PatternDisplayModel model;
    private int squaresNumber = 5;
    private boolean showMuraQuestion = false;
    private boolean showTutorial = false;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        model = new PatternDisplayModel(showTutorial, squaresNumber, showMuraQuestion);
    }

    @Test
    public void checkInitTest() {
        //Assert
        assertEquals(squaresNumber, model.getSquaresNumber());
        assertEquals(showMuraQuestion, model.shouldShowMuraQuestion());
        assertEquals(showTutorial, model.shouldShowTutorial());
    }

    @Test
    public void nextStateTest() {
        //Act
        model.nextState();
        //Assert
        assertEquals(PatternDisplayModel.QUESTION_TWO, model.getState());
    }
}
