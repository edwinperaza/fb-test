package com.facebook.devices.patterndisplaycheck;


import android.os.Handler;

import com.facebook.devices.activities.PatternDisplayActivity;
import com.facebook.devices.mvp.model.PatternDisplayModel;
import com.facebook.devices.mvp.presenter.PatternDisplayPresenter;
import com.facebook.devices.mvp.view.PatternDisplayView;
import com.facebook.devices.mvp.view.PatternDisplayView.QuestionOneEvent;
import com.facebook.devices.mvp.view.PatternDisplayView.QuestionTwoEvent;
import com.facebook.devices.utils.AlertDialogBaseEvent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PatternDisplayPresenterTest {

    private PatternDisplayPresenter presenter;

    @Mock
    private PatternDisplayView view;

    @Mock
    private PatternDisplayModel model;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(view.getUiHandler()).thenReturn(mockHandler);

        presenter = new PatternDisplayPresenter(model, view);
    }

    @Test
    public void initCorrectlyTest() {
        //Assert
        verify(view).setPatternSize(model.getSquaresNumber());
    }

    @Test
    public void onPauseTest() {
        /*Act*/
        presenter.onPause();
        /*Assert*/
        verify(view.getUiHandler()).removeCallbacks(any(Runnable.class));
    }

    @Test
    public void onResumeShouldShowTutorialTest() {
        //Arrange
        when(model.shouldShowTutorial()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(view).showTutorialOnBannerTop();
        verify(view.getUiHandler(), never()).postDelayed(any(Runnable.class), anyLong());
    }

    @Test
    public void onResumeNotShouldShowTutorialTest() {
        //Arrange
        when(model.shouldShowTutorial()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view, never()).showTutorialOnBannerTop();
    }

    @Test
    public void onResumeNotShouldShowTutorialAndQuestionOneStateTest() {
        //Arrange
        final Handler immediateHandler = mock(Handler.class);
        when(immediateHandler.postDelayed(any(Runnable.class), anyLong())).thenAnswer((Answer) invocation -> {
            Runnable msg = invocation.getArgument(0);
            msg.run();
            return null;
        });
        when(view.getUiHandler()).thenReturn(immediateHandler);

        PatternDisplayModel model = new PatternDisplayModel(false, 8, false);
        PatternDisplayPresenter presenter = new PatternDisplayPresenter(model, view);

        //Act
        presenter.onResume();

        //Assert
        verify(view).showTutorialFloatingButton();
        verify(view).showFirstQuestion();
        verify(view, never()).showSecondQuestion();
    }

    @Test
    public void onResumeNotShouldShowTutorialAndQuestionTwoStateTest() {
        //Arrange
        final Handler immediateHandler = mock(Handler.class);
        when(immediateHandler.postDelayed(any(Runnable.class), anyLong())).thenAnswer((Answer) invocation -> {
            Runnable msg = invocation.getArgument(0);
            msg.run();
            return null;
        });
        when(view.getUiHandler()).thenReturn(immediateHandler);

        PatternDisplayModel model = new PatternDisplayModel(false, 8, false);
        PatternDisplayPresenter presenter = new PatternDisplayPresenter(model, view);
        //Act
        model.nextState();
        presenter.onResume();

        //Assert
        verify(view).showTutorialFloatingButton();
        verify(view).showSecondQuestion();
        verify(view, never()).showFirstQuestion();
    }

    @Test
    public void onQuestionOneEventOptionPositiveShowMuraQuestionTest() {
        /*Arrange*/
        when(model.shouldShowMuraQuestion()).thenReturn(true);
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        /*Act*/
        presenter.onQuestionOneEvent(event);
        /*Assert*/
        verify(model).nextState();
        verify(view).showSecondQuestion();
    }

    @Test
    public void onQuestionOneEventOptionPositiveNotShowMuraQuestionTest() {
        /*Arrange*/
        when(model.shouldShowMuraQuestion()).thenReturn(false);
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        /*Act*/
        presenter.onQuestionOneEvent(event);
        /*Assert*/
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionOneEventOptionNegativeTest() {
        /*Arrange*/
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEGATIVE);
        /*Act*/
        presenter.onQuestionOneEvent(event);
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionOneEventOptionNeutralTest() {
        /*Arrange*/
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEUTRAL);
        /*Act*/
        presenter.onQuestionOneEvent(event);
        /*Assert*/
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view).hideBannerTop();
        verify(view).hideTutorialFloatingButton();
    }

    @Test
    public void onQuestionTwoEventOptionPositiveTest() {
        /*Arrange*/
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        /*Act*/
        presenter.onQuestionTwoEvent(event);
        /*Assert*/
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionTwoEventOptionNegativeTest() {
        /*Arrange*/
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEGATIVE);
        /*Act*/
        presenter.onQuestionTwoEvent(event);
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionTwoEventOptionNeutralTest() {
        /*Arrange*/
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEUTRAL);
        /*Act*/
        presenter.onQuestionTwoEvent(event);
        /*Assert*/
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view).hideBannerTop();
        verify(view).hideTutorialFloatingButton();
    }

    @Test
    public void onTutorialTopBannerEventTest() {
        //Arrange
        PatternDisplayView.OnTutorialTopBannerEvent event = new PatternDisplayView.OnTutorialTopBannerEvent();
        //Act
        presenter.onTutorialTopBannerEvent(event);
        //Assert
        verify(view).hideBannerTop();
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
    }
}
