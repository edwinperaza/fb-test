package com.facebook.devices.colorcheck;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.ColorModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static com.facebook.devices.mvp.model.ColorModel.QUESTION_TWO;
import static junit.framework.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class ColorModelTest {

    private ColorModel model;
    private String[] startColorCode = new String[]{"#ffff0000"};
    private String[] multiColors = new String[]{"#000000", "#00FFFF", "#FAFAFA", "#0066ff", "#66ff66", "#ff33cc"};
    private boolean showMuraQuestion = false;
    private boolean showTutorial = true;

    @Before
    public void setup() {
        model = new ColorModel(showTutorial, showMuraQuestion, startColorCode);
    }

    @Test
    public void checkInit() {
        //Assert
        assertEquals(startColorCode, model.getColorCode());
        assertEquals(showMuraQuestion, model.shouldShowMuraQuestion());
        assertEquals(showTutorial,  model.shouldShowTutorial());
    }

    @Test
    public void changeColor() {
        //Arrange
        String[] colorCheck = new String[]{"#ffff0000"};
        //Act
        model.setColorCode(colorCheck);
        //Assert
        assertEquals(model.getColorCode(), colorCheck);
    }

    @Test
    public void checkMultiColors() {
        //Act
        model.setColorCode(multiColors);
        //Assert
        assertEquals(model.getColorCode(), multiColors);
    }

    @Test
    public void nextStateTest() {
        //Act
        model.nextState();
        //Assert
        assertEquals(model.getState(), QUESTION_TWO);
    }

}