package com.facebook.devices.colorcheck;


import android.os.Handler;

import com.facebook.devices.activities.ColorActivity;
import com.facebook.devices.mvp.model.ColorModel;
import com.facebook.devices.mvp.presenter.ColorPresenter;
import com.facebook.devices.mvp.view.ColorView;
import com.facebook.devices.mvp.view.ColorView.OnTutorialTopBannerEvent;
import com.facebook.devices.mvp.view.ColorView.QuestionOneEvent;
import com.facebook.devices.mvp.view.ColorView.QuestionTwoEvent;
import com.facebook.devices.utils.AlertDialogBaseEvent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.mvp.model.ColorModel.QUESTION_ONE;
import static com.facebook.devices.mvp.model.ColorModel.QUESTION_TWO;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ColorPresenterTest {

    private ColorPresenter presenter;

    @Mock
    private ColorModel model;

    @Mock
    private ColorView view;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(view.getUiHandler()).thenReturn(mockHandler);

        presenter = new ColorPresenter(model, view);
    }

    @Test
    public void initCorrectly() {
        //Assert
        verify(view).setColor(model.getColorCode());
    }

    @Test
    public void onResumeShouldTutorialTest() {
        //Assert
        when(model.shouldShowTutorial()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(view).showTutorialOnBannerTop();
    }

    @Test
    public void onResumeNotShouldTutorialQuestionOneStateTest() {
        //Assert
        when(model.shouldShowTutorial()).thenReturn(false);
        when(model.getState()).thenReturn(QUESTION_ONE);
        //Act
        presenter.onResume();
        //Assert
        verify(view, never()).showTutorialOnBannerTop();

        ArgumentCaptor<Runnable> listenerCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(view.getUiHandler()).postDelayed(listenerCaptor.capture(), anyLong());
        listenerCaptor.getValue().run();

        verify(view).showTutorialFloatingButton();
        verify(view).showFirstQuestion();
    }

    @Test
    public void onResumeNotShouldTutorialQuestionTwoStateTest() {
        //Assert
        when(model.shouldShowTutorial()).thenReturn(false);
        when(model.getState()).thenReturn(QUESTION_TWO);
        //Act
        presenter.onResume();
        //Assert
        verify(view, never()).showTutorialOnBannerTop();

        ArgumentCaptor<Runnable> listenerCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(view.getUiHandler()).postDelayed(listenerCaptor.capture(), anyLong());
        listenerCaptor.getValue().run();

        verify(view).showTutorialFloatingButton();
        verify(view).showSecondQuestion();
    }

    @Test
    public void onPause() {
        //Act
        presenter.onPause();
        //Assert
        verify(view.getUiHandler()).removeCallbacks(any(Runnable.class));
    }

    @Test
    public void onServiceDisconnected() {
        //Act
        presenter.onServiceDisconnected(null);
        //Assert
        verify(model).removeService();
    }

    @Test
    public void onQuestionOneEventPositiveShowMuraQuestion() {
        //Arrange
        when(model.shouldShowMuraQuestion()).thenReturn(true);
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        //Act
        presenter.onQuestionOneEvent(event);
        //Assert
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
        verify(model).nextState();
        verify(view).showSecondQuestion();
    }

    @Test
    public void onQuestionOneEventPositivePassTest() {
        //Arrange
        when(model.shouldShowMuraQuestion()).thenReturn(false);
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        //Act
        presenter.onQuestionOneEvent(event);
        //Assert
        verify(model, never()).nextState();
        verify(view, never()).showSecondQuestion();
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionOneEvent_Negative() {
        //Arrange
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEGATIVE);
        //Act
        presenter.onQuestionOneEvent(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionOneEvent_Neutral() {
        //Arrange
        QuestionOneEvent event = new QuestionOneEvent(AlertDialogBaseEvent.OPTION_NEUTRAL);
        //Act
        presenter.onQuestionOneEvent(event);
        //Assert
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
    }

    @Test
    public void onQuestionTwoEvent_Positive() {
        //Arrange
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        //Act
        presenter.onQuestionTwoEvent(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionTwoEvent_Negative() {
        //Arrange
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEGATIVE);
        //Act
        presenter.onQuestionTwoEvent(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionTwoEvent_Neutral() {
        //Arrange
        QuestionTwoEvent event = new QuestionTwoEvent(AlertDialogBaseEvent.OPTION_NEUTRAL);
        //Act
        presenter.onQuestionTwoEvent(event);
        //Assert
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
        verify(view).hideBannerTop();
        verify(view).hideTutorialFloatingButton();
    }

    @Test
    public void onTutorialTopBannerEventTest() {
        //Arrange
        OnTutorialTopBannerEvent event = new OnTutorialTopBannerEvent();
        //Act
        presenter.onTutorialTopBannerEvent(event);
        //Assert
        verify(view).hideBannerTop();
        verify(view.getUiHandler()).postDelayed(any(Runnable.class), anyLong());
    }

}
