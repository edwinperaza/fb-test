package com.facebook.devices.colorcheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.ColorActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.ColorView;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class ColorViewTest {

    private ColorActivity activity;
    private ColorView view;

    private FloatingTutorialView floatingTutorialView;

    @Captor
    private ArgumentCaptor<ColorView.QuestionOneEvent> argCaptorQuestionOne;

    @Captor
    private ArgumentCaptor<ColorView.QuestionTwoEvent> argCaptorQuestionTwo;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, ColorActivity.class);
        intent.putExtra("color", "#ffff0000");
        activity = Robolectric.buildActivity(ColorActivity.class, intent).create().start().resume().get();
        view = new ColorView(activity, bus);

        //Bindings
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void hideTutorialFloatingButtonTest() {
        view.hideTutorialFloatingButton();
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialFloatingButtonTest() {
        view.showTutorialFloatingButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showFirstQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_question_one_dialog_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);
        String neutralText = activity.getString(R.string.try_again_base);

        //Act
        view.showFirstQuestion();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
        assertEquals(bannerView.getBannerTextOptionThree(), neutralText);
    }

    @Test
    public void firstQuestionOptionOneTest() {
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(argCaptorQuestionOne.capture());
        assertEquals(argCaptorQuestionOne.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void firstQuestionOptionTwoTest() {
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(argCaptorQuestionOne.capture());
        assertEquals(argCaptorQuestionOne.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEGATIVE);
    }

    @Test
    public void firstQuestionOptionThreeTest() {
        //Act
        view.showFirstQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionThreeButton().callOnClick();
        verify(bus).post(argCaptorQuestionOne.capture());
        assertEquals(argCaptorQuestionOne.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEUTRAL);
    }

    @Test
    public void showSecondQuestionTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_question_two_dialog_text);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);
        String neutralText = activity.getString(R.string.try_again_base);

        //Act
        view.showSecondQuestion();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
        assertEquals(bannerView.getBannerTextOptionThree(), neutralText);
    }

    @Test
    public void secondQuestionOptionOneTest() {
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(argCaptorQuestionTwo.capture());
        assertEquals(argCaptorQuestionTwo.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void secondQuestionOptionTwoTest() {
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        verify(bus).post(argCaptorQuestionTwo.capture());
        assertEquals(argCaptorQuestionTwo.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEGATIVE);
    }

    @Test
    public void secondQuestionOptionThreeTest() {
        //Act
        view.showSecondQuestion();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionThreeButton().callOnClick();
        verify(bus).post(argCaptorQuestionTwo.capture());
        assertEquals(argCaptorQuestionTwo.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEUTRAL);
    }

    @Test
    public void showTutorialOnBannerTopTest() {
        //Arrange
        String message = activity.getString(R.string.color_check_tutorial_dialog_text);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.showTutorialOnBannerTop();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void tutorialOptionOneTest() {
        //Act
        view.showTutorialOnBannerTop();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(ColorView.OnTutorialTopBannerEvent.class));
    }

    @Test
    public void hideBannerTopTest() {
        //Act
        view.showFirstQuestion();
        view.hideBannerTop();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.color_check_tutorial_dialog_text);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        floatingTutorialView.getButton().performClick();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);

        assertEquals(tutorialTitle.getText(), title);
        assertEquals(tutorialMessage.getText(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void QuestionOneEventTest() {
        ColorView.QuestionOneEvent questionOneEvent = new ColorView.QuestionOneEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        assertEquals(questionOneEvent.optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void QuestionTwoEventTest() {
        ColorView.QuestionTwoEvent questionTwoEvent = new ColorView.QuestionTwoEvent(AlertDialogBaseEvent.OPTION_POSITIVE);
        assertEquals(questionTwoEvent.optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }
}