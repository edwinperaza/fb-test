package com.facebook.devices.wifidatabuilder;

import com.facebook.devices.utils.models.WifiDataContainer;
import com.facebook.devices.utils.models.WifiDataContainerBuilder;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class WifiDataBuilderTest {

    private String[] ssids = new String[]{"ssid0", "ssid1", "ssid2", "ssid3", "ssid4"};
    private String[] passwords = new String[]{"pass0", "pass1", "pass2", "pass3", "pass4"};
    private String[] ssidsError = new String[]{"ssid0", "ssid1", "ssid2"};

    @Before
    public void setup() {
        /*Nothing to do here*/
    }

    @Test
    public void build_data() {
        /*Arrange*/
        WifiDataContainer[] data = null;
        /*Act*/
        try {
            data = WifiDataContainerBuilder.buildWifiData(ssids, passwords);
        } catch (WifiDataContainerBuilder.DataMissingException e) {
            e.printStackTrace();
        }
        /*Assert*/
        assertNotNull(data);
        assertEquals(data[0].getSsID(), ssids[0]);
        assertEquals(data[1].getSsID(), ssids[1]);
        assertEquals(data[2].getSsID(), ssids[2]);
        assertEquals(data[3].getSsID(), ssids[3]);
        assertEquals(data[4].getSsID(), ssids[4]);
        assertEquals(data[0].getPassword(), passwords[0]);
        assertEquals(data[1].getPassword(), passwords[1]);
        assertEquals(data[2].getPassword(), passwords[2]);
        assertEquals(data[3].getPassword(), passwords[3]);
        assertEquals(data[4].getPassword(), passwords[4]);
    }

    @Test
    public void incoherent_data() {
        /*Arrange*/
        WifiDataContainer[] data = null;
        boolean exceptionLaunched = false;
        /*Act*/
        try {
            data = WifiDataContainerBuilder.buildWifiData(ssidsError, passwords);
        } catch (WifiDataContainerBuilder.DataMissingException e) {
            exceptionLaunched = true;
        }
        /*Assert*/
        assertNull(data);
        assertTrue(exceptionLaunched);
    }

    @Test
    public void data_container(){
        /*Arrange*/
        String ssid = ssids[0];
        String pass = passwords[0];
        int level = 100;
        /*Act*/
        WifiDataContainer container = new WifiDataContainer(ssid, pass);
        container.setLevel(level);
        /*Assert*/
        assertEquals(container.getSsID(), ssid);
        assertEquals(container.getPassword(), pass);
        assertEquals(container.getLevel(), level);
    }
}
