package com.facebook.devices.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel;
import com.facebook.devices.mvp.presenter.BluetoothSpeakerPresenter;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class BluetoothSpeakerActivityTest {

    private ShadowTestActivity<BluetoothSpeakerPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BluetoothSpeakerActivity.class);
        activityController = Robolectric.buildActivity(BluetoothSpeakerActivity.class, intent);
        activity = (Activity) activityController.get();
        shadowTestActivity = new ShadowTestActivity<>(activity);
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }

    @Test
    public void destroying() {
        /*Arrange*/
        BluetoothSpeakerPresenter presenterMock = mock(BluetoothSpeakerPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.destroy();
        /*Assert*/
        verify(presenterMock).onDestroy();
    }

    @Test
    public void onSaveInstanceState() {
        /*Arrange*/
        Bundle bundleMock = mock(Bundle.class);
        BluetoothSpeakerPresenter presenterMock = mock(BluetoothSpeakerPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.saveInstanceState(bundleMock);
        /*Assert*/
        verify(presenterMock).onSaveInstantState(bundleMock);
    }

    @Test
    public void onRequestPermissionResult() {
        /*Arrange*/
        RuntimePermissionHandler permissionHandlerMock = mock(RuntimePermissionHandler.class);
        shadowTestActivity.setPermissionHandler(permissionHandlerMock);
        int requestCode = 0;
        String[] permissions = new String[]{};
        int[] grantResults = new int[]{0, 1, 2};
        /*Act*/
        activity.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*Assert*/
        verify(permissionHandlerMock).onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Test
    public void onActivityResult() {
        /*Arrange*/
        BluetoothSpeakerPresenter presenterMock = mock(BluetoothSpeakerPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        activity.startActivityForResult(enableBtIntent, BluetoothSpeakerModel.REQUEST_ENABLE_BT);
        shadowOf(activity).receiveResult(
                shadowOf(activity).getNextStartedActivity(),
                Activity.RESULT_OK,
                new Intent());
        /*Assert*/
        verify(presenterMock).onActivityResult(anyInt(), anyInt());
    }

    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

}
