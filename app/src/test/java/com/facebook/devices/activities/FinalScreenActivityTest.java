package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.mvp.model.FinalScreenModel;
import com.facebook.devices.mvp.presenter.FinalScreenPresenter;
import com.facebook.hwtp.service.ConnectionService;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class FinalScreenActivityTest {


    private ShadowTestActivity<FinalScreenPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    private int status = FinalScreenModel.MODE_SUCCESS;
    private long duration = FinalScreenModel.DEFAULT_MILLIS_TO_CLOSE;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, FinalScreenActivity.class);
        intent.putExtra("status", String.valueOf(status));
        intent.putExtra("duration", String.valueOf(duration));
        activityController = Robolectric.buildActivity(FinalScreenActivity.class, intent);
        activity = (Activity) activityController.get();
        shadowTestActivity = new ShadowTestActivity<>(activity);
    }

    @Test
    public void creation(){
        /*Act*/
        activityController.create();
        /*Assert*/
        assertEquals(status, shadowTestActivity.getAttributeValue("status"));
        assertEquals(duration, shadowTestActivity.getAttributeValue("duration"));
        assertTrue(shadowTestActivity.isPresenterCreated());
    }

    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        Assert.assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        Assert.assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

}
