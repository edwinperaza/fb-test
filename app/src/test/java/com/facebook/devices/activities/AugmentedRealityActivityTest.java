package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.AugmentedRealityPresenter;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.SystemHelper;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*", "javax.xml.parsers.*", "com.sun.org.apache.xerces.internal.jaxp.*"})
@PrepareForTest(SystemHelper.class)
public class AugmentedRealityActivityTest {

    private ActivityController activityController;
    private Activity activity;

    @Rule
    public PowerMockRule rule = new PowerMockRule();

    @Before
    public void setup() {
        PowerMockito.mockStatic(SystemHelper.class);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, AugmentedRealityActivity.class);
        activityController = Robolectric.buildActivity(AugmentedRealityActivity.class, intent);
        activity = (Activity) activityController.get();
    }

    @Test
    public void creation_not_supported() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }


    @Test
    public void starting() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void stopping() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void resuming() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        AugmentedRealityPresenter presenter = mock(AugmentedRealityPresenter.class);
        activityController.create();
        shadowTestActivity.setPresenter(presenter);
        /*Act*/
        activityController.resume();
        /*Assert*/
        verify(presenter).onResume();
    }


    @Test
    public void pausing() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        AugmentedRealityPresenter presenter = mock(AugmentedRealityPresenter.class);
        activityController.create();
        shadowTestActivity.setPresenter(presenter);
        /*Act*/
        activityController.resume();
        activityController.pause();
        /*Assert*/
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void requestPermissionResult() {
        /*Arrange*/
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3f);
        ShadowTestActivity<AugmentedRealityPresenter> shadowTestActivity = new ShadowTestActivity<>(activity);
        int requestCode = 1;
        String[] permissions = new String[]{};
        int[] grantResults = new int[]{};
        PermissionHandler permissionHandler = mock(PermissionHandler.class);
        shadowTestActivity.setPermissionHandler(permissionHandler);
        /*Act*/
        activity.onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*Assert*/
        verify(permissionHandler).onRequestPermissionResult(eq(requestCode), eq(permissions), eq(grantResults));
    }

}
