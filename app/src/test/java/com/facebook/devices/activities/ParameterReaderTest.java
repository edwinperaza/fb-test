package com.facebook.devices.activities;


import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.Assert.assertEquals;

public class ParameterReaderTest {

    @Before
    public void setup() {

    }

    @Test
    public void readString() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"test"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals("test", pr.readString());
    }

    @Test
    public void readInteger() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1, pr.readInteger());
    }

    @Test
    public void readFloat() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1F, pr.readFloat());
    }

    @Test
    public void readLong() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1L, pr.readLong());
    }

    @Test
    public void readDouble() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1D, pr.readDouble());
    }

    @Test
    public void readBoolean() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"true"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(true, pr.readBoolean());
    }

    @Test
    public void readByte() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1, pr.readByte());
    }

    @Test
    public void readStringAndBoolean() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"test", "true"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals("test", pr.readString());
        assertEquals(true, pr.readBoolean());
    }

    @Test
    public void readStringAndInteger() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"test", "1"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals("test", pr.readString());
        assertEquals(1, pr.readInteger());
    }

    @Test
    public void readIntegerAndBoolean() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1", "true"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1, pr.readInteger());
        assertEquals(true, pr.readBoolean());
    }

    @Test
    public void readIntegerAndBooleanAndStringAndByte() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(new String[]{"1", "true", "test", "3"});
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(1, pr.readInteger());
        assertEquals(true, pr.readBoolean());
        assertEquals("test", pr.readString());
        assertEquals(3, pr.readByte());
    }

    @Test
    public void readStringArray() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        String[] orig = new String[]{"1", "true", "test", "3"};
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(orig);
        ParameterReader pr = new ParameterReader(intent);
        //Assert

        String[] target = pr.readStringArray();
        for (int i = 0; i < target.length; i++) {
            assertEquals(orig[i], target[i]);
        }
    }

    @Test
    public void readStringArrayWithArgs() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(null);

        ParameterReader pr = new ParameterReader(intent);
        String[] orig = new String[]{"1", "true", "test", "3"};

        //Assert

        String[] target = pr.readStringArray(orig);
        for (int i = 0; i < target.length; i++) {
            assertEquals(orig[i], target[i]);
        }
    }

    @Test
    public void readBooleanAndStringArrayWithArgs() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);

        String[] orig = new String[]{"true", "1", "2", "3"};
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(orig);

        ParameterReader pr = new ParameterReader(intent);

        //Assert
        assertEquals(true, pr.readBoolean());
        String[] target = pr.readStringArray();
        for (int i = 0; i < target.length; i++) {
            assertEquals(orig[i + 1], target[i]);
        }
    }

    @Test
    public void readBooleanAndIntegerAndStringArrayWithArgs() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);

        String[] orig = new String[]{"true", "1", "String 1", "String 2"};
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(orig);

        ParameterReader pr = new ParameterReader(intent);

        //Assert
        assertEquals(true, pr.readBoolean());
        assertEquals(1, pr.readInteger());
        String[] target = pr.readStringArray();
        for (int i = 0; i < target.length; i++) {
            assertEquals(orig[i + 2], target[i]);
        }
    }


    @Test
    public void readIntegerDefault() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(false);
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(0, pr.readInteger());
    }

    @Test
    public void readIntegerDefaultWithValue() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(false);
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(2, pr.readInteger(2));
//        assertEquals(true, pr.readBoolean());
    }

    @Test
    public void readBooleanDefault() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(false);
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(false, pr.readBoolean());
    }

    @Test
    public void readIntegerDefaultWithArgs() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(null);
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(0, pr.readInteger());
        assertEquals(false, pr.readBoolean());
    }

    @Test
    public void readIntegerAndBooleanDefaultWithArgs() {
        Intent intent = Mockito.mock(Intent.class);
        Mockito.when(intent.hasExtra("args")).thenReturn(true);
        Mockito.when(intent.getStringArrayExtra("args")).thenReturn(null);
        ParameterReader pr = new ParameterReader(intent);
        //Assert
        assertEquals(0, pr.readInteger());
        assertEquals(true, pr.readBoolean(true));
    }
}
