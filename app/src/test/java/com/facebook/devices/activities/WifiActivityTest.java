package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.WifiPresenter;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class WifiActivityTest {

    private ShadowTestActivity<WifiPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    private boolean keepWifiConnected = false;
    private String[] ssids = new String[]{"ssid1","ssid2","ssid3"};
    private String[] passwords = new String[]{"pass1","pass2","pass3"};
    private String baseUrl = "url";
    private int fileSize = 10;
    private boolean measureTransferRate = false;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, WifiActivity.class);
        intent.putExtra("keepWifiConnected", keepWifiConnected);
        intent.putExtra("ssid[0]", ssids[0]);
        intent.putExtra("ssid[1]", ssids[1]);
        intent.putExtra("ssid[2]", ssids[2]);
        intent.putExtra("password[0]", passwords[0]);
        intent.putExtra("password[1]", passwords[1]);
        intent.putExtra("password[2]", passwords[2]);
        intent.putExtra("baseUrl", baseUrl);
        intent.putExtra("fileSize", String.valueOf(fileSize));
        intent.putExtra("measureTransferRate", measureTransferRate);
        activityController = Robolectric.buildActivity(WifiActivity.class, intent);
        shadowTestActivity = new ShadowTestActivity<>((Activity) activityController.get());
        activity = (Activity) activityController.get();
    }

    @Test
    public void dataVerification(){
        /*Act*/
        activityController.create();
        /*Assert*/
        assertEquals(keepWifiConnected, shadowTestActivity.getAttributeValue("keepWifiConnected"));
        assertEquals(ssids[0], ((String[])shadowTestActivity.getAttributeValue("ssids"))[0]);
        assertEquals(ssids[1], ((String[])shadowTestActivity.getAttributeValue("ssids"))[1]);
        assertEquals(ssids[2], ((String[])shadowTestActivity.getAttributeValue("ssids"))[2]);
        assertEquals(passwords[0], ((String[])shadowTestActivity.getAttributeValue("passwords"))[0]);
        assertEquals(passwords[1], ((String[])shadowTestActivity.getAttributeValue("passwords"))[1]);
        assertEquals(passwords[2], ((String[])shadowTestActivity.getAttributeValue("passwords"))[2]);
        assertEquals(baseUrl, shadowTestActivity.getAttributeValue("baseUrl"));
        assertEquals(fileSize, shadowTestActivity.getAttributeValue("fileSize"));
        assertEquals(measureTransferRate, shadowTestActivity.getAttributeValue("measureTransferRate"));
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }


    @Test
    public void pausing() {
        /*Arrange*/
        WifiPresenter presenterMock = mock(WifiPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.resume();
        activityController.pause();
        /*Assert*/
        verify(presenterMock).onPause();
    }

    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void destroying() {
        /*Arrange*/
        WifiPresenter presenterMock = mock(WifiPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.destroy();
        /*Assert*/
        verify(presenterMock).onDestroy();
    }

    @Test
    public void onRequestPermissionResult() {
        /*Arrange*/
        RuntimePermissionHandler permissionHandlerMock = mock(RuntimePermissionHandler.class);
        shadowTestActivity.setPermissionHandler(permissionHandlerMock);
        int requestCode = 0;
        String[] permissions = new String[]{};
        int[] grantResults = new int[]{0, 1, 2};
        /*Act*/
        ((Activity) activityController.get()).onRequestPermissionsResult(requestCode, permissions, grantResults);
        /*Assert*/
        verify(permissionHandlerMock).onRequestPermissionResult(requestCode, permissions, grantResults);
    }

}
