package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.ColorPresenter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class ColorActivityTest {

    private ShadowTestActivity<ColorPresenter> shadowTestActivity;
    private ActivityController activityController;

    private String color = "#ffff0000";
    private boolean showMuraQuestion = true;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, ColorActivity.class);
        intent.putExtra("color[0]", color);
        intent.putExtra("showMuraQuestion", showMuraQuestion);
        activityController = Robolectric.buildActivity(ColorActivity.class, intent);
        shadowTestActivity = new ShadowTestActivity<>((Activity) activityController.get());
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }

    @Test
    public void resuming() {
        /*Arrange*/
        ColorPresenter presenterMock = mock(ColorPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.resume();
        /*Assert*/
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
        verify(presenterMock).onResume();
    }

    @Test
    public void pausing() {
        /*Arrange*/
        ColorPresenter presenterMock = mock(ColorPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.resume();
        activityController.pause();
        /*Assert*/
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
        verify(presenterMock).onPause();
    }
}
