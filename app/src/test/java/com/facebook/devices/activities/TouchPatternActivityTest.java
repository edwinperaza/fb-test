package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.presenter.TouchPatternPresenter;
import com.facebook.devices.utils.customviews.TouchDetectorView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class TouchPatternActivityTest {

    private ShadowTestActivity<TouchPatternPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    private int defaultOrientation = TouchDetectorView.ORIENTATION_HORIZONTAL;
    private int defaultSize = TouchPatternModel.SIX_SIX;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, TouchPatternActivity.class);
        intent.putExtra("defaultOrientation", String.valueOf(defaultOrientation));
        intent.putExtra("defaultSize", String.valueOf(defaultSize));
        activityController = Robolectric.buildActivity(TouchPatternActivity.class, intent);
        activity = (Activity) activityController.get();
        shadowTestActivity = new ShadowTestActivity<>(activity);
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertEquals(defaultOrientation, shadowTestActivity.getAttributeValue("defaultOrientation"));
        assertEquals(defaultSize, shadowTestActivity.getAttributeValue("defaultSize"));
        assertTrue(shadowTestActivity.isPresenterCreated());
    }

    @Test
    public void resuming() {
        /*Arrange*/
        activityController.create();
        /*Act*/
        activityController.resume();
        /*Assert*/
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }


    @Test
    public void pausing() {
        /*Act*/
        activityController.resume();
        activityController.pause();
        /*Assert*/
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }


    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }
}
