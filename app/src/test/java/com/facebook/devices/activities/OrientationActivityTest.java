package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.OrientationPresenter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class OrientationActivityTest {

    private ShadowTestActivity<OrientationPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, OrientationActivity.class);
        activityController = Robolectric.buildActivity(OrientationActivity.class, intent);
        activity = (Activity) activityController.get();
        shadowTestActivity = new ShadowTestActivity<>(activity);
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }

    @Test
    public void onSaveInstanceState() {
        /*Arrange*/
        Bundle bundleMock = mock(Bundle.class);
        OrientationPresenter presenterMock = mock(OrientationPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.saveInstanceState(bundleMock);
        /*Assert*/
        verify(presenterMock).saveInstanceState(bundleMock);
    }

    @Test
    public void resuming() {
        /*Arrange*/
        activityController.create();
        /*Act*/
        activityController.resume();
        /*Assert*/
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }


    @Test
    public void pausing() {
        /*Act*/
        activityController.resume();
        activityController.pause();
        /*Assert*/
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void onConfigurationChanged() {
        /*Arrange*/
        Configuration configMock = mock(Configuration.class);
        OrientationPresenter presenterMock = mock(OrientationPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activity.onConfigurationChanged(configMock);
        /*Assert*/
        verify(presenterMock).onConfigurationChanged(configMock);
    }

    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void destroying() {
        /*Arrange*/
        OrientationPresenter presenterMock = mock(OrientationPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.destroy();
        /*Assert*/
        verify(presenterMock).onDestroy();
    }
}
