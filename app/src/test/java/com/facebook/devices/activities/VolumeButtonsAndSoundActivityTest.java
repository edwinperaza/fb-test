package com.facebook.devices.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.ShadowTestActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.presenter.VolumeButtonsAndSoundPresenter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VolumeButtonsAndSoundActivityTest {

    private ShadowTestActivity<VolumeButtonsAndSoundPresenter> shadowTestActivity;
    private ActivityController activityController;
    private Activity activity;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, VolumeButtonsAndSoundActivity.class);
        activityController = Robolectric.buildActivity(VolumeButtonsAndSoundActivity.class, intent);
        activity = (Activity) activityController.get();
        shadowTestActivity = new ShadowTestActivity<>(activity);
    }

    @Test
    public void creation() {
        /*Act*/
        activityController.create();
        /*Assert*/
        assertTrue(shadowTestActivity.isPresenterCreated());
    }


    @Test
    public void resuming() {
        /*Arrange*/
        VolumeButtonsAndSoundPresenter presenterMock = mock(VolumeButtonsAndSoundPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.resume();
        /*Assert*/
        assertTrue(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
        verify(presenterMock).onResumed();
    }

    @Test
    public void pausing() {
        /*Arrange*/
        activityController.create();
        activityController.resume();
        /*Act*/
        activityController.pause();
        /*Assert*/
        assertFalse(BusProvider.isRegistered(shadowTestActivity.getPresenter()));
    }

    @Test
    public void onKeyDown() {
        /*Arrange*/
        VolumeButtonsAndSoundPresenter presenterMock = mock(VolumeButtonsAndSoundPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        int keyCode = 1;
        KeyEvent mockEvent = mock(KeyEvent.class);
        /*Act*/
        activity.onKeyDown(keyCode, mockEvent);
        /*Assert*/
        verify(presenterMock).onKeyDown(keyCode);
    }

    @Test
    public void starting() {
        /*Act*/
        activityController.create();
        activityController.start();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getBoundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void stopping() {
        /*Act*/
        activityController.create();
        activityController.stop();
        /*Assert*/
        assertEquals(ShadowApplication.getInstance().getUnboundServiceConnections().get(0), shadowTestActivity.getPresenter());
    }

    @Test
    public void destroying() {
        /*Arrange*/
        VolumeButtonsAndSoundPresenter presenterMock = mock(VolumeButtonsAndSoundPresenter.class);
        shadowTestActivity.setPresenter(presenterMock);
        /*Act*/
        activityController.destroy();
        /*Assert*/
        verify(presenterMock).onDestroy();
    }

}
