package com.facebook.devices.bluetoothspeakercheck;


import android.os.Bundle;

import com.facebook.devices.mvp.model.BluetoothSpeakerModel;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel.ConnectedBluetoothSuccess;
import com.facebook.devices.mvp.presenter.BluetoothSpeakerPresenter;
import com.facebook.devices.mvp.view.BluetoothSpeakerView;
import com.facebook.devices.mvp.view.BluetoothSpeakerView.ResultOptionPressedEvent;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.devices.mvp.model.BluetoothSpeakerModel.PERMISSION_BLUETOOTH_ACCEPTED;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BluetoothSpeakerPresenterTest {

    private BluetoothSpeakerPresenter presenter;

    @Mock
    private BluetoothSpeakerView view;

    @Mock
    private BluetoothSpeakerModel model;

    @Mock
    private Bundle savedInstantState;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new BluetoothSpeakerPresenter(model, view);
    }

    @Test
    public void onServiceDisconnectedTest() {
        //Act
        presenter.onServiceDisconnected(null);
        //Assert
        verify(model).removeService();
    }

    @Test
    public void onSuccessBluetoothConnectedTest() {
        //Arrange
        String name = "name";
        String address = "address";
        ConnectedBluetoothSuccess event = new ConnectedBluetoothSuccess(name, address);
        //Act
        presenter.onSuccessBluetoothConnected(event);
        //Assert
        verify(view).showConnectedMessage(event.getDeviceName(), event.getDeviceAddress());
    }

    @Test
    public void onDisconnectedBluetoothSuccessTest() {
        //Act
        presenter.onDisconnectedBluetoothSuccess(null);
        //Assert
        verify(view).showDisconnectedMessage();
    }

    @Test
    public void onFailedBluetoothBondedTest() {
        //Act
        presenter.onFailedBluetoothBonded(null);
        //Assert
        verify(view).showFailedBluetoothPairedMessage();
    }

    @Test
    public void onFailedBtnClickedTest() {
        //Act
        presenter.onFailedBtnClicked(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onResultPatterMessageInteractedNoTest() {
        //Arrange
        ResultOptionPressedEvent event = new ResultOptionPressedEvent(ResultOptionPressedEvent.NO);
        //Act
        presenter.onResultPatterMessageInteracted(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onResultPatterMessageInteractedYesTest() {
        //Arrange
        ResultOptionPressedEvent event = new ResultOptionPressedEvent(ResultOptionPressedEvent.YES);
        //Act
        presenter.onResultPatterMessageInteracted(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onInitDialogPressedEventBluetoothAdapterEnabledTest() {
        //Arrange
        when(model.isEnabledBluetoothAdapter()).thenReturn(true);
        when(model.wasDiscoverableBluetoothShown()).thenReturn(false);
        when(model.showDiscoverableView()).thenReturn(true);
        when(model.getBluetoothName()).thenReturn("name");
        //Act
        presenter.onInitDialogPressedEvent(null);
        //Assert
        verify(model).discoverableBluetoothDialogShown();
        verify(view).requestDiscoverableBluetooth();
        verify(view).showPairingBluetoothLayout(model.getBluetoothName());
    }

    @Test
    public void onInitDialogPressedEventBluetoothAdapterDisabledTest() {
        //Arrange
        when(model.isEnabledBluetoothAdapter()).thenReturn(false);
        //Act
        presenter.onInitDialogPressedEvent(null);
        //Assert
        verify(view).requestEnableBluetooth();
    }


    @Test
    public void onActivityResultEnableBluetoothTest() {
        //Arrange
        when(model.wasDiscoverableBluetoothShown()).thenReturn(false);
        when(model.showDiscoverableView()).thenReturn(true);
        when(model.getBluetoothName()).thenReturn("name");
        //Act
        presenter.onActivityResult(BluetoothSpeakerModel.REQUEST_ENABLE_BT, RESULT_OK);
        //Assert
        verify(model).permissionBluetoothAccepted();
        verify(view).showPairingBluetoothLayout(model.getBluetoothName());
        verify(model).discoverableBluetoothDialogShown();
        verify(view).requestDiscoverableBluetooth();
    }

    @Test
    public void onActivityResultCanceledTest() {
        //Act
        presenter.onActivityResult(0, RESULT_CANCELED);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void preCheckNoAvailableAdapterTest() {
        when(model.isAvailableBluetoothAdapter()).thenReturn(false);
        presenter.preCheck();
        verify(view).showNoAvailableBluetooth();
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void preCheckPermissionGrantedTest() {
        //Arrange
        when(model.isAvailableBluetoothAdapter()).thenReturn(true);
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.wasPermissionBluetoothAccepted()).thenReturn(false);
        //Act
        presenter.preCheck();
        //Assert
        verify(view).showTutorialButton();
        verify(model).registerReceiver();
        verify(model).permissionBluetoothAccepted();
        verify(view).showInitDialog();
    }

    @Test
    public void onDestroyTest() {
        //Act
        presenter.onDestroy();
        //Assert
        verify(model).onDestroy();
    }

    @Test
    public void onSaveInstantStateTest() {
        //Act
        presenter.onSaveInstantState(savedInstantState);
        //Assert
        verify(savedInstantState).putBoolean(PERMISSION_BLUETOOTH_ACCEPTED, model.wasPermissionBluetoothAccepted());
    }

    @Test
    public void onRequestPermissionGrantedResultTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.wasPermissionBluetoothAccepted()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionGrantedEvent());
        //Assert
        verify(view).showTutorialButton();
        verify(model).registerReceiver();
        verify(model).permissionBluetoothAccepted();
        verify(view).showInitDialog();
    }

    @Test
    public void onRequestPermissionDeniedResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionDeniedAndShouldRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(true);
        when(model.isAvailableBluetoothAdapter()).thenReturn(true);
        when(model.isPermissionGranted()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        //Assert
        verify(model).requestPermissions();
    }
}