package com.facebook.devices.bluetoothspeakercheck;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.BluetoothSpeakerActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.BluetoothSpeakerView;
import com.facebook.devices.mvp.view.BluetoothSpeakerView.FailedBtnPressedEvent;
import com.facebook.devices.mvp.view.BluetoothSpeakerView.OnInitDialogPressedEvent;
import com.facebook.devices.mvp.view.BluetoothSpeakerView.ResultOptionPressedEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowToast;

import static com.facebook.devices.mvp.model.BluetoothSpeakerModel.REQUEST_DISCOVERABLE_BT_SECONDS;
import static com.facebook.devices.mvp.view.BluetoothSpeakerView.ResultOptionPressedEvent.NO;
import static com.facebook.devices.mvp.view.BluetoothSpeakerView.ResultOptionPressedEvent.YES;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class BluetoothSpeakerViewTest {

    private BluetoothSpeakerActivity activity;
    private BluetoothSpeakerView view;

    private TextView infoMessage;
    private ConstraintLayout informationLayout;
    private LottieAnimationView lottieAnimationView;
    private FloatingTutorialView floatingTutorialView;

    @Captor
    private ArgumentCaptor<ResultOptionPressedEvent> argCaptorResultOption;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        MockitoAnnotations.initMocks(this);
        Intent intent = new Intent(RuntimeEnvironment.application, BluetoothSpeakerActivity.class);
        activity = Robolectric.buildActivity(BluetoothSpeakerActivity.class, intent).create().start().resume().get();
        view = new BluetoothSpeakerView(activity, bus);

        /*Bindings*/
        infoMessage = activity.findViewById(R.id.tv_waiting_message);
        informationLayout = activity.findViewById(R.id.cl_animation_container);
        lottieAnimationView = activity.findViewById(R.id.lottie_animation);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.bluetooth_tutorial_dialog_message);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(dialog));
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showInitDialogTest() {
        //Arrange
        String message = activity.getString(R.string.bluetooth_init_dialog_message);
        String okText = activity.getString(R.string.ok_base);

        //Act
        view.showInitDialog();
        TextView messageDialog = ShadowView.getCurrentBanner(view).getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(messageDialog.getText(), message);
        assertEquals(ShadowView.getCurrentBanner(view).getBannerTextOptionOne(), okText);
    }

    @Test
    public void showInitDialogOptionOneTest() {
        //Act
        view.showInitDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(OnInitDialogPressedEvent.class));
    }

    @Test
    public void showConnectedMessageTest() {
        //Arrange
        String deviceName = "name";
        String deviceAddress = "address";
        String message = activity.getString(R.string.bluetooth_test_result_title);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);
        String infoMessageString = activity.getString(R.string.bluetooth_connection_string, deviceName, deviceAddress);

        //Act
        view.showConnectedMessage(deviceName, deviceAddress);
        BannerView bannerCV = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerCV.getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(infoMessage.getText(), infoMessageString);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerCV.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerCV.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showConnectedMessageOptionOneTest() {
        //Arrange
        String deviceName = "name";
        String deviceAddress = "address";
        //Act
        view.showConnectedMessage(deviceName, deviceAddress);
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(argCaptorResultOption.capture());
        assertEquals(argCaptorResultOption.getValue().getOptionSelected(), YES);
    }

    @Test
    public void showConnectedMessageOptionTwoTest() {
        //Arrange
        String deviceName = "name";
        String deviceAddress = "address";
        //Act
        view.showConnectedMessage(deviceName, deviceAddress);
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        //Assert
        verify(bus).post(argCaptorResultOption.capture());
        assertEquals(argCaptorResultOption.getValue().getOptionSelected(), NO);
    }

    @Test
    public void showPairingBluetoothLayoutTest() {
        //Arrange
        String bluetoothName = "bluetoothName";
        String message = activity.getString(R.string.bluetooth_pairing_dialog_message);
        String negativeText = activity.getString(R.string.fail_base);
        String infoMessageString = activity.getString(R.string.bluetooth_waiting_message, bluetoothName);

        //Act
        view.showPairingBluetoothLayout(bluetoothName);
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(informationLayout.getVisibility(), View.VISIBLE);
        assertEquals(lottieAnimationView.getVisibility(), View.VISIBLE);
        assertEquals(infoMessage.getText(), infoMessageString);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showPairingBluetoothLayoutOptionOneTest() {
        //Arrange
        String bluetoothName = "bluetoothName";
        //Act
        view.showPairingBluetoothLayout(bluetoothName);
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        //Assert
        verify(bus).post(any(FailedBtnPressedEvent.class));
    }

    @Test
    public void showTutorialButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showNoAvailableBluetoothTest() {
        String noAvailableBluetooth = activity.getString(R.string.bluetooth_not_available);
        view.showNoAvailableBluetooth();
        assertEquals(ShadowToast.getTextOfLatestToast(), noAvailableBluetooth);
    }

    @Test
    public void showFailedBluetoothPairedMessageTest() {
        String failedPaired = activity.getString(R.string.bluetooth_device_paired_failed);
        view.showFailedBluetoothPairedMessage();
        assertEquals(ShadowToast.getTextOfLatestToast(), failedPaired);
        assertEquals(informationLayout.getVisibility(), View.GONE);
    }

    @Test
    public void showDisconnectedMessageTest() {
        String deviceDisconnected = activity.getString(R.string.bluetooth_device_disconnected);
        view.showDisconnectedMessage();
        assertEquals(ShadowToast.getTextOfLatestToast(), deviceDisconnected);
        assertEquals(informationLayout.getVisibility(), View.GONE);
    }

    @Test
    public void requestEnableBluetoothTest() {
        //Act
        view.requestEnableBluetooth();
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Intent intent = shadowActivity.peekNextStartedActivityForResult().intent;
        //Assert
        assertEquals(intent.getAction(), BluetoothAdapter.ACTION_REQUEST_ENABLE);
    }

    @Test
    public void requestDiscoverableBluetoothTest() {
        //Act
        view.requestDiscoverableBluetooth();
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Intent intent = shadowActivity.peekNextStartedActivityForResult().intent;
        //Assert
        assertEquals(intent.getAction(), BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        assertNotNull(intent.getExtras());
        assertNotNull(intent.getExtras().get(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION));
        assertEquals((int) intent.getExtras().get(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION), REQUEST_DISCOVERABLE_BT_SECONDS);
    }

    @Test
    public void resultOptionPressedEventTest() {
        ResultOptionPressedEvent questionOneEvent = new ResultOptionPressedEvent(YES);
        assertEquals(questionOneEvent.getOptionSelected(), YES);
    }

}