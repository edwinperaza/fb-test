package com.facebook.devices.bluetoothspeakercheck;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.activities.BluetoothSpeakerActivity;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel;
import com.facebook.devices.mvp.model.BluetoothSpeakerModel.ConnectedBluetoothSuccess;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowBluetoothAdapter;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class BluetoothSpeakerModelTest {

    private BluetoothSpeakerModel model;

    @Mock
    private BluetoothSpeakerActivity activity;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    private ShadowBluetoothAdapter shadowBluetoothAdapter;

    private int[] grantResults = new int[]{PERMISSION_GRANTED, 2, 3};

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BluetoothAdapter bluetoothAdapter = Shadow.newInstanceOf(BluetoothAdapter.class);
        shadowBluetoothAdapter = shadowOf(bluetoothAdapter);
        model = new BluetoothSpeakerModel(activity, bus, bluetoothAdapter, true,
                permissionHandler);
    }

    @Test
    public void isEnabledBluetoothAdapterTest() {
        //Arrange
        shadowBluetoothAdapter.setEnabled(true);
        //Assert
        assertEquals(model.isEnabledBluetoothAdapter(), true);
    }

    @Test
    public void permissionBluetoothAcceptedTest() {
        //Act
        model.permissionBluetoothAccepted();
        //Assert
        assertEquals(true, model.wasPermissionBluetoothAccepted());
    }

    @Test
    public void discoverableBluetoothDialogShownTest() {
        //Act
        model.discoverableBluetoothDialogShown();
        //Assert
        assertEquals(true, model.wasDiscoverableBluetoothShown());
    }

    @Test
    public void showDiscoverableViewTest() {
        //Arrange
        shadowBluetoothAdapter.setState(BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE);
        //Assert
        assertEquals(true, model.showDiscoverableView());
    }

    @Test
    public void isPermissionGrantedTest() {
        //Assert
        assertEquals(true, model.isPermissionGranted(grantResults));
    }

    @Test
    public void connectedBluetoothSuccessTest() {
        //Arrange
        String name = "name";
        String address = "address";
        ConnectedBluetoothSuccess event = new ConnectedBluetoothSuccess(name, address);
        //Assert
        assertEquals(name, event.getDeviceName());
        assertEquals(address, event.getDeviceAddress());
    }

    @Test
    public void shouldRequestPermissionsNegativeTest() {
        //Arrange
        when(activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)).thenReturn(false);
        when(activity.shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)).thenReturn(true);
        //Assert
        assertEquals(false, model.shouldRequestPermissions());
    }

    @Test
    public void getBluetoothNameTest() {
        assertNotNull(model.getBluetoothName());
    }
}