package com.facebook.devices.microphonecheck;

import com.facebook.devices.mvp.model.MicrophoneModel;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MicrophoneModelTest {

    private MicrophoneModel model;

    @Mock
    private AudioRecorder audioRecorder;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = new MicrophoneModel(permissionHandler, audioRecorder);
    }

    @Test
    public void getBufferSize() {
        /*Assert*/
        assertEquals(model.getBufferSize(), audioRecorder.getBufferSize());
    }


    @Test
    public void startRecording() {
        /*Arrange*/
        when(audioRecorder.isRunning()).thenReturn(false);
        AudioRecorder.AudioRecorderListener listener = mock(AudioRecorder.AudioRecorderListener.class);
        /*Act*/
        model.startRecording(listener);
        /*Assert*/
        verify(audioRecorder).setSampleRate(model.SAMPLE_RATE);
        verify(audioRecorder).startRecording(listener);
    }

    @Test
    public void stopRecording() {
        /*Arrange*/
        when(audioRecorder.isRunning()).thenReturn(true);
        /*Act*/
        model.stopRecording();
        /*Assert*/
        verify(audioRecorder).stopRecording();
    }

    @Test
    public void isRecordAudioThreadRunning(){
        /*Arrange*/
        when(audioRecorder.isRunning()).thenReturn(true);
        /*Assert*/
        assertTrue(model.isRecordAudioThreadRunning());
    }
}
