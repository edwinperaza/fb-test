package com.facebook.devices.microphonecheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.MicrophoneActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.MicrophoneView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.WaveGraphView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class MicrophoneViewTest {

    private MicrophoneActivity activity;
    private MicrophoneView view;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private WaveGraphView waveGraphView;
    private RelativeLayout waveLayout;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, MicrophoneActivity.class);
        activity = Robolectric.buildActivity(MicrophoneActivity.class, intent).create().start().resume().get();

        view = new MicrophoneView(activity, bus);

        /* Bindings */
        waveGraphView = activity.findViewById(R.id.wave_view);
        waveLayout = activity.findViewById(R.id.rl_wave_layout);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void init() {
        assertNotNull(view.getUiHandler());
    }

    @Test
    public void setStateMicroRecordingCheckTrue() {
        //Act
        view.setStateMicroRecording(true);
        //Assert
        assertThat(waveGraphView.getVisibility(), equalTo(View.VISIBLE));
        assertThat(waveLayout.getVisibility(), equalTo(View.VISIBLE));
    }

    @Test
    public void setStateMicroRecordingCheckFalse() {
        //Act
        view.setStateMicroRecording(false);
        //Assert
        assertThat(waveGraphView.getVisibility(), equalTo(View.GONE));
        assertThat(waveLayout.getVisibility(), equalTo(View.GONE));
    }

    @Test
    public void setAudioSampleCheck() {
        //Act
        short[] samples = new short[]{1, 2, 3, 4, 5, 6, 7};
        view.drawAudioSample(samples);
        //Assert
        assertThat(waveGraphView.getSamples(), equalTo(samples));
    }

    @Test
    public void showTutorial() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.microphone_tutorial_dialog_message);
        String positive = activity.getString(R.string.ok_base);
        /*Act*/
        view.onFloatingTutorialClicked();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        /*Assert*/
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positive);
    }

    @Test
    public void showInitDialog() {
        /*Arrange*/
        String dialogMsg = activity.getString(R.string.microphone_sound_wave_message);
        String optionOne = activity.getString(R.string.yes_base);
        String optionTwo = activity.getString(R.string.no_base);
        /*Act*/
        view.showInitialDialog();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(bannerView.getBannerOptionOneButton().getText(), optionOne);
        assertEquals(bannerView.getBannerOptionTwoButton().getText(), optionTwo);
        assertEquals(messageDialog.getText(), dialogMsg);
    }

    @Test
    public void showInitDialog_option_one() {
        /*Act*/
        view.showInitialDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        /*Assert*/
        verify(bus).post(any(MicrophoneView.ButtonSucceededClicked.class));
    }

    @Test
    public void showInitDialog_option_two() {
        /*Act*/
        view.showInitialDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        /*Assert*/
        verify(bus).post(any(MicrophoneView.ButtonFailedPressedEvent.class));
    }
}