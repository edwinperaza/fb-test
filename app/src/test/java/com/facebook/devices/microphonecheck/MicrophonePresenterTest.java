package com.facebook.devices.microphonecheck;

import android.os.Handler;

import com.facebook.devices.mvp.model.MicrophoneModel;
import com.facebook.devices.mvp.presenter.MicrophonePresenter;
import com.facebook.devices.mvp.view.MicrophoneView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.GenericUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GenericUtils.class)
public class MicrophonePresenterTest {

    @Mock
    private MicrophoneView view;
    @Mock
    private MicrophoneModel model;

    private MicrophonePresenter presenter;

    @Mock
    private Handler handlerMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(GenericUtils.class);
        when(view.getUiHandler()).thenReturn(handlerMock);

        presenter = new MicrophonePresenter(model, view);
    }

    @Test
    public void initState() {
        verify(view).showFloatingTutorialButton(eq(true));
    }

    @Test
    public void onPaused() {
        /*Act*/
        presenter.onPaused();
        /*Assert*/
        verify(model).stopRecording();
    }

    @Test
    public void viewResumedAudioPermissionGranted() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        //Act
        presenter.onResumed();
        //Assert
        verify(model).startRecording(any(AudioRecorder.AudioRecorderListener.class));
        verify(view).showInitialDialog();
        verify(view).setStateMicroRecording(true);
    }

    @Test
    public void viewResumedAudioPermissionNotGranted() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        //Act
        presenter.onResumed();
        //Assert
        verify(model).requestPermissions();
    }

    @Test
    public void viewResumedAudioPermissionRequest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        //Act
        presenter.onResumed();
        //Assert
        verify(model).requestPermissions();
    }

    @Test
    public void onSucceededButtonClickedCheck() {
        //Act
        presenter.onButtonSucceededPressed(null);
        //Assert
        verify(view).finishActivity();
    }

    @Test
    public void onFailedButtonClickedCheck() {
        //Act
        presenter.onButtonFailedPressed(null);
        //Assert
        verify(view).finishActivity();
    }

    @Test
    public void listenerCheck(){
        /*Arrange*/
        ArgumentCaptor<AudioRecorder.AudioRecorderListener> captor = ArgumentCaptor.forClass(AudioRecorder.AudioRecorderListener.class);
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        when(model.isPermissionGranted()).thenReturn(true);
        short[] data = new short[]{1,2,3,4};
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        verify(model).startRecording(captor.capture());
        captor.getValue().onAvailableData(data);
        //onAvailableData
        verify(view.getUiHandler()).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(view).drawAudioSample(eq(data));
        //onRecordCompleted
        captor.getValue().onRecordCompleted(data);
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        RuntimePermissionHandler.PermissionDeniedEvent event = new RuntimePermissionHandler.PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }
}
