package com.facebook.devices.initialscreen;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.InfoActivity;
import com.facebook.devices.mvp.view.InitialScreenView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class InitialScreenViewTest {

    private InfoActivity activity;
    private BusProvider.Bus bus;
    private InitialScreenView view;

    /*Bindings*/
    private TextView tvDeviceName;
    private TextView tvSerialNumber;
    private TextView tvVersionNumber;
    private TextView tvDeviceStatus;
    private TextView progressLabel;
    private View downloadResourceContainer;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, InfoActivity.class);
        activity = Robolectric.buildActivity(InfoActivity.class, intent).create().start().resume().get();
        bus = BusProvider.getInstance();

        view = new InitialScreenView(activity);

        /*Bindings*/
        tvDeviceName = activity.findViewById(R.id.tv_device_name);
        tvSerialNumber = activity.findViewById(R.id.tv_serial_number);
        tvVersionNumber = activity.findViewById(R.id.tv_version_number);
        tvDeviceStatus = activity.findViewById(R.id.tv_device_status);
        progressLabel = activity.findViewById(R.id.progress_label);
        downloadResourceContainer = activity.findViewById(R.id.download_resource_container);
    }

    @Test
    public void setDeviceName() {
        /*Arrange*/
        String deviceName = "Device";
        /*Act*/
        view.setDeviceName(deviceName);
        /*Assert*/
        assertEquals(tvDeviceName.getText().toString(), deviceName);
    }

    @Test
    public void setSerialNumber() {
        /*Arrange*/
        String serialNumber = "123456";
        /*Act*/
        view.setSerialNumber(serialNumber);
        /*Assert*/
        assertEquals(tvSerialNumber.getText().toString(), serialNumber);
        assertEquals(tvSerialNumber.getVisibility(), View.VISIBLE);
    }

    @Test
    public void setApkName() {
        /*Arrange*/
        String versionNumber = "1.0.3";
        /*Act*/
        view.setVersionNumber(versionNumber);
        /*Assert*/
        assertEquals(tvVersionNumber.getText().toString(), versionNumber);
    }

    @Test
    public void showConnected() {
        /*Arrange*/
        String message = activity.getString(R.string.connected);
        /*Act*/
        view.showConnected();
        /*Assert*/
        assertEquals(tvDeviceStatus.getText().toString(), message);
    }

    @Test
    public void showDisconnected() {
        /*Arrange*/
        String message = activity.getString(R.string.disconnected);
        /*Act*/
        view.showDisconnected();
        /*Assert*/
        assertEquals(tvDeviceStatus.getText().toString(), message);
    }

    @Test
    public void showDownloadingTest() {
        //Arrange
        String message = activity.getString(R.string.downloading_resources);
        //Act
        view.showDownloading();
        //Assert
        assertEquals(progressLabel.getText().toString(), message);
        assertEquals(downloadResourceContainer.getVisibility(), View.VISIBLE);
    }

    @Test
    public void hideDownloadingTest() {
        //Act
        view.hideDownloading();
        //Assert
        assertEquals(downloadResourceContainer.getVisibility(), View.GONE);
    }

    @Test
    public void showTransferringTest() {
        //Arrange
        String message = activity.getString(R.string.transferring_resources);
        //Act
        view.showTransferring();
        //Assert
        assertEquals(progressLabel.getText().toString(), message);
        assertEquals(downloadResourceContainer.getVisibility(), View.VISIBLE);
    }

    @Test
    public void hideTransferringTest() {
        //Act
        view.hideTransferring();
        //Assert
        assertEquals(downloadResourceContainer.getVisibility(), View.GONE);
    }
}
