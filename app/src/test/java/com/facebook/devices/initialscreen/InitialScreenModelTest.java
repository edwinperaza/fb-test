package com.facebook.devices.initialscreen;

import android.os.Build;

import com.facebook.devices.mvp.model.InitialScreenModel;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class InitialScreenModelTest {

    private InitialScreenModel model;

    private String deviceId = "Device123";
    private String sessionId = "312abc21b3basb3";

    @Before
    public void setup() {
        model = new InitialScreenModel(deviceId, sessionId);
    }

    static void setFinalStatic(Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(null, newValue);
    }

    @Test
    public void getDeviceName() {
        /*Arrange*/
        try {
            setFinalStatic(Build.class.getField("MODEL"), "Aloha");
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*Assert*/
        assertNotNull(model.getDeviceName());
    }

    @Test
    public void getSerialNumber() {
        /*Assert*/
        assertEquals(deviceId, model.getSerialNumber());
    }

    @Test
    public void getSessionId() {
        /*Assert*/
        assertEquals(sessionId, model.getSessionId());
    }

    @Test
    public void getApkName() {
        /*Assert*/
        assertNotNull(model.getApkName());
    }

    @Test
    public void getVersionNumber() {
        /*Assert*/
        assertNotNull(model.getVersionNumber());
    }
}
