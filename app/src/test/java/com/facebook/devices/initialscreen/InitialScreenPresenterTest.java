package com.facebook.devices.initialscreen;

import com.facebook.devices.mvp.model.InitialScreenModel;
import com.facebook.devices.mvp.presenter.InitialScreenPresenter;
import com.facebook.devices.mvp.view.InitialScreenView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class InitialScreenPresenterTest {

    private InitialScreenPresenter presenter;

    @Mock
    private InitialScreenView view;
    @Mock
    private InitialScreenModel model;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new InitialScreenPresenter(model, view);
    }

    @Test
    public void onResume_Connected() {
        /*Arrange*/
        when(model.isConnected()).thenReturn(true);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(view, times(2)).setDeviceName(model.getDeviceName());
        verify(view, times(2)).setApkName(model.getApkName());
        verify(view, times(2)).setVersionNumber(model.getVersionNumber());
        verify(view).showConnected();
        verify(view, times(2)).setSerialNumber(model.getSerialNumber());
    }

    @Test
    public void onResume_Disconnected() {
        /*Arrange*/
        when(model.isConnected()).thenReturn(false);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(view, times(2)).setDeviceName(model.getDeviceName());
        verify(view, times(2)).setApkName(model.getApkName());
        verify(view, times(2)).setVersionNumber(model.getVersionNumber());
        verify(view, times(2)).showDisconnected();
        verify(view, times(2)).setSerialNumber(model.getSerialNumber());
    }

    @Test
    public void onServiceConnected() {
        /*Arrange*/
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        /*Act*/
        presenter.onServiceConnected(null, binder);
        /*Assert*/
        verify(model).setService(binder.getService());
    }

    @Test
    public void onServiceDisconnected() {
        /*Act*/
        presenter.onServiceDisconnected(null);
        /*Assert*/
        verify(model).removeService();
    }

    @Test
    public void onConnected_True() {
        /*Arrange*/
        boolean isConnected = true;
        /*Act*/
        presenter.onConnected(isConnected);
        /*Assert*/
        verify(view).showConnected();
    }

    @Test
    public void onConnected_False() {
        /*Arrange*/
        boolean isConnected = false;
        /*Act*/
        presenter.onConnected(isConnected);
        /*Assert*/
        verify(view, times(2)).showDisconnected();
    }

    @Test
    public void showDownloadingTest() {
        //Act
        presenter.showDownloading(true);
        //Assert
        verify(view).showDownloading();
    }

    @Test
    public void hideDownloadingTest() {
        //Act
        presenter.showDownloading(false);
        //Assert
        verify(view).hideDownloading();
    }

    @Test
    public void showTransferringTest() {
        //Act
        presenter.showTransferring(true);
        //Assert
        verify(view).showTransferring();
    }

    @Test
    public void hideTransferringTest() {
        //Act
        presenter.showTransferring(false);
        //Assert
        verify(view).hideTransferring();
    }
}
