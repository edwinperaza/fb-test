package com.facebook.devices.finalscreencheck;


import com.facebook.devices.mvp.model.FinalScreenModel;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class FinalScreenModelTest {

    private FinalScreenModel model;
    private long millisToClose = 5000;

    @Before
    public void setup() {
        model = new FinalScreenModel(FinalScreenModel.MODE_SUCCESS, millisToClose);
    }

    @Test
    public void getMode() {
        assertEquals(model.getMode(), FinalScreenModel.MODE_SUCCESS);
    }

    @Test
    public void changeMode() {
        //Arrange
        int mode = FinalScreenModel.MODE_FAILURE;
        //Act
        model.setMode(mode);
        //Assert
        assertEquals(model.getMode(), FinalScreenModel.MODE_FAILURE);
    }

    @Test
    public void getMillis() {
        assertEquals(model.getMillisToClose(), millisToClose);
    }
}
