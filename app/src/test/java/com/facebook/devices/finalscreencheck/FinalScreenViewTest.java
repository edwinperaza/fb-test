package com.facebook.devices.finalscreencheck;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.FinalScreenActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.FinalScreenView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class FinalScreenViewTest {

    private FinalScreenView view;
    private Activity activity;

    /*Bindings*/
    private ImageView ivIcon;
    private TextView tvFinalMessage;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, FinalScreenActivity.class);
        activity = Robolectric.buildActivity(FinalScreenActivity.class, intent).create().start().resume().get();

        view = new FinalScreenView(activity, BusProvider.getInstance());

        /*Bindings*/
        ivIcon = activity.findViewById(R.id.iv_icon);
        tvFinalMessage = activity.findViewById(R.id.tv_final_message);
    }

    @Test
    public void morphSuccessMode() {
        //Arrange
        String text = activity.getString(R.string.final_success_message) + " " + activity.getString(R.string.final_success_message_bold);
        Drawable icon = ContextCompat.getDrawable(activity, R.drawable.success_icon);
        //Act
        view.successMode();
        //Assert
        assertEquals(ivIcon.getDrawable(), icon);
        assertEquals(tvFinalMessage.getText().toString().replace("\n",""), text);
    }

    @Test
    public void morphFailureMode() {
        //Arrange
        String text = activity.getString(R.string.final_failure_message) + " " + activity.getString(R.string.final_failure_message_bold);
        Drawable icon = ContextCompat.getDrawable(activity, R.drawable.fail_icon);
        //Act
        view.failureMode();
        //Assert
        assertEquals(ivIcon.getDrawable(), icon);
        assertEquals(tvFinalMessage.getText().toString().replace("\n",""), text);
    }
}
