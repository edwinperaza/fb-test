package com.facebook.devices.finalscreencheck;


import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.FinalScreenActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.FinalScreenModel;
import com.facebook.devices.mvp.presenter.FinalScreenPresenter;
import com.facebook.devices.mvp.view.FinalScreenView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowLooper;

import static com.facebook.devices.mvp.model.FinalScreenModel.MODE_FAILURE;
import static com.facebook.devices.mvp.model.FinalScreenModel.MODE_SUCCESS;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
@Ignore /*TODO - fix this unit test in the next iteration */
public class FinalScreenPresenterTest {

    private FinalScreenPresenter presenter;

    @Mock
    private FinalScreenModel model;

    private Activity activity;
    private FinalScreenView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, FinalScreenActivity.class);
        activity = Robolectric.buildActivity(FinalScreenActivity.class, intent).create().start().resume().get();

        view = new FinalScreenView(activity, BusProvider.getInstance());

        presenter = new FinalScreenPresenter(model, view);
    }

   /* @Test
    public void renderingUI_Success() {
        *//*Arrange*//*
        when(model.getMode()).thenReturn(MODE_SUCCESS);
        *//*Act*//*
        presenter.renderingUI();
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();
        *//*Assert*//*
        verify(model, times(2)).pass();
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        assertTrue(shadowActivity.isFinishing());

        *//*UI check*//*
        *//*Arrange*//*
        String text = activity.getString(R.string.final_success_message);
        Drawable icon = ContextCompat.getDrawable(activity, R.drawable.success_icon);
        *//*Assert*//*
        assertEquals(((ImageView) activity.findViewById(R.id.iv_icon)).getDrawable(), icon);
        assertEquals(((TextView) activity.findViewById(R.id.tv_final_message)).getText(), text);
    }

    @Test
    public void renderingUI_Failure() {
        *//*Arrange*//*
        when(model.getMode()).thenReturn(MODE_FAILURE);
        *//*Act*//*
        presenter.renderingUI();
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();
        *//*Assert*//*
        verify(model, times(2)).pass();
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        assertTrue(shadowActivity.isFinishing());

        *//*UI check*//*
        *//*Arrange*//*
        String text = activity.getString(R.string.final_failure_message);
        Drawable icon = ContextCompat.getDrawable(activity, R.drawable.fail_icon);
        *//*Assert*//*
        assertEquals(((ImageView) activity.findViewById(R.id.iv_icon)).getDrawable(), icon);
        assertEquals(((TextView) activity.findViewById(R.id.tv_final_message)).getText(), text);
    }

    @Test
    public void onServiceConnected() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        *//*Act*//*
        presenter.onServiceConnected(null, binder);
        *//*Assert*//*
        verify(model).setService(binder.getService());
    }

    @Test
    public void onServiceDisconnected() {
        *//*Act*//*
        presenter.onServiceDisconnected(null);
        *//*Assert*//*
        verify(model).removeService();
    }*/

}
