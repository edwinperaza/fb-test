package com.facebook.devices.multitouchcheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.MultitouchActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.MultitouchView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.view.MultitouchView.OnFailureDialogOptionSelected.OPTION_FAIL;
import static com.facebook.devices.mvp.view.MultitouchView.OnFailureDialogOptionSelected.OPTION_RETRY;
import static com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_DROPPED;
import static com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_SUCCESS;
import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class MultitouchViewTest {

    private MultitouchActivity activity;
    private MultitouchView view;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private FloatingTutorialView floatingTutorialView;
    private com.facebook.devices.utils.customviews.MultitouchView multitouchView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, MultitouchActivity.class);
        activity = Robolectric.buildActivity(MultitouchActivity.class, intent).create().start().resume().get();

        view = new MultitouchView(activity, bus);

        /* Bindings */
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        multitouchView = activity.findViewById(R.id.multitouch_view);
    }

    @Test
    public void multitouchListener_NotNull() {
        assertNotNull(multitouchView.getListener());
    }

    @Test
    public void showFloatingTutorialButton() {
        /*Act*/
        view.showFloatingTutorialButton();
        /*Assert*/
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showDialogOne_UI() {
        /*Arrange*/
        String message = activity.getString(R.string.multitouch_dialog_one_message);
        String positiveOption = activity.getString(R.string.ok_base);
        /*Act*/
        view.showDialogOne();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView tvMessage = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(tvMessage.getText().toString(), message);
        assertEquals(bannerView.getBannerOptionOneButton().getText().toString(), positiveOption);
        assertEquals(bannerView.getBannerOptionTwoButton().getVisibility(), View.GONE);
        assertEquals(bannerView.getBannerOptionThreeButton().getVisibility(), View.GONE);
    }

    @Test
    public void showDialogOne_Behavior() {
        /*Act*/
        view.showDialogOne();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        /*Assert*/
        verify(bus).post(any(MultitouchView.InitDialogEvent.class));
    }

    @Test
    public void hideDialogOne() {
        /*Act*/
        view.showDialogOne();
        view.hideDialogOne();
        /*Assert*/
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void hideFloatingTutorialButton() {
        /*Act*/
        view.hideFloatingButton();
        /*Assert*/
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorial() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.multitouch_tutorial_dialog_message);
        String positiveText = activity.getString(R.string.ok_base);
        /*Act*/
        view.onFloatingTutorialClicked();
        /*Assert*/
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertEquals(title, shadowAlertDialog.getTitle());
        assertEquals(message, shadowAlertDialog.getMessage());
        assertEquals(positiveText, dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText());
    }

    @Test
    public void showDroppedDialog_UI() {
        /*Arrange*/
        String title = activity.getString(R.string.multitouch_dropped_dialog_title);
        String message = activity.getString(R.string.multitouch_dropped_dialog_message);
        String positiveText = activity.getString(R.string.multitouch_dropped_option_one_message);
        String negativeText = activity.getString(R.string.fail_base);
        /*Act*/
        view.showDroppedDialog();
        /*Assert*/
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertEquals(title, shadowAlertDialog.getTitle());
        assertEquals(message, shadowAlertDialog.getMessage());
        assertEquals(positiveText, dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText());
        assertEquals(negativeText, dialog.getButton(DialogInterface.BUTTON_NEGATIVE).getText());
    }

    @Test
    public void showDroppedDialog_Behavior_Positive() {
        /*Act positive*/
        view.showDroppedDialog();
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        /*Positive*/
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        ArgumentCaptor<MultitouchView.OnFailureDialogOptionSelected> captorPositive = ArgumentCaptor.forClass(MultitouchView.OnFailureDialogOptionSelected.class);
        verify(bus).post(captorPositive.capture());
        assertEquals(captorPositive.getValue().getOptionSelected(), OPTION_RETRY);
    }

    @Test
    public void showDroppedDialog_Behavior_Negative() {
        view.showDroppedDialog();
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        /*Positive*/
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        ArgumentCaptor<MultitouchView.OnFailureDialogOptionSelected> captorNegative = ArgumentCaptor.forClass(MultitouchView.OnFailureDialogOptionSelected.class);
        verify(bus).post(captorNegative.capture());
        assertEquals(captorNegative.getValue().getOptionSelected(), OPTION_FAIL);
    }

    @Test
    public void resetMultitouch() {
        /*Act*/
        view.resetMultitouch();
        /*Assert*/
        assertTrue(multitouchView.areItemsNotCompletedReset());
    }


    @Test
    public void enableMultitouchComponent() {
        /*Act*/
        view.enableMultitouchComponent();
        /*Assert*/
        assertTrue(multitouchView.isEnabled());
    }

    @Test
    public void disableMultitouchComponent() {
        /*Act*/
        view.disableMultitouchComponent();
        /*Assert*/
        assertFalse(multitouchView.isEnabled());
    }

    @Test
    public void onMultitouchRepliesEvent() {
        /*Arrange*/
        MultitouchView.OnMultitouchReplies eventA = new MultitouchView.OnMultitouchReplies(FINISHED_DROPPED);
        MultitouchView.OnMultitouchReplies eventB = new MultitouchView.OnMultitouchReplies(FINISHED_SUCCESS);
        /*Assert*/
        assertEquals(eventA.getResultMode(), FINISHED_DROPPED);
        assertEquals(eventB.getResultMode(), FINISHED_SUCCESS);
    }

    @Test
    public void onFailureDialogOptionSelectedEvent() {
        /*Arrange*/
        MultitouchView.OnFailureDialogOptionSelected eventA = new MultitouchView.OnFailureDialogOptionSelected(OPTION_FAIL);
        MultitouchView.OnFailureDialogOptionSelected eventB = new MultitouchView.OnFailureDialogOptionSelected(OPTION_RETRY);
        /*Assert*/
        assertEquals(eventA.getOptionSelected(), OPTION_FAIL);
        assertEquals(eventB.getOptionSelected(), OPTION_RETRY);
    }

}
