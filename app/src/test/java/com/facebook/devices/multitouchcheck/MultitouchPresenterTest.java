package com.facebook.devices.multitouchcheck;


import com.facebook.devices.mvp.model.MultitouchModel;
import com.facebook.devices.mvp.presenter.MultitouchPresenter;
import com.facebook.devices.mvp.view.MultitouchView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class MultitouchPresenterTest {

    @Mock
    private MultitouchView view;
    @Mock
    private MultitouchModel model;

    private MultitouchPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new MultitouchPresenter(model, view);
    }

    @Test
    public void init() {
        verify(view).showDialogOne();
        verify(view).showFloatingTutorialButton();
        verify(view).disableMultitouchComponent();
    }

    @Test
    public void onBannerCVEvent() {
        /*Act*/
        presenter.onBannerCVEvent(null);
        /*Assert*/
        verify(view).hideDialogOne();
        verify(view).hideFloatingButton();
        verify(view).enableMultitouchComponent();
    }


    @Test
    public void onMultitouchRepliesSuccess() {
        //Arrange
        MultitouchView.OnMultitouchReplies event = new MultitouchView.OnMultitouchReplies(com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_SUCCESS);
        //Act
        presenter.onMultitouchReplies(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onMultitouchRepliesDropped() {
        //Arrange
        MultitouchView.OnMultitouchReplies event = new MultitouchView.OnMultitouchReplies(com.facebook.devices.utils.customviews.MultitouchView.MultiTouchDetectorViewCallback.FINISHED_DROPPED);
        //Act
        presenter.onMultitouchReplies(event);
        //Assert
        verify(view).showDroppedDialog();
    }

    @Test
    public void onFailureDialogOptionSelectedRetry() {
        //Arrange
        MultitouchView.OnFailureDialogOptionSelected event = new MultitouchView.OnFailureDialogOptionSelected(MultitouchView.OnFailureDialogOptionSelected.OPTION_RETRY);
        //Act
        presenter.onFailureDialogOptionSelected(event);
        //Assert
        verify(view).resetMultitouch();
    }

    @Test
    public void onFailureDialogOptionSelectedFail() {
        //Arrange
        MultitouchView.OnFailureDialogOptionSelected event = new MultitouchView.OnFailureDialogOptionSelected(MultitouchView.OnFailureDialogOptionSelected.OPTION_FAIL);
        //Act
        presenter.onFailureDialogOptionSelected(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

}
