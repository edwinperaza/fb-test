package com.facebook.devices.previewimage;

import android.content.Context;
import android.net.Uri;

import com.facebook.devices.mvp.model.PreviewImagesModel;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class PreviewImagesModelTest {

    private PreviewImagesModel model;

    @Mock
    private Context context;

    private Uri[] imageResources = new Uri[]{
            Uri.EMPTY, Uri.EMPTY, Uri.EMPTY, Uri.EMPTY
    };

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PermissionHandler permissionHandler = Mockito.mock(RuntimePermissionHandler.class);
        model = new PreviewImagesModel(context, permissionHandler, imageResources);
    }

    @Test
    public void dataCheck() {
        //Assert
        assertTrue(model.getNumberOfImages() == imageResources.length);
    }

    @Test
    public void resetPosition() {
        //Act
        model.resetPosition();
        //Assert
        assertEquals(0, model.getImagePosition());
    }

    @Test
    public void getPreviousImageIdTest() {
        //Arrange
        int position = 1;
        //Act
        model.setImagePosition(position);
        model.movePreviousImagePosition();
        //Assert
        assertEquals(position - 1, model.getImagePosition());
    }

    @Test
    public void isLastImageTest() {
        //Act
        model.setImagePosition(imageResources.length - 1);
        //Assert
        assertTrue(model.isLastImage());
    }

    @Test
    public void moveThroughAllImagesAndAskForResult() {
        //Act
        for (Uri uri : imageResources) {
            model.moveNextImagePosition();
        }

        //Assert
        assertTrue(model.isLastImage());
        assertTrue(model.shouldAskResult());
    }

    @Test
    public void moveBackaAndFortThroughAllImagesAndShouldNotAskForResult() {
        //Act
        for (Uri uri : imageResources) {
            model.moveNextImagePosition();
        }

        assertTrue(model.isLastImage());
        assertTrue(model.shouldAskResult());

        for (Uri uri : imageResources) {
            model.movePreviousImagePosition();
        }

        //Assert
        assertFalse(model.isLastImage());
        assertFalse(model.shouldAskResult());
        assertEquals(0, model.getImagePosition());
    }

    @Test
    public void getResourcePositionTest() {
        //Arrange
        int position = 1;
        //Act
        model.setImagePosition(position);
        //Assert
        assertEquals(position, model.getImagePosition());
    }
}
