package com.facebook.devices.previewimage;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.view.MotionEvent;

import com.facebook.devices.mvp.model.PreviewImagesModel;
import com.facebook.devices.mvp.presenter.PreviewImagesPresenter;
import com.facebook.devices.mvp.view.PreviewImagesView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.facebook.devices.mvp.view.PreviewImagesView.ResultDialogEvent.RESULT_NEGATIVE;
import static com.facebook.devices.mvp.view.PreviewImagesView.ResultDialogEvent.RESULT_POSITIVE;
import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GenericUtils.class)
public class PreviewImagesPresenterTest {

    private PreviewImagesPresenter presenter;

    @Mock
    private Context context;

    @Mock
    private PreviewImagesModel model;

    @Mock
    private PreviewImagesView view;

    @Mock
    private MotionEvent event;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockStatic(GenericUtils.class);
        when(view.getUiHandler()).thenReturn(mockHandler);
        presenter = new PreviewImagesPresenter(model, view);
    }

    @Test
    public void init() {
        //Assert
        verify(view).setupView(model.getImages());
    }

    @Test
    public void addOnPageChangeListenerRegularPositionTest() {
        //Arrange
        int position = 1;
        when(model.getImagePosition()).thenReturn(position);
        // Act
        presenter.onNextButtonPressedEvent(new PreviewImagesView.NextButtonPressedEvent());
        //Assert
        verify(model, times(1)).moveNextImagePosition();
        verify(view, times(1)).changeImage(position);
        verify(view, times(1)).previousImageIsVisible(true);
        verify(view, times(1)).nextImageIsVisible(true);
    }

    @Test
    public void addOnPageChangeListenerInitPositionTest() {
        //Arrange
        int position = 0;
        //Assert
        presenter.onPageScrolledEvent(new PreviewImagesView.PageSelectedEvent(position));
        verify(model).setImagePosition(position);
        verify(view).nextImageIsVisible(true);
        verify(view).previousImageIsVisible(false);
    }

    @Test
    public void onPageChangedToLastPositionTest() {
        //Arrange
        int position = 4;
        when(model.isLastImage()).thenReturn(true);
        when(model.getImagePosition()).thenReturn(position);
        presenter.onPageScrolledEvent(new PreviewImagesView.PageSelectedEvent(position));
        //Assert
        verify(view).previousImageIsVisible(true);
        verify(view).nextImageIsVisible(false);
        verify(model).setImagePosition(position);
    }

    @Test
    public void setOnTouchListenerTutorialVisibleTest() {
        //Arrange
        when(event.getAction()).thenReturn(MotionEvent.ACTION_UP);
        when(view.isTutorialVisible()).thenReturn(false);
        presenter.onPageTouchedEvent(new PreviewImagesView.PageTouchedEvent(event));
        //Assert
        verify(view).showTutorialButton();
    }

    @Test
    public void setOnTouchListenerTutorialNotVisibleTest() {
        //Arrange
        when(event.getAction()).thenReturn(MotionEvent.ACTION_UP);
        when(view.isTutorialVisible()).thenReturn(true);
        presenter.onPageTouchedEvent(new PreviewImagesView.PageTouchedEvent(event));
        //Assert
        verify(view, never()).showTutorialButton();
    }

    @Test
    public void onResumeIsPermissionGrantedAndLastImageTest() {
        /*Arrange*/
        final Handler immediateHandler = mock(Handler.class);
        when(immediateHandler.postDelayed(any(Runnable.class), anyLong())).thenAnswer((Answer) invocation -> {
            Runnable msg = invocation.getArgument(0);
            msg.run();
            return null;
        });

        when(view.getUiHandler()).thenReturn(immediateHandler);

        Uri[] imageResources = new Uri[]{
                Uri.EMPTY
        };

        RuntimePermissionHandler handler = Mockito.mock(RuntimePermissionHandler.class);
        PreviewImagesModel model = new PreviewImagesModel(context, handler, imageResources);
        PreviewImagesPresenter presenter = new PreviewImagesPresenter(model, view);
        when(handler.isPermissionGranted()).thenReturn(true);

        //Act
        presenter.onResume();

        //Assert
        verify(handler).isPermissionGranted();
        verify(view).setViewPagerAdapter();

        assertEquals(0, model.getImagePosition());
        verify(view).changeImage(0);

        verify(view).showResultDialog();
    }

    @Test
    public void onResumeIsPermissionGrantedAndNotLastImageTest() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.isLastImage()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(model).resetPosition();
        verify(view).setViewPagerAdapter();

        verify(view).previousImageIsVisible(false);
        verify(view).nextImageIsVisible(true);
        verify(view).changeImage(0);
        verify(view, never()).showResultDialog();
    }

    @Test
    public void onResumeIsPermissionGrantedAndImagePositionNegativeTest() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.isLastImage()).thenReturn(false);
        when(model.getImagePosition()).thenReturn(-1);
        //Act
        presenter.onResume();
        //Assert
        verify(model).resetPosition();
        verify(view).setViewPagerAdapter();

        verify(view).previousImageIsVisible(false);
        verify(view).nextImageIsVisible(false);
        verify(view).changeImage(-1);
        verify(view, never()).showResultDialog();
    }

    @Test
    public void onResumeIsPermissionNotGrantedAndShouldRequestPermissionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        when(model.shouldRequestPermissions()).thenReturn(true);
        //Act
        presenter.onResume();
        //Assert
        verify(model, never()).resetPosition();
        verify(view, never()).setViewPagerAdapter();
    }

    @Test
    public void onResumeIsPermissionNotGrantedAndNotShouldRequestPermissionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onResume();
        //Assert
        verify(model).requestPermissions();
    }

    @Test
    public void onServiceConnected() {
        //Arrange
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        //Act
        presenter.onServiceConnected(null, binder);
        //Assert
        verify(model).setService(binder.getService());
    }

    @Test
    public void onServiceDisconnected() {
        //Act
        presenter.onServiceDisconnected(null);
        //Assert
        verify(model).removeService();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onStopTest() {
        //Act
        presenter.onStop();
        //Assert
        verify(view.getUiHandler(), times(2)).removeCallbacks(any(Runnable.class));
    }

    @Test
    public void onResultDialogEvent_ResultPositive() {
        //Arrange
        PreviewImagesView.ResultDialogEvent event = new PreviewImagesView.ResultDialogEvent(RESULT_POSITIVE);
        //Act
        presenter.onResultDialogEvent(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onResultDialogEvent_ResultNegative() {
        //Arrange
        PreviewImagesView.ResultDialogEvent event = new PreviewImagesView.ResultDialogEvent(RESULT_NEGATIVE);
        //Act
        presenter.onResultDialogEvent(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onPreviousButtonPressedEventTest() {
        //Arrange
        PreviewImagesView.PreviousButtonPressedEvent event = new PreviewImagesView.PreviousButtonPressedEvent();
        //Act
        presenter.onPreviousButtonPressedEvent(event);
        //Assert
        verify(model).movePreviousImagePosition();
    }

    @Test
    public void onNextButtonPressedEventTest() {
        //Arrange
        PreviewImagesView.NextButtonPressedEvent event = new PreviewImagesView.NextButtonPressedEvent();
        //Act
        presenter.onNextButtonPressedEvent(event);
        //Assert
        verify(model).moveNextImagePosition();
    }
}
