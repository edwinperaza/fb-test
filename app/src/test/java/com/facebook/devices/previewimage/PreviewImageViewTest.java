package com.facebook.devices.previewimage;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.PreviewImagesActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.PreviewImagesView;
import com.facebook.devices.mvp.view.PreviewImagesView.NextButtonPressedEvent;
import com.facebook.devices.mvp.view.PreviewImagesView.PageSelectedEvent;
import com.facebook.devices.mvp.view.PreviewImagesView.PreviousButtonPressedEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class PreviewImageViewTest {

    private PreviewImagesView view;
    private PreviewImagesActivity activity;

    /*Bindings*/
    private ViewPager viewPager;
    private FloatingTutorialView floatingTutorialView;
    private ImageView previousImage;
    private ImageView nextImage;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, PreviewImagesActivity.class);
        activity = Robolectric.buildActivity(PreviewImagesActivity.class, intent).create().start().resume().get();

        view = new PreviewImagesView(activity, bus);

        /*Bindings*/
        viewPager = activity.findViewById(R.id.view_pager);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        previousImage = activity.findViewById(R.id.iv_prev_button);
        nextImage = activity.findViewById(R.id.iv_next_button);
    }

    @Test
    public void initTest() {
        //Arrange
        Uri[] uris = new Uri[]{Uri.parse(""), Uri.parse("")};
        int position = 1;
        ArgumentCaptor<PageSelectedEvent> event = ArgumentCaptor.forClass(PageSelectedEvent.class);
        //Act
        view.setupView(uris);
        view.setViewPagerAdapter();
        viewPager.setCurrentItem(position);
        //Assert
        assertNotNull(view.getViewPager().getAdapter());
        verify(bus).post(event.capture());
        assertEquals(position, event.getValue().position);
    }

    @Test
    public void showTutorial() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String dialogMessage = activity.getString(R.string.images_preview_tutorial_text);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);
        //Assert
        assertEquals(tutorialTitle.getText(), title);
        assertEquals(tutorialMessage.getText(), dialogMessage);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void setViewPagerAdapterTest() {
        //Act
        view.setViewPagerAdapter();
        //Assert
        assertEquals(View.VISIBLE, viewPager.getVisibility());
        assertNull(view.getViewPager().getAdapter());
    }

    @Test
    public void setViewPagerAdapterNotNullTest() {
        Uri[] uris = new Uri[]{Uri.parse("")};
        //Act
        view.setupView(uris);
        view.setViewPagerAdapter();
        //Assert
        assertEquals(View.VISIBLE, viewPager.getVisibility());
        assertNotNull(view.getViewPager().getAdapter());
    }

    @Test
    public void setupViewTest() {
        //Arrange
        Uri[] uris = new Uri[]{Uri.parse("")};
        //Act
        view.setupView(uris);
        //Assert
        assertEquals(View.VISIBLE, viewPager.getVisibility());
        assertNotNull(view.getAdapter());
        assertNotNull(view.getViewPager());
    }

    @Test
    public void previousImageIsVisibleTest() {
        //Act
        view.previousImageIsVisible(true);
        //Assert
        assertEquals(View.VISIBLE, previousImage.getVisibility());
    }

    @Test
    public void previousImageIsNotVisibleTest() {
        //Act
        view.previousImageIsVisible(false);
        //Assert
        assertEquals(View.GONE, previousImage.getVisibility());
    }

    @Test
    public void nextImageIsVisibleTest() {
        //Act
        view.nextImageIsVisible(true);
        //Assert
        assertEquals(View.VISIBLE, nextImage.getVisibility());
    }

    @Test
    public void nextImageIsnOTVisibleTest() {
        //Act
        view.nextImageIsVisible(false);
        //Assert
        assertEquals(View.GONE, nextImage.getVisibility());
    }

    @Test
    public void changeImage() {
        /*Arrange*/
        int image = 0;
        /*Act*/
        view.changeImage(image);
        /*Assert*/
        assertTrue(viewPager.getCurrentItem() == 0);
    }

    @Test
    public void showResultDialog_Integrity() {
        /*Arrange*/
        String text = activity.getString(R.string.images_preview_dialog_title);
        String optionOne = activity.getString(R.string.yes_base);
        String optionTwo = activity.getString(R.string.no_base);
        /*Act*/
        view.showResultDialog();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        assertEquals(((TextView) bannerView.getBannerContainer().findViewById(R.id.tv_message)).getText(), text);
        assertEquals(bannerView.getBannerTextOptionOne(), optionOne);
        assertEquals(bannerView.getBannerTextOptionTwo(), optionTwo);
    }

    @Test
    public void showResultDialog_OptionOne() {
        /*Arrange*/
        ArgumentCaptor<PreviewImagesView.ResultDialogEvent> argument = ArgumentCaptor.forClass(PreviewImagesView.ResultDialogEvent.class);
        /*Act*/
        view.showResultDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        /*Assert*/
        verify(bus).post(argument.capture());
        assertEquals(argument.getValue().getResult(), PreviewImagesView.ResultDialogEvent.RESULT_POSITIVE);
    }

    @Test
    public void showResultDialog_OptionTwo() {
        /*Arrange*/
        ArgumentCaptor<PreviewImagesView.ResultDialogEvent> argument = ArgumentCaptor.forClass(PreviewImagesView.ResultDialogEvent.class);
        /*Act*/
        view.showResultDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().callOnClick();
        /*Assert*/
        verify(bus).post(argument.capture());
        assertEquals(argument.getValue().getResult(), PreviewImagesView.ResultDialogEvent.RESULT_NEGATIVE);
    }

    @Test
    public void onPreviousButtonPressedTest() {
        //Act
        view.onPreviousButtonPressed();
        //Assert
        verify(bus).post(any(PreviousButtonPressedEvent.class));
    }

    @Test
    public void onNextButtonPressedTest() {
        //Act
        view.onNextButtonPressed();
        //Assert
        verify(bus).post(any(NextButtonPressedEvent.class));
    }

    @Test
    public void hideTutorialButtonTest() {
        //Act
        view.hideTutorialButton();
        //Assert
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialButtonTest() {
        //Act
        view.showTutorialButton();
        //Assert
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void isTutorialVisibleTest() {
        //Act
        view.showTutorialButton();
        //Assert
        assertTrue(view.isTutorialVisible());
    }
}
