package com.facebook.devices.multitouchv2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.MultitouchV2Activity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.MultitouchV2View;
import com.facebook.devices.mvp.view.MultitouchV2View.OnFailureDialogOptionSelectedEvent;
import com.facebook.devices.mvp.view.MultitouchV2View.OnIntroAccepted;
import com.facebook.devices.mvp.view.MultitouchV2View.OnMultitouchReplies;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class MultitouchV2ViewTest {

    private MultitouchV2Activity activity;
    private MultitouchV2View view;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, MultitouchV2Activity.class);
        activity = Robolectric.buildActivity(MultitouchV2Activity.class, intent).create().start().resume().get();

        view = new MultitouchV2View(activity, bus);

        /* Bindings */
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void init() {
        //Arrange
        int resultMode = 0;
        ArgumentCaptor<OnMultitouchReplies> argumentCaptor = ArgumentCaptor.forClass(OnMultitouchReplies.class);
        //Act
        view.getMultitouchCirclesView().getListener().onMultitouchReplies(resultMode);
        //Assert
        verify(bus).post(argumentCaptor.capture());
        assertEquals(resultMode, argumentCaptor.getValue().getResultMode());
    }

    @Test
    public void enableMultitouchViewTest() {
        //Act
        view.enableMultitouchView();
        //Assert
        assertTrue(view.getMultitouchCirclesView().isEnabled());
    }

    @Test
    public void resetMultitouchViewTest() {
        //Act
        view.resetMultitouchView();
        //Assert
        assertFalse(view.getMultitouchCirclesView().isDualFingersWereDetected());
        assertTrue(view.getMultitouchCirclesView().isRunningMode());
    }
    @Test
    public void initDialogCheck() {
        //Arrange
        String message = activity.getString(R.string.multitouch_v2_tutorial_text_dialog);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        floatingTutorialView.getButton().performClick();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertThat(shadowAlertDialog.getMessage(), equalTo(message));
        assertThat(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), equalTo(positiveText));
    }

    @Test
    public void hideFloatingTutorialButtonTest() {
        //Act
        view.showFloatingTutorialButton();
        view.hideFloatingTutorialButton();
        //Assert
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showFloatingTutorialButtonTest() {
        //Act
        view.showFloatingTutorialButton();
        //Assert
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideBannerTopTest() {
        //Act
        view.showInitBanner();
        view.hideBannerTop();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void showInitBannerTest() {
        /*Arrange*/
        String message = activity.getString(R.string.multitouch_v2_init_message_dialog);
        String optionPositive = activity.getString(R.string.ok_base);

        /*Act*/
        view.showInitBanner();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), optionPositive);
    }

    @Test
    public void showInitBannerOptionOneTest() {
        //Act
        view.showInitBanner();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnIntroAccepted.class));
    }

    @Test
    public void showDroppedDialogTest() {
        //Arrange
        String title = activity.getString(R.string.multitouch_v2_failure_title_dialog);
        String message = activity.getString(R.string.multitouch_v2_failure_message_dialog);
        String positiveText = activity.getString(R.string.try_again_base);
        String negativeText = activity.getString(R.string.no_base);
        //Act
        view.showDroppedDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertThat(shadowAlertDialog.getTitle(), equalTo(title));
        assertThat(shadowAlertDialog.getMessage(), equalTo(message));
        assertThat(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), equalTo(positiveText));
        assertThat(dialog.getButton(DialogInterface.BUTTON_NEGATIVE).getText(), equalTo(negativeText));
    }

    @Test
    public void showDroppedDialogOptionOneTest() {
        //Arrange
        ArgumentCaptor<OnFailureDialogOptionSelectedEvent> argumentCaptor = ArgumentCaptor.forClass(OnFailureDialogOptionSelectedEvent.class);
        //Act
        view.showDroppedDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue().getOptionSelected(), OnFailureDialogOptionSelectedEvent.OPTION_RETRY);
    }

    @Test
    public void showDroppedDialogOptionTwoTest() {
        //Arrange
        ArgumentCaptor<OnFailureDialogOptionSelectedEvent> argumentCaptor = ArgumentCaptor.forClass(OnFailureDialogOptionSelectedEvent.class);
        //Act
        view.showDroppedDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue().getOptionSelected(), OnFailureDialogOptionSelectedEvent.OPTION_FAIL);
    }

}