package com.facebook.devices.multitouchv2;


import com.facebook.devices.mvp.model.MultitouchV2Model;
import com.facebook.devices.mvp.presenter.MultitouchV2Presenter;
import com.facebook.devices.mvp.view.MultitouchV2View;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.utils.customviews.MultitouchCirclesView.MultiTouchDetectorViewCallback.FINISHED_DROPPED;
import static com.facebook.devices.utils.customviews.MultitouchCirclesView.MultiTouchDetectorViewCallback.FINISHED_SUCCESS;
import static org.mockito.Mockito.verify;

public class MultitouchV2PresenterTest {

    @Mock
    private MultitouchV2View view;
    @Mock
    private MultitouchV2Model model;

    private MultitouchV2Presenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new MultitouchV2Presenter(model, view);
    }

    @Test
    public void onMultitouchRepliesSuccess() {
        //Arrange
        MultitouchV2View.OnMultitouchReplies event = new MultitouchV2View.OnMultitouchReplies(FINISHED_SUCCESS);
        //Act
        presenter.onMultitouchReplies(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onMultitouchRepliesDropped() {
        //Arrange
        MultitouchV2View.OnMultitouchReplies event = new MultitouchV2View.OnMultitouchReplies(FINISHED_DROPPED);
        //Act
        presenter.onMultitouchReplies(event);
        //Assert
        verify(view).showDroppedDialog();
    }

    @Test
    public void onFailureDialogOptionSelectedRetry() {
        //Arrange
        MultitouchV2View.OnFailureDialogOptionSelectedEvent event = new MultitouchV2View.OnFailureDialogOptionSelectedEvent(MultitouchV2View.OnFailureDialogOptionSelectedEvent.OPTION_RETRY);
        //Act
        presenter.onFailureDialogOptionSelectedEvent(event);
        //Assert
        verify(view).resetMultitouchView();
    }

    @Test
    public void onFailureDialogOptionSelectedFail() {
        //Arrange
        MultitouchV2View.OnFailureDialogOptionSelectedEvent event = new MultitouchV2View.OnFailureDialogOptionSelectedEvent(MultitouchV2View.OnFailureDialogOptionSelectedEvent.OPTION_FAIL);
        //Act
        presenter.onFailureDialogOptionSelectedEvent(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onIntroAcceptedTest() {
        //Act
        presenter.onIntroAccepted(null);
        //Assert
        verify(view).hideBannerTop();
        verify(view).hideFloatingTutorialButton();
        verify(view).enableMultitouchView();
    }

}
