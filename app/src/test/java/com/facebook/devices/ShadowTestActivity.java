package com.facebook.devices;

import android.app.Activity;

import com.facebook.devices.permission.PermissionHandler;
import com.facebook.hwtp.mvp.presenter.PresenterBase;

import java.lang.reflect.Field;

public class ShadowTestActivity<P extends PresenterBase> {

    private Activity activity;

    public ShadowTestActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean isPresenterCreated() {
        return getPresenter() != null;
    }

    public Object getAttributeValue(String attrName) {
        try {
            Field field = activity.getClass().getDeclaredField(attrName);
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            return field.get(activity);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            return null;
        }
    }

    public void setAttributeValue(String attrName, Object value) {
        try {
            Field field = activity.getClass().getDeclaredField(attrName);
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            field.set(activity, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
        }
    }

    public P getPresenter() {
        try {
            Field field = activity.getClass().getDeclaredField("presenter");
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            return (P) field.get(activity);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            return null;
        }
    }

    public void setPresenter(P presenter) {
        try {
            Field field = activity.getClass().getDeclaredField("presenter");
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            field.set(activity, presenter);
        } catch (NoSuchFieldException | IllegalAccessException e) {
        }
    }

    public void setPermissionHandler(PermissionHandler permissionHandler) {
        try {
            Field field = activity.getClass().getDeclaredField("permissionHandler");
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            field.set(activity, permissionHandler);
        } catch (NoSuchFieldException | IllegalAccessException e) {
        }
    }
}
