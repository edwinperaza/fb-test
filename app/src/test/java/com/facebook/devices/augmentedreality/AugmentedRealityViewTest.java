package com.facebook.devices.augmentedreality;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.AugmentedRealityActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.AugmentedRealityView;
import com.facebook.devices.mvp.view.AugmentedRealityView.IntroEvent;
import com.facebook.devices.mvp.view.AugmentedRealityView.QuestionEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerEventBase;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;
import com.google.ar.sceneform.ArSceneView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class AugmentedRealityViewTest {

    private AugmentedRealityActivity activity;
    private AugmentedRealityView view;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private BannerView bannerView;
    private FloatingTutorialView floatingTutorialView;
    private ArSceneView arSceneView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, AugmentedRealityActivity.class);
        activity = Robolectric.buildActivity(AugmentedRealityActivity.class, intent).create().start().resume().get();

        view = new AugmentedRealityView(activity, bus);

        /*
        PowerMockito.mockStatic(SystemHelper.class);
        when(SystemHelper.getOpenGLSystemVersion(any(Context.class))).thenReturn(3.1f);
        */

        //Bindings
        bannerView = ShadowView.getCurrentBanner(view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        arSceneView = activity.findViewById(R.id.surfaceview);
    }

    @Test
    public void showIntroBannerTest(){
        //Arrange
        String message = activity.getString(R.string.augmented_reality_intro_message);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.showIntroBanner();
        //Assert
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void showIntroBannerOptionOneTest() {
        //Arrange
        ArgumentCaptor<IntroEvent> argumentCaptor = ArgumentCaptor.forClass(IntroEvent.class);
        //Act
        view.showIntroBanner();
        //Assert
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(BannerEventBase.OPTION_ONE, argumentCaptor.getValue().optionSelected);
    }

    @Test
    public void showQuestionBannerTest(){
        //Arrange
        String message = activity.getString(R.string.augmented_reality_question_message);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);
        //Act
        view.showQuestionBanner();
        //Assert
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showQuestionBannerOptionOneTest() {
        //Arrange
        ArgumentCaptor<QuestionEvent> argumentCaptor = ArgumentCaptor.forClass(QuestionEvent.class);
        //Act
        view.showQuestionBanner();
        //Assert
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(BannerEventBase.OPTION_ONE, argumentCaptor.getValue().optionSelected);
    }

    @Test
    public void showQuestionBannerOptionTwoTest() {
        //Arrange
        ArgumentCaptor<QuestionEvent> argumentCaptor = ArgumentCaptor.forClass(QuestionEvent.class);
        //Act
        view.showQuestionBanner();
        //Assert
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(BannerEventBase.OPTION_TWO, argumentCaptor.getValue().optionSelected);
    }

    @Test
    public void showTutorialButtonTest() {
        //Act
        view.showTutorialButton();
        //Assert
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideTutorialButtonTest() {
        //Act
        view.hideTutorialButton();
        //Assert
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void onFloatingTutorialClicked() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.augmented_reality_tutorial_message);
        String option = activity.getString(R.string.ok_base);

        /*Act*/
        view.onFloatingTutorialClicked();

        /*Assert*/
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(alertDialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), option);
    }
}