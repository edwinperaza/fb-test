package com.facebook.devices.augmentedreality;

import android.content.ServiceConnection;
import android.hardware.camera2.CameraManager;

import com.facebook.devices.mvp.model.AugmentedRealityModel;
import com.facebook.devices.mvp.presenter.AugmentedRealityPresenter;
import com.facebook.devices.mvp.view.AugmentedRealityView;
import com.facebook.devices.permission.RuntimePermissionHandler.PermissionDeniedEvent;
import com.facebook.devices.utils.customviews.BannerEventBase;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AugmentedRealityPresenterTest {

    private AugmentedRealityPresenter presenter;

    @Mock
    private AugmentedRealityView view;
    @Mock
    private AugmentedRealityModel model;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        presenter = new AugmentedRealityPresenter(model, view);
    }

    @Test
    public void initArSceneViewVerification(){
        verify(view).initArSceneView(model.getModelFileName(), model.getObjectiveDbName());
    }

    @Test
    public void getServiceConnection(){
        assertEquals((ServiceConnection) presenter, presenter.getServiceConnection());
    }

    @Test
    public void onResume() throws UnavailableUserDeclinedInstallationException, UnavailableDeviceNotCompatibleException {
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        when(model.checkArCoreInstallation()).thenReturn(true);
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(mockCameraManager).registerAvailabilityCallback(any(CameraManager.AvailabilityCallback.class), eq(null));
        verify(model).sendCameraUnlockRequest();
    }

    @Test
    public void onResume_RequestPermissions() throws UnavailableUserDeclinedInstallationException, UnavailableDeviceNotCompatibleException {
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        when(model.checkArCoreInstallation()).thenReturn(true);
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(model).requestPermissions();
    }

    @Test
    public void availabilityCallback() throws UnavailableUserDeclinedInstallationException, UnavailableDeviceNotCompatibleException {
        /*Arrange*/
        String cameraId = "0";
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        when(model.checkArCoreInstallation()).thenReturn(true);
        when(model.isPermissionGranted()).thenReturn(true);
        ArgumentCaptor<CameraManager.AvailabilityCallback> captor = ArgumentCaptor.forClass(CameraManager.AvailabilityCallback.class);
        when(model.isCameraTarget(anyString())).thenReturn(true);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(mockCameraManager).registerAvailabilityCallback(captor.capture(), eq(null));
        captor.getValue().onCameraAvailable(cameraId);
        verify(model).isCameraTarget(eq(cameraId));
        verify(view).checkSessionStateAndConfigure(model.getObjectiveDbName(), model.getObjectiveImgPath());
    }

    @Test
    public void onResume_Fail() throws UnavailableUserDeclinedInstallationException, UnavailableDeviceNotCompatibleException {
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        when(model.checkArCoreInstallation()).thenThrow(new UnavailableUserDeclinedInstallationException());
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(mockCameraManager).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model).sendCameraLockRequest();
        verify(model).fail(any());
        verify(view).finishActivity();
    }

    @Test
    public void onPause(){
        /*Act*/
        presenter.onPause();
        /*Assert*/
        verify(view).pauseArComponents();
    }

    @Test
    public void onIntroEvent(){
        /*Act*/
        presenter.onIntroEvent(null);
        /*Assert*/
        verify(view).showQuestionBanner();
        verify(view).hideTutorialButton();
    }

    @Test
    public void onQuestionEvent_OptionOne(){
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        AugmentedRealityView.QuestionEvent event = new AugmentedRealityView.QuestionEvent(BannerEventBase.OPTION_ONE);
        /*Act*/
        presenter.onQuestionEvent(event);
        /*Assert*/
        verify(mockCameraManager).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model).sendCameraLockRequest();
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onQuestionEvent_OptionTwo(){
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        AugmentedRealityView.QuestionEvent event = new AugmentedRealityView.QuestionEvent(BannerEventBase.OPTION_TWO);
        /*Act*/
        presenter.onQuestionEvent(event);
        /*Assert*/
        verify(mockCameraManager).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model).sendCameraLockRequest();
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onSessionCreatedAndConfiguredEvent(){
        /*Act*/
        presenter.onSessionCreatedAndConfiguredEvent(null);
        /*Assert*/
        verify(view).resumeArComponents();
        verify(view).showIntroBanner();
        verify(view).showTutorialButton();
    }

    @Test
    public void onSessionCreationErrorEvent(){
        /*Arrange*/
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        AugmentedRealityView.SessionCreationOrConfiguredErrorEvent event = new AugmentedRealityView.SessionCreationOrConfiguredErrorEvent("Error");
        /*Act*/
        presenter.onSessionCreationErrorEvent(event);
        /*Assert*/
        verify(mockCameraManager).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model).sendCameraLockRequest();
        verify(model).fail(eq(event.message));
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        PermissionDeniedEvent event = new PermissionDeniedEvent();
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(mockCameraManager).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model).sendCameraLockRequest();
        verify(model).fail(eq("User denied permissions"));
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResultShouldRequestTest() {
        //Arrange
        CameraManager mockCameraManager = mock(CameraManager.class);
        when(model.getCameraManager()).thenReturn(mockCameraManager);
        PermissionDeniedEvent event = new PermissionDeniedEvent();
        when(model.shouldRequestPermissions()).thenReturn(true);
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(mockCameraManager, never()).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        verify(model, never()).sendCameraLockRequest();
        verify(model, never()).fail(eq("User denied permissions"));
        verify(view, never()).finishActivity();
    }
}