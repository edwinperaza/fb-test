package com.facebook.devices.augmentedreality;

import android.app.Activity;
import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.text.TextUtils;

import com.facebook.devices.mvp.model.AugmentedRealityModel;
import com.facebook.devices.permission.PermissionHandler;
import com.facebook.devices.utils.camera.CameraUtils;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ArCoreApk.class, CameraUtils.class})
public class AugmentedRealityModelTest {

    private AugmentedRealityModel model;

    @Mock
    private ArCoreApk mockArCoreApk;
    @Mock
    private CameraManager cameraManager;
    @Mock
    private Activity activity;
    @Mock
    private PermissionHandler permissionHandler;
    @Mock
    private CameraUtils cameraUtils;

    private String filename = "Name";
    private String objectivePath = "Path";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ArCoreApk.class);
        PowerMockito.mockStatic(CameraUtils.class);
        when(ArCoreApk.getInstance()).thenReturn(mockArCoreApk);

        when(activity.getSystemService(Context.CAMERA_SERVICE)).thenReturn(cameraManager);

        model = new AugmentedRealityModel(activity, permissionHandler,
                filename, objectivePath, cameraUtils);
    }

    @Test
    public void getObjectiveDbName() {
        /*Assert*/
        assertNotNull(model.getObjectiveDbName());
        assertFalse(TextUtils.isEmpty(model.getObjectiveDbName()));
    }

    @Test
    public void getModelFileName() {
        /*Assert*/
        assertNotNull(model.getModelFileName());
        assertEquals(model.getModelFileName(), filename);
    }

    @Test
    public void getObjectiveImgPath() {
        /*Assert*/
        assertNotNull(model.getObjectiveImgPath());
        assertEquals(model.getObjectiveImgPath(), objectivePath);
    }

    @Test
    public void checkArCoreInstallation_NotInstalled() {
        try {
            /*Arrange*/
            when(mockArCoreApk.requestInstall(activity, true)).thenReturn(ArCoreApk.InstallStatus.INSTALL_REQUESTED);
            /*Assert*/
            assertFalse(model.checkArCoreInstallation());
        } catch (UnavailableUserDeclinedInstallationException e) {
            e.printStackTrace();
        } catch (UnavailableDeviceNotCompatibleException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkArCoreInstallation_Installed() {
        try {
            /*Arrange*/
            when(mockArCoreApk.requestInstall(activity, true)).thenReturn(ArCoreApk.InstallStatus.INSTALLED);
            /*Assert*/
            assertTrue(model.checkArCoreInstallation());
        } catch (UnavailableUserDeclinedInstallationException e) {
            e.printStackTrace();
        } catch (UnavailableDeviceNotCompatibleException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void requestPermissions() {
        /*Act*/
        model.requestPermissions();
        /*Assert*/
        verify(permissionHandler).requestPermission();
    }

    @Test
    public void shouldRequestPermissions() {
        /*Arrange*/
        when(permissionHandler.shouldRequestPermission()).thenReturn(true);
        /*Act*/
        assertTrue(model.shouldRequestPermissions());
        /*Assert*/
        verify(permissionHandler).shouldRequestPermission();
    }

    @Test
    public void isPermissionGranted() {
        /*Arrange*/
        when(permissionHandler.isPermissionGranted()).thenReturn(true);
        /*Act*/
        assertTrue(model.isPermissionGranted());
        /*Assert*/
        verify(permissionHandler).isPermissionGranted();
    }

    @Test
    public void getCameraManger() {
        /*Assert*/
        assertEquals(model.getCameraManager(), cameraManager);
    }

    @Test
    public void isCameraTarget_Found() {
        /*Arrange*/
        String cameraId = "0";
        CameraCharacteristics cameraCharacteristics = mock(CameraCharacteristics.class);
        try {
            when(cameraManager.getCameraCharacteristics(cameraId)).thenReturn(cameraCharacteristics);
            when(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)).thenReturn(CameraCharacteristics.LENS_FACING_BACK);
            /*Assert*/
            assertTrue(model.isCameraTarget(cameraId));
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void isCameraTarget_NotFound() {
        /*Arrange*/
        String cameraId = "0";
        CameraCharacteristics cameraCharacteristics = mock(CameraCharacteristics.class);
        try {
            when(cameraManager.getCameraCharacteristics(cameraId)).thenReturn(cameraCharacteristics);
            when(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)).thenReturn(CameraCharacteristics.LENS_FACING_FRONT);
            /*Assert*/
            assertFalse(model.isCameraTarget(cameraId));
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void isCameraTarget_Exception() {
        /*Arrange*/
        String cameraId = "0";
        try {
            when(cameraManager.getCameraCharacteristics(cameraId)).thenThrow(CameraAccessException.class);
            /*Assert*/
            assertFalse(model.isCameraTarget(cameraId));
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendCameraUnlockRequest() {
        /*Act*/
        model.sendCameraUnlockRequest();
        /*Assert*/
        verify(cameraUtils).sendCameraUnlockRequest();
    }

    @Test
    public void sendCameraLockRequest() {
        /*Act*/
        model.sendCameraLockRequest();
        /*Assert*/
        verify(cameraUtils).sendCameraLockRequest();
    }
}
