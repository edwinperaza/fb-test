package com.facebook.devices.touchpatternsimple;


import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.TouchPatternSimpleActivity;
import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowLooper;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class GridTouchPatternModelTest {

    private Context context;
    private GridTouchPatternModel model;
    private int size = TouchPatternModel.FIVE_FIVE;

    @Mock
    private Handler handlerMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, TouchPatternSimpleActivity.class);
        context = Robolectric.buildActivity(TouchPatternSimpleActivity.class, intent).create().start().resume().get();

        model = new GridTouchPatternModel(size, handlerMock);
    }

    @Test
    public void correctSize() {
        assertEquals(size, model.getTouchSize());
    }

    @Test
    public void startTimer() {
       /*Act*/
        model.startTimer();
       /*Assert*/
       assertFalse(model.isTimerInitAvailable());
       verify(handlerMock).postDelayed(any(Runnable.class), eq(model.TIMER_MILLIS));
    }
    
    
    @Test
    public void cancelTimer(){
        /*Act*/
        model.cancelTimer();
        /*Assert*/
        assertTrue(model.isTimerInitAvailable());
        verify(handlerMock).removeCallbacks(any(Runnable.class));
    }

    @Test
    public void runnable(){
        /*Arrange*/
        model = new GridTouchPatternModel(size, new Handler(context.getMainLooper()));
        GridTouchPatternModel.TouchPatternSimpleModelCallback callbackMock = mock(GridTouchPatternModel.TouchPatternSimpleModelCallback.class);
        model.setCallback(callbackMock);
        /*Act*/
        model.startTimer();
        /*Assert*/
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();
        verify(callbackMock).onTimeUp();
    }

}
