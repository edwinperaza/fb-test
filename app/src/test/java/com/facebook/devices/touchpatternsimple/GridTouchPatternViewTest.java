package com.facebook.devices.touchpatternsimple;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.TestBaseActivity;
import com.facebook.devices.activities.TouchPatternSimpleActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.devices.mvp.view.GridTouchPatternView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.TouchDetectorGridView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.view.GridTouchPatternView.TimeUpDialogEvent.FAIL_TEST;
import static com.facebook.devices.mvp.view.GridTouchPatternView.TimeUpDialogEvent.KEEP_TOUCHING;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_RELEASED;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_TOUCHING;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class GridTouchPatternViewTest {

    private GridTouchPatternView view;
    private TestBaseActivity activity;

    @Mock
    private BusProvider.Bus bus;

    /*Bindings*/
    private TouchDetectorGridView touchDetectorSimple;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, TouchPatternSimpleActivity.class);
        activity = Robolectric.buildActivity(TouchPatternSimpleActivity.class, intent).create().start().resume().get();

        view = new GridTouchPatternView(activity, bus);

        /*Bindings*/
        touchDetectorSimple = activity.findViewById(R.id.touch_detector);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void touchState_Finishes() {
        /*Finishes*/
        touchDetectorSimple.getCallback().onPatternDetectionFinishes(FINISHED_PATTERN_SUCCESS);
        ArgumentCaptor<GridTouchPatternView.TouchDetectorSimpleResult> captorA = ArgumentCaptor.forClass(GridTouchPatternView.TouchDetectorSimpleResult.class);
        verify(bus).post(captorA.capture());
        assertEquals(captorA.getValue().getResult(), FINISHED_PATTERN_SUCCESS);
        /*Release*/
        touchDetectorSimple.getCallback().onViewStateChanged(STATE_RELEASED);
        ArgumentCaptor<GridTouchPatternView.OnTouchDetectorStateChanged> captorB = ArgumentCaptor.forClass(GridTouchPatternView.OnTouchDetectorStateChanged.class);
        verify(bus, times(2)).post(captorB.capture());
        assertEquals(captorB.getValue().getResult(), STATE_RELEASED);
        /*Touching*/
        touchDetectorSimple.getCallback().onViewStateChanged(STATE_TOUCHING);
        ArgumentCaptor<GridTouchPatternView.OnTouchDetectorStateChanged> captorC = ArgumentCaptor.forClass(GridTouchPatternView.OnTouchDetectorStateChanged.class);
        verify(bus, times(3)).post(captorC.capture());
        assertEquals(captorC.getValue().getResult(), STATE_TOUCHING);
    }

    @Test
    public void touchState_Released() {
        /*Release*/
        touchDetectorSimple.getCallback().onViewStateChanged(STATE_RELEASED);
        ArgumentCaptor<GridTouchPatternView.OnTouchDetectorStateChanged> captorB = ArgumentCaptor.forClass(GridTouchPatternView.OnTouchDetectorStateChanged.class);
        verify(bus).post(captorB.capture());
        assertEquals(captorB.getValue().getResult(), STATE_RELEASED);
    }

    @Test
    public void touchState_Touching() {
        /*Touching*/
        touchDetectorSimple.getCallback().onViewStateChanged(STATE_TOUCHING);
        ArgumentCaptor<GridTouchPatternView.OnTouchDetectorStateChanged> captorC = ArgumentCaptor.forClass(GridTouchPatternView.OnTouchDetectorStateChanged.class);
        verify(bus).post(captorC.capture());
        assertEquals(captorC.getValue().getResult(), STATE_TOUCHING);
    }

    @Test
    public void enableTouch() {
        /*Act*/
        view.enableTouch();
        /*Assert*/
        assertTrue(touchDetectorSimple.isEnabled());
    }

    @Test
    public void showTutorial() {
        /*Act*/
        view.onFloatingTutorialClicked();
        /*Assert*/
        verify(bus).post(any(GridTouchPatternView.TutorialFloatingButtonClicked.class));
    }

    @Test
    public void showTutorialDialog() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.touch_pattern_simple_intro_message);
        String positiveText = activity.getString(R.string.ok_base);
        /*Act*/
        view.onFloatingTutorialClicked();
        /*Assert*/
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertEquals(title, shadowAlertDialog.getTitle());
        assertEquals(message, shadowAlertDialog.getMessage());
        assertEquals(positiveText, dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText());
    }

    @Test
    public void showInitDialog_UI() {
        /*Arrange*/
        String message = activity.getString(R.string.touch_pattern_simple_init_dialog_text);
        String positiveOption = activity.getString(R.string.ok_base);
        /*Act*/
        view.showInitDialog();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        Assert.assertEquals(messageDialog.getText().toString(), message);
        Assert.assertEquals(bannerView.getBannerOptionOneButton().getText().toString(), positiveOption);
        Assert.assertEquals(bannerView.getBannerOptionTwoButton().getVisibility(), View.GONE);
        Assert.assertEquals(bannerView.getBannerOptionThreeButton().getVisibility(), View.GONE);
    }

    @Test
    public void showInitDialog_Behavior() {
        /*Act*/
        view.showInitDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        /*Assert*/
        verify(bus).post(any(GridTouchPatternView.InitDialogEvent.class));
    }

    @Test
    public void hideInitDialog() {
        /*Act*/
        view.showInitDialog();
        view.hideInitDialog();
        /*Assert*/
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }

    @Test
    public void hideTutorialFloatingButton() {
        /*Act*/
        view.hideTutorialFloatingButton();
        /*Assert*/
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTimeUpPatternDialog_UI() {
        //Arrange
        String title = activity.getString(R.string.touch_pattern_simple_time_up_message);
        String positiveText = activity.getString(R.string.time_up_positive_option);
        String negativeText = activity.getString(R.string.time_up_negative_option);
        //Act
        view.showTimeUpPatternDialog();
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(alertDialog));
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).getText(), negativeText);
        assertEquals(shadowAlertDialog.getMessage(), title);
    }

    @Test
    public void showTimeupPatternDialog_Behavior() {
        //Behavior positive
        view.showTimeUpPatternDialog();
        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        ArgumentCaptor<GridTouchPatternView.TimeUpDialogEvent> captorA = ArgumentCaptor.forClass(GridTouchPatternView.TimeUpDialogEvent.class);
        verify(bus).post(captorA.capture());
        assertEquals(captorA.getValue().getOptionSelected(), KEEP_TOUCHING);
        //Behavior negative
        view.showTimeUpPatternDialog();
        ShadowAlertDialog.getLatestAlertDialog().getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        ArgumentCaptor<GridTouchPatternView.TimeUpDialogEvent> captorB = ArgumentCaptor.forClass(GridTouchPatternView.TimeUpDialogEvent.class);
        verify(bus, times(2)).post(captorB.capture());
        assertEquals(captorB.getValue().getOptionSelected(), FAIL_TEST);
    }

    @Test
    public void setTouchSize() {
        //Arrange
        @GridTouchPatternModel.TouchDetectorSize int size = GridTouchPatternModel.EIGHT_EIGHT;
        //Act
        view.setTouchDetectorSize(size);
        //Assert
        assertEquals(size, touchDetectorSimple.getSizeModeSet());
    }

}
