package com.facebook.devices.touchpatternsimple;


import com.facebook.devices.mvp.model.GridTouchPatternModel;
import com.facebook.devices.mvp.presenter.GridTouchPatternPresenter;
import com.facebook.devices.mvp.view.GridTouchPatternView;
import com.facebook.devices.mvp.view.TestViewBase;
import com.facebook.devices.utils.customviews.TouchDetectorGridView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_RELEASED;
import static com.facebook.devices.utils.customviews.TouchDetectorGridView.STATE_TOUCHING;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GridTouchPatternPresenterTest {

    private GridTouchPatternPresenter presenter;

    @Mock
    private GridTouchPatternModel model;
    @Mock
    private GridTouchPatternView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);


        presenter = new GridTouchPatternPresenter(model, view);
    }

    @Test
    public void checkInit_IntroMessage() {
        verify(model).setCallback(any(GridTouchPatternModel.TouchPatternSimpleModelCallback.class));
        verify(view).initTouchDetectorView(model.getTouchSize());

    }

    @Test
    public void onInitDialogEvent() {
        /*Act*/
        presenter.onInitDialogEvent(null);
        /*Assert*/
        verify(view).hideInitDialog();
        verify(view).hideTutorialFloatingButton();
        verify(view).enableTouch();
        verify(model).startTimer();
    }

    @Test
    public void onTutorialShowed() {
        /*Act*/
        presenter.onTutorialShowed(null);
        /*Assert*/
        verify(model).cancelTimer();
    }

    @Test
    public void onStop() {
        /*Act*/
        presenter.onStop();
        /*Assert*/
        verify(model).cancelTimer();
    }


    @Test
    public void onTouchDetectorStateChanged_Touching() {
        /*Arrange*/
        GridTouchPatternView.OnTouchDetectorStateChanged event = new GridTouchPatternView.OnTouchDetectorStateChanged(STATE_TOUCHING);
        /*Act*/
        presenter.onTouchDetectorStateChanged(event);
        /*Assert*/
        verify(model).cancelTimer();
    }

    @Test
    public void onTouchDetectorStateChanged_Release() {
        /*Arrange*/
        GridTouchPatternView.OnTouchDetectorStateChanged event = new GridTouchPatternView.OnTouchDetectorStateChanged(STATE_RELEASED);
        /*Act*/
        presenter.onTouchDetectorStateChanged(event);
        /*Assert*/
        verify(model).startTimer();
    }

    @Test
    public void onTouchDetectorCallback_Success() {
        /*Arrange*/
        GridTouchPatternView.TouchDetectorSimpleResult event = new GridTouchPatternView.TouchDetectorSimpleResult(TouchDetectorGridView.TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS);
        /*Act*/
        presenter.onTouchDetectorCallback(event);
        /*Assert*/
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onServiceConnected() {
        /*Arrange*/
        ConnectionService.Device mockDevice = mock(ConnectionService.Device.class);
        ConnectionService.LocalBinder localBinderMock = mock(ConnectionService.LocalBinder.class);
        when(localBinderMock.getService()).thenReturn(mockDevice);
        /*Act*/
        presenter.onServiceConnected(null, localBinderMock);
        /*Assert*/
        verify(model).setService(mockDevice);
    }

    @Test
    public void onServiceDisconnected() {
        /*Act*/
        presenter.onServiceDisconnected(null);
        /*Assert*/
        verify(model).removeService();
    }

    @Test
    public void onTimeUpDialogOptionEvent_KeepTouching() {
        /*Act*/
        presenter.onTimeUpDialogOptionEvent(new GridTouchPatternView.TimeUpDialogEvent(GridTouchPatternView.TimeUpDialogEvent.KEEP_TOUCHING));
        /*Assert*/
        verify(model).startTimer();
    }

    @Test
    public void onTimeUpDialogOptionEvent_FailTest() {
        /*Act*/
        presenter.onTimeUpDialogOptionEvent(new GridTouchPatternView.TimeUpDialogEvent(GridTouchPatternView.TimeUpDialogEvent.FAIL_TEST));
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }
}
