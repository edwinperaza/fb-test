package com.facebook.devices.microphoneplayback;

import com.facebook.devices.mvp.model.MicrophonePlaybackModel;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioPlayer;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.VolumeManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.mvp.model.MicrophonePlaybackModel.STATE_PLAYBACK;
import static com.facebook.devices.mvp.model.MicrophonePlaybackModel.STATE_RECORDING;
import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MicrophonePlaybackModelTest {

    private MicrophonePlaybackModel model;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    @Mock
    private AudioPlayer audioPlayer;

    @Mock
    private VolumeManager volumeManager;

    @Mock
    private AudioRecorder audioRecorder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        model = new MicrophonePlaybackModel(permissionHandler, null, audioRecorder, audioPlayer, volumeManager);
    }

    @Test
    public void mode_constructor_with_data(){
        /*Arrange*/
        short[] bufferData = new short[]{1,2,3,4,5,6};
        /*Act*/
        MicrophonePlaybackModel modelAux = new MicrophonePlaybackModel(permissionHandler, bufferData, audioRecorder, audioPlayer, volumeManager);
        /*Assert*/
        assertEquals(modelAux.getRecordedBuffer(), bufferData);
    }

    @Test
    public void setRecordedBuffer(){
        /*Arrange*/
        short[] data = new short[]{1,2,3,4,5,6};
        /*Act*/
        model.setRecordedBuffer(data);
        /*Assert*/
        assertEquals(data, model.getRecordedBuffer());
    }

    @Test
    public void getRecordedBuffer_Empty() {
        assertEquals(0, model.getRecordedBuffer().length);
    }

    @Test
    public void getActualState_BaseInit() {
        /*Arrange*/
        int state = STATE_RECORDING;
        /*Assert*/
        assertEquals(model.getActualState(), state);
    }

    @Test
    public void requestPermissions() {
        /*Act*/
        model.requestPermissions();
        /*Assert*/
        verify(permissionHandler).requestPermission();
    }

    @Test
    public void startRecording() {
        /*Arrange*/
        AudioRecorder.AudioRecorderListener listener = mock(AudioRecorder.AudioRecorderListener.class);
        /*Act*/
        model.startRecording(listener);
        /*Assert*/
        assertEquals(model.getActualState(), STATE_RECORDING);
        verify(audioRecorder).startRecording(listener);
    }


    @Test
    public void shouldRequestPermission() {
        /*Act*/
        model.shouldRequestPermissions();
        /*Assert*/
        verify(permissionHandler).shouldRequestPermission();
    }

    @Test
    public void stopRecording(){
        /*Arrange*/
        when(audioRecorder.isRunning()).thenReturn(true);
        /*Act*/
        model.stopRecording();
        /*Assert*/
        verify(audioRecorder).stopRecording();
    }

    @Test
    public void stopPlayback(){
        /*Arrange*/
        when(audioPlayer.isRunning()).thenReturn(true);
        /*Act*/
        model.stopPlayback();
        /*Assert*/
        verify(audioPlayer).stopPlaying();
    }

    @Test
    public void startPlayback(){
        /*Arrange*/
        when(audioPlayer.isRunning()).thenReturn(false);
        when(audioRecorder.isRunning()).thenReturn(false);
        AudioPlayer.AudioPlayerListener listenerMock = mock(AudioPlayer.AudioPlayerListener.class);
        short[] bufferData = new short[]{1,2,3,4,5,6,7,8};
        /*Act*/
        model.startPlayback(listenerMock, bufferData);
        /*Assert*/
        assertEquals(model.getActualState(), STATE_PLAYBACK);
        verify(audioPlayer).startPlaying(eq(listenerMock), eq(bufferData));
    }


    @Test
    public void startPlayback_RecordAlreadyRunning(){
        /*Arrange*/
        when(audioPlayer.isRunning()).thenReturn(false);
        when(audioRecorder.isRunning()).thenReturn(true);
        AudioPlayer.AudioPlayerListener listenerMock = mock(AudioPlayer.AudioPlayerListener.class);
        short[] bufferData = new short[]{1,2,3,4,5,6,7,8};
        /*Act*/
        model.startPlayback(listenerMock, bufferData);
        /*Assert*/
        verify(audioPlayer, never()).startPlaying(any(AudioPlayer.AudioPlayerListener.class), any(short[].class));
    }

    @Test
    public void startPlayback_PlayerAlreadyRunning(){
        /*Arrange*/
        when(audioPlayer.isRunning()).thenReturn(true);
        when(audioRecorder.isRunning()).thenReturn(false);
        AudioPlayer.AudioPlayerListener listenerMock = mock(AudioPlayer.AudioPlayerListener.class);
        short[] bufferData = new short[]{1,2,3,4,5,6,7,8};
        /*Act*/
        model.startPlayback(listenerMock, bufferData);
        /*Assert*/
        verify(audioPlayer, never()).startPlaying(any(AudioPlayer.AudioPlayerListener.class), any(short[].class));
    }
}
