package com.facebook.devices.microphoneplayback;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.MicrophonePlaybackActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.MicrophonePlaybackView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.WaveGraphView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class MicrophonePlaybackViewTest {

    private MicrophonePlaybackView view;

    private MicrophonePlaybackActivity activity;
    private BusProvider.Bus bus;

    /*Bindings*/
    private FloatingTutorialView floatingTutorialView;
    private WaveGraphView waveGraphView;
    private ImageView ivFloatingState;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, MicrophonePlaybackActivity.class);
        activity = Robolectric.buildActivity(MicrophonePlaybackActivity.class, intent).create().start().resume().get();
        bus = mock(BusProvider.Bus.class);
        view = new MicrophonePlaybackView(activity, bus);

        /*Bindings*/
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        waveGraphView = activity.findViewById(R.id.wave_view);
        ivFloatingState = activity.findViewById(R.id.iv_floating_state);
    }

    @Test
    public void uiHandlerNotNull() {
        /*Assert*/
        assertNotNull(view.getUIHandler());
    }

    @Test
    public void showTutorial() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.microphone_playback_dialog_help_message);
        String positiveText = activity.getString(R.string.ok_base);
        /*Act*/
        view.onFloatingTutorialClicked();
        /*Assert*/
        AlertDialog shadowAlert = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(shadowAlert));
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(shadowAlert.getButton(DialogInterface.BUTTON_POSITIVE).getText().toString(), positiveText);
    }

    @Test
    public void showFloatingTutorialButton() {
        /*Act*/
        view.showFloatingTutorialButton();
        /*Assert*/
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showRecordingCircle() {
        /*Act*/
        view.showRecordingCircle();
        /*Assert*/
        assertFalse(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.red_one));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showRecordingCircle_FloatingAlreadyVisible() {
        /*Arrange*/
        ivFloatingState.setVisibility(View.VISIBLE);
        /*Act*/
        view.showRecordingCircle();
        /*Assert*/
        assertFalse(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.red_one));
    }

    @Test
    public void showPlayingCircle() {
        /*Act*/
        view.showPlayingCircle();
        /*Assert*/
        assertFalse(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.bright_aqua));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showRepeatPlaybackCircle() {
        /*Act*/
        view.showRepeatPlaybackCircle();
        /*Assert*/
        assertTrue(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.bright_aqua));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showRecordingCircle_NoVisible() {
        /*Act*/
        view.showRecordingCircle();
        /*Assert*/
        assertFalse(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.red_one));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }


    @Test
    public void showPlayingCircle_NoVisible() {
        /*Act*/
        view.showPlayingCircle();
        /*Assert*/
        assertFalse(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.bright_aqua));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showRepeatPlaybackCircle_NoVisible() {
        /*Act*/
        view.showRepeatPlaybackCircle();
        /*Assert*/
        assertTrue(ivFloatingState.hasOnClickListeners());
        assertEquals(ivFloatingState.getBackgroundTintList(),
                activity.getResources().getColorStateList(R.color.bright_aqua));
        assertEquals(ivFloatingState.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showInitialDialogOptionOneTest() {
        //Act
        view.showRepeatPlaybackCircle();
        ivFloatingState.callOnClick();
        //Assert
        verify(bus).post(any(MicrophonePlaybackView.RepeatEvent.class));
    }

    @Test
    public void drawAudioSample() {
        /*Arrange*/
        short[] samples = new short[]{1, 2, 3, 4, 5, 6};
        /*Act*/
        view.drawAudioSample(samples);
        /*Assert*/
        assertEquals(waveGraphView.getSamples(), samples);
    }

    @Test
    public void showRecordingDialog_Design() {
        /*Arrange*/
        String message = activity.getString(R.string.microphone_playback_record_message);
        String optionOneText = activity.getString(R.string.yes_base);
        String optionTwoText = activity.getString(R.string.no_base);
        /*Act*/
        view.showRecordingDialog();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView tvMessage = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(tvMessage.getText().toString(), message);
        assertEquals(bannerView.getBannerOptionOneButton().getText().toString(), optionOneText);
        assertEquals(bannerView.getBannerOptionTwoButton().getText().toString(), optionTwoText);
    }

    @Test
    public void showRecordingDialog_OptionOneBehavior() {
        /*Act*/
        view.showRecordingDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        /*Assert*/
        verify(bus).post(any(MicrophonePlaybackView.RecordPassEvent.class));
    }

    @Test
    public void showRecordingDialog_OptionTwoBehavior() {
        /*Act*/
        view.showRecordingDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().performClick();
        /*Assert*/
        verify(bus).post(any(MicrophonePlaybackView.RecordFailEvent.class));
    }

    @Test
    public void showPlaybackDialog_Design() {
        /*Arrange*/
        String message = activity.getString(R.string.microphone_playback_play_message);
        String optionOneText = activity.getString(R.string.yes_base);
        String optionTwoText = activity.getString(R.string.no_base);
        /*Act*/
        view.showPlaybackDialog();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView tvMessage = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(tvMessage.getText().toString(), message);
        assertEquals(bannerView.getBannerOptionOneButton().getText().toString(), optionOneText);
        assertEquals(bannerView.getBannerOptionTwoButton().getText().toString(), optionTwoText);
    }

    @Test
    public void showPlaybackDialog_OptionOneBehavior() {
        /*Act*/
        view.showPlaybackDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        /*Assert*/
        verify(bus).post(any(MicrophonePlaybackView.PlaybackPassEvent.class));
    }

    @Test
    public void showPlaybackDialog_OptionTwoBehavior() {
        /*Act*/
        view.showPlaybackDialog();
        ShadowView.getCurrentBanner(view).getBannerOptionTwoButton().performClick();
        /*Assert*/
        verify(bus).post(any(MicrophonePlaybackView.PlaybackFailEvent.class));
    }

}
