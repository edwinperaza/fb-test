package com.facebook.devices.microphoneplayback;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.devices.mvp.model.MicrophonePlaybackModel;
import com.facebook.devices.mvp.presenter.MicrophonePlaybackPresenter;
import com.facebook.devices.mvp.view.MicrophonePlaybackView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.AudioPlayer;
import com.facebook.devices.utils.AudioRecorder;
import com.facebook.devices.utils.GenericUtils;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.facebook.devices.mvp.model.MicrophonePlaybackModel.RECORDED_BUFFER;
import static com.facebook.devices.mvp.model.MicrophonePlaybackModel.STATE_PLAYBACK;
import static com.facebook.devices.mvp.model.MicrophonePlaybackModel.STATE_RECORDING;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GenericUtils.class})
public class MicrophonePlaybackPresenterTest {

    private MicrophonePlaybackPresenter presenter;

    @Mock
    private MicrophonePlaybackView view;
    @Mock
    private MicrophonePlaybackModel model;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(GenericUtils.class);

        when(view.getUIHandler()).thenReturn(mockHandler);

        presenter = new MicrophonePlaybackPresenter(model, view);
    }

    @Test
    public void saveInstantState_Recording() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle, never()).putShortArray(any(), any());
    }

    @Test
    public void saveInstantState_Playback() {
        /*Arrange*/
        when(model.getActualState()).thenReturn(STATE_PLAYBACK);
        Bundle bundle = mock(Bundle.class);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle).putShortArray(RECORDED_BUFFER, model.getRecordedBuffer());
    }

    @Test
    public void onResumed_PermissionGranted_Recording() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_RECORDING);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        verify(view).showRecordingDialog();
        verify(model).startRecording(any(AudioRecorder.AudioRecorderListener.class));
    }


    @Test
    public void onResumed_PermissionGranted_Playback() {
        /*Arrange*/
        short[] data = new short[]{};
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_PLAYBACK);
        when(model.getRecordedBuffer()).thenReturn(data);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        verify(view).showPlaybackDialog();
        verify(view).showPlayingCircle();
        verify(model).startPlayback(any(AudioPlayer.AudioPlayerListener.class), eq(data));
    }

    @Test
    public void onResumed_PermissionNotGranted_ShouldNotRequestPermission() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        when(model.shouldRequestPermissions()).thenReturn(false);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        verify(model).requestPermissions();
    }

    @Test
    public void onPaused_RecordState() {
        /*Arrange*/
        when(model.getActualState()).thenReturn(STATE_RECORDING);
        /*Act*/
        presenter.onPaused();
        /*Assert*/
        verify(model).stopRecording();
    }

    @Test
    public void onPaused_RecordPlaybackState() {
        /*Arrange*/
        when(model.getActualState()).thenReturn(STATE_PLAYBACK);
        /*Act*/
        presenter.onPaused();
        /*Assert*/
        verify(model).stopPlayback();
    }

    @Test
    public void onButtonRecordYesPressed() {
        /*Act*/
        presenter.onButtonRecordYesPressed(null);
        /*Assert*/
        verify(model).stopRecording();
    }

    @Test
    public void onButtonRecordFailedPressed() {
        /*Act*/
        presenter.onButtonRecordFailedPressed(null);
        /*Assert*/
        verify(model).stopRecording();
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onButtonPlaybackYesPressed() {
        /*Act*/
        presenter.onButtonPlaybackYesPressed(null);
        /*Assert*/
        verify(model).stopPlayback();
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onButtonPlaybackFailedPressed() {
        /*Act*/
        presenter.onButtonPlaybackFailedPressed(null);
        /*Assert*/
        verify(model).stopPlayback();
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onRepeatEvent(){
        /*Arrange*/
        short[] dataBuffer = new short[]{1,2,3,4,5};
        when(model.getRecordedBuffer()).thenReturn(dataBuffer);
        /*Act*/
        presenter.onRepeatEvent(null);
        /*Assert*/
        verify(view).showPlayingCircle();
        verify(model).startPlayback(any(AudioPlayer.AudioPlayerListener.class), eq(dataBuffer));
    }

    @Test
    public void onServiceConnected() {
        /*Arrange*/
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        /*Act*/
        presenter.onServiceConnected(null, binder);
        /*Assert*/
        verify(model).setService(binder.getService());
    }

    @Test
    public void onServiceDisconnected() {
        /*Act*/
        presenter.onServiceDisconnected(null);
        /*Assert*/
        verify(model).removeService();
    }

    @Test
    public void audioRecordListener_AvailableData(){
        /*Arrange*/
        short[] data = new short[]{1,2,3,4,5,6};
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_RECORDING);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        ArgumentCaptor<AudioRecorder.AudioRecorderListener> listenerCaptor = ArgumentCaptor.forClass(AudioRecorder.AudioRecorderListener.class);
        verify(model).startRecording(listenerCaptor.capture());
        listenerCaptor.getValue().onAvailableData(data);
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(view).drawAudioSample(eq(data));
    }

    @Test
    public void audioRecordListener_RecordCompleted(){
        /*Arrange*/
        short[] data = new short[]{1,2,3,4,5,6};
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_RECORDING);
        when(model.getRecordedBuffer()).thenReturn(data);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        ArgumentCaptor<AudioRecorder.AudioRecorderListener> listenerCaptor = ArgumentCaptor.forClass(AudioRecorder.AudioRecorderListener.class);
        verify(model).startRecording(listenerCaptor.capture());
        listenerCaptor.getValue().onRecordCompleted(data);
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(model).setRecordedBuffer(eq(data));
        verify(model).startPlayback(any(AudioPlayer.AudioPlayerListener.class), eq(data));
        verify(view).showPlaybackDialog();
        verify(view).showPlayingCircle();
    }

    @Test
    public void audioPlayerListener_PlayingDataAvailable(){
        /*Arrange*/
        short[] data = new short[]{1,2,3,4,5,6};
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_PLAYBACK);
        when(model.getRecordedBuffer()).thenReturn(data);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        ArgumentCaptor<AudioPlayer.AudioPlayerListener> listenerCaptor = ArgumentCaptor.forClass(AudioPlayer.AudioPlayerListener.class);
        verify(model).startPlayback(listenerCaptor.capture(), eq(data));
        listenerCaptor.getValue().onPlayingDataAvailable(data);
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(view).drawAudioSample(eq(data));
    }

    @Test
    public void audioPlayerListener_PlaybackCompleted(){
        /*Arrange*/
        short[] data = new short[]{1,2,3,4,5,6};
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getActualState()).thenReturn(STATE_PLAYBACK);
        when(model.getRecordedBuffer()).thenReturn(data);
        /*Act*/
        presenter.onResumed();
        /*Assert*/
        ArgumentCaptor<AudioPlayer.AudioPlayerListener> listenerCaptor = ArgumentCaptor.forClass(AudioPlayer.AudioPlayerListener.class);
        verify(model).startPlayback(listenerCaptor.capture(), eq(data));
        listenerCaptor.getValue().onPlaybackCompleted();
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler).post(runnableCaptor.capture());
        runnableCaptor.getValue().run();
        verify(view).showRepeatPlaybackCircle();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        RuntimePermissionHandler.PermissionDeniedEvent event = new RuntimePermissionHandler.PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }
}
