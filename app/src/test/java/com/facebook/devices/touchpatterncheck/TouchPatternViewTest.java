package com.facebook.devices.touchpatterncheck;

import android.app.AlertDialog;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.TouchPatternActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.TouchPatternView;
import com.facebook.devices.mvp.view.TouchPatternView.IncorrectDialogOptionPressedEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.TouchDetectorView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class TouchPatternViewTest {

    private TouchPatternView view;
    private TouchPatternActivity activity;

    @Mock
    private BusProvider.Bus bus;

    @Captor
    private ArgumentCaptor<IncorrectDialogOptionPressedEvent> incorrectDialogCaptor;

    /*Bindings*/
    private TouchDetectorView touchDetectorView;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, TouchPatternActivity.class);

        activity = Robolectric.buildActivity(TouchPatternActivity.class, intent).create().start().resume().get();
        view = new TouchPatternView(activity, bus);

        /*Bindings*/
        touchDetectorView = activity.findViewById(R.id.touch_detector);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void showTutorial() {
        //Arrange
        String title = activity.getString(R.string.touch_pattern_tutorial_message);
        //Act
        floatingTutorialView.getButton().performClick();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertThat(shadowAlertDialog.getMessage(), equalTo(title));
    }

    @Test
    public void incorrectPatternDialog() {
        //Arrange
        String title = activity.getString(R.string.touch_pattern_line_incorrect_message);
        //Act
        view.showIncorrectPatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));
        assertThat(shadowAlertDialog.getMessage(), equalTo(title));
        assertThat(dialog.getButton(AlertDialog.BUTTON_NEGATIVE).getText(), equalTo(activity.getString(R.string.no_base)));
        assertThat(dialog.getButton(AlertDialog.BUTTON_POSITIVE).getText(), equalTo(activity.getString(R.string.try_again_base)));
    }

    @Test
    public void showIncorrectPatternOptionPositiveDialog() {
        //Act
        view.showIncorrectPatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
        verify(bus).post(incorrectDialogCaptor.capture());
        assertEquals(incorrectDialogCaptor.getValue().getOptionSelected(), IncorrectDialogOptionPressedEvent.TRY_AGAIN);
    }

    @Test
    public void showIncorrectPatternOptionNegativeDialog() {
        //Act
        view.showIncorrectPatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).performClick();
        verify(bus).post(incorrectDialogCaptor.capture());
        assertEquals(incorrectDialogCaptor.getValue().getOptionSelected(), IncorrectDialogOptionPressedEvent.NO);
    }

    @Test
    public void incompletePatternDialog() {
        //Arrange
        String title = activity.getString(R.string.touch_pattern_line_incorrect_message);
        //Act
        view.showIncompletePatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(dialog));

        assertThat(shadowAlertDialog.getMessage(), equalTo(title));
        assertThat(dialog.getButton(AlertDialog.BUTTON_POSITIVE).getText(), equalTo(activity.getString(R.string.try_again_base)));
        assertThat(dialog.getButton(AlertDialog.BUTTON_NEGATIVE).getText(), equalTo(activity.getString(R.string.no_base)));
    }

    @Test
    public void incompletePatternOptionPositiveDialog() {
        //Act
        view.showIncompletePatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).performClick();
        verify(bus).post(incorrectDialogCaptor.capture());
        assertEquals(incorrectDialogCaptor.getValue().getOptionSelected(), IncorrectDialogOptionPressedEvent.TRY_AGAIN);
    }

    @Test
    public void incompletePatternOptionNegativeDialog() {
        //Act
        view.showIncompletePatternDialog();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).performClick();
        verify(bus).post(incorrectDialogCaptor.capture());
        assertEquals(incorrectDialogCaptor.getValue().getOptionSelected(), IncorrectDialogOptionPressedEvent.NO);
    }

    @Test
    public void touchDetectorViewOrientationSetting() {
        //Arrange
        int orientation = TouchDetectorView.ORIENTATION_HORIZONTAL;
        //Act
        view.setTouchDetectorOrientationMode(orientation, new int[9]);
        //Assert
        assertThat(touchDetectorView.getOrientationModeSet(), equalTo(orientation));
    }

    @Test
    public void touchDetectorViewSizeSetting() {
        //Arrange
        int size = 4;
        //Act
        view.setTouchDetectorSize(4, new int[16]);
        //Assert
        assertThat(touchDetectorView.getSizeModeSet(), equalTo(size));
    }

    @Test
    public void touchDetectorViewInitCheck() {
        //Arrange
        int size = 4;
        int orientation = TouchDetectorView.ORIENTATION_HORIZONTAL;
        int[] correctPath = new int[16];
        //Act
        view.initTouchDetectorView(orientation, size, correctPath);
        //Assert
        assertThat(touchDetectorView.getSizeModeSet(), equalTo(size));
        assertThat(touchDetectorView.getOrientationModeSet(), equalTo(orientation));
        assertThat(touchDetectorView.getCorrectPathSet(), equalTo(correctPath));
    }

    @Test
    public void showFloatingTutorialButton() {
        view.showFloatingTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideTutorialFloatingButtonTest() {
        view.hideFloatingButton();
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showBannerTest() {
        //Arrange
        String message = activity.getString(R.string.touch_pattern_banner_message);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.showBanner();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void showBannerOptionOneTest() {
        //Act
        view.showBanner();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(TouchPatternView.OnBannerOkPressedEvent.class));
    }

    @Test
    public void hideBannerTest() {
        //Act
        view.hideBanner();
        //Assert
        assertTrue(ShadowView.getCurrentBanner(view).isBannerHidden());
    }
}