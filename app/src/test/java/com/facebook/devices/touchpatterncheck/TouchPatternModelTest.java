package com.facebook.devices.touchpatterncheck;


import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.utils.customviews.TouchDetectorView;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TouchPatternModelTest {

    private TouchPatternModel model;
    private int initOrientation = TouchDetectorView.ORIENTATION_HORIZONTAL;
    private int size = TouchPatternModel.SIX_SIX;

    @Before
    public void setup() {
        model = new TouchPatternModel(initOrientation, size);
    }

    @Test
    public void initSizeCheck() {
        //Assert
        assertThat(model.getTouchSize(), equalTo(size));
    }

    @Test
    public void initOrientationCheck() {
        //Assert
        assertThat(model.getTouchOrientation(), equalTo(initOrientation));
    }

    @Test
    public void respectivePathCheck() {
        //Assert
        assertNotNull(model.generateRespectivePath());
    }


}
