package com.facebook.devices.touchpatterncheck;

import com.facebook.devices.mvp.model.TouchPatternModel;
import com.facebook.devices.mvp.presenter.TouchPatternPresenter;
import com.facebook.devices.mvp.view.TouchPatternView;
import com.facebook.devices.utils.customviews.TouchDetectorView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TouchPatternPresenterTest {

    private TouchPatternModel model;
    private TouchPatternView view;
    private TouchPatternPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = mock(TouchPatternModel.class);
        view = mock(TouchPatternView.class);

        presenter = new TouchPatternPresenter(model, view);
    }


    @Test
    public void initCheck() {
        verify(view).initTouchDetectorView(model.getTouchOrientation(), model.getTouchSize(), model.generateRespectivePath());
    }

    @Test
    public void onBannerIntroClosedTest() {
        //Act
        presenter.onBannerIntroClosed(new TouchPatternView.OnBannerOkPressedEvent());
        //Assert
        verify(view).hideBanner();
        verify(view).hideFloatingButton();
        verify(view).setTouchDetectorEnabled(true);
    }

    @Test
    public void touchDetectorCallbackSuccess() {
        //Act
        presenter.onTouchDetectorCallback(new TouchPatternView.OnTouchDetectorViewCallback(TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_SUCCESS));
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void touchDetectorCallbackFailed() {
        //Act
        presenter.onTouchDetectorCallback(new TouchPatternView.OnTouchDetectorViewCallback(TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_FAILED));
        //Assert
        verify(view).showIncorrectPatternDialog();
    }

    @Test
    public void touchDetectorCallbackIncomplete() {
        //Act
        presenter.onTouchDetectorCallback(new TouchPatternView.OnTouchDetectorViewCallback(TouchDetectorView.TouchDetectorViewCallback.FINISHED_PATTERN_FAILED_INCOMPLETE));
        //Assert
        verify(view).showIncompletePatternDialog();
    }

    @Test
    public void incorrectPatternDialogInteractionNo() {
        //Act
        presenter.onIncorrectPatterMessageInteracted(new TouchPatternView.IncorrectDialogOptionPressedEvent(TouchPatternView.IncorrectDialogOptionPressedEvent.NO));
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void incorrectPatternDialogInteractionTryAgain() {
        //Act
        presenter.onIncorrectPatterMessageInteracted(new TouchPatternView.IncorrectDialogOptionPressedEvent(TouchPatternView.IncorrectDialogOptionPressedEvent.TRY_AGAIN));
        //Assert
        verify(view).resetTouchDetectorView();
    }
}
