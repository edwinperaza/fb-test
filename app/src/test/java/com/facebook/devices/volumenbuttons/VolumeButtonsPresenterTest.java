package com.facebook.devices.volumenbuttons;


import android.view.KeyEvent;

import com.facebook.devices.mvp.model.VolumeButtonsModel;
import com.facebook.devices.mvp.presenter.VolumeButtonsPresenter;
import com.facebook.devices.mvp.view.VolumeButtonsView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

public class VolumeButtonsPresenterTest {

    private VolumeButtonsPresenter presenter;

    @Mock
    private VolumeButtonsModel model;

    @Mock
    private VolumeButtonsView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new VolumeButtonsPresenter(model, view);
    }

    @Test
    public void verifyInitOk() {
        verify(view).showFloatingTutorial();
        verify(view).showQuestionUp();
    }

    @Test
    public void onKeyUpNormal() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_UP;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_UP);
        when(model.isStepUpCompleted()).thenReturn(false);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model).increaseTouchedTimes();
        verify(view).updateCountValuePositive(model.getTouchedTimes());
        verify(view, never()).completeUpKeyDelay();
    }

    @Test
    public void onKeyUpCompleted() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_UP;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_UP);
        when(model.isStepUpCompleted()).thenReturn(true);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model).increaseTouchedTimes();
        verify(view).updateCountValuePositive(model.getTouchedTimes());
        verify(view).completeUpKeyDelay();
    }

    @Test
    public void onKeyUpNormal_Incorrect() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_UP;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_DOWN);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model, never()).increaseTouchedTimes();
        verify(view, never()).updateCountValuePositive(model.getTouchedTimes());
    }

    @Test
    public void onKeyDownNormal() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_DOWN;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_DOWN);
        when(model.isStepUpCompleted()).thenReturn(false);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model).increaseTouchedTimes();
        verify(view).updateCountValueNegative(model.getTouchedTimes());
        verify(model, never()).pass();
        verify(view, never()).finishActivity();
    }

    @Test
    public void onKeyDownCompleted() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_DOWN;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_DOWN);
        when(model.isStepDownCompleted()).thenReturn(true);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model).increaseTouchedTimes();
        verify(view).updateCountValueNegative(model.getTouchedTimes());
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onKeyDownNormal_Incorrect() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_DOWN;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_UP);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model, never()).increaseTouchedTimes();
        verify(view, never()).updateCountValueNegative(model.getTouchedTimes());
        verify(model, never()).isStepDownCompleted();
    }

    @Test
    public void onKeyDownOther() {
        //Arrange
        int keyCode = KeyEvent.KEYCODE_VOLUME_MUTE;
        when(model.getCurrentMode()).thenReturn(VolumeButtonsModel.MODE_UP);
        //Act
        presenter.onKeyDown(keyCode);
        //Assert
        verify(model, never()).increaseTouchedTimes();
        verify(view, never()).updateCountValueNegative(model.getTouchedTimes());
        verify(model, never()).isStepDownCompleted();
    }

    @Test
    public void onHandlerUICompleteEvent() {
        //Act
        presenter.onHandlerUiUpCompleteEvent(null);
        //Assert
        verify(view).showQuestionDown();
        verify(view).updateCountValuePositive(model.getTouchedTimes());
    }

    @Test
    public void onButtonFailureClicked() {
        //Act
        presenter.onButtonFailureClicked(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }
}
