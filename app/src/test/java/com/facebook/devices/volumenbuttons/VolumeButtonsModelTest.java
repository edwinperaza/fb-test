package com.facebook.devices.volumenbuttons;


import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.VolumeButtonsActivity;
import com.facebook.devices.mvp.model.VolumeButtonsModel;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

//import android.support.test.BuildConfig;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VolumeButtonsModelTest {

    private VolumeButtonsModel model;
    private VolumeButtonsActivity activity;
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, VolumeButtonsActivity.class);
        activity = Robolectric.buildActivity(VolumeButtonsActivity.class, intent).create().start().resume().get();
        bus = BusProvider.getInstance();

        model = new VolumeButtonsModel();
    }

    @Test
    public void checkCorrectInitMode() {
        assertEquals(model.getCurrentMode(), VolumeButtonsModel.MODE_UP);
        assertEquals(model.getTouchedTimes(), 0);
    }

    @Test
    public void checkCountUpNormal() {
        //Arrange
        int previousState = model.getTouchedTimes();
        //Act
        model.increaseTouchedTimes();
        //Assert
        assertTrue(previousState < model.getTouchedTimes());
    }

    @Test
    public void checkCountDownNormal() {
        //Arrange
        model.isStepDownCompleted();
        model.isStepDownCompleted();
        model.isStepDownCompleted();
        //Assert
        assertEquals(0, model.getTouchedTimes());
        assertEquals(model.getCurrentMode(), VolumeButtonsModel.MODE_UP);
    }

    @Test
    public void checkChangeMode() {
        //Act
        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        model.isStepUpCompleted();
        //Assert
        assertEquals(0, model.getTouchedTimes());
        assertEquals(model.getCurrentMode(), VolumeButtonsModel.MODE_DOWN);
    }

    @Test
    public void checkCompleteFlow() {
        //Act
        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        model.isStepUpCompleted();

        assertEquals(model.getCurrentMode(), VolumeButtonsModel.MODE_DOWN);

        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        model.increaseTouchedTimes();
        //Assert
        assertTrue(model.isStepDownCompleted());

        assertEquals(model.getCurrentMode(), VolumeButtonsModel.MODE_DOWN);

        assertEquals(0, model.getTouchedTimes());
    }

}
