package com.facebook.devices.volumenbuttons;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.VolumeButtonsActivity;
import com.facebook.devices.mvp.view.VolumeButtonsAndSoundView;
import com.facebook.devices.mvp.view.VolumeButtonsView;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.VolumeButtonsView.OnButtonFailureClicked;
import com.facebook.devices.utils.CenteredImageSpan;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowLooper;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class VolumeButtonsViewTest {

    private VolumeButtonsActivity activity;
    private VolumeButtonsView view;

    @Mock
    private BusProvider.Bus bus;

    /*Bindings*/
    TextView tvCountZero;
    TextView tvUpCountOne;
    TextView tvUpCountTwo;
    TextView tvUpCountThree;
    TextView tvDownCountOne;
    TextView tvDownCountTwo;
    TextView tvDownCountThree;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, VolumeButtonsActivity.class);
        activity = Robolectric.buildActivity(VolumeButtonsActivity.class, intent).create().start().resume().get();

        view = new VolumeButtonsView(activity, bus);

        /*Bindings*/
        tvCountZero = activity.findViewById(R.id.tv_count_zero);
        tvUpCountOne = activity.findViewById(R.id.tv_up_count_one);
        tvUpCountTwo = activity.findViewById(R.id.tv_up_count_two);
        tvUpCountThree = activity.findViewById(R.id.tv_up_count_three);
        tvDownCountOne = activity.findViewById(R.id.tv_down_count_one);
        tvDownCountTwo = activity.findViewById(R.id.tv_down_count_two);
        tvDownCountThree = activity.findViewById(R.id.tv_down_count_three);
    }

    @Test
    public void completeUpKeyDelay() {
        //Act
        view.completeUpKeyDelay();
        //Assert
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks();
        verify(bus).post(any(VolumeButtonsView.OnHandlerUiUpCompleteEvent.class));
    }

    @Test
    public void updateCountValuePositive() {
        //Act
        view.updateCountValuePositive(1);
        //Assert
        assertEquals(tvUpCountOne.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValuePositive(2);
        //Assert
        assertEquals(tvUpCountTwo.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValuePositive(3);
        //Assert
        assertEquals(tvUpCountThree.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValuePositive(0);
        //Assert
        assertEquals(tvCountZero.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        assertEquals(tvUpCountOne.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
        assertEquals(tvUpCountTwo.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
        assertEquals(tvUpCountThree.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
    }

    @Test
    public void updateCountValueNegative() {
        //Act
        view.updateCountValueNegative(1);
        //Assert
        assertEquals(tvDownCountOne.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValueNegative(2);
        //Assert
        assertEquals(tvDownCountTwo.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValueNegative(3);
        //Assert
        assertEquals(tvDownCountThree.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        //Act
        view.updateCountValueNegative(0);
        //Assert
        assertEquals(tvCountZero.getAlpha(), VolumeButtonsView.ALPHA_ENABLED);
        assertEquals(tvDownCountOne.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
        assertEquals(tvDownCountTwo.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
        assertEquals(tvDownCountThree.getAlpha(), VolumeButtonsView.ALPHA_DISABLED);
    }

    @Test
    public void onFloatingTutorialClickedTest() {
        //Arrange
        String title  = activity.getString(R.string.more_help_base);
        String okButton  = activity.getString(R.string.ok_base);
        CenteredImageSpan imagePlusSpan  = new CenteredImageSpan(activity, R.drawable.plus_button_small);
        CenteredImageSpan imageLessSpan  = new CenteredImageSpan(activity, R.drawable.less_button_small);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_tutorial_no_sound_tutorial_message));
        spannableString.setSpan(imagePlusSpan, 13, 14, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(imageLessSpan, 15, 16, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        /*Assert*/
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), spannableString.toString());
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), okButton);
    }

    @Test
    public void showQuestionUpTest() {
        //Arrange
        CenteredImageSpan imageSpan = new CenteredImageSpan(activity, R.drawable.plus_button);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_up_no_sound_question_message));
        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showQuestionUp();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(spannableString.toString(), messageDialog.getText().toString());
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showQuestionUpOptionOneTest() {
        //Act
        view.showQuestionUp();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnButtonFailureClicked.class));
    }

    @Test
    public void showQuestionDownTest() {
        //Arrange
        CenteredImageSpan imageSpan = new CenteredImageSpan(activity, R.drawable.less_button);
        SpannableString spannableString = new SpannableString(activity.getString(R.string.volume_down_no_sound_question_message));
        int start = 7;
        int end = 8;
        spannableString.setSpan(imageSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showQuestionDown();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(spannableString.toString(), messageDialog.getText().toString());
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showQuestionDownOptionOneTest() {
        //Act
        view.showQuestionDown();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnButtonFailureClicked.class));
    }

}
