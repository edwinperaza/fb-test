package com.facebook.devices.orientationcheck;


import android.content.res.Configuration;
import android.os.Bundle;

import com.facebook.devices.mvp.model.OrientationModel;
import com.facebook.devices.mvp.presenter.OrientationPresenter;
import com.facebook.devices.mvp.view.OrientationView;
import com.facebook.devices.utils.AlertDialogBaseEvent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT;
import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_ONE;
import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_ONE;
import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_TWO;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrientationPresenterTest {

    private OrientationModel model;
    private OrientationView view;
    private OrientationPresenter presenter;

    @OrientationModel.OrientationStateMode
    private int initOrientationState = ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = mock(OrientationModel.class);
        view = mock(OrientationView.class);

        when(model.getActualState()).thenReturn(initOrientationState);

        presenter = new OrientationPresenter(model, view);
    }

    @Test
    public void saveInstanceStateTest() {
        /*Arrange*/
        when(model.getActualState()).thenReturn(2);
        Bundle bundle = mock(Bundle.class);
        /*Act*/
        presenter.saveInstanceState(bundle);
        /*Assert*/
        verify(bundle).putInt(OrientationModel.ORIENTATION_STATE_SAVED, model.getActualState());
    }

    @Test
    public void onConfigurationChanged_WrongOrientation() {
        //Arrange
        Configuration config = new Configuration();
        config.orientation = Configuration.ORIENTATION_LANDSCAPE;
        when(model.getActualState()).thenReturn(ORIENTATION_STATE_MODE_LANDSCAPE_INCORRECT);
        //Act
        presenter.onConfigurationChanged(config);
        //Assert
        verify(model).nextState();
    }

    @Test
    public void onConfigurationChanged_PortraitOne() {
        //Arrange
        Configuration config = new Configuration();
        config.orientation = Configuration.ORIENTATION_PORTRAIT;
        when(model.getActualState()).thenReturn(ORIENTATION_STATE_MODE_PORTRAIT_ONE);
        //Act
        presenter.onConfigurationChanged(config);
        //Assert
        verify(model).nextState();
    }

    @Test
    public void onConfigurationChangedLandscapeTest() {
        //Arrange
        Configuration config = new Configuration();
        config.orientation = Configuration.ORIENTATION_LANDSCAPE;
        when(model.getActualState()).thenReturn(ORIENTATION_STATE_MODE_LANDSCAPE_ONE);
        //Act
        presenter.onConfigurationChanged(config);
        //Assert
        verify(model).nextState();
    }

    @Test
    public void onConfigurationChangedPortraitTwoTest() {
        //Arrange
        Configuration config = new Configuration();
        config.orientation = Configuration.ORIENTATION_PORTRAIT;
        when(model.getActualState()).thenReturn(ORIENTATION_STATE_MODE_PORTRAIT_TWO);
        //Act
        presenter.onConfigurationChanged(config);
        //Assert
        verify(model).nextState();
    }

    @Test
    public void onDialogQuestionLandscapePositiveTest() {
        //Act
        presenter.onDialogQuestionLandscape(new OrientationView.DialogQuestionLandscape(AlertDialogBaseEvent.OPTION_POSITIVE));
        //Assert
        verify(view).showLandscapeNextStep();
    }

    @Test
    public void onDialogQuestionLandscapeNegativeTest() {
        //Act
        presenter.onDialogQuestionLandscape(new OrientationView.DialogQuestionLandscape(AlertDialogBaseEvent.OPTION_NEGATIVE));
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogQuestionPortraitOnePositiveTest() {
        //Act
        presenter.onDialogQuestionPortraitOne(new OrientationView.DialogQuestionPortraitOne(AlertDialogBaseEvent.OPTION_POSITIVE));
        //Assert
        verify(view).showQuestionDialogPortraitTwo();
    }

    @Test
    public void onDialogQuestionPortraitOneNegativeTest() {
        //Act
        presenter.onDialogQuestionPortraitOne(new OrientationView.DialogQuestionPortraitOne(AlertDialogBaseEvent.OPTION_NEGATIVE));
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogQuestionPortraitTwoPositiveTest() {
        //Act
        presenter.onDialogQuestionPortraitTwo(new OrientationView.DialogQuestionPortraitTwo(AlertDialogBaseEvent.OPTION_POSITIVE));
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogQuestionPortraitTwoNegativeTest() {
        //Act
        presenter.onDialogQuestionPortraitTwo(new OrientationView.DialogQuestionPortraitTwo(AlertDialogBaseEvent.OPTION_NEGATIVE));
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onWrongPositionEventTest() {
        //Act
        presenter.onWrongPositionEvent(new OrientationView.WrongPositionEvent());
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onTutorialPortraitEventTest() {
        //Act
        presenter.onTutorialPortraitEvent(new OrientationView.TutorialPortraitEvent());
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onTutorialLandscapeEventTest() {
        //Act
        presenter.onTutorialLandscapeEvent(new OrientationView.TutorialLandscapeEvent());
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onDestroyTest() {
        //Act
        presenter.onDestroy();
        //Assert
        verify(view).onDestroy();
    }
}
