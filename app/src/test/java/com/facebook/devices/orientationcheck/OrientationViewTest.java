package com.facebook.devices.orientationcheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.OrientationActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.OrientationView;
import com.facebook.devices.mvp.view.OrientationView.DialogQuestionLandscape;
import com.facebook.devices.mvp.view.OrientationView.DialogQuestionPortraitOne;
import com.facebook.devices.mvp.view.OrientationView.DialogQuestionPortraitTwo;
import com.facebook.devices.mvp.view.OrientationView.TutorialLandscapeEvent;
import com.facebook.devices.mvp.view.OrientationView.TutorialPortraitEvent;
import com.facebook.devices.mvp.view.OrientationView.WrongPositionEvent;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class OrientationViewTest {

    private OrientationActivity activity;
    private OrientationView view;

    @Mock
    private BusProvider.Bus bus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, OrientationActivity.class);

        activity = Robolectric.buildActivity(OrientationActivity.class, intent).create().start().resume().get();
        view = new OrientationView(activity, bus);
    }

    @Test
    public void tutorialButtonClick() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.orientation_tutorial_dialog_text);
        String option = activity.getString(R.string.ok_base);

        /*Act*/
        view.onFloatingTutorialClicked();

        /*Assert*/
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(shadowOf(alertDialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), option);
    }

    @Test
    public void showIntroWrongOrientationMessage() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_dialog_text_wrong);
        String option = activity.getString(R.string.fail_base);

        /*Act*/
        view.showTutorialWrongOrientationMessage();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), option);
    }

    @Test
    public void showIntroWrongOrientationMessageTest() {
        /*Act*/
        view.showTutorialWrongOrientationMessage();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(WrongPositionEvent.class));
    }

    @Test
    public void showTutorialPortraitTest() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_intro_dialog_message_portrait);
        String option = activity.getString(R.string.fail_base);

        /*Act*/
        view.showTutorialPortrait();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), option);
    }

    @Test
    public void showTutorialPortraitOptionOneTest() {
        /*Act*/
        view.showTutorialPortrait();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(TutorialPortraitEvent.class));
    }

    @Test
    public void showIntroDialogLandscape() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_intro_dialog_message_landscape);
        String option = activity.getString(R.string.fail_base);

        /*Act*/
        view.showLandscapeNextStep();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), option);
    }

    @Test
    public void showLandscapeNextStepOptionOneTest() {
        /*Act*/
        view.showLandscapeNextStep();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(TutorialLandscapeEvent.class));
    }

    @Test
    public void showQuestionDialogLandscape() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_landscape_question_dialog_message);
        String optionPositive = activity.getString(R.string.yes_base);
        String optionNegative = activity.getString(R.string.no_base);

        /*Act*/
        view.showQuestionDialogLandscape();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), optionPositive);
        assertEquals(bannerView.getBannerTextOptionTwo(), optionNegative);
    }

    @Test
    public void showQuestionDialogLandscapeOptionOneTest() {
        ArgumentCaptor<DialogQuestionLandscape> captor = ArgumentCaptor.forClass(DialogQuestionLandscape.class);
        /*Act*/
        view.showQuestionDialogLandscape();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void showQuestionDialogLandscapeOptionTwoTest() {
        ArgumentCaptor<DialogQuestionLandscape> captor = ArgumentCaptor.forClass(DialogQuestionLandscape.class);
        /*Act*/
        view.showQuestionDialogLandscape();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEGATIVE);
    }

    @Test
    public void showQuestionDialogPortraitOneTest() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_portrait_question_dialog_one_message);
        String optionPositive = activity.getString(R.string.no_base);
        String optionNegative = activity.getString(R.string.yes_base);

        /*Act*/
        view.showQuestionDialogPortraitOne();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), optionNegative);
        assertEquals(bannerView.getBannerTextOptionTwo(), optionPositive);
    }

    @Test
    public void showQuestionDialogPortraitOneOptionOneTest() {
        ArgumentCaptor<DialogQuestionPortraitOne> captor = ArgumentCaptor.forClass(DialogQuestionPortraitOne.class);
        /*Act*/
        view.showQuestionDialogPortraitOne();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void showQuestionDialogPortraitOneOptionTwoTest() {
        ArgumentCaptor<DialogQuestionPortraitOne> captor = ArgumentCaptor.forClass(DialogQuestionPortraitOne.class);
        /*Act*/
        view.showQuestionDialogPortraitOne();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEGATIVE);
    }

    @Test
    public void showQuestionDialogPortraitTwo() {
        /*Arrange*/
        String message = activity.getString(R.string.orientation_portrait_question_dialog_two_message);
        String optionPositive = activity.getString(R.string.no_base);
        String optionNegative = activity.getString(R.string.yes_base);

        /*Act*/
        view.showQuestionDialogPortraitTwo();

        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), optionPositive);
        assertEquals(bannerView.getBannerTextOptionTwo(), optionNegative);
    }

    @Test
    public void showQuestionDialogPortraitTwoOptionOneTest() {
        ArgumentCaptor<DialogQuestionPortraitTwo> captor = ArgumentCaptor.forClass(DialogQuestionPortraitTwo.class);
        /*Act*/
        view.showQuestionDialogPortraitTwo();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void showQuestionDialogPortraitTwoOptionTwoTest() {
        ArgumentCaptor<DialogQuestionPortraitTwo> captor = ArgumentCaptor.forClass(DialogQuestionPortraitTwo.class);
        /*Act*/
        view.showQuestionDialogPortraitTwo();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_NEGATIVE);
    }
}