package com.facebook.devices.orientationcheck;


import com.facebook.devices.mvp.model.OrientationModel;

import org.junit.Before;
import org.junit.Test;

import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_LANDSCAPE_ONE;
import static com.facebook.devices.mvp.model.OrientationModel.ORIENTATION_STATE_MODE_PORTRAIT_ONE;
import static junit.framework.Assert.assertEquals;

public class OrientationModelTest {

    private OrientationModel model;

    @OrientationModel.OrientationStateMode
    private int initOrientation = ORIENTATION_STATE_MODE_PORTRAIT_ONE;

    @Before
    public void setup() {
        model = new OrientationModel(initOrientation);
    }

    @Test
    public void checkInitialOrientation() {
        //Assert
        assertEquals(model.getActualState(), initOrientation);
    }

    @Test
    public void nextStepBehaviorCheck() {
        //Act
        model.nextState();
        //Assert
        assertEquals(model.getActualState(), ORIENTATION_STATE_MODE_LANDSCAPE_ONE);
        //Act
        model.nextState();
        model.nextState();
        //Assert
        assertEquals(model.getActualState(), ORIENTATION_STATE_MODE_LANDSCAPE_ONE);
    }
}
