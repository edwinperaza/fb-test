package com.facebook.devices.speakerfilecheck;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.SpeakerFileActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.SpeakerFileView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.devices.utils.customviews.WaveGraphView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.view.SpeakerFileView.*;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class SpeakerFileViewTest {

    private SpeakerFileActivity activity;
    private SpeakerFileView view;

    @Mock
    private BusProvider.Bus bus;

    /*UI stuffs*/
    private WaveGraphView waveView;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, SpeakerFileActivity.class);

        activity = Robolectric.buildActivity(SpeakerFileActivity.class, intent).create().start().resume().get();
        view = new SpeakerFileView(activity, bus);

        /*Utils*/
        waveView = activity.findViewById(R.id.wave_view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void showTutorialFloatingButtonTest() {
        view.showTutorialFloatingButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showTutorialTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.speaker_file_tutorial_text);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(dialog));
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void setAudioBytesTest() {
        //Arrange
        byte[] bytes = new byte[]{};
        //Act
        view.updateWaveView(bytes);
        //Assert
        assertThat(waveView.getBytes(), equalTo(bytes));
    }

    @Test
    public void showDialogStateOneTest() {
        //Arrange
        String message = activity.getString(R.string.speaker_test_sound_message);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);

        //Act
        view.showDialogStateOne();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showDialogStateOneOptionOneTest() {
        /*Act*/
        view.showDialogStateOne();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogOnePassEvent.class));
    }

    @Test
    public void showDialogStateOneOptionTwoTest() {
        /*Act*/
        view.showDialogStateOne();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(any(DialogFailedPressedEvent.class));
    }

    @Test
    public void showDialogStateTwoTest() {
        //Arrange
        String message = activity.getString(R.string.speaker_test_distortion_message);
        String positiveText = activity.getString(R.string.no_base);
        String negativeText = activity.getString(R.string.yes_base);

        //Act
        view.showDialogStateTwo();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showDialogStateTwoOptionOneTest() {
        /*Act*/
        view.showDialogStateTwo();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogTwoPassEvent.class));
    }

    @Test
    public void showDialogStateTwoOptionTwoTest() {
        /*Act*/
        view.showDialogStateTwo();
        /*Assert*/
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(any(DialogFailedPressedEvent.class));
    }
}
