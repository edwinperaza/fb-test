package com.facebook.devices.speakerfilecheck;


import android.media.audiofx.Visualizer;

import com.facebook.devices.mvp.model.SpeakerFileModel;
import com.facebook.devices.mvp.presenter.SpeakerFilePresenter;
import com.facebook.devices.mvp.view.SpeakerFileView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.permission.RuntimePermissionHandler.PermissionDeniedEvent;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpeakerFilePresenterTest {

    private SpeakerFilePresenter presenter;

    @Mock
    private SpeakerFileModel model;

    @Mock
    private SpeakerFileView view;

    @Mock
    private Visualizer visualizer;

    @Captor
    private ArgumentCaptor<Visualizer.OnDataCaptureListener> onDataCaptureListener;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new SpeakerFilePresenter(model, view);
    }

    @Test
    public void init() {
        verify(model).setDefaultVolume();
        verify(view).showTutorialFloatingButton();
    }

    @Test
    public void onResumedAndPermissionGrantedAndQuestionOneTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getQuestionState()).thenReturn(SpeakerFileModel.QUESTION_STATE_1);
        byte[] waveform = new byte[10];
        //Act
        presenter.onResumed();
        //Assert
        verify(view).showDialogStateOne();
        verify(model).startPlayer(onDataCaptureListener.capture());

        onDataCaptureListener.getValue().onWaveFormDataCapture(visualizer, waveform, 0);
        verify(view).updateWaveView(waveform);
    }

    @Test
    public void onResumedAndPermissionGrantedAndQuestionTwoTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.getQuestionState()).thenReturn(SpeakerFileModel.QUESTION_STATE_2);
        //Act
        presenter.onResumed();
        //Assert
        verify(view).showDialogStateTwo();
        verify(model).startPlayer(any(Visualizer.OnDataCaptureListener.class));
    }

    @Test
    public void isPermissionNotGrantedAndNotShouldRequestPermissionTest() {
        //Arrange
        when(model.isPermissionGranted()).thenReturn(false);
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onResumed();
        //Assert
        verify(model).requestPermissions();
    }

    @Test
    public void onPausedTest() {
        //Act
        presenter.onPaused();
        //Assert
        verify(model).stopPlayer();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        //Act
        presenter.onRequestPermissionResult(new PermissionDeniedEvent());
        //Arrange
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }

    @Test
    public void onDialogOnePassPressedTest() {
        //Act
        presenter.onDialogOnePassPressed(null);
        //Assert
        verify(model).nextQuestion();
        verify(view).showDialogStateTwo();
    }

    @Test
    public void onDialogTwoPassPressedTest() {
        //Act
        presenter.onDialogTwoPassPressed(null);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void testFailedClicked() {
        //Act
        presenter.onDialogFailedPressed(null);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

}
