package com.facebook.devices.speakerfilecheck;

import android.content.Context;
import android.net.Uri;

import com.facebook.devices.mvp.model.SpeakerFileModel;
import com.facebook.devices.mvp.model.SpeakerModel;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.VolumeManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

public class SpeakerFileModelTest {

    private SpeakerFileModel model;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    @Mock
    private VolumeManager volumeManager;

    @Mock
    private Context context;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        model = new SpeakerFileModel(context, permissionHandler, Uri.EMPTY, volumeManager);
    }

    @Test
    public void checkInit() {
        //Assert
        assertEquals(model.getQuestionState(), SpeakerModel.QUESTION_STATE_1);
    }

    @Test
    public void nextQuestionTest() {
        //Act
        model.nextQuestion();
        //Assert
        assertEquals(model.getQuestionState(), SpeakerModel.QUESTION_STATE_2);
    }

    @Test
    public void shouldRequestPermissionsTest() {
        //Act
        model.shouldRequestPermissions();
        //Assert
        assertFalse(permissionHandler.shouldRequestPermission());
    }
}
