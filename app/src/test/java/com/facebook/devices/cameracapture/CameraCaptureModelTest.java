package com.facebook.devices.cameracapture;

import android.graphics.ImageFormat;
import android.graphics.Point;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.util.Size;
import android.view.TextureView;

import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.model.CameraCaptureModel;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.camera.CameraUtils;
import com.facebook.devices.utils.customcomponents.CancellableHandler;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ImageReader.class)
public class CameraCaptureModelTest {

    private CameraCaptureModel model;

    @Mock
    private CameraManager cameraManager;
    @Mock
    private Point point;
    @Mock
    private BusProvider.Bus bus;
    @Mock
    private CameraUtils cameraUtils;
    @Mock
    private CancellableHandler uiHandler;
    @Mock
    private RuntimePermissionHandler runtimePermissionHandler;
    @Mock
    private TextureView textureView;
    @Mock
    private CameraCharacteristics cameraCharacteristics;
    @Mock
    private StreamConfigurationMap streamConfigurationMap;
    @Mock
    private ImageReader imageReader;

    private String photoFilePath = "File/path";
    private boolean isMirrored = true;
    private int sensorOrientation = 0;
    private Size[] outputsizes = new Size[]{new Size(100, 100), new Size(200, 200), new Size(300, 300)};

    @Before
    public void setup() {
        PowerMockito.mockStatic(ImageReader.class);
        when(ImageReader.newInstance(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(imageReader);
        MockitoAnnotations.initMocks(this);

        try {
            when(cameraManager.getCameraCharacteristics(anyString())).thenReturn(cameraCharacteristics);
            when(cameraUtils.getCameraStreamConfigurationMap(eq(cameraManager), anyString())).thenReturn(streamConfigurationMap);
            when(streamConfigurationMap.getOutputSizes(eq(ImageFormat.JPEG))).thenReturn(outputsizes);
            when(cameraUtils.getSensorOrientation(eq(cameraManager), anyString())).thenReturn(sensorOrientation);
            when(cameraUtils.getCameraLensFacing(eq(cameraManager), anyString())).thenReturn(CameraCharacteristics.LENS_FACING_BACK);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        model = new CameraCaptureModel(cameraManager, point, bus,
                runtimePermissionHandler, cameraUtils, uiHandler, photoFilePath, isMirrored);
    }

    @Test
    public void flowActiveBehavior() {
        /*Arrange*/
        boolean isFlowActive = false;
        /*Act*/
        model.setFlowActive(isFlowActive);
        /*Assert*/
        assertFalse(model.isFlowActive());
    }

    @Test
    public void getUIHandler() {
        /*Assert*/
        assertEquals(uiHandler, model.getUiHandler());
    }

    @Test
    public void permissionGranted() {
        /*Arrange*/
        when(runtimePermissionHandler.isPermissionGranted()).thenReturn(true);
        /*Assert*/
        assertTrue(model.isPermissionGranted());
    }

    @Test
    public void shouldRequestPermission() {
        /*Arrange*/
        when(runtimePermissionHandler.shouldRequestPermission()).thenReturn(false);
        /*Assert*/
        assertFalse(model.shouldRequestPermissions());
    }

    @Test
    public void requestPermission() {
        /*Act*/
        model.requestPermissions();
        /*Assert*/
        verify(runtimePermissionHandler).requestPermission();
    }

    @Test
    public void isMirrored() {
        /*Assert*/
        assertEquals(model.isMirrored(), isMirrored);
    }

    @Test
    public void getResourcesManager() {
        /*Assert*/
        assertNotNull(model.getResourcesManager());
    }

    @Test
    public void getPhotoFilePath() {
        /*Assert*/
        assertEquals(model.getPhotoAbsoluteFilePath(), photoFilePath);
    }

    @Test
    public void setPhotoFilePath() {
        /*Arrange*/
        String path = "NewPath";
        /*Act*/
        model.setActualPhotoFile(path);
        /*Assert*/
        assertEquals(model.getPhotoAbsoluteFilePath(), path);
    }

    @Test
    public void wasImageTaken_Taken() {
        /*Arrange*/
        String path = "NewPath";
        /*Act*/
        model.setActualPhotoFile(path);
        /*Assert*/
        assertTrue(model.wasImageTaken());
    }

    @Test
    public void wasImageTaken_NotTaken() {
        /*Arrange*/
        String path = null;
        /*Act*/
        model.setActualPhotoFile(path);
        /*Assert*/
        assertFalse(model.wasImageTaken());
    }

    @Test
    public void getWaitTimeMillis() {
        /*Assert*/
        assertNotNull(model.getWaitTimeMillis());
    }

    @Test
    public void openCamera_CameraAvailabilityNotRegistered() {
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager).registerAvailabilityCallback(any(CameraManager.AvailabilityCallback.class), eq(null));
    }

    @Test
    public void AvailabilityCallback_CameraAvailable_CorrectTarget_NotReady() {
        /*Arrange*/
        ArgumentCaptor<CameraManager.AvailabilityCallback> captor = ArgumentCaptor.forClass(CameraManager.AvailabilityCallback.class);
        String cameraId = "1";
        when(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)).thenReturn(CameraCharacteristics.LENS_FACING_BACK);
        model.getResourcesManager().setAllResourcesUnavailable();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager).registerAvailabilityCallback(captor.capture(), any());
        captor.getValue().onCameraAvailable(cameraId);
        assertEquals(cameraId, model.getResourcesManager().getCameraId());
        verify(bus).post(any(CameraCaptureModel.OnCameraTargetAvailable.class));
    }

    @Test
    public void AvailabilityCallback_CameraAvailable_IncorrectTarget_NotReady() throws CameraAccessException {
        /*Arrange*/
        ArgumentCaptor<CameraManager.AvailabilityCallback> captor = ArgumentCaptor.forClass(CameraManager.AvailabilityCallback.class);
        String cameraId = "1";
        when(cameraUtils.getCameraLensFacing(eq(cameraManager), anyString())).thenReturn(CameraCharacteristics.LENS_FACING_FRONT);
        model.getResourcesManager().setAllResourcesUnavailable();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager).registerAvailabilityCallback(captor.capture(), any());
        captor.getValue().onCameraAvailable(cameraId);
        assertNotSame(cameraId, model.getResourcesManager().getCameraId());
        verify(bus, never()).post(any(CameraCaptureModel.OnCameraTargetAvailable.class));
    }

    @Test
    public void AvailabilityCallback_CameraAvailable_CorrectTarget_Ready() {
        /*Arrange*/
        ArgumentCaptor<CameraManager.AvailabilityCallback> captor = ArgumentCaptor.forClass(CameraManager.AvailabilityCallback.class);
        String cameraId = "1";
        when(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING)).thenReturn(CameraCharacteristics.LENS_FACING_FRONT);
        model.getResourcesManager().setCameraResourceReady(cameraId);
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager).registerAvailabilityCallback(captor.capture(), any());
        captor.getValue().onCameraAvailable(cameraId);
        verify(bus, never()).post(any(CameraCaptureModel.OnCameraTargetAvailable.class));
    }

    /*Feature removed for now*/
    @Test
    @Ignore
    public void openCamera_UnlockRequest_Send() {
        /*Arrange*/
        model.getResourcesManager().setAllResourcesUnavailable();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraUtils).sendCameraUnlockRequest();
    }

    @Test
    public void openCamera_UnlockRequest_NotSend() {
        /*Arrange*/
        String cameraId = "1";
        model.getResourcesManager().setCameraResourceReady(cameraId);
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraUtils, never()).sendCameraUnlockRequest();
    }

    @Test
    public void openCamera_ResourcesNotReady() throws CameraAccessException {
        /*Arrange*/
        model.getResourcesManager().setAllResourcesUnavailable();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager, never()).openCamera(anyString(), any(), any());
    }

    @Test
    public void openCamera_CameraAccessException() throws CameraAccessException {
        /*Arrange*/
        String cameraId = "1";
        model.getResourcesManager().setCameraResourceReady(cameraId);
        model.getResourcesManager().setSurfaceTextureReady();
        doThrow(CameraAccessException.class).when(cameraManager).openCamera(eq(cameraId), any(CameraDevice.StateCallback.class), any());
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(bus).post(any(CameraCaptureModel.OnCameraError.class));
    }

    @Test
    @Ignore
    public void openCamera_CameraOutputEventSent() throws CameraAccessException {
        /*Arrange*/
        String cameraId = "1";
        model.getResourcesManager().setCameraResourceReady(cameraId);
        model.getResourcesManager().setSurfaceTextureReady();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        verify(cameraManager).openCamera(eq(cameraId), any(CameraDevice.StateCallback.class), any());
        verify(bus).post(any(CameraCaptureModel.CameraOutputSetEvent.class));
    }

    @Test
    public void onImageAvailableListener() {
        /*Arrange*/
        String cameraId = "1";
        model.getResourcesManager().setCameraResourceReady(cameraId);
        model.getResourcesManager().setSurfaceTextureReady();
        /*Act*/
        model.openCamera(textureView);
        /*Assert*/
        ArgumentCaptor<ImageReader.OnImageAvailableListener> captor = ArgumentCaptor.forClass(ImageReader.OnImageAvailableListener.class);
        /*TODO -- future*/
    }

    @Test
    public void stopCamera(){
        /*Arrange*/

        /*Act*/
        model.stopCamera(true);
        /*Assert*/
        verify(model.getCameraManager()).unregisterAvailabilityCallback(any(CameraManager.AvailabilityCallback.class));
        assertFalse(model.getResourcesManager().allResourcesReady());
    }

    @Test
    public void sendCameraUnlockRequest(){
        /*Act*/
        model.sendCameraUnlockRequest();
        /*Assert*/
        verify(cameraUtils).sendCameraUnlockRequest();
        assertTrue(model.isStopCameraBroadcastSent());
    }

    @Test
    public void sendCameraLockRequest() {
        /*Act*/
        model.sendCameraLockRequest();
        /*Assert*/
        verify(cameraUtils).sendCameraLockRequest();
    }

    @Test
    public void getRatio() {

        //RATIO_16_9
        com.facebook.devices.utils.Size resolution = new com.facebook.devices.utils.Size(1280, 720);
        assertEquals(16, model.getRatio(resolution).getWidth());
        assertEquals(9, model.getRatio(resolution).getHeight());

        //RATIO_4_3
        resolution = new com.facebook.devices.utils.Size(1600, 1200);
        assertEquals(4, model.getRatio(resolution).getWidth());
        assertEquals(3, model.getRatio(resolution).getHeight());

        //RATIO_3_2
        resolution = new com.facebook.devices.utils.Size(1080, 720);
        assertEquals(3, model.getRatio(resolution).getWidth());
        assertEquals(2, model.getRatio(resolution).getHeight());

        //RATIO_21_9
        resolution = new com.facebook.devices.utils.Size(2100, 900);
        assertEquals(21, model.getRatio(resolution).getWidth());
        assertEquals(9, model.getRatio(resolution).getHeight());

        //RATIO_DEFAULT
        resolution = new com.facebook.devices.utils.Size(1, 1);
        assertEquals(16, model.getRatio(resolution).getWidth());
        assertEquals(9, model.getRatio(resolution).getHeight());
    }

    @Test
    public void isCameraAvailabilityRegisteredTest() {
        //Act
        model.stopCamera(true);
        //Assert
        assertFalse(model.isCameraAvailabilityRegistered());
        assertNull(model.getResourcesManager().getCameraId());
        assertFalse(model.getResourcesManager().isCameraInControl());
    }
}
