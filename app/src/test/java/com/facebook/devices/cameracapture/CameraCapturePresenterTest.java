package com.facebook.devices.cameracapture;

import android.graphics.Bitmap;
import android.os.Bundle;

import com.facebook.devices.mvp.model.CameraCaptureModel;
import com.facebook.devices.mvp.presenter.CameraCapturePresenter;
import com.facebook.devices.mvp.view.CameraCaptureView;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.Size;
import com.facebook.devices.utils.customcomponents.CancellableHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CameraCapturePresenterTest {

    @Mock
    private CameraCaptureView view;
    @Mock
    private CameraCaptureModel model;

    private CameraCapturePresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new CameraCapturePresenter(model, view);
    }

    @Test
    public void onStart_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onStart_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view, never()).hideCameraAccessSpinner();
    }

    @Test
    public void onStop() {
        /*Arrange*/
        boolean isChangingConfig = true;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onStopFalseTest() {
        /*Arrange*/
        boolean isChangingConfig = false;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onDestroy() {
        /*Arrange*/
        CameraCaptureModel.ResourcesManager resManager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(resManager);
        /*Act*/
        presenter.onDestroy();
        /*Assert*/
        verify(resManager).setSurfaceTextureUnavailable();
    }

    @Test
    public void onViewResumed_ImageCaptured() {
        /*Arrange*/
        CameraCaptureModel.ImageProcessorAsyncTask imageProcessorAsyncTask = mock(CameraCaptureModel.ImageProcessorAsyncTask.class);
        when(model.wasImageTaken()).thenReturn(true);
        when(model.getImageProcessorAsyncTask(any())).thenReturn(imageProcessorAsyncTask);
        Bitmap bitmap = mock(Bitmap.class);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        ArgumentCaptor<CameraCaptureModel.ImageProcessorAsyncTask.ImageProcessorListener> captor = ArgumentCaptor.forClass(CameraCaptureModel.ImageProcessorAsyncTask.ImageProcessorListener.class);
        verify(model).getImageProcessorAsyncTask(captor.capture());
        verify(imageProcessorAsyncTask).execute(model.getPhotoAbsoluteFilePath());
        captor.getValue().onPostExecute(bitmap);
        verify(view).setStatePhotoTaken(bitmap);
    }

    @Test
    public void onViewResumed_PermissionsNeeded() {
        /*Arrange*/
        when(model.wasImageTaken()).thenReturn(false);
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).getImageProcessorAsyncTask(any());
        verify(model).requestPermissions();
    }

    @Test
    public void onViewResumed_OpenPreview() {
        /*Arrange*/
        when(model.wasImageTaken()).thenReturn(false);
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).getImageProcessorAsyncTask(any());
        verify(model, never()).requestPermissions();
        verify(view).showInitDialog();
        verify(view).showFloatingButtonTutorial();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onViewPaused_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewPaused();
        /*Assert*/
        verify(model).pauseCamera();
    }

    @Test
    public void onViewPaused_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).pauseCamera();
    }

    @Test
    public void onSaveInstanceState_CameraIDReceived() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        when(model.isStopCameraBroadcastSent()).thenReturn(true);
        when(model.wasImageTaken()).thenReturn(false);
        String cameraId = "1";
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(manager.getCameraId()).thenReturn(cameraId);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle).putString(eq(CameraCaptureModel.CAMERA_ID_RECEIVED), eq(cameraId));
        verify(bundle, never()).putString(eq(CameraCaptureModel.IMAGE_FILE_PATH), anyString());
    }

    @Test
    public void onSaveInstanceState_ImageCaptured() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        when(model.isStopCameraBroadcastSent()).thenReturn(false);
        when(model.wasImageTaken()).thenReturn(true);
        String absoluteFilePath = "SomePath";
        when(model.getPhotoAbsoluteFilePath()).thenReturn(absoluteFilePath);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle, never()).putString(eq(CameraCaptureModel.CAMERA_ID_RECEIVED), any());
        verify(bundle).putString(eq(CameraCaptureModel.IMAGE_FILE_PATH), eq(absoluteFilePath));
    }

    @Test
    public void onRestoreInstantState_CameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        String cameraId = "1";
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraCaptureModel.CAMERA_ID_RECEIVED)).thenReturn(true);
        when(savedState.containsKey(CameraCaptureModel.IMAGE_FILE_PATH)).thenReturn(false);
        when(savedState.getString(CameraCaptureModel.CAMERA_ID_RECEIVED)).thenReturn(cameraId);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager).setCameraResourceReady(cameraId);
    }

    @Test
    public void onRestoreInstantState_NotCameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraCaptureModel.CAMERA_ID_RECEIVED)).thenReturn(false);
        when(savedState.containsKey(CameraCaptureModel.IMAGE_FILE_PATH)).thenReturn(false);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager, never()).setCameraResourceReady(any());
    }

    @Test
    public void onRestoreInstantStateFilePathTrueTest() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraCaptureModel.CAMERA_ID_RECEIVED)).thenReturn(true);
        when(savedState.containsKey(CameraCaptureModel.IMAGE_FILE_PATH)).thenReturn(true);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager, never()).setCameraResourceReady(any());
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_NoRequest() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(false);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model).fail(anyString());
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_Request() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(true);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model, never()).fail(anyString());
        verify(view, never()).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionGranted() {
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionGrantedEvent());
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onSurfaceTextureAvailable_PermissionGranted_ImgNotTaken() {
        /*Arrange*/
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.wasImageTaken()).thenReturn(false);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onSurfaceTextureAvailable_NotPermission() {
        /*Arrange*/
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(false);
        when(model.wasImageTaken()).thenReturn(false);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view, never()).showCameraAccessSpinner();
        verify(model, never()).openCamera(view.getTextureView());
    }

    @Test
    public void onSurfaceTextureAvailable_ImgAlreadyTaken() {
        /*Arrange*/
        CameraCaptureModel.ResourcesManager manager = mock(CameraCaptureModel.ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(true);
        when(model.wasImageTaken()).thenReturn(true);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view, never()).showCameraAccessSpinner();
        verify(model, never()).openCamera(view.getTextureView());
    }

    @Test
    public void onCameraTriggerClicked(){
        /*Act*/
        presenter.onCameraTriggerClicked(null);
        /*Assert*/
        verify(model).captureImage();
    }

    @Test
    public void onSuccessButtonClicked(){
        /*Act*/
        presenter.onSuccessButtonClicked(null);
        /*Assert*/
        passCalled();
    }

    @Test
    public void onFailureButtonClicked(){
        /*Act*/
        presenter.onFailureButtonClicked(null);
        /*Assert*/
        failCalled();
    }

    @Test
    public void onInitDialogEventClicked(){
        /*Act*/
        presenter.onInitDialogEventClicked(null);
        /*Assert*/
        failCalled();
    }

    @Test
    public void onCameraOutputSetEvent(){
        /*Arrange*/
        Size previewRatio = new Size(100,100);
        /*Act*/
        presenter.onCameraOutputSetEvent(new CameraCaptureModel.CameraOutputSetEvent(previewRatio));
        /*Assert*/
        verify(view).setMirrorEffect(model.isMirrored());
        verify(view).setAspectRatio(eq(previewRatio.getWidth()), eq(previewRatio.getHeight()));
    }

    @Test
    public void onCameraPreviewSetEvent(){
        /*Act*/
        presenter.onCameraPreviewSetEvent(null);
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
        verify(view).setStatePreview();
    }

    @Test
    public void onOpenCameraError(){
        /*Arrange*/
        String message = "msg";
        /*Act*/
        presenter.onOpenCameraError(new CameraCaptureModel.OnCameraError(message));
        /*Assert*/
        failCalled();
    }

    @Test
    public void onCameraTargetAvailable_PictureNotTaken(){
        /*Arrange*/
        when(model.wasImageTaken()).thenReturn(false);
        /*Act*/
        presenter.onCameraTargetAvailable(null);
        /*Assert*/
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onCameraTargetAvailable_PictureTaken(){
        /*Arrange*/
        when(model.wasImageTaken()).thenReturn(true);
        /*Act*/
        presenter.onCameraTargetAvailable(null);
        /*Assert*/
        verify(model, never()).openCamera(view.getTextureView());
    }

    @Test
    public void onImageAvailable(){
        /*Arrange*/
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        long time = 100;
        when(model.getWaitTimeMillis()).thenReturn(time);
        Bitmap imgTaken = mock(Bitmap.class);
        /*Act*/
        presenter.onImageAvailableEvent(new CameraCaptureModel.OnImageAvailableEvent(imgTaken));
        /*Assert*/
        verify(view).hideDialog();
        verify(view).hideFloatingButton();
        verify(view).setStatePhotoTaken(eq(imgTaken));
        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(handler).postDelayedCustom(runnableCaptor.capture(), eq(time));
        runnableCaptor.getValue().run();
        verify(view).showResultDialog();
    }

    @Test
    public void onImageAvailable_ImageError(){
        /*Arrange*/
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        Bitmap imgTaken = null;
        /*Act*/
        presenter.onImageAvailableEvent(new CameraCaptureModel.OnImageAvailableEvent(imgTaken));
        /*Assert*/
        failCalled();
        verify(view, never()).hideDialog();
        verify(view, never()).hideFloatingButton();
        verify(view, never()).setStatePhotoTaken(eq(imgTaken));
        verify(handler, never()).postDelayedCustom(view::showResultDialog, model.getWaitTimeMillis());
    }

    /* Pass or Fail Check */
    public void passCalled(){
        verify(model).pass(any());
        verify(view).finishActivity();
    }

    public void passNotCalled(){
        verify(model, never()).pass(any());
        verify(view, never()).finishActivity();
    }

    public void failCalled(){
        verify(model).fail(any());
        verify(view).finishActivity();
    }

    public void failNotCalled(){
        verify(model, never()).fail(any());
        verify(view, never()).finishActivity();
    }


}
