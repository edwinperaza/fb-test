package com.facebook.devices.cameracapture;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.CameraCaptureActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.CameraCaptureView;
import com.facebook.devices.utils.AlertDialogBaseEvent;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.AutoFitTextureView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class CameraCaptureViewTest {

    private CameraCaptureActivity activity;
    private CameraCaptureView view;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private SurfaceTexture surfaceTextureMock;

    /*UI stuffs*/
    private AutoFitTextureView textureView;
    private LinearLayout shutterLayout;
    private RelativeLayout textureContainerLayout;
    private ImageView ivPhotoFinal, ivTrigger;
    private ProgressBar cameraAccessSpinner;
    private BannerView bannerView;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, CameraCaptureActivity.class);
        activity = Robolectric.buildActivity(CameraCaptureActivity.class, intent).create().start().resume().get();

        view = new CameraCaptureView(activity, bus);

        /*Utils*/
        textureView = activity.findViewById(R.id.texture_camera);
        shutterLayout = activity.findViewById(R.id.shutter_layout);
        textureContainerLayout = activity.findViewById(R.id.texture_container_layout);
        ivPhotoFinal = activity.findViewById(R.id.iv_photo_final);
        ivTrigger = activity.findViewById(R.id.iv_trigger);
        cameraAccessSpinner = activity.findViewById(R.id.camera_access_spinner);
        bannerView = ShadowView.getCurrentBanner(view);
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
    }

    @Test
    public void isPortraitTest() {
        //Arrange
        activity.getResources().getConfiguration().orientation = Configuration.ORIENTATION_LANDSCAPE;
        //Assert
        assertFalse(view.isPortrait());
    }

    @Test
    public void isPortraitFalseTest() {
        //Arrange
        activity.getResources().getConfiguration().orientation = Configuration.ORIENTATION_PORTRAIT;
        //Assert
        assertTrue(view.isPortrait());
    }

    @Test
    public void setupTextureView() {
        /*Assert*/
        assertNotNull(textureView.getSurfaceTextureListener());
    }

    @Test
    public void setupTextureView_SurfaceAvailable() {
        /*Arrange*/
        int width = 100;
        int height = 200;
        /*Act*/
        textureView.getSurfaceTextureListener().onSurfaceTextureAvailable(surfaceTextureMock, width, height);
        /*Assert*/
        ArgumentCaptor<CameraCaptureView.OnSurfaceTextureAvailable> captor = ArgumentCaptor.forClass(CameraCaptureView.OnSurfaceTextureAvailable.class);
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().surface, surfaceTextureMock);
        assertEquals(captor.getValue().height, height);
        assertEquals(captor.getValue().width, width);
    }

    @Test
    public void setupTextureView_SurfaceTextureDestroyed() {
        //Act
        assertFalse(textureView.getSurfaceTextureListener().onSurfaceTextureDestroyed(surfaceTextureMock));
    }

    @Test
    public void showTutorial() {
        /*Arrange*/
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.camera_capture_dialog_tutorial_message);
        /*Act*/
        floatingTutorialView.getButton().performClick();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        /*Assert*/
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
    }

    @Test
    public void showTutorial_Positive() {
        /*Act*/
        floatingTutorialView.getButton().performClick();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        /*Assert*/
        ArgumentCaptor<CameraCaptureView.TutorialEvent> captor = ArgumentCaptor.forClass(CameraCaptureView.TutorialEvent.class);
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().optionSelected, AlertDialogBaseEvent.OPTION_POSITIVE);
    }

    @Test
    public void showInitDialog() {
        /*Arrange*/
        String message = activity.getString(R.string.init_dialog_text_camera_capture);
        String positiveText = activity.getString(R.string.fail_base);
        /*Act*/
        view.showInitDialog();
        /*Assert*/
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }


    @Test
    public void showInitDialog_Positive_Action() {
        /*Act*/
        view.showInitDialog();
        bannerView.getBannerOptionOneButton().performClick();
        /*Assert*/
        verify(bus).post(any(CameraCaptureView.InitDialogEvent.class));
    }

    @Test
    public void showFloatingButtonTutorial() {
        /*Act*/
        view.showFloatingButtonTutorial();
        /*Assert*/
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideFloatingButton() {
        /*Act*/
        view.hideFloatingButton();
        /*Assert*/
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void hideDialog() {
        /*Act*/
        view.showInitDialog();
        view.hideDialog();
        /*Assert*/
        assertFalse(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void getTextureView() {
        /*Assert*/
        assertNotNull(view.getTextureView());
    }

    @Test
    public void setAspectRatio() {
        /*Arrange*/
        int aspectWidth = 4;
        int aspectHeight = 3;
        /*Act*/
        view.setAspectRatio(aspectWidth, aspectHeight);
        /*Assert*/
        assertEquals(textureView.getAspectRatio().getHeight(), aspectHeight);
        assertEquals(textureView.getAspectRatio().getWidth(), aspectWidth);
    }

    @Test
    public void isAvailable() {
        /*Assert*/
        assertFalse(view.isAvailable());
    }

    @Test
    public void setStatePreview() {
        /*Act*/
        view.setStatePreview();
        /*Assert*/
        assertEquals(ivTrigger.getVisibility(), View.VISIBLE);
        assertEquals(shutterLayout.getVisibility(), View.VISIBLE);
        assertEquals(textureContainerLayout.getVisibility(), View.VISIBLE);
        assertEquals(textureView.getVisibility(), View.VISIBLE);
    }

    @Test
    public void setStatePhotoTaken() {
        Bitmap bitmap = mock(Bitmap.class);
        /*Act*/
        view.setStatePhotoTaken(bitmap);
        /*Assert*/
        assertEquals(ivTrigger.getVisibility(), View.GONE);
        assertEquals(((BitmapDrawable) ivPhotoFinal.getDrawable()).getBitmap(), bitmap);
        assertEquals(textureContainerLayout.getVisibility(), View.GONE);
        assertEquals(textureView.getVisibility(), View.GONE);
        assertEquals(shutterLayout.getVisibility(), View.GONE);
        assertEquals(ivPhotoFinal.getVisibility(), View.VISIBLE);
    }

    @Test
    public void showResultDialog() {
        /*Arrange*/
        String message = activity.getString(R.string.camera_capture_dialog_result_message);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);
        /*Act*/
        view.showResultDialog();
        /*Assert*/
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showResultDialog_PositiveAction() {
        /*Act*/
        view.showResultDialog();
        bannerView.getBannerOptionOneButton().performClick();
        /*Assert*/
        verify(bus).post(any(CameraCaptureView.SuccessEvent.class));
    }

    @Test
    public void showResultDialog_NegativeAction() {
        /*Act*/
        view.showResultDialog();
        bannerView.getBannerOptionTwoButton().performClick();
        /*Assert*/
        verify(bus).post(any(CameraCaptureView.FailureEvent.class));
    }

    @Test
    public void cameraTriggerClicked() {
        /*Act*/
        view.cameraTriggerClicked();
        /*Assert*/
        verify(bus).post(any(CameraCaptureView.CameraTriggerEvent.class));
    }

    @Test
    public void showCameraAccessSpinner() {
        /*Act*/
        view.showCameraAccessSpinner();
        /*Assert*/
        assertEquals(cameraAccessSpinner.getVisibility(), View.VISIBLE);
    }

    @Test
    public void hideCameraAccessSpinner() {
        /*Act*/
        view.hideCameraAccessSpinner();
        /*Assert*/
        assertEquals(cameraAccessSpinner.getVisibility(), View.GONE);
    }
}
