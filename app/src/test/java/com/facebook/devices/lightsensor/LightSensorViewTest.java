package com.facebook.devices.lightsensor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.LightSensorActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.LightSensorView;
import com.facebook.devices.mvp.view.LightSensorView.OnButtonFailurePressed;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class LightSensorViewTest {

    private LightSensorActivity activity;
    private LightSensorView view;

    @Mock
    private BusProvider.Bus bus;

    /*Bindings*/
    private TextView luxValue;
    private SeekBar seekBarLuxValue;
    private FloatingTutorialView floatingTutorialView;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, LightSensorActivity.class);
        intent.putExtra("args", new String[]{String.valueOf("200.0"), String.valueOf("20.0")});
        activity = Robolectric.buildActivity(LightSensorActivity.class, intent).create().start().resume().get();
        view = new LightSensorView(activity, bus);

        /*Bindings*/
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        luxValue = activity.findViewById(R.id.tv_sensor_value);
        seekBarLuxValue = activity.findViewById(R.id.light_sensor_seek_bar);
    }

    @Test
    public void showLuxValueTest() {
        //Arrange
        float value = 100f;

        //Act
        view.showLuxValue(value);

        //Assert
        assertEquals(seekBarLuxValue.getProgress(), (int) value);
        assertEquals(Float.valueOf(luxValue.getText().toString()), value);
    }

    @Test
    public void showInitDialogTest() {
        //Arrange
        String message = activity.getString(R.string.sensor_init_dialog_title);
        String positiveText = activity.getString(R.string.fail_base);

        //Act
        view.showInitDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void showInitDialogOptionOneTest() {
        //Act
        view.showInitDialog();
        //Assert
        ShadowView.getCurrentBanner(view).getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showTutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.sensor_tutorial_dialog_message);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        TextView tutorialTitle = dialog.findViewById(R.id.tv_material_dialog_title);
        TextView tutorialMessage = dialog.findViewById(R.id.tv_material_dialog_message);

        assertEquals(tutorialTitle.getText(), title);
        assertEquals(tutorialMessage.getText(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showTutorialFloatingButtonTest() {
        view.showTutorialButton();
        assertTrue(floatingTutorialView.isButtonVisible());
    }
}
