package com.facebook.devices.lightsensor;

import com.facebook.devices.activities.LightSensorActivity;
import com.facebook.devices.mvp.model.LightSensorModel;
import com.facebook.devices.mvp.model.LightSensorModel.LightSensorCoverEvent;
import com.facebook.devices.mvp.model.LightSensorModel.LightSensorUncoverEvent;
import com.facebook.devices.mvp.presenter.LightSensorPresenter;
import com.facebook.devices.mvp.view.LightSensorView;
import com.facebook.devices.mvp.view.LightSensorView.OnButtonFailurePressed;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LightSensorPresenterTest {

    private LightSensorPresenter presenter;

    @Mock
    private LightSensorModel model;

    @Mock
    private LightSensorView view;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new LightSensorPresenter(model, view);
    }

    @Test
    public void onLightSensorUncoverTest() {
        //Arrange
        when(model.wasMeasurementSuccess()).thenReturn(true);
        LightSensorUncoverEvent event = new LightSensorUncoverEvent();
        //Act
        presenter.onLightSensorUncover(event);
        //Assert
        verify(view).finishActivity();
        verify(model).pass();
    }

    @Test
    public void onLightSensorCoverTest() {
        //Arrange
        when(model.wasMeasurementSuccess()).thenReturn(true);
        LightSensorCoverEvent event = new LightSensorCoverEvent();
        //Act
        presenter.onLightSensorCover(event);
        //Assert
        verify(view).finishActivity();
        verify(model).pass();
    }

    @Test
    public void onLightSensorEventTest() {
        //Arrange
        float value = 20;
        LightSensorModel.LightSensorEvent event = new LightSensorModel.LightSensorEvent(value);
        //Act
        presenter.onLightSensorEvent(event);
        //Assert
        verify(view).showLuxValue(event.value);
    }

    @Test
    public void onButtonFailurePressedTest() {
        //Arrange
        OnButtonFailurePressed event = new OnButtonFailurePressed();
        //Act
        presenter.onButtonFailurePressed(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onPauseTest() {
        //Act
        presenter.onPause();
        //Assert
        verify(model).stopSensing();
    }

    @Test
    public void onServiceDisconnected() {
        //Act
        presenter.onServiceDisconnected(null);
        //Assert
        verify(model).stopSensing();
        verify(model).removeService();
    }

    @Test
    public void onServiceConnected() {
        /*Arrange*/
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        /*Act*/
        presenter.onServiceConnected(null, binder);
        /*Assert*/
        verify(model).startSensing();
        verify(model).setService(binder.getService());
    }
}
