package com.facebook.devices.lightsensor;

import android.hardware.SensorManager;

import com.facebook.devices.mvp.model.LightSensorModel;
import com.facebook.devices.mvp.model.LightSensorModel.LightSensorEvent;
import com.facebook.devices.bus.BusProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;

public class LightSensorModelTest {

    private LightSensorModel model;

    @Mock
    private SensorManager sensorManager;

    @Mock
    private BusProvider.Bus bus;

    float minLux = 20f;
    float maxLux = 200f;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = new LightSensorModel(sensorManager, bus, minLux, maxLux);
    }

    @Test
    public void onUpdateDecibelEventTest() {
        //Arrange
        float value = 2;
        //Act
        LightSensorEvent event = new LightSensorEvent(value);
        //Assert
        assertEquals(event.value, value);
    }
}
