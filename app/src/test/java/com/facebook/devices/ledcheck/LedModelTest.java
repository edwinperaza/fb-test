package com.facebook.devices.ledcheck;


import android.app.Activity;
import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.LedActivity;
import com.facebook.devices.mvp.model.LedModel;
import com.facebook.devices.bus.BusProvider;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.model.LedModel.STATE_BLUE;
import static com.facebook.devices.mvp.model.LedModel.STATE_GREEN;
import static com.facebook.devices.mvp.model.LedModel.STATE_RED;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class LedModelTest {

    private LedModel model;
    private Activity activity;
    private BusProvider.Bus bus;
    private String ledLocation = "cam";

    @Captor
    private ArgumentCaptor<LedModel.LedCallback> ledCallbackCaptor;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());

        Intent intent = new Intent(RuntimeEnvironment.application, LedActivity.class);
        intent.putExtra("args", new String[]{"cam"});
        activity = Robolectric.buildActivity(LedActivity.class, intent).create().start().resume().get();
        bus = BusProvider.getInstance();

        model = new LedModel(activity, ledLocation);
    }

    @Test
    public void prettyLedLocationCam() {
        //Arrange
        model.setLedLocation(ledLocation);
        //Assert
        assertEquals(model.getPrettyLEDLocation(), activity.getString(R.string.led_camera_pretty));
    }

    @Test
    public void prettyLedLocationSideOne() {
        //Arrange
        model.setLedLocation(LedModel.LED_SIDE_1);
        //Assert
        assertEquals(model.getPrettyLEDLocation(), activity.getString(R.string.led_side_1_pretty));
    }

    @Test
    public void currentLead() {
        //Assert
        assertEquals(model.getCurrentLed(), ledLocation);
    }

    @Test
    public void currentState() {
        //Arrange
        String currentState = STATE_RED;
        //Act
        model.resetLedState();
        //Assert
        assertEquals(model.getCurrentState(), currentState);
    }

    @Test
    public void resetState() {
        //Act
        model.resetLedState();
        //Assert
        assertEquals(model.getCurrentState(), STATE_RED);
    }

    @Test
    public void jumNextColor() {
        //Arrange
        model.resetLedState();
        //Assert
        /*Step 1*/
        assertTrue(model.jumpNextColor());
        assertEquals(model.getCurrentState(), STATE_GREEN);
        /*Step 2*/
        assertTrue(model.jumpNextColor());
        assertEquals(model.getCurrentState(), STATE_BLUE);
        /*Step 3*/
        assertFalse(model.jumpNextColor());
    }

    @Test
    @Ignore
    public void startThreadBehavior() {
        //Arrange
        String state = model.getCurrentState();
        //Act
        model.startLEDBehavior(state, ledCallbackCaptor.capture());
        Robolectric.flushForegroundThreadScheduler();
        //Assert
    }

}
