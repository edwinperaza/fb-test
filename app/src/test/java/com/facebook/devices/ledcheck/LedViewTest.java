package com.facebook.devices.ledcheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.LedActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.LedView;
import com.facebook.devices.mvp.view.LedView.OnDialogOptionSelected;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class LedViewTest {

    private LedActivity activity;
    private LedView view;

    @Mock
    private BusProvider.Bus bus;

    /* Bindings */
    private TextView tvUserMessage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, LedActivity.class);
        intent.putExtra("args", new String[]{"cam"});
        activity = Robolectric.buildActivity(LedActivity.class, intent).create().start().resume().get();

        view = new LedView(activity, bus);

        /* Bindings */
        tvUserMessage = activity.findViewById(R.id.tv_user_message);
    }

    @Test
    public void changeLedLocationMessage() {
        //Arrange
        String locationLed = "camera";
        //Act
        view.changeLedLocationMessage(locationLed);
        //
        assertEquals(tvUserMessage.getText(), activity.getString(R.string.led_location_message, locationLed));
    }

    @Test
    public void showQuestionDialog() {
        //Arrange
        String locationLed = "camera";
        String title = activity.getString(R.string.led_dialog_title, locationLed);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);
        String neutralText = activity.getString(R.string.try_again_base);
        //Act
        view.showQuestionDialog(locationLed);
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowDialog = new ShadowMaterialDialog(Shadows.shadowOf(alertDialog));
        assertEquals(shadowDialog.getMessage(), title);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).getText(), negativeText);
        assertEquals(alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).getText(), neutralText);
    }

    @Test
    public void showQuestionDialogOptionOneTest() {
        //Arrange
        String locationLed = "camera";
        ArgumentCaptor<OnDialogOptionSelected> argumentCaptor = ArgumentCaptor.forClass(OnDialogOptionSelected.class);
        //Act
        view.showQuestionDialog(locationLed);
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue().getOptionSelected(), OnDialogOptionSelected.OPTION_POSITIVE);
    }

    @Test
    public void showQuestionDialogOptionTwoTest() {
        //Arrange
        String locationLed = "camera";
        ArgumentCaptor<OnDialogOptionSelected> argumentCaptor = ArgumentCaptor.forClass(OnDialogOptionSelected.class);
        //Act
        view.showQuestionDialog(locationLed);
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue().getOptionSelected(), OnDialogOptionSelected.OPTION_NEGATIVE);
    }

    @Test
    public void showQuestionDialogOptionThreeTest() {
        //Arrange
        String locationLed = "camera";
        ArgumentCaptor<OnDialogOptionSelected> argumentCaptor = ArgumentCaptor.forClass(OnDialogOptionSelected.class);
        //Act
        view.showQuestionDialog(locationLed);
        //Assert
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL).performClick();
        verify(bus).post(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue().getOptionSelected(), OnDialogOptionSelected.OPTION_NEUTRAL);
    }
    @Test
    public void dialogOptionSelectedEvent() {
        /*Arrange*/
        OnDialogOptionSelected event = new OnDialogOptionSelected(OnDialogOptionSelected.OPTION_POSITIVE);
        /*Assert*/
        assertEquals(event.getOptionSelected(), OnDialogOptionSelected.OPTION_POSITIVE);
    }
}