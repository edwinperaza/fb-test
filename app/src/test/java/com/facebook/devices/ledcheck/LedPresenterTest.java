package com.facebook.devices.ledcheck;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.mvp.model.LedModel;
import com.facebook.devices.mvp.presenter.LedPresenter;
import com.facebook.devices.mvp.view.LedView;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class LedPresenterTest {

    private LedPresenter presenter;

    @Mock
    private EventBus bus;

    @Mock
    private LedView view;
    @Mock
    private LedModel model;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new LedPresenter(model, view);
    }

    @Test
    public void executeLEDBehaviorFalse() {
        //Arrange
        boolean shouldContinue = false;
        //Act
        presenter.executeLEDBehavior(shouldContinue);
        //Assert
        verify(view).showQuestionDialog(model.getPrettyLEDLocation());
    }

    @Test
    public void onDialogOptionSelected_Positive() {
        /*Arrange*/
        int optionSelected = LedView.OnDialogOptionSelected.OPTION_POSITIVE;
        /*Act*/
        presenter.onDialogOptionSelected(new LedView.OnDialogOptionSelected(optionSelected));
        /*Assert*/
        verify(model).pass();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogOptionSelected_Negative() {
        /*Arrange*/
        int optionSelected = LedView.OnDialogOptionSelected.OPTION_NEGATIVE;
        /*Act*/
        presenter.onDialogOptionSelected(new LedView.OnDialogOptionSelected(optionSelected));
        /*Assert*/
        verify(model).fail();
        verify(view).finishActivity();
    }

    @Test
    public void onDialogOptionSelected_Neutral() {
        /*Arrange*/
        int optionSelected = LedView.OnDialogOptionSelected.OPTION_NEUTRAL;
        /*Act*/
        presenter.onDialogOptionSelected(new LedView.OnDialogOptionSelected(optionSelected));
        /*Assert*/
        verify(model).resetLedState();
        verify(model, times(2)).startLEDBehavior(model.getCurrentState(), eq(any(LedModel.LedCallback.class)));
        verify(view, times(2)).changeLedLocationMessage(model.getPrettyLEDLocation());
    }


}
