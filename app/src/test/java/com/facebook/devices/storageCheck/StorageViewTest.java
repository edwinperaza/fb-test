package com.facebook.devices.storageCheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.StorageActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.StorageView;
import com.facebook.devices.mvp.view.StorageView.OnButtonFailurePressed;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class StorageViewTest {

    private StorageActivity activity;
    private StorageView view;

    @Mock
    private BusProvider.Bus bus;

    private TextView label;
    private ProgressBar progressBar;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, StorageActivity.class);
        intent.putExtra("args", "0xff");
        activity = Robolectric.buildActivity(StorageActivity.class, intent).create().start().resume().get();

        view = new StorageView(activity, bus);

        //Bindings
        label = activity.findViewById(R.id.tv_label);
        progressBar = activity.findViewById(R.id.progressBar);
    }

    @Test
    public void init() {
        assertNotNull(view.getUiHandler());
    }

    @Test
    public void updateProgressBarTest() {
        //Arrange
        int value = 10;
        //Act
        view.updateProgressBar(value);
        //Assert
        assertEquals(progressBar.getProgress(), value);
    }

    @Test
    public void updatePercentageTest() {
        //Arrange
        TextView percentage = activity.findViewById(R.id.tv_percentage);
        String percentageValue = "25.0%";
        double value = 25;
        //Act
        view.updatePercentage(value);
        //Assert
        assertEquals(percentage.getText().toString(), percentageValue);
    }

    @Test
    public void showValidationTest() {
        //Arrange
        String value = activity.getString(R.string.storage_validation);
        //Act
        view.showValidation();
        //Assert
        assertEquals(label.getText().toString(), value);
    }

    @Test
    public void patternFailTest() {
        //Arrange
        String value = activity.getString(R.string.storage_fail);
        //Act
        view.patternFail();
        //Assert
        assertEquals(label.getText().toString(), value);
    }

    @Test
    public void setupViewTest() {
        //Arrange
        String value = activity.getString(R.string.allocate_storage);
        //Act
        view.setupView();
        //Assert
        assertEquals(label.getText().toString(), value);
    }

    @Test
    public void setMaxTest() {
        //Arrange
        long value = 10000;
        //Act
        view.setMax(value);
        //Assert
        assertEquals(progressBar.getMax(), value);
    }

    @Test
    public void showStartTest() {
        //Arrange
        String value = activity.getString(R.string.start_storage_test);
        //Act
        view.showStart();
        //Assert
        assertEquals(label.getText().toString(), value);
    }

    @Test
    public void setMinTest() {
        //Arrange
        int value = 0;
        //Act
        view.setMin(value);
        //Assert
        assertEquals(progressBar.getMin(), value);
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.storage_tutorial_message);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(dialog));
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showValidationDialogTest() {
        //Arrange
        String message = activity.getString(R.string.storage_validation_question);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showValidationDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);

        bannerView.getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showWritingDialogTest() {
        //Arrange
        String message = activity.getString(R.string.storage_writing_question);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showWritingDialog();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);

        bannerView.getBannerOptionOneButton().callOnClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void ivHourOneTest() {
        //Arrange
        int icon = R.drawable.num_cero;
        ImageView ivHourOne = activity.findViewById(R.id.iv_hour_digit_one);
        ImageView ivHourTwo = activity.findViewById(R.id.iv_hour_digit_two);
        ImageView ivMinOne = activity.findViewById(R.id.iv_minutes_digit_one);
        ImageView ivMinTwo = activity.findViewById(R.id.iv_minutes_digit_two);
        ImageView ivSecOne = activity.findViewById(R.id.iv_seconds_digit_one);
        ImageView ivSecTwo = activity.findViewById(R.id.iv_seconds_digit_two);

        //Act
        view.showRemainingTime(icon, icon, icon, icon, icon, icon);

        //Assert
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivHourOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivHourTwo.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivMinOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivMinTwo.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivSecOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivSecTwo.getDrawable()).getCreatedFromResId())));
    }


}