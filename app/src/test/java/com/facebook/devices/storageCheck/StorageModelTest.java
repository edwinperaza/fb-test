package com.facebook.devices.storageCheck;

import android.os.StatFs;

import com.facebook.devices.mvp.model.StorageModel;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class StorageModelTest {

    private StorageModel model;
    private byte pattern = (byte) 0xff;
    private byte percentage = 10;

    @Mock
    private StatFs statFs;

    @Mock
    private RuntimePermissionHandler permissionHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        model = new StorageModel(pattern, percentage, permissionHandler, statFs);
    }
}
