package com.facebook.devices.storageCheck;


import android.os.Handler;

import com.facebook.devices.mvp.model.StorageModel;
import com.facebook.devices.mvp.model.StorageModel.StorageProgress;
import com.facebook.devices.mvp.presenter.StoragePresenter;
import com.facebook.devices.mvp.view.StorageView;
import com.facebook.devices.permission.RuntimePermissionHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StoragePresenterTest {

    private StoragePresenter presenter;

    @Mock
    private StorageView view;

    @Mock
    private StorageModel model;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(view.getUiHandler()).thenReturn(mockHandler);
        presenter = new StoragePresenter(model, view);
    }

    @Test
    public void onResumeIntroTest() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(model).runStorageTest();
    }

    @Test
    public void onResume_RequestPermissionsTest() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onResume();
        /*Assert*/
        verify(model).requestPermissions();
    }

    @Test
    public void initCallbackTest() {
        //Arrange
        long limit = 10;
        String message = "";
        float progress = 25;
        int position = 15;
        ArgumentCaptor<StorageProgress> callbackCaptor = ArgumentCaptor.forClass(StorageProgress.class);
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        //Assert
        verify(model).setStorageProgress(callbackCaptor.capture());

        callbackCaptor.getValue().onSetup();
        verify(view.getUiHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).setupView();

        callbackCaptor.getValue().onStart(limit);
        verify(view.getUiHandler(), times(2)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).setMin(0);
        verify(view).setMax(limit);
        verify(view).showStart();
        verify(view).showWritingDialog();

        callbackCaptor.getValue().onValidation();
        verify(view.getUiHandler(), times(3)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).showValidation();
        verify(view).showValidationDialog();

        callbackCaptor.getValue().onSucceed();
        verify(model).pass();
        verify(view).finishActivity();

        callbackCaptor.getValue().onFail(message);
        verify(model).fail(message);
        verify(view, times(2)).finishActivity();

        callbackCaptor.getValue().onProgress(progress, position);
        verify(view.getUiHandler(), times(4)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).updatePercentage(progress);
        verify(view).updateProgressBar(position);

        callbackCaptor.getValue().onPatternFail();
        verify(view.getUiHandler(), times(5)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).patternFail();
        verify(model).fail();
        verify(view, times(3)).finishActivity();

        callbackCaptor.getValue().onRemainingTime(1,2,3,4,5,6);
        verify(view.getUiHandler(), times(6)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).showRemainingTime(1,2,3,4,5,6);
    }

    @Test
    public void onButtonFailurePressedTest(){
        //Act
        presenter.onButtonFailurePressed(null);
        //Assert
        verify(model).cancel();
    }

    @Test
    public void onStopTest() {
        //Act
        presenter.onStop();
        //Assert
        verify(model).stop();
    }

    @Test
    public void onRequestPermissionResultTest() {
        //Arrange
        when(model.shouldRequestPermissions()).thenReturn(false);
        RuntimePermissionHandler.PermissionDeniedEvent event = new RuntimePermissionHandler.PermissionDeniedEvent();
        //Act
        presenter.onRequestPermissionResult(event);
        //Assert
        verify(model).fail("User denied permissions");
        verify(view).finishActivity();
    }
}
