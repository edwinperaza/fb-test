package com.facebook.devices.cameraprivacycheck;

import android.os.Bundle;

import com.facebook.devices.mvp.model.CameraPrivacyModel;
import com.facebook.devices.mvp.model.CameraPrivacyModel.ResourcesManager;
import com.facebook.devices.mvp.presenter.CameraPrivacyPresenter;
import com.facebook.devices.mvp.view.CameraPrivacyView;
import com.facebook.devices.mvp.view.CameraPrivacyView.DialogTurnOffPrivacyEvent;
import com.facebook.devices.permission.RuntimePermissionHandler;
import com.facebook.devices.utils.Size;
import com.facebook.devices.utils.customcomponents.CancellableHandler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CameraPrivacyPresenterTest {

    @Mock
    private CameraPrivacyView view;
    @Mock
    private CameraPrivacyModel model;

    private CameraPrivacyPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        presenter = new CameraPrivacyPresenter(model, view);
    }

    @Test
    public void onStart_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onStart_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onStart();
        /*Assert*/
        verify(view, never()).hideCameraAccessSpinner();
    }

    @Test
    public void onStopTest() {
        /*Arrange*/
        boolean isChangingConfig = true;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onStopFalseTest() {
        /*Arrange*/
        boolean isChangingConfig = false;
        CancellableHandler handler = mock(CancellableHandler.class);
        when(model.getUiHandler()).thenReturn(handler);
        /*Act*/
        presenter.onStop(isChangingConfig);
        /*Assert*/
        verify(handler).removeCallbacks();
        verify(model).stopCamera(!isChangingConfig);
    }

    @Test
    public void onDestroy() {
        /*Arrange*/
        ResourcesManager resManager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(resManager);
        /*Act*/
        presenter.onDestroy();
        /*Assert*/
        verify(resManager).setSurfaceTextureUnavailable();
    }

    @Test
    public void onViewResumed_PermissionsNeeded() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(view, never()).showDialogOne();
        verify(model).requestPermissions();
    }

    @Test
    public void onViewResumed_OpenPreview() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).requestPermissions();
        verify(view).showDialogOne();
        verify(view).showFloatingTutorialButton();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onViewPaused_PermissionGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onViewPaused();
        /*Assert*/
        verify(model).pauseCamera();
    }

    @Test
    public void onViewPaused_PermissionNotGranted() {
        /*Arrange*/
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onViewResumed();
        /*Assert*/
        verify(model, never()).pauseCamera();
    }

    @Test
    public void onSaveInstanceState_CameraIDReceived() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        when(model.isStopCameraBroadcastSent()).thenReturn(true);
        String cameraId = "1";
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(manager.getCameraId()).thenReturn(cameraId);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle).putString(eq(CameraPrivacyModel.CAMERA_ID_RECEIVED), eq(cameraId));
    }

    @Test
    public void onSaveInstanceStateCameraIDReceived() {
        /*Arrange*/
        Bundle bundle = mock(Bundle.class);
        when(model.isStopCameraBroadcastSent()).thenReturn(false);
        String cameraId = "1";
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(manager.getCameraId()).thenReturn(cameraId);
        when(model.isPrivacyWorking()).thenReturn(true);
        /*Act*/
        presenter.onSaveInstanceState(bundle);
        /*Assert*/
        verify(bundle, never()).putString(eq(CameraPrivacyModel.CAMERA_ID_RECEIVED), eq(cameraId));
        verify(bundle).putBoolean(eq(CameraPrivacyModel.PRIVACY_BUTTON_WORKING), eq(true));
    }

    @Test
    public void onRestoreInstantState_CameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        String cameraId = "1";
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraPrivacyModel.CAMERA_ID_RECEIVED)).thenReturn(true);
        when(savedState.getString(CameraPrivacyModel.CAMERA_ID_RECEIVED)).thenReturn(cameraId);
        when(savedState.containsKey(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(true);
        when(savedState.getBoolean(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(true);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager).setCameraResourceReady(cameraId);
        verify(model).privacyButtonWorking();
        verify(model, never()).privacyButtonFailed();
    }

    @Test
    public void onRestoreInstantStateCameraAvailablePrivacyButtonWorkingFalseTest() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        String cameraId = "1";
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraPrivacyModel.CAMERA_ID_RECEIVED)).thenReturn(true);
        when(savedState.getString(CameraPrivacyModel.CAMERA_ID_RECEIVED)).thenReturn(cameraId);
        when(savedState.containsKey(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(true);
        when(savedState.getBoolean(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(false);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager).setCameraResourceReady(cameraId);
        verify(model).privacyButtonFailed();
        verify(model, never()).privacyButtonWorking();
    }

    @Test
    public void onRestoreInstantState_NotCameraAvailable() {
        /*Arrange*/
        Bundle savedState = mock(Bundle.class);
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(savedState.containsKey(CameraPrivacyModel.CAMERA_ID_RECEIVED)).thenReturn(false);
        when(savedState.containsKey(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(false);
        when(savedState.getBoolean(CameraPrivacyModel.PRIVACY_BUTTON_WORKING)).thenReturn(false);
        /*Act*/
        presenter.onRestoreInstanceState(savedState);
        /*Assert*/
        verify(manager, never()).setCameraResourceReady(any());
        verify(model, never()).privacyButtonFailed();
        verify(model, never()).privacyButtonWorking();
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_NoRequest() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(false);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model).fail(anyString());
        verify(view).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionDenied_Request() {
        /*Arrange*/
        when(model.shouldRequestPermissions()).thenReturn(true);
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionDeniedEvent());
        /*Assert*/
        verify(model, never()).fail(anyString());
        verify(view, never()).finishActivity();
    }

    @Test
    public void onRequestPermissionResult_PermissionGranted() {
        /*Act*/
        presenter.onRequestPermissionResult(new RuntimePermissionHandler.PermissionGrantedEvent());
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
        verify(view).showFloatingTutorialButton();
        verify(view).showDialogOne();
    }

    @Test
    public void onSurfaceTextureAvailable_PermissionGranted_ImgNotTaken() {
        /*Arrange*/
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(true);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view).showCameraAccessSpinner();
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onSurfaceTextureAvailable_NotPermission() {
        /*Arrange*/
        ResourcesManager manager = mock(ResourcesManager.class);
        when(model.getResourcesManager()).thenReturn(manager);
        when(model.isPermissionGranted()).thenReturn(false);
        /*Act*/
        presenter.onSurfaceTextureAvailable(null);
        /*Assert*/
        verify(manager).setSurfaceTextureReady();
        verify(view, never()).showCameraAccessSpinner();
        verify(model, never()).openCamera(view.getTextureView());
    }

    @Test
    public void onCameraOutputSetEvent(){
        /*Arrange*/
        Size previewRatio = new Size(100,100);
        /*Act*/
        presenter.onCameraOutputSetEvent(new CameraPrivacyModel.CameraOutputSetEvent(previewRatio));
        /*Assert*/
        verify(view).setMirrorEffect(model.isMirrored());
        verify(view).setAspectRatio(eq(previewRatio.getWidth()), eq(previewRatio.getHeight()));
    }

    @Test
    public void onCameraPreviewSetEvent(){
        /*Act*/
        presenter.onCameraPreviewSetEvent(null);
        /*Assert*/
        verify(view).hideCameraAccessSpinner();
    }

    @Test
    public void onOpenCameraError(){
        /*Arrange*/
        String message = "msg";
        /*Act*/
        presenter.onOpenCameraError(new CameraPrivacyModel.OnCameraError(message));
        /*Assert*/
        failCalled();
    }

    @Test
    public void onCameraTargetAvailableTest(){
        /*Act*/
        presenter.onCameraTargetAvailable(null);
        /*Assert*/
        verify(model).openCamera(view.getTextureView());
    }

    @Test
    public void onFailButtonClickedTest(){
        /*Act*/
        presenter.onFailButtonClicked(null);
        /*Assert*/
        failCalled();
    }

    @Test
    public void onDialogTwoPassEventTest(){
        /*Act*/
        presenter.onDialogTwoPassEvent(null);
        /*Assert*/
        verify(view).showDialogTurnOffPrivacy();
        verify(model).privacyButtonWorking();
    }

    @Test
    public void onDialogTwoFailEventTest(){
        /*Act*/
        presenter.onDialogTwoFailEvent(null);
        /*Assert*/
        verify(view).showDialogTurnOffPrivacy();
        verify(model).privacyButtonFailed();
    }

    @Test
    public void onDialogTurnOffPrivacyEventTrueTest() {
        //Arrange
        when(model.isPrivacyWorking()).thenReturn(true);
        DialogTurnOffPrivacyEvent event = new DialogTurnOffPrivacyEvent();
        //Act
        presenter.onDialogTurnOffPrivacyEvent(event);
        //Assert
        verify(model).pass();
        verify(view).finishActivity();
        verify(model, never()).fail();
    }

    @Test
    public void onDialogTurnOffPrivacyEventFalseTest() {
        //Arrange
        when(model.isPrivacyWorking()).thenReturn(false);
        DialogTurnOffPrivacyEvent event = new DialogTurnOffPrivacyEvent();
        //Act
        presenter.onDialogTurnOffPrivacyEvent(event);
        //Assert
        verify(model).fail();
        verify(view).finishActivity();
        verify(model, never()).pass();
    }

    /* Pass or Fail Check */
    public void failCalled(){
        verify(model).fail(any());
        verify(view).finishActivity();
    }
}