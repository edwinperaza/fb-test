package com.facebook.devices.cameraprivacycheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.CameraPrivacyActivity;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.mvp.view.CameraPrivacyView;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.AutoFitTextureView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static com.facebook.devices.mvp.view.CameraPrivacyView.DialogOneEvent;
import static com.facebook.devices.mvp.view.CameraPrivacyView.DialogTurnOffPrivacyEvent;
import static com.facebook.devices.mvp.view.CameraPrivacyView.DialogTwoFailEvent;
import static com.facebook.devices.mvp.view.CameraPrivacyView.DialogTwoPassEvent;
import static com.facebook.devices.mvp.view.CameraPrivacyView.OnSurfaceTextureAvailable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class CameraPrivacyViewTest {

    private CameraPrivacyActivity activity;
    private CameraPrivacyView view;

    @Mock
    private BusProvider.Bus bus;

    @Mock
    private SurfaceTexture surfaceTextureMock;

    /* Bindings */
    private FloatingTutorialView floatingTutorialView;
    private ProgressBar cameraAccessSpinner;
    private AutoFitTextureView textureView;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, CameraPrivacyActivity.class);
        activity = Robolectric.buildActivity(CameraPrivacyActivity.class, intent).create().start().resume().get();

        view = new CameraPrivacyView(activity, bus);

        /* Bindings */
        floatingTutorialView = ShadowView.getTutorialFloatingButton(view);
        cameraAccessSpinner = activity.findViewById(R.id.camera_access_spinner);
        textureView = activity.findViewById(R.id.texture_camera);
    }

    @Test
    public void isPortraitTest() {
        //Arrange
        activity.getResources().getConfiguration().orientation = Configuration.ORIENTATION_LANDSCAPE;
        //Assert
        assertFalse(view.isPortrait());
    }

    @Test
    public void isPortraitFalseTest() {
        //Arrange
        activity.getResources().getConfiguration().orientation = Configuration.ORIENTATION_PORTRAIT;
        //Assert
        assertTrue(view.isPortrait());
    }

    @Test
    public void setupTextureView_SurfaceAvailable() {
        /*Arrange*/
        int width = 100;
        int height = 200;
        /*Act*/
        textureView.getSurfaceTextureListener().onSurfaceTextureAvailable(surfaceTextureMock, width, height);
        /*Assert*/
        ArgumentCaptor<OnSurfaceTextureAvailable> captor = ArgumentCaptor.forClass(OnSurfaceTextureAvailable.class);
        verify(bus).post(captor.capture());
        assertEquals(captor.getValue().surface, surfaceTextureMock);
        assertEquals(captor.getValue().height, height);
        assertEquals(captor.getValue().width, width);
    }

    @Test
    public void setupTextureView_SurfaceTextureDestroyed() {
        //Act
        assertFalse(textureView.getSurfaceTextureListener().onSurfaceTextureDestroyed(surfaceTextureMock));
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.camera_privacy_dialog_tutorial_message);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.onFloatingTutorialClicked();

        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowAlertDialog = new ShadowMaterialDialog(Shadows.shadowOf(dialog));
        assertEquals(shadowAlertDialog.getTitle(), title);
        assertEquals(shadowAlertDialog.getMessage(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showFloatingTutorialButtonTest() {
        //Act
        view.showFloatingTutorialButton();
        //Assert
        assertTrue(floatingTutorialView.isButtonVisible());
    }

    @Test
    public void showDialogOneTest() {
        //Arrange
        String message = activity.getString(R.string.privacy_dialog_message_one);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showDialogOne();
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);

        //Assert
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showDialogOneOptionOneTest() {
        //Act
        view.showDialogOne();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogOneEvent.class));
    }

    @Test
    public void showDialogTwoTest() {
        //Arrange
        String message = activity.getString(R.string.privacy_dialog_message_two);
        String positiveText = activity.getString(R.string.yes_base);
        String negativeText = activity.getString(R.string.no_base);

        //Act
        view.showDialogTwo();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
        assertEquals(bannerView.getBannerTextOptionTwo(), negativeText);
    }

    @Test
    public void showDialogTwoOptionOneTest() {
        //Act
        view.showDialogTwo();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogTwoPassEvent.class));
    }

    @Test
    public void showDialogTwoOptionTwoTest() {
        //Act
        view.showDialogTwo();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionTwoButton().performClick();
        verify(bus).post(any(DialogTwoFailEvent.class));
    }

    @Test
    public void showDialogTurnOffPrivacyTest() {
        //Arrange
        String message = activity.getString(R.string.privacy_dialog_turn_off_privacy);
        String positiveText = activity.getString(R.string.ok_base);

        //Act
        view.showDialogTurnOffPrivacy();

        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), positiveText);
    }

    @Test
    public void showDialogTurnOffPrivacyOptionOneTest() {
        //Act
        view.showDialogTurnOffPrivacy();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(DialogTurnOffPrivacyEvent.class));
    }

    @Test
    public void showCameraAccessSpinnerTest() {
        //Act
        view.showCameraAccessSpinner();
        //Assert
        assertEquals(cameraAccessSpinner.getVisibility(), View.VISIBLE);
    }

    @Test
    public void hideCameraAccessSpinnerTest() {
        //Act
        view.hideCameraAccessSpinner();
        //Assert
        assertEquals(cameraAccessSpinner.getVisibility(), View.GONE);
    }

    @Test
    public void onFailureDialogOptionSelectedEvent() {
        //Arrange
        SurfaceTexture surfaceTexture = new SurfaceTexture(0);
        int width = 100;
        int height = 200;
        OnSurfaceTextureAvailable event = new OnSurfaceTextureAvailable(surfaceTexture, width, height);
        //Assert
        assertEquals(event.surface, surfaceTexture);
        assertEquals(event.width, width);
        assertEquals(event.height, height);
    }

    @Test
    public void textureViewTest() {
        assertNotNull(view.getTextureView());
    }

    @Test
    public void isAvailable() {
        /*Assert*/
        assertFalse(view.isAvailable());
    }

    @Test
    public void setAspectRatioTest() {
        //Arrange
        int width = 100;
        int height = 200;
        //Act
        view.setAspectRatio(width, height);
        //Assert
        assertEquals(textureView.getAspectRatio().getWidth(), width);
        assertEquals(textureView.getAspectRatio().getHeight(), height);
    }
}
