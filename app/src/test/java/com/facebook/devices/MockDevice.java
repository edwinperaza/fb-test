package com.facebook.devices;


import com.facebook.hwtp.service.ConnectionService;

public class MockDevice implements ConnectionService.Device {
    @Override
    public void pass(String... output) {

    }

    @Override
    public void fail(String... output) {

    }

    @Override
    public void cancel(String... output) {

    }

    @Override
    public boolean isConnected() {
        return false;
    }
}
