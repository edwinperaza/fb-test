package com.facebook.devices.utils;

import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.utils.recyclers.DigestTest;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TestUtilsTest {

    private List<GroupTestsEntity> groupsEntity;
    private Map<String, TestInfoEntity> testDefinitions;

    private String groupOne = "groupOne";
    private String groupTwo = "groupTwo";
    private String groupThree = "groupThree";

    @Before
    public void setup() {
        /*Groups*/
        groupsEntity = new ArrayList<>();
        groupsEntity.add(new GroupTestsEntity("0", groupOne));
        groupsEntity.add(new GroupTestsEntity("0", groupTwo));
        groupsEntity.add(new GroupTestsEntity("0", groupThree));

        /*Tests*/
        TestInfoEntity test1 = new TestInfoEntity("0", "com.facebook.devices.activities.sometest", "Test One", "Description", "Some", 0, 1, 0);
        TestInfoEntity test2 = new TestInfoEntity("1", "com.facebook.devices.activities.sometest", "Test Two", "Description", "Some", 0, 2, 0);
        TestInfoEntity test3 = new TestInfoEntity("2", "com.facebook.devices.activities.sometest", "Test Three", "Description", "Some", 0, 3, 0);
        TestInfoEntity test4 = new TestInfoEntity("3", "com.facebook.devices.activities.sometest", "Test Four", "Description", "Some", 0, 4, 0);
        TestInfoEntity test5 = new TestInfoEntity("4", "com.facebook.devices.activities.sometest", "Test Five", "Description", "Some", 0, 5, 0);
        TestInfoEntity test6 = new TestInfoEntity("5", "com.facebook.devices.activities.sometest", "Test Six", "Description", "Some", 0, 6, 0);
        test1.groupId = groupOne;
        test2.groupId = groupOne;
        test3.groupId = groupTwo;
        test4.groupId = groupTwo;
        test5.groupId = groupThree;
        test6.groupId = groupThree;


        testDefinitions = new HashMap<>();
        testDefinitions.put("Test1", test1);
        testDefinitions.put("Test2", test2);
        testDefinitions.put("Test3", test3);
        testDefinitions.put("Test4", test4);
        testDefinitions.put("Test5", test5);
        testDefinitions.put("Test6", test6);
    }

    @Test
    public void processTest() {
        /*Act*/
        List<DigestTest> digestTests = TestUtils.processTests(groupsEntity, testDefinitions);
        /*Assert*/

        /*group*/
        assertEquals(digestTests.get(0).getTest().groupId, groupsEntity.get(0).getGroupType());
        assertEquals(digestTests.get(1).getTest().groupId, groupsEntity.get(0).getGroupType());
        assertEquals(digestTests.get(2).getTest().groupId, groupsEntity.get(1).getGroupType());
        assertEquals(digestTests.get(3).getTest().groupId, groupsEntity.get(1).getGroupType());
        assertEquals(digestTests.get(4).getTest().groupId, groupsEntity.get(2).getGroupType());
        assertEquals(digestTests.get(5).getTest().groupId, groupsEntity.get(2).getGroupType());

        /*Test*/
        assertEquals(digestTests.get(0).getTest().id, testDefinitions.get("Test1").getId());
        assertEquals(digestTests.get(1).getTest().id, testDefinitions.get("Test2").getId());
        assertEquals(digestTests.get(2).getTest().id, testDefinitions.get("Test3").getId());
        assertEquals(digestTests.get(3).getTest().id, testDefinitions.get("Test4").getId());
        assertEquals(digestTests.get(4).getTest().id, testDefinitions.get("Test5").getId());
        assertEquals(digestTests.get(5).getTest().id, testDefinitions.get("Test6").getId());
    }
}
