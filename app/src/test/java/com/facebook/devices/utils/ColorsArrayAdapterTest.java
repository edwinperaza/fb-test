package com.facebook.devices.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.activities.BaseActivity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.utils.recyclers.ColorsArrayAdapter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class ColorsArrayAdapterTest {

    private Activity activity;
    private ColorsArrayAdapter adapter;
    private Spinner spinner;

    private List<ColorEntity> colors = new ArrayList<ColorEntity>() {{
        add(new ColorEntity(0, "#FFFFFF", "color1"));
        add(new ColorEntity(1, "#FFFFF0", "color2"));
        add(new ColorEntity(2, "#FFFF00", "color3"));
    }};

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BaseActivity.class);
        activity = Robolectric.buildActivity(BaseActivity.class, intent).create().start().resume().get();

        spinner = new Spinner(activity);

        adapter = new ColorsArrayAdapter(activity, R.layout.color_spinner_item_layout, colors);
    }

    @Test
    public void getDropDownView() {
        /*Act*/
        View view = adapter.getDropDownView(0, null, spinner);
        /*Assert*/
        assertEquals(((TextView) view.findViewById(R.id.tv_color_name)).getText().toString(), colors.get(0).getName());
    }

    @Test
    public void getView() {
        /*Act*/
        View view = adapter.getView(0, null, spinner);
        /*Assert*/
        assertEquals(((TextView) view.findViewById(R.id.tv_color_name)).getText().toString(), colors.get(0).getName());
    }

}
