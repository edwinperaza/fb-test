package com.facebook.devices.utils;


import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

public class PlanXmlParserTest {

    private static final String TAG_KEY_START = "${";
    private static final String TAG_KEY_END = "}";
    private Map<String, String> mapValues = new HashMap<>();
    private Map<String, String> mapEmptyValues = new HashMap<>();
    private String value;
    private String key;
    private String name;
    private String type;
    private String testId;
    private boolean readOnly;

    @Before
    public void setup() {
        value = "ShowThisValue";
        key = "key";
        name = "name";
        type = "type";
        testId = "testId";
        readOnly = false;
        mapValues.put(key, value);
    }

    @Test
    public void parserKeyMiddleTest() {
        /*Arrange*/
        String init = "inthe" + TAG_KEY_START + key + TAG_KEY_END + "middle";
        String finalString = "inthe" + value + "middle";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyMiddleEmptyMapTest() {
        /*Arrange*/
        String init = "inthe" + TAG_KEY_START + key + TAG_KEY_END + "middle";
        String finalString = "inthemiddle";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapEmptyValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserEscapeKeyMiddleTest() {
        /*Arrange*/
        String init = "inthe" + TAG_KEY_START + TAG_KEY_START + key + TAG_KEY_END + "middle";
        String finalString = "inthe" + TAG_KEY_START + key + TAG_KEY_END + "middle";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyBeginningTest() {
        /*Arrange*/
        String init = TAG_KEY_START + key + TAG_KEY_END + "atthebeginning";
        String finalString = value + "atthebeginning";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyBeginningEmptyMapTest() {
        /*Arrange*/
        String init = TAG_KEY_START + key + TAG_KEY_END + "atthebeginning";
        String finalString = "atthebeginning";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapEmptyValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserEscapeKeyBeginningTest() {
        /*Arrange*/
        String init = TAG_KEY_START + TAG_KEY_START + key + TAG_KEY_END + "atthebeginning";
        String finalString = TAG_KEY_START + key + TAG_KEY_END + "atthebeginning";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyEndTest() {
        /*Arrange*/
        String init = "attheend" + TAG_KEY_START + key + TAG_KEY_END;
        String finalString = "attheend" + value;
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyEndEmptyMapTest() {
        /*Arrange*/
        String init = "attheend" + TAG_KEY_START + key + TAG_KEY_END;
        String finalString = "attheend";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapEmptyValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserEscapeKeyEndTest() {
        /*Arrange*/
        String init = "attheend" + TAG_KEY_START + TAG_KEY_START + key + TAG_KEY_END;
        String finalString = "attheend" + TAG_KEY_START + key + TAG_KEY_END;
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserKeyIncompleteEndTest() {
        /*Arrange*/
        String init = "attheend" + TAG_KEY_START + "$";
        String finalString = "attheend" + TAG_KEY_START + "$";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserOnlyKeyTest() {
        /*Arrange*/
        String init = TAG_KEY_START + key + TAG_KEY_END;
        String finalString = value;
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserOnlyKeyEmptyMapTest() {
        /*Arrange*/
        String init = TAG_KEY_START + key + TAG_KEY_END;
        String finalString = "";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapEmptyValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserOnlyEscapeKeyTest() {
        /*Arrange*/
        String init = TAG_KEY_START + TAG_KEY_START + key + TAG_KEY_END;
        String finalString = TAG_KEY_START + key + TAG_KEY_END;
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;
        //Assert
        assertEquals(finalString, result);
    }

    @Test
    public void parserWithoutKeyTest() {
        /*Arrange*/
        String init = key;
        String finalString = key;
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapValues).value;;
        //Assert
        assertEquals(result, finalString);
    }

    @Test
    public void parserEscapeEmptyKeyTest() {
        /*Arrange*/
        String init = TAG_KEY_START + TAG_KEY_START + TAG_KEY_END + "text}";
        String finalString = TAG_KEY_START + TAG_KEY_END + "text}";
        /*Act*/
        String result = PlanXmlParser.getArgument(type, name, readOnly, init, testId, mapEmptyValues).value;
        //Assert
        assertEquals(finalString, result);
    }
}
