package com.facebook.devices.utils;

import com.facebook.devices.mvp.view.TestViewBase;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.devices.utils.customviews.FloatingTutorialView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ShadowView {

    public static BannerView getCurrentBanner(TestViewBase viewBase) {
        try {
            Method method = TestViewBase.class.getDeclaredMethod("getBannerView");
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            return (BannerView) method.invoke(viewBase);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }

    public static FloatingTutorialView getTutorialFloatingButton(TestViewBase viewBase) {
        try {
            Method method = TestViewBase.class.getDeclaredMethod("getFloatingTutorialView");
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            return (FloatingTutorialView) method.invoke(viewBase);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }
}
