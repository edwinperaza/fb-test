package com.facebook.devices.utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.View;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.BaseActivity;
import com.facebook.devices.db.entity.ArgumentEntity;
import com.facebook.devices.db.entity.ColorEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.utils.listeners.ArgumentRecyclerViewListener;
import com.facebook.devices.utils.recyclers.ArgumentsRecyclerAdapter;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class ArgumentsRecyclerAdapterTest {

    private ArgumentsRecyclerAdapter adapter;
    private Activity activity;
    private List<ArgumentEntity> argumentEntities = new ArrayList<ArgumentEntity>() {{
        add(new ArgumentEntity("Test", "type", "value", "0", false));
        add(new ArgumentEntity("Test", "type", "value", "1", false));
        add(new ArgumentEntity("Test", "type", "value", "2", false));
        add(new ArgumentEntity("Test", "type", "value", "3", false));
        add(new ArgumentEntity("Test", "type", "value", "4", false));
    }};
    private RecyclerView recyclerView;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BaseActivity.class);
        activity = Robolectric.buildActivity(BaseActivity.class, intent).create().start().resume().get();

        recyclerView = new RecyclerView(activity);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));

        adapter = new ArgumentsRecyclerAdapter(activity, argumentEntities);
        recyclerView.setAdapter(adapter);
    }


    @Test
    public void setListener() {
        /*Arrange*/
        ArgumentRecyclerViewListener<ArgumentEntity> listener = new ArgumentRecyclerViewListener<ArgumentEntity>() {
            @Override
            public void recyclerViewOnItemClickListener(View view, int position, ArgumentEntity object) {

            }

            @Override
            public void recyclerViewOnItemTextChange(int position, ArgumentEntity object) {

            }

            @Override
            public void recyclerViewOnItemLongClickListener(View view, int position, ArgumentEntity object) {

            }
        };
        /*Act*/
        adapter.setListener(listener);
        /*Assert*/
        assertNotNull(adapter.getListener());
    }

    @Test
    public void getItemList() {
        /*Assert*/
        assertNotNull(adapter.getArgumentEntityList());
        assertTrue(adapter.getArgumentEntityList().size() > 0);
    }

    @Test
    public void updateItems() {
        /*Arrange*/
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(new ArgumentEntity("Aux", "type", "value", "0", false));
        }};

        TestInfoEntity test1 = new TestInfoEntity("0", "com.facebook.devices.activities.sometest", "Test One", "Description", "Some", 0, 1, 0);
        TestInfoEntity test2 = new TestInfoEntity("1", "com.facebook.devices.activities.sometest", "Test Two", "Description", "Some", 0, 2, 0);
        test1.groupId = "GroupOne";
        test2.groupId = "GroupTwo";

        Map<String, TestInfoEntity> testDefinitions = new HashMap<>();
        testDefinitions.put("Test1", test1);
        testDefinitions.put("Test2", test2);

        List<ColorEntity> colorEntityList = new ArrayList<ColorEntity>() {{
            add(new ColorEntity(1, "#FFFFFF", "color one"));
            add(new ColorEntity(1, "#FFFFF0", "color two"));
            add(new ColorEntity(1, "#FFFF00", "color three"));
        }};

        /*Act*/
        adapter.updateItems(argumentsAux, testDefinitions, colorEntityList);

        /*Assert*/
        assertEquals(adapter.getArgumentEntityList().get(0), argumentsAux.get(0));
    }

    @Test
    public void getItemViewType_color() {
        /*Arrange*/
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(new ArgumentEntity("Aux", "color", "value", "0", false));
        }};
        adapter.updateItems(argumentsAux, new HashMap<>(), new ArrayList<>());
        /*Assert*/
        assertEquals(adapter.getItemViewType(0), 0);
    }

    @Test
    public void getItemViewType_no_color() {
        /*Arrange*/
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(new ArgumentEntity("Aux", "other", "value", "0", false));
        }};
        adapter.updateItems(argumentsAux, new HashMap<>(), new ArrayList<>());
        /*Assert*/
        assertNotSame(adapter.getItemViewType(0), 0);
    }

    @Test
    @Ignore /*Not working for now - pointerException on spinner adapter loading*/
    public void onCreateViewHolder_color() {
        /*Arrange*/
        int type = 0; /*color*/
        /*Act*/
        RecyclerView.ViewHolder viewHolder = adapter.onCreateViewHolder(recyclerView, type);
        /*Assert*/
        assertTrue(viewHolder instanceof ArgumentsRecyclerAdapter.TestColorViewHolder);
    }

    @Test
    public void onCreateViewHolder_other() {
        /*Arrange*/
        int type = 1; /*other*/
        /*Act*/
        RecyclerView.ViewHolder viewHolder = adapter.onCreateViewHolder(recyclerView, type);
        /*Assert*/
        assertTrue(viewHolder instanceof ArgumentsRecyclerAdapter.TestGeneralViewHolder);
    }

    @Test
    public void onBindViewHolder_other_arg_int() throws NoSuchFieldException, IllegalAccessException {
        /*Arrange*/
        int type = 1; /*general*/
        ArgumentsRecyclerAdapter.TestGeneralViewHolder holder = (ArgumentsRecyclerAdapter.TestGeneralViewHolder) adapter.onCreateViewHolder(recyclerView, type);
        ArgumentEntity argument = new ArgumentEntity("Aux", "int", "value", "Test1", false);
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(argument);
        }};
        TestInfoEntity test = new TestInfoEntity("0", "com.facebook.devices.activities.sometest", "Test One", "Description", "Some", 0, 1, 0);
        Map<String, TestInfoEntity> testDefinitions = new HashMap<>();
        testDefinitions.put("Test1", test);

        ColorEntity colorEntity = new ColorEntity(1, "#FFFFFF", "color one");
        List<ColorEntity> colorEntityList = new ArrayList<ColorEntity>() {{
            add(colorEntity);
        }};

        adapter.updateItems(argumentsAux, testDefinitions, colorEntityList);

        Field field = RecyclerView.ViewHolder.class.getDeclaredField("mItemViewType");
        field.setAccessible(true);
        field.set(holder, type);

        int position = 0;

        /*Act*/
        adapter.onBindViewHolder(holder, position);
        /*Assert*/
        assertEquals(holder.tvArgumentTitle.getText().toString(), argumentsAux.get(position).name);
        assertEquals(holder.tvArgumentTestTitle.getText().toString(), testDefinitions.get(argumentsAux.get(position).testId).name);
        assertEquals(holder.etArgumentValue.getInputType(), InputType.TYPE_CLASS_NUMBER);
        assertEquals(holder.etArgumentValue.getText().toString(), argumentsAux.get(position).value);
        assertTrue(holder.etArgumentValue.isEnabled());
    }

    @Test
    public void onBindViewHolder_other_arg_noint() throws NoSuchFieldException, IllegalAccessException {
        /*Arrange*/
        int type = 1; /*general*/
        ArgumentsRecyclerAdapter.TestGeneralViewHolder holder = (ArgumentsRecyclerAdapter.TestGeneralViewHolder) adapter.onCreateViewHolder(recyclerView, type);
        ArgumentEntity argument = new ArgumentEntity("Aux", "other", "value", "Test1", false);
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(argument);
        }};
        TestInfoEntity test = new TestInfoEntity("0", "com.facebook.devices.activities.sometest", "Test One", "Description", "Some", 0, 1, 0);
        Map<String, TestInfoEntity> testDefinitions = new HashMap<>();
        testDefinitions.put("Test1", test);

        ColorEntity colorEntity = new ColorEntity(1, "#FFFFFF", "color one");
        List<ColorEntity> colorEntityList = new ArrayList<ColorEntity>() {{
            add(colorEntity);
        }};

        adapter.updateItems(argumentsAux, testDefinitions, colorEntityList);

        Field field = RecyclerView.ViewHolder.class.getDeclaredField("mItemViewType");
        field.setAccessible(true);
        field.set(holder, type);

        int position = 0;

        /*Act*/
        adapter.onBindViewHolder(holder, position);
        /*Assert*/
        assertEquals(holder.tvArgumentTitle.getText().toString(), argumentsAux.get(position).name);
        assertEquals(holder.tvArgumentTestTitle.getText().toString(), testDefinitions.get(argumentsAux.get(position).testId).name);
        assertEquals(holder.etArgumentValue.getInputType(), InputType.TYPE_CLASS_TEXT);
        assertEquals(holder.etArgumentValue.getText().toString(), argumentsAux.get(position).value);
        assertTrue(holder.etArgumentValue.isEnabled());
    }

    @Test
    @Ignore /*Not working for now - pointerException on spinner adapter loading*/
    public void onBindViewHolder_color() throws NoSuchFieldException, IllegalAccessException {
        /*Arrange*/
        int type = 0; /*color*/
        ArgumentsRecyclerAdapter.TestColorViewHolder holder = (ArgumentsRecyclerAdapter.TestColorViewHolder) adapter.onCreateViewHolder(recyclerView, type);
        ArgumentEntity argument = new ArgumentEntity("Aux", "int", "value", "Test1", false);
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(argument);
        }};
        TestInfoEntity test = new TestInfoEntity("0", "com.facebook.devices.activities.sometest", "Test One", "Description", "Some", 0, 1, 0);
        Map<String, TestInfoEntity> testDefinitions = new HashMap<>();
        testDefinitions.put("Test1", test);

        ColorEntity colorEntity = new ColorEntity(1, "#FFFFFF", "color one");
        List<ColorEntity> colorEntityList = new ArrayList<ColorEntity>() {{
            add(colorEntity);
        }};

        adapter.updateItems(argumentsAux, testDefinitions, colorEntityList);

        Field field = RecyclerView.ViewHolder.class.getDeclaredField("mItemViewType");
        field.setAccessible(true);
        field.set(holder, type);

        int position = 0;

        /*Act*/
        adapter.onBindViewHolder(holder, position);
        /*Assert*/
        assertEquals(holder.tvArgumentTitle.getText().toString(), argumentsAux.get(position).name);
        assertEquals(holder.tvArgumentTestTitle.getText().toString(), testDefinitions.get(argumentsAux.get(position).testId).name);
        assertTrue(holder.etArgumentOptions.isEnabled());
    }

    @Test
    public void getItemCount() {
        /*Arrange*/
        List<ArgumentEntity> argumentsAux = new ArrayList<ArgumentEntity>() {{
            add(new ArgumentEntity("Aux", "type", "value", "0", false));
        }};

        adapter.updateItems(argumentsAux, new HashMap<>(), new ArrayList<>());
        /*Assert*/
        assertEquals(adapter.getItemCount(), argumentsAux.size());
    }

}
