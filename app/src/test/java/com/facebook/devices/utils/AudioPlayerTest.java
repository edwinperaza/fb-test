package com.facebook.devices.utils;

import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.BaseActivity;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.lang.reflect.Field;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class AudioPlayerTest {

    private BaseActivity activity;
    private short[] bufferData = new short[]{30, 20, 20, 30, 20, 20, 30, 20, 20, 30, 20, 20, 30, 20, 20, 30, 20, 20};

    @Mock
    private AudioPlayer.AudioPlayerListener listener;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BaseActivity.class);
        intent.putExtra("color", "#ffff0000");
        activity = Robolectric.buildActivity(BaseActivity.class, intent).create().start().resume().get();
    }


    @Test
    public void player_constructor_overload() {
        /*Arrange*/
        int sampleRate = 44100;
        /*Act*/
        ShadowAudioPlayer audioPlayer = new ShadowAudioPlayer(new AudioPlayer(sampleRate));
        /*Assert*/
        try {
            assertNotNull(audioPlayer.getBufferSize());
            assertNotNull(audioPlayer.getSampleRate());
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
    }

    @Test
    public void setSampleRate() {
        /*Arrange*/
        int sampleRate = 44100;
        AudioPlayer audioPlayer = new AudioPlayer(1000);
        /*Act*/
        audioPlayer.setSampleRate(sampleRate);
        ShadowAudioPlayer shadowAudioPlayer = new ShadowAudioPlayer(audioPlayer);
        /*Assert*/
        try {
            assertEquals(shadowAudioPlayer.getSampleRate(), sampleRate);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
    }

    @Test
    public void startPlaying_no_sample_rate() {
        /*Arrange*/
        AudioPlayer audioPlayer = new AudioPlayer();
        /*Act*/
        audioPlayer.startPlaying(null, bufferData);
        /*Assert*/
        assertFalse(audioPlayer.isRunning());
    }

    @Test
    @Ignore /*TODO - this test is not working on jenkins - seems verification runs before onPlayingDataAvailable is executed - inspect!*/
    public void onAvailableData() {
        /*Arrange*/
        AudioPlayer audioPlayer = new AudioPlayer(44100);
        /*Act*/
        audioPlayer.startPlaying(listener, bufferData);
        /*Assert*/
        verify(listener, after(1000).atLeastOnce()).onPlayingDataAvailable(any(short[].class));
    }

    @Test
    public void onPlaybackComplete() {
        /*Arrange*/
        AudioPlayer audioPlayer = new AudioPlayer(44100);
        /*Act*/
        audioPlayer.startPlaying(listener, bufferData);
        /*Assert*/
        verify(listener, after(1000)).onPlaybackCompleted();
    }

    @Test
    public void stopPlaying() {
        /*Arrange*/
        AudioPlayer audioPlayer = new AudioPlayer(44100);
        /*Act*/
        audioPlayer.startPlaying(listener, bufferData);
        audioPlayer.stopPlaying();
        /*Assert*/
        assertFalse(audioPlayer.isRunning());
    }


    private class ShadowAudioPlayer {

        private AudioPlayer audioPlayer;

        public ShadowAudioPlayer(AudioPlayer audioPlayer) {
            this.audioPlayer = audioPlayer;
        }

        public int getSampleRate() throws IllegalAccessException, NoSuchFieldException {
            Field field = audioPlayer.getClass().getDeclaredField("sampleRate");
            field.setAccessible(true);
            return (int) field.get(audioPlayer);
        }

        public int getBufferSize() throws NoSuchFieldException, IllegalAccessException {
            Field field = audioPlayer.getClass().getDeclaredField("bufferSize");
            field.setAccessible(true);
            return (int) field.get(audioPlayer);
        }
    }

}
