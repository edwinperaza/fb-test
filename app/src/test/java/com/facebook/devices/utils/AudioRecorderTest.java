package com.facebook.devices.utils;

import android.content.Intent;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.activities.BaseActivity;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationAfterDelay;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.after;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class AudioRecorderTest {

    private BaseActivity activity;
    private AudioRecorder audioRecorder;
    private int sampleRate = 4400;

    @Mock
    private AudioRecorder.AudioRecorderListener listener;

    @Before
    public void setup() {
        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);

        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);

        when(binder.getService()).thenReturn(new MockDevice());
        MockitoAnnotations.initMocks(this);

        Intent intent = new Intent(RuntimeEnvironment.application, BaseActivity.class);
        intent.putExtra("color", "#ffff0000");
        activity = Robolectric.buildActivity(BaseActivity.class, intent).create().start().resume().get();

        audioRecorder = new AudioRecorder(AudioRecorder.MODE_DETECTOR, sampleRate);
    }

    @Test
    public void sampleRate() {
        /*Act*/
        audioRecorder.setSampleRate(sampleRate);
        /*Assert*/
        assertEquals(audioRecorder.getSampleRate(), sampleRate);
        assertNotNull(audioRecorder.getBufferSize());
    }

    @Test
    @Ignore
    public void kill() {
        /*Arrange*/
        audioRecorder.startRecording(listener);
        assertTrue(audioRecorder.isRunning());
        /*Act*/
        audioRecorder.stopRecording();
        /*Assert*/
        assertFalse(audioRecorder.isRunning());
    }

    @Test
    @Ignore
    public void startRecording_Generic() {
        /*Act*/
        audioRecorder.startRecording(listener);
        /*Assert*/
        assertTrue(audioRecorder.isRunning());
        /*Finally*/
        audioRecorder.stopRecording();
    }

    @Test
    public void startRecording_NoSample() {
        /*Arrange*/
        AudioRecorder audioRecorder = new AudioRecorder(AudioRecorder.MODE_DETECTOR);
        /*Act*/
        audioRecorder.startRecording(listener);
        /*Assert*/
        assertFalse(audioRecorder.isRunning());
    }

    @Test
    public void startRecording_DetectorMode() {
        /*Arrange*/
        AudioRecorder audioRecorderAux = new AudioRecorder(AudioRecorder.MODE_DETECTOR, sampleRate);
        /*Act*/
        audioRecorderAux.startRecording(listener);
        /*Assert*/
        verify(listener, after(1000).atLeastOnce()).onAvailableData(any(short[].class));
        /*Finally*/
        audioRecorderAux.stopRecording();
        verify(listener, never()).onRecordCompleted(any(short[].class));
    }

    @Test
    @Ignore /*TODO - this test is not working on jenkins - seems verification runs before onRecordCompleted is executed - inspect!*/
    public void startRecording_RecordingMode() {
        /*Arrange*/
        AudioRecorder audioRecorderAux = new AudioRecorder(AudioRecorder.MODE_RECORD, sampleRate);
        /*Act*/
        audioRecorderAux.startRecording(listener);
        /*Assert*/
        verify(listener, after(1000).atLeastOnce()).onAvailableData(any(short[].class));
        /*Finally*/
        audioRecorderAux.stopRecording();
        verify(listener, after(5000)).onRecordCompleted(any(short[].class));
    }

    @Test
    public void simpleConstructor() {
        /*Arrange*/
        AudioRecorder audioRecorderAux = new AudioRecorder(AudioRecorder.MODE_DETECTOR);
        /*Assert*/
        assertEquals(0, audioRecorderAux.getSampleRate());
        assertEquals(0, audioRecorderAux.getBufferSize());
    }
}
