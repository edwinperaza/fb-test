package com.facebook.devices.memorycheck;

import android.os.Handler;

import com.facebook.devices.mvp.model.MemoryModel;
import com.facebook.devices.mvp.presenter.MemoryPresenter;
import com.facebook.devices.mvp.view.MemoryView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MemoryPresenterTest {

    private MemoryPresenter presenter;

    @Mock
    private MemoryView view;

    @Mock
    private MemoryModel model;

    @Mock
    private Handler mockHandler;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new MemoryPresenter(model, view);
        when(view.getUiHandler()).thenReturn(mockHandler);
    }

    @Test
    public void initCallbackTest(){
        //Arrange
        long threshold = 100;
        int limit = 15;
        String output = "";
        byte byte1 = 1;
        float progress = 20;
        int position = 5;
        ArgumentCaptor<MemoryModel.MemoryProgress> callback = ArgumentCaptor.forClass(MemoryModel.MemoryProgress.class);
        ArgumentCaptor<Runnable> runnableArgumentCaptor = ArgumentCaptor.forClass(Runnable.class);
        //Assert
        verify(view).showTutorialButton();
        verify(model).setMemoryProgressCallback(callback.capture());

        callback.getValue().onSetup(threshold);
        verify(view.getUiHandler()).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).showSetup();

        callback.getValue().onStart(limit);
        verify(view.getUiHandler(), times(2)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).setMin(0);
        verify(view).setMax(limit);
        verify(view).showStart();
        verify(view).showWritingDialog();

        callback.getValue().onValidation();
        verify(view.getUiHandler(), times(3)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).showValidation();
        verify(view).showValidationDialog();

        callback.getValue().onSucceed(output);
        verify(model).pass(output);
        verify(view).finishActivity();

        callback.getValue().onFail(output, byte1);
        verify(model).fail(output);
        verify(view, times(2)).finishActivity();

        callback.getValue().onProgress(progress, position);
        verify(view.getUiHandler(), times(4)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).updateProgressLabel(progress);
        verify(view).updateProgressBar(position);

        callback.getValue().onRemainingTime(0, 1,2,3,4,5);
        verify(view.getUiHandler(), times(5)).post(runnableArgumentCaptor.capture());
        runnableArgumentCaptor.getValue().run();
        verify(view).showRemainingTime(0, 1, 2, 3, 4, 5);
    }

    @Test
    public void onButtonFailurePressedTest(){
        //Act
        presenter.onButtonFailurePressed(null);
        //Assert
        verify(model).cancel();
    }

    @Test
    public void onResumeTest() {
        //Act
        presenter.onResume();
        //Assert
        verify(model).runMemoryTest();
    }

    @Test
    public void onStopTest() {
        //Act
        presenter.onStop();
        //Assert
        verify(model).stop();
    }

}
