package com.facebook.devices.memorycheck;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.MockDevice;
import com.facebook.devices.R;
import com.facebook.devices.ShadowMaterialDialog;
import com.facebook.devices.activities.MemoryActivity;
import com.facebook.devices.mvp.view.MemoryView;
import com.facebook.devices.mvp.view.MemoryView.OnButtonFailurePressed;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.utils.ShadowView;
import com.facebook.devices.utils.customviews.BannerView;
import com.facebook.hwtp.service.ConnectionService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.facebook.devices")
public class MemoryViewTest {

    private MemoryActivity activity;
    private MemoryView view;

    @Mock
    private BusProvider.Bus bus;

    private BannerView bannerView;
    private ProgressBar progressBar;
    private TextView label;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        ConnectionService.LocalBinder binder = mock(ConnectionService.LocalBinder.class);
        ShadowApplication.getInstance().setComponentNameAndServiceForBindService(null, binder);
        when(binder.getService()).thenReturn(new MockDevice());
        Intent intent = new Intent(RuntimeEnvironment.application, MemoryActivity.class);
        activity = Robolectric.buildActivity(MemoryActivity.class, intent).create().start().resume().get();

        view = new MemoryView(activity, bus);

        /*Bindings*/
        bannerView = ShadowView.getCurrentBanner(view);
        progressBar = activity.findViewById(R.id.progressBar);
        label = activity.findViewById(R.id.tv_label);
    }

    @Test
    public void tutorialDialogTest() {
        //Arrange
        String title = activity.getString(R.string.more_help_base);
        String message = activity.getString(R.string.memory_tutorial_message);
        String positiveText = activity.getString(R.string.ok_base);
        //Act
        view.onFloatingTutorialClicked();
        //Assert
        AlertDialog dialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowMaterialDialog shadowMaterialDialog = new ShadowMaterialDialog(Shadows.shadowOf(dialog));
        assertEquals(shadowMaterialDialog.getTitle(), title);
        assertEquals(shadowMaterialDialog.getMessage(), message);
        assertEquals(dialog.getButton(DialogInterface.BUTTON_POSITIVE).getText(), positiveText);
    }

    @Test
    public void showSetupTest() {
        //Arrange
        String message = activity.getString(R.string.allocate_memory);
        //Act
        view.showSetup();
        //Assert
        assertEquals(message, label.getText().toString());
    }

    @Test
    public void showStartTest() {
        //Arrange
        String message = activity.getString(R.string.start_memory_test);
        //Act
        view.showStart();
        //Assert
        assertEquals(message, label.getText().toString());
    }

    @Test
    public void showValidationTest() {
        //Arrange
        String message = activity.getString(R.string.memory_validation);
        //Act
        view.showValidation();
        //Assert
        assertEquals(message, label.getText().toString());
    }

    @Test
    public void setMaxTest() {
        //Arrange
        int max = 100;
        //Act
        view.setMax(max);
        //Assert
        assertEquals(max, progressBar.getMax());
    }

    @Test
    public void setMinTest() {
        //Arrange
        int min = 0;
        //Act
        view.setMin(min);
        //Assert
        assertEquals(min, progressBar.getMax());
    }

    @Test
    public void updateProgressBarTest() {
        //Arrange
        int value = 0;
        //Act
        view.updateProgressBar(value);
        //Assert
        assertEquals(progressBar.getProgress(), value);
    }

    @Test
    public void updateProgressLabelTest() {
        //Arrange
        TextView percentage = activity.findViewById(R.id.tv_percentage);
        String percentageValue = "25.0%";
        float value = 25;
        //Act
        view.updateProgressLabel(value);
        //Assert
        assertEquals(percentage.getText().toString(), percentageValue);
    }

    @Test
    public void showWritingDialogTest() {
        //Arrange
        String message = activity.getString(R.string.memory_writing_question);
        String negativeText = activity.getString(R.string.fail_base);
        //Act
        view.showWritingDialog();
        //Assert
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(message, messageDialog.getText().toString());
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);
    }

    @Test
    public void showWritingDialogOptionOneTest() {
        //Act
        view.showWritingDialog();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showValidationDialogTest() {
        //Arrange
        String message = activity.getString(R.string.memory_validation_question);
        String negativeText = activity.getString(R.string.fail_base);

        //Act
        view.showValidationDialog();

        //Assert
        TextView messageDialog = bannerView.getBannerContainer().findViewById(R.id.tv_message);
        assertEquals(messageDialog.getText(), message);
        assertEquals(bannerView.getBannerTextOptionOne(), negativeText);

        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showValidationDialogOptionOneTest() {
        //Act
        view.showValidationDialog();
        //Assert
        BannerView bannerView = ShadowView.getCurrentBanner(view);
        bannerView.getBannerOptionOneButton().performClick();
        verify(bus).post(any(OnButtonFailurePressed.class));
    }

    @Test
    public void showRemainingTimeTest() {
        //Arrange
        int icon = R.drawable.num_cero;
        ImageView ivHourOne = activity.findViewById(R.id.iv_hour_digit_one);
        ImageView ivHourTwo = activity.findViewById(R.id.iv_hour_digit_two);
        ImageView ivMinOne = activity.findViewById(R.id.iv_minutes_digit_one);
        ImageView ivMinTwo = activity.findViewById(R.id.iv_minutes_digit_two);
        ImageView ivSecOne = activity.findViewById(R.id.iv_seconds_digit_one);
        ImageView ivSecTwo = activity.findViewById(R.id.iv_seconds_digit_two);

        //Act
        view.showRemainingTime(icon, icon, icon, icon, icon, icon);

        //Assert
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivHourOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivHourTwo.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivMinOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivMinTwo.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivSecOne.getDrawable()).getCreatedFromResId())));
        assertThat(R.drawable.num_cero, is(equalTo(Shadows.shadowOf(ivSecTwo.getDrawable()).getCreatedFromResId())));
    }
}
