package com.facebook.devices.aloha.diagnostics.utils.listeners;


import android.view.View;

public interface RecyclerViewListener<T> {

    void recyclerViewOnItemClickListener(View view, int position, T object);

    void recyclerViewOnItemLongClickListener(View view, int position, T object);
}
