package com.facebook.devices.aloha.diagnostics.utils.recyclers;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.devices.R;
import com.facebook.devices.aloha.diagnostics.utils.listeners.RecyclerViewListener;
import com.facebook.devices.db.entity.GroupTestsEntity;
import com.facebook.devices.db.entity.TestInfoEntity;
import com.facebook.devices.utils.TestUtils;
import com.facebook.devices.utils.recyclers.DigestTest;

import java.util.List;
import java.util.Map;

public class GroupsRecyclerAdapter extends RecyclerView.Adapter<GroupsRecyclerAdapter.TestViewHolder> {

    private RecyclerViewListener<TestInfoEntity> listener;
    private Context context;
    private List<DigestTest> itemsList;

    public GroupsRecyclerAdapter(Context context, List<GroupTestsEntity> groupsEntity, Map<String, TestInfoEntity> testDefinitions) {
        this.context = context;
        this.itemsList = TestUtils.processTests(groupsEntity, testDefinitions);
    }

    public void setListener(RecyclerViewListener<TestInfoEntity> listener) {
        this.listener = listener;
    }

    public RecyclerViewListener<TestInfoEntity> getListener() {
        return listener;
    }

    public List<DigestTest> getItemsList() {
        return itemsList;
    }

    public void updateItems(List<GroupTestsEntity> groupsEntity, Map<String, TestInfoEntity> testDefinitions) {
        this.itemsList = TestUtils.processTests(groupsEntity, testDefinitions);
        notifyDataSetChanged();
    }

    @Override
    public GroupsRecyclerAdapter.TestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsRecyclerAdapter.TestViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.group_test_recycler_new_item_layout,
                        parent, false),listener);
    }

    @Override
    public void onBindViewHolder(GroupsRecyclerAdapter.TestViewHolder holder, int position) {

        DigestTest test = itemsList.get(position);

        holder.tvTestName.setText(test.getTest().getName());
        holder.tvTestDescription.setText(test.getTest().getDescription());

        if (position == 0 ||
                !itemsList.get(position-1).getTest().groupId.equals(test.getTest().groupId)) {
            holder.ivGroupIcon.setImageResource(context
                    .getResources()
                    .getIdentifier(
                            test.getIconResource(),
                            "drawable",
                            context.getPackageName()));
            holder.ivGroupIcon.setVisibility(View.VISIBLE);
            holder.lineSpacing.setVisibility(position == 0? View.GONE : View.VISIBLE);
        }else {
            holder.lineSpacing.setVisibility(View.GONE);
            holder.ivGroupIcon.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class TestViewHolder extends RecyclerView.ViewHolder {

        public CardView cardlayout;
        public ImageView ivGroupIcon;
        public TextView tvTestName;
        public TextView tvTestDescription;
        public View lineSpacing;

        public TestViewHolder(View itemView, RecyclerViewListener<TestInfoEntity> listener) {
            super(itemView);

            ivGroupIcon = itemView.findViewById(R.id.iv_icon_test_group);
            tvTestName = itemView.findViewById(R.id.tv_test_name);
            tvTestDescription = itemView.findViewById(R.id.tv_test_description);
            lineSpacing = itemView.findViewById(R.id.line_spacing);
            cardlayout = itemView.findViewById(R.id.card_layout);

            cardlayout.setOnClickListener(v -> {
                if (listener != null){
                    listener.recyclerViewOnItemClickListener(itemView, getLayoutPosition(), itemsList.get(getLayoutPosition()).getTest());
                }
            });
        }
    }
}