package com.facebook.devices.aloha.diagnostics.mvp.model;

import android.annotation.SuppressLint;
import android.os.Build;

import com.facebook.devices.BuildConfig;
import com.facebook.devices.bus.BusProvider;
import com.facebook.devices.db.TestDatabase;
import com.facebook.devices.db.entity.TestInfoEntity;

import java.util.List;
import java.util.concurrent.Executor;

public class TestsResultsModel {

    private static String VERSION = "1";
    private TestDatabase testDatabase;
    private boolean hasPermission;
    private String serialNumber = "N/A";
    private BusProvider.Bus bus;
    private Executor executor;

    public TestsResultsModel(
            TestDatabase testDatabase,
            boolean hasPermission,
            BusProvider.Bus bus,
            Executor executor) {
        this.testDatabase = testDatabase;
        this.hasPermission = hasPermission;
        this.bus = bus;
        this.executor = executor;
    }

    private List<TestInfoEntity> getAllTestInfoExecutedList() {
        return testDatabase.testInfoDao().getAllTestInfoExecutedList();
    }

    private boolean someTestsFailed() {
        return testDatabase.testInfoDao().getAllTestInfo().stream().anyMatch(p -> p.status == TestInfoEntity.FAIL);
    }

    private long getFailedTestCount() {
        return testDatabase.testInfoDao().getAllTestInfo().stream().filter(p -> p.status == TestInfoEntity.FAIL).count();
    }

    private long getPassedTestCount() {
        return testDatabase.testInfoDao().getAllTestInfo().stream().filter(p -> p.status == TestInfoEntity.PASS).count();
    }

    private long getTotalTimeSpent() {
        return testDatabase.testInfoDao().getAllTestInfoExecutedList().stream().mapToLong(t -> t.time).sum();
    }

    @SuppressLint({"MissingPermission", "NewApi"})
    public String getSerialNumber() {
        if (hasPermission) {
            serialNumber = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ? Build.getSerial() : Build.SERIAL;
        }
        return serialNumber;
    }

    @SuppressLint({"MissingPermission", "NewApi"})
    public String getData() {
        StringBuilder data = new StringBuilder(VERSION).append(";");
        if (hasPermission) {
            serialNumber = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ? Build.getSerial() : Build.SERIAL;
        }

        data.append(serialNumber)
                .append(";")
                .append(BuildConfig.VERSION_NAME)
                .append(";");

        StringBuilder tests = new StringBuilder();
        boolean overallResult = true;
        List<TestInfoEntity> executedList = testDatabase.testInfoDao().getAllTestInfoExecutedList();
        int size = executedList.size();
        for (int i = 0; i < size; i++) {
            TestInfoEntity ti = executedList.get(i);
            boolean pass = ti.status == TestInfoEntity.PASS;
            overallResult &= pass;
            tests.append(ti.id).append(":").append(booleanToInt(pass)).append(":").append(ti.time);
            if (i < size - 1) {
                tests.append(";");
            }
        }

        data.append(booleanToInt(overallResult)).append(";").append(tests);
        return data.toString();
    }

    private int booleanToInt(boolean bool) {
        return bool ? 1 : 0;
    }

    public void getTestResults() {
        executor.execute(() -> bus.post(new TestResultsEvent(getAllTestInfoExecutedList(),
                someTestsFailed(),
                getData(),
                getFailedTestCount(),
                getPassedTestCount(),
                getTotalTimeSpent(),
                getSerialNumber())));
    }

    public static class TestResultsEvent {
        public List<TestInfoEntity> testInfoList;
        public boolean someTestsFailed;
        public String data;
        public long totalFailedTest;
        public long totalPassedTest;
        public long totalTime;
        public String serialNumber;

        public TestResultsEvent(List<TestInfoEntity> testInfoList, boolean someTestsFailed, String data,
                long totalFailedTest, long totalPassedTest, long totalTime, String serialNumber) {
            this.testInfoList = testInfoList;
            this.someTestsFailed = someTestsFailed;
            this.data = data;
            this.totalFailedTest = totalFailedTest;
            this.totalPassedTest = totalPassedTest;
            this.totalTime = totalTime;
            this.serialNumber = serialNumber;
        }
    }
}