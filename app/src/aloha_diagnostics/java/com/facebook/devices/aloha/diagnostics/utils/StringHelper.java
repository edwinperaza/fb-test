package com.facebook.devices.aloha.diagnostics.utils;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class StringHelper {

    public static String getTimeFromMilliseconds(long millis) {
        
        if (millis >= TimeUnit.SECONDS.toMillis(1)) {
            return String.format(Locale.US, "%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
            );
        } else {
            return String.format(Locale.US, "0:0:0.%03d", millis);
        }
    }
}
