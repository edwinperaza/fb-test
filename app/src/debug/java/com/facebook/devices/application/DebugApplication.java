package com.facebook.devices.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.devices.db.TestDatabase;
import com.squareup.leakcanary.LeakCanary;

public class DebugApplication extends Application {

    private static DebugApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        LeakCanary.install(this);
        // Normal app init code...

        //TODO tidy up
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putBoolean("should_read_plan", true).apply();

        new Thread(() -> TestDatabase.getDatabase(this).clearAllTables()).start();

        // Catch unhandled exception
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> Log.e(DebugApplication.class.getName(), e.getMessage()));
    }

    public static DebugApplication getInstance() {
        if (instance == null) {
            instance = new DebugApplication();
        }
        return instance;
    }
}